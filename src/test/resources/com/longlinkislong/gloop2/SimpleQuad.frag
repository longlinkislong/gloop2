#version 330

in vec2 fTex;

uniform sampler2D uFont;

out vec4 fColor;

void main() {
    fColor = texture(uFont, fTex).rrrr;
}
