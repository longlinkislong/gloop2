#version 330

in vec2 vPos;
in vec2 vTex;

uniform mat4 uProjection;

out vec2 fTex;

void main() {
    gl_Position = uProjection * vec4(vPos, 0.0, 1.0);
    fTex = vTex;
}