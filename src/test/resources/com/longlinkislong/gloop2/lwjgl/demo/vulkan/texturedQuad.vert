#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec2 position;
layout (location = 1) in vec2 texCoords;

layout (location = 0) out vec2 texCoordsVarying;

void main() {
    texCoordsVarying = texCoords;

    gl_Position = vec4(position, 0.0, 1.0);
}
