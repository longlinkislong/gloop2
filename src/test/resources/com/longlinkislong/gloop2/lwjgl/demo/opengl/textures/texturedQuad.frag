#version 410

uniform sampler2D tex;

layout (location = 0) in vec2 texCoordsVarying;

layout (location = 0) out vec4 color;

void main() {
	color = texture(tex, texCoordsVarying);
}
