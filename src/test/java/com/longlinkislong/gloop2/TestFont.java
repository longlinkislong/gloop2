/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2;

import com.longlinkislong.gloop2.gl.GLBlendFunction;
import com.longlinkislong.gloop2.gl.GLBlendStateInfo;
import com.longlinkislong.gloop2.gl.GLBuffer;
import com.longlinkislong.gloop2.gl.GLBufferAccess;
import com.longlinkislong.gloop2.gl.GLBufferInfo;
import com.longlinkislong.gloop2.gl.GLClearBuffer;
import com.longlinkislong.gloop2.gl.GLClearStateInfo;
import com.longlinkislong.gloop2.gl.GLDraw;
import com.longlinkislong.gloop2.gl.GLFilter;
import com.longlinkislong.gloop2.gl.GLInternalFormat;
import com.longlinkislong.gloop2.gl.GLProgram;
import com.longlinkislong.gloop2.gl.GLProgramInfo;
import com.longlinkislong.gloop2.gl.GLProgramInfo.AttributeStateInfo;
import com.longlinkislong.gloop2.gl.GLSamplerInfo;
import com.longlinkislong.gloop2.gl.GLShader;
import com.longlinkislong.gloop2.gl.GLTexture;
import com.longlinkislong.gloop2.gl.GLTextureInfo;
import com.longlinkislong.gloop2.gl.GLUniform;
import com.longlinkislong.gloop2.gl.GLVertexArray;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo.AttributeDescription;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo.BindingDescription;
import com.longlinkislong.gloop2.gl.GLVertexFormat;
import com.longlinkislong.gloop2.gl.GLViewport;
import com.longlinkislong.gloop2.glfw.GLFWHints;
import com.longlinkislong.gloop2.glfw.GLFWWindow;
import com.longlinkislong.gloop2.image.FontImage;
import com.longlinkislong.gloop2.image.FontInfo;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.EnumSet;
import java.util.Set;
import org.joml.Matrix4f;
import org.junit.Test;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public class TestFont {
    static {
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }

    private final GLFWWindow window = new GLFWWindow();
    private final GLTexture ftex = new GLTexture();
    private final GLBuffer vtex = new GLBuffer();
    private final GLProgram program = new GLProgram();
    private final GLVertexArray model = new GLVertexArray();
    private FontImage font;
    private FloatBuffer projection;
    private int uFont;
    private int uProjection;
    private final GLClearStateInfo clearState = new GLClearStateInfo()
            .withBuffers(GLClearBuffer.COLOR)
            .withColor(0F, 0F, 1F, 1F);
    private final GLBlendStateInfo blendState = new GLBlendStateInfo()
            .withBlendEnable(true)
            .withSrcColorBlend(GLBlendFunction.SRC_ALPHA)
            .withDstColorBlend(GLBlendFunction.ONE_MINUS_SRC_ALPHA)
            .withSrcAlphaBlend(GLBlendFunction.ONE)
            .withDstAlphaBlend(GLBlendFunction.ONE_MINUS_SRC_ALPHA);

    private void initWindow() {
        final GLFWHints hints = new GLFWHints()
                .withContextHints(new GLFWHints.OpenGLHints()
                        .withVersion(4, 5))
                .withWindowHints(new GLFWHints.WindowHints()
                        .withVisible(false)
                        .withResizable(false));

        try (GLFWWindow newWindow = new GLFWWindow(hints, "Test Font", 800, 600)) {
            newWindow.setSwapInterval(0);

            this.window.swap(newWindow);
        }
    }

    private void init() throws IOException {
        this.initWindow();
        this.initProgram();
        this.initVertexData();
        this.initModel();
        this.initFont();
        this.projection = MemoryUtil.memCallocFloat(16);
    }

    private void initModel() {
        try (GLVertexArray newModel = new GLVertexArrayInfo()
                .withAttributes(
                        new AttributeDescription()
                                .withBinding(0)
                                .withFormat(GLVertexFormat.VEC2)
                                .withLocation(0)
                                .withOffset(0),
                        new AttributeDescription()
                                .withBinding(0)
                                .withFormat(GLVertexFormat.VEC2)
                                .withLocation(1)
                                .withOffset(2 * Float.BYTES))
                .withBindings(new BindingDescription()
                        .withBinding(0)
                        .withBuffer(this.vtex)
                        .withStride(2 * 2 * Float.BYTES))
                .create()) {

            this.model.swap(newModel);
        }
    }

    private void initVertexData() {
        try (GLBuffer newBuffer = new GLBufferInfo()
                .withSize(6 * 4 * Float.BYTES * 1024)
                .withUsage(GLBufferInfo.Usage.STREAM_DRAW)
                .create()) {

            this.vtex.swap(newBuffer);
        }
    }

    private void initProgram() throws IOException {
        try (InputStream vin = TestFont.class.getResourceAsStream("SimpleQuad.vert");
                InputStream fin = TestFont.class.getResourceAsStream("SimpleQuad.frag")) {

            final String vsrc = Tools.readAll(vin);
            final String fsrc = Tools.readAll(fin);

            try (GLShader vsh = GLShader.newVertexShader(vsrc);
                    GLShader fsh = GLShader.newFragmentShader(fsrc)) {

                try (GLProgram newProgram = new GLProgramInfo()
                        .withShaders(vsh, fsh)
                        .withAttributes(
                                new AttributeStateInfo()
                                        .withIndex(0)
                                        .withName("vPos"),
                                new AttributeStateInfo()
                                        .withIndex(1)
                                        .withName("vTex"))
                        .create()) {

                    this.uFont = newProgram.getUniformLocation("uFont");
                    this.uProjection = newProgram.getUniformLocation("uProjection");
                    this.program.swap(newProgram);

                    System.out.printf("uFont: %d uProj: %d%n", uFont, uProjection);
                }
            }
        }
    }
    
    private static final Set<GLBufferAccess> MAP_FLAGS = EnumSet.of(GLBufferAccess.WRITE, GLBufferAccess.INVALIDATE_BUFFER);    
    private ByteBuffer data;

    private int print(final float x, final float y, final String text) {
        final int start = this.data.position();
        
        this.font.encode(x, y, text).forEach(c -> c.writeVertexTextureData(data));            
        
        final int end = this.data.position();
        

        return text.length() * 6;
    }

    private void render() {
        final Matrix4f proj = new Matrix4f()
                .ortho(0F, 800F, 600F, 0F, 0F, 1F, true);

        proj.get(this.projection);
        
        clearState.apply();
        blendState.apply();
        
        try (GLBuffer.MappedBuffer mapping = this.vtex.map(MAP_FLAGS)) {
            this.data = mapping.getPointer();
            
            
        }        

        float y = font.lineSpacing;
        int totalCharacters = print(16F, y, "Apparently we had reached a great height in the atmosphere, for the sky was a dead")
                + print(16F, y * 2F, "black, and the stars had ceased to twinkle. By the same illusion which lifts the horizon")
                + print(16F, y * 3F, "of the sea to the level of the spectator on a hillside, the sable cloud beneath was dished")
                + print(16F, y * 4F, "out, and the car seemed to float in the middle of an immense dark sphere, whose")
                + print(16F, y * 5F, "upper half was strewn with silver. Looking down into the dark gulf below, I could see a")
                + print(16F, y * 6F, "ruddy light streaming through a rift in the clouds.");
        
        ftex.bind(0);
        program.use();

        GLUniform.uniformMatrix4(this.uProjection, false, this.projection);
        GLUniform.uniform1(this.uFont, 0);

        model.bind();

        GLDraw.drawArrays(GLDraw.Mode.TRIANGLES, 0, totalCharacters);
    }

    private void initFont() throws IOException {
        try (InputStream infont = TestFont.class.getResource("/fonts/roboto/Roboto-Regular.ttf").openStream()) {
            this.font = FontImage.loadASCII(infont, FontInfo.PT16_FONT);
        }

        try (GLTexture newFontTex = GLTextureInfo.wrapImage(font)
                .withFormat(GLInternalFormat.R8)                
                .withSamplerInfo(new GLSamplerInfo()
                        .withMagFilter(GLFilter.NEAREST)
                        .withMinFilter(GLFilter.NEAREST)
                        .withWrapR(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE)
                        .withWrapS(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE)
                        .withWrapT(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE))
                .create()) {

            this.ftex.swap(newFontTex);
        }
    }

    @Test
    public void runTest() throws Exception {
        this.init();

        this.window.show();

        while (!this.window.shouldClose()) {
            this.window.pollEvents();

            this.window.beginFrame();

            new GLViewport(0, 0, 800, 600, 0, 1).apply();
            render();

            this.window.endFrame();
        }
    }
}
