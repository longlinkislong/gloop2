/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.spirv.vm.SpirVM;
import com.longlinkislong.gloop2.spirv.vm.ExecutionCompleteException;
import com.longlinkislong.gloop2.spirv.vm.Reference;
import com.longlinkislong.gloop2.spirv.vm.VariableReference;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestSpirVM {

    @Test
    public void runTest() throws IOException {
        final Path testFile = Paths.get("texturedQuad.vert.spv");

        try (InputStream in = TestSpirVM.class.getResourceAsStream("/com/longlinkislong/gloop2/lwjgl/demo/opengl/textures/texturedQuad.vert.spv")) {
            Files.copy(in, testFile, StandardCopyOption.REPLACE_EXISTING);
        }

        try (FileChannel fc = FileChannel.open(testFile, StandardOpenOption.READ)) {
            final SpirVM vm = new SpirVM(fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size()));

            try {
                while (true) {
                    vm.step();
                }
            } catch (ExecutionCompleteException | IllegalStateException ex) {

            }

            System.out.println(vm.header);            
            
            vm.getSource().ifPresent(System.out::println);
            
            System.out.println("\nAttributes:");
            vm.debugSymbols().forEach(symbol -> {
                final Reference ref = vm.getReference(symbol);

                if (ref instanceof VariableReference) {
                    final VariableReference var = (VariableReference) ref;

                    if (var.storageClass == StorageClass.INPUT) {
                        vm.getDecoration(ref).ifPresent(decoration -> {
                            if (decoration.decoration == Decoration.LOCATION) {
                                System.out.printf("layout(location=%d) in %s %s%n", decoration.literals.getInt(0), var.type, symbol);
                            }
                        });
                    }
                }
            });
        }
    }
}
