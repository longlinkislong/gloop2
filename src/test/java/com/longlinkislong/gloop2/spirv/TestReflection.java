/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.gl.GLVertexArrayInfo;
import com.longlinkislong.gloop2.spirv.vm.Reflection;
import com.longlinkislong.gloop2.spirv.vm.SpirVM;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestReflection {

    @Test
    public void runTest() throws IOException {
        final Path testFile = Paths.get("texturedQuad.vert.spv");

        try (InputStream in = TestReflection.class.getResourceAsStream("/com/longlinkislong/gloop2/lwjgl/demo/opengl/textures/texturedQuad.vert.spv")) {
            Files.copy(in, testFile, StandardCopyOption.REPLACE_EXISTING);
        }

        try (FileChannel fc = FileChannel.open(testFile, StandardOpenOption.READ)) {
            final SpirVM vm = new SpirVM(fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size()));
            final Reflection refl = new Reflection(vm);
            final List<GLVertexArrayInfo.AttributeDescription> inputs = refl.getVertexAttributes();

            for (GLVertexArrayInfo.AttributeDescription input : inputs) {
                System.out.printf("layout (location=%d) in %s%n", input.location, input.format);
            }
        }
    }
}
