/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.memory;

import java.util.Random;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestMemoryManager {

    private static final Random RNG = new Random(0);

    @Test
    public void testConflictingAllocations() {
        System.out.println("---------------------------------------------------");
        System.out.println("-- Conflicting Allocation test --");
        System.out.println("---------------------------------------------------");
        
        final MemoryBlock memBlock = new MemoryBlock(2048L, 4096L, 1024L);
        
        try {
            while(true) {
                final long bufferSize = Math.abs(RNG.nextLong()) % 64;
                final long imageSize = Math.abs(RNG.nextLong()) % 128;
                
                memBlock.allocate(MemoryType.BUFFER, bufferSize, 16);
                memBlock.allocate(MemoryType.IMAGE_OPTIMAL, imageSize, 16);
            } 
        } catch (OutOfMemoryError err) {            
            System.out.println(memBlock.getStats());
            memBlock.sanityCheck();
        }
    }
    
    @Test
    public void testSimpleAllocations() {
        System.out.println("---------------------------------------------------");
        System.out.println("-- Simple Allocation test --");
        System.out.println("---------------------------------------------------");
        
        final MemoryBlock memBlock = new MemoryBlock(256L, 1024L);

        try {
            while(true) {
                final long size = Math.abs(RNG.nextLong()) % 64;                
                
                memBlock.allocate(MemoryType.BUFFER, size, 16L);
            }
        } catch (OutOfMemoryError err) {            
            System.out.println(memBlock.getStats());
            memBlock.sanityCheck();
        }
    }
    
    @Test
    public void testFragmentation() {
        System.out.println("---------------------------------------------------");
        System.out.println("-- Fragmentation Test --");
        System.out.println("---------------------------------------------------");
        
        final MemoryBlock memBlock = new MemoryBlock(128L, 1024L);
        MemoryBlock.SubBlock prev = null;             
        
        try {
            int i = 0;
            while(true) {             
                final long size = i++ * 16 + Math.abs(RNG.nextLong()) % 64;                
                final MemoryBlock.SubBlock alloc = (MemoryBlock.SubBlock) memBlock.allocate(MemoryType.BUFFER, size, 16L);                
                
                if (prev != null && RNG.nextBoolean() && RNG.nextBoolean()) {                    
                    prev.free();
                }
                
                prev = alloc;
            }
        } catch (OutOfMemoryError err) {
            System.out.println(memBlock.getStats());
            memBlock.sanityCheck();
        }
    }
}
