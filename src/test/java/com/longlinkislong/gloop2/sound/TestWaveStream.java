/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.al.ALBuffer;
import com.longlinkislong.gloop2.al.ALFormat;
import com.longlinkislong.gloop2.al.ALSource;
import com.longlinkislong.gloop2.al.ALSourceState;
import java.io.EOFException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;
import org.junit.Test;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public class TestWaveStream {

    private static final int BUFFERS = 3;

    private static void stream(InputStream in) throws Exception {
        final Queue<ALBuffer> buffers = new ArrayDeque<>();
        final ALSource source = new ALSource();

        for (int i = 0; i < BUFFERS; i++) {
            buffers.offer(new ALBuffer());
        }

        ByteBuffer transfer = null;
        
        try (WaveChannel wc = new WaveChannel(in)) {            
            final int sampleSize = wc.getByteRate() / 30; //30 samples per second
            final ALFormat format = wc.getALFormat().orElseThrow(() -> new UnsupportedOperationException("Wav object does not have compatible format!"));

            transfer = MemoryUtil.memAlloc(sampleSize).order(wc.getByteOrder());            

            while (!buffers.isEmpty()) {
                final ALBuffer buffer = buffers.poll();

                transfer.clear();

                if (wc.read(transfer) == -1) {
                    throw new EOFException();
                }

                transfer.flip();
                buffer.setData(format, transfer, wc.getSampleRate());
                source.queueBuffers(buffer);
            }

            source.play();

            while (true) {
                final List<ALBuffer> ready = source.unqueueBuffers();

                for (ALBuffer buffer : ready) {

                    transfer.clear();

                    if (wc.read(transfer) == -1) {
                        throw new EOFException();
                    }

                    transfer.flip();
                    buffer.setData(format, transfer, wc.getSampleRate());
                    source.queueBuffers(buffer);
                }
            }
        } catch (EOFException ex) {
            while (source.getState() == ALSourceState.PLAYING) {
                Thread.sleep(30);
            }
        } finally {
            if (transfer != null) {
                MemoryUtil.memFree(transfer);
            }
        }
    }
    
    @Test
    public void testPCMF32() throws Exception {
        System.out.println("Testing Float32LE");
        try (InputStream in = TestWaveStream.class.getResourceAsStream("atpcmf32le.wav")) {
            stream(in);
        }
    }
    
    @Test
    public void testPCMAlaw() throws Exception {
        System.out.println("Testing alaw");
        try (InputStream in = TestWaveStream.class.getResourceAsStream("atpcmalaw.wav")) {
            stream(in);
        }
    }
    
    @Test
    public void testPCM16() throws Exception {        
        System.out.println("Testing PCM16");
        try (InputStream in = TestWaveStream.class.getResourceAsStream("atpcm16.wav")) {
            stream(in);
        }
    }
}
