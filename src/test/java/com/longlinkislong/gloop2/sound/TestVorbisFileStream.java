/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.al.ALBuffer;
import com.longlinkislong.gloop2.al.ALFormat;
import com.longlinkislong.gloop2.al.ALSource;
import com.longlinkislong.gloop2.al.ALSourceState;
import java.io.EOFException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;
import org.junit.Test;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class TestVorbisFileStream {

    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }

    private static final Path SOUND_DIR = Paths.get("src", "test", "resources", "com", "longlinkislong", "gloop2", "sound");
    private static final int BUFFERS = 3;
    private static final Logger LOGGER = LoggerFactory.getLogger(TestVorbisStream.class);

    @Test
    public void runTest() throws Exception {
        final Queue<ALBuffer> buffers = new ArrayDeque<>();
        final ALSource source = new ALSource();        

        for (int i = 0; i < BUFFERS; i++) {
            buffers.offer(new ALBuffer());
        }

        ByteBuffer transfer = null;
        try (VorbisFileChannel vfc = new VorbisFileChannel(SOUND_DIR.resolve("atmono.ogg"))) {
            final ALFormat format = vfc.getALFormat().get();
            LOGGER.info("Length: {}", vfc.getLength());
            final int bufferSize = vfc.getByteRate() / 20;

            transfer = MemoryUtil.memAlloc(bufferSize);

            while (!buffers.isEmpty()) {
                final ALBuffer buffer = buffers.poll();

                transfer.clear();

                final int read = vfc.read(transfer);

                transfer.flip();
                buffer.setData(format, transfer, vfc.getSampleRate());
                source.queueBuffers(buffer);

                if (read == -1) {
                    break;
                }
            }

            source.play();

            while (true) {
                final List<ALBuffer> ready = source.unqueueBuffers();

                if (ready.isEmpty()) {
                    Thread.sleep(30);
                }

                for (ALBuffer buffer : ready) {
                    transfer.clear();

                    final int read = vfc.read(transfer);

                    transfer.flip();
                    buffer.setData(format, transfer, vfc.getSampleRate());
                    source.queueBuffers(buffer);

                    if (read == -1) {
                        throw new EOFException();
                    }
                }
            }
        } catch (EOFException ex) {
            while (source.getState() == ALSourceState.PLAYING) {
                Thread.sleep(30);
            }
        } finally {
            MemoryUtil.memFree(transfer);
        }
    }
}
