/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.al.ALBuffer;
import com.longlinkislong.gloop2.al.ALFormat;
import com.longlinkislong.gloop2.al.ALSource;
import com.longlinkislong.gloop2.al.ALSourceState;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;
import org.junit.Test;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class TestVorbisStream {

    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }

    private static final int BUFFERS = 3;
    private static final Logger LOGGER = LoggerFactory.getLogger(TestVorbisStream.class);

    private static void stream(SoundChannel ch) throws Exception {
        final Queue<ALBuffer> buffers = new ArrayDeque<>();
        final ALSource source = new ALSource();

        for (int i = 0; i < BUFFERS; i++) {
            buffers.offer(new ALBuffer());
        }

        ByteBuffer transfer = null;
        try (SoundChannel sch = ch) {
            final int bufferSize = 128 * 1024;
            final ALFormat format = sch.getALFormat().orElseThrow(() -> new UnsupportedOperationException("Ogg object does not have compatible format!"));

            transfer = MemoryUtil.memAlloc(bufferSize).order(sch.getByteOrder());

            while (!buffers.isEmpty()) {
                final ALBuffer buffer = buffers.poll();

                transfer.clear();

                final int read = sch.read(transfer);

                transfer.flip();
                buffer.setData(format, transfer, sch.getSampleRate());
                source.queueBuffers(buffer);

                if (read == -1) {
                    source.play();
                    throw new EOFException();
                }
            }

            source.play();

            while (true) {
                final List<ALBuffer> ready = source.unqueueBuffers();

                if (ready.isEmpty()) {
                    Thread.sleep(30);
                }

                for (ALBuffer buffer : ready) {
                    transfer.clear();

                    final int read = sch.read(transfer);

                    transfer.flip();
                    buffer.setData(format, transfer, sch.getSampleRate());
                    source.queueBuffers(buffer);

                    if (read == -1) {
                        throw new EOFException();
                    }
                }
            }
        } catch (EOFException ex) {
            while (source.getState() == ALSourceState.PLAYING) {
                Thread.sleep(30);
            }
        } finally {
            if (transfer != null) {
                MemoryUtil.memFree(transfer);
            }
        }
    }

    // converts the stream to PCM16 before playback
    @Test
    public void testStreamPCM16() throws Exception {
        try (InputStream in = TestVorbisStream.class.getResourceAsStream("atmono.ogg")) {
            try (VorbisChannel vc = new VorbisChannel(in)) {
                stream(new FormatConversionChannel(vc, ALFormat.MONO16));
            }
        }
    }

    @Test
    public void testStreamPCMF32() throws Exception {

        try (InputStream in = TestVorbisStream.class.getResourceAsStream("atmono.ogg")) {
            try (VorbisChannel vc = new VorbisChannel(in)) {
                stream(vc);
            }
        }
    }

    @Test
    public void testInit() throws IOException {
        try (InputStream in = TestVorbisStream.class.getResourceAsStream("atmono.ogg")) {
            try (VorbisChannel vc = new VorbisChannel(Channels.newChannel(in))) {
                LOGGER.info("Sample Rate: {}hz", vc.getSampleRate());
                LOGGER.info("Channels: {}", vc.getChannels());

                final ByteBuffer transfer = ByteBuffer.allocate(4096);

                int nread = vc.read(transfer);

                LOGGER.info("Read {} bytes", nread);
            }
        }
    }
}
