/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.image.Image;
import com.longlinkislong.gloop2.image.ImageIO;
import com.longlinkislong.gloop2.vk.VKManagedBuffer.MappedBuffer;
import com.longlinkislong.gloop2.vk.VKPipelineVertexInputStateInfo.AttributeDescription;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class SimpleTexturedQuad {

    static {
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
        System.setProperty("com.longlinkislong.gloop2.vk.VKContext.async_present", "false");
    }

    private static VKShaderModule loadShader(final VKContext ctx, final String path) throws IOException {
        try (InputStream in = SimpleTexturedQuad.class.getResourceAsStream(path)) {
            return Tools.withStreamData(in, data -> new VKShaderModule(ctx, data));
        }
    }

    private static VKManagedBuffer createVertexBuffer(final VKContext ctx) {
        final int bytesNeeded = (2 * 6) * Float.BYTES + (2 * 6) * Float.BYTES;

        final VKManagedBuffer out;
        try (VKManagedBuffer staging = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                .withSize(bytesNeeded)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE))) {

            try (MappedBuffer map = staging.map()) {
                map.write(
                        // x, y, u, v
                        -1.0F, 1.0F, 0.0F, 1.0F,
                        1.0F, 1.0F, 1.0F, 1.0F,
                        1.0F, -1.0F, 1.0F, 0.0F,
                        1.0F, -1.0F, 1.0F, 0.0F,
                        -1.0F, -1.0F, 0.0F, 0.0F,
                        -1.0F, 1.0F, 0.0F, 1.0F);
            }

            out = new VKBufferInfo()
                    .withUsage(VKBufferInfo.Usage.VERTEX, VKBufferInfo.Usage.TRANSFER_DST)
                    .withSize(bytesNeeded)
                    .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));

            final VKCommandBuffer cmdbuf = ctx.primaryCommandPool.newPrimaryCommandBuffer();

            cmdbuf.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);
            cmdbuf.copyBuffer(staging, out, new VKBufferCopy().withSize(bytesNeeded));
            cmdbuf.end();

            ctx.mainQueue.submit(VKQueue.SubmitInfo.wrap(cmdbuf));
            ctx.mainQueue.waitIdle();
        }

        return out;
    }

    private static VKImageView createTexture(final VKContext ctx, final String path) throws IOException {
        try (InputStream in = com.longlinkislong.gloop2.lwjgl.demo.opengl.textures.SimpleTexturedQuad.class.getResourceAsStream(path)) {
            try (Image img = ImageIO.read(in, 4)) {
                final ByteBuffer data = img.getData();

                try (VKManagedBuffer stagingBuffer = new VKBufferInfo()
                        .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                        .withSize(data.remaining())
                        .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT))) {

                    try (MappedBuffer map = stagingBuffer.map()) {
                        map.write(data);
                    }

                    final VKImage out = new VKImageInfo()
                            .withImageType(VKImageType.IMAGE_2D)
                            .withExtent(img.getWidth(), img.getHeight(), 1)
                            .withMipLevels(1)
                            .withArrayLayers(1)
                            .withFormat(VKFormat.R8G8B8A8_UNORM)
                            .withTiling(VKImageTiling.OPTIMAL)
                            .withInitialLayout(VKImageLayout.UNDEFINED)
                            .withUsage(VKImageUsage.TRANSFER_DST, VKImageUsage.SAMPLED)
                            .withSharingMode(VKSharingMode.EXCLUSIVE)
                            .withSamples(1)
                            .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));

                    final VKCommandBuffer cmdbuf = ctx.primaryCommandPool.newPrimaryCommandBuffer();

                    cmdbuf.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);

                    cmdbuf.stageImageLayoutTransition(
                            out, out.getFullRange(),
                            EnumSet.of(VKPipelineStage.TOP_OF_PIPE), VKImageLayout.UNDEFINED,
                            EnumSet.of(VKPipelineStage.TRANSFER), VKImageLayout.TRANSFER_DST_OPTIMAL);

                    cmdbuf.copyBufferToImage(
                            stagingBuffer,
                            out,
                            VKImageLayout.TRANSFER_DST_OPTIMAL,
                            new VKBufferImageCopy()
                                    .withImageSubresource(out.getBaseLayer(0))
                                    .withImageExtent(img.getWidth(), img.getHeight(), 1));

                    cmdbuf.stageImageLayoutTransition(
                            out, out.getFullRange(),
                            EnumSet.of(VKPipelineStage.TRANSFER), VKImageLayout.TRANSFER_DST_OPTIMAL,
                            EnumSet.of(VKPipelineStage.FRAGMENT_SHADER), VKImageLayout.SHADER_READ_ONLY_OPTIMAL);

                    cmdbuf.end();

                    ctx.mainQueue.submit(VKQueue.SubmitInfo.wrap(cmdbuf));
                    ctx.mainQueue.waitIdle();

                    return VKImageView.fullView(ctx, out);
                }
            }
        }
    }

    private static VKRenderPass createRenderPass(final VKContext ctx) {
        return VKRenderPassInfo.infer(ctx.getPresentAttachments()).create(ctx);
    }

    private static VKDescriptorSet createDescriptorSet(
            final VKContext ctx,
            final VKDescriptorPool pool, final VKDescriptorSetLayout layout,
            final VKImageView view, final VKSampler sampler) {

        final VKDescriptorSet out = pool.allocate(layout);

        ctx.device.updateDescriptorSets(
                new VKWriteDescriptorSet()
                        .withDstSet(out)
                        .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                        .withImageInfo(new VKDescriptorImageInfo()
                                .withImageView(view)
                                .withSampler(sampler)
                                .withImageLayout(VKImageLayout.SHADER_READ_ONLY_OPTIMAL)),
                null);

        return out;
    }

    private static VKDescriptorSetLayout createDescriptorSetLayout(final VKContext ctx) {
        return new VKDescriptorSetLayoutInfo()
                .withBindings(new VKDescriptorSetLayoutInfo.Binding()
                        .withBinding(0)
                        .withDescriptorCount(1)
                        .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                        .withStageFlags(VKShaderStage.FRAGMENT))
                .create(ctx);
    }

    private static VKDescriptorPool createDescriptorPool(final VKContext ctx) {
        return new VKDescriptorPoolInfo()
                .withPoolSizes(new VKDescriptorPoolSize()
                        .withType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                        .withDescriptorCount(1))
                .withMaxSets(1)
                .create(ctx);
    }

    private static VKPipelineLayout createPipelineLayout(final VKContext ctx, final VKDescriptorSetLayout setLayout) {
        return new VKPipelineLayoutInfo()
                .withSetLayouts(setLayout)
                .create(ctx);
    }

    private static VKGraphicsPipeline createPipeline(
            final VKContext ctx,
            final VKRenderPass renderPass,
            final VKPipelineLayout pipelineLayout,
            final VKShaderModule vsh, final VKShaderModule fsh) {

        VKGraphicsPipelineInfo pipelineInfo = VKGraphicsPipelineInfo.inferDynamicViewports(null, 1);

        pipelineInfo = VKGraphicsPipelineInfo.inferFromRenderPass(pipelineInfo, renderPass);

        return pipelineInfo
                .withLayout(pipelineLayout)
                .withVertexInputState(new VKPipelineVertexInputStateInfo()
                        .withBindingDescriptions(new VKPipelineVertexInputStateInfo.BindingDescription()
                                .withBinding(0)
                                .withStride((2 + 2) * Float.BYTES)
                                .withInputRate(VKVertexInputRate.VERTEX))
                        .withAttributeDescriptions(
                                new AttributeDescription()
                                        .withBinding(0)
                                        .withLocation(0)
                                        .withFormat(VKFormat.VEC2)
                                        .withOffset(0),
                                new AttributeDescription()
                                        .withBinding(0)
                                        .withLocation(1)
                                        .withFormat(VKFormat.VEC2)
                                        .withOffset(2 * Float.BYTES)))
                .withAssemblyState(VKPipelineInputAssemblyStateInfo.DEFAULT_TRIANGLE_LIST)
                .withRasterizationState(VKPipelineRasterizationStateInfo.DEFAULT_OPENGL)
                .withDepthStencilState(VKPipelineDepthStencilStateInfo.DEFAULT_OPENGL_DEPTH_TEST)
                .withStages(
                        VKPipelineShaderStageInfo.vertexShader(vsh),
                        VKPipelineShaderStageInfo.fragmentShader(fsh))
                .create(ctx);
    }

    private static VKSampler createSampler(final VKContext ctx) {
        return new VKSamplerInfo()
                .withAddressModeU(VKSamplerAddressMode.CLAMP_TO_EDGE)
                .withAddressModeV(VKSamplerAddressMode.CLAMP_TO_EDGE)
                .withMagFilter(VKFilter.LINEAR)
                .withMinFilter(VKFilter.LINEAR)
                .withAnisotropyEnable(false)
                .withMaxAnisotropy(1.0F)
                .create(ctx);
    }

    @Test
    public void runTest() {
        VulkanTestFramework test = new VulkanTestFramework();

        //test.enableLayers = true;
        test.name = "Simple Textured Quad";
        test.context = new VulkanTestFramework.Context() {
            final String VERTEX_SHADER_URL = "/com/longlinkislong/gloop2/lwjgl/demo/vulkan/texturedQuad.vert.spv";
            final String FRAGMENT_SHADER_URL = "/com/longlinkislong/gloop2/lwjgl/demo/vulkan/texturedQuad.frag.spv";
            final String TEXTURE_URL = "/com/longlinkislong/gloop2/lwjgl/demo/opengl/textures/environment.png";

            VKRenderPass renderPass;
            VKShaderModule vsh, fsh;
            VKPipelineLayout pipelineLayout;
            VKGraphicsPipeline pipeline;
            VKManagedBuffer vertexBuffer;
            VKDescriptorPool descriptorPool;
            VKDescriptorSetLayout descriptorSetLayout;
            VKDescriptorSet descriptorSet;
            VKSampler sampler;
            VKImageView texture;

            VKScissor renderArea = new VKScissor(0, 0, 800, 600);
            VKViewport viewport = new VKViewport(0, 0, 800, 600, 0, 1);

            @Override
            public void init(VKContext ctx) throws Exception {
                renderPass = createRenderPass(ctx);
                vsh = loadShader(ctx, VERTEX_SHADER_URL);
                fsh = loadShader(ctx, FRAGMENT_SHADER_URL);
                descriptorSetLayout = createDescriptorSetLayout(ctx);
                pipelineLayout = createPipelineLayout(ctx, descriptorSetLayout);
                pipeline = createPipeline(ctx, renderPass, pipelineLayout, vsh, fsh);
                vertexBuffer = createVertexBuffer(ctx);
                texture = createTexture(ctx, TEXTURE_URL);
                sampler = createSampler(ctx);
                descriptorPool = createDescriptorPool(ctx);
                descriptorSet = createDescriptorSet(ctx, descriptorPool, descriptorSetLayout, texture, sampler);

            }

            private final Set<VKCommandBuffer> drawCommands = new HashSet<>();

            @Override
            public void onFrame(VKContext ctx) {
                final VKCommandBuffer cmdbuf = ctx.getPrimaryCommandBuffer();

                if (drawCommands.add(cmdbuf)) {
                    final VKFramebuffer fb = ctx.getFramebuffer();
                    final ByteBuffer[] pClearValues = {
                        Tools.clearColor(100F / 255F, 149F / 255F, 237F / 255F, 1F),
                        Tools.clearDepthStencil(1.0F, 0)};

                    cmdbuf.begin(new VKCommandBufferBeginInfo());
                    cmdbuf.beginRenderPass(fb, renderPass, VKSubpassContents.INLINE, renderArea, pClearValues);

                    cmdbuf.setViewport(viewport);
                    cmdbuf.setScissor(renderArea);
                    cmdbuf.bindPipeline(pipeline);
                    cmdbuf.bindDescriptorSet(VKPipelineBindPoint.GRAPHICS, pipelineLayout, 0, descriptorSet);
                    cmdbuf.bindVertexBuffer(0, vertexBuffer, 0L);
                    cmdbuf.draw(6, 1, 0, 0);

                    cmdbuf.endRenderPass();
                    cmdbuf.end();
                }
            }

            @Override
            public void cleanup() throws Exception {                
                descriptorPool.close();
                sampler.close();
                texture.close();
                vertexBuffer.close();
                pipeline.close();
                pipelineLayout.close();
                descriptorSetLayout.close();
                fsh.close();
                vsh.close();
                renderPass.close();
            }
        };
        test.start();
    }
}
