/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class InitDevice {
    static {
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }
    
    @Test
    public void test() {
        try (VKInstance instance = new VKInstanceCreateInfo()
                .withApplicationName("Init Device Demo")
                .withVersion(1, 0, 2)
                .create()) {
            
            final VKPhysicalDevice selectedPhysicalDevice = instance.physicalDevices()
                    .filter(VKPhysicalDevice::isGPU)
                    .findFirst()
                    .get();
            
            final VKQueueFamily selectedQueueFamily = selectedPhysicalDevice.queueFamilies()
                    .filter(VKQueueFamily::canDraw)
                    .findFirst()
                    .get();
            
            try (VKDevice device = new VKDeviceCreateInfo()
                    .withQueueCreateInfos(new VKDeviceCreateInfo.DeviceQueueCreateInfo()
                    .withQueueFamily(selectedQueueFamily))
                    .create()) {
                
                Assert.assertNotNull(device);
            }
        }
    }
}
