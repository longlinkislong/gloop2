/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.glfw.GLFWHints;
import com.longlinkislong.gloop2.glfw.GLFWWindow;
import com.longlinkislong.gloop2.glfw.GLFWWindowCreateInfo;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class InitSwapchain {

    static {
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }

    @Test
    public void test() throws Exception{
        try (VKInstance instance = new VKInstanceCreateInfo()
                .withApplicationName("Init Device Demo")
                .withVersion(1, 0, 2)
                .create()) {

            final VKPhysicalDevice selectedPhysicalDevice = instance.physicalDevices()
                    .filter(VKPhysicalDevice::isGPU)
                    .findFirst()
                    .get();

            final VKQueueFamily selectedQueueFamily = selectedPhysicalDevice.queueFamilies()
                    .filter(VKQueueFamily::canDraw)
                    .findFirst()
                    .get();

            final GLFWWindowCreateInfo windowInfo = new GLFWWindowCreateInfo()
                    .withWidth(800)
                    .withHeight(600)
                    .withTitle("Init Swapchain Demo")
                    .withHints(new GLFWHints()
                            .withContextHints(
                                    new GLFWHints.VulkanHints(
                                             new VKDeviceCreateInfo()
                                                    .withQueueCreateInfos(
                                                            new VKDeviceCreateInfo.DeviceQueueCreateInfo()
                                                                    .withQueueFamily(selectedQueueFamily)))));

            try (GLFWWindow window = windowInfo.create()) {
                window.show();
                                
                while (!window.shouldClose()) {
                    window.pollEvents();
                    
                    // this call is only needed for Vulkan
                    // it signals that the backbuffer needs to be acquired NOW
                    window.beginFrame();                                                            
                    window.endFrame();                                           
                }
            }
        }
    }
}
