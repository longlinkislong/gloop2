/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.vk.VKManagedBuffer.MappedBuffer;
import com.longlinkislong.gloop2.vk.VKPipelineVertexInputStateInfo.AttributeDescription;
import com.longlinkislong.gloop2.vk.VKPipelineVertexInputStateInfo.BindingDescription;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.EnumSet;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class ColoredTriangleDemo {

    static {
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }

    private static VKRenderPass createRenderPass(final VKContext ctx) {
        return VKRenderPassInfo.infer(ctx.getPresentAttachments()).create(ctx);
    }

    private static VKShaderModule loadShader(final VKContext ctx, final String path) throws IOException {
        try (InputStream shin = ColoredTriangleDemo.class.getResourceAsStream(path)) {
            return Tools.withStreamData(shin, data -> new VKShaderModule(ctx, data));
        }
    }

    private static VKPipelineLayout createPipelineLayout(final VKContext ctx) {
        return new VKPipelineLayout(ctx);
    }

    private static VKManagedBuffer createVertexBuffer(final VKContext ctx) {
        final VKManagedBuffer out = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.VERTEX)
                .withSize(3 * (2 + 3) * Float.BYTES)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE));

        try (MappedBuffer mapping = out.map()) {
            mapping.write(
                    -0.5F, -0.5F, 1.0F, 0.0F, 0.0F,
                    0.5F, -0.5F, 0.0F, 1.0F, 0.0F,
                    0.0F, 0.5F, 0.0F, 0.0F, 1.0F);
        }

        return out;
    }

    private static VKGraphicsPipeline createPipeline(
            final VKContext ctx,
            final VKRenderPass renderPass,
            final VKPipelineLayout layout,
            final VKShaderModule vsh, final VKShaderModule fsh) {

        VKGraphicsPipelineInfo pipelineInfo = VKGraphicsPipelineInfo.inferDynamicViewports(null, 1);

        pipelineInfo = VKGraphicsPipelineInfo.inferFromRenderPass(pipelineInfo, renderPass);

        return pipelineInfo
                .withLayout(layout)
                .withVertexInputState(new VKPipelineVertexInputStateInfo()
                        .withBindingDescriptions(new BindingDescription()
                                .withBinding(0)
                                .withStride((2 + 3) * 4)
                                .withInputRate(VKVertexInputRate.VERTEX))
                        .withAttributeDescriptions(
                                new AttributeDescription()
                                        .withBinding(0)
                                        .withLocation(0)
                                        .withFormat(VKFormat.VEC2)
                                        .withOffset(0),
                                new AttributeDescription()
                                        .withBinding(0)
                                        .withLocation(1)
                                        .withFormat(VKFormat.VEC3)
                                        .withOffset(2 * 4)))
                .withAssemblyState(VKPipelineInputAssemblyStateInfo.DEFAULT_TRIANGLE_LIST)
                .withRasterizationState(VKPipelineRasterizationStateInfo.DEFAULT_OPENGL)
                .withStages(
                        VKPipelineShaderStageInfo.vertexShader(vsh),
                        VKPipelineShaderStageInfo.fragmentShader(fsh))
                .create(ctx);
    }

    @Test
    public void test() {
        final VulkanTestFramework test = new VulkanTestFramework();

        test.name = "Colored Triangle Demo";
        test.context = new VulkanTestFramework.Context() {
            final String VERTEX_SHADER_URL = "/com/longlinkislong/gloop2/lwjgl/demo/vulkan/coloredTriangle.vert.spv";
            final String FRAGMENT_SHADER_URL = "/com/longlinkislong/gloop2/lwjgl/demo/vulkan/coloredTriangle.frag.spv";

            VKRenderPass renderPass;
            VKShaderModule vsh, fsh;
            VKPipelineLayout pipelineLayout;
            VKGraphicsPipeline pipeline;
            VKManagedBuffer vertexBuffer;
            
            VKScissor renderArea = new VKScissor(0, 0, 800, 600);
            VKViewport viewport = new VKViewport(0, 0, 800, 600, 0, 1);

            @Override
            public void init(VKContext ctx) throws Exception {
                renderPass = createRenderPass(ctx);
                vsh = loadShader(ctx, VERTEX_SHADER_URL);
                fsh = loadShader(ctx, FRAGMENT_SHADER_URL);
                pipelineLayout = createPipelineLayout(ctx);
                pipeline = createPipeline(ctx, renderPass, pipelineLayout, vsh, fsh);
                vertexBuffer = createVertexBuffer(ctx);
            }

            @Override
            public void onFrame(VKContext ctx) {
                final ByteBuffer[] pClearValues = {Tools.clearColor(100F / 255F, 149F / 255F, 237F / 255F, 1F)};
                final VKCommandBuffer cmdbuf = ctx.getPrimaryCommandBuffer();
                final VKFramebuffer fb = ctx.getFramebuffer();

                cmdbuf.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);
                cmdbuf.beginRenderPass(fb, this.renderPass, VKSubpassContents.INLINE, renderArea, pClearValues);
                cmdbuf.setViewport(viewport);
                cmdbuf.setScissor(renderArea);
                cmdbuf.bindPipeline(pipeline);
                cmdbuf.bindVertexBuffer(0, vertexBuffer, 0L);
                cmdbuf.draw(3, 1, 0, 0);
                cmdbuf.endRenderPass();
                cmdbuf.end();
            }

            @Override
            public void cleanup() {
                vertexBuffer.close();
                pipeline.close();
                pipelineLayout.close();
                vsh.close();
                fsh.close();
                renderPass.close();
            }
        };
        test.start();
    }
}
