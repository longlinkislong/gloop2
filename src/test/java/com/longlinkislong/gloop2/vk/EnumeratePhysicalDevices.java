/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class EnumeratePhysicalDevices {    
    static {
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }
    
    @Test
    public void test() {
        try (VKInstance instance = new VKInstanceCreateInfo()
                .withApplicationName("Enumerate Physical Devices demo")
                .withVersion(1, 0, 2)
                .create()) {
            
            instance.physicalDevices()
                    .forEach(System.out::println);
        }
    }
}
