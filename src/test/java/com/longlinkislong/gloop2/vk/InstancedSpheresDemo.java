/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.vk.VKManagedBuffer.MappedBuffer;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.EnumSet;
import java.util.Random;
import org.joml.Matrix4f;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class InstancedSpheresDemo {
    
    static {
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
        System.setProperty("com.longlinkislong.gloop2.vk.VKContext.vsync", "false");
    }
    
    private static final int SPHERE_TESSELATION = 40;
    private static final int SPHERE_COUNT = 256;
    private static final double SPHERE_SPACING = 10.0;
    private static final Matrix4f[] SPHERES;
    
    static {
        SPHERES = new Matrix4f[SPHERE_COUNT];
        
        final Random rng = new Random();
        
        for (int i = 0; i < SPHERES.length; i++) {
            double theta = 2.0 * Math.PI * rng.nextDouble() - Math.PI;
            double z = (2.0 * rng.nextDouble() - 1.0);
            double x = Math.sqrt(1.0 - z * z) * Math.cos(theta) * SPHERE_SPACING;
            double y = Math.sqrt(1.0 - z * z) * Math.sin(theta) * SPHERE_SPACING;
            
            SPHERES[i] = new Matrix4f()
                    .translate((float) x, (float) y, (float) (z * SPHERE_SPACING))
                    .scale(0.5f);
        }
    }
    
    private static VKRenderPass createRenderPass(final VKContext ctx) {
        return VKRenderPassInfo.infer(ctx.getPresentAttachments()).create(ctx);
    }
    
    private static VKShaderModule loadShader(final VKContext ctx, final String path) throws IOException {
        try (InputStream in = InstancedSpheresDemo.class.getResourceAsStream(path)) {
            return Tools.withStreamData(in, data -> new VKShaderModule(ctx, data));
        }
    }
    
    private static VKManagedBuffer createVertexBuffer(final VKContext ctx) {
        final int bytesNeeded = SPHERE_TESSELATION * SPHERE_TESSELATION * 2 * 3 * Float.BYTES;
        
        final VKManagedBuffer vertexBuffer = new VKBufferInfo()
                .withSize(bytesNeeded)
                .withUsage(VKBufferInfo.Usage.VERTEX)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE));
        
        try (MappedBuffer map = vertexBuffer.map()) {
            final FloatBuffer fb = map.getPointer().asFloatBuffer();
            
            for (int i = 0; i < SPHERE_TESSELATION; i++) {
                double lat0 = Math.PI * (-0.5 + i / ((double) SPHERE_TESSELATION));
                double y0 = Math.sin(lat0);
                double zr0 = Math.cos(lat0);
                double lat1 = Math.PI * (-0.5 + (i + 1.0) / ((double) SPHERE_TESSELATION));
                double y1 = Math.sin(lat1);
                double zr1 = Math.cos(lat1);
                
                for (int j = 0; j < SPHERE_TESSELATION; j++) {
                    double lng = 2.0 * Math.PI * j / (SPHERE_TESSELATION - 1.0);
                    double x = Math.sin(lng);
                    double z = Math.cos(lng);
                    
                    fb.put((float) (x * zr1)).put((float) y1).put((float) (z * zr1));
                    fb.put((float) (x * zr0)).put((float) y0).put((float) (z * zr0));
                }
            }
        }
        
        return vertexBuffer;
    }
    
    private static VKDescriptorSetLayout createDescriptorSetLayout(final VKContext ctx) {
        return new VKDescriptorSetLayoutInfo()
                .withBindings(new VKDescriptorSetLayoutInfo.Binding()
                        .withBinding(0)
                        .withDescriptorType(VKDescriptorType.UNIFORM_BUFFER)
                        .withDescriptorCount(1)
                        .withStageFlags(VKShaderStage.VERTEX))
                .create(ctx);
    }
    
    private static VKDescriptorSet createDescriptorSet(final VKContext ctx, final VKDescriptorPool pool, final VKDescriptorSetLayout layout, final VKBuffer buffer) {
        final VKDescriptorSet out = pool.allocate(layout);
        
        ctx.device.updateDescriptorSets(
                new VKWriteDescriptorSet()
                        .withDstSet(out)
                        .withDescriptorType(VKDescriptorType.UNIFORM_BUFFER)
                        .withBufferInfo(new VKDescriptorBufferInfo()
                                .withBuffer(buffer)
                                .withOffset(0L)
                                .withRange(16 * Float.BYTES + 16 * Float.BYTES * SPHERE_COUNT))
                        .withDstBinding(0),
                null);
        
        return out;
    }
    
    private static void createUniformBuffer(final VKContext ctx, final VKManagedBuffer uniformBuffer, final VKManagedBuffer uniformStagingBuffer) {
        try (VKManagedBuffer newUniformBuffer = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.UNIFORM, VKBufferInfo.Usage.TRANSFER_DST)
                .withSize(16 * Float.BYTES + 16 * Float.BYTES * SPHERE_COUNT)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL))) {
            
            uniformBuffer.swap(newUniformBuffer);
        }
        
        try (VKManagedBuffer newUniformStagingBuffer = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                .withSize(16 * Float.BYTES + 16 * Float.BYTES * SPHERE_COUNT)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_CACHED))) {
            
            uniformStagingBuffer.swap(newUniformStagingBuffer);
        }
    }
    
    private static VKDescriptorPool createDescriptorPool(final VKContext ctx) {
        return new VKDescriptorPoolInfo()
                .withPoolSizes(new VKDescriptorPoolSize()
                        .withType(VKDescriptorType.UNIFORM_BUFFER)
                        .withDescriptorCount(1))
                .withMaxSets(1)
                .create(ctx);
    }
    
    private static VKPipelineLayout createPipelineLayout(final VKContext ctx, final VKDescriptorSetLayout setLayout) {
        return new VKPipelineLayoutInfo()
                .withSetLayouts(setLayout)
                .create(ctx);
    }
    
    private static VKGraphicsPipeline createPipeline(
            final VKContext ctx,
            final VKRenderPass renderPass,
            final VKPipelineLayout layout,
            final VKShaderModule vsh, final VKShaderModule fsh) {
        
        VKGraphicsPipelineInfo pipelineInfo = VKGraphicsPipelineInfo.inferDynamicViewports(null, 1);
        
        pipelineInfo = VKGraphicsPipelineInfo.inferFromRenderPass(pipelineInfo, renderPass);
        
        return pipelineInfo
                .withLayout(layout)
                .withVertexInputState(new VKPipelineVertexInputStateInfo()
                        .withBindingDescriptions(new VKPipelineVertexInputStateInfo.BindingDescription()
                                .withBinding(0)
                                .withStride(3 * 4)
                                .withInputRate(VKVertexInputRate.VERTEX))
                        .withAttributeDescriptions(new VKPipelineVertexInputStateInfo.AttributeDescription()
                                .withBinding(0)
                                .withLocation(0)
                                .withFormat(VKFormat.VEC3)
                                .withOffset(0)))
                .withAssemblyState(VKPipelineInputAssemblyStateInfo.DEFAULT_TRIANGLE_STRIP)
                .withRasterizationState(VKPipelineRasterizationStateInfo.DEFAULT_OPENGL)
                .withDepthStencilState(VKPipelineDepthStencilStateInfo.DEFAULT_OPENGL_DEPTH_TEST)
                .withStages(
                        VKPipelineShaderStageInfo.vertexShader(vsh),
                        VKPipelineShaderStageInfo.fragmentShader(fsh))
                .create(ctx);
    }
    
    @Test
    public void runTest() {
        VulkanTestFramework test = new VulkanTestFramework();
        
        test.name = "Instanced Spheres Demo";
        test.context = new VulkanTestFramework.Context() {
            final String VERTEX_SHADER_URL = "/com/longlinkislong/gloop2/lwjgl/demo/vulkan/instancedSpheres.vert.spv";
            final String FRAGMENT_SHADER_URL = "/com/longlinkislong/gloop2/lwjgl/demo/vulkan/instancedSpheres.frag.spv";
            
            VKRenderPass renderPass;
            VKShaderModule vsh, fsh;
            VKPipelineLayout pipelineLayout;
            VKGraphicsPipeline pipeline;
            VKManagedBuffer uniformStaging = new VKManagedBuffer();
            VKManagedBuffer vertexBuffer;
            VKManagedBuffer uniformBuffer = new VKManagedBuffer();
            VKDescriptorPool descriptorPool;
            VKDescriptorSetLayout descriptorSetLayout;
            VKDescriptorSet descriptorSet;
            
            VKFrameLocal<VKCommandBuffer> renderCommand;
            
            VKScissor renderArea = new VKScissor(0, 0, 800, 600);
            VKViewport viewport = new VKViewport(0, 0, 800, 600, 0, 1);
            
            float time;
            long lastTime = System.nanoTime();
            
            @Override
            public void init(VKContext ctx) throws Exception {
                renderPass = createRenderPass(ctx);
                vsh = loadShader(ctx, VERTEX_SHADER_URL);
                fsh = loadShader(ctx, FRAGMENT_SHADER_URL);
                descriptorSetLayout = createDescriptorSetLayout(ctx);
                pipelineLayout = createPipelineLayout(ctx, descriptorSetLayout);
                pipeline = createPipeline(ctx, renderPass, pipelineLayout, vsh, fsh);
                vertexBuffer = createVertexBuffer(ctx);
                createUniformBuffer(ctx, uniformBuffer, uniformStaging);
                descriptorPool = createDescriptorPool(ctx);
                descriptorSet = createDescriptorSet(ctx, descriptorPool, descriptorSetLayout, uniformBuffer);
                
                renderCommand = new VKFrameLocal<VKCommandBuffer>(ctx) {
                    @Override
                    protected VKCommandBuffer initialValue(final VKContext ctx) {
                        final VKFramebuffer fb = ctx.getFramebuffer();
                        final VKCommandBuffer out = ctx.primaryCommandPool.newSecondaryCommandBuffer();
                        
                        out.begin(new VKCommandBufferBeginInfo()
                                .withFlags(VKCommandBufferBeginInfo.Usage.RENDER_PASS_CONTINUE)
                                .withInheritanceInfo(new VKCommandBufferBeginInfo.InheritanceInfo()
                                        .withRenderPass(renderPass)
                                        .withFramebuffer(fb)
                                        .withSubpass(0)));
                        
                        out.setViewport(viewport);
                        out.setScissor(renderArea);
                        out.bindPipeline(pipeline);
                        out.bindDescriptorSet(VKPipelineBindPoint.GRAPHICS, pipelineLayout, 0, descriptorSet);
                        out.bindVertexBuffer(0, vertexBuffer, 0L);
                        
                        out.draw(SPHERE_TESSELATION * SPHERE_TESSELATION * 2, SPHERES.length, 0, 0);
                        
                        out.end();
                        
                        return out;
                    }
                };
            }
            
            private boolean runOnce = true;
            
            private int updateUBO(final VKContext ctx, float angle) {
                final Matrix4f viewProj = new Matrix4f()
                        .identity()
                        .scale(1, -1, 1)
                        .perspective((float) Math.toRadians(45.0), (float) ctx.getAspectRatio(), 0.1F, 50.0F, true)
                        .translate(0, 0, -30)
                        .rotateY(angle);
                
                if (runOnce) {
                    try (MappedBuffer mapping = this.uniformStaging.map()) {
                        final ByteBuffer matrixBuffer = mapping.getPointer();
                        
                        viewProj.get(matrixBuffer);
                        
                        for (int i = 0; i < SPHERE_COUNT; i++) {
                            SPHERES[i].get(16 * 4 * (i + 1), matrixBuffer);
                        }
                        
                        runOnce = false;
                        
                        return matrixBuffer.capacity();
                    }
                } else {
                    try (MappedBuffer mapping = this.uniformStaging.map(0L, 16 * Float.BYTES)) {
                        final ByteBuffer matrixBuffer = mapping.getPointer();
                        
                        viewProj.get(matrixBuffer);
                    }
                    
                    return 16 * Float.BYTES;
                }
            }
            
            @Override
            public void onFrame(VKContext ctx) {
                final ByteBuffer[] pClearValues = {
                    Tools.clearColor(100F / 255F, 149F / 255F, 237F / 255F, 1F),
                    Tools.clearDepthStencil(1.0F, 0)};
                
                final VKCommandBuffer cmdbuf = ctx.getPrimaryCommandBuffer();
                final VKFramebuffer fb = ctx.getFramebuffer();
                
                long now = System.nanoTime();
                time += (now - lastTime) / 1E9F;
                lastTime = now;
                
                final int updateSize = this.updateUBO(ctx, time);
                
                cmdbuf.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);
                cmdbuf.copyBuffer(this.uniformStaging, this.uniformBuffer, new VKBufferCopy()
                        .withSize(updateSize));
                
                cmdbuf.beginRenderPass(fb, this.renderPass, VKSubpassContents.SECONDARY_COMMAND_BUFFERS, renderArea, pClearValues);
                
                cmdbuf.executeCommands(renderCommand.get());
                
                cmdbuf.endRenderPass();
                cmdbuf.end();
            }
            
            @Override
            public void cleanup() {
                uniformBuffer.close();
                vertexBuffer.close();
                pipeline.close();
                pipelineLayout.close();
                vsh.close();
                fsh.close();
                renderPass.close();
            }
        };
        
        test.start();
    }
}
