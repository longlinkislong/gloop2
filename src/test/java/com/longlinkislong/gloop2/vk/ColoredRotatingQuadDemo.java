/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.vk.VKManagedBuffer.MappedBuffer;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import org.joml.Matrix4f;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class ColoredRotatingQuadDemo {
    
    static {
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        //System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
        System.setProperty("com.longlinkislong.gloop2.vk.VKContext.vsync", "true");
        System.setProperty("com.longlinkislong.gloop2.vk.VKContext.async_present", "true");
    }
    
    private static VKRenderPass createRenderPass(final VKContext ctx) {
        return VKRenderPassInfo.infer(ctx.getPresentAttachments()).create(ctx);
    }
    
    private static VKShaderModule loadShader(final VKContext ctx, final String path) throws IOException {
        try (InputStream shin = ColoredTriangleDemo.class.getResourceAsStream(path)) {
            return Tools.withStreamData(shin, data -> new VKShaderModule(ctx, data));
        }
    }
    
    private static VKDescriptorSetLayout createDescriptorSetLayout(final VKContext ctx) {
        return new VKDescriptorSetLayoutInfo()
                .withBindings(new VKDescriptorSetLayoutInfo.Binding()
                        .withBinding(0)
                        .withDescriptorType(VKDescriptorType.UNIFORM_BUFFER)
                        .withDescriptorCount(1)
                        .withStageFlags(VKShaderStage.VERTEX))
                .create(ctx);
    }
    
    private static VKPipelineLayout createPipelineLayout(final VKContext ctx, final VKDescriptorSetLayout setLayout) {
        return new VKPipelineLayoutInfo()
                .withSetLayouts(setLayout)
                .create(ctx);
    }
    
    private static VKDescriptorSet createDescriptorSet(final VKContext ctx, final VKDescriptorPool descriptorPool, final VKDescriptorSetLayout layout, final VKBuffer ubo, final long offset, final long range) {
        final VKDescriptorSet set = descriptorPool.allocate(layout);
        
        ctx.device.updateDescriptorSets(new VKWriteDescriptorSet()
                .withDstSet(set)
                .withDescriptorType(VKDescriptorType.UNIFORM_BUFFER)
                .withDstBinding(0)
                .withBufferInfo(new VKDescriptorBufferInfo()
                        .withBuffer(ubo)
                        .withOffset(offset)
                        .withRangeWholeSize()),
                null);
        
        return set;
    }
    
    private static VKDescriptorPool createDescriptorPool(final VKContext ctx) {
        return new VKDescriptorPoolInfo()
                .withPoolSizes(new VKDescriptorPoolSize()
                        .withType(VKDescriptorType.UNIFORM_BUFFER)
                        .withDescriptorCount(1))
                .withMaxSets(1)
                .create(ctx);
    }
    
    private static VKManagedBuffer createUniformBuffer(final VKContext ctx) {
        final VKManagedBuffer out = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.UNIFORM)
                .withSize(16 * Float.BYTES)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE));
        
        return out;
    }
    
    private static VKManagedBuffer createVertexBuffer(final VKContext ctx) {
        final VKManagedBuffer out = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.VERTEX)
                .withSize(6 * (2 + 3) * Float.BYTES)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE));
        
        try (VKManagedBuffer.MappedBuffer mapping = out.map()) {
            mapping.write(
                    -0.5F, -0.5F, 1.0F, 0.0F, 0.0F,
                    0.5F, -0.5F, 0.0F, 1.0F, 0.0F,
                    0.5F, 0.5F, 0.0F, 0.0F, 1.0F,
                    0.5F, 0.5F, 0.0F, 0.0F, 1.0F,
                    -0.5F, 0.5F, 0.0F, 1.0F, 0.0F,
                    -0.5F, -0.5F, 1.0F, 0.0F, 0.0F);
        }
        
        return out;
    }
    
    private static VKGraphicsPipeline createPipeline(
            final VKContext ctx,
            final VKRenderPass renderPass,
            final VKPipelineLayout layout,
            final VKShaderModule vsh, final VKShaderModule fsh) {
        
        VKGraphicsPipelineInfo pipelineInfo = VKGraphicsPipelineInfo.inferDynamicViewports(null, 1);
        
        pipelineInfo = VKGraphicsPipelineInfo.inferFromRenderPass(pipelineInfo, renderPass);
        
        return pipelineInfo
                .withLayout(layout)
                .withVertexInputState(new VKPipelineVertexInputStateInfo()
                        .withBindingDescriptions(new VKPipelineVertexInputStateInfo.BindingDescription()
                                .withBinding(0)
                                .withStride((2 + 3) * 4)
                                .withInputRate(VKVertexInputRate.VERTEX))
                        .withAttributeDescriptions(
                                new VKPipelineVertexInputStateInfo.AttributeDescription()
                                        .withBinding(0)
                                        .withLocation(0)
                                        .withFormat(VKFormat.VEC2)
                                        .withOffset(0),
                                new VKPipelineVertexInputStateInfo.AttributeDescription()
                                        .withBinding(0)
                                        .withLocation(1)
                                        .withFormat(VKFormat.VEC3)
                                        .withOffset(2 * 4)))
                .withAssemblyState(VKPipelineInputAssemblyStateInfo.DEFAULT_TRIANGLE_LIST)
                .withRasterizationState(VKPipelineRasterizationStateInfo.DEFAULT_OPENGL)
                .withDepthStencilState(VKPipelineDepthStencilStateInfo.DEFAULT_OPENGL_DEPTH_TEST)
                .withStages(
                        VKPipelineShaderStageInfo.vertexShader(vsh),
                        VKPipelineShaderStageInfo.fragmentShader(fsh))
                .create(ctx);
    }
    
    @Test
    public void test() {
        final VulkanTestFramework test = new VulkanTestFramework();
        
        test.name = "Colored Triangle Demo";
        test.context = new VulkanTestFramework.Context() {
            final String VERTEX_SHADER_URL = "/com/longlinkislong/gloop2/lwjgl/demo/vulkan/coloredRotatingTriangle.vert.spv";
            final String FRAGMENT_SHADER_URL = "/com/longlinkislong/gloop2/lwjgl/demo/vulkan/coloredRotatingTriangle.frag.spv";
            
            VKRenderPass renderPass;
            VKShaderModule vsh, fsh;
            VKPipelineLayout pipelineLayout;
            VKGraphicsPipeline pipeline;
            VKManagedBuffer vertexBuffer;
            VKManagedBuffer uniformBuffer;
            VKDescriptorPool descriptorPool;
            VKDescriptorSetLayout descriptorSetLayout;
            VKDescriptorSet descriptorSet;
            
            VKScissor renderArea = new VKScissor(0, 0, 800, 600);
            VKViewport viewport = new VKViewport(0, 0, 800, 600, 0, 1);
            
            long lastTime = System.nanoTime();
            float time = 0;
            
            @Override
            public void init(VKContext ctx) throws Exception {
                renderPass = createRenderPass(ctx);
                vsh = loadShader(ctx, VERTEX_SHADER_URL);
                fsh = loadShader(ctx, FRAGMENT_SHADER_URL);
                descriptorSetLayout = createDescriptorSetLayout(ctx);
                pipelineLayout = createPipelineLayout(ctx, descriptorSetLayout);
                pipeline = createPipeline(ctx, renderPass, pipelineLayout, vsh, fsh);
                vertexBuffer = createVertexBuffer(ctx);
                uniformBuffer = createUniformBuffer(ctx);
                descriptorPool = createDescriptorPool(ctx);
                descriptorSet = createDescriptorSet(ctx, descriptorPool, descriptorSetLayout, uniformBuffer, 0, 16 * Float.BYTES);
            }
            
            private void updateUBO(float angle) {
                Matrix4f m = new Matrix4f()
                        .scale(1, -1, 1)
                        .rotateZ(angle);
                
                try (MappedBuffer map = uniformBuffer.map()) {
                    m.get(map.getPointer());
                }
            }
            
            private final Set<VKCommandBuffer> primary = new HashSet<>();
            
            @Override
            public void onFrame(VKContext ctx) {
                final ByteBuffer[] pClearValues = {
                    Tools.clearColor(100F / 255F, 149F / 255F, 237F / 255F, 1F),
                    Tools.clearDepthStencil(1.0F, 0)};
                
                final VKCommandBuffer cmdbuf = ctx.getPrimaryCommandBuffer();
                final VKFramebuffer fb = ctx.getFramebuffer();
                
                long now = System.nanoTime();
                time += (now - lastTime) / 1E9F;
                lastTime = now;
                
                this.updateUBO(time);
                
                if (this.primary.add(cmdbuf)) {
                    System.out.println("Wrote commandbuffer!");
                    
                    cmdbuf.begin(new VKCommandBufferBeginInfo());
                    cmdbuf.beginRenderPass(fb, this.renderPass, VKSubpassContents.INLINE, renderArea, pClearValues);
                    
                    cmdbuf.setViewport(viewport);
                    cmdbuf.setScissor(renderArea);
                    cmdbuf.bindPipeline(pipeline);
                    cmdbuf.bindDescriptorSet(VKPipelineBindPoint.GRAPHICS, pipelineLayout, 0, descriptorSet);
                    cmdbuf.bindVertexBuffer(0, vertexBuffer, 0L);
                    cmdbuf.draw(6, 1, 0, 0);
                    
                    cmdbuf.endRenderPass();
                    cmdbuf.end();
                }
            }
            
            @Override
            public void cleanup() {
                vertexBuffer.close();
                pipeline.close();
                pipelineLayout.close();
                vsh.close();
                fsh.close();
                renderPass.close();
            }
        };
        test.start();
    }
    
}
