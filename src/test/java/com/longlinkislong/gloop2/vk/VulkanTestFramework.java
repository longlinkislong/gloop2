/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.glfw.GLFWHints;
import com.longlinkislong.gloop2.glfw.GLFWWindow;
import com.longlinkislong.gloop2.glfw.GLFWWindowCreateInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import junit.framework.Assert;
import org.lwjgl.vulkan.EXTDebugReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class VulkanTestFramework {
    private static final Logger LOGGER = LoggerFactory.getLogger(VulkanTestFramework.class);
    public interface Context {
        
        void init(VKContext ctx) throws Exception;
        
        void onFrame(VKContext ctx);
        
        void cleanup() throws Exception;
    }
    
    public String name = "Demo App";
    public final List<String> layers = Arrays.asList(
            //"VK_LAYER_LUNARG_api_dump",
            "VK_LAYER_LUNARG_core_validation",
            "VK_LAYER_LUNARG_parameter_validation");
    
    public final List<String> extensions = Arrays.asList(EXTDebugReport.VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    public boolean enableLayers = true;
    public int width = 800;
    public int height = 600;
    public Context context = null;
    
    public void start() {
        final List<String> enabledLayers = enableLayers ? layers : Collections.emptyList();
        
        try (VKInstance instance = new VKInstanceCreateInfo()
                .withApplicationName(this.name)
                .withVersion(1, 0, 0)
                .withLayers(enabledLayers)
                .withExtensions(extensions)
                .create()) {
            
            final VKPhysicalDevice selectedPhysicalDevice = instance.physicalDevices()
                    .filter(VKPhysicalDevice::isGPU)
                    .findFirst()
                    .get();
            
            final List<VKDeviceCreateInfo.DeviceQueueCreateInfo> queueInfos = new ArrayList<>();
            
            final VKQueueFamily mainQueueFamily = selectedPhysicalDevice.queueFamilies()
                    .filter(VKQueueFamily::canDraw)
                    .filter(VKQueueFamily::canCompute)
                    .findFirst()                    
                    .orElseThrow(() -> new UnsupportedOperationException("Could not find the primary queue family!"));
            
            queueInfos.add(new VKDeviceCreateInfo.DeviceQueueCreateInfo().withQueueFamily(mainQueueFamily));
            
            try {
                final VKQueueFamily asyncPresentQueueFamily = selectedPhysicalDevice.queueFamilies()
                    .filter(VKQueueFamily::canCompute)
                    .filter(VKQueueFamily::canPresent)
                    .filter(qf -> qf != mainQueueFamily)
                    .findFirst()
                    .orElseThrow(() -> new UnsupportedOperationException("Could not find async present queue family!"));
                
                queueInfos.add(new VKDeviceCreateInfo.DeviceQueueCreateInfo().withQueueFamily(asyncPresentQueueFamily));
            } catch (UnsupportedOperationException ex) {
                LOGGER.warn("Could not find queue family needed for async present! Attempting to use main queue for present...");                
                
                if (!mainQueueFamily.canPresent()) {                    
                    throw new UnsupportedOperationException("Main Queue Family cannot present!");
                }
            }
            
            final GLFWWindowCreateInfo windowInfo = new GLFWWindowCreateInfo()
                    .withWidth(width)
                    .withHeight(height)
                    .withTitle(name)
                    .withHints(new GLFWHints()
                            .withContextHints(
                                    new GLFWHints.VulkanHints(
                                            new VKDeviceCreateInfo()
                                                    .withLayers(enabledLayers)
                                                    .withQueueCreateInfos(queueInfos))));
            
            try (GLFWWindow window = windowInfo.create()) {
                window.show();
                
                final VKContext ctx = window.getVulkanContext().get();
                
                this.context.init(ctx);
                
                int frames = 0;
                long previousTime = System.currentTimeMillis();
                
                while (!window.shouldClose()) {
                    window.pollEvents();
                    window.beginFrame();
                    
                    this.context.onFrame(ctx);
                    
                    window.endFrame();
                    
                    frames++;
                    
                    final long now = System.currentTimeMillis();
                    final double dT = (now - previousTime) * 0.001;
                    
                    if (dT > 3.0) {
                        System.out.printf("FPS: %.2f%n", (frames / dT));
                        previousTime = now;
                        frames = 0;
                    }
                }
                
                ctx.mainQueue.waitIdle();
                ctx.presentQueue.waitIdle();
                
                this.context.cleanup();
            } catch (Exception ex) {
                ex.printStackTrace();
                Assert.fail(ex.getMessage());
            }
        }
    }
}
