package com.longlinkislong.gloop2.vk;

import java.nio.ByteBuffer;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class ClearScreenDemo {

    static {
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
        System.setProperty("com.longlinkislong.gloop2.vk.VKContext.vsync", "true");
    }

    @Test
    public void test() {
        VulkanTestFramework test = new VulkanTestFramework();
                
        test.name = "Clear Screen Demo";
        test.context = new VulkanTestFramework.Context() {
            VKRenderPass renderPass;
            VKScissor renderArea = new VKScissor(0, 0, 800, 600);
            VKViewport viewport = new VKViewport(0, 0, 800, 600, 0, 1);

            @Override
            public void init(VKContext ctx) {                
                renderPass = VKRenderPassInfo.infer(
                        ctx.getPresentAttachments())
                        .create(ctx);
            }

            double t = 0.0;
            
            @Override
            public void onFrame(VKContext ctx) {
                final float r = (float) (0.5 * Math.cos(t) + 0.5);
                final float g = (float) (0.5 * Math.sin(t) + 0.5);
                final float b = (float) (0.5 * Math.sin(t + 0.5) + 0.5);
                
                final ByteBuffer[] pClearValues = ctx.clearValues(r, g, b, 1F, 1F, 0);
                final VKCommandBuffer cmdbuf = ctx.getPrimaryCommandBuffer();
                final VKFramebuffer fb = ctx.getFramebuffer();

                cmdbuf.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);
                cmdbuf.beginRenderPass(fb, renderPass, VKSubpassContents.INLINE, renderArea, pClearValues);
                cmdbuf.setViewport(viewport);
                cmdbuf.setScissor(renderArea);
                cmdbuf.endRenderPass();
                cmdbuf.end();                                
                                
                t += 0.001;
            }

            @Override
            public void cleanup() {
                renderPass.close();
            }
        };        
        test.start();
    }
}
