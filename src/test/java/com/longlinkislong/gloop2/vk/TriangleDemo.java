/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.glfw.GLFWHints;
import com.longlinkislong.gloop2.glfw.GLFWWindow;
import com.longlinkislong.gloop2.glfw.GLFWWindowCreateInfo;
import com.longlinkislong.gloop2.vk.VKBufferInfo.Usage;
import com.longlinkislong.gloop2.vk.VKManagedBuffer.MappedBuffer;
import com.longlinkislong.gloop2.vk.VKPipelineVertexInputStateInfo.AttributeDescription;
import com.longlinkislong.gloop2.vk.VKPipelineVertexInputStateInfo.BindingDescription;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import org.junit.Test;
import org.lwjgl.vulkan.EXTDebugReport;

/**
 *
 * @author zmichaels
 */
public class TriangleDemo {

    static {
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }

    private static final boolean ENABLE_LAYERS = Boolean.getBoolean("com.longlinkislong.gloop2.vk.enable_layers");
    private static final String[] LAYERS = {"VK_LAYER_LUNARG_api_dump"};
    private static final String DEMO_NAME = "Triangle Demo";
    private static final VKScissor RENDER_AREA = new VKScissor(0, 0, 800, 600);
    private static final VKViewport VIEWPORT = new VKViewport(0F, 0F, 800F, 600F, 0F, 1F);
    private static final String VERTEX_SHADER_URL = "/com/longlinkislong/gloop2/lwjgl/demo/vulkan/triangle.vert.spv";
    private static final String FRAGMENT_SHADER_URL = "/com/longlinkislong/gloop2/lwjgl/demo/vulkan/triangle.frag.spv";

    private static final class AppContext implements AutoCloseable {

        VKContext vkctx;
        VKRenderPass renderPass;
        VKShaderModule vsh;
        VKShaderModule fsh;
        VKPipelineLayout layout;
        VKGraphicsPipeline pipeline;
        VKManagedBuffer vertexBuffer;

        @Override
        public void close() {
            this.renderPass.close();
            this.vsh.close();
            this.fsh.close();
            this.layout.close();
            this.pipeline.close();
            this.vertexBuffer.close();
        }

        private void createVertexBuffer() {
            this.vertexBuffer = new VKBufferInfo()
                    .withSize(2 * 3 * Float.BYTES)
                    .withUsage(Usage.VERTEX)
                    .createManaged(vkctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE));

            try (MappedBuffer mapping = this.vertexBuffer.map()) {
                mapping.write(-0.5F, -0.5F, 0.5F, -0.5F, 0.0F, 0.5F);
            }
        }

        private void loadShaders() throws IOException {
            try (InputStream vin = this.getClass().getResourceAsStream(VERTEX_SHADER_URL);
                    InputStream fin = this.getClass().getResourceAsStream(FRAGMENT_SHADER_URL)) {

                this.vsh = Tools.withStreamData(vin, data -> new VKShaderModule(this.vkctx, data));
                this.fsh = Tools.withStreamData(fin, data -> new VKShaderModule(this.vkctx, data));
            }
        }

        private void createPipelineLayout() {
            this.layout = new VKPipelineLayout(this.vkctx);
        }

        private void createPipeline() {
            VKGraphicsPipelineInfo pipelineInfo = VKGraphicsPipelineInfo.inferDynamicViewports(null, 1);

            pipelineInfo = VKGraphicsPipelineInfo.inferFromRenderPass(pipelineInfo, renderPass);

            this.pipeline = pipelineInfo
                    .withLayout(layout)
                    .withVertexInputState(new VKPipelineVertexInputStateInfo()
                            .withBindingDescriptions(new BindingDescription()
                                    .withBinding(0)
                                    .withStride(2 * 4)
                                    .withInputRate(VKVertexInputRate.VERTEX))
                            .withAttributeDescriptions(new AttributeDescription()
                                    .withBinding(0)
                                    .withLocation(0)
                                    .withFormat(VKFormat.VEC2)
                                    .withOffset(0)))
                    .withAssemblyState(new VKPipelineInputAssemblyStateInfo().withTopology(VKPrimitiveTopology.TRIANGLE_LIST))
                    .withRasterizationState(VKPipelineRasterizationStateInfo.DEFAULT_OPENGL)
                    .withStages(
                            VKPipelineShaderStageInfo.vertexShader(vsh),
                            VKPipelineShaderStageInfo.fragmentShader(fsh))
                    .create(vkctx);
        }

        private void createRenderPass() {
            this.renderPass = VKRenderPassInfo.infer(vkctx.getPresentAttachments())
                    .create(vkctx);
        }

        private void onFrame() {
            final ByteBuffer[] pClearValues = {Tools.clearColor(100F / 255F, 149F / 255F, 237F / 255F, 1F)};
            final VKCommandBuffer cmdbuf = this.vkctx.getPrimaryCommandBuffer();
            final VKFramebuffer fb = this.vkctx.getFramebuffer();

            cmdbuf.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);
            cmdbuf.beginRenderPass(fb, this.renderPass, VKSubpassContents.INLINE, RENDER_AREA, pClearValues);
            cmdbuf.setViewport(VIEWPORT);
            cmdbuf.setScissor(RENDER_AREA);
            cmdbuf.bindPipeline(pipeline);
            cmdbuf.bindVertexBuffer(0, vertexBuffer, 0L);
            cmdbuf.draw(3, 1, 0, 0);
            cmdbuf.endRenderPass();
            cmdbuf.end();
        }
    }

    @Test
    public void test() throws Exception {
        final List<String> enabledLayers = ENABLE_LAYERS ? Arrays.asList(LAYERS) : Collections.emptyList();

        try (VKInstance instance = new VKInstanceCreateInfo()
                .withApplicationName(DEMO_NAME)
                .withVersion(1, 0, 0)
                .withLayers(enabledLayers)
                .withExtensions(EXTDebugReport.VK_EXT_DEBUG_REPORT_EXTENSION_NAME)
                .create()) {

            final VKPhysicalDevice selectedPhysicalDevice = instance.physicalDevices()
                    .filter(VKPhysicalDevice::isGPU)
                    .findFirst()
                    .get();

            final VKQueueFamily selectedQueueFamily = selectedPhysicalDevice.queueFamilies()
                    .filter(qf -> qf.canDraw() && qf.canPresent())
                    .findFirst()
                    .get();

            final GLFWWindowCreateInfo windowInfo = new GLFWWindowCreateInfo()
                    .withWidth(RENDER_AREA.width)
                    .withHeight(RENDER_AREA.height)
                    .withTitle(DEMO_NAME)
                    .withHints(new GLFWHints()
                            .withContextHints(
                                    new GLFWHints.VulkanHints(
                                            new VKDeviceCreateInfo()
                                                    .withLayers(enabledLayers)
                                                    .withQueueCreateInfos(
                                                            new VKDeviceCreateInfo.DeviceQueueCreateInfo()
                                                                    .withQueueFamily(selectedQueueFamily)))));

            try (GLFWWindow window = windowInfo.create();
                    AppContext appctx = new AppContext()) {

                window.show();

                appctx.vkctx = window.getVulkanContext().get();
                appctx.createRenderPass();
                appctx.loadShaders();
                appctx.createPipelineLayout();
                appctx.createPipeline();
                appctx.createVertexBuffer();

                while (!window.shouldClose()) {

                    window.pollEvents();
                    window.beginFrame();

                    appctx.onFrame();

                    window.endFrame();
                }
            }
        }
    }
}
