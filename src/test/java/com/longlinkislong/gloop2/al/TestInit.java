/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import org.junit.Test;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;

/**
 *
 * @author zmichaels
 */
public class TestInit {
    @Test
    public void runTest() {        
        ALContext.getInstance();
        
        System.out.println(AL10.alGetString(AL10.AL_EXTENSIONS));
    }
}
