/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2;

import com.longlinkislong.gloop2.gl.GLPixelInfo;
import java.util.Random;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestPackedInts {

    private final int ITERATIONS = 1000;
    private final Random rng = new Random();

    @Test
    public void testPack_4_4_4_4() {
        for (int i = 0; i < ITERATIONS; i++) {
            final int r = rng.nextInt(0xF + 1);
            final int g = rng.nextInt(0xF + 1);
            final int b = rng.nextInt(0xF + 1);
            final int a = rng.nextInt(0xF + 1);

            final int packed = GLPixelInfo.Type.UNSIGNED_SHORT_4_4_4_4.pack(r, g, b, a);

            Assert.assertEquals(r, GLPixelInfo.Type.UNSIGNED_SHORT_4_4_4_4.r(packed));
            Assert.assertEquals(g, GLPixelInfo.Type.UNSIGNED_SHORT_4_4_4_4.g(packed));
            Assert.assertEquals(b, GLPixelInfo.Type.UNSIGNED_SHORT_4_4_4_4.b(packed));
            Assert.assertEquals(a, GLPixelInfo.Type.UNSIGNED_SHORT_4_4_4_4.a(packed));
        }
    }
    
    @Test
    public void testPack_5_6_5() {
        for (int i = 0; i < ITERATIONS; i++) {
            final int r = rng.nextInt(0x1F + 1);
            final int g = rng.nextInt(0x3F + 1);
            final int b = rng.nextInt(0x1F + 1);
            
            final int packed = GLPixelInfo.Type.UNSIGNED_SHORT_5_6_5.pack(r, g, b, 0);
            
            Assert.assertEquals(r, GLPixelInfo.Type.UNSIGNED_SHORT_5_6_5.r(packed));
            Assert.assertEquals(g, GLPixelInfo.Type.UNSIGNED_SHORT_5_6_5.g(packed));
            Assert.assertEquals(b, GLPixelInfo.Type.UNSIGNED_SHORT_5_6_5.b(packed));
        }
    }
    
    @Test
    public void testPack_5_5_5_1() {
        
    }
}
