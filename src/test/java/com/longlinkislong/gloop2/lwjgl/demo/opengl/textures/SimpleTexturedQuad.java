/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.lwjgl.demo.opengl.textures;

import com.longlinkislong.gloop2.glfw.GLFWHints;
import com.longlinkislong.gloop2.glfw.GLFWWindow;
import com.longlinkislong.gloop2.ResourceSet;
import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.gl.GLBuffer;
import com.longlinkislong.gloop2.gl.GLBuffer.MappedBuffer;
import com.longlinkislong.gloop2.gl.GLBufferAccess;
import com.longlinkislong.gloop2.gl.GLBufferInfo;
import com.longlinkislong.gloop2.gl.GLClearBuffer;
import com.longlinkislong.gloop2.gl.GLClearStateInfo;
import com.longlinkislong.gloop2.gl.GLDraw;
import com.longlinkislong.gloop2.gl.GLInternalFormat;
import com.longlinkislong.gloop2.gl.GLLinkException;
import com.longlinkislong.gloop2.gl.GLPixelInfo;
import com.longlinkislong.gloop2.gl.GLProgram;
import com.longlinkislong.gloop2.gl.GLProgramBinaryFormat;
import com.longlinkislong.gloop2.gl.GLProgramInfo;
import com.longlinkislong.gloop2.gl.GLProgramInfo.AttributeStateInfo;
import com.longlinkislong.gloop2.gl.GLShader;
import com.longlinkislong.gloop2.gl.GLShaderBinaryFormat;
import com.longlinkislong.gloop2.gl.GLShaderInfo;
import com.longlinkislong.gloop2.gl.GLShaderType;
import com.longlinkislong.gloop2.gl.GLTexture;
import com.longlinkislong.gloop2.gl.GLTextureInfo;
import com.longlinkislong.gloop2.gl.GLVertexArray;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo.AttributeDescription;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo.BindingDescription;
import com.longlinkislong.gloop2.gl.GLVertexFormat;
import com.longlinkislong.gloop2.gl.GLViewport;
import com.longlinkislong.gloop2.image.Image;
import com.longlinkislong.gloop2.image.ImageIO;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DecimalFormat;
import java.util.EnumSet;
import org.junit.Test;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class SimpleTexturedQuad {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleTexturedQuad.class);

    static final boolean SPIRV = false;

    final GLFWHints hints = new GLFWHints()
            .withContextHints(new GLFWHints.OpenGLHints()
                    .withVersion(4, 5))
            .withWindowHints(new GLFWHints.WindowHints()
                    .withVisible(false)
                    .withResizable(false));

    final GLFWWindow window = new GLFWWindow();
    final GLVertexArray vao = new GLVertexArray();
    final GLProgram quadProgram = new GLProgram();
    final GLBuffer vboPosition = new GLBuffer();
    final GLBuffer vboTexCoords = new GLBuffer();
    final GLTexture texture = new GLTexture();

    int width = 800;
    int height = 600;

    void init() throws Exception {
        try (GLFWWindow newWindow = new GLFWWindow(hints, "Simple textured quad", width, height);
                GLTexture newTexture = this.createTexture();
                GLProgram newProgram = this.createQuadProgram();
                GLBuffer newVPos = this.createPositionData();
                GLBuffer newVTex = this.createTexCoordData()) {

            newWindow.setSwapInterval(0);

            width = newWindow.getFramebufferWidth();
            height = newWindow.getFramebufferHeight();

            this.vboPosition.swap(newVPos);
            this.vboTexCoords.swap(newVTex);

            try (GLVertexArray newVao = new GLVertexArrayInfo()
                    .withBindings(
                            new BindingDescription()
                                    .withBinding(0)
                                    .withBuffer(vboPosition)
                                    .withStride(GLVertexFormat.VEC2.bytes),
                            new BindingDescription()
                                    .withBinding(1)
                                    .withBuffer(vboTexCoords)
                                    .withStride(GLVertexFormat.VEC2.bytes))
                    .withAttributes(
                            new AttributeDescription()
                                    .withBinding(0)
                                    .withFormat(GLVertexFormat.VEC2)
                                    .withLocation(0),
                            new AttributeDescription()
                                    .withBinding(1)
                                    .withFormat(GLVertexFormat.VEC2)
                                    .withLocation(1))
                    .create()) {

                this.vao.swap(newVao);
            }

            newWindow.show();

            this.window.swap(newWindow);
            this.texture.swap(newTexture);
            this.quadProgram.swap(newProgram);
        }

    }

    GLProgram createQuadProgramSPIRV() throws IOException {
        final GLProgram out = new GLProgram();

        try (InputStream vin = this.getClass().getResourceAsStream("texturedQuad.vert.spv");
                InputStream fin = this.getClass().getResourceAsStream("texturedQuad.frag.spv")) {

            try (GLShader vsh = Tools.withStreamData(vin, data -> new GLShaderInfo()
                    .withEntryPoint("main")
                    .withType(GLShaderType.VERTEX)
                    .withFormat(GLShaderBinaryFormat.SPIRV)
                    .withData(data)
                    .create());
                    GLShader fsh = Tools.withStreamData(fin, data -> new GLShaderInfo()
                    .withEntryPoint("main")
                    .withType(GLShaderType.FRAGMENT)
                    .withFormat(GLShaderBinaryFormat.SPIRV)
                    .withData(data)
                    .create())) {

                try (GLProgram newProgram = new GLProgramInfo()
                        .withShaders(vsh, fsh)
                        .create()) {

                    newProgram.uniform1(newProgram.getUniformLocation("tex"), 0);
                    out.swap(newProgram);
                }
            }
        }

        return out;
    }

    GLProgram createQuadProgramGLSL() throws IOException {
        final GLProgram out = new GLProgram();

        try (InputStream vin = this.getClass().getResourceAsStream("texturedQuad.vert");
                InputStream fin = this.getClass().getResourceAsStream("texturedQuad.frag")) {

            final String vsrc = Tools.readAll(vin);
            final String fsrc = Tools.readAll(fin);

            try (GLShader vsh = GLShader.newVertexShader(vsrc);
                    GLShader fsh = GLShader.newFragmentShader(fsrc)) {

                try (GLProgram newProgram = new GLProgramInfo()
                        .withAttributes(
                                new AttributeStateInfo()
                                        .withIndex(0)
                                        .withName("position"),
                                new AttributeStateInfo()
                                        .withIndex(1)
                                        .withName("texCoords"))
                        .withShaders(vsh, fsh)
                        .create()) {

                    newProgram.uniform1(newProgram.getUniformLocation("tex"), 0);

                    out.swap(newProgram);
                }
            }
        }

        return out;
    }

    private static final Path CACHE_PROGRAM_DIR = Tools.APPDATA.resolve("com.longlinkislong/gloop2/program_cache");
    private static final Path CACHED_PROGRAM = CACHE_PROGRAM_DIR.resolve("texturedQuad.bin");

    GLProgram createQuadProgramCached() throws IOException {
        return Tools.withFileDataMapped(CACHED_PROGRAM, data -> new GLProgramInfo()
                .withBinaryFormat(GLProgramBinaryFormat.PROPRIETARY)
                .withBinary(data)
                .create());
    }

    GLProgram createQuadProgram() throws IOException {
        if (Files.exists(CACHED_PROGRAM)) {
            LOGGER.info("Loading cached program...");
            
            try {
                return createQuadProgramCached();
            } catch (GLLinkException ex) {
                LOGGER.error("Unable to load cached program!");
                LOGGER.debug(ex.getMessage(), ex);
                Files.deleteIfExists(CACHED_PROGRAM);
            }
        }

        final GLProgram out;

        if (SPIRV) {
            out = createQuadProgramSPIRV();
        } else {
            out = createQuadProgramGLSL();
        }

        if (GLProgramBinaryFormat.PROPRIETARY.isSupported()) {
            Files.createDirectories(CACHE_PROGRAM_DIR);

            final int binarySize = out.getBinarySize();

            try (FileChannel fc = FileChannel.open(CACHED_PROGRAM, StandardOpenOption.CREATE_NEW, StandardOpenOption.READ, StandardOpenOption.WRITE)) {
                final ByteBuffer writeData = fc.map(FileChannel.MapMode.READ_WRITE, 0L, binarySize);

                out.getBinary(GLProgramBinaryFormat.PROPRIETARY, writeData);
            }
        }

        return out;
    }

    GLBuffer createPositionData() {
        final GLBuffer out = new GLBuffer();

        try (GLBuffer newBuffer = new GLBufferInfo()
                .withSize(12 * Float.BYTES)
                .withUsage(GLBufferInfo.Usage.STATIC_DRAW)
                .create()) {

            try (MappedBuffer map = newBuffer.map(EnumSet.of(GLBufferAccess.WRITE))) {
                final float[] data = {
                    -1.0F, -1.0F,
                    1.0F, -1.0F,
                    1.0F, 1.0F,
                    1.0F, 1.0F,
                    -1.0F, 1.0F,
                    -1.0F, -1.0F};

                MemoryUtil.memFloatBuffer(map.ptr(), data.length).put(data);
            }

            out.swap(newBuffer);
        }

        return out;
    }

    GLBuffer createTexCoordData() {
        final GLBuffer out = new GLBuffer();

        try (GLBuffer newBuffer = new GLBufferInfo()
                .withSize(12 * Float.BYTES)
                .withUsage(GLBufferInfo.Usage.STATIC_DRAW)
                .create()) {

            try (MappedBuffer map = newBuffer.map(EnumSet.of(GLBufferAccess.WRITE))) {
                final float[] data = {
                    0.0F, 1.0F,
                    1.0F, 1.0F,
                    1.0F, 0.0F,
                    1.0F, 0.0F,
                    0.0F, 0.0F,
                    0.0F, 1.0F
                };

                MemoryUtil.memFloatBuffer(map.ptr(), data.length).put(data);
            }

            out.swap(newBuffer);
        }

        return out;
    }

    GLTexture createTexture() throws IOException {
        try (InputStream in = this.getClass().getResourceAsStream("environment.png")) {
            try (Image img = ImageIO.read(in)) {
                final GLTexture out = new GLTexture();

                try (GLTexture newTex = new GLTextureInfo()
                        .withExtent(img.getWidth(), img.getHeight(), 1)
                        .withFormat(GLInternalFormat.RGBA8)
                        .withData(GLPixelInfo.of(img))
                        .create()) {

                    out.swap(newTex);
                }

                return out;
            }
        }
    }

    final GLClearStateInfo clearState = new GLClearStateInfo()
            .withBuffers(GLClearBuffer.COLOR)
            .withColor(0F, 0F, 0F, 1F);

    void render() {
        clearState.apply();

        this.quadProgram.use();
        this.texture.bind(0);

        this.vao.bind();

        GLDraw.drawArrays(GLDraw.Mode.TRIANGLES, 0, 6);
    }

    @Test
    public void runTest() throws Exception {
        this.init();

        int fps = 0;
        long lastTime = System.currentTimeMillis();

        try (ResourceSet res = ResourceSet.setOf(this.quadProgram, this.vao, this.vboPosition, this.vboTexCoords, this.window)) {
            final DecimalFormat fpsFormat = new DecimalFormat(".##");

            while (!this.window.shouldClose()) {
                this.window.pollEvents();

                new GLViewport(0, 0, width, height, 0, 1).apply();
                render();
                this.window.endFrame();

                long now = System.currentTimeMillis();
                double dT = (now - lastTime) * 0.001;

                if (dT > 3.0) {
                    LOGGER.info("FPS: {}", fpsFormat.format(fps / dT));
                    fps = 0;
                    lastTime = now;
                }

                fps++;
            }
        }
    }
}
