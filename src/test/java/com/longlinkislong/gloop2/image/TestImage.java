/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

import java.io.InputStream;
import java.nio.file.Paths;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestImage {
    @Test
    public void testSTBIStore() throws Exception {
        try (InputStream in = TestImage.class.getResourceAsStream("/com/longlinkislong/gloop2/lwjgl/demo/opengl/textures/environment.png")) {      
            try (Image img = ImageIO.read(in, 4)) {
                long start = System.currentTimeMillis();
                ImageIO.write(img, ImageFileFormat.BMP, Paths.get("test.bmp"));               
                long bmpEnd = (System.currentTimeMillis() - start);
                
                start = System.currentTimeMillis();
                ImageIO.write(img, ImageFileFormat.PNG, Paths.get("test.png"));
                long pngEnd = (System.currentTimeMillis() - start);
                
                start = System.currentTimeMillis();
                ImageIO.write(img, ImageFileFormat.TGA, Paths.get("test.tga"));
                long tgaEnd = (System.currentTimeMillis() - start);
                
                System.out.printf("BMP write time: %dms%n", bmpEnd);
                System.out.printf("PNG write time: %dms%n", pngEnd);
                System.out.printf("TGA write time: %dms%n", tgaEnd);
            }
        }
    }
}
