/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

import com.longlinkislong.gloop2.ResourceSet;
import java.io.InputStream;
import java.nio.file.Paths;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestImageJoin {
    @Test
    public void testJoin() throws Exception {
        try (ResourceSet res = new ResourceSet()) {
            final Image[] sprites = new Image[10];
            int w = 0;
            int h = 0;
            
            for (int i = 0; i < sprites.length; i++) {
                try (InputStream in = TestImageJoin.class.getResourceAsStream("duke" + i + ".png")) {
                    sprites[i] = ImageIO.read(in, PixelFormat.R8G8B8A8_UNORM.components);
                    res.add(sprites[i]);
                    
                    h = Math.max(h, sprites[i].getHeight());
                    w += sprites[i].getWidth();
                }
            }
            
            final BufferedImage out = new BufferedImage(w, h, PixelFormat.R8G8B8A8_UNORM);
            
            res.add(out);
            
            int x = 0;
            
            for (Image sprite : sprites) {
                out.setSubimage(x, 0, sprite.getWidth(), sprite.getHeight(), sprite);
                x += sprite.getWidth();
            }
            
            ImageIO.write(out, ImageFileFormat.PNG, Paths.get("duke.png"));
        }
    }
}
