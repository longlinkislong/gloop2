/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import java.io.IOException;

/**
 * A SoundChannel that supports seeking.
 * @author zmichaels
 */
public interface SeekableSoundChannel extends SoundChannel {
    /**
     * Seeks to the start of the SoundChannel.
     * @throws IOException if the seek operation failed.
     */
    void seekStart() throws IOException;
    
    /**
     * Seeks to a specific sample in the SoundChannel.
     * @param sample the sample
     * @throws IOException if the seek operation failed.
     */
    void seek(int sample) throws IOException;
    
    /**
     * Retrieves the total length of the SoundChannel in seconds.
     * @return the total time.
     */
    float getLength();
}
