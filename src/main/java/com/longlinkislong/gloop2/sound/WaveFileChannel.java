/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.al.ALFormat;
import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An implementation of SeekableSoundChannel that stream decodes a sound file
 * encoded with Microsoft Wave PCM.
 *
 * @author zmichaels
 */
public class WaveFileChannel implements SeekableSoundChannel {

    private static final Logger LOGGER = LoggerFactory.getLogger(WaveFileChannel.class);

    private final SeekableByteChannel base;
    private WaveAudioFormat format;
    private int channels;
    private int sampleRate;
    private int byteRate;
    private int blockAlign;
    private int bitsPerSample;
    private int size;
    private int validBitsPerSample;
    private int channelMask;
    private int dataStart;

    public WaveFileChannel(final Path path) throws IOException {
        this.base = Files.newByteChannel(path, StandardOpenOption.READ);

        final ByteBuffer riffHeader = ByteBuffer.allocate(12);

        while (riffHeader.hasRemaining()) {
            if (base.read(riffHeader) == -1) {
                throw new IOException("Incorrect header; expecting RIFF!");
            }
        }

        riffHeader.flip();

        final int chunkId = riffHeader.order(ByteOrder.BIG_ENDIAN).getInt();

        if (chunkId != 0x52494646) {
            throw new IOException("Container is not RIFF!");
        }

        final int chunkSize = riffHeader.order(ByteOrder.LITTLE_ENDIAN).getInt();
        final int format = riffHeader.order(ByteOrder.BIG_ENDIAN).getInt();

        if (format != 0x57415645) {
            throw new IOException("Audio format is not WAVE!");
        }

        while (!parseSubchunk()) {

        }

        // WAVE is limited to 2GB so this isn't really a concern
        this.dataStart = (int) this.base.position();
    }

    @Override
    public void seekStart() throws IOException {
        if (this.isOpen()) {
            this.base.position(dataStart);
        } else {
            throw new IOException("WaveFileChannel is already closed!");
        }
    }

    @Override
    public void seek(int sample) throws IOException {
        if (this.isOpen()) {
            this.base.position(this.channels * this.bitsPerSample / 8 * sample);
        } else {
            throw new IOException("WaveFileChannel is already closed!");
        }
    }

    @Override
    public float getLength() {
        return ((float) this.size) / ((float) this.byteRate);
    }

    @Override
    public int getChannels() {
        return this.channels;
    }

    @Override
    public int getSampleRate() {
        return this.sampleRate;
    }

    @Override
    public int getBitsPerSample() {
        return this.bitsPerSample;
    }

    @Override
    public Optional<ALFormat> getALFormat() {
        switch (this.format) {
            case PCM:
                switch (this.channels) {
                    case 1:
                        switch (this.bitsPerSample) {
                            case 8:
                                return Optional.of(ALFormat.MONO8);
                            case 16:
                                return Optional.of(ALFormat.MONO16);
                            default:
                                return Optional.empty();
                        }
                    case 2:
                        switch (this.bitsPerSample) {
                            case 8:
                                return Optional.of(ALFormat.STEREO8);
                            case 16:
                                return Optional.of(ALFormat.STEREO16);
                            default:
                                return Optional.empty();
                        }
                    default:
                        return Optional.empty();
                }
            case FLOAT:
                switch (this.channels) {
                    case 1:
                        switch (this.bitsPerSample) {
                            case 32:
                                return Optional.of(ALFormat.MONO_FLOAT32);
                            case 64:
                                return Optional.of(ALFormat.STEREO_FLOAT32);
                            default:
                                return Optional.empty();
                        }
                    case 2:
                        switch (this.bitsPerSample) {
                            case 32:
                                return Optional.of(ALFormat.STEREO_FLOAT32);
                            case 64:
                                return Optional.of(ALFormat.STEREO_DOUBLE);
                            default:
                                return Optional.empty();
                        }
                    default:
                        return Optional.empty();
                }
            case ALAW:
                switch (this.channels) {
                    case 1:
                        return Optional.of(ALFormat.MONO_ALAW);
                    case 2:
                        return Optional.of(ALFormat.STEREO_ALAW);
                    default:
                        return Optional.empty();
                }
            case MULAW:
                switch (this.channels) {
                    case 1:
                        return Optional.of(ALFormat.MONO_MULAW);
                    case 2:
                        return Optional.of(ALFormat.STEREO_MULAW);
                    default:
                        return Optional.empty();
                }
            default:
                return Optional.empty();
        }
    }

    @Override
    public ByteOrder getByteOrder() {
        return ByteOrder.LITTLE_ENDIAN;
    }

    @Override
    public int read(ByteBuffer dst) throws IOException {
        return this.base.read(dst);
    }

    @Override
    public boolean isOpen() {
        return this.base.isOpen();
    }

    @Override
    public void close() throws IOException {
        this.base.close();
    }

    private boolean parseSubchunk() throws IOException {
        final ByteBuffer header = ByteBuffer.allocate(8);

        while (header.hasRemaining()) {
            if (this.base.read(header) == -1) {
                throw new EOFException();
            }
        }

        header.flip();

        final int chunkId = header.order(ByteOrder.BIG_ENDIAN).getInt();
        final int chunkSize = header.order(ByteOrder.LITTLE_ENDIAN).getInt();

        switch (chunkId) {
            case 0x666d7420:
                parseFormatSubchunk(chunkSize);
                return false;
            case 0x64617461:
                this.size = chunkSize;
                return true;
            default:
                LOGGER.debug("Unknown chunk Id: {}", chunkId);
                skipSubchunk(chunkSize);
                return false;
        }
    }

    private void skipSubchunk(int chunkSize) throws IOException {
        final ByteBuffer data = ByteBuffer.allocate(chunkSize);

        while (data.hasRemaining()) {
            if (base.read(data) == -1) {
                throw new EOFException();
            }
        }
    }

    private void parseFormatSubchunk(int chunkSize) throws IOException {
        final ByteBuffer data = ByteBuffer.allocate(chunkSize).order(ByteOrder.LITTLE_ENDIAN);

        while (data.hasRemaining()) {
            if (base.read(data) == -1) {
                throw new EOFException();
            }
        }

        data.flip();

        int audioFormat = data.getShort();

        this.channels = data.getShort();
        this.sampleRate = data.getInt();
        this.byteRate = data.getInt();
        this.blockAlign = data.getShort();
        this.bitsPerSample = data.getShort();

        if (audioFormat != WaveAudioFormat.PCM.value) {
            final int extSize = data.getShort();

            if (extSize == 22) {
                this.validBitsPerSample = data.getShort();
                this.channelMask = data.getInt();

                audioFormat = data.getShort();
            }
        }

        this.format = WaveAudioFormat.of(audioFormat).orElseThrow(() -> new IOException("Unsupported AudioFormat!"));
    }
}
