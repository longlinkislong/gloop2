/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 * Audio formats supported by Wave.
 * @author zmichaels
 */
public enum WaveAudioFormat implements CEnum {
    /**
     * PCM data; either unsigned 8bit or signed 16bit little endian.
     */
    PCM(0x0001),
    /**
     * PCM data stored as either 32 or 64bit little endian IEEE754 floats
     */
    FLOAT(0x0003),
    /**
     * Logarithmic PCM data using a-law.
     */
    ALAW(0x0006),
    /**
     * Logarithmic PCM data using mu-law.
     */
    MULAW(0x0007),
    EXTENSIBLE(-2);

    public final int value;

    private WaveAudioFormat(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<WaveAudioFormat> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

}
