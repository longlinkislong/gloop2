/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.al.ALFormat;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Optional;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An implementation of SeekableSoundChannel that stream decodes sound memory
 * encoded with Microsoft wave PCM.
 *
 * @author zmichaels
 */
public class WaveMemoryChannel implements SeekableSoundChannel {

    private static final Logger LOGGER = LoggerFactory.getLogger(WaveFileChannel.class);

    private final ByteBuffer base;
    private WaveAudioFormat format;
    private int channels;
    private int sampleRate;
    private int byteRate;
    private int blockAlign;
    private int bitsPerSample;
    private int size;
    private int validBitsPerSample;
    private int channelMask;
    private int dataStart;

    public WaveMemoryChannel(final ByteBuffer data) throws IOException {
        this.base = data.asReadOnlyBuffer().order(data.order());

        final int chunkId = this.base.order(ByteOrder.BIG_ENDIAN).getInt();

        if (chunkId != 0x52494646) {
            throw new IOException("Container is not RIFF!");
        }

        final int chunkSize = this.base.order(ByteOrder.LITTLE_ENDIAN).getInt();
        final int format = this.base.order(ByteOrder.BIG_ENDIAN).getInt();

        if (format != 0x57415645) {
            throw new IOException("Audio format is not WAVE!");
        }

        while (!parseSubchunk()) {

        }

        // WAVE is limited to 2GB so this isn't really a concern
        this.dataStart = (int) this.base.position();
    }

    @Override
    public void seekStart() {
        this.base.position(this.dataStart);
    }

    @Override
    public void seek(int sample) throws IOException {
        this.base.position(this.channels * this.bitsPerSample / 8 * sample);
    }

    @Override
    public float getLength() {
        return ((float) this.size) / ((float) this.byteRate);
    }

    @Override
    public int getChannels() {
        return this.channels;
    }

    @Override
    public int getSampleRate() {
        return this.sampleRate;
    }

    @Override
    public int getBitsPerSample() {
        return this.bitsPerSample;
    }

    @Override
    public Optional<ALFormat> getALFormat() {
        switch (this.format) {
            case PCM:
                switch (this.channels) {
                    case 1:
                        switch (this.bitsPerSample) {
                            case 8:
                                return Optional.of(ALFormat.MONO8);
                            case 16:
                                return Optional.of(ALFormat.MONO16);
                            default:
                                return Optional.empty();
                        }
                    case 2:
                        switch (this.bitsPerSample) {
                            case 8:
                                return Optional.of(ALFormat.STEREO8);
                            case 16:
                                return Optional.of(ALFormat.STEREO16);
                            default:
                                return Optional.empty();
                        }
                    default:
                        return Optional.empty();
                }
            case FLOAT:
                switch (this.channels) {
                    case 1:
                        switch (this.bitsPerSample) {
                            case 32:
                                return Optional.of(ALFormat.MONO_FLOAT32);
                            case 64:
                                return Optional.of(ALFormat.STEREO_FLOAT32);
                            default:
                                return Optional.empty();
                        }
                    case 2:
                        switch (this.bitsPerSample) {
                            case 32:
                                return Optional.of(ALFormat.STEREO_FLOAT32);
                            case 64:
                                return Optional.of(ALFormat.STEREO_DOUBLE);
                            default:
                                return Optional.empty();
                        }
                    default:
                        return Optional.empty();
                }
            case ALAW:
                switch (this.channels) {
                    case 1:
                        return Optional.of(ALFormat.MONO_ALAW);
                    case 2:
                        return Optional.of(ALFormat.STEREO_ALAW);
                    default:
                        return Optional.empty();
                }
            case MULAW:
                switch (this.channels) {
                    case 1:
                        return Optional.of(ALFormat.MONO_MULAW);
                    case 2:
                        return Optional.of(ALFormat.STEREO_MULAW);
                    default:
                        return Optional.empty();
                }
            default:
                return Optional.empty();
        }
    }

    @Override
    public ByteOrder getByteOrder() {
        return ByteOrder.LITTLE_ENDIAN;
    }

    @Override
    public int read(ByteBuffer dst) throws IOException {
        if (this.base.remaining() < dst.remaining()) {
            dst.put(this.base);
            return -1;
        }

        final int len = dst.remaining();
        final ByteBuffer sub = MemoryUtil.memSlice(base, 0, len);

        dst.put(sub);
        
        base.position(base.position() + len);

        return len;
    }

    @Override
    public boolean isOpen() {
        return true;
    }

    @Override
    public void close() {
    }

    private boolean parseSubchunk() throws IOException {
        final int chunkId = this.base.order(ByteOrder.BIG_ENDIAN).getInt();
        final int chunkSize = this.base.order(ByteOrder.LITTLE_ENDIAN).getInt();

        switch (chunkId) {
            case 0x666d7420:
                parseFormatSubchunk(chunkSize);
                return false;
            case 0x64617461:
                this.size = chunkSize;
                return true;
            default:
                LOGGER.debug("Unknown chunk Id: {}", chunkId);
                skipSubchunk(chunkSize);
                return false;
        }
    }

    private void skipSubchunk(int chunkSize) throws IOException {
        this.base.position(this.base.position() + chunkSize);
    }

    private void parseFormatSubchunk(int chunkSize) throws IOException {
        int audioFormat = this.base.getShort();

        this.channels = this.base.getShort();
        this.sampleRate = this.base.getInt();
        this.byteRate = this.base.getInt();
        this.blockAlign = this.base.getShort();
        this.bitsPerSample = this.base.getShort();

        if (audioFormat != WaveAudioFormat.PCM.value) {
            final int extSize = this.base.getShort();

            if (extSize == 22) {
                this.validBitsPerSample = this.base.getShort();
                this.channelMask = this.base.getInt();

                audioFormat = this.base.getShort();
            }
        }

        this.format = WaveAudioFormat.of(audioFormat).orElseThrow(() -> new IOException("Unsupported AudioFormat!"));
    }
}
