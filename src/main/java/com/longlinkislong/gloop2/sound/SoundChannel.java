/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.al.ALFormat;
import java.nio.ByteOrder;
import java.nio.channels.ReadableByteChannel;
import java.util.Optional;

/**
 * A ByteChannel that represents a SoundFile.
 * @author zmichaels
 */
public interface SoundChannel extends ReadableByteChannel {
    int getChannels();
    
    int getSampleRate();
    
    int getBitsPerSample();
    
    default int getByteRate() {
        return getSampleRate() * getChannels() * (getBitsPerSample() / Byte.SIZE);
    }
    
    Optional<ALFormat> getALFormat();
    
    ByteOrder getByteOrder();
}
