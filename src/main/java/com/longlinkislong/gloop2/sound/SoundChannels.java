/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import org.lwjgl.system.MemoryStack;

/**
 * Utility class for SoundChannel objects.
 *
 * @author zmichaels
 */
public final class SoundChannels {

    /**
     * Attempts to open a SoundChannel and pick the correct decoder.
     *
     * @param path the Path to the sound object.
     * @return the SoundChannel interface to the decoded sound.
     * @throws IOException if the SoundChannel could not be opened.
     */
    public static SeekableSoundChannel open(final Path path) throws IOException {
        final int magic;

        try (ReadableByteChannel bc = Files.newByteChannel(path, StandardOpenOption.READ);
                MemoryStack mem = MemoryStack.stackPush()) {

            final ByteBuffer bMagic = mem.calloc(Integer.BYTES);

            while (bMagic.hasRemaining()) {
                bc.read(bMagic);
            }

            magic = bMagic.order(ByteOrder.BIG_ENDIAN).getInt(0);
        }

        switch (magic) {
            case 1332176723:
                return new VorbisFileChannel(path);
            case 0x52494646:
                return new WaveFileChannel(path);
            default:
                throw new IOException("Only OGG and RIFF containers are supported!");
        }
    }

    /**
     * Attempts to open a SoundChannel and pick the correct decoder.
     *
     * @param data the data of the entire sound object.
     * @return the SoundChannel interface to the decoded sound.
     * @throws IOException if the SoundChannel could not be opened.
     */
    public static SeekableSoundChannel open(final ByteBuffer data) throws IOException {
        final ByteOrder order = data.order();

        final int magic = data.order(ByteOrder.BIG_ENDIAN).getInt(0);

        data.order(order);

        switch (magic) {
            case 1332176723:
                return new VorbisMemoryChannel(data);
            case 0x52494646:
                return new WaveMemoryChannel(data);
            default:
                throw new IOException("Only OGG and RIFF containers are supported!");
        }
    }
}
