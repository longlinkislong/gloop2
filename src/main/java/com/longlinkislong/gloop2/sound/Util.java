/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import java.io.IOException;
import org.lwjgl.stb.STBVorbis;

/**
 * Internal utility class. 
 * @author zmichaels
 */
final class Util {
    private Util() {}

    static IOException translateVorbisError(int err) {
        switch (err) {
            case STBVorbis.VORBIS_bad_packet_type:
                return new IOException("Bad packet type");
            case STBVorbis.VORBIS_cant_find_last_page:
                return new IOException("Cant find last page");
            case STBVorbis.VORBIS_continued_packet_flag_invalid:
                return new IOException("Continued packet flag invalid");
            case STBVorbis.VORBIS_feature_not_supported:
                return new IOException("Feature not supported");
            case STBVorbis.VORBIS_file_open_failure:
                return new IOException("File open failure");
            case STBVorbis.VORBIS_incorrect_stream_serial_number:
                return new IOException("Incorrect stream serial number");
            case STBVorbis.VORBIS_invalid_api_mixing:
                return new IOException("Invalid API mixing");
            case STBVorbis.VORBIS_invalid_first_page:
                return new IOException("Invalid first page");
            case STBVorbis.VORBIS_invalid_setup:
                return new IOException("Invalid setup");
            case STBVorbis.VORBIS_invalid_stream:
                return new IOException("Invalid stream");
            case STBVorbis.VORBIS_invalid_stream_structure_version:
                return new IOException("Invalid stream structure version");
            case STBVorbis.VORBIS_missing_capture_pattern:
                return new IOException("Missing capture pattern");
            case STBVorbis.VORBIS_outofmem:
                return new IOException("Out of memory");
            case STBVorbis.VORBIS_seek_failed:
                return new IOException("Seek failed");
            case STBVorbis.VORBIS_seek_invalid:
                return new IOException("Seek invalid");
            case STBVorbis.VORBIS_seek_without_length:
                return new IOException("Seek without length");
            case STBVorbis.VORBIS_too_many_channels:
                return new IOException("Too many channels");
            case STBVorbis.VORBIS_unexpected_eof:
                return new IOException("Unexpected EOF");
            default:
                return null;
        }
    }
}
