/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.al.ALFormat;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Optional;
import org.lwjgl.system.MemoryUtil;

/**
 * Wrapper channel for converting from one format to another Currently only
 * supports FLOAT to SIGNED_SHORT.
 *
 * @author zmichaels
 */
public class FormatConversionChannel implements SoundChannel {

    private static enum ConversionType {
        FLOAT32_TO_SHORT
    }

    private final SoundChannel base;
    private final ALFormat format;
    private final ConversionType convType;

    /**
     * Wraps a base SoundChannel
     *
     * @param base the base SoundChannel.
     * @param format the target format.
     * @throws IOException if the base SoundChannel doesn't have a compatible OpenAL format.
     */
    public FormatConversionChannel(final SoundChannel base, final ALFormat format) throws IOException {
        this.base = base;
        this.format = format;

        final ALFormat srcFmt = base.getALFormat().orElseThrow(() -> new IOException("Base SoundChannel doesn't support a valid OpenAL format!"));

        if ((srcFmt == ALFormat.STEREO_FLOAT32 && format == ALFormat.STEREO16)
                || (srcFmt == ALFormat.MONO_FLOAT32 && format == ALFormat.MONO16)) {

            this.convType = ConversionType.FLOAT32_TO_SHORT;
        } else {
            throw new IllegalArgumentException("Incompatible conversion type: " + srcFmt + " -> " + format);
        }
    }

    @Override
    public int getChannels() {
        return this.base.getChannels();
    }

    @Override
    public int getSampleRate() {
        return this.base.getSampleRate();
    }

    @Override
    public int getBitsPerSample() {
        return this.base.getBitsPerSample();
    }

    @Override
    public Optional<ALFormat> getALFormat() {
        return Optional.of(this.format);
    }

    @Override
    public ByteOrder getByteOrder() {
        return this.base.getByteOrder();
    }

    private int clamp(int val, int min, int max) {
        if (val < min) {
            return min;
        } else if (val > max) {
            return max;
        } else {
            return val;
        }
    }

    private int float32ToShort(ByteBuffer dst) throws IOException {
        final ByteBuffer tmp = MemoryUtil.memAlloc(dst.remaining() * 2);
        final int read;

        try {
            read = this.base.read(tmp);
            tmp.flip();

            while (tmp.hasRemaining()) {
                dst.putShort((short) (clamp((int) (tmp.getFloat() * 32768), Short.MIN_VALUE, Short.MAX_VALUE)));
            }
        } finally {
            MemoryUtil.memFree(tmp);
        }

        return read;
    }

    @Override
    public int read(ByteBuffer dst) throws IOException {
        switch (this.convType) {
            case FLOAT32_TO_SHORT:
                return float32ToShort(dst);
            default:
                throw new UnsupportedOperationException("Unsupported conversion type: " + this.convType);
        }
    }

    @Override
    public boolean isOpen() {
        return this.base.isOpen();
    }

    @Override
    public void close() throws IOException {
        this.base.close();
    }

}
