/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.al.ALFormat;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Optional;
import org.lwjgl.PointerBuffer;
import org.lwjgl.stb.STBVorbis;
import org.lwjgl.stb.STBVorbisInfo;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An implementation of SoundChannel that stream decodes Vorbis frames.
 *
 * @author zmichaels
 */
public class VorbisChannel implements SoundChannel {

    private static final Logger LOGGER = LoggerFactory.getLogger(VorbisChannel.class);

    private final ReadableByteChannel base;
    private long stbHandle;
    private final int sampleRate;
    private final int channels;
    private ByteBuffer block;
    private ByteBuffer buffer;

    @Override
    public ByteOrder getByteOrder() {
        return ByteOrder.nativeOrder();
    }

    @Override
    public Optional<ALFormat> getALFormat() {
        switch (channels) {
            case 1:
                return Optional.of(ALFormat.MONO_FLOAT32);
            case 2:
                return Optional.of(ALFormat.STEREO_FLOAT32);
            default:
                return Optional.empty();
        }
    }

    @Override
    public int getBitsPerSample() {
        return Float.SIZE;
    }

    @Override
    public int getSampleRate() {
        return this.sampleRate;
    }

    @Override
    public int getChannels() {
        return this.channels;
    }

    public VorbisChannel(final InputStream in) throws IOException {
        this(Channels.newChannel(in));
    }

    public VorbisChannel(final ReadableByteChannel bc) throws IOException {
        this.base = bc;

        int bufferSize = 4096;
        block = MemoryUtil.memAlloc(bufferSize);

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pRead = mem.callocInt(1);
            final IntBuffer pError = mem.callocInt(1);

            while (true) {
                while (block.hasRemaining()) {
                    if (bc.read(block) == -1) {
                        throw new EOFException();
                    }
                }

                block.flip();

                final long handle = STBVorbis.stb_vorbis_open_pushdata(block, pRead, pError, null);

                if (handle == MemoryUtil.NULL) {
                    final int err = pError.get(0);

                    if (err == STBVorbis.VORBIS_need_more_data) {
                        bufferSize += 4096;
                        final ByteBuffer newBlock = MemoryUtil.memAlloc(bufferSize);

                        newBlock.put(block);

                        MemoryUtil.memFree(block);
                        block = newBlock;

                        pRead.put(0, 0);
                        pError.put(0, 0);
                    } else {
                        throw new IOException(Util.translateVorbisError(err));
                    }
                } else {
                    this.stbHandle = handle;
                    break;
                }
            }

            final STBVorbisInfo info = STBVorbisInfo.callocStack(mem);

            STBVorbis.stb_vorbis_get_info(stbHandle, info);

            this.sampleRate = info.sample_rate();
            this.channels = info.channels();

            // advance the read buffer
            this.block.position(pRead.get());
            this.block.compact();
            this.buffer = MemoryUtil.memAlloc(8 * 1024);

            LOGGER.debug("Block size: {}KB", (this.block.capacity() / 1024.0));
        }
    }

    private int decode() throws IOException {
        this.block.flip();

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pSamples = mem.callocInt(1);
            final IntBuffer pChannels = mem.callocInt(1);
            final PointerBuffer ppOutput = mem.callocPointer(1);
            final int read = STBVorbis.stb_vorbis_decode_frame_pushdata(stbHandle, block, pChannels, ppOutput, pSamples);

            block.position(block.position() + read);
            block.compact();

            if (read == 0) {
                // not enough data; read more and report bytes read.
                return this.base.read(block);
            }

            final int samples = pSamples.get();

            if (samples == 0) {
                // frame resync; read more and report bytes read.
                return this.base.read(block);
            } else {
                final int channels = pChannels.get();
                final PointerBuffer pOutput = ppOutput.getPointerBuffer(1);

                if (channels == 1) {
                    final ByteBuffer mono = pOutput.getByteBuffer(samples * Float.BYTES);
                    final int neededSize = mono.remaining();

                    // expand the internal buffer if needed.
                    if (this.buffer.remaining() < neededSize) {
                        final int newBufferSize = (buffer.capacity() + neededSize + 4096 - 1) / 4096 * 4096;
                        final ByteBuffer newBuffer = MemoryUtil.memAlloc(newBufferSize);

                        this.buffer.flip();
                        newBuffer.put(this.buffer);
                        MemoryUtil.memFree(this.buffer);
                        this.buffer = newBuffer;
                    }

                    this.buffer.put(mono);
                } else if (channels == 2) {
                    final ByteBuffer left = pOutput.getByteBuffer(0, samples * Float.BYTES);
                    final ByteBuffer right = pOutput.getByteBuffer(1, samples * Float.BYTES);
                    final int neededSize = left.remaining() + right.remaining();

                    if (this.buffer.remaining() < neededSize) {
                        final int newBufferSize = (buffer.capacity() + neededSize + 4096 - 1) / 4096 * 4096;
                        final ByteBuffer newBuffer = MemoryUtil.memAlloc(newBufferSize);

                        this.buffer.flip();
                        newBuffer.put(this.buffer);
                        MemoryUtil.memFree(this.buffer);
                        this.buffer = newBuffer;
                    }

                    // interlace the streams
                    while (left.hasRemaining() && right.hasRemaining()) {
                        this.buffer.putFloat(left.getFloat());
                        this.buffer.putFloat(right.getFloat());
                    }
                }
            }
        }

        return 0;
    }

    @Override
    public int read(ByteBuffer dst) throws IOException {
        int totalRead = 0;

        while (dst.hasRemaining()) {
            this.buffer.flip();

            final int len = Math.min(this.buffer.remaining(), dst.remaining());
            final ByteBuffer src = this.buffer.slice();

            src.limit(len);
            dst.put(src);

            this.buffer.position(this.buffer.position() + len);

            if (!this.buffer.hasRemaining()) {
                if (totalRead == -1) {
                    break;
                }

                this.buffer.clear();
                final int read = this.decode();

                if (read == -1) {
                    totalRead = -1;
                } else {
                    totalRead += read;
                }
            }
        }

        return totalRead;
    }

    @Override
    public boolean isOpen() {
        return base.isOpen();
    }

    @Override
    public void close() throws IOException {
        if (this.isOpen()) {
            STBVorbis.stb_vorbis_close(this.stbHandle);
            MemoryUtil.memFree(this.block);
            this.base.close();
        }
    }
}
