/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.al.ALFormat;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.file.Path;
import java.util.Optional;
import org.lwjgl.stb.STBVorbis;
import org.lwjgl.stb.STBVorbisInfo;
import org.lwjgl.system.MemoryStack;

/**
 * An implementation of SeekableSoundChannel that stream decodes an Ogg/Vorbis
 * sound file.
 *
 * @author zmichaels
 */
public class VorbisFileChannel implements SeekableSoundChannel {

    private final long handle;
    private final int channels;
    private final int sampleRate;
    private final ALFormat format;
    private boolean isOpen;
    private final float time;

    public VorbisFileChannel(final Path path) throws IOException {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final ByteBuffer pathStr = mem.UTF8(path.toAbsolutePath().toString(), true);
            final IntBuffer pError = mem.callocInt(1);

            this.handle = STBVorbis.stb_vorbis_open_filename(pathStr, pError, null);

            final STBVorbisInfo info = STBVorbis.stb_vorbis_get_info(handle, STBVorbisInfo.callocStack(mem));

            this.channels = info.channels();
            this.sampleRate = info.sample_rate();
            this.isOpen = true;
        }

        this.time = STBVorbis.stb_vorbis_stream_length_in_seconds(handle);

        switch (this.channels) {
            case 1:
                this.format = ALFormat.MONO_FLOAT32;
                break;
            case 2:
                this.format = ALFormat.STEREO_FLOAT32;
                break;
            default:
                throw new IOException("Only 1 or 2 channel Vorbis is currently supported!");
        }
    }

    @Override
    public float getLength() {
        return this.time;
    }

    @Override
    public int getChannels() {
        return this.channels;
    }

    @Override
    public int getSampleRate() {
        return this.sampleRate;
    }

    @Override
    public int getBitsPerSample() {
        return 32;
    }

    @Override
    public Optional<ALFormat> getALFormat() {
        return Optional.of(this.format);
    }

    @Override
    public ByteOrder getByteOrder() {
        return ByteOrder.nativeOrder();
    }

    @Override
    public int read(ByteBuffer dst) throws IOException {
        final int read;

        read = STBVorbis.stb_vorbis_get_samples_float_interleaved(handle, this.channels, dst.asFloatBuffer());
        dst.position(dst.position() + read * Float.BYTES * this.channels);

        return (read == 0) ? -1 : read;
    }

    @Override
    public boolean isOpen() {
        return this.isOpen;
    }

    @Override
    public void close() throws IOException {
        if (this.isOpen) {
            STBVorbis.stb_vorbis_close(handle);
            this.isOpen = false;
        }
    }

    @Override
    public void seek(final int sample) throws IOException {
        if (this.isOpen()) {
            STBVorbis.stb_vorbis_seek(handle, sample);
        } else {
            throw new IOException("VorbisFileChannel has already closed!");
        }
    }

    @Override
    public void seekStart() throws IOException {
        if (this.isOpen()) {
            STBVorbis.stb_vorbis_seek_start(handle);
        } else {
            throw new IOException("VorbisFileChannel has already closed!");
        }
    }

}
