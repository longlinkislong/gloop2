/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.sound;

import com.longlinkislong.gloop2.al.ALFormat;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An implementation of SoundChannel that stream decodes Wave PCM data. This
 * implementation of SoundChannel has nearly no overhead.
 *
 * @author zmichaels
 */
public class WaveChannel implements SoundChannel {

    private static final Logger LOGGER = LoggerFactory.getLogger(WaveChannel.class);

    private final ReadableByteChannel base;

    private WaveAudioFormat format;
    private int channels;
    private int sampleRate;
    private int byteRate;
    private int blockAlign;
    private int bitsPerSample;
    private int size;
    private int validBitsPerSample;
    private int channelMask;

    @Override
    public ByteOrder getByteOrder() {
        return ByteOrder.LITTLE_ENDIAN;
    }

    @Override
    public String toString() {
        return String.format("Wave [Channels: %d Bits per Sample: %d SampleRate: %d Size: %d]", channels, bitsPerSample, sampleRate, size);
    }

    public int getValidBitsPerSample() {
        return this.validBitsPerSample;
    }

    public int getChannelMask() {
        return this.channelMask;
    }

    @Override
    public int getChannels() {
        return this.channels;
    }

    @Override
    public int getSampleRate() {
        return this.sampleRate;
    }

    @Override
    public int getByteRate() {
        return this.byteRate;
    }

    public int getBlockAlign() {
        return this.blockAlign;
    }

    @Override
    public int getBitsPerSample() {
        return this.bitsPerSample;
    }

    public int getSize() {
        return this.size;
    }

    @Override
    public Optional<ALFormat> getALFormat() {
        switch (this.format) {
            case PCM:
                switch (this.channels) {
                    case 1:
                        switch (this.bitsPerSample) {
                            case 8:
                                return Optional.of(ALFormat.MONO8);
                            case 16:
                                return Optional.of(ALFormat.MONO16);
                            default:
                                return Optional.empty();
                        }
                    case 2:
                        switch (this.bitsPerSample) {
                            case 8:
                                return Optional.of(ALFormat.STEREO8);
                            case 16:
                                return Optional.of(ALFormat.STEREO16);
                            default:
                                return Optional.empty();
                        }
                    default:
                        return Optional.empty();
                }
            case FLOAT:
                switch (this.channels) {
                    case 1:
                        switch (this.bitsPerSample) {
                            case 32:
                                return Optional.of(ALFormat.MONO_FLOAT32);
                            case 64:
                                return Optional.of(ALFormat.STEREO_FLOAT32);
                            default:
                                return Optional.empty();
                        }
                    case 2:
                        switch (this.bitsPerSample) {
                            case 32:
                                return Optional.of(ALFormat.STEREO_FLOAT32);
                            case 64:
                                return Optional.of(ALFormat.STEREO_DOUBLE);
                            default:
                                return Optional.empty();
                        }
                    default:
                        return Optional.empty();
                }
            case ALAW:
                switch (this.channels) {
                    case 1:
                        return Optional.of(ALFormat.MONO_ALAW);
                    case 2:
                        return Optional.of(ALFormat.STEREO_ALAW);
                    default:
                        return Optional.empty();
                }
            case MULAW:
                switch (this.channels) {
                    case 1:
                        return Optional.of(ALFormat.MONO_MULAW);
                    case 2:
                        return Optional.of(ALFormat.STEREO_MULAW);
                    default:
                        return Optional.empty();
                }
            default:
                return Optional.empty();
        }
    }

    public WaveChannel(final InputStream in) throws IOException {
        this(Channels.newChannel(in));
    }

    private boolean parseSubchunk() throws IOException {
        final ByteBuffer header = ByteBuffer.allocate(8);

        while (header.hasRemaining()) {
            if (this.base.read(header) == -1) {
                throw new EOFException();
            }
        }

        header.flip();

        final int chunkId = header.order(ByteOrder.BIG_ENDIAN).getInt();
        final int chunkSize = header.order(ByteOrder.LITTLE_ENDIAN).getInt();

        switch (chunkId) {
            case 0x666d7420:
                parseFormatSubchunk(chunkSize);
                return false;
            case 0x64617461:
                this.size = chunkSize;
                return true;
            default:
                LOGGER.debug("Unknown chunk Id: {}", chunkId);
                skipSubchunk(chunkSize);
                return false;
        }
    }

    private void skipSubchunk(int chunkSize) throws IOException {
        final ByteBuffer data = ByteBuffer.allocate(chunkSize);

        while (data.hasRemaining()) {
            if (base.read(data) == -1) {
                throw new EOFException();
            }
        }
    }

    private void parseFormatSubchunk(int chunkSize) throws IOException {
        final ByteBuffer data = ByteBuffer.allocate(chunkSize).order(ByteOrder.LITTLE_ENDIAN);

        while (data.hasRemaining()) {
            if (base.read(data) == -1) {
                throw new EOFException();
            }
        }

        data.flip();

        int audioFormat = data.getShort();

        this.channels = data.getShort();
        this.sampleRate = data.getInt();
        this.byteRate = data.getInt();
        this.blockAlign = data.getShort();
        this.bitsPerSample = data.getShort();

        if (audioFormat != WaveAudioFormat.PCM.value) {
            final int extSize = data.getShort();

            if (extSize == 22) {
                this.validBitsPerSample = data.getShort();
                this.channelMask = data.getInt();

                audioFormat = data.getShort();
            }
        }

        this.format = WaveAudioFormat.of(audioFormat).orElseThrow(() -> new IOException("Unsupported AudioFormat!"));
    }

    public WaveChannel(final ReadableByteChannel bc) throws IOException {
        this.base = bc;

        final ByteBuffer riffHeader = ByteBuffer.allocate(12);

        while (riffHeader.hasRemaining()) {
            if (bc.read(riffHeader) == -1) {
                throw new EOFException("Incorrect header; expecting RIFF!");
            }
        }

        riffHeader.flip();

        final int chunkId = riffHeader.order(ByteOrder.BIG_ENDIAN).getInt();

        if (chunkId != 0x52494646) {
            throw new IOException("Container is not RIFF!");
        }

        final int chunkSize = riffHeader.order(ByteOrder.LITTLE_ENDIAN).getInt();
        final int format = riffHeader.order(ByteOrder.BIG_ENDIAN).getInt();

        if (format != 0x57415645) {
            throw new IOException("Audio format is not WAVE!");
        }

        while (!parseSubchunk()) {

        }
    }

    @Override
    public int read(ByteBuffer dst) throws IOException {
        return this.base.read(dst);
    }

    @Override
    public boolean isOpen() {
        return this.base.isOpen();
    }

    @Override
    public void close() throws IOException {
        this.base.close();
    }
}
