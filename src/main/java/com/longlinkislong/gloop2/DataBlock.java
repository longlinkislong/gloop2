/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public final class DataBlock implements AutoCloseable {

    private static final int BUFFER_SIZE = Integer.getInteger("com.longlinkislong.gloop2.DataBlock.buffer_size", MemoryUtil.PAGE_SIZE * 2);
    
    public final ByteBuffer data;
    private boolean isValid;
    
    private DataBlock(final ByteBuffer data) {
        this.data = data;
        this.isValid = true;
    }
    
    public static DataBlock malloc(final int bytes) {
        return new DataBlock(MemoryUtil.memAlloc(bytes));
    }
    
    public static DataBlock calloc(final int num, final int size) {
        return new DataBlock(MemoryUtil.memCalloc(num, size));
    }    
    
    public static DataBlock of(final Path path) throws IOException {
        try (ReadableByteChannel bc = Files.newByteChannel(path, StandardOpenOption.READ)) {
            return of(bc);
        }
    }
    
    public static DataBlock of(final InputStream in) throws IOException {
        try (ReadableByteChannel bc = Channels.newChannel(in)) {
            return of(bc);
        }
    }
    
    public static DataBlock of(final ReadableByteChannel bc) throws IOException {
        ByteBuffer tmp = MemoryUtil.memAlloc(BUFFER_SIZE);
        
        try {
            while (bc.read(tmp) != -1) {
                if (tmp.remaining() == 0) {
                    final ByteBuffer newTmp = MemoryUtil.memAlloc(tmp.capacity() * 2);
                    
                    tmp.flip();
                    newTmp.put(tmp);
                    
                    MemoryUtil.memFree(tmp);
                    tmp = newTmp;
                }
            }
            
            tmp.flip();
            
            return new DataBlock(tmp);
        } catch (IOException ex) {
            MemoryUtil.memFree(tmp);
            
            throw ex;
        }
    }
    
    @Override
    public void close() {
        if (this.isValid) {
            MemoryUtil.memFree(this.data);
            this.isValid = false;
        }
    }
    
}
