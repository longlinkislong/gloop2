/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

/**
 *
 * @author zmichaels
 */
public interface GLFWCharCallback {
    void onChar(GLFWWindow window, int codepoint);
    
    default void glfwCharCallback(long window, int codepoint) {
        onChar(new GLFWWindow(window), codepoint);
    }
}
