/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

/**
 *
 * @author zmichaels
 */
public interface GLFWCursorEnterCallback {
    void onCursorEnter(GLFWWindow window);
    
    void onCursorExit(GLFWWindow window);
    
    default void glfwCursorEnterCallback(long window, boolean entered) {
        if (entered) {
            this.onCursorEnter(new GLFWWindow(window));
        } else {
            this.onCursorExit(new GLFWWindow(window));
        }
    }
}
