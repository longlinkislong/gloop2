/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import java.util.Optional;
import org.lwjgl.glfw.GLFW;

/**
 *
 * @author zmichaels
 */
public enum GLFWMouseButtonAction {
    PRESS(GLFW.GLFW_PRESS),
    RELEASE(GLFW.GLFW_RELEASE);
    
    final int value;
    
    private GLFWMouseButtonAction(final int value) {
        this.value = value;
    }
    
    public static Optional<GLFWMouseButtonAction> of(final int value) {
        switch (value) {
            case GLFW.GLFW_PRESS:
                return Optional.of(PRESS);
            case GLFW.GLFW_RELEASE:
                return Optional.of(RELEASE);
            default:
                return Optional.empty();
        }
    }
}
