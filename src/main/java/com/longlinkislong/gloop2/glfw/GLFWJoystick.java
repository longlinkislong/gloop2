/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.glfw.GLFW;

/**
 *
 * @author zmichaels
 */
public class GLFWJoystick {
    private int id = -1;
    
    GLFWJoystick(final int id) {
        this.id = id;
    }
    
    public static List<GLFWJoystick> getJoysticksPresent() {
        final List<GLFWJoystick> out = new ArrayList<>();
        
        for (int i = GLFW.GLFW_JOYSTICK_1; i <= GLFW.GLFW_JOYSTICK_LAST; i++) {
            if (GLFW.glfwJoystickPresent(i)) {
                out.add(new GLFWJoystick(i));
            }
        }
        
        return out;
    }
    
    public boolean isValid() {
        return this.id >= 0;
    }
    
    public GLFWJoystickHatState[] getHats() {
        assert this.isValid() : "GLFWJoystick is not valid!";
        
        final ByteBuffer rawHats = GLFW.glfwGetJoystickHats(id);
        final GLFWJoystickHatState[] out = new GLFWJoystickHatState[rawHats.capacity()];
        
        for (int i = 0; i < rawHats.capacity(); i++) {            
            switch (rawHats.get(i)) {
                case GLFW.GLFW_HAT_CENTERED:
                    out[i] = GLFWJoystickHatState.CENTERED;
                    break;
                case GLFW.GLFW_HAT_UP:
                    out[i] = GLFWJoystickHatState.UP;
                    break;
                case GLFW.GLFW_HAT_RIGHT:
                    out[i] = GLFWJoystickHatState.RIGHT;
                    break;
                case GLFW.GLFW_HAT_DOWN:
                    out[i] = GLFWJoystickHatState.DOWN;
                    break;
                case GLFW.GLFW_HAT_LEFT:
                    out[i] = GLFWJoystickHatState.LEFT;
                    break;
                case GLFW.GLFW_HAT_RIGHT_UP:
                    out[i] = GLFWJoystickHatState.RIGHT_UP;
                    break;
                case GLFW.GLFW_HAT_RIGHT_DOWN:
                    out[i] = GLFWJoystickHatState.RIGHT_DOWN;
                    break;
                case GLFW.GLFW_HAT_LEFT_UP:
                    out[i] = GLFWJoystickHatState.LEFT_UP;
                    break;
                case GLFW.GLFW_HAT_LEFT_DOWN:
                    out[i] = GLFWJoystickHatState.LEFT_DOWN;
                    break;
                default:
                    out[i] = GLFWJoystickHatState.UNKNOWN;
            }
        }        
                
        return out;
    }    
    
    public float[] getAxes() {
        assert this.isValid() : "GLFWJoystick is not valid!";
        
        final FloatBuffer rawAxes = GLFW.glfwGetJoystickAxes(this.id);
        final float[] out = new float[rawAxes.capacity()];
        
        for (int i = 0; i < rawAxes.capacity(); i++) {
            out[i] = rawAxes.get(i);
        }
        
        return out;
    }
    
    public GLFWJoystickButtonState[] getButtons() {
        assert this.isValid() : "GLFWJoystick is not valid!";
        
        final ByteBuffer rawStates = GLFW.glfwGetJoystickButtons(this.id);
        final GLFWJoystickButtonState[] out = new GLFWJoystickButtonState[rawStates.capacity()];
        
        for (int i = 0; i < rawStates.capacity(); i++) {
            switch (rawStates.get(i)) {
                case GLFW.GLFW_PRESS:
                    out[i] = GLFWJoystickButtonState.PRESS;
                    break;
                case GLFW.GLFW_RELEASE:
                    out[i] = GLFWJoystickButtonState.RELEASE;
                    break;
                default:
                    out[i] = GLFWJoystickButtonState.UNKNOWN;
                    break;
            }
        }
        
        return out;
    }
    
    public String getName() {
        assert this.isValid() : "GLFWJoystick is not valid!";
        
        return GLFW.glfwGetJoystickName(this.id);
    }
    
    public void swap(final GLFWJoystick other) {
        if (other == null) {
            //TODO: release resources
            return;
        }
        
        final int tmpId = other.id;
        
        other.id = this.id;
        this.id = tmpId;
    }
}
