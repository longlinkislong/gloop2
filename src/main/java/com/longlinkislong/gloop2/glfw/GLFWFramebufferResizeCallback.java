/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

/**
 *
 * @author zmichaels
 */
public interface GLFWFramebufferResizeCallback {
    void onFramebufferResize(GLFWWindow window, int newWidth, int newHeight);
    
    default void glfwFramebufferResizeCallback(long window, int width, int height) {
        this.onFramebufferResize(new GLFWWindow(window), width, height);
    }
}
