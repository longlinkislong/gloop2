/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import java.util.Set;

/**
 *
 * @author zmichaels
 */
public interface GLFWCharModsCallback {
    void onCharMods(GLFWWindow window, int codepoint, Set<GLFWKeyModifier> mods);
    
    default void glfwCharModsCallback(long window, int codepoint, int mods) {
        onCharMods(new GLFWWindow(window), codepoint, GLFWKeyModifier.from(mods));
    }
}
