/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

/**
 *
 * @author zmichaels
 */
public final class GLFWWindowCreateInfo {

    public final String title;
    public final int width;
    public final int height;
    public final GLFWHints hints;

    public GLFWWindowCreateInfo(
            final String windowName,
            final int width,
            final int height,
            final GLFWHints hints) {

        this.title = windowName;
        this.width = width;
        this.height = height;
        this.hints = hints;
    }
    
    public GLFWWindowCreateInfo() {
        this("GLFW Window", 640, 480, new GLFWHints());
    }
    
    public GLFWWindowCreateInfo withWidth(final int width) {
        return new GLFWWindowCreateInfo(title, width, height, hints);
    }
    
    public GLFWWindowCreateInfo withHeight(final int height) {
        return new GLFWWindowCreateInfo(title, width, height, hints);
    }
    
    public GLFWWindowCreateInfo withHints(final GLFWHints hints) {
        return new GLFWWindowCreateInfo(title, width, height, hints);
    }
    
    public GLFWWindowCreateInfo withTitle(final String title) {
        return new GLFWWindowCreateInfo(title, width, height, hints);
    }
    
    public GLFWWindow create() {
        return new GLFWWindow(hints, title, width, height);
    }
}
