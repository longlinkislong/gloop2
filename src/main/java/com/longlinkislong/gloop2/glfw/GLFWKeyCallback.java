/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import java.util.Set;

/**
 *
 * @author zmichaels
 */
public interface GLFWKeyCallback {
    void onKey(GLFWWindow window, int key, int scancode, GLFWKeyAction action, Set<GLFWKeyModifier> modifiers);
    
    default void glfwKeyCallback(long window, int key, int scancode, int action, int mods) {
        final GLFWKeyAction keyAction = GLFWKeyAction.of(action)
                .orElseThrow(() -> new IllegalArgumentException("Unsupported key action: " + action));
        
        onKey(new GLFWWindow(window), key, scancode, keyAction, GLFWKeyModifier.from(mods));
    }
}
