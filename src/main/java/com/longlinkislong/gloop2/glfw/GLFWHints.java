/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import com.longlinkislong.gloop2.vk.VKDeviceCreateInfo;
import org.lwjgl.glfw.GLFW;

/**
 *
 * @author zmichaels
 */
public final class GLFWHints {

    private static int bool(final boolean value) {
        return value ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE;
    }    

    public static enum ContextCreationAPI {
        NATIVE(GLFW.GLFW_NATIVE_CONTEXT_API),
        EGL(GLFW.GLFW_EGL_CONTEXT_API);

        final int value;

        private ContextCreationAPI(final int value) {
            this.value = value;
        }
    }

    public static enum ContextRobustness {
        NO_ROBUSTNESS(GLFW.GLFW_NO_ROBUSTNESS),
        NO_RESET_NOTIFICATION(GLFW.GLFW_NO_RESET_NOTIFICATION),
        LOSE_CONTEXT_ON_RESET(GLFW.GLFW_LOSE_CONTEXT_ON_RESET);

        final int value;

        private ContextRobustness(final int value) {
            this.value = value;
        }
    }

    public static enum ContextReleaseBehavior {
        ANY(GLFW.GLFW_ANY_RELEASE_BEHAVIOR),
        FLUSH(GLFW.GLFW_RELEASE_BEHAVIOR_FLUSH),
        NONE(GLFW.GLFW_RELEASE_BEHAVIOR_NONE);

        final int value;

        private ContextReleaseBehavior(final int value) {
            this.value = value;
        }
    }

    public static enum OpenGLProfile {
        ANY(GLFW.GLFW_OPENGL_ANY_PROFILE),
        COMPAT(GLFW.GLFW_OPENGL_COMPAT_PROFILE),
        CORE(GLFW.GLFW_OPENGL_CORE_PROFILE);

        final int value;

        private OpenGLProfile(final int value) {
            this.value = value;
        }
    }

    public static abstract class ContextHints {

        abstract void applyHints();
    }

    public static final class VulkanHints extends ContextHints {

        public final VKDeviceCreateInfo deviceCreateInfo;

        public VulkanHints(final VKDeviceCreateInfo deviceCreateInfo) {            
            this.deviceCreateInfo = deviceCreateInfo;
        }
        
        public VulkanHints(){
            this(null);
        }
        
        public VulkanHints withDeviceCreateInfo(final VKDeviceCreateInfo deviceCreateInfo) {
            return new VulkanHints(deviceCreateInfo);
        }
        
        @Override
        void applyHints() {
            GLFW.glfwWindowHint(GLFW.GLFW_CLIENT_API, GLFW.GLFW_NO_API);
        }

    }

    public static final class OpenGLHints extends ContextHints {

        public final ContextCreationAPI contextCreationAPI;
        public final int versionMajor;
        public final int versionMinor;
        public final OpenGLProfile profile;
        public final boolean forwardCompat;
        public final ContextRobustness robustnessStrategy;
        public final ContextReleaseBehavior releaseBehavior;

        private OpenGLHints(
                final int versionMajor, final int versionMinor,
                final ContextCreationAPI contextCreationAPI,
                final OpenGLProfile profile,
                final boolean forwardCompat,
                final ContextRobustness robustnessStrategy,
                final ContextReleaseBehavior releaseBehavior) {

            this.forwardCompat = forwardCompat;
            this.versionMajor = versionMajor;
            this.versionMinor = versionMinor;
            this.contextCreationAPI = contextCreationAPI;
            this.profile = profile;
            this.robustnessStrategy = robustnessStrategy;
            this.releaseBehavior = releaseBehavior;
        }

        public OpenGLHints() {
            this(1, 1, ContextCreationAPI.NATIVE, OpenGLProfile.ANY, false, ContextRobustness.NO_ROBUSTNESS, ContextReleaseBehavior.ANY);
        }

        public OpenGLHints withContextCreationAPI(final ContextCreationAPI contextCreationAPI) {
            return new OpenGLHints(versionMajor, versionMinor, contextCreationAPI, profile, forwardCompat, this.robustnessStrategy, this.releaseBehavior);
        }

        public OpenGLHints withVersion(final int versionMajor, final int versionMinor) {
            return new OpenGLHints(versionMajor, versionMinor, contextCreationAPI, profile, forwardCompat, this.robustnessStrategy, this.releaseBehavior);
        }

        public OpenGLHints withProfile(final OpenGLProfile profile) {
            return new OpenGLHints(versionMajor, versionMinor, contextCreationAPI, profile, forwardCompat, this.robustnessStrategy, this.releaseBehavior);
        }

        public OpenGLHints withForwardCompat(final boolean forwardCompat) {
            return new OpenGLHints(versionMajor, versionMinor, contextCreationAPI, profile, forwardCompat, this.robustnessStrategy, this.releaseBehavior);
        }

        public OpenGLHints withRobustnessStrategy(final ContextRobustness robustnessStrategy) {
            return new OpenGLHints(versionMajor, versionMinor, contextCreationAPI, profile, forwardCompat, this.robustnessStrategy, this.releaseBehavior);
        }

        public OpenGLHints withReleaseBehavior(final ContextReleaseBehavior releaseBehavior) {
            return new OpenGLHints(versionMajor, versionMinor, contextCreationAPI, profile, forwardCompat, this.robustnessStrategy, this.releaseBehavior);
        }

        @Override
        void applyHints() {
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_CREATION_API, this.contextCreationAPI.value);
            GLFW.glfwWindowHint(GLFW.GLFW_CLIENT_API, GLFW.GLFW_OPENGL_API);
            GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT, bool(this.forwardCompat));
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, this.versionMajor);
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, this.versionMinor);
            GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, this.profile.value);
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_ROBUSTNESS, this.robustnessStrategy.value);
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_RELEASE_BEHAVIOR, this.releaseBehavior.value);
        }
    }

    public static final class OpenGLESHints extends ContextHints {

        public final ContextCreationAPI contextCreationAPI;
        public final int versionMajor;
        public final int versionMinor;
        public final ContextRobustness robustnessStrategy;
        public final ContextReleaseBehavior releaseBehavior;

        private OpenGLESHints(final int versionMajor, final int versionMinor,
                final ContextCreationAPI contextCreationAPI,
                final ContextRobustness robustnessStrategy,
                final ContextReleaseBehavior releaseBehavior) {

            this.versionMajor = versionMajor;
            this.versionMinor = versionMinor;
            this.contextCreationAPI = contextCreationAPI;
            this.robustnessStrategy = robustnessStrategy;
            this.releaseBehavior = releaseBehavior;
        }

        public OpenGLESHints() {
            this(2, 0, ContextCreationAPI.NATIVE, ContextRobustness.NO_ROBUSTNESS, ContextReleaseBehavior.ANY);
        }

        public OpenGLESHints withContextCreationAPI(final ContextCreationAPI contextCreationAPI) {
            return new OpenGLESHints(versionMajor, versionMinor, contextCreationAPI, robustnessStrategy, releaseBehavior);
        }

        public OpenGLESHints withVersion(final int versionMajor, final int versionMinor) {
            return new OpenGLESHints(versionMajor, versionMinor, contextCreationAPI, robustnessStrategy, releaseBehavior);
        }

        public OpenGLESHints withRobustnessStrategy(final ContextRobustness robustnessStrategy) {
            return new OpenGLESHints(versionMajor, versionMinor, contextCreationAPI, robustnessStrategy, releaseBehavior);
        }

        public OpenGLESHints withReleaseBehavior(final ContextReleaseBehavior releaseBehavior) {
            return new OpenGLESHints(versionMajor, versionMinor, contextCreationAPI, robustnessStrategy, releaseBehavior);
        }

        @Override
        void applyHints() {
            GLFW.glfwWindowHint(GLFW.GLFW_CLIENT_API, GLFW.GLFW_OPENGL_ES_API);
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_CREATION_API, this.contextCreationAPI.value);
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, this.versionMajor);
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, this.versionMinor);
        }
    }

    public static final class WindowHints {

        public final boolean resizable;
        public final boolean visible;
        public final boolean decorated;
        public final boolean focused;
        public final boolean autoIconify;
        public final boolean floating;
        public final boolean maximized;

        private WindowHints(final boolean resizable,
                final boolean visible,
                final boolean decorated,
                final boolean focused,
                final boolean autoIconify,
                final boolean floating,
                final boolean maximized) {

            this.resizable = resizable;
            this.visible = visible;
            this.decorated = decorated;
            this.focused = focused;
            this.autoIconify = autoIconify;
            this.floating = floating;
            this.maximized = maximized;
        }

        public WindowHints() {
            this(true, true, true, true, true, false, false);
        }

        public WindowHints withResizable(final boolean resizable) {
            return new WindowHints(resizable, visible, decorated, focused, autoIconify, floating, maximized);
        }

        public WindowHints withVisible(final boolean visible) {
            return new WindowHints(resizable, visible, decorated, focused, autoIconify, floating, maximized);
        }

        public WindowHints withDecorated(final boolean decorated) {
            return new WindowHints(resizable, visible, decorated, focused, autoIconify, floating, maximized);
        }

        public WindowHints withFocused(final boolean focused) {
            return new WindowHints(resizable, visible, decorated, focused, autoIconify, floating, maximized);
        }

        public WindowHints withAutoIconify(final boolean autoIconify) {
            return new WindowHints(resizable, visible, decorated, focused, autoIconify, floating, maximized);
        }

        public WindowHints withFloating(final boolean floating) {
            return new WindowHints(resizable, visible, decorated, focused, autoIconify, floating, maximized);
        }

        public WindowHints withMaximized(final boolean maximized) {
            return new WindowHints(resizable, visible, decorated, focused, autoIconify, floating, maximized);
        }

        void applyHints() {
            GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, bool(this.resizable));
            GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, bool(this.visible));
            GLFW.glfwWindowHint(GLFW.GLFW_DECORATED, bool(this.decorated));
            GLFW.glfwWindowHint(GLFW.GLFW_FOCUSED, bool(this.focused));
            GLFW.glfwWindowHint(GLFW.GLFW_AUTO_ICONIFY, bool(this.autoIconify));
            GLFW.glfwWindowHint(GLFW.GLFW_FLOATING, bool(this.floating));
            GLFW.glfwWindowHint(GLFW.GLFW_MAXIMIZED, bool(this.maximized));
        }
    }

    public static final class FramebufferHints {

        public final int redBits;
        public final int blueBits;
        public final int greenBits;
        public final int alphaBits;
        public final int depthBits;
        public final int stencilBits;
        public final boolean stereo;
        public final int samples;
        public final boolean srgbCapable;
        public final boolean doublebuffer;

        private FramebufferHints(
                final int redBits, final int greenBits, final int blueBits, final int alphaBits,
                final int depthBits, final int stencilBits,
                final boolean stereo,
                final int samples,
                final boolean srgbCapable,
                final boolean doublebuffer) {

            this.redBits = redBits;
            this.greenBits = greenBits;
            this.blueBits = blueBits;
            this.alphaBits = alphaBits;
            this.depthBits = depthBits;
            this.stencilBits = stencilBits;
            this.stereo = stereo;
            this.samples = samples;
            this.srgbCapable = srgbCapable;
            this.doublebuffer = doublebuffer;
        }

        public FramebufferHints() {
            this(8, 8, 8, 8, 24, 8, false, 0, false, true);
        }

        public FramebufferHints withColorBits(final int redBits, final int greenBits, final int blueBits, final int alphaBits) {
            return new FramebufferHints(redBits, greenBits, blueBits, alphaBits, depthBits, stencilBits, stereo, samples, srgbCapable, doublebuffer);
        }

        public FramebufferHints withDepthStencilBits(final int depthBits, final int stencilBits) {
            return new FramebufferHints(redBits, greenBits, blueBits, alphaBits, depthBits, stencilBits, stereo, samples, srgbCapable, doublebuffer);
        }

        public FramebufferHints withStereoRendering(final boolean stereo) {
            return new FramebufferHints(redBits, greenBits, blueBits, alphaBits, depthBits, stencilBits, stereo, samples, srgbCapable, doublebuffer);
        }

        public FramebufferHints withSamples(final int samples) {
            return new FramebufferHints(redBits, greenBits, blueBits, alphaBits, depthBits, stencilBits, stereo, samples, srgbCapable, doublebuffer);
        }

        public FramebufferHints withSRGBCapable(final boolean srgbCapable) {
            return new FramebufferHints(redBits, greenBits, blueBits, alphaBits, depthBits, stencilBits, stereo, samples, srgbCapable, doublebuffer);
        }

        public FramebufferHints withDoublebuffer(final boolean doublebuffer) {
            return new FramebufferHints(redBits, greenBits, blueBits, alphaBits, depthBits, stencilBits, stereo, samples, srgbCapable, doublebuffer);
        }

        void applyHints() {
            GLFW.glfwWindowHint(GLFW.GLFW_RED_BITS, this.redBits);
            GLFW.glfwWindowHint(GLFW.GLFW_GREEN_BITS, this.greenBits);
            GLFW.glfwWindowHint(GLFW.GLFW_BLUE_BITS, this.blueBits);
            GLFW.glfwWindowHint(GLFW.GLFW_ALPHA_BITS, this.alphaBits);
            GLFW.glfwWindowHint(GLFW.GLFW_DEPTH_BITS, this.depthBits);
            GLFW.glfwWindowHint(GLFW.GLFW_STENCIL_BITS, this.stencilBits);
            GLFW.glfwWindowHint(GLFW.GLFW_SAMPLES, this.samples);
            GLFW.glfwWindowHint(GLFW.GLFW_SRGB_CAPABLE, bool(this.srgbCapable));
            GLFW.glfwWindowHint(GLFW.GLFW_DOUBLEBUFFER, bool(this.doublebuffer));
        }
    }

    public static final class MonitorHints {

        public final int refreshRate;

        private MonitorHints(final int refreshRate) {
            this.refreshRate = refreshRate;
        }

        public MonitorHints() {
            this(GLFW.GLFW_DONT_CARE);
        }

        public MonitorHints withRefreshRate(final int refreshRate) {
            return new MonitorHints(refreshRate);
        }

        void applyHints() {
            GLFW.glfwWindowHint(GLFW.GLFW_REFRESH_RATE, this.refreshRate);
        }
    }

    public final ContextHints contextHints;
    public final WindowHints windowHints;
    public final FramebufferHints framebufferHints;
    public final MonitorHints monitorHints;

    private GLFWHints(
            final ContextHints contextHints,
            final WindowHints windowHints,
            final FramebufferHints framebufferHints,
            final MonitorHints monitorHints) {

        this.contextHints = contextHints;
        this.windowHints = windowHints;
        this.framebufferHints = framebufferHints;
        this.monitorHints = monitorHints;
    }

    public GLFWHints() {
        this(new OpenGLHints(), new WindowHints(), new FramebufferHints(), new MonitorHints());
    }

    public GLFWHints withContextHints(final ContextHints contextHints) {
        return new GLFWHints(contextHints, windowHints, framebufferHints, monitorHints);
    }

    public GLFWHints withWindowHints(final WindowHints windowHints) {
        return new GLFWHints(contextHints, windowHints, framebufferHints, monitorHints);
    }

    public GLFWHints withMonitorHints(final MonitorHints monitorHints) {
        return new GLFWHints(contextHints, windowHints, framebufferHints, monitorHints);
    }

    public GLFWHints withFramebufferHints(final FramebufferHints framebufferHints) {
        return new GLFWHints(contextHints, windowHints, framebufferHints, monitorHints);
    }

    void applyHints() {
        this.contextHints.applyHints();
        this.framebufferHints.applyHints();
        this.monitorHints.applyHints();
        this.windowHints.applyHints();
    }
}
