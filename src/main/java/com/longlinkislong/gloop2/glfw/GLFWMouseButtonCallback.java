/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import java.util.Set;

/**
 *
 * @author zmichaels
 */
public interface GLFWMouseButtonCallback {
    void onMouseButton(GLFWWindow window, int button, GLFWMouseButtonAction action, Set<GLFWKeyModifier> mods);
    
    default void glfwMouseButtonCallback(long window, int button, int action, int mods) {
        final GLFWMouseButtonAction eAction = GLFWMouseButtonAction.of(action)
                .orElseThrow(() -> new IllegalArgumentException("Invalid GLFW action value: " + action));
                
        onMouseButton(new GLFWWindow(window), button, eAction, GLFWKeyModifier.from(mods));
    }
}
