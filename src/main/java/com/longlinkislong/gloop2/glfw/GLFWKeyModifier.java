/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.lwjgl.glfw.GLFW;

/**
 *
 * @author zmichaels
 */
public enum GLFWKeyModifier {
    SHIFT(GLFW.GLFW_MOD_SHIFT),
    CONTROL(GLFW.GLFW_MOD_CONTROL),
    ALT(GLFW.GLFW_MOD_ALT),
    SUPER(GLFW.GLFW_MOD_SUPER);
    
    final int value;
    
    private GLFWKeyModifier(final int value) {
        this.value = value;
    }
    
    public static Optional<GLFWKeyModifier> of(final int value) {
        switch (value) {
            case GLFW.GLFW_MOD_SHIFT:
                return Optional.of(SHIFT);
            case GLFW.GLFW_MOD_CONTROL:
                return Optional.of(CONTROL);
            case GLFW.GLFW_MOD_ALT:
                return Optional.of(ALT);
            case GLFW.GLFW_MOD_SUPER:
                return Optional.of(SUPER);
            default:
                return Optional.empty();
        }
    }
    
    public static Set<GLFWKeyModifier> from(final int value) {        
        return Arrays.stream(GLFWKeyModifier.values())
                .filter(mod -> (value & mod.value) != 0)
                .collect(Collectors.toSet());
    }
}
