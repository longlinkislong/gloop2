/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import com.longlinkislong.gloop2.vk.VKContext;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWVulkan;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 *
 * @author zmichaels
 */
public class GLFWWindow implements AutoCloseable {

    static final boolean HAS_VULKAN_SUPPORT;

    static {
        GLFW.glfwSetErrorCallback(GLFWErrorCallback.createPrint(System.err));
        assert GLFW.glfwInit() : "Failed to initialize GLFW!";

        HAS_VULKAN_SUPPORT = GLFWVulkan.glfwVulkanSupported();
    }

    private static final AtomicInteger OPEN_WINDOWS = new AtomicInteger(0);

    public long handle;
    private VKContext vkContext;

    public GLFWWindow() {
        this.vkContext = null;
    }

    GLFWWindow(final long window) {
        this.handle = window;
        this.vkContext = null;
    }

    public GLFWWindow(final GLFWHints windowHints, final String title, final int width, final int height) {
        OPEN_WINDOWS.getAndIncrement();

        windowHints.applyHints();

        this.handle = GLFW.glfwCreateWindow(width, height, title, NULL, NULL);

        if (windowHints.contextHints instanceof GLFWHints.OpenGLHints) {
            GLFW.glfwMakeContextCurrent(this.handle);
            GL.createCapabilities();
            this.vkContext = null;
        } else if (windowHints.contextHints instanceof GLFWHints.OpenGLESHints) {
            throw new UnsupportedOperationException("OpenGLES context not yet supported!");
        } else if (windowHints.contextHints instanceof GLFWHints.VulkanHints) {
            this.vkContext = new VKContext(((GLFWHints.VulkanHints) windowHints.contextHints).deviceCreateInfo, this);
            this.vkContext.resizeSwapchain(width, height);
        } else {
            throw new UnsupportedOperationException("Unsupported context type: " + windowHints.contextHints);
        }

        OPEN_WINDOWS.incrementAndGet();
    }

    public void setSize(final int width, final int height) {
        assert this.isValid() : "GLFWWindow is not valid!";

        GLFW.glfwSetWindowSize(this.handle, width, height);
    }

    public void setSwapInterval(final int interval) {
        assert this.isValid() : "GLFWWindow is not valid!";

        GLFW.glfwSwapInterval(interval);
    }

    public void setPosition(int x, int y) {
        assert this.isValid() : "GLFWWindow is not valid!";

        GLFW.glfwSetWindowPos(this.handle, x, x);
    }

    public int getX() {
        assert this.isValid() : "GLFWWindow is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer px = mem.ints(0);
            final IntBuffer py = mem.ints(0);

            GLFW.glfwGetWindowPos(this.handle, px, py);

            return px.get(0);
        }
    }

    public int getFramebufferWidth() {
        assert this.isValid() : "GLFWWindow is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pw = mem.ints(0);
            final IntBuffer ph = mem.ints(0);

            GLFW.glfwGetFramebufferSize(this.handle, pw, ph);

            return pw.get(0);
        }
    }

    public int getFramebufferHeight() {
        assert this.isValid() : "GLFWWindow is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pw = mem.ints(0);
            final IntBuffer ph = mem.ints(0);

            GLFW.glfwGetFramebufferSize(this.handle, pw, ph);

            return ph.get(0);

        }
    }

    public int getY() {
        assert this.isValid() : "GLFWWindow is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer px = mem.ints(0);
            final IntBuffer py = mem.ints(0);

            GLFW.glfwGetWindowPos(this.handle, px, py);

            return px.get(0);
        }
    }

    public void show() {
        assert this.isValid() : "GLFWWindow is not valid!";

        GLFW.glfwShowWindow(this.handle);
    }

    public void hide() {
        assert this.isValid() : "GLFWWindow is not valid!";

        GLFW.glfwHideWindow(this.handle);
    }

    public void signalClose() {
        if (this.isValid()) {
            GLFW.glfwSetWindowShouldClose(this.handle, true);
        }
    }

    @Override
    public void close() {
        if (this.isValid()) {
            GLFW.glfwDestroyWindow(this.handle);

            this.handle = 0L;

            if (OPEN_WINDOWS.decrementAndGet() == 0) {
                GLFW.glfwTerminate();
            }
        }
    }

    public boolean isValid() {
        return this.handle > 0L;
    }

    public boolean shouldClose() {
        if (this.isValid()) {
            return GLFW.glfwWindowShouldClose(this.handle);
        } else {
            return true;
        }
    }

    public void pollEvents() {
        assert this.isValid() : "GLFWWindow is not valid!";

        GLFW.glfwPollEvents();
    }

    public void beginFrame() {
        assert this.isValid() : "GLFWWindow is not valid!";

        if (this.vkContext != null) {
            this.vkContext.acquireBackBuffer();
        }
    }

    public void endFrame() {
        assert this.isValid() : "GLFWWindow is not valid!";

        if (this.vkContext == null) {
            GLFW.glfwSwapBuffers(this.handle);
        } else {
            this.vkContext.swapBuffers();
        }
    }

    public void setMouseButtonCallback(final GLFWMouseButtonCallback callback) {
        assert this.isValid() : "GLFWWindow is not valid!";

        if (callback == null) {
            GLFW.glfwSetFramebufferSizeCallback(this.handle, null);
        } else {
            GLFW.glfwSetMouseButtonCallback(this.handle, callback::glfwMouseButtonCallback);
        }
    }

    public void setFramebufferResizeCallback(final GLFWFramebufferResizeCallback callback) {
        assert this.isValid() : "GLFWWindow is not valid!";

        if (callback == null) {
            GLFW.glfwSetFramebufferSizeCallback(this.handle, null);
        } else {
            GLFW.glfwSetFramebufferSizeCallback(this.handle, callback::glfwFramebufferResizeCallback);
        }
    }

    public void setScrollCallback(final GLFWScrollCallback callback) {
        assert this.isValid() : "GLFWWindow is not valid!";

        if (callback == null) {
            GLFW.glfwSetScrollCallback(this.handle, null);
        } else {
            GLFW.glfwSetScrollCallback(this.handle, callback::glfwScrollCallback);
        }
    }

    public void setCursorPositionCallback(final GLFWCursorPosCallback callback) {
        assert this.isValid() : "GLFWWindow is not valid!";

        if (callback == null) {
            GLFW.glfwSetCursorPosCallback(this.handle, null);
        } else {
            GLFW.glfwSetCursorPosCallback(this.handle, callback::glfwCursorPosCallback);
        }
    }

    public void setKeyCallback(final GLFWKeyCallback callback) {
        assert this.isValid() : "GLFWWindow is not valid!";

        if (callback == null) {
            GLFW.glfwSetKeyCallback(this.handle, null);
        } else {
            GLFW.glfwSetKeyCallback(this.handle, callback::glfwKeyCallback);
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("GLFWWindow does not support clone!");
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        } else if (o == null) {
            return false;
        } else if (o instanceof GLFWWindow) {
            return this.handle == ((GLFWWindow) o).handle;
        } else {
            return false;
        }
    }

    public Optional<VKContext> getVulkanContext() {
        return Optional.ofNullable(this.vkContext);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (int) (this.handle ^ (this.handle >>> 32));
        return hash;
    }

    public void swap(final GLFWWindow other) {
        if (other == null) {
            this.close();
            return;
        }

        final long tmpWindow = other.handle;
        final VKContext tmpCtx = other.vkContext;

        other.handle = this.handle;
        other.vkContext = tmpCtx;

        this.handle = tmpWindow;
        this.vkContext = tmpCtx;
    }

    private final Map<GLFWCursor, Long> cursorHandles = new HashMap<>();

    public void setCursor(final GLFWCursor cursor) {
        assert this.isValid() : "GLFWWindow is not valid!";

        Long hCursor = this.cursorHandles.get(cursor);

        if (hCursor == null) {
            hCursor = GLFW.glfwCreateStandardCursor(cursor.value);

            this.cursorHandles.put(cursor, hCursor);
        }

        GLFW.glfwSetCursor(this.handle, hCursor);
    }

    public static GLFWVidMode getVideoMode() {
        return GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
    }

    public GLFWKeyAction getKey(final int key) {
        assert this.isValid() : "GLFWWindow is not valid!";

        switch (GLFW.glfwGetKey(this.handle, key)) {
            case GLFW.GLFW_PRESS:
                return GLFWKeyAction.PRESS;
            case GLFW.GLFW_RELEASE:
                return GLFWKeyAction.RELEASE;
            default:
                throw new IllegalStateException("Unknown key action!");
        }
    }

    public double[] getCursor() {
        assert this.isValid() : "GLFWWindow is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final DoubleBuffer pX = mem.callocDouble(1);
            final DoubleBuffer pY = mem.callocDouble(1);

            GLFW.glfwGetCursorPos(this.handle, pX, pY);

            return new double[]{pX.get(), pY.get()};
        }
    }

    public GLFWMouseButtonAction getMouseButton(final int btn) {
        assert this.isValid() : "GLFWWindow is not valid!";

        switch (GLFW.glfwGetMouseButton(this.handle, btn)) {
            case GLFW.GLFW_PRESS:
                return GLFWMouseButtonAction.PRESS;
            case GLFW.GLFW_RELEASE:
                return GLFWMouseButtonAction.RELEASE;
            default:
                throw new IllegalStateException("Unknown mouse button action!");
        }
    }

    public String getClipboardString() {
        assert this.isValid() : "GLFWWindow is not valid!";

        return GLFW.glfwGetClipboardString(this.handle);
    }

    public void setClipboardString(final String str) {
        assert this.isValid() : "GLFWWindow is not valid!";

        GLFW.glfwSetClipboardString(handle, str);
    }

    public void setCharCallback(final GLFWCharCallback callback) {
        assert this.isValid() : "GLFWWindow is not valid!";

        if (callback == null) {
            GLFW.glfwSetCharCallback(this.handle, null);
        } else {
            GLFW.glfwSetCharCallback(this.handle, callback::glfwCharCallback);
        }
    }

    public void setCharModsCallback(final GLFWCharModsCallback callback) {
        assert this.isValid() : "GLFWWindow is not valid!";

        if (callback == null) {
            GLFW.glfwSetCharModsCallback(this.handle, null);
        } else {
            GLFW.glfwSetCharModsCallback(this.handle, callback::glfwCharModsCallback);
        }
    }

    public void setCursorEnterCallback(final GLFWCursorEnterCallback callback) {
        assert this.isValid() : "GLFWWindow is not valid!";

        if (callback == null) {
            GLFW.glfwSetCursorEnterCallback(this.handle, null);
        } else {
            GLFW.glfwSetCursorEnterCallback(handle, callback::glfwCursorEnterCallback);
        }
    }
}
