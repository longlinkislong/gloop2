/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

/**
 *
 * @author zmichaels
 */
public interface GLFWCursorPosCallback {
    void onMouseMove(GLFWWindow window, double xpos, double ypos);
    
    default void glfwCursorPosCallback(long window, double xpos, double ypos) {
        onMouseMove(new GLFWWindow(window), xpos, ypos);
    }
}
