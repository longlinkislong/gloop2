/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import java.util.Optional;
import org.lwjgl.glfw.GLFW;

/**
 *
 * @author zmichaels
 */
public enum GLFWKeyAction {
    PRESS(GLFW.GLFW_PRESS),
    REPEAT(GLFW.GLFW_REPEAT),
    RELEASE(GLFW.GLFW_RELEASE);
    
    final int value;
    
    private GLFWKeyAction(final int value) {
        this.value = value;
    }
    
    public static Optional<GLFWKeyAction> of(final int value) {
        switch (value) {
            case GLFW.GLFW_PRESS:
                return Optional.of(PRESS);
            case GLFW.GLFW_REPEAT:
                return Optional.of(REPEAT);
            case GLFW.GLFW_RELEASE:
                return Optional.of(RELEASE);
            default:
                return Optional.empty();
        }
    }
}
