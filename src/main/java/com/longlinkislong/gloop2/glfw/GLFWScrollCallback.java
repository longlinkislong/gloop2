/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

/**
 *
 * @author zmichaels
 */
public interface GLFWScrollCallback {
    void onScroll(GLFWWindow window, double xoffset, double yoffset);
    
    default void glfwScrollCallback(long window, double xoffset, double yoffset) {
        onScroll(new GLFWWindow(window), xoffset, yoffset);
    }
}
