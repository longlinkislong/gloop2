/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

/**
 *
 * @author zmichaels
 */
public enum GLFWJoystickHatState {
    UNKNOWN,
    CENTERED,
    UP,
    RIGHT,
    DOWN,
    LEFT,
    RIGHT_UP,
    RIGHT_DOWN,
    LEFT_UP,
    LEFT_DOWN
}
