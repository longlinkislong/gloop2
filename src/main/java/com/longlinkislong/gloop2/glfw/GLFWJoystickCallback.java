/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import org.lwjgl.glfw.GLFW;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public interface GLFWJoystickCallback {
    void onJoystickConnect(GLFWJoystick joystick);
    
    void onJoystickDisconnect(GLFWJoystick joystick);
    
    default void glfwJoystickCallback(int jid, int event) {
        switch (event) {
            case GLFW.GLFW_CONNECTED:
                this.onJoystickConnect(new GLFWJoystick(jid));
                break;
            case GLFW.GLFW_DISCONNECTED:
                this.onJoystickDisconnect(new GLFWJoystick(jid));
                break;
            default:
                LoggerFactory.getLogger(this.getClass()).debug("Unknown Joystick event! [joystick: {}, event: {}]", jid, event);
        }
    }
}
