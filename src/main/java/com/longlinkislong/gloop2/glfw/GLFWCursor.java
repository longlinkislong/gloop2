/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.glfw;

import org.lwjgl.glfw.GLFW;

/**
 *
 * @author zmichaels
 */
public enum GLFWCursor {
    ARROW(GLFW.GLFW_ARROW_CURSOR),
    HAND(GLFW.GLFW_HAND_CURSOR),
    CROSSHAIR(GLFW.GLFW_CROSSHAIR_CURSOR),
    HIDDEN(GLFW.GLFW_CURSOR_HIDDEN),
    HRESIZE(GLFW.GLFW_HRESIZE_CURSOR),
    VRESIZE(GLFW.GLFW_VRESIZE_CURSOR),
    IBEAM(GLFW.GLFW_IBEAM_CURSOR);
    public final int value;

    private GLFWCursor(final int value) {
        this.value = value;
    }
}
