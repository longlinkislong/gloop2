/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2;

/**
 *
 * @author zmichaels
 */
public class Fixed extends Number {

    public static final Fixed POSITIVE_INFINITY = new Fixed(1, 0);
    public static final Fixed NEGATIVE_INFINITY = new Fixed(-1, 0);
    public static final int SIZE = 8;
    public static final Fixed MAX_VALUE = new Fixed(Integer.MAX_VALUE, 1);
    public static final Fixed MIN_VALUE = new Fixed(0, 1);
    public static final Fixed NaN = new Fixed(0, 0);
    
    public final int value;
    public final int scale;

    public Fixed(final int value, final int scale) {
        this.value = value;
        this.scale = scale;
    }
    
    public Fixed(final float value, final int scale) {
        this.scale = scale;
        this.value = (int) (value * scale);
    }
    
    public Fixed(final float value) {
        this(value, 0x400);
    }

    @Override
    public int intValue() {
        return this.value / this.scale;
    }

    @Override
    public long longValue() {
        return intValue();
    }

    @Override
    public float floatValue() {
        return ((float) this.value) / this.scale;
    }

    @Override
    public double doubleValue() {
        return floatValue();
    }

    public float epsilon() {
        return 1F / this.scale;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        } else if (obj instanceof Fixed) {
            final Fixed other = (Fixed) obj;

            if (this.value == other.value && this.scale == other.scale) {
                return true;
            } else {
                final float epsilon = Math.max(this.epsilon(), other.epsilon());

                return Math.abs(this.floatValue() - other.floatValue()) < epsilon;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.value;
        hash = 67 * hash + this.scale;
        return hash;
    }
    
    public static String toHexString(final int value, final int scale) {
        return String.format("%x%x", value, scale);
    }

    @Override
    public String toString() {
        return toString(this.value, this.scale);
    }

    public static String toString(final int value, final int scale) {
        if (scale == 1) {
            return Integer.toString(value);
        } else {
            return new StringBuilder(4096)
                    .append(Integer.toString(value))
                    .append('/')
                    .append(Integer.toString(scale))
                    .toString();
        }
    }
}
