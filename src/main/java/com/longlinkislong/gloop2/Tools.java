/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.vulkan.EXTDebugReport;
import org.lwjgl.vulkan.KHRDisplaySwapchain;
import org.lwjgl.vulkan.KHRSurface;
import org.lwjgl.vulkan.KHRSwapchain;
import org.lwjgl.vulkan.VK10;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */public final class Tools {     
     /**
      * Application temporary storage.
      */
    public static final Path APPDATA;
    
    static {
        final String osName = System.getProperty("os.name").toLowerCase();
        
        if (osName.contains("darwin")) {
            APPDATA = Paths.get(System.getProperty("user.home"), "Library/Application Support");
        } else if (osName.contains("win")) {
            APPDATA = Paths.get(System.getenv("APPDATA"));
        } else if (osName.contains("nux")) {
            APPDATA = Paths.get(System.getProperty("user.home"), ".local/share");
        } else {
            APPDATA = Paths.get(".");
        }
    }
    
    private static final Logger LOGGER = LoggerFactory.getLogger(Tools.class);
    
    private Tools() {
    }
    
    /**
     * Reads all text from an InputStream.
     * @param in the InputStream source.
     * @return the text
     * @throws IOException if an error occurred while reading the data.
     */
    public static String readAll(final InputStream in) throws IOException {
        final ByteArrayOutputStream out = new ByteArrayOutputStream(16384);
        final byte[] buffer = new byte[4096];
        int length;
        
        while ((length = in.read(buffer)) != -1) {
            out.write(buffer, 0, length);
        }
        
        return out.toString("UTF-8");
    }
    
    public static <T> T withStreamData(final InputStream in, final Function<ByteBuffer, T> dataCallback) throws IOException {
        ByteBuffer data = null;
        
        try (ReadableByteChannel bc = Channels.newChannel(in)) {
            data = MemoryUtil.memAlloc(8192);
            
            while (bc.read(data) != -1) {
                if (data.remaining() == 0) {
                    final ByteBuffer newData = MemoryUtil.memAlloc(data.capacity() * 2);
                    
                    data.flip();
                    newData.put(data);
                    
                    MemoryUtil.memFree(data);
                    
                    data = newData;
                }
            }
            
            data.flip();
            
            return dataCallback.apply(data);
        } finally {
            if (data != null) {
                MemoryUtil.memFree(data);
            }
        }
    }
    
    public static <T> T withFileData(final Path file, final Function<ByteBuffer, T> dataCallback) throws IOException {
        ByteBuffer data = null;
        
        try (FileChannel fc = FileChannel.open(file, StandardOpenOption.READ)) {
            data = MemoryUtil.memAlloc((int) fc.size());
            
            while (fc.read(data) != -1) {
                // operation is performed in side-effect
            }
            
            return dataCallback.apply(data);
        } finally {
            if (data != null) {
                MemoryUtil.memFree(data);
            }
        }
    }
    
    public static <T> T withFileDataMapped(final Path file, final Function<ByteBuffer, T> dataCallback) throws IOException {
        try (FileChannel fc = FileChannel.open(file, StandardOpenOption.READ, StandardOpenOption.WRITE)) {
            final ByteBuffer data = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            
            return dataCallback.apply(data);
        }
    }
    
    public static boolean vksuccess(int result) {
        switch (result) {
            case VK10.VK_SUCCESS:
                return true;
            case VK10.VK_NOT_READY:
                LOGGER.trace("SUCCESS: NOT_READY");
                return true;
            case VK10.VK_TIMEOUT:
                LOGGER.trace("SUCCESS: TIMEOUT");
                return true;
            case VK10.VK_EVENT_SET:
                LOGGER.trace("SUCCESS: EVENT_SET");
                return true;
            case VK10.VK_EVENT_RESET:
                LOGGER.trace("SUCCESS: EVENT_RESET");
                return true;
            case VK10.VK_INCOMPLETE:
                LOGGER.trace("SUCCESS: INCOMPLETE");
                return true;
            default:
                return false;
        }
    }
    
    public static String vkerr(int err) {
        switch (err) {
            case VK10.VK_SUCCESS:
                return "Command successfully completed.";
            case VK10.VK_NOT_READY:
                return "A fence or query has not yet completed.";
            case VK10.VK_TIMEOUT:
                return "A wait operation has not completed in the specified time.";
            case VK10.VK_EVENT_SET:
                return "An event is signaled.";
            case VK10.VK_EVENT_RESET:
                return "An event is unsignaled.";
            case VK10.VK_INCOMPLETE:
                return "A return array was too small for the result.";
            case KHRSwapchain.VK_SUBOPTIMAL_KHR:
                return "A swapchain no longer matches the surface properties exactly, but still can be used to present the surface successfully.";
            case VK10.VK_ERROR_OUT_OF_HOST_MEMORY:
                return "A host memory allocation has failed.";
            case VK10.VK_ERROR_OUT_OF_DEVICE_MEMORY:
                return "A device memory allocation has failed.";
            case VK10.VK_ERROR_INITIALIZATION_FAILED:
                return "Initializatin of an object could not be completed for implementation-specific reasons.";
            case VK10.VK_ERROR_DEVICE_LOST:
                return "The logical or physical device has been lost.";
            case VK10.VK_ERROR_MEMORY_MAP_FAILED:
                return "Mapping of a memory object has failed.";
            case VK10.VK_ERROR_LAYER_NOT_PRESENT:
                return "A requested layer is not present or could not be loaded.";
            case VK10.VK_ERROR_EXTENSION_NOT_PRESENT:
                return "A requested extension is not supported.";
            case VK10.VK_ERROR_FEATURE_NOT_PRESENT:
                return "A requested feature is not supported.";
            case VK10.VK_ERROR_INCOMPATIBLE_DRIVER:
                return "The requested version of Vulkan is not supported by the driver or is otherwise incompatible for implementation-specific reasons.";
            case VK10.VK_ERROR_TOO_MANY_OBJECTS:
                return "Too many objects of the type have already been created.";
            case VK10.VK_ERROR_FORMAT_NOT_SUPPORTED:
                return "A requested format is not supported on this device.";
            case KHRSurface.VK_ERROR_SURFACE_LOST_KHR:
                return "A surface is no longer available.";
            case KHRSurface.VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
                return "The requested window is already connected to a VkSurfaceKHR, or to some other non-Vulkan API.";
            case KHRSwapchain.VK_ERROR_OUT_OF_DATE_KHR:
                return "A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the "
                        + "swapchain will fail. Applications must query the new surface properties and recreate their swapchain if they wish to continue" + "presenting to the surface.";
            case KHRDisplaySwapchain.VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
                return "The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an" + " image.";
            case EXTDebugReport.VK_ERROR_VALIDATION_FAILED_EXT:
                return "A validation layer found an error.";
            default:
                return String.format("%s [%d]", "Unknown", Integer.valueOf(err));
        }
    }
    
    public static ByteBuffer clearDepthStencil(final float depth, final int stencil) {
        final ByteBuffer out = ByteBuffer.allocate(16)
                .putFloat(depth)
                .putInt(stencil)
                .putInt(0)
                .putInt(0);
        
        out.flip();
        
        return out;
    }
    
    public static ByteBuffer clearColor(final float r, final float g, final float b, final float a) {
        final ByteBuffer out = ByteBuffer.allocate(Float.BYTES * 4)
                .putFloat(r)
                .putFloat(g)
                .putFloat(b)
                .putFloat(a);
        
        out.flip();
        
        return out;
    }    
    
    public static <T> List<T> copyList(final Collection<T> c) {
        return (c == null || c.isEmpty())
                ? Collections.emptyList()
                : Collections.unmodifiableList(new ArrayList<>(c));
    }
    
    public static <T> Set<T> copySet(final Collection<T> c) {
        return (c == null || c.isEmpty())
                ? Collections.emptySet()
                : Collections.unmodifiableSet(new HashSet<>(c));
    }
    
    public static <K, V> Map<K, V> copyMap(final Map<K, V> m) {
        return (m == null || m.isEmpty())
                ? Collections.emptyMap()
                : Collections.unmodifiableMap(new HashMap<>(m));
    }    
    
    public static <T extends CBitfield> int flagsToBitfield(final Set<T> field) {
        return field.stream()
                .mapToInt(CBitfield::getValue)
                .reduce(0, (a, b) -> a | b);
    }
    
    public static <T extends CBitfield> Set<T> bitfieldToFlags(final int bitfield, final Stream<T> values) {
        return Collections.unmodifiableSet(values
                .filter(e -> (e.getValue() & bitfield) != 0)
                .collect(Collectors.toSet()));
    }
    
    public static <T extends CEnum> Optional<T> valueOf(final int value, final Stream<T> values) {
        return values
                .filter(e -> e.getValue() == value)
                .findFirst();
    }
}
