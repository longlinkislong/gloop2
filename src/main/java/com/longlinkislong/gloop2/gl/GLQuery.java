/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

/**
 *
 * @author zmichaels
 */
public class GLQuery extends GLObject<Object, GLQuery> {    
    public GLQuery() {        
        this.handle = GL15.glGenQueries();
    }
    
    public static final class QueryTask implements AutoCloseable {
        GLQuery host;
        GLQueryTarget target;
        
        private QueryTask(final GLQuery host, final GLQueryTarget target) {
            this.host = host;
            this.target = target;
            
            GL15.glBeginQuery(target.value, host.handle);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "BeginQuery";
        }
        
        public boolean isValid() {
            return this.host != null && this.host.isValid();
        }
        
        @Override
        public void close() {
            if (this.isValid()) {
                GL15.glEndQuery(this.target.value);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "EndQuery";
                
                this.host = null;
                this.target = null;
            }
        }
        
    }
            
    public QueryTask begin(final GLQueryTarget target) {
        return new QueryTask(this, target);
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GL15.glDeleteQueries(this.handle);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "DeleteQueries";
            
            this.info = null;
            this.handle = 0;
        }
    }
    
    public boolean isResultAvailable() {
        assert this.isValid() : "GLQuery is not valid!";
        
        final boolean out = GL15.glGetQueryObjectui(this.handle, GL15.GL_QUERY_RESULT_AVAILABLE) == GL11.GL_TRUE;
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetQueryObjectui";
        
        return out;
    }
    
    public int getResult() {
        assert this.isValid() : "GLQuery is not valid!";        
        
        final int out = GL15.glGetQueryObjectui(this.handle, GL15.GL_QUERY_RESULT);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetQueryObjectui";
        
        return out;
    }
}
