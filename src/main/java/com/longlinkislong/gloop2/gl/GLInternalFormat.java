/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL21;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL33;
import org.lwjgl.opengl.GL41;

/**
 *
 * @author zmichaels
 */
public enum GLInternalFormat implements CEnum {
    R8(GL30.GL_R8, 8, 0, 0, 0, 0, 0),
    R8I(GL30.GL_R8I, 8, 0, 0, 0, 0, 0),
    R8UI(GL30.GL_R8UI, 8, 0, 0, 0, 0, 0),
    R8_SNORM(GL31.GL_R8_SNORM, 8, 0, 0, 0, 0, 0),
    R16(GL30.GL_R16, 16, 0, 0, 0, 0, 0),
    R16I(GL30.GL_R16I, 16, 0, 0, 0, 0, 0),
    R16UI(GL30.GL_R16UI, 16, 0, 0, 0, 0, 0),
    R16F(GL30.GL_R16F, 16, 0, 0, 0, 0, 0),
    R32I(GL30.GL_R32I, 32, 0, 0, 0, 0, 0),
    R32UI(GL30.GL_R32UI, 32, 0, 0, 0, 0, 0),
    R32F(GL30.GL_R32F, 32, 0, 0, 0, 0, 0),
    RG8(GL30.GL_RG8, 8, 8, 0, 0, 0, 0),
    RG8I(GL30.GL_RG8I, 8, 8, 0, 0, 0, 0),
    RG8UI(GL30.GL_RG8UI, 8, 8, 0, 0, 0, 0),
    RG8_SNORM(GL31.GL_RG8_SNORM, 8, 8, 0, 0, 0, 0),
    RG16(GL30.GL_RG16, 16, 16, 0, 0, 0, 0),
    RG16I(GL30.GL_RG16I, 16, 16, 0, 0, 0, 0),
    RG16UI(GL30.GL_RG16UI, 16, 16, 0, 0, 0, 0),
    RG16F(GL30.GL_RG16F, 16, 16, 0, 0, 0, 0),
    RG32I(GL30.GL_RG32I, 32, 32, 0, 0, 0, 0),
    RG32UI(GL30.GL_RG32UI, 32, 32, 0, 0, 0, 0),
    RG32F(GL30.GL_RG32F, 32, 32, 0, 0, 0, 0),
    RGB5_A1(GL11.GL_RGB5_A1, 5, 5, 5, 1, 0, 0),
    RGB565(GL41.GL_RGB565, 5, 6, 5, 0, 0, 0),
    RGB8(GL11.GL_RGB8, 8, 8, 8, 0, 0, 0),
    RGB8I(GL30.GL_RGB8I, 8, 8, 8, 0, 0, 0),
    RGB8UI(GL30.GL_RGB8UI, 8, 8, 8, 0, 0, 0),
    RGB8_SNORM(GL31.GL_RGB8_SNORM, 8, 8, 8, 0, 0, 0),
    RGB9_E5(GL30.GL_RGB9_E5, 9, 9, 9, 5, 0, 0),
    RGB10_A2(GL11.GL_RGB10_A2, 10, 10, 10, 2, 0, 0),
    RGB10_A2UI(GL33.GL_RGB10_A2UI, 10, 10, 10, 2, 0, 0),
    RGB16(GL11.GL_RGB16, 16, 16, 16, 0, 0, 0),
    RGB16I(GL30.GL_RGB16I, 16, 16, 16, 0, 0, 0),
    RGB16UI(GL30.GL_RGB16UI, 16, 16, 16, 0, 0, 0),
    RGB16F(GL30.GL_RGB16F, 16, 16, 16, 0, 0, 0),
    RGB32I(GL30.GL_RGB32I, 32, 32, 32, 0, 0, 0),
    RGB32UI(GL30.GL_RGB32UI, 32, 32, 32, 0, 0, 0),
    RGB32F(GL30.GL_RGB32F, 32, 32, 32, 0, 0, 0),
    SRGB8(GL21.GL_SRGB8, 8, 8, 8, 0, 0, 0),
    RGBA4(GL11.GL_RGBA4, 4, 4, 4, 4, 0, 0),
    RGBA8(GL11.GL_RGBA8, 8, 8, 8, 8, 0, 0),
    RGBA8I(GL30.GL_RGBA8I, 8, 8, 8, 8, 0, 0),
    RGBA8UI(GL30.GL_RGBA8UI, 8, 8, 8, 8, 0, 0),
    RGBA8_SNORM(GL31.GL_RGBA8_SNORM, 8, 8, 8, 8, 0, 0),
    RGBA16(GL11.GL_RGBA16, 16, 16, 16, 16, 0, 0),
    RGBA16I(GL30.GL_RGBA16I, 16, 16, 16, 16, 0, 0),
    RGBA16UI(GL30.GL_RGBA16UI, 16, 16, 16, 16, 0, 0),
    RGBA16F(GL30.GL_RGBA16F, 16, 16, 16, 16, 0, 0),
    RGBA32I(GL30.GL_RGBA32I, 32, 32, 32, 32, 0, 0),
    RGBA32UI(GL30.GL_RGBA32UI, 32, 32, 32, 32, 0, 0),
    RGBA32F(GL30.GL_RGBA32F, 32, 32, 32, 32, 0, 0),
    SRGB8_ALPHA8(GL21.GL_SRGB8_ALPHA8, 8, 8, 8, 8, 0, 0),
    R11F_G11F_B10F(GL30.GL_R11F_G11F_B10F, 11, 11, 10, 0, 0, 0),
    DEPTH_COMPONENT16(GL14.GL_DEPTH_COMPONENT16, 0, 0, 0, 0, 16, 0),
    DEPTH_COMPONENT24(GL14.GL_DEPTH_COMPONENT24, 0, 0, 0, 0, 24, 0),
    DEPTH_COMPONENT32F(GL30.GL_DEPTH_COMPONENT32F, 0, 0, 0, 0, 32, 0),
    DEPTH24_STENCIL8(GL30.GL_DEPTH24_STENCIL8, 0, 0, 0, 0, 24, 8),
    DEPTH32F_STENCIL8(GL30.GL_DEPTH32F_STENCIL8, 0, 0, 0, 0, 32, 8);

    public static final GLInternalFormat R8_UNORM = R8;    
    public static final GLInternalFormat R8_SINT = R8I;
    public static final GLInternalFormat R8_UINT = R8UI;       
    
    public static final GLInternalFormat R8G8_UNORM = RG8;
    public static final GLInternalFormat R8G8_SINT = RG8I;
    public static final GLInternalFormat R8G8_UINT = RG8UI;
    public static final GLInternalFormat R8G8_SNORM = RG8_SNORM;
    
    public static final GLInternalFormat R8G8B8_UNORM = RGB8;
    public static final GLInternalFormat R8G8B8_SINT = RGB8I;
    public static final GLInternalFormat R8G8B8_UINT = RGB8UI;
    public static final GLInternalFormat R8G8B8_SNORM = RGB8_SNORM;
    
    public static final GLInternalFormat R8G8B8A8_UNORM = RGBA8;
    public static final GLInternalFormat R8G8B8A8_SNORM = RGBA8_SNORM;
    public static final GLInternalFormat R8G8B8A8_SINT = RGBA8I;
    public static final GLInternalFormat R8G8B8A8_UINT = RGBA8UI;
    
    public static final GLInternalFormat R16_UNORM = R16;
    public static final GLInternalFormat R16_SINT = R16I;
    public static final GLInternalFormat R16_UINT = R16UI;
    public static final GLInternalFormat R16_SFLOAT = R16F;
    
    public static final GLInternalFormat R16G16_UNORM = RG16;    
    public static final GLInternalFormat R16G16_SINT = RG16I;
    public static final GLInternalFormat R16G16_UINT = RG16UI;
    public static final GLInternalFormat R16G16_SFLOAT = RG16F;
    
    public static final GLInternalFormat R16G16B16_UNORM = RGB16;
    public static final GLInternalFormat R16G16B16_SINT = RGB16I;
    public static final GLInternalFormat R16G16B16_UINT = RGB16UI;
    public static final GLInternalFormat R16G16B16_SFLOAT = RGB16F;
    
    public static final GLInternalFormat R16G16B16A16_UNORM = RGBA16;
    public static final GLInternalFormat R16G16B16A16_SINT = RGBA16I;
    public static final GLInternalFormat R16G16B16A16_UINT = RGBA16UI;
    public static final GLInternalFormat R16G16B16A16_SFLOAT = RGBA16F;
    
    public static final GLInternalFormat R32_SINT = R32I;
    public static final GLInternalFormat R32_UINT = R32UI;
    public static final GLInternalFormat R32_SFLOAT = R32F;
    
    public static final GLInternalFormat R32G32_SINT = RG32I;
    public static final GLInternalFormat R32G32_UINT = RG32UI;
    public static final GLInternalFormat R32G32_SFLOAT = RG32F;
    
    public static final GLInternalFormat R32G32B32_SINT = RGB32I;
    public static final GLInternalFormat R32G32B32_UINT = RGB32UI;
    public static final GLInternalFormat R32G32B32_SFLOAT = RGB32F;
    
    public static final GLInternalFormat R32G32B32A32_SINT = RGBA32I;
    public static final GLInternalFormat R32G32B32A32_UINT = RGBA32UI;
    public static final GLInternalFormat R32G32B32A32_SFLOAT = RGBA32F;
    
    public static final GLInternalFormat R5G5B5A1_UNORM = RGB5_A1;
    public static final GLInternalFormat R5G6B5_UNORM = RGB565;
    public static final GLInternalFormat R9G9B9E5_UNORM = RGB9_E5;
    
    public static final GLInternalFormat R10G10B10A2_UNORM = RGB10_A2;
    public static final GLInternalFormat R10G10B10A2_UINT = RGB10_A2UI;
    public static final GLInternalFormat R8G8B8_SRGB = SRGB8;
    public static final GLInternalFormat R4G4B4A4_UNORM = RGBA4;
    public static final GLInternalFormat R8G8B8A8_SRGB = SRGB8_ALPHA8;
    public static final GLInternalFormat R11G11B10_SFLOAT = R11F_G11F_B10F;
    
    
    public final int value;
    public final int redBits, greenBits, blueBits, alphaBits;
    public final int depthBits, stencilBits;

    public boolean isColor() {
        return (redBits > 0 || greenBits > 0 || blueBits > 0 || alphaBits > 0);
    }

    public boolean isDepth() {
        return this.depthBits > 0;
    }

    public boolean isStencil() {
        return this.stencilBits > 0;
    }

    public boolean isDepthStencil() {
        return this.isDepth() && this.isStencil();
    }

    private GLInternalFormat(final int value,
            final int redBits, final int greenBits, final int blueBits, final int alphaBits,
            final int depthBits, final int stencilBits) {

        this.value = value;
        this.redBits = redBits;
        this.greenBits = greenBits;
        this.blueBits = blueBits;
        this.alphaBits = alphaBits;
        this.depthBits = depthBits;
        this.stencilBits = stencilBits;
    }

    public static Optional<GLInternalFormat> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
