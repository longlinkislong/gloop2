/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL40;
import org.lwjgl.opengl.GL45;

/**
 *
 * @author zmichaels
 */
public class GLTransformFeedback extends GLObject<Object, GLTransformFeedback> {

    public static enum PrimitiveMode implements CEnum {
        TRIANGLES(GL11.GL_TRIANGLES),
        LINES(GL11.GL_LINES),
        POINTS(GL11.GL_POINTS);

        public final int value;

        private PrimitiveMode(final int value) {
            this.value = value;
        }

        public static Optional<PrimitiveMode> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }

        @Override
        public int getValue() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    public GLTransformFeedback() {
        this.handle = GL45.glCreateTransformFeedbacks();
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "CreateTransformFeedbacks";
    }

    @Override
    public void close() {
        if (this.isValid()) {
            GL40.glDeleteTransformFeedbacks(handle);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "DeleteTransformFeedbacks";
            
            this.handle = 0;
        }
    }

    public static final class TransformTask implements AutoCloseable {

        private TransformTask(final PrimitiveMode primitiveMode) {
            GL30.glBeginTransformFeedback(primitiveMode.value);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "BeginTransformFeedback";
        }

        @Override
        public void close() {
            GL30.glEndTransformFeedback();
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "EndTransformFeedback";
        }
    }
    
    public void bind() {
        assert this.isValid() : "GLTransformFeedback is not valid!";
        
        GL40.glBindTransformFeedback(GL40.GL_TRANSFORM_FEEDBACK, this.handle);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindTransformFeedback";
    }
    
    public void pause() {
        assert this.isValid() : "GLTransformFeedback is not valid!";
        
        GL40.glPauseTransformFeedback();
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "PauseTransformFeedback";
    }
    
    public void resume() {
        assert this.isValid() : "GLTransformFeedback is not valid!";
        
        GL40.glResumeTransformFeedback();
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ResumeTransformFeedback";
    }

    public TransformTask begin(final PrimitiveMode primitiveMode) {
        assert this.isValid() : "GLTransformFeedback is not valid!";

        return new TransformTask(primitiveMode);
    }
}
