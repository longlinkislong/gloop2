/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL41;

/**
 *
 * @author zmichaels
 */
public class GLClearStateInfo {

    public final Set<GLClearBuffer> buffers;
    public final float r;
    public final float g;
    public final float b;
    public final float a;

    public final float depth;
    public final int stencil;

    public GLClearStateInfo(
            final Collection<GLClearBuffer> buffers,
            final float r, final float g, final float b, final float a,
            final float depth, final int stencil) {

        this.buffers = Tools.copySet(buffers);
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
        this.depth = depth;
        this.stencil = stencil;
    }

    public GLClearStateInfo() {
        this(null, 0F, 0F, 0F, 0F, 1F, 0);
    }

    public GLClearStateInfo withColor(final float r, final float g, final float b, final float a) {
        return new GLClearStateInfo(buffers, r, g, b, a, depth, stencil);
    }

    public GLClearStateInfo withBuffers(final Collection<GLClearBuffer> buffers) {
        return new GLClearStateInfo(buffers, r, g, b, a, depth, stencil);
    }

    public GLClearStateInfo withBuffers(final GLClearBuffer... buffers) {
        return withBuffers(Arrays.asList(buffers));
    }

    public GLClearStateInfo withDepth(final float depth) {
        return new GLClearStateInfo(buffers, r, g, b, a, depth, stencil);
    }

    public GLClearStateInfo withStencil(final int stencil) {
        return new GLClearStateInfo(buffers, r, g, b, a, depth, stencil);
    }

    public void apply() {
        if (this.buffers.contains(GLClearBuffer.COLOR)) {            
            GL11.glClearColor(r, g, b, a);
        }

        if (this.buffers.contains(GLClearBuffer.DEPTH)) {
            GL41.glClearDepthf(depth);
        }

        if (this.buffers.contains(GLClearBuffer.STENCIL)) {
            GL11.glClearStencil(stencil);
        }

        GL11.glClear(GLClearBuffer.bits(buffers));
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ClearStateInfo";
    }
}
