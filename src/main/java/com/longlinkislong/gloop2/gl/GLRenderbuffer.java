/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL45;
import org.lwjgl.opengl.GLCapabilities;

/**
 *
 * @author zmichaels
 */
public class GLRenderbuffer extends GLObject<GLRenderbufferInfo, GLRenderbuffer> {

    public GLRenderbuffer() {
    }

    public GLRenderbuffer(final GLRenderbufferInfo info) {
        this.info = info;

        final GLCapabilities caps = GL.getCapabilities();

        if (caps.OpenGL45) {
            this.handle = GL45.glCreateRenderbuffers();
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "CreateRenderbuffers";

            if (info.samples > 1) {
                GL45.glNamedRenderbufferStorageMultisample(handle, info.samples, info.format.value, info.width, info.height);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "NamedRenderbufferStorageMultisample";
            } else {
                GL45.glNamedRenderbufferStorage(handle, info.format.value, info.width, info.height);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "NamedRenderbufferStorage";
            }
        } else {
            this.handle = GL30.glGenRenderbuffers();
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "GenRenderbuffers";
            
            GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, handle);
            
            if (info.samples > 1) {
                GL30.glRenderbufferStorageMultisample(GL30.GL_RENDERBUFFER, info.samples, info.format.value, info.width, info.height);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "RenderbufferStorageMultisample";
            } else {
                GL30.glRenderbufferStorage(GL30.GL_RENDERBUFFER, info.format.value, info.width, info.height);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "RenderbufferStorage";
            }
            
            GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, 0);
        }
    }

    @Override
    public void close() {
        if (this.isValid()) {
            GL30.glDeleteRenderbuffers(this.handle);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "DeleteRenderbuffers";
            this.handle = 0;
            this.info = null;
        }
    }
}
