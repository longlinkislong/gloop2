/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

/**
 *
 * @author zmichaels
 */
public class GLStencilOpState {
    public final GLStencilOp failOp;
    public final GLStencilOp passOp;
    public final GLStencilOp depthFailOp;
    public final GLCompareOp compareOp;
    public final int reference;
    public final int compareMask;
    public final int writeMask;
    
    public GLStencilOpState(
            final GLStencilOp failOp, final GLStencilOp passOp, final GLStencilOp depthFailOp,
            final GLCompareOp compareOp, final int reference, final int compareMask,
            final int writeMask) {
        
        this.failOp = failOp;
        this.passOp = passOp;
        this.depthFailOp = depthFailOp;
        this.compareOp = compareOp;
        this.reference = reference;
        this.compareMask = compareMask;
        this.writeMask = writeMask;
    }
    
    public GLStencilOpState() {
        this(GLStencilOp.KEEP, GLStencilOp.KEEP, GLStencilOp.KEEP, GLCompareOp.ALWAYS, 0, 0xFFFFFFFF, 0xFFFFFFFF);
    }
    
    public GLStencilOpState withFailOp(final GLStencilOp failOp) {
        return new GLStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
    
    public GLStencilOpState withPassOp(final GLStencilOp passOp) {
        return new GLStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
    
    public GLStencilOpState withDepthFailOp(final GLStencilOp depthFailOp) {
        return new GLStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
    
    public GLStencilOpState withCompareOp(final GLCompareOp compareOp) {
        return new GLStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
    
    public GLStencilOpState withReference(final int reference) {
        return new GLStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
    
    public GLStencilOpState withCompareMask(final int compareMask) {
        return new GLStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }        
    
    public GLStencilOpState withWriteMask(final int writeMask) {
        return new GLStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
}
