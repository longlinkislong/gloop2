/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL20;

/**
 *
 * @author zmichaels
 */
public class GLBlendStateInfo {

    public final boolean blendEnable;
    public final GLBlendEquation colorBlendOp;
    public final GLBlendEquation alphaBlendOp;
    public final GLBlendFunction srcColorBlend;
    public final GLBlendFunction dstColorBlend;
    public final GLBlendFunction srcAlphaBlend;
    public final GLBlendFunction dstAlphaBlend;
    public final int colorWriteMask;

    public GLBlendStateInfo(
            final boolean blendEnable,
            final GLBlendEquation colorBlendOp, final GLBlendEquation alphaBlendOp,
            final GLBlendFunction srcColorBlend, final GLBlendFunction srcAlphaBlend,
            final GLBlendFunction dstColorBlend, final GLBlendFunction dstAlphaBlend,
            final int colorWriteMask) {

        this.blendEnable = blendEnable;
        this.colorBlendOp = colorBlendOp;
        this.alphaBlendOp = alphaBlendOp;
        this.srcColorBlend = srcColorBlend;
        this.dstColorBlend = dstColorBlend;
        this.srcAlphaBlend = srcAlphaBlend;
        this.dstAlphaBlend = dstAlphaBlend;
        this.colorWriteMask = colorWriteMask;
    }

    public GLBlendStateInfo() {
        this(false,
                GLBlendEquation.ADD, GLBlendEquation.ADD,
                GLBlendFunction.ONE, GLBlendFunction.ONE,
                GLBlendFunction.ZERO, GLBlendFunction.ZERO, 0xFF);
    }
    
    public GLBlendStateInfo withColorMask(final int colorWriteMask) {
        return new GLBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }

    public GLBlendStateInfo withBlendEnable(final boolean blendEnable) {
        return new GLBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }
    
    public GLBlendStateInfo withColorBlendOp(final GLBlendEquation colorBlendOp) {
        return new GLBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }
    
    public GLBlendStateInfo withAlphaBlendOp(final GLBlendEquation alphaBlendOp) {
        return new GLBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }
    
    public GLBlendStateInfo withSrcColorBlend(final GLBlendFunction srcColorBlend) {
        return new GLBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }
    
    public GLBlendStateInfo withDstColorBlend(final GLBlendFunction dstColorBlend) {
        return new GLBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }
    
    public GLBlendStateInfo withSrcAlphaBlend(final GLBlendFunction srcAlphaBlend) {
        return new GLBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }
    
    public GLBlendStateInfo withDstAlphaBlend(final GLBlendFunction dstAlphaBlend) {
        return new GLBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }

    public void apply() {
        if (this.blendEnable) {
            GL11.glEnable(GL11.GL_BLEND);
            GL14.glBlendFuncSeparate(this.srcColorBlend.value, this.dstColorBlend.value, this.srcAlphaBlend.value, this.dstAlphaBlend.value);
            GL20.glBlendEquationSeparate(this.colorBlendOp.value, this.alphaBlendOp.value);
                        
            GL11.glColorMask((colorWriteMask & 0x1) != 0, (colorWriteMask & 0x2) != 0, (colorWriteMask & 0x4) != 0, (colorWriteMask & 0x8) != 0);
        } else {
            GL11.glDisable(GL11.GL_BLEND);
        }
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "BlendStateInfo";
    }
}
