/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.EXTTextureFilterAnisotropic;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL33;
import org.lwjgl.opengl.GL45;
import org.lwjgl.opengl.GLCapabilities;

/**
 *
 * @author zmichaels
 */
final class GLTextureHelper {

    private GLTextureHelper() {
    }

    static int create45(final GLTextureInfo info) {
        final GLTextureTarget tgt = info.target();
        final int handle = GL45.glCreateTextures(tgt.value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "CreateTextures";

        GL45.glTextureParameteri(handle, GL11.GL_TEXTURE_MIN_FILTER, info.samplerInfo.minFilter.value);
        GL45.glTextureParameteri(handle, GL11.GL_TEXTURE_MAG_FILTER, info.samplerInfo.magFilter.value);

        GL45.glTextureParameteri(handle, GL11.GL_TEXTURE_WRAP_S, info.samplerInfo.wrapS.value);
        GL45.glTextureParameteri(handle, GL11.GL_TEXTURE_WRAP_T, info.samplerInfo.wrapT.value);

        final GLCapabilities caps = GL.getCapabilities();        

        if (tgt == GLTextureTarget.TEXTURE_3D) {
            GL45.glTextureParameteri(handle, GL12.GL_TEXTURE_WRAP_R, info.samplerInfo.wrapR.value);
        }

        GL45.glTextureParameterf(handle, GL12.GL_TEXTURE_MIN_LOD, info.samplerInfo.minLOD);
        GL45.glTextureParameterf(handle, GL12.GL_TEXTURE_MAX_LOD, info.samplerInfo.maxLOD);

        GL45.glTextureParameteri(handle, GL12.GL_TEXTURE_MAX_LEVEL, info.levelCount - 1);
        GL45.glTextureParameteri(handle, GL12.GL_TEXTURE_BASE_LEVEL, 0);

        if (info.samplerInfo.swizzle.r != GLSamplerInfo.Swizzle.Mode.IDENTITY) {
            GL45.glTextureParameteri(handle, GL33.GL_TEXTURE_SWIZZLE_R, info.samplerInfo.swizzle.r.value);
        }

        if (info.samplerInfo.swizzle.g != GLSamplerInfo.Swizzle.Mode.IDENTITY) {
            GL45.glTextureParameteri(handle, GL33.GL_TEXTURE_SWIZZLE_G, info.samplerInfo.swizzle.g.value);
        }

        if (info.samplerInfo.swizzle.b != GLSamplerInfo.Swizzle.Mode.IDENTITY) {
            GL45.glTextureParameteri(handle, GL33.GL_TEXTURE_SWIZZLE_B, info.samplerInfo.swizzle.b.value);
        }

        if (info.samplerInfo.swizzle.a != GLSamplerInfo.Swizzle.Mode.IDENTITY) {
            GL45.glTextureParameteri(handle, GL33.GL_TEXTURE_SWIZZLE_A, info.samplerInfo.swizzle.a.value);
        }

        if (caps.GL_EXT_texture_filter_anisotropic) {
            GL45.glTextureParameterf(handle, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, info.samplerInfo.maxAnisotropy);
        }

        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureParameter";
        
        switch (tgt) {
            case TEXTURE_1D:
                init1DGL45(handle, info);
                break;
            case TEXTURE_1D_ARRAY:
                init1DArrayGL45(handle, info);
                break;
            case TEXTURE_2D:
                init2DGL45(handle, info);
                break;
            case TEXTURE_2D_ARRAY:
                init2DArrayGL45(handle, info);
                break;
            case TEXTURE_3D:
                init3DGL45(handle, info);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported target type: " + tgt);
        }

        return handle;
    }

    static int create33(final GLTextureInfo info) {
        final GLTextureTarget tgt = info.target();
        final int handle = GL11.glGenTextures();
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GenTextures";

        GL11.glBindTexture(tgt.value, handle);

        GL11.glTexParameteri(tgt.value, GL11.GL_TEXTURE_MIN_FILTER, info.samplerInfo.minFilter.value);
        GL11.glTexParameteri(tgt.value, GL11.GL_TEXTURE_MAG_FILTER, info.samplerInfo.magFilter.value);

        GL11.glTexParameteri(tgt.value, GL11.GL_TEXTURE_WRAP_S, info.samplerInfo.wrapS.value);
        GL11.glTexParameteri(tgt.value, GL11.GL_TEXTURE_WRAP_T, info.samplerInfo.wrapT.value);

        final GLCapabilities caps = GL.getCapabilities();        

        if (tgt == GLTextureTarget.TEXTURE_3D) {
            GL11.glTexParameteri(tgt.value, GL12.GL_TEXTURE_WRAP_R, info.samplerInfo.wrapR.value);
        }

        GL11.glTexParameterf(tgt.value, GL12.GL_TEXTURE_MIN_LOD, info.samplerInfo.minLOD);
        GL11.glTexParameterf(tgt.value, GL12.GL_TEXTURE_MAX_LOD, info.samplerInfo.maxLOD);

        GL11.glTexParameteri(tgt.value, GL12.GL_TEXTURE_MAX_LEVEL, info.levelCount - 1);
        GL11.glTexParameteri(tgt.value, GL12.GL_TEXTURE_BASE_LEVEL, 0);

        if (info.samplerInfo.swizzle.r != GLSamplerInfo.Swizzle.Mode.IDENTITY) {
            GL11.glTexParameteri(tgt.value, GL33.GL_TEXTURE_SWIZZLE_R, info.samplerInfo.swizzle.r.value);
        }

        if (info.samplerInfo.swizzle.g != GLSamplerInfo.Swizzle.Mode.IDENTITY) {
            GL11.glTexParameteri(tgt.value, GL33.GL_TEXTURE_SWIZZLE_G, info.samplerInfo.swizzle.g.value);
        }

        if (info.samplerInfo.swizzle.b != GLSamplerInfo.Swizzle.Mode.IDENTITY) {
            GL11.glTexParameteri(tgt.value, GL33.GL_TEXTURE_SWIZZLE_B, info.samplerInfo.swizzle.b.value);
        }

        if (info.samplerInfo.swizzle.a != GLSamplerInfo.Swizzle.Mode.IDENTITY) {
            GL11.glTexParameteri(tgt.value, GL33.GL_TEXTURE_SWIZZLE_A, info.samplerInfo.swizzle.a.value);
        }

        if (caps.GL_EXT_texture_filter_anisotropic) {
            GL11.glTexParameterf(tgt.value, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, info.samplerInfo.maxAnisotropy);
        }
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexParameter";

        switch (tgt) {
            case TEXTURE_1D:
                init1DGL11(info);
                break;
            case TEXTURE_1D_ARRAY:
                init1DArrayGL30(info);
                break;
            case TEXTURE_2D:
                init2DGL11(info);
                break;
            case TEXTURE_2D_ARRAY:
                init2DArrayGL30(info);
                break;
            case TEXTURE_3D:
                init3DGL11(info);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported target type: " + tgt);
        }

        GL11.glBindTexture(tgt.value, 0);

        return handle;
    }

    private static void init3DGL11(final GLTextureInfo info) {
        if (!info.initialData.isEmpty()) {
            int w = info.width;
            int h = info.height;
            int d = info.depth;

            for (int i = 0; i < info.levelCount; i++) {
                final GLPixelInfo px = info.initialData.get(i);

                GL12.glTexImage3D(GL12.GL_TEXTURE_3D, i, info.format.value, w, h, d, 0, px.format.value, px.type.value, px.data);
                w = Math.max(1, w >> 1);
                h = Math.max(1, h >> 1);
                d = Math.max(1, d >> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexImage3D";
        } else {
            int w = info.width;
            int h = info.height;
            int d = info.depth;

            for (int i = 0; i < info.levelCount; i++) {
                GL12.glTexImage3D(GL12.GL_TEXTURE_3D, i, info.format.value, w, h, d, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, 0L);
                w = Math.max(1, w >> 1);
                h = Math.max(1, h >> 1);
                d = Math.max(1, d >> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexImage3D";
        }
    }

    private static void init2DGL11(final GLTextureInfo info) {
        if (!info.initialData.isEmpty()) {
            int w = info.width;
            int h = info.height;

            for (int i = 0; i < info.levelCount; i++) {
                final GLPixelInfo px = info.initialData.get(i);

                GL11.glTexImage2D(GL11.GL_TEXTURE_2D, i, info.format.value, w, h, 0, px.format.value, px.type.value, px.data);
                w = Math.max(1, w >> 1);
                h = Math.max(1, h >> 1);                                
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexImage2D";
        } else {
            int w = info.width;
            int h = info.height;

            for (int i = 0; i < info.levelCount; i++) {
                GL11.glTexImage2D(GL11.GL_TEXTURE_2D, i, info.format.value, w, h, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, 0L);
                w = Math.max(1, w >> 1);
                h = Math.max(1, h >> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexImage2D";
        }
    }
    
    private static void init2DArrayGL30(final GLTextureInfo info) {
        if (!info.initialData.isEmpty()) {
            int w = info.width;
            int h = info.height;
            
            for (int i = 0; i < info.levelCount; i++) {
                GL12.glTexImage3D(GL30.GL_TEXTURE_2D_ARRAY, i, info.format.value, w, h, info.layerCount, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, 0L);
             
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexImage3D";
                
                for (int j = 0; j < info.layerCount; j++) {
                    final GLPixelInfo px = info.initialData.get(i * info.levelCount + j);
                    
                    GL12.glTexSubImage3D(GL30.GL_TEXTURE_2D_ARRAY, i, 0, 0, j, w, h, 1, px.format.value, px.type.value, px.data);                    
                }
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage3D";
                
                w = Math.max(1, w >>> 1);
                h = Math.max(1, h >>> 1);
            }                        
        } else {
            int w = info.width;
            int h = info.height;
            
            for (int i = 0; i < info.levelCount; i++) {
                GL12.glTexImage3D(GL30.GL_TEXTURE_2D_ARRAY, i, info.format.value, w, h, info.layerCount, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, 0L);
                
                w = Math.max(1, w >>> 1);
                h = Math.max(1, h >>> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexImage3D";
        }
    }

    private static void init1DArrayGL30(final GLTextureInfo info) {
        if (!info.initialData.isEmpty()) {
            int w = info.width;

            for (int i = 0; i < info.levelCount; i++) {                
                GL11.glTexImage2D(GL30.GL_TEXTURE_1D_ARRAY, i, info.format.value, w, info.layerCount, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, 0L);

                assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexImage2D";
                
                for (int j = 0; j < info.layerCount; j++) {
                    final GLPixelInfo px = info.initialData.get(i * info.levelCount + j);

                    GL11.glTexSubImage2D(GL30.GL_TEXTURE_1D_ARRAY, i, 0, j, w, 1, px.format.value, px.type.value, px.data);
                }

                assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage2D";
                w = Math.max(1, w >>> 1);
            }
        } else {
            int w = info.width;
            
            for (int i = 0; i < info.levelCount; i++) {
                GL11.glTexImage2D(GL30.GL_TEXTURE_1D_ARRAY, i, info.format.value, w, info.layerCount, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, 0L);
                w = Math.max(1, w >>> 1);
            }
        }
    }

    private static void init1DGL11(final GLTextureInfo info) {
        if (!info.initialData.isEmpty()) {
            int w = info.width;

            for (int i = 0; i < info.levelCount; i++) {
                final GLPixelInfo px = info.initialData.get(i);

                GL11.glTexImage1D(GL11.GL_TEXTURE_1D, i, info.format.value, w, 0, px.format.value, px.type.value, px.data);
                w = Math.max(1, w >>> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexImage1D";
        } else {
            int w = info.width;

            for (int i = 0; i < info.levelCount; i++) {
                GL11.glTexImage1D(GL11.GL_TEXTURE_1D, i, info.format.value, w, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, 0L);
                w = Math.max(1, w >>> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexImage1D";
        }
    }

    private static void init3DGL45(final int handle, GLTextureInfo info) {
        GL45.glTextureStorage3D(handle, info.levelCount, info.format.value, info.width, info.height, info.depth);

        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureStorage3D";
        
        if (!info.initialData.isEmpty()) {
            int w = info.width;
            int h = info.height;
            int d = info.depth;

            for (int i = 0; i < info.levelCount; i++) {
                final GLPixelInfo px = info.initialData.get(i);

                GL45.glTextureSubImage3D(handle, i, 0, 0, 0, w, h, d, px.format.value, px.type.value, px.data);
                w = Math.max(1, w >>> 1);
                h = Math.max(1, h >>> 1);
                d = Math.max(1, d >>> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage3D";
        }
    }

    private static void init2DGL45(final int handle, final GLTextureInfo info) {
        GL45.glTextureStorage2D(handle, info.levelCount, info.format.value, info.width, info.height);

        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureStorage2D";
        
        if (!info.initialData.isEmpty()) {
            int w = info.width;
            int h = info.height;

            for (int i = 0; i < info.levelCount; i++) {
                final GLPixelInfo px = info.initialData.get(i);

                GL45.glTextureSubImage2D(handle, i, 0, 0, w, h, px.format.value, px.type.value, px.data);
                w = Math.max(1, w >>> 1);
                h = Math.max(1, h >>> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage2D";
        }
    }

    private static void init2DArrayGL45(final int handle, final GLTextureInfo info) {
        GL45.glTextureStorage3D(handle, info.levelCount, info.format.value, info.width, info.height, info.layerCount);

        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureStorage3D";
        
        if (!info.initialData.isEmpty()) {
            int w = info.width;
            int h = info.height;

            for (int i = 0; i < info.levelCount; i++) {
                for (int j = 0; j < info.layerCount; j++) {
                    final GLPixelInfo px = info.initialData.get(i * info.levelCount + j);

                    GL45.glTextureSubImage3D(handle, i, 0, 0, j, w, h, 1, px.format.value, px.type.value, px.data);
                }
                
                w = Math.max(1, w >>> 1);
                h = Math.max(1, h >>> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage3D";
        }
    }

    private static void init1DArrayGL45(final int handle, final GLTextureInfo info) {
        GL45.glTextureStorage2D(handle, info.levelCount, info.format.value, info.width, info.layerCount);

        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureStorage2D";
        
        if (!info.initialData.isEmpty()) {
            int w = info.width;

            for (int i = 0; i < info.levelCount; i++) {
                for (int j = 0; j < info.layerCount; j++) {
                    final GLPixelInfo px = info.initialData.get(i * info.levelCount + j);

                    GL45.glTextureSubImage2D(handle, i, 0, j, w, 1, px.format.value, px.type.value, px.data);                    
                }
                
                w = Math.max(1, w >>> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage2D";
        }
    }

    private static void init1DGL45(final int handle, final GLTextureInfo info) {
        GL45.glTextureStorage1D(handle, info.levelCount, info.format.value, info.width);

        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureStorage1D";
        
        if (!info.initialData.isEmpty()) {
            int w = info.width;

            for (int i = 0; i < info.levelCount; i++) {
                final GLPixelInfo px = info.initialData.get(i);

                GL45.glTextureSubImage1D(handle, i, 0, w, px.format.value, px.type.value, px.data);
                w = Math.max(1, w >>> 1);
            }
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage1D";
        }
    }
}
