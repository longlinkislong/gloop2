/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.image.Image;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class GLTextureInfo {

    public final int width;
    public final int height;
    public final int depth;
    public final int levelCount;
    public final int layerCount;
    public final GLSamplerInfo samplerInfo;
    public final List<GLPixelInfo> initialData;
    public final GLInternalFormat format;

    public GLTextureInfo(
            final int width, final int height, final int depth,
            final int levelCount, final int layerCount,
            final GLSamplerInfo samplerInfo,
            final List<GLPixelInfo> data,
            final GLInternalFormat format) {

        this.width = width;
        this.height = height;
        this.depth = depth;
        this.levelCount = levelCount;
        this.samplerInfo = samplerInfo;
        this.initialData = Tools.copyList(data);
        this.format = format;
        this.layerCount = layerCount;
    }
    
    public GLTextureInfo() {
        this(0, 0, 0, 1, 1, new GLSamplerInfo(), null, GLInternalFormat.RGBA8);
    }
    
    public GLTextureInfo withExtent(final int width, final int height, final int depth) {
        return new GLTextureInfo(width, height, depth, levelCount, layerCount, samplerInfo, initialData, format);
    }
    
    public GLTextureInfo withLayers(final int layerCount) {
        return new GLTextureInfo(width, height, depth, levelCount, layerCount, samplerInfo, initialData, format);
    }
    
    public GLTextureInfo withLevels(final int levelCount) {
        return new GLTextureInfo(width, height, depth, levelCount, layerCount, samplerInfo, initialData, format);
    }
    
    public GLTextureInfo withSamplerInfo(final GLSamplerInfo samplerInfo) {
        return new GLTextureInfo(width, height, depth, levelCount, layerCount, samplerInfo, initialData, format);
    }
    
    public GLTextureInfo withData(final List<GLPixelInfo> initialData) {
        return new GLTextureInfo(width, height, depth, levelCount, layerCount, samplerInfo, initialData, format);
    }
    
    public GLTextureInfo withFormat(final GLInternalFormat format) {
        return new GLTextureInfo(width, height, depth, levelCount, layerCount, samplerInfo, initialData, format);
    }
    
    public GLTextureInfo withData(final GLPixelInfo... data) {
        return withData(Arrays.asList(data));
    }
    
    public GLTexture create() {
        return new GLTexture(this);
    }
    
    public GLTextureTarget target() {
        if (this.layerCount == 1) {
            if (this.height > 1) {
                if (this.depth > 1) {
                    return GLTextureTarget.TEXTURE_3D;
                } else {
                    return GLTextureTarget.TEXTURE_2D;
                }
            } else {
                return GLTextureTarget.TEXTURE_1D;
            }
        } else if (this.layerCount < 1) {
            throw new IllegalStateException("Level Count must be at least 1!");
        } else {
            if (this.height > 1) {
                return GLTextureTarget.TEXTURE_2D_ARRAY;
            } else {
                return GLTextureTarget.TEXTURE_1D_ARRAY;
            }
        }
    }
    
    public static GLTextureInfo wrapImage(final Image img) {
        return new GLTextureInfo()                
                .withExtent(img.getWidth(), img.getHeight(), 1)
                .withLayers(1)
                .withLevels(1)
                .withData(GLPixelInfo.of(img));
    }
}
