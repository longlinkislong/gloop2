/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Set;
import org.lwjgl.opengl.ARBShaderImageLoadStore;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL42;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GL44;
import org.lwjgl.opengl.GLCapabilities;

/**
 *
 * @author zmichaels
 */
public enum GLMemoryBarrier implements CBitfield {
    VERTEX_ATTRIB_ARRAY(GL42.GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT),
    ELEMENT_ARRAY(GL42.GL_ELEMENT_ARRAY_BARRIER_BIT),
    UNIFORM(GL42.GL_UNIFORM_BARRIER_BIT),
    TEXTURE_FETCH(GL42.GL_TEXTURE_FETCH_BARRIER_BIT),
    SHADER_IMAGE_ACCESS(GL42.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT),
    COMMAND(GL42.GL_COMMAND_BARRIER_BIT),
    PIXEL_BUFFER(GL42.GL_PIXEL_BUFFER_BARRIER_BIT),
    TEXTURE_UPDATE(GL42.GL_TEXTURE_UPDATE_BARRIER_BIT),
    BUFFER_UPDATE(GL42.GL_BUFFER_UPDATE_BARRIER_BIT),
    QUERY_BUFFER(GL44.GL_QUERY_BUFFER_BARRIER_BIT),
    CLIENT_MAPPED_BUFFER(GL44.GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT),
    FRAMEBUFFER_BARRIER(GL42.GL_FRAMEBUFFER_BARRIER_BIT),
    TRANSFORM_FEEDBACK(GL42.GL_TRANSFORM_FEEDBACK_BARRIER_BIT),
    ATOMIC_COUNTER(GL42.GL_ATOMIC_COUNTER_BARRIER_BIT),
    SHADER_STORAGE(GL43.GL_SHADER_STORAGE_BARRIER_BIT),
    ALL_BARRIERS(GL42.GL_ALL_BARRIER_BITS);

    public final int value;

    private GLMemoryBarrier(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static int bits(final Set<GLMemoryBarrier> flags) {
        if (flags.contains(ALL_BARRIERS)) {
            return GL42.GL_ALL_BARRIER_BITS;
        } else {
            return Tools.flagsToBitfield(flags);
        }
    }

    public static Set<GLMemoryBarrier> flags(final int bits) {        
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }

    public static void insertBarrier(final Set<GLMemoryBarrier> barriers) {
        final GLCapabilities caps = GL.getCapabilities();
        
        if (caps.OpenGL42) {
            GL42.glMemoryBarrier(bits(barriers));
        } else if (caps.GL_ARB_shader_image_load_store) {
            ARBShaderImageLoadStore.glMemoryBarrier(bits(barriers));
        } else {
            throw new UnsupportedOperationException("Memory Barriers are not supported!");
        }
    }
}
