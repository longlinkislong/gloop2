/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL21;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL40;
import org.lwjgl.opengl.GL42;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GL44;

/**
 *
 * @author zmichaels
 */
public enum GLBufferTarget implements CEnum {
    ELEMENT_ARRAY(GL15.GL_ELEMENT_ARRAY_BUFFER),
    ARRAY(GL15.GL_ARRAY_BUFFER),
    ATOMIC_COUNTER(GL42.GL_ATOMIC_COUNTER_BUFFER),
    QUERY(GL44.GL_QUERY_BUFFER),
    DISPATCH_INDIRECT(GL43.GL_DISPATCH_INDIRECT_BUFFER),
    DRAW_INDIRECT(GL40.GL_DRAW_INDIRECT_BUFFER),
    TEXTURE(GL31.GL_TEXTURE_BUFFER),
    SHADER_STORAGE(GL43.GL_SHADER_STORAGE_BUFFER),
    PIXEL_UNPACK(GL21.GL_PIXEL_UNPACK_BUFFER),
    PIXEL_PACK(GL21.GL_PIXEL_PACK_BUFFER),
    COPY_READ(GL31.GL_COPY_READ_BUFFER),
    COPY_WRITE(GL31.GL_COPY_WRITE_BUFFER),
    UNIFORM(GL31.GL_UNIFORM_BUFFER),
    TRANSFORM_FEEDBACK(GL40.GL_TRANSFORM_FEEDBACK);
    
    public final int value;
    
    private GLBufferTarget(final int value) {
        this.value = value;
    }
    
    public final Optional<GLBufferTarget> of (final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
