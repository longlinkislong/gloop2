/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL15;

/**
 *
 * @author zmichaels
 */
public enum GLImageAccess implements CEnum {
    READ_ONLY(GL15.GL_READ_ONLY),
    WRITE_ONLY(GL15.GL_WRITE_ONLY),
    READ_WRITE(GL15.GL_READ_WRITE);

    public final int value;

    private GLImageAccess(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<GLImageAccess> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
