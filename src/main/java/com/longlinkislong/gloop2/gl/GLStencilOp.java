/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;

/**
 *
 * @author zmichaels
 */
public enum GLStencilOp implements CEnum {
    KEEP(GL11.GL_KEEP),
    ZERO(GL11.GL_ZERO),
    REPLACE(GL11.GL_REPLACE),
    INCR(GL11.GL_INCR),
    DECR(GL11.GL_DECR),
    INVERT(GL11.GL_INVERT),
    INCR_WRAP(GL14.GL_INCR_WRAP),
    DECR_WRAP(GL14.GL_DECR_WRAP);

    public final int value;

    private GLStencilOp(final int value) {
        this.value = value;
    }
    
    public static Optional<GLStencilOp> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
