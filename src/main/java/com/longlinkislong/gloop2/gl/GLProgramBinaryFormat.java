/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import org.lwjgl.opengl.ARBGetProgramBinary;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public enum GLProgramBinaryFormat implements CEnum {
    PROPRIETARY {
        final ThreadLocal<Integer> value = new ThreadLocal<Integer>() {
            @Override
            protected Integer initialValue() {
                if (GL.getCapabilities().GL_ARB_get_program_binary) {
                    final int numSupportedFormats = GL11.glGetInteger(ARBGetProgramBinary.GL_NUM_PROGRAM_BINARY_FORMATS);

                    if (numSupportedFormats == 0) {
                        throw new UnsupportedOperationException("No program binary formats are supported!");
                    } else if (numSupportedFormats > 1) {
                        LOGGER.debug("More than 1 program binary format is supported!");
                    }

                    return GL11.glGetInteger(ARBGetProgramBinary.GL_PROGRAM_BINARY_FORMATS);
                } else {
                    throw new UnsupportedOperationException("GL_ARB_get_program_binary is not supported!");
                }
            }
        };

        @Override
        public int getValue() {
            return value.get();
        }

        @Override
        public boolean isSupported() {
            try {
                return this.getValue() > 0;
            } catch (UnsupportedOperationException ex) {
                return false;
            }
        }
    };

    private static final Logger LOGGER = LoggerFactory.getLogger(GLProgramBinaryFormat.class);
    
    public abstract boolean isSupported();
}
