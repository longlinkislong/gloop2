/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;

/**
 *
 * @author zmichaels
 */
public enum GLBlendFunction implements CEnum {
    ZERO(GL11.GL_ZERO),
    ONE(GL11.GL_ONE),
    ONE_MINUS_SRC_COLOR(GL11.GL_ONE_MINUS_SRC_COLOR),
    SRC_COLOR(GL11.GL_SRC_COLOR),
    ONE_MINUS_DST_COLOR(GL11.GL_ONE_MINUS_DST_COLOR),
    DST_COLOR(GL11.GL_DST_COLOR),
    ONE_MINUS_SRC_ALPHA(GL11.GL_ONE_MINUS_SRC_ALPHA),
    SRC_ALPHA(GL11.GL_SRC_ALPHA),
    ONE_MINUS_DST_ALPHA(GL11.GL_ONE_MINUS_DST_ALPHA),
    DST_ALPHA(GL11.GL_DST_ALPHA),
    ONE_MINUS_CONSTANT_COLOR(GL14.GL_ONE_MINUS_CONSTANT_COLOR),
    CONSTANT_COLOR(GL14.GL_CONSTANT_COLOR),
    ONE_MINUS_CONSTANT_ALPHA(GL14.GL_ONE_MINUS_CONSTANT_ALPHA),
    CONSTANT_ALPHA(GL14.GL_CONSTANT_ALPHA),
    SRC_ALPHA_SATURATE(GL11.GL_SRC_ALPHA_SATURATE);
    
    public final int value;
    
    private GLBlendFunction(final int value) {
        this.value = value;
    }
    
    public static Optional<GLBlendFunction> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
