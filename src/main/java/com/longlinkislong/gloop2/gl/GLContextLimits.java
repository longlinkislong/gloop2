/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.EXTTextureFilterAnisotropic;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL32;
import org.lwjgl.opengl.GL33;
import org.lwjgl.opengl.GL41;
import org.lwjgl.opengl.GL42;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GLCapabilities;

/**
 *
 * @author zmichaels
 */
public final class GLContextLimits {

    private static final ThreadLocal<GLContextLimits> INSTANCES = new ThreadLocal<GLContextLimits>() {
        @Override
        protected GLContextLimits initialValue() {
            return new GLContextLimits();
        }
    };
    
    public static GLContextLimits getLimits() {
        return INSTANCES.get();
    }

    public final int maxComputeShaderStorageBlocks;
    public final int maxCombinedShaderStorageBlocks;
    public final int maxComputeUniformBlocks;
    public final int maxComputeTextureImageUnits;
    public final int maxComputeUniformComponents;
    public final int maxComputeAtomicCounters;
    public final int maxComputeAtomicCounterBuffers;
    public final int maxCombinedComputeUniformComponents;
    public final int maxComputeWorkGroupInvocations;
    public final int maxComputeWorkGroupCountX;
    public final int maxComputeWorkGroupCountY;
    public final int maxComputeWorkGroupCountZ;
    public final int maxComputeWorkGroupSizeX;
    public final int maxComputeWorkGroupSizeY;
    public final int maxComputeWorkGroupSizeZ;
    public final int max3DTextureSize;
    public final int maxArrayTextureLayers;
    public final int maxClipDistances;
    public final int maxColorTextureSamples;
    public final int maxCombinedAtomicCounters;
    public final int maxCombinedFragmentUniformComponents;
    public final int maxCombinedGeometryUniformComponents;
    public final int maxCombinedTextureImageUnits;
    public final int maxCombinedUniformBlocks;
    public final int maxCombinedVertexUniformComponents;
    public final int maxCubeMapTextureSize;
    public final int maxDepthTextureSamples;
    public final int maxDrawBuffers;
    public final int maxDualSourceDrawBuffers;
    public final int maxElementsIndices;
    public final int maxElementsVertices;
    public final int maxFragmentAtomicCounters;
    public final int maxFragmentShaderStorageBlocks;
    public final int maxFragmentInputComponents;
    public final int maxFragmentUniformComponents;
    public final int maxFragmentUniformVectors;
    public final int maxFragmentUniformBlocks;
    public final int maxFramebufferWidth;
    public final int maxFramebufferHeight;
    public final int maxFramebufferLayers;
    public final int maxFramebufferSamples;
    public final int maxGeometryAtomicCounters;
    public final int maxGeometryShaderStorageBlocks;
    public final int maxGeometryInputComponents;
    public final int maxGeometryOutputComponents;
    public final int maxGeometryTextureImageUnits;
    public final int maxGeometryUniformBlocks;
    public final int maxGeometryUniformComponents;
    public final int maxIntegerSamples;
    public final int minMapBufferAlignment;
    public final int maxProgramTexelOffset;
    public final int minProgramTexelOffset;
    public final int maxRectangleTextureSize;
    public final int maxRenderbufferSize;
    public final int maxSampleMaskWords;
    public final int maxServerWaitTimeout;
    public final int maxShaderStorageBufferBindings;
    public final int maxTessControlAtomicCounters;
    public final int maxTessEvaluationAtomicCounters;
    public final int maxTessControlShaderStorageBlocks;
    public final int maxTessEvaluationShaderStorageBlocks;
    public final int maxTextureBufferSize;
    public final int maxTextureImageUnits;
    public final int maxTextureLODBias;
    public final int maxTextureSize;
    public final int maxUniformBufferBindings;
    public final int maxUniformBlockSize;
    public final int maxUnfiormLocations;
    public final int maxVaryingComponents;
    public final int maxVaryingVectors;
    public final int maxVaryingFloats;
    public final int maxVertexAtomicCounters;
    public final int maxVertexAttribs;
    public final int maxVertexShaderStorageBlocks;
    public final int maxVertexTextureImageUnits;
    public final int maxVertexUniformComponents;
    public final int maxVertexOutputComponents;
    public final int maxVertexUniformBlocks;
    public final int maxViewportWidth;
    public final int maxViewportHeight;
    public final int maxViewports;
    public final int shaderStorageBufferOffsetAlignment;
    public final int uniformBufferOffsetAlign;
    public final int maxVertexAttribRelativeOffset;
    public final int maxVertexAttribBindings;
    public final int maxElementIndex;
    public final float maxTextureMaxAnisotropic;

    private GLContextLimits() {
        final GLCapabilities caps = GL.getCapabilities();

        if (caps.GL_EXT_texture_filter_anisotropic) {
            maxTextureMaxAnisotropic = GL11.glGetFloat(EXTTextureFilterAnisotropic.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT);
        } else {
            maxTextureMaxAnisotropic = 1.0F;
        }
        
        if (caps.OpenGL43) {
            maxComputeShaderStorageBlocks = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS);
            maxCombinedShaderStorageBlocks = GL11.glGetInteger(GL43.GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS);
            maxComputeUniformBlocks = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_UNIFORM_BLOCKS);
            maxComputeTextureImageUnits = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS);
            maxComputeUniformComponents = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_UNIFORM_COMPONENTS);
            maxComputeAtomicCounters = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_ATOMIC_COUNTERS);
            maxComputeAtomicCounterBuffers = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS);
            maxCombinedComputeUniformComponents = GL11.glGetInteger(GL43.GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS);                                
            
            maxComputeWorkGroupInvocations = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS);
            
            maxComputeWorkGroupCountX = GL30.glGetIntegeri(GL43.GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0);
            maxComputeWorkGroupCountY = GL30.glGetIntegeri(GL43.GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1);
            maxComputeWorkGroupCountZ = GL30.glGetIntegeri(GL43.GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2);            
                        
            maxComputeWorkGroupSizeX = GL30.glGetIntegeri(GL43.GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0);                        
            maxComputeWorkGroupSizeY = GL30.glGetIntegeri(GL43.GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1);
            maxComputeWorkGroupSizeZ = GL30.glGetIntegeri(GL43.GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2);                        
            
            maxFramebufferWidth = GL11.glGetInteger(GL43.GL_MAX_FRAMEBUFFER_WIDTH);
            maxFramebufferHeight = GL11.glGetInteger(GL43.GL_MAX_FRAMEBUFFER_HEIGHT);
            maxFramebufferLayers = GL11.glGetInteger(GL43.GL_MAX_FRAMEBUFFER_LAYERS);
            maxFramebufferSamples = GL11.glGetInteger(GL43.GL_MAX_FRAMEBUFFER_SAMPLES);
            maxShaderStorageBufferBindings = GL11.glGetInteger(GL43.GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS);
            maxTessControlShaderStorageBlocks = GL11.glGetInteger(GL43.GL_MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS);
            maxTessEvaluationShaderStorageBlocks = GL11.glGetInteger(GL43.GL_MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS);
            maxUnfiormLocations = GL11.glGetInteger(GL43.GL_MAX_UNIFORM_LOCATIONS);
            maxVertexShaderStorageBlocks = GL11.glGetInteger(GL43.GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS);
            maxFragmentShaderStorageBlocks = GL11.glGetInteger(GL43.GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS);
            maxGeometryShaderStorageBlocks = GL11.glGetInteger(GL43.GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS);

        } else {
            maxComputeShaderStorageBlocks = 0;
            maxCombinedShaderStorageBlocks = 0;
            maxComputeUniformBlocks = 0;
            maxComputeTextureImageUnits = 0;
            maxComputeUniformComponents = 0;
            maxComputeAtomicCounters = 0;
            maxComputeAtomicCounterBuffers = 0;
            maxCombinedComputeUniformComponents = 0;
            maxComputeWorkGroupInvocations = 0;
            maxComputeWorkGroupCountX = 0;
            maxComputeWorkGroupCountY = 0;
            maxComputeWorkGroupCountZ = 0;
            maxComputeWorkGroupSizeX = 0;
            maxComputeWorkGroupSizeY = 0;
            maxComputeWorkGroupSizeZ = 0;
            maxFramebufferWidth = 0;
            maxFramebufferHeight = 0;
            maxFramebufferLayers = 0;
            maxFramebufferSamples = 0;
            maxShaderStorageBufferBindings = 0;
            maxTessControlShaderStorageBlocks = 0;
            maxTessEvaluationShaderStorageBlocks = 0;
            maxUnfiormLocations = 0;
            maxVertexShaderStorageBlocks = 0;
            maxFragmentShaderStorageBlocks = 0;
            maxGeometryShaderStorageBlocks = 0;
        }        

        if (caps.OpenGL42) {
            maxCombinedAtomicCounters = GL11.glGetInteger(GL42.GL_MAX_COMBINED_ATOMIC_COUNTERS);
            maxFragmentAtomicCounters = GL11.glGetInteger(GL42.GL_MAX_FRAGMENT_ATOMIC_COUNTERS);
            maxGeometryAtomicCounters = GL11.glGetInteger(GL42.GL_MAX_GEOMETRY_ATOMIC_COUNTERS);
            minMapBufferAlignment = GL11.glGetInteger(GL42.GL_MIN_MAP_BUFFER_ALIGNMENT);
            maxVertexAtomicCounters = GL11.glGetInteger(GL42.GL_MAX_VERTEX_ATOMIC_COUNTERS);
            maxTessControlAtomicCounters = GL11.glGetInteger(GL42.GL_MAX_TESS_CONTROL_ATOMIC_COUNTERS);
            maxTessEvaluationAtomicCounters = GL11.glGetInteger(GL42.GL_MAX_TESS_EVALUATION_ATOMIC_COUNTERS);
        } else {
            maxCombinedAtomicCounters = 0;
            maxFragmentAtomicCounters = 0;
            maxGeometryAtomicCounters = 0;
            minMapBufferAlignment = 0;
            maxVertexAtomicCounters = 0;
            maxTessControlAtomicCounters = 0;
            maxTessEvaluationAtomicCounters = 0;
        }

        if (caps.OpenGL41) {
            maxFragmentUniformVectors = GL11.glGetInteger(GL41.GL_MAX_FRAGMENT_UNIFORM_VECTORS);
            maxVaryingVectors = GL11.glGetInteger(GL41.GL_MAX_VARYING_VECTORS);
        } else {
            maxFragmentUniformVectors = 0;
            maxVaryingVectors = 0;
        }

        maxDualSourceDrawBuffers = GL11.glGetInteger(GL33.GL_MAX_DUAL_SOURCE_DRAW_BUFFERS);

        maxFragmentInputComponents = GL11.glGetInteger(GL32.GL_MAX_FRAGMENT_INPUT_COMPONENTS);
        maxGeometryInputComponents = GL11.glGetInteger(GL32.GL_MAX_GEOMETRY_INPUT_COMPONENTS);
        maxGeometryOutputComponents = GL11.glGetInteger(GL32.GL_MAX_GEOMETRY_OUTPUT_COMPONENTS);
        maxGeometryTextureImageUnits = GL11.glGetInteger(GL32.GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS);
        maxGeometryUniformComponents = GL11.glGetInteger(GL32.GL_MAX_GEOMETRY_UNIFORM_COMPONENTS);
        maxIntegerSamples = GL11.glGetInteger(GL32.GL_MAX_INTEGER_SAMPLES);
        maxSampleMaskWords = GL11.glGetInteger(GL32.GL_MAX_SAMPLE_MASK_WORDS);
        maxServerWaitTimeout = GL11.glGetInteger(GL32.GL_MAX_SERVER_WAIT_TIMEOUT);
        maxVertexOutputComponents = GL11.glGetInteger(GL32.GL_MAX_VERTEX_OUTPUT_COMPONENTS);
        maxColorTextureSamples = GL11.glGetInteger(GL32.GL_MAX_COLOR_TEXTURE_SAMPLES);
        maxDepthTextureSamples = GL11.glGetInteger(GL32.GL_MAX_DEPTH_TEXTURE_SAMPLES);

        maxCombinedFragmentUniformComponents = GL11.glGetInteger(GL31.GL_MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS);
        maxCombinedGeometryUniformComponents = GL11.glGetInteger(GL31.GL_MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS);
        maxCombinedUniformBlocks = GL11.glGetInteger(GL31.GL_MAX_COMBINED_UNIFORM_BLOCKS);
        maxCombinedVertexUniformComponents = GL11.glGetInteger(GL31.GL_MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS);
        maxFragmentUniformBlocks = GL11.glGetInteger(GL31.GL_MAX_FRAGMENT_UNIFORM_BLOCKS);
        maxVertexUniformBlocks = GL11.glGetInteger(GL31.GL_MAX_VERTEX_UNIFORM_BLOCKS);
        maxGeometryUniformBlocks = GL11.glGetInteger(GL31.GL_MAX_GEOMETRY_UNIFORM_BLOCKS);
        maxRectangleTextureSize = GL11.glGetInteger(GL31.GL_MAX_RECTANGLE_TEXTURE_SIZE);
        maxTextureBufferSize = GL11.glGetInteger(GL31.GL_MAX_TEXTURE_BUFFER_SIZE);
        maxUniformBufferBindings = GL11.glGetInteger(GL31.GL_MAX_UNIFORM_BUFFER_BINDINGS);
        maxUniformBlockSize = GL11.glGetInteger(GL31.GL_MAX_UNIFORM_BLOCK_SIZE);

        maxArrayTextureLayers = GL11.glGetInteger(GL30.GL_MAX_ARRAY_TEXTURE_LAYERS);
        maxClipDistances = GL11.glGetInteger(GL30.GL_MAX_CLIP_DISTANCES);
        maxProgramTexelOffset = GL11.glGetInteger(GL30.GL_MAX_PROGRAM_TEXEL_OFFSET);
        minProgramTexelOffset = GL11.glGetInteger(GL30.GL_MIN_PROGRAM_TEXEL_OFFSET);
        maxRenderbufferSize = GL11.glGetInteger(GL30.GL_MAX_RENDERBUFFER_SIZE);
        maxVaryingComponents = GL11.glGetInteger(GL30.GL_MAX_VARYING_COMPONENTS);

        maxCombinedTextureImageUnits = GL11.glGetInteger(GL20.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS);
        maxDrawBuffers = GL11.glGetInteger(GL20.GL_MAX_DRAW_BUFFERS);
        maxFragmentUniformComponents = GL11.glGetInteger(GL20.GL_MAX_FRAGMENT_UNIFORM_COMPONENTS);
        maxTextureImageUnits = GL11.glGetInteger(GL20.GL_MAX_TEXTURE_IMAGE_UNITS);
        maxVaryingFloats = GL11.glGetInteger(GL20.GL_MAX_VARYING_FLOATS);
        maxVertexAttribs = GL11.glGetInteger(GL20.GL_MAX_VERTEX_ATTRIBS);
        maxVertexTextureImageUnits = GL11.glGetInteger(GL20.GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS);
        maxVertexUniformComponents = GL11.glGetInteger(GL20.GL_MAX_VERTEX_UNIFORM_COMPONENTS);

        
        
        maxTextureLODBias = GL11.glGetInteger(GL14.GL_MAX_TEXTURE_LOD_BIAS);
        maxCubeMapTextureSize = GL11.glGetInteger(GL13.GL_MAX_CUBE_MAP_TEXTURE_SIZE);

        max3DTextureSize = GL11.glGetInteger(GL12.GL_MAX_3D_TEXTURE_SIZE);
        maxElementsIndices = GL11.glGetInteger(GL12.GL_MAX_ELEMENTS_INDICES);
        maxElementsVertices = GL11.glGetInteger(GL12.GL_MAX_ELEMENTS_VERTICES);

        maxTextureSize = GL11.glGetInteger(GL11.GL_MAX_TEXTURE_SIZE);

        final int[] dims = new int[2];

        GL11.glGetIntegerv(GL11.GL_MAX_VIEWPORT_DIMS, dims);
        maxViewportWidth = dims[0];
        maxViewportHeight = dims[1];

        
        maxViewports = GL11.glGetInteger(GL41.GL_MAX_VIEWPORTS);
        shaderStorageBufferOffsetAlignment = GL11.glGetInteger(GL43.GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT);
        uniformBufferOffsetAlign = GL11.glGetInteger(GL31.GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT);
        maxVertexAttribRelativeOffset = GL11.glGetInteger(GL43.GL_MAX_VERTEX_ATTRIB_RELATIVE_OFFSET);
        maxVertexAttribBindings = GL11.glGetInteger(GL43.GL_MAX_VERTEX_ATTRIB_BINDINGS);
        maxElementIndex = GL11.glGetInteger(GL43.GL_MAX_ELEMENT_INDEX);
                
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetLimits";
    }
}
