/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author zmichaels
 */
public enum GLCompareOp implements CEnum {
    NEVER(GL11.GL_NEVER),
    ALWAYS(GL11.GL_ALWAYS),
    LESS(GL11.GL_LESS),
    GREATER(GL11.GL_GREATER),
    LEQUAL(GL11.GL_LEQUAL),
    GEQUAL(GL11.GL_GEQUAL),
    NOTEQUAL(GL11.GL_NOTEQUAL);
    
    public final int value;
    
    private GLCompareOp(final int value) {
        this.value = value;
    }
    
    public static Optional<GLCompareOp> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
