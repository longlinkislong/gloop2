/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public class GLScissorTest {
    public final boolean scissorTestEnabled;
    public final int left, bottom, width, height;
    
    public GLScissorTest(
            final boolean scissorTestEnabled, 
            final int left, final int bottom, final int width, final int height) {
        
        this.scissorTestEnabled = scissorTestEnabled;
        this.left = left;
        this.bottom = bottom;
        this.width = width;
        this.height = height;
    }
    
    public void apply() {
        if (this.scissorTestEnabled) {
            GL11.glEnable(GL11.GL_SCISSOR_TEST);
            GL11.glScissor(left, bottom, width, height);
        
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "Scissor Test";        
        } else {
            GL11.glDisable(GLES20.GL_SCISSOR_TEST);
        }        
    }
}
