/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL44;
import org.lwjgl.opengl.GL45;

/**
 *
 * @author zmichaels
 */
final class GLBufferHelper {
    private GLBufferHelper() {}

    static int create45(final GLBufferInfo info) {        
        final int handle = GL45.glCreateBuffers();

        if (info.initialData != null) {
            assert info.size == info.initialData.remaining();

            if (info.storage.isEmpty()) {
                GL45.glNamedBufferData(handle, info.initialData, info.usage.value);
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "namedBufferData";
            } else {
                GL45.glNamedBufferStorage(handle, info.initialData, GLBufferStorage.bits(info.storage));
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "namedBufferStorage";
            }
        } else {
            if (info.storage.isEmpty()) {
                GL45.glNamedBufferData(handle, info.size, info.usage.value);                                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "namedBufferData";
            } else {
                GL45.glNamedBufferStorage(handle, info.size, GLBufferStorage.bits(info.storage));                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "namedBufferStorage";
            }
        }
        
        return handle;
    }
    
    static int create44(final GLBufferInfo info) {
        final int handle = GL15.glGenBuffers();

        GL15.glBindBuffer(info.target.value, handle);
        
        if (info.initialData != null) {
            assert info.size == info.initialData.remaining();

            if (info.storage.isEmpty()) {
                GL15.glBufferData(info.target.value, info.initialData, info.usage.value);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "BufferData";
            } else {
                GL44.glBufferStorage(info.target.value, info.initialData, GLBufferStorage.bits(info.storage));
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "BufferStorage";
            }
        } else {
            if (info.storage.isEmpty()) {
                GL15.glBufferData(info.target.value, info.size, info.usage.value);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "BufferData";
            } else {
                GL44.glBufferStorage(info.target.value, info.size, GLBufferStorage.bits(info.storage));  
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "BufferStorage";
            }
        }
        
        GL15.glBindBuffer(info.target.value, 0);
        
        return handle;
    }
    
}
