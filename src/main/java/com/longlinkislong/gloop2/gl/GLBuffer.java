/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import java.nio.ByteBuffer;
import java.util.Set;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GL45;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public class GLBuffer extends GLObject<GLBufferInfo, GLBuffer> {

    public GLBuffer() {
    }

    public GLBuffer(final GLBufferInfo info) {
        this.info = info;

        final GLCapabilities caps = GL.getCapabilities();

        if (caps.OpenGL45) {
            this.handle = GLBufferHelper.create45(info);
        } else {
            this.handle = GLBufferHelper.create44(info);
        }                
    }

    public void bindRange(final GLBufferTarget target, final int index, final long offset, final long size) {
        assert this.isValid() : "GLBuffer is not valid!";

        switch (target) {
            case TRANSFORM_FEEDBACK:
            case UNIFORM:
                break;
            default:
                throw new IllegalArgumentException("Only TRANSFORM_FEEDBACK and UNIFORM are supported!");
        }

        GL30.glBindBufferRange(target.value, index, this.handle, offset, size);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindBufferRange";
    }

    public void bindRange(final int index, final long offset, final long size) {
        bindRange(this.info.target, index, offset, size);
    }

    public void bindBase(final int index) {
        bindBase(this.info.target, index);
    }

    public void bindBase(final GLBufferTarget target, final int index) {
        assert this.isValid() : "GLBuffer is not valid!";

        switch (target) {
            case TRANSFORM_FEEDBACK:
            case UNIFORM:
                break;
            default:
                throw new IllegalArgumentException("Only TRANSFORM_FEEDBACK and UNIFORM are supported!");
        }

        GL30.glBindBufferBase(target.value, index, this.handle);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindBufferBase";
    }

    public void bind() {
        this.bind(this.info.target);
    }

    public void bind(final GLBufferTarget target) {
        assert this.isValid() : "GLBuffer is not valid!";

        GL15.glBindBuffer(target.value, handle);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindBuffer";
    }

    public void subData(final long offset, final ByteBuffer data) {
        if (!data.isDirect()) {
            final ByteBuffer directData = MemoryUtil.memAlloc(data.remaining())
                    .order(data.order())
                    .put(data);
            
            directData.flip();
            
            try {            
                subData(offset, directData);
            } finally {
                MemoryUtil.memFree(directData);
            }
            
            return;
        }
        
        assert this.isValid() : "GLBuffer is not valid!";
        
        if (GL.getCapabilities().OpenGL45) {
            GL45.glNamedBufferSubData(handle, offset, data);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "NamedBufferSubData";
        } else {
            GL15.glBindBuffer(this.info.target.value, this.handle);
            GL15.glBufferSubData(this.info.target.value, offset, data);
            GL15.glBindBuffer(this.info.target.value, 0);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "BufferSubData";
        }
    }

    public static final class MappedBuffer implements AutoCloseable {

        GLBuffer host;
        long ptr;
        public final long size;

        private MappedBuffer(final GLBuffer buffer, final long offset, final long size, final Set<GLBufferAccess> access) {
            this.host = buffer;
            this.size = size;
            
            if (GL.getCapabilities().OpenGL45) {
                this.ptr = GL45.nglMapNamedBufferRange(buffer.handle, offset, size, GLBufferAccess.bits(access));                               
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "MapNamedBufferRange";
            } else {
                GL15.glBindBuffer(buffer.info.target.value, buffer.handle);
                this.ptr = GL30.nglMapBufferRange(buffer.info.target.value, offset, size, GLBufferAccess.bits(access));                                               
                GL15.glBindBuffer(buffer.info.target.value, 0);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "MapBufferRange";
            }
        }
        
        public ByteBuffer getPointer() {
            return MemoryUtil.memByteBuffer(this.ptr, (int) this.size);
        }

        public long ptr() {
            return this.ptr;
        }

        public void flushRange(long offset, long size) {
            if (!this.isValid()) {
                throw new IllegalStateException("MappedBuffer data is no longer valid!");
            }

            if (GL.getCapabilities().OpenGL45) {
                GL45.glFlushMappedNamedBufferRange(host.handle, offset, size);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "FlushMappedNamedBufferRange";
            } else {
                GL15.glBindBuffer(host.info.target.value, host.handle);
                GL30.glFlushMappedBufferRange(host.info.target.value, offset, size);
                GL15.glBindBuffer(host.info.target.value, 0);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "FlushMappedBufferRange";
            }
        }        

        public boolean isValid() {
            return this.host != null && this.host.isValid();
        }

        @Override
        public void close() {
            if (this.isValid()) {
                if (GL.getCapabilities().OpenGL45) {
                    GL45.glUnmapNamedBuffer(host.handle);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "UnmapNamedBuffer";
                } else {
                    GL15.glBindBuffer(host.info.target.value, host.handle);
                    GL15.glUnmapBuffer(host.info.target.value);
                    GL15.glBindBuffer(host.info.target.value, 0);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "UnmapBuffer";
                }

                this.host = null;
                this.ptr = 0L;
            }
        }
    }

    public MappedBuffer map(final Set<GLBufferAccess> access) {
        return map(0, this.info.size, access);
    }
    
    public MappedBuffer map(final long offset, final long size, final Set<GLBufferAccess> access) {
        return new MappedBuffer(this, offset, size, access);
    }

    @Override
    public void close() {
        if (this.isValid()) {
            GL15.glDeleteBuffers(this.handle);
            this.handle = 0;
            this.info = null;
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "DeleteBuffers";
        }
    }

    public static void copySubData(final GLBuffer src, final GLBuffer dst, final long srcOff, final long dstOff, final long size) {
        if (!src.isValid()) {
            throw new IllegalStateException("Source buffer is not valid!");
        } else if (!dst.isValid()) {
            throw new IllegalStateException("Destination buffer is not valid!");
        }

        if (GL.getCapabilities().OpenGL45) {
            GL45.glCopyNamedBufferSubData(src.handle, dst.handle, srcOff, dstOff, size);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "CopyNamedBufferSubData";
        } else {
            GL15.glBindBuffer(GL31.GL_COPY_READ_BUFFER, src.handle);
            GL15.glBindBuffer(GL31.GL_COPY_WRITE_BUFFER, dst.handle);

            GL31.glCopyBufferSubData(GL31.GL_COPY_READ_BUFFER, GL31.GL_COPY_WRITE_BUFFER, srcOff, dstOff, size);

            GL15.glBindBuffer(GL31.GL_COPY_WRITE_BUFFER, 0);
            GL15.glBindBuffer(GL31.GL_COPY_READ_BUFFER, 0);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "CopyBufferSubData";
        }
    }

    public void invalidate() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESBuffer is not valid!");
        }

        if (GL.getCapabilities().OpenGL43) {
            GL43.glInvalidateBufferData(this.handle);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "InvalidateBufferData";
        } else {
            GL15.glBindBuffer(info.target.value, handle);
            GL15.glBufferData(info.target.value, info.size, info.usage.value);
            GL15.glBindBuffer(info.target.value, 0);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "BufferData";
        }        
    }

    public static final GLBuffer DEFAULT_BUFFER = new GLBuffer() {
        @Override
        public void bind(GLBufferTarget tgt) {
            GL15.glBindBuffer(tgt.value, 0);                        
        }

        @Override
        public void bindBase(final GLBufferTarget tgt, final int index) {
            GL30.glBindBufferBase(tgt.value, index, 0);
        }

        @Override
        public void bindRange(GLBufferTarget tgt, int index, long offset, long size) {
            GL30.glBindBufferRange(tgt.value, index, 0, offset, size);
        }
    };
}
