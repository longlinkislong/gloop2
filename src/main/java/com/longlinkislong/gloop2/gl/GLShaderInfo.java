/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.Tools;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class GLShaderInfo {
    private static final Logger LOGGER = LoggerFactory.getLogger(GLShaderInfo.class);
            
    public static final class SpecializationInfo {
        public final int constantIndex;
        public final int constantValue;
        
        public SpecializationInfo(final int index, final int value) {
            this.constantIndex = index;
            this.constantValue = value;
        }
        
        public SpecializationInfo withConstantIndex(final int index) {
            return new SpecializationInfo(index, constantValue);
        }
        
        public SpecializationInfo withConstantValue(final int value) {
            return new SpecializationInfo(constantIndex, value);
        }
    }
    
    public final GLShaderType type;
    public final GLShaderBinaryFormat format;
    public final ByteBuffer data;
    public final String entryPoint;
    public final List<SpecializationInfo> specializationInfo;        
    
    public GLShaderInfo(final GLShaderType type, final GLShaderBinaryFormat format, final ByteBuffer data, final String entryPoint, final List<SpecializationInfo> specializationInfo) {
        this.type = type;
        this.format = format;        
        this.entryPoint = entryPoint;
        this.specializationInfo = Tools.copyList(specializationInfo);
        
        if (data == null) {
            this.data = null;
        } else if (data.isReadOnly()) {
            this.data = data.asReadOnlyBuffer();
        } else {
            this.data = data.asReadOnlyBuffer();
        }
                
    }
    
    public GLShaderInfo() {
        this(GLShaderType.VERTEX, GLShaderBinaryFormat.GLSL, null, "main", null);
    }
    
    public GLShaderInfo withType(final GLShaderType type) {
        return new GLShaderInfo(type, format, data, entryPoint, specializationInfo);
    }
    
    public GLShaderInfo withSrc(final String src) {
        return new GLShaderInfo(type, format, StandardCharsets.UTF_8.encode(src), entryPoint, specializationInfo);
    }
    
    public GLShaderInfo withSpecializationInfo(final SpecializationInfo... specializationInfo) {
        return withSpecializationInfo(Arrays.asList(specializationInfo));
    }
    
    public GLShaderInfo withSpecializationInfo(final List<SpecializationInfo> specializationInfo) {
        return new GLShaderInfo(type, format, data, entryPoint, specializationInfo);
    }
    
    public GLShaderInfo withEntryPoint(final String entryPoint) {
        return new GLShaderInfo(type, format, data, entryPoint, specializationInfo);
    }
    
    public GLShaderInfo withData(final ByteBuffer data) {
        return new GLShaderInfo(type, format, data, entryPoint, specializationInfo);
    }
    
    public GLShaderInfo withFormat(final GLShaderBinaryFormat format) {
        return new GLShaderInfo(type, format, data, entryPoint, specializationInfo);
    }
    
    public GLShader create() {
        return new GLShader(this);
    }
}
