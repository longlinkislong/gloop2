/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public class GLDepthStencilStateInfo {

    public final boolean depthTestEnable;
    public final GLCompareOp depthCompareOp;
    public final boolean depthWriteEnable;
    public final boolean stencilTestEnable;
    public final GLStencilOpState front;
    public final GLStencilOpState back;

    public GLDepthStencilStateInfo(
            final boolean depthTestEnable, final boolean depthWriteEnable,
            final GLCompareOp depthCompareOp,
            final boolean stencilTestEnable, final GLStencilOpState front, final GLStencilOpState back) {

        this.depthTestEnable = depthTestEnable;
        this.depthWriteEnable = depthWriteEnable;
        this.depthCompareOp = depthCompareOp;
        this.stencilTestEnable = stencilTestEnable;
        this.front = front;
        this.back = back;
    }

    public GLDepthStencilStateInfo() {
        this(false, true, GLCompareOp.LESS, false, new GLStencilOpState(), new GLStencilOpState());
    }

    public GLDepthStencilStateInfo withDepthTestEnable(final boolean depthTestEnable) {
        return new GLDepthStencilStateInfo(depthTestEnable, depthWriteEnable, depthCompareOp, stencilTestEnable, front, back);
    }

    public GLDepthStencilStateInfo withDepthCompareOp(final GLCompareOp depthCompareOp) {
        return new GLDepthStencilStateInfo(depthTestEnable, depthWriteEnable, depthCompareOp, stencilTestEnable, front, back);
    }

    public GLDepthStencilStateInfo withStencilTestEnable(final boolean stencilTestEnable) {
        return new GLDepthStencilStateInfo(depthTestEnable, depthWriteEnable, depthCompareOp, stencilTestEnable, front, back);
    }

    public GLDepthStencilStateInfo withFront(final GLStencilOpState front) {
        return new GLDepthStencilStateInfo(depthTestEnable, depthWriteEnable, depthCompareOp, stencilTestEnable, front, back);
    }

    public GLDepthStencilStateInfo withBack(final GLStencilOpState back) {
        return new GLDepthStencilStateInfo(depthTestEnable, depthWriteEnable, depthCompareOp, stencilTestEnable, front, back);
    }

    public void apply() {
        if (this.depthTestEnable) {            
            GL11.glEnable(GL11.GL_DEPTH_TEST);
            GL11.glDepthFunc(this.depthCompareOp.value);
        } else {
            GL11.glDisable(GL11.GL_DEPTH_TEST);
        }

        GL11.glDepthMask(this.depthWriteEnable);
        
        if (this.stencilTestEnable) {
            GL11.glEnable(GL11.GL_STENCIL_TEST);
            
            GL20.glStencilOpSeparate(GLES20.GL_FRONT, this.front.failOp.value, this.front.depthFailOp.value, this.front.passOp.value);
            GL20.glStencilOpSeparate(GLES20.GL_BACK, this.back.failOp.value, this.back.depthFailOp.value, this.back.passOp.value);
            
            GL20.glStencilMaskSeparate(GLES20.GL_FRONT, this.front.writeMask);
            GL20.glStencilMaskSeparate(GLES20.GL_BACK, this.back.writeMask);
            
            GL20.glStencilFuncSeparate(GLES20.GL_FRONT, this.front.compareOp.value, this.front.reference, this.front.compareMask);
            GL20.glStencilFuncSeparate(GLES20.GL_BACK, this.back.compareOp.value, this.back.reference, this.back.compareMask);                        
        } else {
            GL11.glDisable(GLES20.GL_STENCIL_TEST);
        }
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DepthStencilStateInfo";
    }
}
