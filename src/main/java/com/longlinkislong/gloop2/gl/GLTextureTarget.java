/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL30;

/**
 *
 * @author zmichaels
 */
public enum GLTextureTarget implements CEnum {
    TEXTURE_1D(GL11.GL_TEXTURE_1D),
    TEXTURE_1D_ARRAY(GL30.GL_TEXTURE_1D_ARRAY),
    TEXTURE_2D(GL11.GL_TEXTURE_2D),
    TEXTURE_2D_ARRAY(GL30.GL_TEXTURE_2D_ARRAY),
    TEXTURE_3D(GL12.GL_TEXTURE_3D);
    
    public final int value;
    
    private GLTextureTarget(final int value) {
        this.value = value;
    }
    
    public static Optional<GLTextureTarget> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
