/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;
import org.lwjgl.opengl.ARBGLSPIRV;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL41;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public class GLShader extends GLObject<GLShaderInfo, GLShader> {

    public GLShader() {
    }

    private void initGLSL(final GLShaderInfo info) {
        GL20.glShaderSource(this.handle, StandardCharsets.UTF_8.decode(info.data));
        GL20.glCompileShader(this.handle);

        assert GL11.glGetError() == GL11.GL_NO_ERROR : "CompileShader";

        if (GL20.glGetShaderi(this.handle, GL20.GL_COMPILE_STATUS) != GL11.GL_TRUE) {
            final String infoLog = GL20.glGetShaderInfoLog(this.handle);

            GL20.glDeleteShader(this.handle);

            this.handle = 0;

            throw new GLCompileException(infoLog);
        }
    }

    private void initSPIRV(final GLShaderInfo info) {
        assert info.data.isDirect() : "Shader Binary must be supplied as a direct ByteBuffer!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pShader = mem.ints(this.handle);
            final IntBuffer pConstIndices;
            final IntBuffer pConstValues;
            final ByteBuffer pEntryPoint = mem.UTF8(info.entryPoint);

            if (info.specializationInfo.isEmpty()) {
                pConstIndices = mem.mallocInt(0);
                pConstValues = mem.mallocInt(0);
            } else {
                pConstIndices = MemoryUtil.memCallocInt(info.specializationInfo.size());
                pConstValues = MemoryUtil.memCallocInt(info.specializationInfo.size());

                for (GLShaderInfo.SpecializationInfo specInfo : info.specializationInfo) {
                    pConstIndices.put(specInfo.constantIndex);
                    pConstValues.put(specInfo.constantValue);
                }

                pConstIndices.flip();
                pConstValues.flip();
            }
            
            GL41.glShaderBinary(pShader, ARBGLSPIRV.GL_SHADER_BINARY_FORMAT_SPIR_V_ARB, info.data);

            assert GL11.glGetError() == GL11.GL_NO_ERROR : "ShaderBinary";

            ARBGLSPIRV.glSpecializeShaderARB(handle, pEntryPoint, pConstIndices, pConstValues);

            assert GL11.glGetError() == GL11.GL_NO_ERROR : "SpecializeShader";

            final int result = GL20.glGetShaderi(this.handle, GL20.GL_COMPILE_STATUS);
                                   
            if (result != GL11.GL_TRUE) {
                final String infoLog = GL20.glGetShaderInfoLog(this.handle);
                
                GL20.glDeleteShader(this.handle);

                this.handle = 0;

                throw new GLCompileException(infoLog);
            }
        }
    }

    public GLShader(final GLShaderInfo info) {
        this.info = info;
        this.handle = GL20.glCreateShader(info.type.value);

        switch (info.format) {
            case GLSL:
                this.initGLSL(info);
                break;
            case SPIRV:
                this.initSPIRV(info);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported Shader Format: " + info.format);
        }
    }

    @Override
    public void close() {
        if (this.isValid()) {
            GL20.glDeleteShader(this.handle);

            assert GL11.glGetError() == GL11.GL_NO_ERROR : "DeleteShader";

            this.handle = 0;
            this.info = null;
        }
    }

    public static GLShader newVertexShader(final String src) {
        return new GLShaderInfo()
                .withFormat(GLShaderBinaryFormat.GLSL)
                .withSrc(src)
                .withType(GLShaderType.VERTEX)
                .create();
    }

    public static GLShader newFragmentShader(final String src) {
        return new GLShaderInfo()
                .withFormat(GLShaderBinaryFormat.GLSL)
                .withSrc(src)
                .withType(GLShaderType.FRAGMENT)
                .create();
    }

    public static GLShader newGeometryShader(final String src) {
        return new GLShaderInfo()
                .withFormat(GLShaderBinaryFormat.GLSL)
                .withSrc(src)
                .withType(GLShaderType.GEOMETRY)
                .create();
    }

    public static GLShader newTessControlShader(final String src) {
        return new GLShaderInfo()
                .withFormat(GLShaderBinaryFormat.GLSL)
                .withSrc(src)
                .withType(GLShaderType.TESS_CONTROL)
                .create();
    }

    public static GLShader newTessEvaluationShader(final String src) {
        return new GLShaderInfo()
                .withFormat(GLShaderBinaryFormat.GLSL)
                .withSrc(src)
                .withType(GLShaderType.TESS_EVALUATION)
                .create();
    }

    public static GLShader newComputeShader(final String src) {
        return new GLShaderInfo()
                .withFormat(GLShaderBinaryFormat.GLSL)
                .withSrc(src)
                .withType(GLShaderType.COMPUTE)
                .create();
    }
}
