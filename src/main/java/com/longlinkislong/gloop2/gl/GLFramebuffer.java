/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

/**
 *
 * @author zmichaels
 */
public class GLFramebuffer extends GLObject<GLFramebufferInfo, GLFramebuffer> {
    public GLFramebuffer() {
        
    }
    
    public GLFramebuffer(final GLFramebufferInfo info) {
        this.info = info;
        
        if (GL.getCapabilities().OpenGL45) {
            this.handle = GLFramebufferHelper.create45(info);
        } else {
            this.handle = GLFramebufferHelper.create30(info);
        }
    }
    
    public void bind() {
        assert this.isValid() : "GLFramebuffer is not valid!";
        
        GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, this.handle);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindFramebuffer";
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GL30.glDeleteFramebuffers(this.handle);
            this.handle = 0;
            this.info = null;
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "DeleteFramebuffers";
        }
    }
    
    public static final GLFramebuffer DEFAULT_FRAMEBUFFER = new GLFramebuffer() {
        @Override
        public void bind() {
            GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);
        }
    };
}
