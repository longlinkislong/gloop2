/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author zmichaels
 */
public class GLRasterizationStateInfo {

    public static enum FrontFace implements CEnum {
        CCW(GL11.GL_CCW),
        CW(GL11.GL_CW);
        
        public final int value;
        
        private FrontFace(final int value) {
            this.value = value;
        }
        
        public static Optional<FrontFace> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }

        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public static enum PolygonMode implements CEnum {
        FILL(GL11.GL_FILL),
        LINE(GL11.GL_LINE),
        POINT(GL11.GL_POINT);
        
        public final int value;
        
        private PolygonMode(final int value) {
            this.value = value;
        }
        
        public static Optional<PolygonMode> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }

        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public static enum CullMode implements CEnum {
        NONE(0),
        FRONT(GL11.GL_FRONT),
        BACK(GL11.GL_BACK),
        FRONT_AND_BACK(GL11.GL_FRONT_AND_BACK);

        public final int value;

        private CullMode(final int value) {
            this.value = value;
        }

        public static Optional<CullMode> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }

        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    
    public final FrontFace frontFace;
    public final float lineWidth;
    public final CullMode cullMode;
    public final boolean polygonOffsetEnabled;
    public final float polygonOffsetFactor;
    public final float polygonOffsetUnits;
    public final PolygonMode polygonMode;

    public GLRasterizationStateInfo(
            final CullMode cullMode,
            final float lineWidth,
            final boolean polygonOffsetEnabled, final float polygonOffsetFactor, final float polygonOffsetUnits,
            final PolygonMode polygonMode,
            final FrontFace frontFace) {

        this.frontFace = frontFace;
        this.cullMode = cullMode;
        this.lineWidth = lineWidth;
        this.polygonOffsetEnabled = polygonOffsetEnabled;
        this.polygonOffsetFactor = polygonOffsetFactor;
        this.polygonOffsetUnits = polygonOffsetUnits;
        this.polygonMode = polygonMode;
    }
    
    public GLRasterizationStateInfo() {
        this(CullMode.NONE, 1.0F, false, 0.0F, 0.0F, PolygonMode.FILL, FrontFace.CCW);
    }
    
    public GLRasterizationStateInfo withCullMode(final CullMode cullMode) {
        return new GLRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, polygonMode, frontFace);
    }

    public GLRasterizationStateInfo withLineWidth(final float lineWidth) {
        return new GLRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, polygonMode, frontFace);
    }
    
    public GLRasterizationStateInfo withPolygonOffsetEnabled(final boolean polygonOffsetEnabled) {
        return new GLRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, polygonMode, frontFace);
    }
    
    public GLRasterizationStateInfo withPolygonOffsetFactor(final float polygonOffsetFactor) {
        return new GLRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, polygonMode, frontFace);
    }
    
    public GLRasterizationStateInfo withPolygonOffsetUnits(final float polygonOffsetUnits) {
        return new GLRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, polygonMode, frontFace);
    }
    
    public GLRasterizationStateInfo withPolygonMode(final PolygonMode polygonMode) {
        return new GLRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, polygonMode, frontFace);
    }
    
    public GLRasterizationStateInfo withCullFace(final FrontFace frontFace) {
        return new GLRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, polygonMode, frontFace);    
    }
    
    public void apply() {
        GL11.glLineWidth(this.lineWidth);        
        
        GL11.glPolygonOffset(this.polygonOffsetFactor, this.polygonOffsetUnits);
        GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, this.polygonMode.value);
        
        if (this.cullMode != CullMode.NONE) {
            GL11.glEnable(GL11.GL_CULL_FACE);
            GL11.glCullFace(this.cullMode.value);
            GL11.glFrontFace(this.frontFace.value);
        } else {
            GL11.glDisable(GL11.GL_CULL_FACE);
        }
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "Rasterization State Info";
    }
}
