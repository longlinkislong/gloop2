/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

/**
 *
 * @author zmichaels
 */
public final class GLAttachment {
    public final int layer;
    public final int level;
    public final GLTexture texture;
    public final GLRenderbuffer renderbuffer;
    
    public GLAttachment(final GLTexture texture, final GLRenderbuffer renderbuffer, final int level, final int layer) {
        this.texture = texture;
        this.level = level;
        this.layer = layer;
        this.renderbuffer = renderbuffer;
    }
    
    public GLAttachment() {
        this(null, null, 0, 0);
    }
    
    public GLAttachment withTexture(final GLTexture texture) {
        return new GLAttachment(texture, renderbuffer, level, layer);
    }
    
    public GLAttachment withLevel(final int level) {
        return new GLAttachment(texture, renderbuffer, level, layer);
    }
    
    public GLAttachment withLayer(final int layer) {
        return new GLAttachment(texture, renderbuffer, level, layer);
    }
    
    public GLAttachment withRenderbuffer(final GLRenderbuffer renderbuffer) {
        return new GLAttachment(texture, renderbuffer, level, layer);
    }
}
