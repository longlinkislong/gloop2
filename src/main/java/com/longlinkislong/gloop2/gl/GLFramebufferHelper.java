/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL32;
import org.lwjgl.opengl.GL45;

/**
 *
 * @author zmichaels
 */
final class GLFramebufferHelper {
    private GLFramebufferHelper() {}
    
    static int create30(final GLFramebufferInfo info) {
        final int handle = GL30.glGenFramebuffers();
        
        GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, handle);
        
        int colorAttachId = GL30.GL_COLOR_ATTACHMENT0;
        for (GLAttachment attachment : info.attachments) {
            assert attachment.renderbuffer != null && attachment.texture != null : "Attachment must be either a renderbuffer or texture, not both!";
            assert !(attachment.renderbuffer == null && attachment.texture == null) : "Attachment must have either a renderbuffer or texture!";
            
            if (attachment.renderbuffer != null) {
                final GLRenderbuffer rba = attachment.renderbuffer;
                
                final int attachId;
                
                if (rba.info.format.isColor()) {
                    attachId = colorAttachId++;
                } else if (rba.info.format.isDepthStencil()) {
                    attachId = GL30.GL_DEPTH_STENCIL_ATTACHMENT;
                } else if (rba.info.format.isDepth()) {
                    attachId = GL30.GL_DEPTH_ATTACHMENT;
                } else if (rba.info.format.isStencil()) {
                    attachId = GL30.GL_STENCIL_ATTACHMENT;
                } else {
                    throw new UnsupportedOperationException("Unsupported attachment: " + rba.info.format);
                }
                
                GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, attachId, GL30.GL_RENDERBUFFER, rba.handle);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "FramebufferRenderbuffer";
            } else if (attachment.texture != null) {
                final GLTexture ta = attachment.texture;
                final GLTextureTarget tgt = ta.info.target();                
                final int attachId;
                
                if (ta.info.format.isColor()) {
                    attachId = colorAttachId++;
                } else if (ta.info.format.isDepthStencil()) {
                    attachId = GL30.GL_DEPTH_STENCIL_ATTACHMENT;
                } else if (ta.info.format.isDepth()) {
                    attachId = GL30.GL_DEPTH_ATTACHMENT;
                } else if (ta.info.format.isStencil()) {
                    attachId = GL30.GL_STENCIL_ATTACHMENT;
                } else {
                    throw new UnsupportedOperationException("Unsupported attachment: " + ta.info.format);
                }
                
                switch (tgt) {
                    case TEXTURE_1D:
                    case TEXTURE_2D:
                        GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, attachId, tgt.value, attachment.level);
                        
                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "FramebufferTexture";
                        break;
                    case TEXTURE_1D_ARRAY:
                    case TEXTURE_2D_ARRAY:
                        GL30.glFramebufferTextureLayer(GL30.GL_FRAMEBUFFER, attachId, tgt.value, attachment.level, attachment.layer);
                        
                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "FramebufferTextureLayer";
                        break;
                    default:
                        throw new UnsupportedOperationException("Unsupported Texture Target: " + tgt);
                }
            }                        
        }
        
        if (GL30.glCheckFramebufferStatus(GL30.GL_FRAMEBUFFER) != GL30.GL_FRAMEBUFFER_COMPLETE) {
            GL30.glDeleteFramebuffers(handle);
            
            throw new GLIncompleteFramebufferException();
        }
        
        return handle;
    }
    
    static int create45(final GLFramebufferInfo info) {
        final int handle = GL45.glCreateFramebuffers();                
        
        int colorAttachId = GL30.GL_COLOR_ATTACHMENT0;
        for (GLAttachment attachment : info.attachments) {
            assert attachment.renderbuffer != null && attachment.texture != null : "Attachment must be either a renderbuffer or texture, not both!";
            assert !(attachment.renderbuffer == null && attachment.texture == null) : "Attachment must have either a renderbuffer or texture!";
            
            if (attachment.renderbuffer != null) {
                final GLRenderbuffer rba = attachment.renderbuffer;                
                final int attachId;
                
                if (rba.info.format.isColor()) {
                    attachId = colorAttachId++;
                } else if (rba.info.format.isDepthStencil()) {
                    attachId = GL30.GL_DEPTH_STENCIL_ATTACHMENT;
                } else if (rba.info.format.isDepth()) {
                    attachId = GL30.GL_DEPTH_ATTACHMENT;
                } else if (rba.info.format.isStencil()) {
                    attachId = GL30.GL_STENCIL_ATTACHMENT;
                } else {
                    throw new UnsupportedOperationException("Unsupported attachment: " + rba.info.format);
                }
                
                GL45.glNamedFramebufferRenderbuffer(handle, attachId, GL30.GL_RENDERBUFFER, rba.handle);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "NamedFramebufferRenderbuffer";
            } else if (attachment.texture != null) {
                final GLTexture ta = attachment.texture;
                final GLTextureTarget tgt = ta.info.target();                                                
                final int attachId;
                
                if (ta.info.format.isColor()) {
                    attachId = colorAttachId++;
                } else if (ta.info.format.isDepthStencil()) {
                    attachId = GL30.GL_DEPTH_STENCIL_ATTACHMENT;
                } else if (ta.info.format.isDepth()) {
                    attachId = GL30.GL_DEPTH_ATTACHMENT;
                } else if (ta.info.format.isStencil()) {
                    attachId = GL30.GL_STENCIL_ATTACHMENT;
                } else {
                    throw new UnsupportedOperationException("Unsupported attachment: " + ta.info.format);
                }
                
                switch (tgt) {
                    case TEXTURE_1D:
                    case TEXTURE_2D:
                        GL45.glNamedFramebufferTexture(handle, attachId, ta.handle, attachment.level);
                        
                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "NamedFramebufferTexture";
                        break;
                    case TEXTURE_1D_ARRAY:
                    case TEXTURE_2D_ARRAY:                    
                        GL45.glNamedFramebufferTextureLayer(handle, attachId, ta.handle, attachment.level, attachment.layer);
                        
                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "NamedFramebufferTextureLayer";
                        break;
                    default:
                        throw new UnsupportedOperationException("Texture attachments must either be TEXTURE_2D or TEXTURE_2D_ARRAY!");
                }
            }                        
        }
        
        if (GL45.glCheckNamedFramebufferStatus(handle, GL30.GL_FRAMEBUFFER) != GL30.GL_FRAMEBUFFER_COMPLETE) {
            GL30.glDeleteFramebuffers(handle);            
            
            throw new GLIncompleteFramebufferException();
        }
        
        return handle;
    }
}
