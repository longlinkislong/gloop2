/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Set;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL44;

/**
 *
 * @author zmichaels
 */
public enum GLBufferAccess implements CBitfield {
    READ(GL30.GL_MAP_READ_BIT),
    WRITE(GL30.GL_MAP_WRITE_BIT),
    PERSISTENT(GL44.GL_MAP_PERSISTENT_BIT),
    COHERENT(GL44.GL_MAP_COHERENT_BIT),
    INVALIDATE_RANGE(GL30.GL_MAP_INVALIDATE_RANGE_BIT),
    INVALIDATE_BUFFER(GL30.GL_MAP_INVALIDATE_BUFFER_BIT),
    FLUSH_EXPLICIT(GL30.GL_MAP_FLUSH_EXPLICIT_BIT),
    UNSYNCHRONIZED(GL30.GL_MAP_UNSYNCHRONIZED_BIT);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private GLBufferAccess(final int value) {
        this.value = value;
    }
    
    public static Set<GLBufferAccess> flags(final int bits) {
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<GLBufferAccess> flags) {
        return Tools.flagsToBitfield(flags);
    }
}
