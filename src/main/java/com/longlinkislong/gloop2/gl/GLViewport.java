/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL41;

/**
 *
 * @author zmichaels
 */
public class GLViewport {
    public final int x;
    public final int y;
    public final int width;
    public final int height;
    public final float minDepth;
    public final float maxDepth;
    
    public GLViewport(final int x, final int y, final int width, final int height, final float minDepth, final float maxDepth) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.minDepth = minDepth;
        this.maxDepth = maxDepth;
    }
    
    public GLViewport() {
        this(0, 0, 0, 0, 0, 0);
    }
    
    public GLViewport withOffset(final int x, final int y) {
        return new GLViewport(x, y, width, height, minDepth, maxDepth);
    }
    
    public GLViewport withExtent(final int width, final int height) {
        return new GLViewport(x, y, width, height, minDepth, maxDepth);
    }
    
    public GLViewport withDepthRange(final float minDepth, final float maxDepth) {
        return new GLViewport(x, y, width, height, minDepth, maxDepth);
    }
    
    public void apply() {        
        if (minDepth != maxDepth) {
            GL41.glDepthRangef(this.minDepth, this.maxDepth);
        }
        
        GL11.glViewport(x, y, width, height);
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "Viewport";
    }
}
