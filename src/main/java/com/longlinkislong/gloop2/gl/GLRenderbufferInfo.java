/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

/**
 *
 * @author zmichaels
 */
public class GLRenderbufferInfo {
    public final GLInternalFormat format;
    public final int width;
    public final int height;
    public final int samples;
    
    public GLRenderbufferInfo(
            final GLInternalFormat internalFormat,
            final int width, final int height,
            final int samples) {
        
        this.format = internalFormat;
        this.width = width;
        this.height = height;
        this.samples = samples;
    }
    
    public GLRenderbufferInfo() {
        this(GLInternalFormat.RGBA8, 0, 0, 0);
    }
    
    public GLRenderbufferInfo withFormat(final GLInternalFormat format) {
        return new GLRenderbufferInfo(format, width, height, samples);
    }
    
    public GLRenderbufferInfo withExtent(final int width, final int height) {
        return new GLRenderbufferInfo(format, width, height, samples);
    }
    
    public GLRenderbufferInfo withSamples(final int samples) {
        return new GLRenderbufferInfo(format, width, height, samples);
    }
    
    public GLRenderbuffer create() {
        return new GLRenderbuffer(this);
    }
}
