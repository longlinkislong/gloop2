/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.gl.GLProgramInfo.AttributeStateInfo;
import com.longlinkislong.gloop2.gl.GLProgramInfo.VaryingStateInfo;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.opengl.ARBGetProgramBinary;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL40;
import org.lwjgl.opengl.GL41;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.system.MemoryStack;

/**
 *
 * @author zmichaels
 */
public class GLProgram extends GLObject<GLProgramInfo, GLProgram> {
    public GLProgram() {}
    
    public GLProgram(final GLProgramInfo info) {
        this.info = info;
        
        this.handle = GL20.glCreateProgram();
        
        for (GLShader shader : info.shaders) {
            assert shader.isValid() : "GLShader is not valid!";
            
            GL20.glAttachShader(handle, shader.handle);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "AttachShader";
        }        
        
        for (AttributeStateInfo attribInfo : info.attributes) {
            GL20.glBindAttribLocation(handle, attribInfo.location, attribInfo.name);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindAttribLocation";
        }
        
        for (VaryingStateInfo varyingInfo : info.varyings) {
            GL30.glTransformFeedbackVaryings(handle, varyingInfo.name, varyingInfo.mode.value);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "TransformFeedbackVaryings";
        }        
        
        if (info.binaryFormat != null && info.binary != null) {
            GL41.glProgramBinary(handle, info.binaryFormat.getValue(), info.binary);
        } else {
            GL20.glLinkProgram(handle);
        }
                
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "LinkProgram";
        
        if (GL20.glGetProgrami(handle, GL20.GL_LINK_STATUS) != GL11.GL_TRUE) {                        
            final String infoLog = GL20.glGetProgramInfoLog(this.handle);
            
            GL20.glDeleteProgram(handle);
            
            this.handle = 0;
            
            throw new GLLinkException(infoLog);
        }
    }
    
    public int getSubroutineUniformLocation(final GLShaderType shaderType, final String name) {
        assert this.isValid() : "GLProgram is not valid!";
        
        final int out = GL40.glGetSubroutineUniformLocation(this.handle, shaderType.value, name);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetSubroutineUniformLocation";
        
        return out;
    }
    
    public int getUniformLocation(final String name) {
        assert this.isValid() : "GLProgram is not valid!";
        
        final int out = GL20.glGetUniformLocation(this.handle, name);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetUniformLocation";
        
        return out;
    }
    
    public int getUniformBlockIndex(final String name) {
        assert this.isValid() : "GLProgram is not valid!";
        
        final int out = GL31.glGetUniformBlockIndex(this.handle, name);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetUniformBlockIndex";
        
        return out;
    }
    
    public int getFragDataLocation(final String name) {
        assert this.isValid() : "GLProgram is not valid!";
        
        final int out = GL30.glGetFragDataLocation(this.handle, name);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetFragDataLocation";
        
        return out;
    }
    
    public void validate() throws GLValidationException {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL20.glValidateProgram(this.handle);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ValidateProgram";
        
        if (GL20.glGetProgrami(this.handle, GL20.GL_VALIDATE_STATUS) != GL11.GL_TRUE) {
            throw new GLValidationException(GLES20.glGetProgramInfoLog(this.handle));
        }
    }
    
    public void use() {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL20.glUseProgram(this.handle);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "UseProgram";
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GL20.glDeleteProgram(this.handle);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "DeleteProgram";
            
            this.handle = 0;
            this.info = null;
        }
    }
    
    public static final GLProgram DEFAULT_PROGRAM = new GLProgram() {      
        @Override
        public void use() {
            GL20.glUseProgram(0);
        }
    };
    
    public void uniformBlockBinding(final int uniformBlockIndex, final int binding) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL31.glUniformBlockBinding(this.handle, uniformBlockIndex, binding);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "UniformBlockBinding";
    }
    
    public void uniform1(final int loc, final int x) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform1i(handle, loc, x);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform1i";
    }
    
    public void uniform1(final int loc, final float x) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform1f(handle, loc, x);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform1f";
    }
    
    public void uniform2(final int loc, final int x, final int y) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform2i(handle, loc, x, y);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform2i";
    }
    
    public void uniform2(final int loc, final float x, final float y) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform2f(handle, loc, x, y);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform2f";
    }
    
    public void uniform3(final int loc, final int x, final int y, final int z) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform3i(handle, loc, x, y, z);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform3i";
    }
    
    public void uniform3(final int loc, final float x, final float y, final float z) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform3f(handle, loc, x, y, z);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform3f";
    }
    
    public void uniform4(final int loc, final int x, final int y, final int z, final int w) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform4i(handle, loc, x, y, z, w);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform4i";
    }
    
    public void uniform4(final int loc, final float x, final float y, final float z, final float w) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform4f(handle, loc, x, y, z, w);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform4f";
    }
    
    public void uniform1(final int loc, final IntBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform1iv(handle, loc, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform1iv";
    }
    
    public void uniform1(final int loc, final FloatBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform1fv(handle, loc, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform1fv";
    }
    
    public void uniform2(final int loc, final IntBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform2iv(handle, loc, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform2iv";
    }
    
    public void uniform2(final int loc, final FloatBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform2fv(handle, loc, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform2fv";
    }
    
    public void uniform3(final int loc, final IntBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform3iv(handle, loc, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform3iv";
    }
    
    public void uniform3(final int loc, final FloatBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform3fv(handle, loc, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform3fv";
    }
    
    public void uniform4(final int loc, final IntBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform4iv(handle, loc, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform4iv";
    }
    
    public void uniform4(final int loc, final FloatBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniform4fv(handle, loc, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniform4fv";
    }
    
    public void uniformMatrix2(final int loc, final boolean transpose, final FloatBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniformMatrix2fv(handle, loc, transpose, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniformMatrix2fv";
    }
    
    public void uniformMatrix3(final int loc, final boolean transpose, final FloatBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniformMatrix3fv(handle, loc, transpose, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniformMatrix3fv";
    }
    
    public void uniformMatrix4(final int loc, final boolean transpose, final FloatBuffer value) {
        assert this.isValid() : "GLProgram is not valid!";
        
        GL41.glProgramUniformMatrix4fv(handle, loc, transpose, value);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ProgramUniformMatrix4fv";
    }
    
    public int getSubroutineIndex(final GLShaderType shaderType, final String name) {
        assert this.isValid() : "GLProgram is not valid!";
        
        final int out = GL40.glGetSubroutineIndex(this.handle, shaderType.value, name);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetSubroutineIndex";
        
        return out;
    }
    
    public int getBinarySize() {
        assert this.isValid() : "GLProgram is not valid!";
        
        return GL20.glGetProgrami(this.handle, GL41.GL_PROGRAM_BINARY_LENGTH);
    }
    
    public void getBinary(final GLProgramBinaryFormat format, final ByteBuffer binary) {
        assert this.isValid() : "GLProgram is not valid!";               
        
        try (MemoryStack mem = MemoryStack.stackPush()) {
            ARBGetProgramBinary.glGetProgramBinary(this.handle, null, mem.ints(format.getValue()), binary);
        }
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetProgramBinary";
    }
}
