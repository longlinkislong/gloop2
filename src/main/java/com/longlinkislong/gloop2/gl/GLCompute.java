/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.ARBComputeShader;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GLCapabilities;

/**
 *
 * @author zmichaels
 */
public final class GLCompute {

    private GLCompute() {
    }
    
    private static final class ComputeLimits {
        private final int maxUniformBlocks;
        private final int maxImageUnits;
        private final int maxSharedMemorySize;
        private final int maxUniformComponents;
        private final int maxAtomicCounterBuffers;
        private final int maxAtomicCounters;
        private final int maxCombinedUniformComponents;
        private final int maxWorkGroupsInvocations;
        private final int[] maxWorkGroupCount = new int[3];
        private final int[] maxWorkGroupSize = new int[3];
        
        private ComputeLimits() {
            this.maxUniformBlocks = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_UNIFORM_BLOCKS);
            this.maxImageUnits = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS);
            this.maxSharedMemorySize = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_SHARED_MEMORY_SIZE);
            this.maxUniformComponents = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_UNIFORM_COMPONENTS);
            this.maxAtomicCounterBuffers = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS);
            this.maxAtomicCounters = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_ATOMIC_COUNTERS);
            this.maxCombinedUniformComponents = GL11.glGetInteger(GL43.GL_MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS);
            this.maxWorkGroupsInvocations = GL11.glGetInteger(GL43.GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS);
            
            for (int i = 0; i < 3; i++) {
                maxWorkGroupCount[i] = GL30.glGetIntegeri(GL43.GL_MAX_COMPUTE_WORK_GROUP_COUNT, i);
                maxWorkGroupSize[i] = GL30.glGetIntegeri(GL43.GL_MAX_COMPUTE_WORK_GROUP_SIZE, i);
            }
        }
    }
    
    private static final ThreadLocal<ComputeLimits> LIMITS = new ThreadLocal<ComputeLimits>() {
        @Override
        protected ComputeLimits initialValue() {
            return new ComputeLimits();
        }
    };
    
    public static int getMaxWorkGroupSizeZ() {
        return LIMITS.get().maxWorkGroupSize[2];
    }
    
    public static int getMaxWorkGroupSizeY() {
        return LIMITS.get().maxWorkGroupSize[1];
    }
    
    public static int getMaxWorkGroupSizeX() {
        return LIMITS.get().maxWorkGroupSize[0];
    }
    
    public static int getMaxWorkGroupCountZ() {
        return LIMITS.get().maxWorkGroupCount[2];
    }
    
    public static int getMaxWorkGroupCountY() {
        return LIMITS.get().maxWorkGroupCount[1];
    }
    
    public static int getMaxWorkGroupCountX() {
        return LIMITS.get().maxWorkGroupCount[0];
    }
    
    public static int getMaxWorkGroupsInvocations() {
        return LIMITS.get().maxWorkGroupsInvocations;
    }
    
    public static int getMaxCombinedUniformComponents() {
        return LIMITS.get().maxCombinedUniformComponents;
    }
    
    public static int getMaxAtomicCounters() {
        return LIMITS.get().maxAtomicCounters;
    }
    
    public static int getMaxAtomicCounterBuffers() {
        return LIMITS.get().maxAtomicCounterBuffers;
    }
    
    public static int getMaxUniformBlocks() {
        return LIMITS.get().maxUniformBlocks;
    }
    
    public static int getMaxImageUnits() {
        return LIMITS.get().maxImageUnits;
    }
    
    public static int getMaxSharedMemorySize() {
        return LIMITS.get().maxSharedMemorySize;
    }
    
    public static int getMaxUniformComponents() {
        return LIMITS.get().maxUniformComponents;
    }

    public static void dispatchCompute(final int numGroupsX) {
        dispatchCompute(numGroupsX, 1, 1);
    }
    
    public static void dispatchCompute(final int numGroupsX, final int numGroupsY) {
        dispatchCompute(numGroupsX, numGroupsY, 1);
    }        
    
    public static void dispatchCompute(final int numGroupsX, final int numGroupsY, final int numGroupsZ) {
        final GLCapabilities caps = GL.getCapabilities();

        if (caps.OpenGL43) {
            GL43.glDispatchCompute(numGroupsX, numGroupsY, numGroupsZ);
        } else if (caps.GL_ARB_compute_shader) {
            ARBComputeShader.glDispatchCompute(numGroupsX, numGroupsY, numGroupsZ);
        } else {
            throw new UnsupportedOperationException("Compute Shaders are not supported!");
        }
    }
    
    public static void dispatchComputeIndirect(final long offset) {
        final GLCapabilities caps = GL.getCapabilities();
        
        if (caps.OpenGL43) {
            GL43.glDispatchComputeIndirect(offset);
        } else if (caps.GL_ARB_compute_shader) {
            ARBComputeShader.glDispatchComputeIndirect(offset);
        } else {
            throw new UnsupportedOperationException("Compute Shaders are not supported!");
        }
        
        
    }
}
