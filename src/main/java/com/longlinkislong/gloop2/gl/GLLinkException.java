/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

/**
 *
 * @author zmichaels
 */
public class GLLinkException extends RuntimeException {
    public GLLinkException(final String infoLog) {
        super(infoLog);
    }
}
