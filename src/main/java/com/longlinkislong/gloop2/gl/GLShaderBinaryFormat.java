/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import java.nio.IntBuffer;
import java.util.concurrent.Semaphore;
import org.lwjgl.opengl.ARBGLSPIRV;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL41;
import org.lwjgl.system.MemoryStack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public enum GLShaderBinaryFormat implements CEnum {
    GLSL {
        @Override
        public int getValue() {
            throw new UnsupportedOperationException("GLSL format cannot be used as binary!");
        }

        @Override
        public boolean isSupported() {
            return true;
        }
    },
    SPIRV {
        @Override
        public int getValue() {
            return ARBGLSPIRV.GL_SHADER_BINARY_FORMAT_SPIR_V_ARB;
        }

        @Override
        public boolean isSupported() {
            return GL.getCapabilities().GL_ARB_gl_spirv;
        }
    },
    PROPRIETARY {
        final ThreadLocal<Integer> value = new ThreadLocal<Integer>() {
            @Override
            protected Integer initialValue() {
                final int numSupportedFormats = GL11.glGetInteger(GL41.GL_NUM_SHADER_BINARY_FORMATS);
                
                if (numSupportedFormats == 0) {
                    throw new UnsupportedOperationException("GLShaderBinary is not supported!");
                }
                
                final int out = GL11.glGetInteger(GL41.GL_SHADER_BINARY_FORMATS);
                
                if (out == ARBGLSPIRV.GL_SHADER_BINARY_FORMAT_SPIR_V_ARB) {
                    LOGGER.debug("Proprietary ShaderBinaryFormat is SPIRV!");
                }
                
                return out;
            }
        };

        @Override
        public int getValue() {
            return this.value.get();
        }

        @Override
        public boolean isSupported() {
            try {
                return getValue() > 0;
            } catch (UnsupportedOperationException ex) {
                return false;
            }
        }
    };

    public abstract boolean isSupported();

    private static final Logger LOGGER = LoggerFactory.getLogger(GLShaderBinaryFormat.class);
}
