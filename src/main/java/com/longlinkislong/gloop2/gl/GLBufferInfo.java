/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import org.lwjgl.opengl.GL15;

/**
 *
 * @author zmichaels
 */
public class GLBufferInfo {

    public static enum Usage implements CEnum {
        STATIC_DRAW(GL15.GL_STATIC_DRAW),
        DYNAMIC_DRAW(GL15.GL_DYNAMIC_DRAW),
        STREAM_DRAW(GL15.GL_STREAM_DRAW),
        STATIC_READ(GL15.GL_STATIC_READ),
        DYNAMIC_READ(GL15.GL_DYNAMIC_READ),
        STREAM_READ(GL15.GL_STREAM_READ),
        STATIC_COPY(GL15.GL_STATIC_COPY),
        DYNAMIC_COPY(GL15.GL_DYNAMIC_COPY),
        STREAM_COPY(GL15.GL_STREAM_COPY);

        public final int value;

        private Usage(final int value) {
            this.value = value;
        }
        
        public static Optional<Usage> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }

        @Override
        public int getValue() {
            return this.value;
        }
    }

    public final long size;
    public final Usage usage;
    public final GLBufferTarget target;
    public final ByteBuffer initialData;
    public final Set<GLBufferStorage> storage;

    public GLBufferInfo(
            final long size,
            final Usage usage,
            final GLBufferTarget preferredTarget,
            final ByteBuffer initialData,
            final Collection<GLBufferStorage> storage) {

        this.size = size;
        this.usage = usage;
        this.target = preferredTarget;
        this.initialData = initialData;
        this.storage = Tools.copySet(storage);
    }
    
    public GLBufferInfo() {
        this(0L, Usage.STATIC_DRAW, GLBufferTarget.ARRAY, null, null);
    }
    
    public GLBufferInfo withSize(final long size) {
        return new GLBufferInfo(size, usage, target, initialData, storage);
    }
    
    public GLBufferInfo withUsage(final Usage usage) {
        return new GLBufferInfo(size, usage, target, initialData, storage);
    }
    
    public GLBufferInfo withPreferredTarget(final GLBufferTarget preferredTarget) {
        return new GLBufferInfo(size, usage, preferredTarget, initialData, storage);
    }
    
    public GLBufferInfo withInitialData(final ByteBuffer initialData) {
        return new GLBufferInfo(size, usage, target, initialData, storage);
    }
    
    public GLBufferInfo withStorage(final Collection<GLBufferStorage> storage) {
        return new GLBufferInfo(size, usage, target, initialData, storage);
    }
    
    public GLBufferInfo withStorage(final GLBufferStorage... storage) {
        return withStorage(Arrays.asList(storage));
    }
    
    public GLBuffer create() {
        return new GLBuffer(this);
    }
}
