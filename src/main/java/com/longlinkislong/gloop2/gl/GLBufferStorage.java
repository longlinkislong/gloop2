/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL44;

/**
 *
 * @author zmichaels
 */
public enum GLBufferStorage implements CBitfield {
    READ(GL30.GL_MAP_READ_BIT),
    WRITE(GL30.GL_MAP_WRITE_BIT),
    DYNAMIC_STORAGE(GL44.GL_DYNAMIC_STORAGE_BIT),
    CLIENT_STORAGE(GL44.GL_CLIENT_STORAGE_BIT),
    COHERENT(GL44.GL_MAP_COHERENT_BIT),
    PERSISTENT(GL44.GL_MAP_PERSISTENT_BIT);
    
    public final int value;
    
    private GLBufferStorage(final int value) {
        this.value = value;
    }
    
    public static Set<GLBufferStorage> flags(final int bits) {
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<GLBufferStorage> flags) {
        return Tools.flagsToBitfield(flags);
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
