/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class GLVertexArrayInfo {

    public static final class AttributeDescription {

        public final int location;
        public final GLVertexFormat format;
        public final int offset;
        public final int binding;

        public AttributeDescription(
                final int location, final int binding, final GLVertexFormat format, final int offset) {
            
            this.location = location;
            this.binding = binding;
            this.format = format;
            this.offset = offset;
        }
        
        public AttributeDescription() {
            this(0, 0, GLVertexFormat.VEC4, 0);
        }
        
        public AttributeDescription withLocation(final int location) {
            return new AttributeDescription(location, binding, format, offset);
        }
        
        public AttributeDescription withOffset(final int offset) {
            return new AttributeDescription(location, binding, format, offset);
        }
        
        public AttributeDescription withBinding(final int binding) {
            return new AttributeDescription(location, binding, format, offset);
        }
        
        public AttributeDescription withFormat(final GLVertexFormat format) {
            return new AttributeDescription(location, binding, format, offset);
        }
    }
    
    public static final class BindingDescription {
        public final int binding;
        public final int stride;
        public final int divisor;
        public final GLBuffer buffer;
        public final long offset;
        
        public BindingDescription(final int binding, final int stride, final int divisor, final GLBuffer buffer, final long offset) {
            this.binding = binding;
            this.stride = stride;
            this.divisor = divisor;
            this.buffer = buffer;
            this.offset = offset;
        }
        
        public BindingDescription() {
            this(0, 0, 0, null, 0);
        }
        
        public BindingDescription withBinding(final int binding) {
            return new BindingDescription(binding, stride, divisor, buffer, offset);
        }
        
        public BindingDescription withStride(final int stride) {
            return new BindingDescription(binding, stride, divisor, buffer, offset);
        }
        
        public BindingDescription withDivisor(final int divisor) {
            return new BindingDescription(binding, stride, divisor, buffer, offset);
        }
        
        public BindingDescription withBuffer(final GLBuffer buffer) {
            return new BindingDescription(binding, stride, divisor, buffer, offset);
        }
        
        public BindingDescription withOffset(final long offset) {
            return new BindingDescription(binding, stride, divisor, buffer, offset);
        }
    }

    public final GLBuffer indexBuffer;
    public final List<AttributeDescription> attributes;
    public final List<BindingDescription> bindings;

    public GLVertexArrayInfo(
            final GLBuffer indexBuffer,
            final List<AttributeDescription> attributes,
            final List<BindingDescription> bindings) {

        this.indexBuffer = indexBuffer;
        this.attributes = Tools.copyList(attributes);
        this.bindings = Tools.copyList(bindings);
    }

    public GLVertexArrayInfo() {
        this(null, null, null);
    }

    public GLVertexArrayInfo withIndexBuffer(final GLBuffer indexBuffer) {
        return new GLVertexArrayInfo(indexBuffer, attributes, bindings);
    }

    public GLVertexArrayInfo withAttributes(final List<AttributeDescription> attributes) {
        return new GLVertexArrayInfo(indexBuffer, attributes, bindings);
    }

    public GLVertexArrayInfo withAttributes(final AttributeDescription... attributes) {
        return withAttributes(Arrays.asList(attributes));
    }
    
    public GLVertexArrayInfo withBindings(final List<BindingDescription> bindings) {
        return new GLVertexArrayInfo(indexBuffer, attributes, bindings);
    }
    
    public GLVertexArrayInfo withBindings(final BindingDescription... bindings) {
        return withBindings(Arrays.asList(bindings));
    }

    public GLVertexArray create() {
        return new GLVertexArray(this);
    }
}
