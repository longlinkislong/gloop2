/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;

/**
 *
 * @author zmichaels
 */
public class GLSamplerInfo {

    public static final class Swizzle {

        public static enum Mode implements CEnum {
            // pseudo-state
            IDENTITY(-1),
            ONE(GL11.GL_ONE),
            ZERO(GL11.GL_ZERO),
            RED(GL11.GL_RED),
            GREEN(GL11.GL_GREEN),
            BLUE(GL11.GL_BLUE),
            ALPHA(GL11.GL_ALPHA);

            public final int value;

            private Mode(final int value) {
                this.value = value;
            }
            
            public static Optional<Mode> of(final int value) {
                return Tools.valueOf(value, Arrays.stream(values()));
            }

            @Override
            public int getValue() {
                return this.value;
            }
        }

        public final Mode r;
        public final Mode g;
        public final Mode b;
        public final Mode a;

        public Swizzle(final Mode r, final Mode g, final Mode b, final Mode a) {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public Swizzle() {
            this(Mode.IDENTITY, Mode.IDENTITY, Mode.IDENTITY, Mode.IDENTITY);
        }
    }

    public static enum AddressMode implements CEnum {
        REPEAT(GL11.GL_REPEAT),
        MIRRORED_REPEAT(GL14.GL_MIRRORED_REPEAT),
        CLAMP_TO_EDGE(GL12.GL_CLAMP_TO_EDGE);

        public final int value;

        private AddressMode(final int value) {
            this.value = value;
        }

        @Override
        public int getValue() {
            return this.value;
        }
        
        public static Optional<AddressMode> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }
    }

    public final GLFilter minFilter;
    public final GLFilter magFilter;
    public final float minLOD;
    public final float maxLOD;

    public final Swizzle swizzle;
    public final AddressMode wrapS;
    public final AddressMode wrapT;
    public final AddressMode wrapR;
    public final float maxAnisotropy;

    //TODO: support texture compare func
    public GLSamplerInfo(
            final GLFilter minFilter, final GLFilter magFilter,
            final float minLOD, final float maxLOD,
            final Swizzle swizzle,
            final AddressMode wrapS, final AddressMode wrapT, final AddressMode wrapR,
            final float maxAnisotropy) {

        this.minFilter = minFilter;
        this.magFilter = magFilter;
        this.minLOD = minLOD;
        this.maxLOD = maxLOD;
        this.swizzle = swizzle;
        this.wrapS = wrapS;
        this.wrapT = wrapT;
        this.wrapR = wrapR;
        this.maxAnisotropy = maxAnisotropy;
    }

    public GLSamplerInfo() {
        this(
                GLFilter.NEAREST_MIPMAP_LINEAR, GLFilter.LINEAR,
                -1000F, 1000F,
                new Swizzle(),
                AddressMode.REPEAT, AddressMode.REPEAT, AddressMode.REPEAT,
                1.0F);
    }
    
    public GLSamplerInfo withMinFilter(final GLFilter minFilter) {
        return new GLSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLSamplerInfo withMagFilter(final GLFilter magFilter) {
        return new GLSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLSamplerInfo withMinLOD(final float minLOD) {
        return new GLSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLSamplerInfo withMaxLOD(final float maxLOD) {
        return new GLSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLSamplerInfo withSwizzle(final Swizzle swizzle) {
        return new GLSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLSamplerInfo withWrapS(final AddressMode wrapS) {
        return new GLSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLSamplerInfo withWrapT(final AddressMode wrapT) {
        return new GLSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLSamplerInfo withWrapR(final AddressMode wrapR) {
        return new GLSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLSampler create() {
        return new GLSampler(this);
    }
}
