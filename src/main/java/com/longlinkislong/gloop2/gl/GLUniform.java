/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL40;

/**
 *
 * @author zmichaels
 */
public final class GLUniform {
    private GLUniform() {}
    
    public static void uniformSubroutine(final GLShaderType shadertype, final int index) {
        GL40.glUniformSubroutinesui(shadertype.value, index);
    }
    
    public static void uniformSubroutine(final GLShaderType shadertype, final IntBuffer indices) {
        GL40.glUniformSubroutinesuiv(shadertype.value, indices);
    }
    
    public static void uniform1(final int location, final int value) {
        GL20.glUniform1i(location, value);
    }
    
    public static void uniform1i(final int location, final IntBuffer values) {
        GL20.glUniform1iv(location, values);
    }
    
    public static void uniform2(final int location, final int x, final int y) {
        GL20.glUniform2i(location, x, y);
    }
    
    public static void uniform2(final int location, final IntBuffer values) {
        GL20.glUniform2iv(location, values);
    }
    
    public static void uniform3(final int location, final int x, final int y, final int z) {
        GL20.glUniform3i(location, x, y, z);
    }
    
    public static void uniform3(final int location, final IntBuffer values) {
        GL20.glUniform3iv(location, values);
    }
    
    public static void uniform4(final int location, final int x, final int y, final int z, final int w) {
        GL20.glUniform4i(location, x, y, z, w);
    }
    
    public static void uniform4(final int location, final IntBuffer values) {
        GL20.glUniform4iv(location, values);
    }
    
    public static void uniform1(final int location, final float value) {
        GL20.glUniform1f(location, value);
    }
    
    public static void uniform1(final int location, final FloatBuffer values) {
        GL20.glUniform1fv(location, values);
    }
    
    public static void uniform2(final int location, final float x, final float y) {
        GL20.glUniform2f(location, x, y);
    }
    
    public static void uniform2(final int location, final FloatBuffer values) {
        GL20.glUniform2fv(location, values);
    }
    
    public static void uniform3(final int location, final float x, final float y, final float z) {
        GL20.glUniform3f(location, x, y, z);
    }
    
    public static void uniform3(final int location, final FloatBuffer values) {
        GL20.glUniform3fv(location, values);
    }
    
    public static void uniform4(final int location, final float x, final float y, final float z, final float w) {
        GL20.glUniform4f(location, x, y, z, w);
    }
    
    public static void uniform4(final int location, final FloatBuffer values) {
        GL20.glUniform4fv(location, values);
    }
    
    public static void uniformMatrix2(final int location, final boolean transpose, final FloatBuffer values) {
        GL20.glUniformMatrix2fv(location, transpose, values);
    }
    
    public static void uniformMatrix3(final int location, final boolean transpose, final FloatBuffer values) {
        GL20.glUniformMatrix3fv(location, transpose, values);
    }
    
    public static void uniformMatrix4(final int location, final boolean transpose, final FloatBuffer values) {
        GL20.glUniformMatrix4fv(location, transpose, values);
    }        
}
