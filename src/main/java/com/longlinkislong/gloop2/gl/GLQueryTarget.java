/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL33;
import org.lwjgl.opengl.GL43;

/**
 *
 * @author zmichaels
 */
public enum GLQueryTarget implements CEnum {
    ANY_SAMPLES_PASSED(GL33.GL_ANY_SAMPLES_PASSED),
    ANY_SAMPLES_PASSED_CONSERVATIVE(GL43.GL_ANY_SAMPLES_PASSED_CONSERVATIVE);
    
    public final int value;
    
    private GLQueryTarget(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }
    
    public Optional<GLQueryTarget> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
