/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL32;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLSync extends GLObject<GLFenceInfo, GLSync> {     
    long handle;
    
    private GLSync(final GLFenceInfo info) {
        this.info = info;
        this.handle = GL32.glFenceSync(info.condition.value, info.flags);        
    }
    
    
    @Override
    public void swap(final GLSync other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final long tmpHandle = other.handle;
        final GLFenceInfo tmpInfo = other.info;
        
        other.handle = this.handle;
        other.info = this.info;
        
        this.handle = tmpHandle;
        this.info = tmpInfo;
    }
    
    public static enum ClientWait {
        FLUSH_COMMANDS(GL32.GL_SYNC_FLUSH_COMMANDS_BIT);
        
        public final int value;
        
        private ClientWait(final int value) {
            this.value = value;
        }
    }
    
    public void clientWaitSync(final ClientWait wait, final long timeout) {
        GL32.glClientWaitSync(handle, wait == null ? 0 : wait.value, timeout);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "ClientWaitSync";
    }
    
    public void waitSync() {
        GL32.glWaitSync(handle, 0, GLES30.GL_TIMEOUT_IGNORED);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "WaitSync";
    }
    
    public static GLSync fence(final GLFenceInfo info) {
        return new GLSync(info);
    }
    
    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GL32.glDeleteSync(this.handle);
            this.handle = 0L;
            this.info = null;
        }
    }
}
