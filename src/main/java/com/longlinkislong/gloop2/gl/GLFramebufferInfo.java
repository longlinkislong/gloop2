/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class GLFramebufferInfo {
    public final List<GLAttachment> attachments;
    
    public GLFramebufferInfo(final List<GLAttachment> attachments) {
        this.attachments = Tools.copyList(attachments);
    }
    
    public GLFramebufferInfo withAttachments(final List<GLAttachment> attachments) {
        return new GLFramebufferInfo(attachments);
    }
    
    public GLFramebufferInfo withAttachments(final GLAttachment... attachments) {
        return withAttachments(Arrays.asList(attachments));
    }
    
    public GLFramebuffer create() {
        return new GLFramebuffer(this);
    }
}
