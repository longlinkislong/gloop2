/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL30;

/**
 *
 * @author zmichaels
 */
public enum GLFormat implements CEnum {
    ALPHA(GL11.GL_ALPHA),
    RGBA(GL11.GL_RGBA),
    RGB(GL11.GL_RGB),
    RG(GL30.GL_RG),
    RED(GL11.GL_RED),
    BGR(GL12.GL_BGR),
    BGRA(GL12.GL_BGRA),
    BGR_INTEGER(GL30.GL_BGR_INTEGER),
    BGRA_INTEGER(GL30.GL_BGRA_INTEGER),
    RGBA_INTEGER(GL30.GL_RGBA_INTEGER),
    RGB_INTEGER(GL30.GL_RGB_INTEGER),
    RG_INTEGER(GL30.GL_RG_INTEGER),
    RED_INTEGER(GL30.GL_RED_INTEGER),
    DEPTH_COMPONENT(GL11.GL_DEPTH_COMPONENT),
    DEPTH_STENCIL(GL30.GL_DEPTH_STENCIL);
    
    public final int value;
    
    private GLFormat(final int value) {
        this.value = value;
    }
    
    public static Optional<GLFormat> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
