/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.Half;
import com.longlinkislong.gloop2.image.Image;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL30;

/**
 *
 * @author zmichaels
 */
public final class GLPixelInfo {

    public static enum Type {
        UNSIGNED_BYTE(GL11.GL_UNSIGNED_BYTE) {
            @Override
            public int r(int v) {
                return v & 0xFF;
            }
        },
        UNSIGNED_SHORT(GL11.GL_UNSIGNED_SHORT) {
            @Override
            public int r(int v) {
                return v & 0xFFFF;
            }
        },
        UNSIGNED_INT(GL11.GL_UNSIGNED_INT),
        BYTE(GL11.GL_BYTE) {
            @Override
            public int r(int v) {
                return ((byte) v) & 0xFF;
            }
        },
        SHORT(GL11.GL_SHORT) {
            @Override
            public int r(int v) {
                return ((short) v) & 0xFF;
            }
        },
        INT(GL11.GL_INT),
        HALF_FLOAT(GL30.GL_HALF_FLOAT) {
            @Override
            public int r(int v) {
                return Half.floatToIntBits(Float.floatToIntBits(v));
            }
        },
        FLOAT(GL11.GL_FLOAT),
        UNSIGNED_BYTE_3_3_2(GL12.GL_UNSIGNED_BYTE_3_3_2) {
            @Override
            public int pack(final int r, final int g, final int b, final int a) {
                return ((r & 0x7) << 5)
                        | ((g & 0x7) << 2)
                        | (b & 0x3);
            }

            @Override
            public int r(int v) {
                return (v >>> 5) & 0x7;
            }

            @Override
            public int g(int v) {
                return (v >>> 2) & 0x7;
            }

            @Override
            public int b(int v) {
                return v & 0x3;
            }
        },
        UNSIGNED_BYTE_2_3_3_REV(GL12.GL_UNSIGNED_BYTE_2_3_3_REV) {
            @Override
            public int pack(final int b, final int g, final int r, final int a) {
                return ((b & 0x3) << 6)
                        | ((g & 0x7) << 3)
                        | ((r & 0x7));
            }

            @Override
            public int b(final int v) {
                return (v >>> 6) & 0x3;
            }

            @Override
            public int g(final int v) {
                return (v >>> 3) & 0x7;
            }

            @Override
            public int r(final int v) {
                return (v & 0x3);
            }
        },
        UNSIGNED_SHORT_4_4_4_4_REV(GL12.GL_UNSIGNED_SHORT_4_4_4_4_REV) {
            @Override
            public int pack(final int a, final int b, final int g, final int r) {
                return ((a & 0xF) << 12)
                        | ((b & 0xF) << 8)
                        | ((g & 0xF) << 4)
                        | ((r & 0xF));
            }

            @Override
            public int a(final int v) {
                return (v >>> 12) & 0xF;
            }

            @Override
            public int b(final int v) {
                return (v >>> 8) & 0xF;
            }

            @Override
            public int g(final int v) {
                return (v >>> 4) & 0xF;
            }

            @Override
            public int r(final int v) {
                return (v & 0xF);
            }
        },
        UNSIGNED_SHORT_4_4_4_4(GL12.GL_UNSIGNED_SHORT_4_4_4_4) {
            @Override
            public int pack(final int r, final int g, final int b, final int a) {
                return ((r & 0xF) << 12)
                        | ((g & 0xF) << 8)
                        | ((b & 0xF) << 4)
                        | ((a & 0xF));
            }

            @Override
            public int r(final int s) {
                return (s >> 12) & 0xF;
            }

            @Override
            public int g(final int s) {
                return (s >> 8) & 0xF;
            }

            @Override
            public int b(final int s) {
                return (s >> 4) & 0xF;
            }

            @Override
            public int a(final int s) {
                return s & 0xF;
            }
        },
        UNSIGNED_SHORT_1_5_5_5_REV(GL12.GL_UNSIGNED_SHORT_1_5_5_5_REV) {
            @Override
            public int pack(final int a, final int b, final int g, final int r) {
                return ((a & 0x1) << 15)
                        | ((b & 0x1F) << 10)
                        | ((g & 0x1F) << 5)
                        | ((r & 0x1F));
            }

            @Override
            public int a(final int v) {
                return (v >>> 15) & 0x1;
            }

            @Override
            public int b(final int v) {
                return (v >>> 10) & 0x1F;
            }

            @Override
            public int g(final int v) {
                return (v >>> 5) & 0x1F;
            }

            @Override
            public int r(final int v) {
                return (v & 0x1F);
            }
        },
        UNSIGNED_SHORT_5_5_5_1(GL12.GL_UNSIGNED_SHORT_5_5_5_1) {
            @Override
            public int pack(int r, int g, int b, int a) {
                return ((r & 0x1F) << 11)
                        | ((g & 0x1F) << 6)
                        | ((b & 0x1F) << 1)
                        | ((a & 0x1));
            }

            @Override
            public int r(int v) {
                return (v >>> 11) & 0x1F;
            }

            @Override
            public int g(int v) {
                return (v >>> 6) & 0x3F;
            }

            @Override
            public int b(int v) {
                return (v >>> 1) & 0x3F;
            }

            @Override
            public int a(int v) {
                return v & 0x1;
            }
        },
        UNSIGNED_SHORT_5_6_5_REV(GL12.GL_UNSIGNED_SHORT_5_6_5_REV) {
            @Override
            public int pack(final int b, final int g, final int r, final int a) {
                return ((b & 0x1F) << 11)
                        | ((g & 0x3F) << 5)
                        | ((r & 0x1F));
            }

            @Override
            public int b(final int v) {
                return (v >>> 11) & 0x1F;
            }

            @Override
            public int g(final int v) {
                return (v >>> 5) & 0x3F;
            }

            @Override
            public int r(final int v) {
                return (v & 0x1F0);
            }
        },
        UNSIGNED_SHORT_5_6_5(GL12.GL_UNSIGNED_SHORT_5_6_5) {
            @Override
            public int pack(int r, int g, int b, int a) {
                return ((r & 0x1F) << 11)
                        | ((g & 0x3F) << 5)
                        | ((b & 0x1F));
            }

            @Override
            public int r(int v) {
                return (v >>> 11) & 0x1F;
            }

            @Override
            public int g(int v) {
                return (v >>> 5) & 0x3F;
            }

            @Override
            public int b(int v) {
                return v & 0x1F;
            }
        },
        UNSIGNED_INT_8_8_8_8(GL12.GL_UNSIGNED_INT_8_8_8_8) {
            @Override
            public int pack(final int r, final int g, final int b, final int a) {
                return ((r & 0xFF) << 24)
                        | ((g & 0xFF) << 16)
                        | ((b & 0xFF) << 8)
                        | ((a & 0xFF));
            }

            @Override
            public int r(final int v) {
                return (v >>> 24) & 0xFF;
            }

            @Override
            public int g(final int v) {
                return (v >>> 16) & 0xFF;
            }

            @Override
            public int b(final int v) {
                return (v >>> 8) & 0xFF;
            }

            @Override
            public int a(final int v) {
                return v & 0xFF;
            }
        },
        UNSIGNED_INT_8_8_8_8_REV(GL12.GL_UNSIGNED_INT_8_8_8_8_REV) {
            @Override
            public int pack(final int a, final int b, final int g, final int r) {
                return ((a & 0xFF) << 24)
                        | ((b & 0xFF) << 16)
                        | ((g & 0xFF) << 8)
                        | ((r & 0xFF));
            }
            
            @Override
            public int a(final int v) {
                return (v >>> 24) & 0xFF;
            }
            
            @Override
            public int b(final int v) {
                return (v >>> 16) & 0xFF;
            }
            
            @Override
            public int g(final int v) {
                return (v >>> 8) & 0xFF;
            }
            
            @Override
            public int r(final int v) {
                return v & 0xFF;
            }
        },
        UNSIGNED_INT_10_10_10_2(GL12.GL_UNSIGNED_INT_10_10_10_2) {
            @Override
            public int pack(int r, int g, int b, int a) {
                return ((r & 0x3FF) << 22)
                        | ((g & 0x3FF) << 12)
                        | ((b & 0x3FF) << 2)
                        | ((a & 0x3));
            }

            @Override
            public int r(int v) {
                return (v >>> 22) & 0x3FF;
            }

            @Override
            public int g(int v) {
                return (v >>> 12) & 0x3FF;
            }

            @Override
            public int b(int v) {
                return (v >>> 2) & 0x3FF;
            }

            @Override
            public int a(int v) {
                return v & 0x3;
            }
        },
        UNSIGNED_INT_2_10_10_10_REV(GL12.GL_UNSIGNED_INT_2_10_10_10_REV) {
            @Override
            public int pack(int a, int b, int g, int r) {
                return ((a & 0x3) << 30)
                        | ((b & 0x3FF) << 20)
                        | ((g & 0x3FF) << 10)
                        | ((r & 0x3FF));
            }

            @Override
            public int a(int v) {
                return (v >>> 30) & 0x3;
            }

            @Override
            public int b(int v) {
                return (v >>> 20) & 0x3FF;
            }

            @Override
            public int g(int v) {
                return (v >>> 10) & 0x3FF;
            }

            @Override
            public int r(int v) {
                return v & 0x3FF;
            }
        },
        UNSIGNED_INT_24_8(GL30.GL_UNSIGNED_INT_24_8) {
            @Override
            public int pack(int d, int s, int unused0, int unused1) {
                return ((d & 0xFF_FFFF) << 8) | (s & 0xFF);
            }

            @Override
            public int r(int v) {
                return (v >>> 8) & 0xFF_FFFF;
            }

            @Override
            public int g(int v) {
                return v & 0xFF;
            }
        },
        UNSIGNED_INT_5_9_9_9_REV(GL30.GL_UNSIGNED_INT_5_9_9_9_REV);

        public final int value;

        public String toString(int v) {
            return String.format("<%d, %d, %d, %d>", r(v), g(v), b(v), a(v));
        }

        public int r(int v) {
            return v;
        }

        public int g(int v) {
            return 0;
        }

        public int b(int v) {
            return 0;
        }

        public int a(int v) {
            return 0;
        }

        public int pack(int r, int g, int b, int a) {
            throw new UnsupportedOperationException("Type does not support packing!");
        }

        private Type(final int value) {
            this.value = value;
        }

        public static Optional<Type> of(final int value) {
            return Arrays.stream(values())
                    .filter(e -> e.value == value)
                    .findFirst();
        }
    }

    /**
     * GLBuffer offset. Only used if data is null. Useful for reading from pixel
     * buffers.
     */
    public final long offset;
    /**
     * Data pointer.
     */
    public final ByteBuffer data;
    public final GLFormat format;
    public final Type type;
    public final int size;

    public GLPixelInfo(final GLFormat format, final Type type, final ByteBuffer data, final long offset, final int size) {
        this.data = data;
        this.format = format;
        this.type = type;
        this.offset = offset;
        this.size = size;
    }

    public GLPixelInfo withSize(final int size) {
        return new GLPixelInfo(format, type, data, offset, size);
    }

    public GLPixelInfo() {
        this(null, Type.UNSIGNED_BYTE, null, 0L, 0);
    }

    public GLPixelInfo withFormat(final GLFormat format) {
        return new GLPixelInfo(format, type, data, offset, size);
    }

    public GLPixelInfo withPtrOffset(final long offset) {
        return new GLPixelInfo(format, type, data, offset, size);
    }

    public GLPixelInfo withData(final ByteBuffer data) {
        return new GLPixelInfo(format, type, data, offset, size);
    }

    public GLPixelInfo withType(final Type type) {
        return new GLPixelInfo(format, type, data, offset, size);
    }

    public static GLPixelInfo of(final Image img) {
        final GLFormat format;
        final Type type;

        switch (img.getFormat()) {
            case R8_UNORM:
                format = GLFormat.RED;
                type = Type.UNSIGNED_BYTE;
                break;
            case R8G8_UNORM:
                format = GLFormat.RG;
                type = Type.UNSIGNED_BYTE;
                break;
            case R8G8B8_UNORM:
                format = GLFormat.RGB;
                type = Type.UNSIGNED_BYTE;
                break;
            case R8G8B8A8_UNORM:
                format = GLFormat.RGBA;
                type = Type.UNSIGNED_BYTE;
                break;
            default:
                throw new UnsupportedOperationException("Unsupported format: " + img.getFormat());
        }

        return new GLPixelInfo()
                .withData(img.getData())
                .withFormat(format)
                .withType(type);
    }
}
