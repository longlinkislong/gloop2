/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import java.nio.ByteBuffer;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL42;
import org.lwjgl.opengl.GL45;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public class GLTexture extends GLObject<GLTextureInfo, GLTexture> {

    public GLTexture() {
    }

    public GLTexture(final GLTextureInfo info) {
        assert info.samplerInfo.magFilter.magSupported : "Unsupported mag filter!";

        this.info = info;

        if (GL.getCapabilities().OpenGL45) {
            this.handle = GLTextureHelper.create45(info);
        } else {
            this.handle = GLTextureHelper.create33(info);
        }
    }

    public void generateMipmap() {
        assert this.isValid() : "GLTexture is not valid!";

        if (GL.getCapabilities().OpenGL45) {
            GL45.glGenerateTextureMipmap(handle);

            assert GL11.glGetError() == GL11.GL_NO_ERROR : "GenerateTextureMipmap";
        } else {
            GL11.glBindTexture(info.target().value, handle);
            GL30.glGenerateMipmap(info.target().value);
            GL11.glBindTexture(info.target().value, 0);

            assert GL11.glGetError() == GL11.GL_NO_ERROR : "GenerateMipmap";
        }
    }

    public void bind(int unit) {
        assert this.isValid() : "GLTexture is not valid!";

        if (GL.getCapabilities().OpenGL45) {
            GL45.glBindTextureUnit(unit, handle);

            assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindTextureUnit";
        } else {
            GL13.glActiveTexture(unit + GL13.GL_TEXTURE0);
            GL11.glBindTexture(info.target().value, handle);

            assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindTexture";
        }
    }

    public void getImage(final int level, final GLPixelInfo px) {
        assert this.isValid() : "GLTexture is not valid!";

        if (GL.getCapabilities().OpenGL45) {
            if (px.data == null) {
                GL45.glGetTextureImage(handle, level, px.format.value, px.type.value, px.size, px.offset);
            } else {
                GL45.glGetTextureImage(handle, level, px.format.value, px.type.value, px.data);
            }
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetTextureImage";
        } else {
            final GLTextureTarget tgt = info.target();

            GL11.glBindTexture(tgt.value, handle);
            
            if (px.data == null) {
                GL11.glGetTexImage(tgt.value, level, px.format.value, px.type.value, px.offset);
            } else {
                GL11.glGetTexImage(tgt.value, level, px.format.value, px.type.value, px.data);
            }
            
            GL11.glBindTexture(tgt.value, 0);

            assert GL11.glGetError() == GL11.GL_NO_ERROR : "GetTexImage";
        }
    }

    public void subImage(final int level, final int x, final int y, final int z, final int w, final int h, final int d, final GLPixelInfo px) {
        if (px.data != null && !px.data.isDirect()) {
            final ByteBuffer directData = MemoryUtil.memAlloc(px.data.remaining())
                    .order(px.data.order())
                    .put(px.data);
            
            directData.flip();
            
            try {
                subImage(level, x, y, z, w, h, d, px.withData(directData));
            } finally {
                MemoryUtil.memFree(directData);
            }
            
            return;
        }
        
        assert this.isValid() : "GLTexture is not valid!";

        final GLTextureTarget tgt = info.target();

        if (GL.getCapabilities().OpenGL45) {
            switch (tgt) {
                case TEXTURE_1D:
                    if (px.data == null) {
                        GL45.glTextureSubImage1D(handle, level, x, w, px.format.value, px.type.value, px.offset);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage1D";
                    } else {
                        GL45.glTextureSubImage1D(handle, level, x, w, px.format.value, px.type.value, px.data);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage1D";
                    }
                    break;
                case TEXTURE_2D:
                    if (px.data == null) {
                        GL45.glTextureSubImage2D(handle, level, x, y, w, h, px.format.value, px.type.value, px.offset);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage2D";
                    } else {
                        GL45.glTextureSubImage2D(handle, level, x, y, w, h, px.format.value, px.type.value, px.data);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage2D";
                    }
                    break;
                case TEXTURE_3D:
                    if (px.data == null) {
                        GL45.glTextureSubImage3D(handle, level, x, y, z, w, h, d, px.format.value, px.type.value, px.offset);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage3D";
                    } else {
                        GL45.glTextureSubImage3D(handle, level, x, y, z, w, h, d, px.format.value, px.type.value, px.data);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage3D";
                    }
                    break;
                case TEXTURE_1D_ARRAY:
                    if (px.data == null) {
                        GL45.glTextureSubImage2D(handle, level, x, y, w, h, px.format.value, px.type.value, px.offset);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage2D";
                    } else {
                        GL45.glTextureSubImage2D(handle, level, x, y, w, h, px.format.value, px.type.value, px.data);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage2D";
                    }
                    break;
                case TEXTURE_2D_ARRAY:
                    if (px.data == null) {
                        GL45.glTextureSubImage3D(handle, level, x, y, z, w, h, d, px.format.value, px.type.value, px.offset);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage3D";
                    } else {
                        GL45.glTextureSubImage3D(handle, level, x, y, z, w, h, d, px.format.value, px.type.value, px.data);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TextureSubImage3D";
                    }
                    break;
                default:
                    throw new UnsupportedOperationException("Unsupported Texture Target: " + tgt);
            }
        } else {
            switch (tgt) {
                case TEXTURE_1D:
                    GL11.glBindTexture(GL11.GL_TEXTURE_1D, handle);

                    if (px.data == null) {
                        GL11.glTexSubImage1D(GL11.GL_TEXTURE_1D, level, x, w, px.format.value, px.type.value, px.offset);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage1D";
                    } else {
                        GL11.glTexSubImage1D(GL11.GL_TEXTURE_1D, level, x, w, px.format.value, px.type.value, px.data);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage1D";
                    }

                    GL11.glBindTexture(GL11.GL_TEXTURE_1D, 0);
                    break;
                case TEXTURE_2D:
                    GL11.glBindTexture(GL11.GL_TEXTURE_2D, handle);

                    if (px.data == null) {
                        GL11.glTexSubImage2D(GL11.GL_TEXTURE_2D, level, x, y, w, h, px.format.value, px.type.value, px.offset);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage2D";
                    } else {
                        GL11.glTexSubImage2D(GL11.GL_TEXTURE_2D, level, x, y, w, h, px.format.value, px.type.value, px.data);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage2D";
                    }

                    GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
                    break;
                case TEXTURE_3D:
                    GL11.glBindTexture(GL12.GL_TEXTURE_3D, handle);

                    if (px.data == null) {
                        GL12.glTexSubImage3D(GL12.GL_TEXTURE_3D, level, x, y, z, w, h, d, px.format.value, px.type.value, px.offset);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage3D";
                    } else {
                        GL12.glTexSubImage3D(GL12.GL_TEXTURE_3D, level, x, y, z, w, h, d, px.format.value, px.type.value, px.data);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage3D";
                    }

                    GL11.glBindTexture(GL12.GL_TEXTURE_3D, 0);
                    break;
                case TEXTURE_1D_ARRAY:
                    GL11.glBindTexture(GL30.GL_TEXTURE_1D_ARRAY, handle);

                    if (px.data == null) {
                        GL11.glTexSubImage2D(GL30.GL_TEXTURE_1D_ARRAY, level, x, y, w, h, px.format.value, px.type.value, px.offset);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage2D";
                    } else {
                        GL11.glTexSubImage2D(GL30.GL_TEXTURE_1D_ARRAY, level, x, y, w, h, px.format.value, px.type.value, px.data);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage2D";
                    }

                    GL11.glBindTexture(GL30.GL_TEXTURE_1D_ARRAY, 0);
                    break;
                case TEXTURE_2D_ARRAY:
                    GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, handle);

                    if (px.data == null) {
                        GL12.glTexSubImage3D(GL30.GL_TEXTURE_2D_ARRAY, level, x, y, z, w, h, d, px.format.value, px.type.value, px.offset);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage3D";
                    } else {
                        GL12.glTexSubImage3D(GL30.GL_TEXTURE_2D_ARRAY, level, x, y, z, w, h, d, px.format.value, px.type.value, px.data);

                        assert GL11.glGetError() == GL11.GL_NO_ERROR : "TexSubImage3D";
                    }

                    GL11.glBindTexture(GL30.GL_TEXTURE_2D_ARRAY, 0);
                    break;
                default:
                    throw new UnsupportedOperationException("Unsupported Texture Target: " + tgt);
            }
        }
    }

    @Override
    public void close() {
        if (this.isValid()) {
            GL11.glDeleteTextures(handle);

            assert GL11.glGetError() == GL11.GL_NO_ERROR : "DeleteTextures";
            this.handle = 0;
            this.info = null;
        }
    }

    public static final GLTexture DEFAULT_TEXTURE1D = new GLTexture() {
        @Override
        public void bind(int unit) {
            GL13.glActiveTexture(GL13.GL_TEXTURE0 + unit);
            GL11.glBindTexture(GL11.GL_TEXTURE_1D, 0);
        }
    };

    public static final GLTexture DEFAULT_TEXTURE2D = new GLTexture() {
        @Override
        public void bind(int unit) {
            GL13.glActiveTexture(GL13.GL_TEXTURE0 + unit);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0);
        }
    };

    public static final GLTexture DEFAULT_TEXTURE3D = new GLTexture() {
        @Override
        public void bind(int unit) {
            GL13.glActiveTexture(GL13.GL_TEXTURE0 + unit);
            GL11.glBindTexture(GL12.GL_TEXTURE_3D, 0);
        }
    };

    public static final int ALL_LAYERS = -1;

    public void bindImage(final int unit, final int level, final int layer, final GLImageAccess access, final GLInternalFormat format) {
        assert this.isValid() : "GLTexture is not valid!";

        if (layer == ALL_LAYERS) {
            GL42.glBindImageTexture(unit, this.handle, level, true, 0, access.value, format.value);
        } else {
            GL42.glBindImageTexture(unit, this.handle, level, false, layer, access.value, format.value);
        }
    }
}
