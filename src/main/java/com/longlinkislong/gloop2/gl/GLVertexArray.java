/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GL45;
import org.lwjgl.opengl.GLCapabilities;

/**
 *
 * @author zmichaels
 */
public class GLVertexArray extends GLObject<GLVertexArrayInfo, GLVertexArray> {

    public GLVertexArray() {
    }

    public GLVertexArray(final GLVertexArrayInfo info) {
        this.info = info;

        final GLCapabilities caps = GL.getCapabilities();
        
        if (caps.OpenGL45) {
            this.handle = GLVertexArrayHelper.create45(info);
        } else if (caps.OpenGL43) {
            this.handle = GLVertexArrayHelper.create43(info);
        } else {
            this.handle = GLVertexArrayHelper.create30(info);
        }
    }
    
    public void bindVertexBuffer(final int binding, final GLBuffer buffer, final long offset, final int stride) {
        assert this.isValid() : "GLVertexArray is not valid!";
        
        final GLCapabilities caps = GL.getCapabilities();
        
        if (caps.OpenGL45) {
            GL45.glVertexArrayVertexBuffer(handle, binding, buffer.handle, offset, stride);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexArrayVertexBuffer";
        } else if (caps.OpenGL43) {            
            GL30.glBindVertexArray(handle);
            GL43.glBindVertexBuffer(binding, buffer.handle, offset, stride);
            GL30.glBindVertexArray(0);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindVertexBuffer";
        } else {
            throw new UnsupportedOperationException("ARB_vertex_attrib_binding is required!");
        }
    }
    
    public void bind() {
        assert this.isValid() : "GLVertexArray is not valid!";
        
        GL30.glBindVertexArray(this.handle);        
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindVertexArray";
    }

    @Override
    public void close() {
        if (this.isValid()) {            
            GL30.glDeleteVertexArrays(this.handle);            
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "DeleteVertexArrays";
            
            this.handle = 0;
        }
    }

    public static final GLVertexArray DEFAULT_VERTEX_ARRAY = new GLVertexArray() {
        @Override
        public void bind() {
            GL30.glBindVertexArray(0);            
        }
    };
}
