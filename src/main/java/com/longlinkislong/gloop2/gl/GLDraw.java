/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import java.nio.IntBuffer;
import org.lwjgl.PointerBuffer;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL32;
import org.lwjgl.opengl.GL40;
import org.lwjgl.opengl.GL42;
import org.lwjgl.opengl.GL43;

/**
 *
 * @author zmichaels
 */
public final class GLDraw {

    private GLDraw() {
    }

    public static enum Mode {
        POINTS(GL11.GL_POINTS),
        TRIANGLES(GL11.GL_TRIANGLES),
        LINES(GL11.GL_LINES),
        LINE_STRIP(GL11.GL_LINE_STRIP),
        LINE_LOOP(GL11.GL_LINE_LOOP),
        TRIANGLE_STRIP(GL11.GL_TRIANGLE_STRIP),
        TRIANGLE_FAN(GL11.GL_TRIANGLE_FAN);

        public final int value;

        private Mode(final int value) {
            this.value = value;
        }
    }

    public static enum IndexType {
        UNSIGNED_BYTE(GL11.GL_UNSIGNED_BYTE),
        UNSIGNED_SHORT(GL11.GL_UNSIGNED_SHORT),
        UNSIGNED_INT(GL11.GL_UNSIGNED_INT);

        public final int value;

        private IndexType(final int value) {
            this.value = value;
        }
    }

    public static void drawArrays(final Mode mode, final int first, final int count) {
        GL11.glDrawArrays(mode.value, first, count);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawArrays";
    }

    public static void drawArraysInstanced(final Mode mode, final int first, final int count, final int primCount) {
        GL31.glDrawArraysInstanced(mode.value, first, count, primCount);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawArraysInstanced";
    }
    
    public static void drawArraysInstancedBaseInstance(final Mode mode, final int first, final int count, final int instanceCount, final int baseInstance) {
        GL42.glDrawArraysInstancedBaseInstance(mode.value, first, count, instanceCount, baseInstance);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawArraysInstancedBaseInstance";
    }
    
    public static void drawArraysIndirect(final Mode mode, final long indirect) {
        GL40.glDrawArraysIndirect(mode.value, indirect);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawArraysIndirect";
    }
    
    public static void multiDrawArrays(final Mode mode, final IntBuffer first, final IntBuffer count) {
        assert first.remaining() == count.remaining() : "Mismatch first and count sizes!";
        
        GL14.glMultiDrawArrays(mode.value, first, count);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "MultiDrawArrays";
    }
    
    public static void drawElementsInstancedBaseInstance(final Mode mode, final int count, final IndexType type, final long indices, final int instanceCount, final int baseInstance) {
        GL42.glDrawElementsInstancedBaseInstance(mode.value, count, type.value, indices, instanceCount, baseInstance);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawElementsBaseInstance";
    }
    
    public static void multiDrawElements(final Mode mode, final IntBuffer count, final IndexType type, final PointerBuffer indices) {
        assert count.remaining() == indices.remaining() : "Mismatch count and indices sizes!";
                
        GL14.glMultiDrawElements(mode.value, count, type.value, indices);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "MultiDrawElements";
    }

    public static void drawElements(final Mode mode, final int count, final IndexType type, final long offset) {
        GL11.glDrawElements(mode.value, count, type.value, offset);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawElements";
    }

    public static void drawElementsInstanced(final Mode mode, final int count, final IndexType type, final long offset, final int primCount) {
        GL31.glDrawElementsInstanced(mode.value, count, type.value, offset, primCount);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawElementsInstanced";
    }

    public static void drawRangeElements(final Mode mode, final int start, final int end, final int count, final IndexType type, final long offset) {
        GL12.glDrawRangeElements(mode.value, start, end, count, type.value, offset);        
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawRangeElements";
    }
    
    public static void drawElementsBaseVertex(final Mode mode, final int count, final IndexType type, final long indices) {
        GL32.glDrawElementsBaseVertex(mode.value, count, type.value, indices, count);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawElementsBaseVertex";
    }
    
    public static void drawRangeElementsBaseVertex(final Mode mode, final int start, final int end, final int count, final IndexType type, final long indices, final int baseVertex) {        
        GL32.glDrawRangeElementsBaseVertex(mode.value, start, end, count, type.value, indices, baseVertex);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawRangeElementsBaseVertex";
    }
    
    public static void drawElementsInstancedBaseVertex(final Mode mode, final int count, final IndexType type, final long indices, final int instanceCount, final int baseVertex) {
        GL32.glDrawElementsInstancedBaseVertex(mode.value, count, type.value, indices, instanceCount, baseVertex);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawElementsInstancedBaseVertex";
    }
    
    public static void drawElementsInstancedBaseVertexBaseInstance(final Mode mode, final int count, final IndexType type, final long indices, final int instanceCount, final int baseVertex, final int baseInstance) {
        GL42.glDrawElementsInstancedBaseVertexBaseInstance(mode.value, count, type.value, indices, instanceCount, baseVertex, baseInstance);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawElementsInstancedBaseVertexBaseInstance";
    }
    
    public static void drawElementsIndirect(final Mode mode, final IndexType type, final long indirect) {
        GL40.glDrawElementsIndirect(mode.value, type.value, indirect);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawELementsIndirect";
    }
    
    public static void multiDrawElementsIndirect(final Mode mode, final IndexType type, final long indirect, final int drawCount, final int stride) {
        GL43.glMultiDrawElementsIndirect(mode.value, type.value, indirect, drawCount, stride);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "MultiDrawElementsIndirect";
    }
    
    public static void multiDrawArraysIndirect(final Mode mode, final long indirect, final int drawCount, final int stride) {
        GL43.glMultiDrawArraysIndirect(mode.value, indirect, drawCount, stride);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "MultiDrawArraysIndirect";
    }        

    public void drawBuffers(int... attachments) {
        GL20.glDrawBuffers(attachments);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "DrawBuffers";
    }
}
