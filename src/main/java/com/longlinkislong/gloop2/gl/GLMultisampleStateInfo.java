/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;

/**
 *
 * @author zmichaels
 */
public class GLMultisampleStateInfo {

    public final boolean alphaToCoverageEnabled;
    public final boolean sampleCoverageEnabled;
    public final float sampleCoverage;
    public boolean sampleCoverageInvert;

    public GLMultisampleStateInfo(
            final boolean alphaToCoverageEnabled,
            final boolean sampleCoverageEnabled,
            final float sampleCoverage, final boolean sampleCoverageInvert) {

        this.alphaToCoverageEnabled = alphaToCoverageEnabled;
        this.sampleCoverage = sampleCoverage;
        this.sampleCoverageEnabled = sampleCoverageEnabled;
        this.sampleCoverageInvert = sampleCoverageInvert;
    }
    
    public void apply() {
        if (alphaToCoverageEnabled) {
            GL11.glEnable(GL13.GL_SAMPLE_ALPHA_TO_COVERAGE);
        } else {
            GL11.glDisable(GL13.GL_SAMPLE_ALPHA_TO_COVERAGE);
        }
        
        if (sampleCoverageEnabled) {
            GL11.glEnable(GL13.GL_SAMPLE_COVERAGE);
        } else {
            GL11.glDisable(GL13.GL_SAMPLE_COVERAGE);
        }
        
        if (alphaToCoverageEnabled || sampleCoverageEnabled) {
            GL13.glSampleCoverage(this.sampleCoverage, this.sampleCoverageInvert);
        }
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "MultisampleStateInfo";
    }
}
