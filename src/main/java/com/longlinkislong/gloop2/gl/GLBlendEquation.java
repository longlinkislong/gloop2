/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL14;

/**
 *
 * @author zmichaels
 */
public enum GLBlendEquation implements CEnum {
    ADD(GL14.GL_FUNC_ADD),
    SUBTRACT(GL14.GL_FUNC_SUBTRACT),
    REVERSE_SUBTRACT(GL14.GL_FUNC_REVERSE_SUBTRACT),
    MIN(GL14.GL_MIN),
    MAX(GL14.GL_MAX);
    
    public final int value;
    
    private GLBlendEquation(final int value) {
        this.value = value;
    }
    
    public static Optional<GLBlendEquation> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
