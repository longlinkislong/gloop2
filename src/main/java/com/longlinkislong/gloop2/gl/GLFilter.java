/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author zmichaels
 */
public enum GLFilter implements CEnum {
    NEAREST(GL11.GL_NEAREST, true),
    LINEAR(GL11.GL_LINEAR, true),
    NEAREST_MIPMAP_NEAREST(GL11.GL_NEAREST_MIPMAP_NEAREST, false),
    LINEAR_MIPMAP_NEAREST(GL11.GL_LINEAR_MIPMAP_NEAREST, false),
    NEAREST_MIPMAP_LINEAR(GL11.GL_NEAREST_MIPMAP_LINEAR, false),
    LINEAR_MIPMAP_LINEAR(GL11.GL_LINEAR_MIPMAP_LINEAR, false);
    
    public final int value;
    public final boolean magSupported;
    
    private GLFilter(final int value, final boolean magSupported) {
        this.value = value;
        this.magSupported = magSupported;
    }
    
    public static Optional<GLFilter> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
