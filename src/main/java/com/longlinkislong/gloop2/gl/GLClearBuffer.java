/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Set;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author zmichaels
 */
public enum GLClearBuffer implements CBitfield {
    COLOR(GL11.GL_COLOR_BUFFER_BIT),
    DEPTH(GL11.GL_DEPTH_BUFFER_BIT),
    STENCIL(GL11.GL_STENCIL_BUFFER_BIT);
    
    public int value;
    
    private GLClearBuffer(final int value) {
        this.value = value;
    }
    
    public static Set<GLClearBuffer> flags(final int bits) {        
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<GLClearBuffer> flags) {
        return Tools.flagsToBitfield(flags);
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
