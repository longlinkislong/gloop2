/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

/**
 *
 * @author zmichaels
 */
public class GLFenceInfo {
    public final GLFenceCondition condition;
    public final int flags;
    
    public GLFenceInfo(final GLFenceCondition condition, final int flags) {
        this.condition = condition;
        this.flags = flags;
    }
    
    public GLFenceInfo() {
        this(GLFenceCondition.GPU_COMMANDS_COMPLETE, 0);
    }
    
    public GLFenceInfo withFlags(final int flags) {
        return new GLFenceInfo(condition, flags);
    }
    
    public GLFenceInfo withCondition(final GLFenceCondition condition) {
        return new GLFenceInfo(condition, flags);
    }
    
    public GLSync fence() {
        return GLSync.fence(this);
    }
}
