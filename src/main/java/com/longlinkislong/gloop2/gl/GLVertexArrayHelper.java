/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL43;
import org.lwjgl.opengl.GL45;

/**
 *
 * @author zmichaels
 */
final class GLVertexArrayHelper {
    private GLVertexArrayHelper() {}
    
    static int create30(final GLVertexArrayInfo info) {
        int handle = GL30.glGenVertexArrays();
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GenVertexArrays";
        
        GL30.glBindVertexArray(handle);
        
        if (info.indexBuffer != null) {
            GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, info.indexBuffer.handle);
        }
        
        for (GLVertexArrayInfo.AttributeDescription attrib : info.attributes) {                        
            final GLVertexArrayInfo.BindingDescription binding = info.bindings.stream()
                    .filter(b -> b.binding == attrib.binding)
                    .findFirst()
                    .get();
            
            GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, binding.buffer.handle);
            
            GL20.glEnableVertexAttribArray(attrib.location);
            
            final int strideAdjust = binding.stride - attrib.format.bytes;
            
            assert strideAdjust >= 0;
            
            switch (attrib.format.type) {
                case INTEGER:
                    GL30.glVertexAttribIPointer(attrib.location, attrib.format.size, attrib.format.value, strideAdjust, binding.offset);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexAttribIPointer";
                    break;
                case NORMALIZED:
                    GL20.glVertexAttribPointer(attrib.location, attrib.format.size, attrib.format.value, true, strideAdjust, binding.offset);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexAttribPointer";
                    break;
                case FLOAT:
                    GL20.glVertexAttribPointer(attrib.location, attrib.format.size, attrib.format.value, false, strideAdjust, binding.offset);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexAttribPointer";
                    break;
                default:
                    throw new UnsupportedOperationException();
            }
            
            GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        }
        
        GL30.glBindVertexArray(0);
        
        return handle;
    }
    
    static int create43(final GLVertexArrayInfo info) {
        int handle = GL30.glGenVertexArrays();
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "GenVertexArrays";
        
        GL30.glBindVertexArray(handle);
        
        if (info.indexBuffer != null) {
            GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, info.indexBuffer.handle);                        
        }
        
        for (GLVertexArrayInfo.BindingDescription binding : info.bindings) {
            GL43.glVertexBindingDivisor(binding.binding, binding.divisor);            
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexBindingDivisor";
            
            if (binding.buffer != null) {
                GL43.glBindVertexBuffer(binding.binding, binding.buffer.handle, binding.offset, binding.stride);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindVertexBuffer";
            }                        
        }
        
        for (GLVertexArrayInfo.AttributeDescription attrib : info.attributes) {
            GL20.glEnableVertexAttribArray(attrib.location);
            
            switch (attrib.format.type) {
                case NORMALIZED:
                    GL43.glVertexAttribFormat(attrib.location, attrib.format.size, attrib.format.value, true, attrib.offset);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexAttribFormat";
                    break;
                case INTEGER:
                    GL43.glVertexAttribIFormat(attrib.location, attrib.format.size, attrib.format.value, attrib.offset);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexAttribIFormat";
                    break;
                case FLOAT:
                    GL43.glVertexAttribFormat(attrib.location, attrib.format.size, attrib.format.value, false, attrib.offset);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexAttribFormat";
                    break;
                default:
                    throw new UnsupportedOperationException("Unsupported attribute format: " + attrib.format);
            } 
            
            GL43.glVertexAttribBinding(attrib.location, attrib.binding);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexAttribBinding";
        }  
        
        GL30.glBindVertexArray(0);
        
        return handle;
    }
    
    static int create45(final GLVertexArrayInfo info) {
        int handle = GL45.glCreateVertexArrays();
        
        if (info.indexBuffer != null) {
            GL45.glVertexArrayElementBuffer(handle, info.indexBuffer.handle);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexArrayElementBuffer";
        }
        
        for (GLVertexArrayInfo.BindingDescription binding : info.bindings) {
            GL45.glVertexArrayBindingDivisor(handle, binding.binding, binding.divisor);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexArrayBindingDivisor";
            
            if (binding.buffer != null) {
                GL45.glVertexArrayVertexBuffer(handle, binding.binding, binding.buffer.handle, binding.offset, binding.stride);
                
                assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexArrayVertexBuffer";
            }
        }
        
        for (GLVertexArrayInfo.AttributeDescription attrib : info.attributes) {
            GL45.glEnableVertexArrayAttrib(handle, attrib.location);            
            
            switch (attrib.format.type) {
                case NORMALIZED:
                    GL45.glVertexArrayAttribFormat(handle, attrib.location, attrib.format.size, attrib.format.value, true, attrib.offset);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexArrayAttribFormat";
                    break;
                case INTEGER:
                    GL45.glVertexArrayAttribIFormat(handle, attrib.location, attrib.format.size, attrib.format.value, attrib.offset);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexArrayAttribIFormat";
                    break;
                case FLOAT:
                    GL45.glVertexArrayAttribFormat(handle, attrib.location, attrib.format.size, attrib.format.value, false, attrib.offset);
                    
                    assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexArrayAttribFormat";
                    break;
                default:
                    throw new UnsupportedOperationException("Unsupported attribute format: " + attrib.format);
            } 
            
            GL45.glVertexArrayAttribBinding(handle, attrib.location, attrib.binding);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "VertexArrayAttribBinding";
        }  
        
        return handle;
    }
}
