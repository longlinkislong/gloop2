/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL33;
import org.lwjgl.opengl.GL45;
import org.lwjgl.opengl.EXTTextureFilterAnisotropic;

/**
 *
 * @author zmichaels
 */
public class GLSampler extends GLObject<GLSamplerInfo, GLSampler> {
    public GLSampler() {}
    
    public GLSampler(final GLSamplerInfo info) {
        this.info = info;
        
        this.handle = GL45.glCreateSamplers();
        
        GL33.glSamplerParameteri(handle, GL11.GL_TEXTURE_WRAP_S, info.wrapS.value);
        GL33.glSamplerParameteri(handle, GL11.GL_TEXTURE_WRAP_T, info.wrapT.value);
        GL33.glSamplerParameteri(handle, GL12.GL_TEXTURE_WRAP_R, info.wrapR.value);
        
        GL33.glSamplerParameteri(handle, GL11.GL_TEXTURE_MIN_FILTER, info.minFilter.value);
        GL33.glSamplerParameteri(handle, GL11.GL_TEXTURE_MAG_FILTER, info.magFilter.value);
        
        GL33.glSamplerParameterf(handle, GL12.GL_TEXTURE_MIN_LOD, info.minLOD);
        GL33.glSamplerParameterf(handle, GL12.GL_TEXTURE_MAX_LOD, info.maxLOD);
        
        if (GL.getCapabilities().GL_EXT_texture_filter_anisotropic) {
            GL33.glSamplerParameterf(handle, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, info.maxAnisotropy);
        }
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "SamplerParameter";
    }
    
    public void bind(final int unit) {
        assert this.isValid() : "GLSampler is not valid!";
        
        GL33.glBindSampler(unit, handle);
        
        assert GL11.glGetError() == GL11.GL_NO_ERROR : "BindSampler";
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GL33.glDeleteSamplers(this.handle);
            
            assert GL11.glGetError() == GL11.GL_NO_ERROR : "DeleteSamplers";
            
            this.handle = 0;
            this.info = null;
        }
    }
    
    public static final GLSampler DEFAULT_SAMPLER = new GLSampler() {
        @Override
        public void bind(int unit) {
            GL33.glBindSampler(unit, 0);                        
        }
    };
}
