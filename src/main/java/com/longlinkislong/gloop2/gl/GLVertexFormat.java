/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL33;

/**
 *
 * @author zmichaels
 */
public enum GLVertexFormat implements CEnum {
    // 8bit types    
    X8_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_BYTE, 1, 1),
    X8_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_BYTE, 1, 1),
    X8_SINT(GLVertexType.INTEGER, GL11.GL_BYTE, 1, 1),
    X8_SNORM(GLVertexType.NORMALIZED, GL11.GL_BYTE, 1, 1),
    // 16bit types    
    X16_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_SHORT, 1, 2),
    X16_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_SHORT, 1, 2),
    X16_SINT(GLVertexType.INTEGER, GL11.GL_SHORT, 1, 2),
    X16_SNORM(GLVertexType.NORMALIZED, GL11.GL_SHORT, 1, 2),
    X16_SFLOAT(GLVertexType.FLOAT, GL30.GL_HALF_FLOAT, 1, 2),
    // 32bit types    
    X32_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_INT, 1, 4),
    X32_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_INT, 1, 4),
    X32_SINT(GLVertexType.INTEGER, GL11.GL_INT, 1, 4),
    X32_SNORM(GLVertexType.NORMALIZED, GL11.GL_INT, 1, 4),
    X32_SFLOAT(GLVertexType.FLOAT, GL11.GL_FLOAT, 1, 4),
    // 8bit vec2
    X8Y8_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_BYTE, 2, 2),
    X8Y8_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_BYTE, 2, 2),
    X8Y8_SINT(GLVertexType.INTEGER, GL11.GL_BYTE, 2, 2),
    X8Y8_SNORM(GLVertexType.NORMALIZED, GL11.GL_BYTE, 2, 2),
    // 16bit vec2
    X16Y16_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_SHORT, 2, 4),
    X16Y16_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_SHORT, 2, 4),
    X16Y16_SINT(GLVertexType.INTEGER, GL11.GL_SHORT, 2, 4),
    X16Y16_SNORM(GLVertexType.NORMALIZED, GL11.GL_SHORT, 2, 4),
    X16Y16_SFLOAT(GLVertexType.FLOAT, GL30.GL_HALF_FLOAT, 2, 4),
    // 32bit vec2
    X32Y32_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_INT, 2, 8),
    X32Y32_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_INT, 2, 8),
    X32Y32_SINT(GLVertexType.INTEGER, GL11.GL_INT, 2, 8),
    X32Y32_SNORM(GLVertexType.NORMALIZED, GL11.GL_INT, 2, 8),
    X32Y32_SFLOAT(GLVertexType.FLOAT, GL11.GL_FLOAT, 2, 8),
    // 8bit vec3
    X8Y8Z8_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_BYTE, 2, 3),
    X8Y8Z8_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_BYTE, 2, 3),
    X8Y8Z8_SINT(GLVertexType.INTEGER, GL11.GL_BYTE, 2, 3),
    X8Y8Z8_SNORM(GLVertexType.NORMALIZED, GL11.GL_BYTE, 2, 3),
    // 16bit vec3
    X16Y16Z16_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_SHORT, 3, 6),
    X16Y16Z16_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_SHORT, 3, 6),
    X16Y16Z16_SINT(GLVertexType.INTEGER, GL11.GL_SHORT, 3, 6),
    X16Y16Z16_SNORM(GLVertexType.NORMALIZED, GL11.GL_SHORT, 3, 6),
    X16Y16Z16_SFLOAT(GLVertexType.FLOAT, GL30.GL_HALF_FLOAT, 3, 6),
    // 32bit vec3
    X32Y32Z32_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_INT, 3, 12),
    X32Y32Z32_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_INT, 3, 12),
    X32Y32Z32_SINT(GLVertexType.INTEGER, GL11.GL_INT, 3, 12),
    X32Y32Z32_SNORM(GLVertexType.NORMALIZED, GL11.GL_INT, 3, 12),
    X32Y32Z32_SFLOAT(GLVertexType.FLOAT, GL11.GL_FLOAT, 3, 12),
    // 8bit vec4
    X8Y8Z8W8_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_BYTE, 4, 4),
    X8Y8Z8W8_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_BYTE, 4, 4),
    X8Y8Z8W8_SINT(GLVertexType.INTEGER, GL11.GL_BYTE, 4, 4),
    X8Y8Z8W8_SNORM(GLVertexType.NORMALIZED, GL11.GL_BYTE, 4, 4),
    // 16bit vec4
    X16Y16Z16W16_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_SHORT, 4, 8),
    X16Y16Z16W16_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_SHORT, 4, 8),
    X16Y16Z16W16_SINT(GLVertexType.INTEGER, GL11.GL_SHORT, 4, 8),
    X16Y16Z16W16_SNORM(GLVertexType.NORMALIZED, GL11.GL_SHORT, 4, 8),
    X16Y16Z16W16_SFLOAT(GLVertexType.FLOAT, GL30.GL_HALF_FLOAT, 4, 8),
    // 32bit vec4
    X32Y32Z32W32_UINT(GLVertexType.INTEGER, GL11.GL_UNSIGNED_INT, 4, 16),
    X32Y32Z32W32_UNORM(GLVertexType.NORMALIZED, GL11.GL_UNSIGNED_INT, 4, 16),
    X32Y32Z32W32_SINT(GLVertexType.INTEGER, GL11.GL_INT, 4, 16),
    X32Y32Z32W32_SNORM(GLVertexType.NORMALIZED, GL11.GL_INT, 4, 16),
    X32Y32Z32W32_SFLOAT(GLVertexType.FLOAT, GL11.GL_FLOAT, 4, 16),
    // 32bit packed vec4
    W2Z10Y10X10_UINT(GLVertexType.INTEGER, GL12.GL_UNSIGNED_INT_2_10_10_10_REV, 4, 4),
    W2Z10Y10X10_UNORM(GLVertexType.NORMALIZED, GL12.GL_UNSIGNED_INT_2_10_10_10_REV, 4, 4),
    W2Z10Y10X10_SINT(GLVertexType.INTEGER, GL33.GL_INT_2_10_10_10_REV, 4, 4),
    W2Z10Y10X10_SNORM(GLVertexType.NORMALIZED, GL33.GL_INT_2_10_10_10_REV, 4, 4);

    public static final GLVertexFormat FLOAT = X32_SFLOAT;
    public static final GLVertexFormat INT = X32_SINT;
    public static final GLVertexFormat UINT = X32_UINT;

    public static final GLVertexFormat VEC2 = X32Y32_SFLOAT;
    public static final GLVertexFormat VEC3 = X32Y32Z32_SFLOAT;
    public static final GLVertexFormat VEC4 = X32Y32Z32W32_SFLOAT;

    public static final GLVertexFormat IVEC2 = X32Y32_SINT;
    public static final GLVertexFormat IVEC3 = X32Y32Z32_SINT;
    public static final GLVertexFormat IVEC4 = X32Y32Z32W32_SINT;

    public static final GLVertexFormat UIVEC2 = X32Y32_UINT;
    public static final GLVertexFormat UIVEC3 = X32Y32Z32_UINT;
    public static final GLVertexFormat UIVEC4 = X32Y32Z32W32_UINT;

    final GLVertexType type;
    public final int value;
    public final int size;
    public final int bytes;

    private GLVertexFormat(
            final GLVertexType type,
            final int value, final int size,
            final int bytes) {

        this.type = type;
        this.value = value;
        this.size = size;
        this.bytes = bytes;
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
