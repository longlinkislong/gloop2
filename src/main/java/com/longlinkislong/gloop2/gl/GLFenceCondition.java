/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengl.GL32;

/**
 *
 * @author zmichaels
 */
public enum GLFenceCondition implements CEnum {
    GPU_COMMANDS_COMPLETE(GL32.GL_SYNC_GPU_COMMANDS_COMPLETE);
    
    public final int value;
    
    private GLFenceCondition(final int value) {
        this.value = value;
    }
    
    public static Optional<GLFenceCondition> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
