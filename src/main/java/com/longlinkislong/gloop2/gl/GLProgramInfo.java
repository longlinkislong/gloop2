/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gl;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.lwjgl.opengl.GL30;

/**
 *
 * @author zmichaels
 */
public class GLProgramInfo {

    public static final class VaryingStateInfo {

        public static enum BufferMode implements CEnum {
            INTERLEAVED(GL30.GL_INTERLEAVED_ATTRIBS),
            SEPARATE(GL30.GL_SEPARATE_ATTRIBS);

            public final int value;

            private BufferMode(final int value) {
                this.value = value;
            }
            
            public static Optional<BufferMode> of(final int value) {
                return Tools.valueOf(value, Arrays.stream(values()));
            }

            @Override
            public int getValue() {
                return this.value;
            }
        }

        public final String name;
        public final BufferMode mode;

        public VaryingStateInfo(final String name, final BufferMode mode) {
            this.name = name;
            this.mode = mode;
        }

        public VaryingStateInfo() {
            this("", BufferMode.SEPARATE);
        }

        public VaryingStateInfo withName(final String name) {
            return new VaryingStateInfo(name, mode);
        }

        public VaryingStateInfo withBufferMode(final BufferMode mode) {
            return new VaryingStateInfo(name, mode);
        }
    }

    public static final class AttributeStateInfo {

        public final String name;
        public final int location;

        public AttributeStateInfo(final String name, final int index) {
            this.name = name;
            this.location = index;
        }

        public AttributeStateInfo() {
            this("", 0);
        }

        public AttributeStateInfo withName(final String name) {
            return new AttributeStateInfo(name, location);
        }

        public AttributeStateInfo withIndex(final int index) {
            return new AttributeStateInfo(name, index);
        }
    }

    public final List<GLShader> shaders;
    public final List<VaryingStateInfo> varyings;
    public final List<AttributeStateInfo> attributes;
    public final GLProgramBinaryFormat binaryFormat;
    public final ByteBuffer binary;

    public GLProgramInfo(
            final List<GLShader> shaders,
            final List<VaryingStateInfo> varyings,
            final List<AttributeStateInfo> attributes,
            final GLProgramBinaryFormat binaryFormat,
            final ByteBuffer binary) {

        this.shaders = Tools.copyList(shaders);
        this.varyings = Tools.copyList(varyings);
        this.attributes = Tools.copyList(attributes);
        this.binaryFormat = binaryFormat;
        this.binary = binary;
    }
    
    public GLProgramInfo() {
        this(null, null, null, null, null);
    }
    
    public GLProgramInfo withShaders(final List<GLShader> shaders) {
        return new GLProgramInfo(shaders, varyings, attributes, binaryFormat, binary);
    }
    
    public GLProgramInfo withShaders(final GLShader... shaders) {
        return withShaders(Arrays.asList(shaders));
    }
    
    public GLProgramInfo withVaryings(final List<VaryingStateInfo> varyings) {
        return new GLProgramInfo(shaders, varyings, attributes, binaryFormat, binary);
    }
    
    public GLProgramInfo withVaryings(final VaryingStateInfo... varyings) {
        return withVaryings(Arrays.asList(varyings));
    }
    
    public GLProgramInfo withAttributes(final List<AttributeStateInfo> attributes) {
        return new GLProgramInfo(shaders, varyings, attributes, binaryFormat, binary);
    }
    
    public GLProgramInfo withAttributes(final AttributeStateInfo... attributes) {
        return withAttributes(Arrays.asList(attributes));
    }
    
    public GLProgramInfo withBinaryFormat(final GLProgramBinaryFormat binaryFormat) {
        return new GLProgramInfo(shaders, varyings, attributes, binaryFormat, binary);
    }
    
    public GLProgramInfo withBinary(final ByteBuffer binary) {
        return new GLProgramInfo(shaders, varyings, attributes, binaryFormat, binary);
    }
    
    public GLProgram create() {
        return new GLProgram(this);
    }
}
