/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.memory;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * An interface that wraps a pointer type.
 *
 * @author zmichaels
 */
public interface Allocation {

    /**
     * Counter for times the object has been mapped.
     * @return the mapping counter.
     */
    AtomicInteger getMapCounter();
    
    /**
     * Retrieves the underlying pointer. This should return the address pointing
     * to the start of a memory block.
     *
     * @return the address of the data.
     */
    long ptr();

    /**
     * Retrieves the offset for the allocation. This is the number of bytes to
     * skip to reach the data location.
     *
     * @return
     */
    long offset();

    /**
     * Deletes the memory allocation. Any resources acquired will be returned to
     * the host machine.
     */
    void free();

    /**
     * The size of the allocation.
     * @return The number of bytes allocated.
     */
    long size();
}
