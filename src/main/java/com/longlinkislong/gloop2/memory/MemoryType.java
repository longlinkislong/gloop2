/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.memory;

/**
 * Types of Memory allocations.
 * @author zmichaels
 */
public enum MemoryType {
    FREE(false),    
    BUFFER(true),
    IMAGE_LINEAR(true),
    IMAGE_OPTIMAL(true);
    
    public static boolean conflicts(final MemoryType a, final MemoryType b) {
        if (a == FREE || b == FREE) {
            return true;
        }
        
        switch (a) {                        
            case BUFFER:
                return b == IMAGE_OPTIMAL;            
            case IMAGE_LINEAR:
                return b == IMAGE_OPTIMAL;
            case IMAGE_OPTIMAL:
                return b != IMAGE_OPTIMAL;
            default:
                return true;
        }
    }        
    
    /**
     * True if the type can contain data.
     */
    public final boolean isValidType;
    
    private MemoryType(final boolean isValid) {
        this.isValidType = isValid;
    }
}
