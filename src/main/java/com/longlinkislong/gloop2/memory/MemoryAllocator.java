/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.memory;

/**
 * An interface that implements memory allocation operations.
 *
 * @author zmichaels
 */
public interface MemoryAllocator {

    /**
     * Allocates a block of memory.
     *
     * @param size the number of bytes to allocate.
     * @return the new address.
     */
    long malloc(long size);

    /**
     * Frees an allocation. It is only valid to free blocks of memory allocated
     * byte the same MemoryAllocator's malloc function.
     *
     * @param ptr the pointer to free.
     */
    void free(long ptr);
}
