/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.memory;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A chunk of memory allocated that may be sub divided.
 *
 * @author zmichaels
 */
public class MemoryBlock {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemoryBlock.class);
    private static final long DEFAULT_PAGE_SIZE = Long.getLong("com.longlinkislong.gloop2.memory.default_page_size", 1L);
    private static final long DEFAULT_MIN_ALLOCATION_SIZE = Long.getLong("com.longlinkislong.gloop2.memory.default_min_allocation_size", 16L);

    private final AtomicInteger mapCounter = new AtomicInteger(0);
    private final long smallestSubAlloc;
    private final long address;
    private final long size;
    private final long pageSize;
    private SubBlock blockChain;

    /**
     * Asserts that all of the sub-allocations do not alias.
     *
     * @throws AssertionError if one or more sub-allocation aliases its
     * neighbor.
     */
    public void sanityCheck() {
        for (SubBlock it = this.blockChain; it != null; it = it.next) {
            final long blockEnd = it.start + it.size;

            if (it.type == MemoryType.FREE) {
                continue;
            }

            if (it.next != null && it.next.type != MemoryType.FREE) {
                if (it.next.start < blockEnd) {
                    throw new AssertionError("Sanity check failed!");
                }
            }
        }
    }

    /**
     * Retrieves the base address of the MemoryBank. All sub-allocations exist
     * as offsets from the base address.
     *
     * @return the base address.
     */
    public long address() {
        return this.address;
    }

    /**
     * The size of the memory bank, in bytes.
     *
     * @return the size of the memory bank.
     */
    public long size() {
        return this.size;
    }

    /**
     * Retrieves the number of active sub-allocations. A sub-allocation is
     * determined active if its MemoryType is not [code]MemoryType.FREE[/code].
     *
     * @return the sum of active sub-allocations.
     */
    public int activeAllocations() {
        int sum = 0;

        for (SubBlock it = this.blockChain; it != null; it = it.next) {
            if (it.type != MemoryType.FREE) {
                sum++;
            }
        }

        return sum;
    }

    /**
     * Retrieves the size of the largest Free chunk.
     *
     * @return The size of the largest free chunk. Returns 0 if there are no
     * available chunks.
     */
    public long getLargestFreeChunkSize() {
        return Optional.ofNullable(this.findLargestFreeBlock()).map(block -> block.size).orElse(0L);
    }

    @Override
    public String toString() {
        return getStats();
    }

    /**
     * Retrieves the sub-allocation information formatted as JSON. The unit
     * scaling used is based on the total size of the MemoryBank.
     *
     * @return the sub-allocation information.
     */
    public String getStats() {
        if (this.size > (1L << 30)) {
            return getStats(MemoryScale.GB);
        } else if (this.size > (1L << 20)) {
            return getStats(MemoryScale.MB);
        } else if (this.size > (1L << 10)) {
            return getStats(MemoryScale.KB);
        } else {
            return getStats(MemoryScale.B);
        }
    }

    /**
     * Retrieves the sub-allocation information formatted as JSON. The sizes are
     * scaled to the supplied MemoryScale.
     *
     * @param scale the unit to scale all allocations to.
     * @return the sub-allocation information.
     */
    public String getStats(final MemoryScale scale) {
        final long totalUsed = this.totalUsedMemory();
        final double percentUsed = (double) totalUsed / (double) this.size * 100.0;
        final long largestFreeChunk = this.getLargestFreeChunkSize();

        return new StringBuilder(8192)
                .append("{\n")
                .append(String.format("\t\"address\" : \"0x%x\"", this.address))
                .append(",\n\t\"pageSize\" : ")
                .append(pageSize > 1L ? scale.applyUnits(pageSize) : "\"N/A\"")
                .append(",\n\t\"blockSize\" : ")
                .append(scale.applyUnits(size))
                .append(",\n\t\"totalUsed\" : ")
                .append(scale.applyUnits(totalUsed))
                .append(String.format("\n\t\"percentUsed\" : %.2f%%", percentUsed))
                .append(",\n\t\"largestFreeChunk\" : ")
                .append(scale.applyUnits(largestFreeChunk))
                .append(",\n\t\"activeAllocations\" : ")
                .append(this.activeAllocations())
                .append("\n}")
                .toString();
    }

    /**
     * Retrieves the total number of bytes in use.
     *
     * @return the total number of bytes.
     */
    public long totalUsedMemory() {
        long sum = 0L;

        for (SubBlock it = this.blockChain; it != null; it = it.next) {
            if (it.type != MemoryType.FREE) {
                sum += it.size;
            }
        }

        return sum;
    }

    /**
     * Constructs a new MemoryBlock. The default values for page size and
     * minimum sub-allocation size will be used.
     *
     * @param address the base address for the MemoryBlock.
     * @param size the size of the chunk, in bytes.
     */
    public MemoryBlock(final long address, final long size) {
        this(address, size, DEFAULT_PAGE_SIZE, DEFAULT_MIN_ALLOCATION_SIZE);
    }

    /**
     * Constructs a new MemoryBlock. The default values for minimum
     * sub-allocation size will be used.
     *
     * @param address the base address for the MemoryBlock.
     * @param size the size of the memory chunk, in bytes.
     * @param pageSize the size used for page-aligned object boundaries, in
     * bytes.
     */
    public MemoryBlock(final long address, final long size, final long pageSize) {
        this(address, size, pageSize, DEFAULT_MIN_ALLOCATION_SIZE);
    }

    /**
     * Constructs a new MemoryBlock.
     *
     * @param address the base address for the MemoryBlock.
     * @param size the size of the memory chunk, in bytes.
     * @param pageSize the size used for page-aligned object boundaries, in
     * bytes.
     * @param smallestSubAlloc the size constraint used for limiting
     * sub-allocation slicing.
     */
    public MemoryBlock(final long address, final long size, final long pageSize, final long smallestSubAlloc) {
        this.address = address;
        this.size = size;
        this.pageSize = pageSize;
        this.blockChain = new SubBlock();
        this.blockChain.start = address;
        this.blockChain.offset = 0L;
        this.blockChain.size = size;
        this.smallestSubAlloc = smallestSubAlloc;
    }

    private SubBlock findLargestFreeBlock() {
        SubBlock largest = null;

        for (SubBlock it = this.blockChain; it != null; it = it.next) {
            if (it.type != MemoryType.FREE) {
                continue;
            }

            if (largest == null || it.size > largest.size) {
                largest = it;
            }
        }

        return largest;
    }

    /**
     * Performs a sub-allocation.
     *
     * @param type the type of memory to allocate.
     * @param allocSize the requested size of allocation.
     * @param allocAlign any alignment needed for the allocation.
     * @return the sub allocation. Returns null if MemoryType is free or
     * allocSize is 0.
     * @throws OutOfMemoryError if there is no free SubBlock.
     */
    public Allocation allocate(final MemoryType type, final long allocSize, final long allocAlign) {
        if (allocSize <= 0L || type == MemoryType.FREE) {
            return null;
        }

        final SubBlock block = this.findLargestFreeBlock();

        if (block == null) {
            throw new OutOfMemoryError("The MemoryBlock is completely full!");
        }

        block.allocate(type, allocSize, allocAlign);

        return block;
    }

    final class SubBlock implements Allocation {

        @Override
        public String toString() {
            if (this.type == MemoryType.FREE) {
                return "type: free";
            } else {
                return String.format("type: %s ptr: %d (%d + %d)", this.type, this.ptr(), this.start, this.offset);
            }
        }

        @Override
        public void free() {
            if (this.type != MemoryType.FREE) {
                this.type = MemoryType.FREE;

                if (this.prev != null) {
                    this.prev.next = this.next;

                    if (this.prev.type == MemoryType.FREE) {
                        this.prev.size += this.size;
                    }
                }

                if (this.next != null) {
                    this.next = this.prev;
                }
            }
        }

        @Override
        public long size() {
            return this.size;
        }

        @Override
        public long ptr() {
            return MemoryBlock.this.address;
        }

        @Override
        public long offset() {
            return this.start + this.offset - ptr();
        }

        private long start;
        private long offset;
        private long size;
        private MemoryType type = MemoryType.FREE;
        private SubBlock next = null;
        private SubBlock prev = null;

        private void allocate(final MemoryType type, final long allocSize, final long allocAlign) {
            if (allocSize > this.size) {
                throw new OutOfMemoryError("Allocation too large for SubBlock!");
            }

            long allocStart = this.start;
            long allocEnd = this.start + allocSize;
            long blockEnd = this.start + this.size;

            if (this.prev != null) {
                final long allocOff = Util.alignUp(allocStart - address, allocAlign);

                allocStart = address + allocOff;
                allocEnd = allocStart + allocSize;

                if (allocEnd > blockEnd) {
                    throw new OutOfMemoryError("SubBlock cannot fit allocation after alignment adjustment!");
                }

                // check if this type cannot exist on the same page as the previous memory type.
                if (pageSize > 1L && MemoryType.conflicts(this.prev.type, type)) {
                    assert prev.type != MemoryType.FREE : "Previous buffer is free!";

                    if (Util.blocksOnSamePage(this.prev.start, this.prev.size, allocStart, pageSize)) {
                        final long realign = Util.alignUp(allocStart - address, pageSize);

                        allocStart = address + realign;
                        allocEnd = allocStart + allocSize;

                        if (allocEnd > blockEnd) {
                            throw new OutOfMemoryError("SubBlock cannot fit allocation after alignment adjust (linear granularity confliction)");
                        }
                    }
                }
            }

            final long remaining = blockEnd - allocEnd;

            // split the block if the remaining size can fit another allocation.
            if (remaining >= smallestSubAlloc) {
                final SubBlock subAlloc = new SubBlock();

                subAlloc.start = allocEnd;
                subAlloc.size = remaining;
                subAlloc.prev = this;

                if (this.next != null) {
                    this.next.prev = subAlloc;
                    subAlloc.next = this.next;
                }

                this.next = subAlloc;
                this.size = allocEnd - this.start;
            }

            this.offset = allocStart - this.start;
            this.type = type;
        }

        @Override
        public AtomicInteger getMapCounter() {
            return MemoryBlock.this.mapCounter;
        }
    }
}
