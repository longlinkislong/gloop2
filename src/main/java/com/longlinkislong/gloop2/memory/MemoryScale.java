/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.memory;

/**
 * Scaling used for formatting Memory stats.
 *
 * @author zmichaels
 */
public enum MemoryScale {
    /**
     * Scale bytes to bytes. No operations are done.
     */
    B {
        @Override
        public double scale(final long bytes) {
            return bytes;
        }
    },
    /**
     * Scales bytes to kilobytes. Divides byte 1024.
     */
    KB {
        @Override
        public double scale(final long bytes) {
            return bytes / 1024.0;
        }
    },
    /**
     * Scale bytes to megabytes. Divides bytes by 1024 * 1024.
     */
    MB {
        @Override
        public double scale(final long bytes) {
            return bytes / 1024.0 / 1024.0;
        }
    },
    /**
     * Scale bytes to gigabytes. Divides bytes byte 1024 * 1024 * 1024.
     */
    GB {
        @Override
        public double scale(final long bytes) {
            return bytes / 1024.0 / 1024.0 / 1024.0;
        }
    };

    /**
     * Scales bytes to a different unit format.
     *
     * @param bytes the number of bytes.
     * @return the number of scaled units.
     */
    public abstract double scale(final long bytes);

    /**
     * Performs the byte scaling and applies the appropriate unit symbol.
     * @param bytes the number of bytes
     * @return the scaled format with unit symbol.
     */
    public String applyUnits(final long bytes) {
        return String.format("%.2f%s", scale(bytes), this.name());
    }
}
