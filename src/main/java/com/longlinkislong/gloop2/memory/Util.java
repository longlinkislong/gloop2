/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.memory;

final class Util {
    static long alignUp(final long a, final long b) {        
        return (a + b - 1) / b * b;
    }

    static boolean blocksOnSamePage(final long resourceAOffset, final long resourceASize, final long resourceBOffset, final long pageSize) {        
        assert resourceAOffset + resourceASize <= resourceBOffset : String.format("Resource A (%d) appears after Resource B (%d)", resourceAOffset + resourceASize, resourceBOffset);
        assert resourceASize > 0L : "Resource A does not define a size!";
        assert pageSize > 0L : "Page size is not valid!";

        final long resourceAEnd = resourceAOffset + resourceASize - 1L;
        final long resourceAEndPage = resourceAEnd & ~(pageSize - 1L);
        final long resourceBStart = resourceBOffset;
        final long resourceBStartPage = resourceBStart & ~(pageSize - 1L);

        return resourceAEndPage == resourceBStartPage;
    }
}
