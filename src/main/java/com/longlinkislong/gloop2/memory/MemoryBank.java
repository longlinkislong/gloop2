/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.memory;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A collection of MemoryBlocks that share a common memory allocator. New
 * MemoryBlock objects are allocated if no empty MemoryBlocks exist.
 *
 * @author zmichaels
 */
public class MemoryBank {
    private static final Logger LOGGER = LoggerFactory.getLogger(MemoryBank.class);

    private final Deque<MemoryBlock> blocks = new ArrayDeque<>();
    private final long pageSize;
    private final long minAllocSize;
    private final MemoryAllocator allocator;
    private final long preferredBlockSize;

    private final class SelfAllocation implements Allocation {

        private long address;
        private long size;
        private final AtomicInteger mapCounter = new AtomicInteger(0);

        private SelfAllocation(final long address, final long size) {
            this.address = address;
            this.size = size;
        }

        @Override
        public long size() {
            return this.size;
        }

        @Override
        public long ptr() {
            return this.address;
        }

        @Override
        public long offset() {
            return 0L;
        }

        @Override
        public void free() {
            if (this.address != 0L) {
                allocator.free(this.address);
                this.address = 0L;
                this.size = 0L;
            }
        }

        @Override
        public AtomicInteger getMapCounter() {
            return this.mapCounter;
        }

    }

    /**
     * Constructs a new MemoryBank object.
     *
     * @param allocator the memory allocator to use.
     * @param preferredBlockSize the preferred size of a MemoryBlock. This
     * dictates chunk allocations and self-contained allocations.
     * @param pageSize the page size. Some MemoryTypes must be page aligned
     * @param minAllocSize the smallest size allowed for sub-allocation. Sizes
     * smaller will be rounded up to this size.
     */
    public MemoryBank(final MemoryAllocator allocator, final long preferredBlockSize, final long pageSize, final long minAllocSize) {
        this.allocator = allocator;
        this.preferredBlockSize = preferredBlockSize;
        this.pageSize = pageSize;
        this.minAllocSize = minAllocSize;
    }

    /**
     * Allocates a chunk of data from a block chain of a compatible MemoryType.
     *
     * @param type the MemoryType to allocate. Must be either a buffer or image
     * type.
     * @param allocSize the size of the allocation, in bytes.
     * @param allocAlign the alignment of the allocation, in bytes.
     * @return the allocation.
     */
    public Allocation allocate(final MemoryType type, final long allocSize, final long allocAlign) {               
        if (allocSize <= 0L) {
            return null;
        } else if (allocSize > preferredBlockSize / 2) {
            // this might not be necessary
            
            final long alignedSize = Util.alignUp(allocSize, allocAlign);
            final long ptr = this.allocator.malloc(alignedSize);            
            
            return new SelfAllocation(ptr, alignedSize);
        }

        final Optional<MemoryBlock> allocBlock = blocks.stream()
                .filter(block -> block.getLargestFreeChunkSize() >= allocSize)
                .findFirst();

        if (allocBlock.isPresent()) {
            try {                
                return allocBlock.get().allocate(type, allocSize, allocAlign);
            } catch (OutOfMemoryError err) {
                // try harder; fall through and allocate new block.
            }
        }
        
        final long rawBlock = this.allocator.malloc(this.preferredBlockSize);
        final MemoryBlock newBlock = new MemoryBlock(rawBlock, this.preferredBlockSize, this.pageSize, this.minAllocSize);

        this.blocks.addFirst(newBlock);        
        
        return newBlock.allocate(type, allocSize, allocAlign);
    }

    /**
     * Frees all data allocated by the MemoryBank.
     */
    public void free() {
        while (!this.blocks.isEmpty()) {
            this.allocator.free(this.blocks.pollLast().address());
        }

        this.blocks.clear();
    }
}
