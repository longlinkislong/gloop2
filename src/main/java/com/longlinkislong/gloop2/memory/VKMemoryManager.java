/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.memory;

import com.longlinkislong.gloop2.Tools;
import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.util.Arrays;
import java.util.Objects;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkMemoryAllocateInfo;
import org.lwjgl.vulkan.VkMemoryRequirements;
import org.lwjgl.vulkan.VkPhysicalDevice;
import org.lwjgl.vulkan.VkPhysicalDeviceMemoryProperties;
import org.lwjgl.vulkan.VkPhysicalDeviceProperties;

/**
 * A Memory Manager object for use in Vulkan.
 *
 * @author zmichaels
 */
public class VKMemoryManager {

    private static final long DEFAULT_SMALL_HEAP_SIZE = Long.getLong("com.longlinkislong.gloop2.memory.default_small_heap_size", 512 * 1024 * 1024);
    private static final long DEFAULT_MIN_ALLOCATION_SIZE = Long.getLong("com.longlinkislong.gloop2.memory.default_min_allocation_size", 16);

    private final MemoryBank[] memoryBanks;
    private final int[] heapFlags;
    private final int[] typeProperties;

    public VKMemoryManager(
            final VkPhysicalDevice physicalDevice,
            final VkPhysicalDeviceProperties devProps,
            final VkDevice device,
            final long preferredSmallSize,
            final long preferredLargeSize) {

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkPhysicalDeviceMemoryProperties props = VkPhysicalDeviceMemoryProperties.callocStack(mem);
            final long pageSize = devProps.limits().bufferImageGranularity();

            VK10.vkGetPhysicalDeviceMemoryProperties(physicalDevice, props);

            memoryBanks = new MemoryBank[props.memoryTypeCount()];
            heapFlags = new int[props.memoryHeapCount()];
            typeProperties = new int[props.memoryTypeCount()];

            for (int i = 0; i < heapFlags.length; i++) {
                heapFlags[i] = props.memoryHeaps(i).flags();
            }

            for (int i = 0; i < typeProperties.length; i++) {
                typeProperties[i] = props.memoryTypes(i).propertyFlags();

                final long heapSize = props.memoryHeaps(i).size();
                final long preferredBlockSize = heapSize < DEFAULT_SMALL_HEAP_SIZE ? preferredSmallSize : preferredLargeSize;
                final int typeIndex = i;

                memoryBanks[typeIndex] = new MemoryBank(new MemoryAllocator() {
                    @Override
                    public long malloc(long size) {
                        try (MemoryStack mem = MemoryStack.stackPush()) {
                            final VkMemoryAllocateInfo info = VkMemoryAllocateInfo.callocStack(mem)
                                    .sType(VK10.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
                                    .allocationSize(size)
                                    .memoryTypeIndex(typeIndex);

                            final LongBuffer pMemory = mem.callocLong(1);
                            final int err = VK10.vkAllocateMemory(device, info, null, pMemory);

                            assert err == VK10.VK_SUCCESS : Tools.vkerr(err);

                            return pMemory.get(0);
                        }
                    }

                    @Override
                    public void free(long ptr) {
                        VK10.vkFreeMemory(device, ptr, null);
                    }
                }, preferredBlockSize, pageSize, DEFAULT_MIN_ALLOCATION_SIZE);
            }
        }
    }

    private MemoryBank findCompatibleMemoryBank(final int reqTypeBits, final int props) {
        int bits = reqTypeBits;

        for (int i = 0; i < this.typeProperties.length; i++) {
            if ((bits & 1) == 1) {
                if ((this.typeProperties[i] & props) == props) {
                    return this.memoryBanks[i];
                }
            }

            bits >>= 1;
        }

        return null;
    }

    /**
     * Allocates a buffer. Buffers are linearly stored on both Vulkan device and
     * local device.
     *
     * @param memReqs the Vulkan memory requirements structure.
     * @param props additional properties needed for allocation.
     * @return the Allocation.
     * @throws OutOfMemoryError if no valid block can be
     * allocated/sub-allocated.
     */
    public Allocation allocateBuffer(final VkMemoryRequirements memReqs, final int props) {
        return findCompatibleMemoryBank(memReqs.memoryTypeBits(), props)
                .allocate(MemoryType.BUFFER, memReqs.size(), memReqs.alignment());
    }

    /**
     * Allocates an optimal image. Optimal images are optimized for GPU
     * read/write operations. The format is possibly incomprehensible to the
     * local device.
     *
     * @param memReqs the Vulkan memory requirements structure.
     * @param props additional properties needed for allocation.
     * @return the Allocation.
     * @throws OutOfMemoryError if no valid block can be
     * allocated/sub-allocated.
     */
    public Allocation allocateImage(final VkMemoryRequirements memReqs, final int props) {
        return findCompatibleMemoryBank(memReqs.memoryTypeBits(), props)
                .allocate(MemoryType.IMAGE_OPTIMAL, memReqs.size(), memReqs.alignment());
    }

    /**
     * Allocates a linear image. Linear images are optimized for read/write on
     * the local device. Most Vulkan devices are not optimized for working with
     * linear images.
     *
     * @param memReqs the Vulkan memory requirements structure.
     * @param props additional properties needed for allocation.
     * @return the Allocation.
     * @throws OutOfMemoryError if no valid block can be
     * allocated/sub-allocated.
     */
    public Allocation allocateLinearImage(final VkMemoryRequirements memReqs, final int props) {
        return findCompatibleMemoryBank(memReqs.memoryTypeBits(), props)
                .allocate(MemoryType.IMAGE_LINEAR, memReqs.size(), memReqs.alignment());
    }

    /**
     * Helper function that maps an Allocation. The result is stored within the
     * PointerBuffer.
     *
     * @param device the Vulkan Device
     * @param alloc the Allocation.
     * @param offset offset from the Allocation
     * @param size size of the Allocation
     * @param ppData the PointerBuffer to store the allocation in.
     * @throws AssertionError if the result code is not VK_SUCCESS.
     */
    public static void map(final VkDevice device, final Allocation alloc, final PointerBuffer ppData, final long offset, final long size) {
        //TODO: Allocation should remember the host ptr and the entire memory slab should be mapped.
        alloc.getMapCounter().getAndIncrement();

        final int err = VK10.vkMapMemory(device, alloc.ptr(), alloc.offset() + offset, size, 0, ppData);

        assert err == VK10.VK_SUCCESS : Tools.vkerr(err);
    }

    /**
     * Helper function that unmaps an Allocation.
     *
     * @param device the Vulkan Device object.
     * @param alloc the Allocation to unmap.
     */
    public static void unmap(final VkDevice device, final Allocation alloc) {
        if (alloc.getMapCounter().decrementAndGet() == 0) {
            VK10.vkUnmapMemory(device, alloc.ptr());
        }
    }

    /**
     * Helper function that maps an Allocation and retrieves the associated
     * ByteBuffer. Shorthand for [code]map(device, alloc, 0L,
     * alloc.size())[/code]
     *
     * @param device the Vulkan Device object.
     * @param alloc the allocation
     * @return the local device visible memory block.
     */
    public static ByteBuffer map(final VkDevice device, final Allocation alloc) {
        return map(device, alloc, 0L, alloc.size());
    }

    /**
     * Helper function that maps an Allocation and retrieves the associated
     * ByteBuffer.
     *
     * @param device the Vulkan Device object
     * @param alloc the Allocation to map.
     * @param offset the offset from allocation
     * @param size the size of the mapping
     * @return the local device visible memory block.
     */
    public static ByteBuffer map(final VkDevice device, final Allocation alloc, final long offset, final long size) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer ppData = mem.callocPointer(1);

            map(device, alloc, ppData, offset, size);

            return ppData.getByteBuffer((int) alloc.size());
        }
    }

    public boolean isValid() {
        return Arrays.stream(this.memoryBanks)
                .filter(Objects::nonNull)
                .count() > 0;
    }

    public void free() {
        Arrays.stream(this.memoryBanks)
                .filter(Objects::nonNull)
                .forEach(MemoryBank::free);

        Arrays.fill(this.memoryBanks, null);
    }
}
