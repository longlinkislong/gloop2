/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public class VKDescriptorBufferInfo {

    public final VKBuffer buffer;
    public final long offset;
    public final long range;

    public VKDescriptorBufferInfo(
            final VKBuffer buffer,
            final long offset,
            final long range) {

        this.buffer = buffer;
        this.offset = offset;
        this.range = range;
    }
    
    public VKDescriptorBufferInfo() {
        this(null, 0L, 0L);
    }
    
    public VKDescriptorBufferInfo withBuffer(final VKBuffer buffer) {
        return new VKDescriptorBufferInfo(buffer, offset, range);
    }
    
    public VKDescriptorBufferInfo withOffset(final long offset) {
        return new VKDescriptorBufferInfo(buffer, offset, range);
    }
    
    public VKDescriptorBufferInfo withRange(final long range) {
        return new VKDescriptorBufferInfo(buffer, offset, range);
    }
    
    public VKDescriptorBufferInfo withRangeWholeSize() {
        return withRange(VK10.VK_WHOLE_SIZE);
    }
}
