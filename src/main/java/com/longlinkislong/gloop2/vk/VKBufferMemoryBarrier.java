/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public class VKBufferMemoryBarrier {

    public final Set<VKAccess> srcAccessMask;
    public final Set<VKAccess> dstAccessMask;
    public final VKQueueFamily srcQueueFamily;
    public final VKQueueFamily dstQueueFamily;
    public final VKBuffer buffer;
    public final long offset;
    public final long size;

    public VKBufferMemoryBarrier(
            final Collection<VKAccess> srcAccessMask, final Collection<VKAccess> dstAccessMask,
            final VKQueueFamily srcQueueFamily, final VKQueueFamily dstQueueFamily,
            final VKBuffer buffer, final long offset, final long size) {

        this.srcAccessMask = Tools.copySet(srcAccessMask);        
        this.dstAccessMask = Tools.copySet(dstAccessMask);        
        this.srcQueueFamily = srcQueueFamily;
        this.dstQueueFamily = dstQueueFamily;
        this.buffer = buffer;
        this.offset = offset;
        this.size = size;
    }
    
    public VKBufferMemoryBarrier() {
        this(null, null, null, null, null, 0L, 0L);
    }
    
    public VKBufferMemoryBarrier withSrcAccessMask(final Collection<VKAccess> srcAccessMask) {
        return new VKBufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }
    
    public VKBufferMemoryBarrier withSrcAccessMask(final VKAccess... srcAccessMask) {
        return withSrcAccessMask(Arrays.asList(srcAccessMask));
    }
    
    public VKBufferMemoryBarrier withDstAccessMask(final Collection<VKAccess> dstAccessMask) {
        return new VKBufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }
    
    public VKBufferMemoryBarrier withDstAccessMask(final VKAccess... dstAccessMask) {
        return withDstAccessMask(Arrays.asList(dstAccessMask));
    }
    
    public VKBufferMemoryBarrier withSrcQueueFamily(final VKQueueFamily srcQueueFamily) {
        return new VKBufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }
    
    public VKBufferMemoryBarrier withDstQueueFamily(final VKQueueFamily dstQueueFamily) {
        return new VKBufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }
    
    public VKBufferMemoryBarrier withBuffer(final VKBuffer buffer) {
        return new VKBufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }
    
    public VKBufferMemoryBarrier withOffset(final long offset) {
        return new VKBufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }
    
    public VKBufferMemoryBarrier withSize(final long size) {
        return new VKBufferMemoryBarrier(srcAccessMask, dstAccessMask, srcQueueFamily, dstQueueFamily, buffer, offset, size);
    }
    
    public VKBufferMemoryBarrier withWholeSize() {
        return withSize(VK10.VK_WHOLE_SIZE);
    }
}
