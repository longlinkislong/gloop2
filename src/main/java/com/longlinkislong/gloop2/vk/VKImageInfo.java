/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public class VKImageInfo {

    public static enum CreateFlag implements CBitfield {
        SPARSE_BINDING(VK10.VK_IMAGE_CREATE_SPARSE_BINDING_BIT),
        SPARSE_RESIDENCY(VK10.VK_IMAGE_CREATE_SPARSE_RESIDENCY_BIT),
        SPARSE_ALIASED(VK10.VK_IMAGE_CREATE_SPARSE_ALIASED_BIT),
        MUTABLE_FORMAT(VK10.VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT),
        CUBE_COMPATIBLE(VK10.VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT);

        public final int value;
        
        @Override
        public int getValue() {
            return this.value;
        }

        private CreateFlag(final int value) {
            this.value = value;
        }

        public static Set<CreateFlag> flags(final int bits) {
            return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
        }

        public static int bits(final Set<CreateFlag> flags) {
            return Tools.flagsToBitfield(flags);
        }
    }

    public final Set<CreateFlag> flags;
    public final VKImageType imageType;
    public final VKFormat format;
    public final int width;
    public final int height;
    public final int depth;
    public final int mipLevels;
    public final int arrayLayers;
    public final int samples;
    public final VKImageTiling tiling;
    public final Set<VKImageUsage> usage;
    public final VKSharingMode sharingMode;
    public final List<VKQueueFamily> queueFamilies;
    public final VKImageLayout initialLayout;

    public VKImageInfo(
            final Collection<CreateFlag> flags,
            final VKImageType imageType,
            final VKFormat format,
            final int width, final int height, final int depth,
            final int mipLevels, final int arrayLayers,
            final int samples,
            final VKImageTiling tiling,
            final Collection<VKImageUsage> usage,
            final VKSharingMode sharingMode,
            final List<VKQueueFamily> queueFamilies,
            final VKImageLayout initialLayout) {

        this.flags = Tools.copySet(flags);
        this.imageType = imageType;
        this.format = format;
        this.width = width;
        this.height = height;
        this.depth =depth;
        this.mipLevels = mipLevels;
        this.arrayLayers = arrayLayers;
        this.samples = samples;
        this.tiling = tiling;        
        this.usage = Tools.copySet(usage);        
        this.sharingMode = sharingMode;        
        this.queueFamilies = Tools.copyList(queueFamilies);
        this.initialLayout = initialLayout;
    }
    
    public VKImageInfo() {
        this(null, VKImageType.IMAGE_1D, VKFormat.B8G8R8A8_UNORM,
                0, 0, 0,
                1, 1,
                VK10.VK_SAMPLE_COUNT_1_BIT,
                VKImageTiling.OPTIMAL,
                null,
                VKSharingMode.EXCLUSIVE,
                null,
                VKImageLayout.PREINITIALIZED);
    }
    
    public VKImageInfo withFlags(final Collection<CreateFlag> flags) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withFlags(final CreateFlag... flags) {
        return withFlags(Arrays.asList(flags));
    }
    
    public VKImageInfo withImageType(final VKImageType imageType) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withFormat(final VKFormat format) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withWidth(final int width) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withHeight(final int height) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withDepth(final int depth) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withExtent(final int width, final int height, final int depth) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withMipLevels(final int mipLevels) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withArrayLayers(final int arrayLayers) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withSamples(final int samples) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withTiling(final VKImageTiling tiling) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withUsage(final Collection<VKImageUsage> usage) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withUsage(final VKImageUsage... usage) {
        return withUsage(Arrays.asList(usage));
    }
    
    public VKImageInfo withSharingMode(final VKSharingMode sharingMode) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withQueueFamilies(final List<VKQueueFamily> queueFamilies) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImageInfo withQueueFamilies(final VKQueueFamily... queueFamilies) {
        return withQueueFamilies(Arrays.asList(queueFamilies));
    }
    
    public VKImageInfo withInitialLayout(final VKImageLayout initialLayout) {
        return new VKImageInfo(flags, imageType, format, width, height, depth, mipLevels, arrayLayers, samples, tiling, usage, sharingMode, queueFamilies, initialLayout);
    }
    
    public VKImage create(final VKContext ctx) {
        return new VKImage(ctx, this);
    }
    
    public VKManagedImage createManaged(final VKContext ctx, final Set<VKMemoryProperty> memoryProperties) {
        return new VKManagedImage(ctx, this, memoryProperties);
    }
}
