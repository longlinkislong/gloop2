/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class VKClearAttachment {

    public final Set<VKImageAspect> aspectMask;
    public final int colorAttachment;
    public final ByteBuffer clearValue;

    public VKClearAttachment(
            final Collection<VKImageAspect> aspectMask,
            final int colorAttachment,
            final ByteBuffer clearValue) {

        this.aspectMask = Tools.copySet(aspectMask);
        this.colorAttachment = colorAttachment;
        this.clearValue = clearValue;
    }
    
    public VKClearAttachment() {
        this(null, 0, ByteBuffer.allocate(Float.BYTES * 4));
    }
    
    public VKClearAttachment withAspectMask(final Collection<VKImageAspect> aspectMask) {
        return new VKClearAttachment(aspectMask, colorAttachment, clearValue);
    }
    
    public VKClearAttachment withAspectMask(final VKImageAspect... aspectMask) {
        return withAspectMask(Arrays.asList(aspectMask));
    }
    
    public VKClearAttachment withColorAttachment(final int colorAttachment) {
        return new VKClearAttachment(aspectMask, colorAttachment, clearValue);
    }
    
    public VKClearAttachment withClearValue(final ByteBuffer clearValue) {
        return new VKClearAttachment(aspectMask, colorAttachment, clearValue);
    }
}
