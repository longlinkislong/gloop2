/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import java.util.EnumSet;

final class VKBackBuffer implements VKObject {   
    int imageIndex;
    
    final VKSemaphore acquireSemaphore;
    final VKSemaphore renderSemaphore;
    final VKFence presentFence;    
    
    VKBackBuffer(final VKContext ctx) {
        this.acquireSemaphore = new VKSemaphore(ctx);
        this.renderSemaphore = new VKSemaphore(ctx);
        this.presentFence = new VKFence(ctx, EnumSet.of(VKFence.CreateFlag.CREATE_SIGNALED_BIT));
    }
    
    @Override
    public boolean isValid() {
        return this.acquireSemaphore.isValid() && this.renderSemaphore.isValid() && this.presentFence.isValid();
    }
    
    @Override
    public void close() {
        if (this.acquireSemaphore.isValid()) {
            this.acquireSemaphore.close();
        }
        
        if (this.renderSemaphore.isValid()) {
            this.renderSemaphore.close();
        }
        
        if (this.presentFence.isValid()) {
            this.presentFence.close();
        }
    }
}
