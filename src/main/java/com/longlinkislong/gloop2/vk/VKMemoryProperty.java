/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKMemoryProperty implements CBitfield {
    DEVICE_LOCAL(VK10.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT),
    HOST_VISIBLE(VK10.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT),
    HOST_COHERENT(VK10.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
    HOST_CACHED(VK10.VK_MEMORY_PROPERTY_HOST_CACHED_BIT),
    LAZILY_ALLOCATED(VK10.VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKMemoryProperty(final int value) {
        this.value = value;
    }
    
    public static Set<VKMemoryProperty> flags(final int bits) {
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<VKMemoryProperty> flags) {
        return Tools.flagsToBitfield(flags);
    }
}
