/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkSpecializationInfo;
import org.lwjgl.vulkan.VkSpecializationMapEntry;

public final class VKSpecializationInfo {

    public static final class MapEntry {

        public final int constantID;
        public final int offset;
        public final long size;

        public MapEntry(final int constantID, final int offset, final long size) {
            this.constantID = constantID;
            this.offset = offset;
            this.size = size;
        }

        public MapEntry() {
            this(0, 0, 0);
        }

        public MapEntry withConstantID(final int constantID) {
            return new MapEntry(constantID, offset, size);
        }

        public MapEntry withOffset(final int offset) {
            return new MapEntry(constantID, offset, size);
        }

        public MapEntry withSize(final long size) {
            return new MapEntry(constantID, offset, size);
        }
    }

    public final List<MapEntry> mapEntries;
    public final ByteBuffer data;

    public VKSpecializationInfo(final List<MapEntry> mapEntries, final ByteBuffer data) {
        this.mapEntries = (mapEntries == null || mapEntries.isEmpty())
                ? Collections.emptyList()
                : Collections.unmodifiableList(new ArrayList<>(mapEntries));
        this.data = data;
    }

    public VKSpecializationInfo() {
        this(null, null);
    }

    public VKSpecializationInfo withMapEntries(final List<MapEntry> mapEntries) {
        return new VKSpecializationInfo(mapEntries, data);
    }

    public VKSpecializationInfo withMapEntries(final MapEntry... mapEntries) {
        return withMapEntries(Arrays.asList(mapEntries));
    }

    public VKSpecializationInfo withData(final ByteBuffer data) {
        return new VKSpecializationInfo(mapEntries, data);
    }
    
    VkSpecializationInfo vkStruct(final MemoryStack mem) {
        final VkSpecializationMapEntry.Buffer pMapEntries = VkSpecializationMapEntry.callocStack(this.mapEntries.size(), mem);
        
        for (int i = 0; i < this.mapEntries.size(); i++) {
            final MapEntry mapEntry = this.mapEntries.get(i);
            
            pMapEntries.get(i)
                    .constantID(mapEntry.constantID)
                    .offset(mapEntry.offset)
                    .size(mapEntry.size);
        }
        
        return VkSpecializationInfo.callocStack(mem)
                .pMapEntries(pMapEntries)
                .pData(this.data);
    }
}
