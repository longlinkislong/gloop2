/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class VKMemoryBarrier {
    public final Set<VKAccess> srcAccessMask;
    public final Set<VKAccess> dstAccessMask;
    
    public VKMemoryBarrier(final Collection<VKAccess> srcAccessMask, final Collection<VKAccess> dstAccessMask) {
        this.srcAccessMask = Tools.copySet(srcAccessMask);
        this.dstAccessMask = Tools.copySet(dstAccessMask);
    }
    
    public VKMemoryBarrier() {
        this(null, null);
    }
    
    public VKMemoryBarrier withSrcAccessMask(final Collection<VKAccess> srcAccessMask) {
        return new VKMemoryBarrier(srcAccessMask, dstAccessMask);
    }
    
    public VKMemoryBarrier withSrcAccessMask(final VKAccess... srcAccessMask) {
        return withSrcAccessMask(Arrays.asList(srcAccessMask));
    }
    
    public VKMemoryBarrier withDstAccessMask(final Collection<VKAccess> dstAccessMask) {
        return new VKMemoryBarrier(srcAccessMask, dstAccessMask);
    }
    
    public VKMemoryBarrier withDstAccessMask(final VKAccess... dstAccessMask) {
        return withDstAccessMask(Arrays.asList(dstAccessMask));
    }
}
