/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.List;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineDynamicStateCreateInfo;

public final class VKPipelineDynamicStateInfo {

    public final int flags;
    public final List<VKDynamicState> dynamicStates;

    public VKPipelineDynamicStateInfo(final int flags, final List<VKDynamicState> dynamicStates) {
        this.flags = flags;
        this.dynamicStates = Tools.copyList(dynamicStates);
    }
    
    public VKPipelineDynamicStateInfo() {
        this(0, null);
    }

    public VKPipelineDynamicStateInfo withFlags(final int flags) {
        return new VKPipelineDynamicStateInfo(flags, dynamicStates);
    }

    public VKPipelineDynamicStateInfo withDynamicStates(final List<VKDynamicState> dynamicStates) {
        return new VKPipelineDynamicStateInfo(flags, dynamicStates);
    }

    public VKPipelineDynamicStateInfo withDynamicStates(final VKDynamicState... dynamicStates) {
        return withDynamicStates(Arrays.asList(dynamicStates));
    }
    
    void vkStruct(final VkPipelineDynamicStateCreateInfo struct, final MemoryStack mem) {
        final IntBuffer pDynamicStates = mem.callocInt(this.dynamicStates.size());
        
        this.dynamicStates.stream()
                .mapToInt(state -> state.value)
                .forEach(pDynamicStates::put);
        
        pDynamicStates.flip();
        
        struct.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO)
                .flags(this.flags)
                .pDynamicStates(pDynamicStates);
    }
}
