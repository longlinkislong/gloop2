/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKPipelineBindPoint implements CEnum {
    GRAPHICS(VK10.VK_PIPELINE_BIND_POINT_GRAPHICS),
    COMPUTE(VK10.VK_PIPELINE_BIND_POINT_COMPUTE);

    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }

    private VKPipelineBindPoint(final int value) {
        this.value = value;
    }
    
    public static Optional<VKPipelineBindPoint> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
