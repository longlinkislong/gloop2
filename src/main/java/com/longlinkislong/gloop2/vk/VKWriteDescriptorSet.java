/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class VKWriteDescriptorSet {
    public final VKDescriptorSet dstSet;
    public final int dstBinding;
    public final int dstArrayElement;
    public final VKDescriptorType descriptorType;
    public final List<VKDescriptorImageInfo> imageInfo;
    public final List<VKDescriptorBufferInfo> bufferInfo;
    public final List<VKBufferView> texelBufferView;
    
    public VKWriteDescriptorSet(
        final VKDescriptorSet dstSet,
            final int dstBinding, final int dstArrayElement,
            final VKDescriptorType descriptorType,
            final List<VKDescriptorImageInfo> imageInfo,
            final List<VKDescriptorBufferInfo> bufferInfo,
            final List<VKBufferView> texelBufferView) {
        
        this.dstSet = dstSet;
        this.dstBinding = dstBinding;
        this.dstArrayElement = dstArrayElement;
        this.descriptorType = descriptorType;
        
        this.imageInfo = Tools.copyList(imageInfo);
        this.bufferInfo = Tools.copyList(bufferInfo);        
        this.texelBufferView = Tools.copyList(texelBufferView);
    }
    
    public VKWriteDescriptorSet() {
        this(null, 0, 0, VKDescriptorType.SAMPLER, null, null, null);
    }
    
    public VKWriteDescriptorSet withDstSet(final VKDescriptorSet dstSet) {
        return new VKWriteDescriptorSet(dstSet, dstBinding, dstArrayElement, descriptorType, imageInfo, bufferInfo, texelBufferView);
    }
    
    public VKWriteDescriptorSet withDstBinding(final int dstBinding) {
        return new VKWriteDescriptorSet(dstSet, dstBinding, dstArrayElement, descriptorType, imageInfo, bufferInfo, texelBufferView);
    }
    
    public VKWriteDescriptorSet withDstArrayElement(final int dstArrayElement) {
        return new VKWriteDescriptorSet(dstSet, dstBinding, dstArrayElement, descriptorType, imageInfo, bufferInfo, texelBufferView);
    }
    
    public VKWriteDescriptorSet withDescriptorType(final VKDescriptorType descriptorType) {
        return new VKWriteDescriptorSet(dstSet, dstBinding, dstArrayElement, descriptorType, imageInfo, bufferInfo, texelBufferView);
    }
    
    public VKWriteDescriptorSet withImageInfo(final List<VKDescriptorImageInfo> imageInfo) {
        return new VKWriteDescriptorSet(dstSet, dstBinding, dstArrayElement, descriptorType, imageInfo, bufferInfo, texelBufferView);
    }
    
    public VKWriteDescriptorSet withImageInfo(final VKDescriptorImageInfo... imageInfo) {
        return withImageInfo(Arrays.asList(imageInfo));
    }
    
    public VKWriteDescriptorSet withBufferInfo(final List<VKDescriptorBufferInfo> bufferInfo) {
        return new VKWriteDescriptorSet(dstSet, dstBinding, dstArrayElement, descriptorType, imageInfo, bufferInfo, texelBufferView);
    }
    
    public VKWriteDescriptorSet withBufferInfo(final VKDescriptorBufferInfo... bufferInfo) {
        return withBufferInfo(Arrays.asList(bufferInfo));
    }
    
    public VKWriteDescriptorSet withTexelBufferView(final List<VKBufferView> texelBufferView) {
        return new VKWriteDescriptorSet(dstSet, dstBinding, dstArrayElement, descriptorType, imageInfo, bufferInfo, texelBufferView);
    }
    
    public VKWriteDescriptorSet withTexelBufferView(final VKBufferView... texelBufferView) {
        return withTexelBufferView(Arrays.asList(texelBufferView));
    }
}
