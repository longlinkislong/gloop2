/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKDynamicState implements CEnum {
    VIEWPORT(VK10.VK_DYNAMIC_STATE_VIEWPORT),
    SCISSOR(VK10.VK_DYNAMIC_STATE_SCISSOR),
    LINE_WIDTH(VK10.VK_DYNAMIC_STATE_LINE_WIDTH),
    DEPTH_BIAS(VK10.VK_DYNAMIC_STATE_DEPTH_BIAS),
    BLEND_CONSTANTS(VK10.VK_DYNAMIC_STATE_BLEND_CONSTANTS),
    DEPTH_BOUNDS(VK10.VK_DYNAMIC_STATE_DEPTH_BOUNDS),
    STENCIL_COMPARE_MASK(VK10.VK_DYNAMIC_STATE_STENCIL_COMPARE_MASK),
    STENCIL_WRITE_MASK(VK10.VK_DYNAMIC_STATE_STENCIL_WRITE_MASK),
    STENCIL_REFERENCE(VK10.VK_DYNAMIC_STATE_STENCIL_REFERENCE);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKDynamicState(final int value) {
        this.value = value;
    }
    
    public static Optional<VKDynamicState> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
