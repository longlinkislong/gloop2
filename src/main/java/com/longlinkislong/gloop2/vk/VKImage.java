/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkImageCreateInfo;
import org.lwjgl.vulkan.VkImageSubresource;
import org.lwjgl.vulkan.VkSubresourceLayout;

/**
 *
 * @author zmichaels
 */
public class VKImage implements VKObject {

    private boolean swapchainImage;
    long handle;
    VKImageInfo info;

    VkDevice deviceHandle;

    public boolean isSwapchainImage() {
        return this.swapchainImage;
    }

    public VKImage() {
    }

    /**
     * Retrieves a VKImageSubresourceLayers that spans all layers in the image.
     * @param mipmapLevel the mipmap level
     * @return the entire range of layers.
     */
    public VKImageSubresourceLayers getAllLayers(final int mipmapLevel) {
        if (this.info.usage.contains(VKImageUsage.COLOR_ATTACHMENT) || this.info.usage.contains(VKImageUsage.SAMPLED)) {
            return new VKImageSubresourceLayers()
                    .withAspectMask(VKImageAspect.COLOR)
                    .withMipLevel(mipmapLevel)
                    .withBaseArrayLayer(0)
                    .withLayerCount(this.info.arrayLayers);
        } else if (this.info.usage.contains(VKImageUsage.DEPTH_STENCIL_ATTACHMENT)) {
            return new VKImageSubresourceLayers()
                    .withAspectMask(VKImageAspect.DEPTH, VKImageAspect.STENCIL)
                    .withMipLevel(mipmapLevel)
                    .withBaseArrayLayer(0)
                    .withLayerCount(this.info.arrayLayers);
        } else {
            throw new UnsupportedOperationException("Unable to infer aspectMask from usage!");
        }
    }
    
    /**
     * Retrieves a VKImageSubresourceLayers that spans only the base layer in the image.
     * @param mipmapLevel the mipmap level.
     * @return the base layer.
     */
    public VKImageSubresourceLayers getBaseLayer(final int mipmapLevel) {        
        if (this.info.usage.contains(VKImageUsage.COLOR_ATTACHMENT) || this.info.usage.contains(VKImageUsage.SAMPLED)) {
            return new VKImageSubresourceLayers()
                    .withAspectMask(VKImageAspect.COLOR)
                    .withMipLevel(mipmapLevel)
                    .withBaseArrayLayer(0)
                    .withLayerCount(1);
        } else if (this.info.usage.contains(VKImageUsage.DEPTH_STENCIL_ATTACHMENT)) {
            return new VKImageSubresourceLayers()
                    .withAspectMask(VKImageAspect.DEPTH, VKImageAspect.STENCIL)
                    .withMipLevel(mipmapLevel)
                    .withBaseArrayLayer(0)
                    .withLayerCount(1);
        } else {
            throw new UnsupportedOperationException("Unable to infer aspectMask from usage!");
        }
    }
    
    /**
     * Retrieves a VKImageSubresourceRange that spans the entire range of the image.
     * @return the entire range of the image.
     */
    public VKImageSubresourceRange getFullRange() {
        if (this.info.usage.contains(VKImageUsage.COLOR_ATTACHMENT) || this.info.usage.contains(VKImageUsage.SAMPLED)) {
            return new VKImageSubresourceRange()
                    .withAspectMask(VKImageAspect.COLOR)
                    .withLayerCount(info.arrayLayers)
                    .withLevelCount(info.mipLevels);
        } else if (this.info.usage.contains(VKImageUsage.DEPTH_STENCIL_ATTACHMENT)) {
            return new VKImageSubresourceRange()
                    .withAspectMask(VKImageAspect.DEPTH, VKImageAspect.STENCIL)
                    .withLayerCount(info.arrayLayers)
                    .withLevelCount(info.mipLevels);
        } else {
            throw new UnsupportedOperationException("Unable to infer aspectMask from usage!");
        }
    }

    public VKSubresourceLayout getSubresourceLayout(final VKImageSubresource subres) {
        if (!this.isValid()) {
            throw new IllegalStateException("VKImage is not valid!");
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkImageSubresource pSubres = VkImageSubresource.callocStack(mem)
                    .aspectMask(VKImageAspect.bits(subres.aspectMask))
                    .mipLevel(subres.mipLevel)
                    .arrayLayer(subres.arrayLayer);

            final VkSubresourceLayout pLayout = VkSubresourceLayout.callocStack(mem);

            VK10.vkGetImageSubresourceLayout(this.deviceHandle, this.handle, pSubres, pLayout);

            return new VKSubresourceLayout(pLayout);
        }
    }

    public VKImage(final VKContext ctx, final VKImageInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pQueueFamilyIndices;

            if (info.queueFamilies.isEmpty()) {
                pQueueFamilyIndices = null;
            } else {
                pQueueFamilyIndices = mem.callocInt(info.queueFamilies.size());

                info.queueFamilies.stream()
                        .mapToInt(family -> family.index)
                        .forEach(pQueueFamilyIndices::put);

                pQueueFamilyIndices.flip();
            }

            final VkImageCreateInfo createInfo = VkImageCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO)
                    .flags(VKImageInfo.CreateFlag.bits(info.flags))
                    .imageType(info.imageType.value)
                    .format(info.format.value)
                    .mipLevels(info.mipLevels)
                    .arrayLayers(info.arrayLayers)
                    .samples(info.samples)
                    .tiling(info.tiling.value)
                    .usage(VKImageUsage.bits(info.usage))
                    .sharingMode(info.sharingMode.value)
                    .pQueueFamilyIndices(pQueueFamilyIndices);

            createInfo.extent()
                    .width(info.width)
                    .height(info.height)
                    .depth(info.depth);

            final LongBuffer pHandle = mem.callocLong(1);
            final int err = VK10.vkCreateImage(ctx.device.handle, createInfo, null, pHandle);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            this.handle = pHandle.get(0);
            this.deviceHandle = ctx.device.handle;
            this.info = info;
        }
    }

    public VKImageInfo getInfo() {
        return this.info;
    }

    static VKImage wrapSwapchainImage(final VKContext ctx, final long handle, final VKFormat format, final int width, final int height) {
        final VKImage out = new VKImage();

        out.info = new VKImageInfo()
                .withImageType(VKImageType.IMAGE_2D)
                .withFormat(format)
                .withExtent(width, height, 0)
                .withMipLevels(1)
                .withArrayLayers(1)
                .withSamples(1)
                .withTiling(VKImageTiling.OPTIMAL)
                .withUsage(VKImageUsage.COLOR_ATTACHMENT)
                .withSharingMode(VKSharingMode.EXCLUSIVE)
                .withInitialLayout(VKImageLayout.COLOR_ATTACHMENT_OPTIMAL);

        out.deviceHandle = ctx.device.handle;
        out.handle = handle;
        out.swapchainImage = true;

        return out;
    }

    public void swap(final VKImage other) {
        if (other == null) {
            this.close();
            return;
        }

        final VkDevice tmpDev = other.deviceHandle;
        final long tmpHandle = other.handle;
        final VKImageInfo tmpInfo = other.info;
        final boolean tmpSwapchain = other.swapchainImage;

        other.deviceHandle = this.deviceHandle;
        other.handle = this.handle;
        other.info = this.info;
        other.swapchainImage = this.swapchainImage;

        this.deviceHandle = tmpDev;
        this.handle = tmpHandle;
        this.info = tmpInfo;
        this.swapchainImage = tmpSwapchain;
    }

    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.swapchainImage) {
            throw new UnsupportedOperationException("Cannot delete Swapchain Images!");
        }

        if (this.isValid()) {
            VK10.vkDestroyImage(this.deviceHandle, handle, null);
            this.handle = 0L;
            this.deviceHandle = null;
            this.info = null;
        }
    }
}
