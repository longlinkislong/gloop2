/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.KHRPushDescriptor;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkBufferCopy;
import org.lwjgl.vulkan.VkBufferImageCopy;
import org.lwjgl.vulkan.VkBufferMemoryBarrier;
import org.lwjgl.vulkan.VkClearAttachment;
import org.lwjgl.vulkan.VkClearColorValue;
import org.lwjgl.vulkan.VkClearDepthStencilValue;
import org.lwjgl.vulkan.VkClearRect;
import org.lwjgl.vulkan.VkClearValue;
import org.lwjgl.vulkan.VkCommandBuffer;
import org.lwjgl.vulkan.VkCommandBufferBeginInfo;
import org.lwjgl.vulkan.VkCommandBufferInheritanceInfo;
import org.lwjgl.vulkan.VkDescriptorBufferInfo;
import org.lwjgl.vulkan.VkDescriptorImageInfo;
import org.lwjgl.vulkan.VkImageCopy;
import org.lwjgl.vulkan.VkImageMemoryBarrier;
import org.lwjgl.vulkan.VkImageSubresourceRange;
import org.lwjgl.vulkan.VkMemoryBarrier;
import org.lwjgl.vulkan.VkRect2D;
import org.lwjgl.vulkan.VkRenderPassBeginInfo;
import org.lwjgl.vulkan.VkViewport;
import org.lwjgl.vulkan.VkWriteDescriptorSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class VKCommandBuffer {

    private static final Logger LOGGER = LoggerFactory.getLogger(VKCommandBuffer.class);

    VKCommandPool owner;
    VkCommandBuffer handle;
    public final boolean isPrimary;

    VKCommandBuffer(VKCommandPool owner, final long handle, final boolean isPrimary) {
        this.owner = owner;
        this.handle = new VkCommandBuffer(handle, owner.deviceHandle);
        this.isPrimary = isPrimary;
    }

    public void pushDescriptorSetKHR(final VKPipelineBindPoint bindPoint, final VKPipelineLayout layout, final int set, final VKWriteDescriptorSet... writes) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            VkWriteDescriptorSet.Buffer pDescriptorWrites = VkWriteDescriptorSet.callocStack(writes.length, mem);

            for (int i = 0; i < writes.length; i++) {
                pDescriptorWrites.get(i)
                        .sType(VK10.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
                        .descriptorType(writes[i].descriptorType.value)
                        .dstArrayElement(writes[i].dstArrayElement)
                        .dstBinding(writes[i].dstBinding);

                final VkDescriptorImageInfo.Buffer pImageInfo;

                if (writes[i].imageInfo.isEmpty()) {
                    pImageInfo = null;
                } else {
                    pImageInfo = VkDescriptorImageInfo.callocStack(writes[i].imageInfo.size(), mem);

                    for (int j = 0; j < writes[i].imageInfo.size(); j++) {
                        final VKDescriptorImageInfo info = writes[i].imageInfo.get(j);

                        pImageInfo.get(j)
                                .sampler(info.sampler.handle)
                                .imageView(info.imageView.handle)
                                .imageLayout(info.imageLayout.value);
                    }
                }

                final VkDescriptorBufferInfo.Buffer pBufferInfo;

                if (writes[i].bufferInfo.isEmpty()) {
                    pBufferInfo = null;
                } else {
                    pBufferInfo = VkDescriptorBufferInfo.callocStack(writes[i].bufferInfo.size(), mem);

                    for (int j = 0; j < writes[i].bufferInfo.size(); j++) {
                        final VKDescriptorBufferInfo info = writes[i].bufferInfo.get(j);

                        pBufferInfo.get(j)
                                .buffer(info.buffer.handle)
                                .offset(info.offset)
                                .range(info.range);
                    }
                }

                final LongBuffer pTexelBufferView;

                if (writes[i].texelBufferView.isEmpty()) {
                    pTexelBufferView = null;
                } else {
                    pTexelBufferView = mem.callocLong(writes[i].texelBufferView.size());

                    writes[i].texelBufferView.stream()
                            .mapToLong(tbv -> tbv.handle)
                            .forEach(pTexelBufferView::put);

                    pTexelBufferView.flip();
                }

                pDescriptorWrites.get(i)
                        .pBufferInfo(pBufferInfo)
                        .pImageInfo(pImageInfo)
                        .pTexelBufferView(pTexelBufferView);
            }

            KHRPushDescriptor.vkCmdPushDescriptorSetKHR(handle, bindPoint.value, layout.handle, set, pDescriptorWrites);
        }
    }

    public void updateBuffer(final VKBuffer dstBuffer, final long offset, final long size, final ByteBuffer data) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdUpdateBuffer(handle, dstBuffer.handle, offset, data);
    }

    public void setEvent(final VKEvent event, final Set<VKPipelineStage> stageMask) {
        if (!this.isValid()) {
            throw new IllegalStateException("CommandBuffer is not valid!");
        }

        VK10.vkCmdSetEvent(this.handle, event.handle, VKPipelineStage.bits(stageMask));
    }

    public void resetEvent(final VKEvent event, final Set<VKPipelineStage> stageMask) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdResetEvent(this.handle, event.handle, VKPipelineStage.bits(stageMask));
    }

    public void setLineWidth(final float lineWidth) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdSetLineWidth(handle, lineWidth);
    }

    public void setDepthBounds(final float minDepthBounds, final float maxDepthBounds) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdSetDepthBounds(handle, minDepthBounds, maxDepthBounds);
    }

    public void setDepthBias(final float depthBiasConstantFactor, final float depthBiasClamp, final float depthBiasSlopeFactor) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdSetDepthBias(handle, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor);
    }

    public void setBlendConstants(final float r, final float g, final float b, final float a) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            VK10.vkCmdSetBlendConstants(handle, mem.floats(r, g, b, a));
        }
    }

    public void nextSubpass(final VKSubpassContents subpassContents) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdNextSubpass(handle, subpassContents.value);
    }

    public void fillBuffer(final VKBuffer dstBuffer, final long dstOffset, final long size, final int data) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdFillBuffer(handle, dstBuffer.handle, dstOffset, size, data);
    }

    public void drawIndirect(final VKBuffer buffer, final long offset, final int drawCount, final int stride) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdDrawIndirect(handle, buffer.handle, offset, drawCount, stride);
    }

    public void drawIndexedIndirect(final VKBuffer buffer, final long offset, final int drawCount, final int stride) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdDrawIndexedIndirect(handle, buffer.handle, offset, drawCount, stride);
    }

    public void drawIndexed(final int indexCount, final int instanceCount, final int firstIndex, final int vertexOffset, final int firstInstance) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdDrawIndexed(handle, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
    }

    public void dispatchIndirect(final VKBuffer buffer, final long offset) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdDispatchIndirect(handle, buffer.handle, offset);
    }

    public void dispatch(final int groupCountX, final int groupCountY, final int groupCountZ) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdDispatch(handle, groupCountX, groupCountY, groupCountZ);
    }

    public void copyImageToBuffer(final VKImage srcImage, final VKImageLayout srcImageLayout, final VKBuffer dstBuffer, final List<VKBufferImageCopy> regions) {
        assert this.isValid() : "CommandBuffer is not valid!";

        if (regions.isEmpty()) {
            LOGGER.warn("Copy region list is empty!");
            return;
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkBufferImageCopy.Buffer pRegions = VkBufferImageCopy.callocStack(regions.size(), mem);

            for (int i = 0; i < regions.size(); i++) {
                final VKBufferImageCopy r = regions.get(i);

                pRegions.get(i)
                        .bufferImageHeight(r.bufferImageHeight)
                        .bufferOffset(r.bufferOffset)
                        .bufferRowLength(r.bufferRowLength)
                        .imageExtent().set(r.imageWidth, r.imageHeight, r.imageDepth);
                pRegions.get(i)
                        .imageOffset().set(r.imageOffsetX, r.imageOffsetY, r.imageOffsetZ);
            }

            VK10.vkCmdCopyImageToBuffer(handle, srcImage.handle, srcImageLayout.value, dstBuffer.handle, pRegions);
        }
    }

    public void copyImageToBuffer(final VKImage srcImage, final VKImageLayout srcImageLayout, final VKBuffer dstBuffer, final VKBufferImageCopy... regions) {
        copyImageToBuffer(srcImage, srcImageLayout, dstBuffer, Arrays.asList(regions));
    }

    public void copyBufferToImage(final VKBuffer srcBuffer, final VKImage dstImage, final VKImageLayout dstImageLayout, final List<VKBufferImageCopy> regions) {
        assert this.isValid() : "CommandBuffer is not valid!";

        if (regions.isEmpty()) {
            LOGGER.warn("Copy region list is empty!");
            return;
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkBufferImageCopy.Buffer pRegions = VkBufferImageCopy.callocStack(regions.size(), mem);

            for (int i = 0; i < regions.size(); i++) {
                final VKBufferImageCopy r = regions.get(i);

                pRegions.get(i)
                        .bufferOffset(r.bufferOffset)
                        .bufferRowLength(r.bufferRowLength)
                        .bufferImageHeight(r.bufferImageHeight)
                        .imageExtent().set(r.imageWidth, r.imageHeight, r.imageDepth);
                pRegions.get(i).imageOffset().set(r.imageOffsetX, r.imageOffsetY, r.imageOffsetZ);
                pRegions.get(i).imageSubresource()
                        .aspectMask(VKImageAspect.bits(r.imageSubresource.aspectMask))
                        .mipLevel(r.imageSubresource.mipLevel)
                        .baseArrayLayer(r.imageSubresource.baseArrayLayer)
                        .layerCount(r.imageSubresource.layerCount);
            }

            VK10.vkCmdCopyBufferToImage(handle, srcBuffer.handle, dstImage.handle, dstImageLayout.value, pRegions);
        }
    }

    public void copyBufferToImage(
            final VKBuffer srcBuffer, final VKImage dstImage,
            final VKImageLayout dstImageLayout, final VKBufferImageCopy... regions) {

        copyBufferToImage(srcBuffer, dstImage, dstImageLayout, Arrays.asList(regions));
    }

    public void copyBuffer(final VKBuffer srcBuffer, final VKBuffer dstBuffer, final List<VKBufferCopy> regions) {
        assert this.isValid() : "CommandBuffer is not valid!";

        if (regions.isEmpty()) {
            LOGGER.warn("Copy region list is empty!");
            return;
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkBufferCopy.Buffer pRegions = VkBufferCopy.callocStack(regions.size(), mem);

            for (int i = 0; i < regions.size(); i++) {
                final VKBufferCopy r = regions.get(i);

                pRegions.get(i)
                        .srcOffset(r.srcOffset)
                        .dstOffset(r.dstOffset)
                        .size(r.size);
            }

            VK10.vkCmdCopyBuffer(handle, srcBuffer.handle, dstBuffer.handle, pRegions);
        }
    }

    public void copyBuffer(final VKBuffer srcBuffer, final VKBuffer dstBuffer, final VKBufferCopy... regions) {
        copyBuffer(srcBuffer, dstBuffer, Arrays.asList(regions));
    }

    public void clearDepthStencilImage(final VKImage image, final VKImageLayout imageLayout, final ByteBuffer depthStencil, final VKImageSubresourceRange[] ranges) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkImageSubresourceRange.Buffer pRanges = VkImageSubresourceRange.callocStack(ranges.length, mem);

            for (int i = 0; i < ranges.length; i++) {
                pRanges.get(i)
                        .aspectMask(VKImageAspect.bits(ranges[i].aspectMask))
                        .baseArrayLayer(ranges[i].baseArrayLayer)
                        .baseMipLevel(ranges[i].baseMipLevel)
                        .layerCount(ranges[i].layerCount)
                        .levelCount(ranges[i].levelCount);
            }

            final VkClearDepthStencilValue pDepthStencil = VkClearDepthStencilValue.callocStack(mem)
                    .set(depthStencil.getFloat(), depthStencil.getInt());

            VK10.vkCmdClearDepthStencilImage(handle, image.handle, imageLayout.value, pDepthStencil, pRanges);
        }
    }

    public void clearColorImage(final VKImage image, final VKImageLayout imageLayout, final ByteBuffer color, final VKImageSubresourceRange[] ranges) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkImageSubresourceRange.Buffer pRanges = VkImageSubresourceRange.callocStack(ranges.length, mem);

            for (int i = 0; i < ranges.length; i++) {
                pRanges.get(i)
                        .aspectMask(VK10.VK_IMAGE_ASPECT_COLOR_BIT)
                        .baseArrayLayer(ranges[i].baseArrayLayer)
                        .baseMipLevel(ranges[i].baseMipLevel)
                        .layerCount(ranges[i].layerCount)
                        .levelCount(ranges[i].levelCount);
            }

            final VkClearColorValue pColor = VkClearColorValue.callocStack(mem)
                    .float32(0, color.getFloat())
                    .float32(1, color.getFloat())
                    .float32(2, color.getFloat())
                    .float32(3, color.getFloat());

            VK10.vkCmdClearColorImage(handle, image.handle, imageLayout.value, pColor, pRanges);
        }
    }

    public void clearAttachments(final VKClearAttachment[] attachments, final VKClearRect[] rects) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkClearAttachment.Buffer pAttachments = VkClearAttachment.callocStack(attachments.length, mem);

            for (int i = 0; i < attachments.length; i++) {
                pAttachments.get(i)
                        .aspectMask(VKImageAspect.bits(attachments[i].aspectMask))
                        .colorAttachment(attachments[i].colorAttachment)
                        .clearValue()
                        .color()
                        .float32(0, attachments[i].clearValue.getFloat())
                        .float32(1, attachments[i].clearValue.getFloat())
                        .float32(2, attachments[i].clearValue.getFloat())
                        .float32(3, attachments[i].clearValue.getFloat());
            }

            final VkClearRect.Buffer pRects = VkClearRect.callocStack(rects.length, mem);

            for (int i = 0; i < rects.length; i++) {
                pRects.get(i).rect().extent().set(rects[i].width, rects[i].height);
                pRects.get(i).rect().offset().set(rects[i].x, rects[i].y);
                pRects.get(i)
                        .baseArrayLayer(rects[i].baseArrayLayer)
                        .layerCount(rects[i].layerCount);
            }

            VK10.vkCmdClearAttachments(handle, pAttachments, pRects);
        }
    }

    public void pushConstants(final VKPipelineLayout layout, final Set<VKShaderStage> stageFlags, final int offset, final ByteBuffer values) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdPushConstants(handle, layout.handle, VKShaderStage.bits(stageFlags), offset, values);
    }

    public void bindIndexBuffer(final VKBuffer buffer, final long offset, final VKIndexType indexType) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdBindIndexBuffer(handle, buffer.handle, offset, indexType.value);
    }

    public void bindDescriptorSets(
            final VKPipelineBindPoint bindPoint,
            final VKPipelineLayout layout,
            final int firstSet,
            final VKDescriptorSet[] descriptorSets) {

        bindDescriptorSets(bindPoint, layout, firstSet, descriptorSets, null);
    }

    public void bindDescriptorSet(
            final VKPipelineBindPoint bindPoint,
            final VKPipelineLayout layout,
            final int firstSet,
            final VKDescriptorSet descriptorSet) {

        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            VK10.vkCmdBindDescriptorSets(handle, bindPoint.value, layout.handle, firstSet, mem.longs(descriptorSet.handle), null);
        }
    }

    public void bindDescriptorSet(
            final VKPipelineBindPoint bindPoint,
            final VKPipelineLayout layout,
            final int firstSet,
            final VKDescriptorSet descriptorSet,
            final int dynamicOffset) {

        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            VK10.vkCmdBindDescriptorSets(handle, bindPoint.value, layout.handle, firstSet, mem.longs(descriptorSet.handle), mem.ints(dynamicOffset));
        }
    }

    public void bindDescriptorSets(
            final VKPipelineBindPoint bindPoint,
            final VKPipelineLayout layout,
            final int firstSet,
            final VKDescriptorSet[] descriptorSets,
            final int[] dynamicOffsets) {

        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pDynamicOffsets;
            final LongBuffer pDescriptorSets = mem.callocLong(descriptorSets.length);

            if (dynamicOffsets == null || dynamicOffsets.length == 0) {
                pDynamicOffsets = null;
            } else {
                pDynamicOffsets = mem.ints(dynamicOffsets);
            }

            for (int i = 0; i < descriptorSets.length; i++) {
                pDescriptorSets.put(i, descriptorSets[i].handle);
            }

            VK10.vkCmdBindDescriptorSets(handle, bindPoint.value, layout.handle, firstSet, pDescriptorSets, pDynamicOffsets);
        }
    }

    public void stageImageLayoutTransition(
            final VKImage img, final VKImageSubresourceRange subresources,
            final Set<VKPipelineStage> srcStages, final VKImageLayout oldLayout,
            final Set<VKPipelineStage> dstStages, final VKImageLayout newLayout) {

        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            VkImageMemoryBarrier.Buffer pImgBarrier = VkImageMemoryBarrier.callocStack(1, mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
                    .oldLayout(oldLayout.value)
                    .newLayout(newLayout.value)
                    .srcQueueFamilyIndex(VK10.VK_QUEUE_FAMILY_IGNORED)
                    .dstQueueFamilyIndex(VK10.VK_QUEUE_FAMILY_IGNORED)
                    .image(img.handle);

            pImgBarrier.subresourceRange()
                    .aspectMask(VKImageAspect.bits(subresources.aspectMask))
                    .baseMipLevel(subresources.baseMipLevel)
                    .baseArrayLayer(subresources.baseArrayLayer)
                    .levelCount(subresources.levelCount)
                    .layerCount(subresources.layerCount);

            switch (oldLayout) {
                case COLOR_ATTACHMENT_OPTIMAL:
                    pImgBarrier.srcAccessMask(VK10.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);
                    break;
                case DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
                    pImgBarrier.srcAccessMask(VK10.VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);
                    break;
                case TRANSFER_DST_OPTIMAL:
                    pImgBarrier.srcAccessMask(VK10.VK_ACCESS_TRANSFER_WRITE_BIT);
                    break;
                case PREINITIALIZED:
                    pImgBarrier.srcAccessMask(VK10.VK_ACCESS_HOST_WRITE_BIT);
                    break;
            }

            switch (newLayout) {
                case TRANSFER_DST_OPTIMAL:
                    pImgBarrier.dstAccessMask(VK10.VK_ACCESS_TRANSFER_WRITE_BIT);
                    break;
                case TRANSFER_SRC_OPTIMAL:
                    pImgBarrier.dstAccessMask(VK10.VK_ACCESS_TRANSFER_READ_BIT);
                    break;
                case SHADER_READ_ONLY_OPTIMAL:
                    pImgBarrier.dstAccessMask(VK10.VK_ACCESS_SHADER_READ_BIT);
                    break;
                case COLOR_ATTACHMENT_OPTIMAL:
                    pImgBarrier.dstAccessMask(VK10.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);
                    break;
                case DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
                    pImgBarrier.dstAccessMask(VK10.VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);
                    break;
            }

            VK10.vkCmdPipelineBarrier(
                    handle,
                    VKPipelineStage.bits(srcStages), VKPipelineStage.bits(dstStages),
                    0,
                    null, null, pImgBarrier);
        }
    }

    public void beginRenderPass(
            final VKFramebuffer fb,
            final VKRenderPass renderPass,
            final VKSubpassContents subpassContents,
            final VKScissor renderArea,
            final ByteBuffer[] clearValues) {

        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkRenderPassBeginInfo info = VkRenderPassBeginInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO)
                    .renderPass(renderPass.handle)
                    .framebuffer(fb.handle);

            renderArea.apply(info.renderArea());

            final VkClearValue.Buffer pClearValues;

            if (clearValues == null) {
                pClearValues = null;
            } else {
                pClearValues = VkClearValue.callocStack(clearValues.length, mem);

                for (int i = 0; i < clearValues.length; i++) {
                    pClearValues.get(i)
                            .color()
                            .float32(0, clearValues[i].getFloat())
                            .float32(1, clearValues[i].getFloat())
                            .float32(2, clearValues[i].getFloat())
                            .float32(3, clearValues[i].getFloat());
                }
            }

            info.pClearValues(pClearValues);
            VK10.vkCmdBeginRenderPass(this.handle, info, subpassContents.value);
        }
    }

    public void bindPipeline(final VKGraphicsPipeline pipeline) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdBindPipeline(handle, VK10.VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.handle);
    }

    public void bindVertexBuffer(final int firstBinding, final VKBuffer buffer, final long offset) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            VK10.vkCmdBindVertexBuffers(handle, firstBinding, mem.longs(buffer.handle), mem.longs(offset));
        }
    }

    public void draw(final int vertexCount, final int instanceCount, final int firstVertex, final int firstInstance) {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdDraw(handle, vertexCount, instanceCount, firstVertex, firstInstance);
    }

    public void copyImage(final VKImage srcImage, final VKImageLayout srcImageLayout, final VKImage dstImage, final VKImageLayout dstImageLayout, final VKImageCopy... regions) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkImageCopy.Buffer pRegions = VkImageCopy.callocStack(regions.length, mem);

            for (int i = 0; i < regions.length; i++) {
                pRegions.get(i).srcSubresource()
                        .aspectMask(VKImageAspect.bits(regions[i].srcSubresource.aspectMask))
                        .mipLevel(regions[i].srcSubresource.mipLevel)
                        .baseArrayLayer(regions[i].srcSubresource.baseArrayLayer)
                        .layerCount(regions[i].srcSubresource.layerCount);

                pRegions.get(i).srcOffset().set(regions[i].srcOffsetX, regions[i].srcOffsetY, regions[i].srcOffsetZ);

                pRegions.get(i).dstSubresource()
                        .aspectMask(VKImageAspect.bits(regions[i].dstSubresource.aspectMask))
                        .mipLevel(regions[i].dstSubresource.mipLevel)
                        .baseArrayLayer(regions[i].dstSubresource.baseArrayLayer)
                        .layerCount(regions[i].dstSubresource.layerCount);

                pRegions.get(i).dstOffset().set(regions[i].dstOffsetX, regions[i].dstOffsetY, regions[i].dstOffsetZ);

                pRegions.get(i).extent().set(regions[i].width, regions[i].height, regions[i].depth);
            }

            VK10.vkCmdCopyImage(handle, srcImage.handle, srcImageLayout.value, dstImage.handle, dstImageLayout.value, pRegions);
        }
    }

    public void bindVertexBuffers(final int firstBinding, final VKBuffer[] buffers, final long[] offsets) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final LongBuffer pBuffers = mem.callocLong(buffers.length);
            final LongBuffer pOffsets = mem.callocLong(offsets.length);

            for (int i = 0; i < buffers.length; i++) {
                pBuffers.put(buffers[i].handle);
                pOffsets.put(offsets[i]);
            }

            pBuffers.flip();
            pOffsets.flip();

            VK10.vkCmdBindVertexBuffers(handle, firstBinding, pBuffers, pOffsets);
        }
    }

    public void setViewport(final VKViewport v) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkViewport.Buffer viewport = VkViewport.callocStack(1, mem);

            v.apply(viewport.get(0));

            VK10.vkCmdSetViewport(handle, 0, viewport);
        }
    }

    public void setScissor(final VKScissor r) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkRect2D.Buffer scissor = VkRect2D.callocStack(1, mem);

            r.apply(scissor.get(0));

            VK10.vkCmdSetScissor(handle, 0, scissor);
        }
    }

    public void endRenderPass() {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkCmdEndRenderPass(this.handle);
    }

    public void reset(final int flags) {
        assert this.isValid() : "CommandBuffer is not valid!";

        final int err = VK10.vkResetCommandBuffer(this.handle, flags);

        assert Tools.vksuccess(err) : Tools.vkerr(err);
    }

    public void waitEvents(
            final List<VKEvent> events,
            final Set<VKPipelineStage> srcStageMask, final Set<VKPipelineStage> dstStageMask,
            final List<VKMemoryBarrier> memoryBarriers,
            final List<VKBufferMemoryBarrier> bufferMemoryBarriers,
            final List<VKImageMemoryBarrier> imageMemoryBarriers) {

        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final LongBuffer pEvents;

            if (events == null || events.isEmpty()) {
                pEvents = null;
            } else {
                pEvents = mem.callocLong(events.size());

                events.stream()
                        .mapToLong(event -> event.handle)
                        .forEach(pEvents::put);

                pEvents.flip();
            }

            final VkMemoryBarrier.Buffer pMemoryBarriers;

            if (memoryBarriers == null || memoryBarriers.isEmpty()) {
                pMemoryBarriers = null;
            } else {
                pMemoryBarriers = VkMemoryBarrier.callocStack(memoryBarriers.size(), mem);
                int i = 0;

                for (VKMemoryBarrier mb : memoryBarriers) {
                    pMemoryBarriers.get(i++)
                            .sType(VK10.VK_STRUCTURE_TYPE_MEMORY_BARRIER)
                            .srcAccessMask(VKAccess.bits(mb.srcAccessMask))
                            .dstAccessMask(VKAccess.bits(mb.dstAccessMask));
                }
            }

            final VkBufferMemoryBarrier.Buffer pBufferMemoryBarriers;

            if (bufferMemoryBarriers == null || bufferMemoryBarriers.isEmpty()) {
                pBufferMemoryBarriers = null;
            } else {
                pBufferMemoryBarriers = VkBufferMemoryBarrier.callocStack(bufferMemoryBarriers.size(), mem);
                int i = 0;

                for (VKBufferMemoryBarrier bmb : bufferMemoryBarriers) {
                    pBufferMemoryBarriers.get(i++)
                            .sType(VK10.VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER)
                            .srcAccessMask(VKAccess.bits(bmb.srcAccessMask))
                            .dstAccessMask(VKAccess.bits(bmb.dstAccessMask))
                            .srcQueueFamilyIndex(bmb.srcQueueFamily == null ? VK10.VK_QUEUE_FAMILY_IGNORED : bmb.srcQueueFamily.index)
                            .dstQueueFamilyIndex(bmb.dstQueueFamily == null ? VK10.VK_QUEUE_FAMILY_IGNORED : bmb.dstQueueFamily.index)
                            .buffer(bmb.buffer.handle)
                            .offset(bmb.offset)
                            .size(bmb.size);
                }
            }

            final VkImageMemoryBarrier.Buffer pImageMemoryBarriers;

            if (imageMemoryBarriers == null || imageMemoryBarriers.isEmpty()) {
                pImageMemoryBarriers = null;
            } else {
                pImageMemoryBarriers = VkImageMemoryBarrier.callocStack(imageMemoryBarriers.size(), mem);
                int i = 0;

                for (VKImageMemoryBarrier imb : imageMemoryBarriers) {
                    pImageMemoryBarriers.get(i++)
                            .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
                            .srcAccessMask(VKAccess.bits(imb.srcAccessMask))
                            .dstAccessMask(VKAccess.bits(imb.dstAccessMask))
                            .oldLayout(imb.oldLayout.value)
                            .newLayout(imb.newLayout.value)
                            .srcQueueFamilyIndex(imb.srcQueueFamily == null ? VK10.VK_QUEUE_FAMILY_IGNORED : imb.srcQueueFamily.index)
                            .dstQueueFamilyIndex(imb.dstQueueFamily == null ? VK10.VK_QUEUE_FAMILY_IGNORED : imb.dstQueueFamily.index)
                            .image(imb.image.handle)
                            .subresourceRange()
                            .aspectMask(VKImageAspect.bits(imb.subresourceRange.aspectMask))
                            .baseArrayLayer(imb.subresourceRange.baseArrayLayer)
                            .baseMipLevel(imb.subresourceRange.baseMipLevel)
                            .layerCount(imb.subresourceRange.layerCount)
                            .levelCount(imb.subresourceRange.levelCount);
                }
            }

            VK10.vkCmdWaitEvents(
                    handle,
                    pEvents,
                    VKPipelineStage.bits(srcStageMask), VKPipelineStage.bits(dstStageMask),
                    pMemoryBarriers,
                    pBufferMemoryBarriers,
                    pImageMemoryBarriers);
        }
    }

    public void pipelineBarrier(final VKPipelineBarrier barrier) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkMemoryBarrier.Buffer pMemoryBarriers;

            if (barrier.memoryBarriers.isEmpty()) {
                pMemoryBarriers = null;
            } else {
                pMemoryBarriers = VkMemoryBarrier.callocStack(barrier.memoryBarriers.size(), mem);

                for (int i = 0; i < barrier.memoryBarriers.size(); i++) {
                    final VKMemoryBarrier mb = barrier.memoryBarriers.get(i);

                    pMemoryBarriers.get(i)
                            .sType(VK10.VK_STRUCTURE_TYPE_MEMORY_BARRIER)
                            .srcAccessMask(VKAccess.bits(mb.srcAccessMask))
                            .dstAccessMask(VKAccess.bits(mb.dstAccessMask));
                }
            }

            final VkBufferMemoryBarrier.Buffer pBufferMemoryBarriers;

            if (barrier.bufferMemoryBarriers.isEmpty()) {
                pBufferMemoryBarriers = null;
            } else {
                pBufferMemoryBarriers = VkBufferMemoryBarrier.callocStack(barrier.bufferMemoryBarriers.size(), mem);

                for (int i = 0; i < barrier.bufferMemoryBarriers.size(); i++) {
                    final VKBufferMemoryBarrier bmb = barrier.bufferMemoryBarriers.get(i);

                    pBufferMemoryBarriers.get(i)
                            .sType(VK10.VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER)
                            .srcAccessMask(VKAccess.bits(bmb.srcAccessMask))
                            .dstAccessMask(VKAccess.bits(bmb.dstAccessMask))
                            .srcQueueFamilyIndex(bmb.srcQueueFamily == null ? VK10.VK_QUEUE_FAMILY_IGNORED : bmb.srcQueueFamily.index)
                            .dstQueueFamilyIndex(bmb.dstQueueFamily == null ? VK10.VK_QUEUE_FAMILY_IGNORED : bmb.dstQueueFamily.index)
                            .buffer(bmb.buffer.handle)
                            .offset(bmb.offset)
                            .size(bmb.size);
                }
            }

            final VkImageMemoryBarrier.Buffer pImageMemoryBarriers;

            if (barrier.imageMemoryBarriers.isEmpty()) {
                pImageMemoryBarriers = null;
            } else {
                pImageMemoryBarriers = VkImageMemoryBarrier.callocStack(barrier.imageMemoryBarriers.size(), mem);

                for (int i = 0; i < barrier.imageMemoryBarriers.size(); i++) {
                    final VKImageMemoryBarrier imb = barrier.imageMemoryBarriers.get(i);

                    pImageMemoryBarriers.get(i)
                            .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
                            .srcAccessMask(VKAccess.bits(imb.srcAccessMask))
                            .dstAccessMask(VKAccess.bits(imb.dstAccessMask))
                            .oldLayout(imb.oldLayout.value)
                            .newLayout(imb.newLayout.value)
                            .srcQueueFamilyIndex(imb.srcQueueFamily == null ? VK10.VK_QUEUE_FAMILY_IGNORED : imb.srcQueueFamily.index)
                            .dstQueueFamilyIndex(imb.dstQueueFamily == null ? VK10.VK_QUEUE_FAMILY_IGNORED : imb.dstQueueFamily.index)
                            .image(imb.image.handle)
                            .subresourceRange()
                            .aspectMask(VKImageAspect.bits(imb.subresourceRange.aspectMask))
                            .baseMipLevel(imb.subresourceRange.baseMipLevel)
                            .levelCount(imb.subresourceRange.levelCount)
                            .baseArrayLayer(imb.subresourceRange.baseArrayLayer)
                            .layerCount(imb.subresourceRange.layerCount);

                }
            }

            VK10.vkCmdPipelineBarrier(
                    handle,
                    VKPipelineStage.bits(barrier.srcStageMask),
                    VKPipelineStage.bits(barrier.dstStageMask),
                    VKDependency.bits(barrier.dependencyFlags),
                    pMemoryBarriers,
                    pBufferMemoryBarriers,
                    pImageMemoryBarriers);
        }
    }

    public void executeCommands(final VKCommandBuffer... commands) {
        assert this.isValid() : "CommandBuffer is not valid!";

        // shortcut if only one secondary command buffer was supplied
        if (commands.length == 1) {
            VK10.vkCmdExecuteCommands(handle, commands[0].handle);
            return;
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pCommands = mem.callocPointer(commands.length);

            for (int i = 0; i < commands.length; i++) {
                pCommands.put(i, commands[i].handle);
            }

            VK10.vkCmdExecuteCommands(handle, pCommands);
        }
    }

    public void begin(final VKCommandBufferBeginInfo info) {
        assert this.isValid() : "CommandBuffer is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkCommandBufferBeginInfo beginInfo = VkCommandBufferBeginInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO)
                    .flags(VKCommandBufferBeginInfo.Usage.bits(info.flags));

            if (info.inheritanceInfo != null) {
                final VkCommandBufferInheritanceInfo inheritanceInfo = VkCommandBufferInheritanceInfo.callocStack(mem)
                        .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO)
                        .renderPass(info.inheritanceInfo.renderPass == null ? 0L : info.inheritanceInfo.renderPass.handle)
                        .framebuffer(info.inheritanceInfo.framebuffer == null ? 0L : info.inheritanceInfo.framebuffer.handle)
                        .subpass(info.inheritanceInfo.subpass);

                beginInfo.pInheritanceInfo(inheritanceInfo);
            }

            final int err = VK10.vkBeginCommandBuffer(this.handle, beginInfo);

            assert Tools.vksuccess(err) : Tools.vkerr(err);
        }
    }

    public void end() {
        assert this.isValid() : "CommandBuffer is not valid!";

        VK10.vkEndCommandBuffer(handle);
    }

    public boolean isValid() {
        return this.handle != null;
    }
}
