/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKComponentSwizzle implements CEnum {
    IDENTITY(VK10.VK_COMPONENT_SWIZZLE_IDENTITY),
    ZERO(VK10.VK_COMPONENT_SWIZZLE_ZERO),
    ONE(VK10.VK_COMPONENT_SWIZZLE_ONE),
    R(VK10.VK_COMPONENT_SWIZZLE_R),
    G(VK10.VK_COMPONENT_SWIZZLE_G),
    B(VK10.VK_COMPONENT_SWIZZLE_B),
    A(VK10.VK_COMPONENT_SWIZZLE_A);

    public final int value;

    private VKComponentSwizzle(final int value) {
        this.value = value;
    }

    public static Optional<VKComponentSwizzle> of(final int vkenum) {
        return Tools.valueOf(vkenum, Arrays.stream(values()));
    }

    @Override
    public int getValue() {
        return this.value;
    }
}
