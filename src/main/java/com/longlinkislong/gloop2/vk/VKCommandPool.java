/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkCommandBufferAllocateInfo;
import org.lwjgl.vulkan.VkCommandPoolCreateInfo;
import org.lwjgl.vulkan.VkDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class VKCommandPool implements VKObject {

    private static final Logger LOGGER = LoggerFactory.getLogger(VKCommandPool.class);

    VkDevice deviceHandle;
    long handle;
    VKCommandPoolInfo info;
    
    public VKCommandPool() {}

    public VKCommandPool(final VKContext ctx, final VKCommandPoolInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkCommandPoolCreateInfo cmdPoolInfo = VkCommandPoolCreateInfo.mallocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO)
                    .queueFamilyIndex(info.queueFamily.index)
                    .flags(VKCommandPoolInfo.CreateFlag.bits(info.flags));

            final LongBuffer pCmdPool = mem.callocLong(1);
            final int err = VK10.vkCreateCommandPool(ctx.device.handle, cmdPoolInfo, null, pCmdPool);

            assert err == VK10.VK_SUCCESS : Tools.vkerr(err);

            this.handle = pCmdPool.get(0);
            this.deviceHandle = ctx.device.handle;
        }

        LOGGER.trace("Successfully created Vulkan Command Pool!");
    }

    public void swap(final VKCommandPool other) {
        if (other == null) {
            this.close();
            return;
        }

        final VkDevice tmpDevHandle = other.deviceHandle;
        final long tmpHandle = other.handle;
        final VKCommandPoolInfo tmpInfo = other.info;

        other.deviceHandle = this.deviceHandle;
        other.handle = this.handle;
        other.info = tmpInfo;

        this.deviceHandle = tmpDevHandle;
        this.handle = tmpHandle;
        this.info = tmpInfo;
    }

    public void reset(boolean releaseResources) {
        if (this.isValid()) {
            VK10.vkResetCommandPool(this.deviceHandle, this.handle, releaseResources ? VK10.VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT : 0);
        }
    }
    
    public void freeCommandBuffers(final VKCommandBuffer... cmdBuffers) {
        freeCommandBuffers(Arrays.asList(cmdBuffers));
    }
    
    public void freeCommandBuffers(final List<VKCommandBuffer> cmdBuffers) {
        if (!this.isValid()) {
            throw new IllegalStateException("CommandPool is not valid!");
        }
        
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pCmdBuffers = mem.callocPointer(cmdBuffers.size());
            
            cmdBuffers.stream()
                    .filter(cmdbuf -> cmdbuf.owner == this)
                    .map(cmdbuf -> cmdbuf.handle)
                    .forEach(pCmdBuffers::put);
            
            pCmdBuffers.flip();
            
            VK10.vkFreeCommandBuffers(deviceHandle, handle, pCmdBuffers);
            
            cmdBuffers.stream()
                    .filter(cmdbuf -> cmdbuf.owner == this)
                    .forEach(cmdbuf -> cmdbuf.handle = null);
        }
    }

    @Override
    public boolean isValid() {
        return this.handle > 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyCommandPool(deviceHandle, handle, null);
            this.handle = 0L;
            this.deviceHandle = null;
            LOGGER.trace("Successfully closed Vulkan Command Pool!");
        }
    }

    public VKCommandBuffer newPrimaryCommandBuffer() {
        return newPrimaryCommandBuffers(1).get(0);
    }

    public VKCommandBuffer newSecondaryCommandBuffer() {
        return newSecondaryCommandBuffers(1).get(0);
    }

    public List<VKCommandBuffer> newPrimaryCommandBuffers(final int count) {
        return newCommandBuffers(count, true);
    }

    public List<VKCommandBuffer> newSecondaryCommandBuffers(final int count) {
        return newCommandBuffers(count, false);
    }

    private List<VKCommandBuffer> newCommandBuffers(final int count, final boolean isPrimary) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkCommandBufferAllocateInfo createInfo = VkCommandBufferAllocateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
                    .level(isPrimary ? VK10.VK_COMMAND_BUFFER_LEVEL_PRIMARY : VK10.VK_COMMAND_BUFFER_LEVEL_SECONDARY)
                    .commandPool(this.handle)
                    .commandBufferCount(count);

            final PointerBuffer pCommandBuffers = mem.callocPointer(count);
            final int err = VK10.vkAllocateCommandBuffers(deviceHandle, createInfo, pCommandBuffers);

            assert err == VK10.VK_SUCCESS : Tools.vkerr(err);

            final List<VKCommandBuffer> out = new ArrayList<>(count);

            for (int i = 0; i < count; i++) {
                out.add(new VKCommandBuffer(this, pCommandBuffers.get(i), isPrimary));
            }

            return Collections.unmodifiableList(out);
        }
    }   
    
    public VKCommandPoolInfo getInfo() {
        return this.info;
    }
}
