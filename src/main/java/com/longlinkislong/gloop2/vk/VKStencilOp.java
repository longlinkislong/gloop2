/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKStencilOp implements CEnum {
    KEEP(VK10.VK_STENCIL_OP_KEEP),
    ZERO(VK10.VK_STENCIL_OP_ZERO),
    REPLACE(VK10.VK_STENCIL_OP_REPLACE),
    INCREMENT_AND_CLAMP(VK10.VK_STENCIL_OP_INCREMENT_AND_CLAMP),
    DECREMENT_AND_CLAMP(VK10.VK_STENCIL_OP_DECREMENT_AND_CLAMP),
    INVERT(VK10.VK_STENCIL_OP_INVERT),
    INCREMENT_AND_WRAP(VK10.VK_STENCIL_OP_INCREMENT_AND_WRAP),
    DECREMENT_AND_WRAP(VK10.VK_STENCIL_OP_DECREMENT_AND_WRAP);

    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }

    private VKStencilOp(final int value) {
        this.value = value;
    }

    public static Optional<VKStencilOp> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
