/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VkStencilOpState;

public final class VKStencilOpState {

    public final VKStencilOp failOp;
    public final VKStencilOp passOp;
    public final VKStencilOp depthFailOp;
    public final VKCompareOp compareOp;
    public final int compareMask;
    public final int writeMask;
    public final int reference;

    public VKStencilOpState(
            final VKStencilOp failOp, final VKStencilOp passOp, final VKStencilOp depthFailOp,
            final VKCompareOp compareOp,
            final int compareMask, final int writeMask, final int reference) {

        this.failOp = failOp;
        this.passOp = passOp;
        this.depthFailOp = depthFailOp;
        this.compareOp = compareOp;
        this.compareMask = compareMask;
        this.writeMask = writeMask;
        this.reference = reference;
    }

    public VKStencilOpState() {
        this(VKStencilOp.KEEP, VKStencilOp.KEEP, VKStencilOp.KEEP, VKCompareOp.NEVER, 0, 0, 0);
    }

    public VKStencilOpState withFailOp(final VKStencilOp failOp) {
        return new VKStencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public VKStencilOpState withPassOp(final VKStencilOp passOp) {
        return new VKStencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public VKStencilOpState withDepthFailOp(final VKStencilOp depthFailOp) {
        return new VKStencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public VKStencilOpState withCompareOp(final VKCompareOp compareOp) {
        return new VKStencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public VKStencilOpState withCompareMask(final int compareMask) {
        return new VKStencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public VKStencilOpState withWriteMask(final int writeMask) {
        return new VKStencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    public VKStencilOpState withReference(final int reference) {
        return new VKStencilOpState(failOp, passOp, depthFailOp, compareOp, compareMask, writeMask, reference);
    }

    void vkStruct(VkStencilOpState struct) {
        struct.failOp(this.failOp.value)
                .passOp(this.passOp.value)
                .depthFailOp(this.depthFailOp.value)
                .compareOp(this.compareOp.value)
                .compareMask(this.compareMask)
                .writeMask(this.writeMask)
                .reference(this.reference);
    }
}
