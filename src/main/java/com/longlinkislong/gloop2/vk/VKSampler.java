/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkSamplerCreateInfo;

/**
 *
 * @author zmichaels
 */
public class VKSampler implements VKObject {
    long handle;
    VkDevice deviceHandle;
    VKSamplerInfo info;
    
    public VKSampler() {}
    
    public VKSampler(final VKContext ctx, final VKSamplerInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkSamplerCreateInfo createInfo = VkSamplerCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO)
                    .flags(info.flags)
                    .magFilter(info.magFilter.value)
                    .minFilter(info.minFilter.value)
                    .mipmapMode(info.mipmapMode.value)
                    .addressModeU(info.addressModeU.value)
                    .addressModeV(info.addressModeV.value)
                    .addressModeW(info.addressModeW.value)
                    .mipLodBias(info.mipLodBias)
                    .anisotropyEnable(info.anisotropyEnable)
                    .maxAnisotropy(info.maxAnisotropy)
                    .compareEnable(info.compareEnable)
                    .compareOp(info.compareOp.value)
                    .minLod(info.minLod)
                    .maxLod(info.maxLod)
                    .borderColor(info.borderColor.value)
                    .unnormalizedCoordinates(info.unnormalizedCoordinates);
            
            final LongBuffer pSampler = mem.callocLong(1);
            final int err = VK10.vkCreateSampler(ctx.device.handle, createInfo, null, pSampler);
            
            assert Tools.vksuccess(err) : Tools.vkerr(err);
            
            this.handle = pSampler.get(0);
            this.deviceHandle = ctx.device.handle;
            this.info = info;
        }
    }

    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroySampler(deviceHandle, handle, null);
            this.handle = 0L;
            this.deviceHandle = null;
            this.info = null;
        }
    }
    
    public VKSamplerInfo getInfo() {
        return this.info;
    }
    
    public void swap(final VKSampler other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final long tmpHandle = other.handle;
        final VkDevice tmpDev = other.deviceHandle;
        final VKSamplerInfo tmpInfo = other.info;
        
        other.handle = this.handle;
        other.deviceHandle = this.deviceHandle;
        other.info = this.info;
        
        this.handle = tmpHandle;
        this.deviceHandle = tmpDev;
        this.info = tmpInfo;
    }
}
