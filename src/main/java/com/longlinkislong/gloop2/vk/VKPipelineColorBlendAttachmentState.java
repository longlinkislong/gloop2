/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VkPipelineColorBlendAttachmentState;

public final class VKPipelineColorBlendAttachmentState {

    public final boolean blendEnable;
    public final VKBlendFactor srcColorBlendFactor;
    public final VKBlendFactor dstColorBlendFactor;
    public final VKBlendOp colorBlendOp;
    public final VKBlendFactor srcAlphaBlendFactor;
    public final VKBlendFactor dstAlphaBlendFactor;
    public final VKBlendOp alphaBlendOp;
    public final int colorWriteMask;

    public VKPipelineColorBlendAttachmentState(
            final boolean blendEnable,
            final VKBlendFactor srcColorBlendFactor, final VKBlendFactor dstColorBlendFactor, final VKBlendOp colorBlendOp,
            final VKBlendFactor srcAlphaBlendFactor, final VKBlendFactor dstAlphaBlendFactor, final VKBlendOp alphaBlendOp,
            final int colorWriteMask) {

        this.blendEnable = blendEnable;
        this.srcColorBlendFactor = srcColorBlendFactor;
        this.dstColorBlendFactor = dstColorBlendFactor;
        this.colorBlendOp = colorBlendOp;
        this.srcAlphaBlendFactor = srcAlphaBlendFactor;
        this.dstAlphaBlendFactor = dstAlphaBlendFactor;
        this.alphaBlendOp = alphaBlendOp;
        this.colorWriteMask = colorWriteMask;
    }

    public VKPipelineColorBlendAttachmentState() {
        this(false,
                VKBlendFactor.ONE, VKBlendFactor.ZERO, VKBlendOp.ADD,
                VKBlendFactor.ONE, VKBlendFactor.ZERO, VKBlendOp.ADD,
                0xF);
    }

    public VKPipelineColorBlendAttachmentState withBlendEnable(final boolean blendEnable) {
        return new VKPipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    public VKPipelineColorBlendAttachmentState withSrcColorBlendFactor(final VKBlendFactor srcColorBlendFactor) {
        return new VKPipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    public VKPipelineColorBlendAttachmentState withDstColorBlendFactor(final VKBlendFactor dstColorBlendFactor) {
        return new VKPipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    public VKPipelineColorBlendAttachmentState withColorBlendOp(final VKBlendOp colorBlendOp) {
        return new VKPipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    public VKPipelineColorBlendAttachmentState withSrcAlphaBlendFactor(final VKBlendFactor srcAlphaBlendFactor) {
        return new VKPipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    public VKPipelineColorBlendAttachmentState withDstAlphaBlendFactor(final VKBlendFactor dstAlphaBlendFactor) {
        return new VKPipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    public VKPipelineColorBlendAttachmentState withAlphaBlendOp(final VKBlendOp alphaBlendOp) {
        return new VKPipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    public VKPipelineColorBlendAttachmentState withColorWriteMask(final int colorWriteMask) {
        return new VKPipelineColorBlendAttachmentState(blendEnable, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp, srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }
    
    void vkStruct(VkPipelineColorBlendAttachmentState struct) {
        struct.blendEnable(this.blendEnable)
                .srcColorBlendFactor(this.srcColorBlendFactor.value)
                .dstColorBlendFactor(this.dstColorBlendFactor.value)
                .colorBlendOp(this.colorBlendOp.value)
                .srcAlphaBlendFactor(this.srcAlphaBlendFactor.value)
                .dstAlphaBlendFactor(this.dstAlphaBlendFactor.value)
                .alphaBlendOp(this.alphaBlendOp.value)
                .colorWriteMask(this.colorWriteMask);
    }
}
