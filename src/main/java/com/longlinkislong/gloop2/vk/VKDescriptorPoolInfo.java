/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class VKDescriptorPoolInfo {

    public final int flags;
    public final int maxSets;
    public final List<VKDescriptorPoolSize> poolSizes;

    public VKDescriptorPoolInfo(
            final int flags,
            final int maxSets,
            final List<VKDescriptorPoolSize> poolSizes) {

        this.flags = flags;
        this.maxSets = maxSets;
        this.poolSizes = Tools.copyList(poolSizes);
    }
    
    public VKDescriptorPoolInfo() {
        this(0, 0, null);
    }
    
    public VKDescriptorPoolInfo withFlags(final int flags) {
        return new VKDescriptorPoolInfo(flags, maxSets, poolSizes);
    }
    
    public VKDescriptorPoolInfo withMaxSets(final int maxSets) {
        return new VKDescriptorPoolInfo(flags, maxSets, poolSizes);
    }
    
    public VKDescriptorPoolInfo withPoolSizes(final List<VKDescriptorPoolSize> poolSizes) {
        return new VKDescriptorPoolInfo(flags, maxSets, poolSizes);
    }
    
    public VKDescriptorPoolInfo withPoolSizes(final VKDescriptorPoolSize... poolSizes) {
        return withPoolSizes(Arrays.asList(poolSizes));
    }
    
    public VKDescriptorPool create(final VKContext ctx) {
        return new VKDescriptorPool(ctx, this);
    }
}
