/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.Objects;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkBufferCreateInfo;
import org.lwjgl.vulkan.VkDevice;

/**
 * Buffers represent linear arrays of data which are used for various purposes
 * by binding them to a graphics or compute pipeline via descriptor sets or via
 * certain commands, or by directly specifying them as parameters to certain
 * commands.
 * 
 * @author zmichaels
 */
public class VKBuffer implements VKObject {

    VkDevice deviceHandle;
    long handle;
    VKBufferInfo info;

    public VKBuffer() {
    }

    /**
     * Constructs a new VKBuffer object.
     *
     * @param ctx the context.
     * @param info VKBuffer creation information.
     * @throws AssertionError if an unexpected status is returned.
     * @throws VKOutOfHostMemoryException if a host memory allocation failed.
     * @throws VKOutOfDeviceMemoryException if a device memory allocation
     * failed.
     */
    public VKBuffer(final VKContext ctx, final VKBufferInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pQueueFamilyIndices = mem.callocInt(info.queueFamilies.size());

            info.queueFamilies.stream()
                    .filter(Objects::nonNull)
                    .mapToInt(qf -> qf.index)
                    .forEach(pQueueFamilyIndices::put);

            pQueueFamilyIndices.flip();

            final VkBufferCreateInfo createInfo = VkBufferCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO)
                    .flags(VKBufferInfo.CreateFlag.bits(info.flags))
                    .size(info.size)
                    .usage(VKBufferInfo.Usage.bits(info.usage))
                    .sharingMode(info.sharingMode.value)
                    .pQueueFamilyIndices(pQueueFamilyIndices);

            final LongBuffer pBuffer = mem.callocLong(1);
            final int status = VK10.vkCreateBuffer(ctx.device.handle, createInfo, null, pBuffer);

            switch (status) {
                case VK10.VK_SUCCESS:
                    break;
                case VK10.VK_ERROR_OUT_OF_HOST_MEMORY:
                    throw new VKOutOfHostMemoryException();
                case VK10.VK_ERROR_OUT_OF_DEVICE_MEMORY:
                    throw new VKOutOfDeviceMemoryException();
                default:
                    throw new AssertionError(Tools.vkerr(status));
            }

            this.handle = pBuffer.get(0);
            this.info = info;
            this.deviceHandle = ctx.device.handle;
        }
    }

    /**
     * Retrieves the VKBufferInfo object used to create this VKBuffer.
     *
     * @return the VKBufferInfo object. May return null if the VKBuffer is
     * invalid.
     */
    public VKBufferInfo getInfo() {
        return this.info;
    }

    public void swap(final VKBuffer other) {
        if (other == null) {
            this.close();
            return;
        }

        final VkDevice tmpDevHandle = other.deviceHandle;
        final long tmpHandle = other.handle;
        final VKBufferInfo tmpInfo = other.info;

        other.deviceHandle = this.deviceHandle;
        other.handle = this.handle;
        other.info = this.info;

        this.deviceHandle = tmpDevHandle;
        this.handle = tmpHandle;
        this.info = tmpInfo;
    }

    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyBuffer(this.deviceHandle, this.handle, null);
            this.deviceHandle = null;
            this.handle = 0L;
            this.info = null;
        }
    }
}
