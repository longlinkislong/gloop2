/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKImageAspect implements CBitfield {
    COLOR(VK10.VK_IMAGE_ASPECT_COLOR_BIT),
    DEPTH(VK10.VK_IMAGE_ASPECT_DEPTH_BIT),
    STENCIL(VK10.VK_IMAGE_ASPECT_STENCIL_BIT),
    METADATA(VK10.VK_IMAGE_ASPECT_METADATA_BIT);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKImageAspect(final int value) {
        this.value = value;
    }
    
    public static Set<VKImageAspect> flags(final int bits) {
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<VKImageAspect> flags) {
        return Tools.flagsToBitfield(flags);
    }
}
