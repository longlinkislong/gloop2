/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkPipelineCacheCreateInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Pipeline cache objects allow the result of pipeline construction to be reused
 * between pipelines and between runs of an application. Reuse between pipelines
 * is achieved by passing the same pipeline cache object when creating multiple
 * related pipelines. Reuse across runs of an application is achieved by
 * retrieving pipeline cache contents in one run of an application, saving the
 * contents, and using them to preinitialize a pipeline cache on a subsequent
 * run. The contents of the pipeline cache objects are managed by the
 * implementation. Applications can manage the host memory consumed by a
 * pipeline cache object and control the amount of data retrieved from a
 * pipeline cache object.
 *
 * @author zmichaels
 */
public class VKPipelineCache implements VKObject {

    private static final Logger LOGGER = LoggerFactory.getLogger(VKPipelineCache.class);

    long handle;
    VkDevice deviceHandle;
    VKPipelineCacheInfo info;

    /**
     * Creates a new invalid VKPipelineCache. This is only useful with swap.
     */
    public VKPipelineCache() {
    }

    /**
     * Creates a new VKPipelineCache with create info.
     *
     * @param ctx the Vulkan context.
     * @param info the PipelineCache create info.
     */
    public VKPipelineCache(final VKContext ctx, final VKPipelineCacheInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkPipelineCacheCreateInfo createInfo = VkPipelineCacheCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO)
                    .flags(VKPipelineCacheInfo.CreateFlag.bits(info.flags))
                    .pInitialData(info.initialData);

            final LongBuffer pHandle = mem.callocLong(1);
            final int err = VK10.vkCreatePipelineCache(ctx.device.handle, createInfo, null, pHandle);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            this.handle = pHandle.get(0);
            this.deviceHandle = ctx.device.handle;
            this.info = info;
        }
    }

    /**
     * Swaps all resources from this Pipeline cache with another.
     *
     * @param other the other pipeline cache.
     */
    public void swap(final VKPipelineCache other) {
        if (other == null) {
            this.close();
            return;
        }

        final long tmpHandle = other.handle;
        final VkDevice tmpDev = other.deviceHandle;
        final VKPipelineCacheInfo tmpInfo = other.info;

        other.info = this.info;
        other.handle = this.handle;
        other.deviceHandle = this.deviceHandle;

        this.handle = tmpHandle;
        this.deviceHandle = tmpDev;
        this.info = tmpInfo;
    }

    /**
     * Retrieves the maximum size that can be retrieved by the pipeline cache.
     *
     * @return the number of bytes that can be retrieved from the pipeline
     * cache.
     */
    public long getCacheDatasize() {
        assert this.isValid() : "VKPipelineCache is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pDataSize = mem.callocPointer(1);
            final int err = VK10.vkGetPipelineCacheData(this.deviceHandle, this.handle, pDataSize, null);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            return pDataSize.get(0);
        }
    }

    /**
     * Retrieves data from the VKPipelineCache. At most ByteBuffer.remaining
     * bytes will be read. A warning may be recorded if dst is not large enough
     * to contain the entire cache.
     *
     * @param dst the ByteBuffer to write the contents to.
     */
    public void getCacheData(final ByteBuffer dst) {
        assert this.isValid() : "VKPipelineCache is not valid!";
        assert dst.isDirect() : "VKPipelineCache.getCacheData can only write to direct bytebuffer!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pDataSize = mem.pointers(dst.remaining());
            final int err = VK10.vkGetPipelineCacheData(this.deviceHandle, this.handle, pDataSize, dst);

            assert Tools.vksuccess(err) : Tools.vkerr(err);
        }
    }

    /**
     * Performs an operation on the data held by the Pipeline cache. Temporary
     * buffers will be used for storing intermediate data.
     *
     * @param callback operation to perform.
     */
    public void withCacheData(final Consumer<ByteBuffer> callback) {
        assert this.isValid() : "VKPipelineCache is not valid!";

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pDataSize = mem.callocPointer(1);
            int err = VK10.vkGetPipelineCacheData(this.deviceHandle, this.handle, pDataSize, null);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            final ByteBuffer pData = mem.malloc((int) pDataSize.get(0));
            err = VK10.vkGetPipelineCacheData(deviceHandle, handle, pDataSize, pData);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            callback.accept(pData);
        }
    }

    /**
     * Merges at least one other PipelineCache into the destination PipelineCache.
     * This method calls [code]merge(dst, Arrays.asList(srcs))[/code]
     * @param dst the destination Pipeline cache.
     * @param srcs array of at least one source Pipeline cache.
     */
    public static void merge(final VKPipelineCache dst, final VKPipelineCache... srcs) {
        merge(dst, Arrays.asList(srcs));
    }
    
    /**
     * Merges at least one other PipelineCache into the destination PipelineCache.
     * @param dst the destination Pipeline cache.
     * @param srcs a list of at least one source Pipeline caches.
     */
    public static void merge(final VKPipelineCache dst, final List<VKPipelineCache> srcs) {
        assert dst.isValid() : "Destination VKPipelineCache is not valid!";

        if (srcs.isEmpty()) {
            LOGGER.warn("Source cache list is empty!");
            return;
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final LongBuffer pSrcCaches = mem.callocLong(srcs.size());

            srcs.stream()
                    .mapToLong(src -> src.handle)
                    .forEach(pSrcCaches::put);

            pSrcCaches.flip();

            final int res = VK10.vkMergePipelineCaches(dst.deviceHandle, dst.handle, pSrcCaches);

            assert Tools.vksuccess(res) : Tools.vkerr(res);
        }
    }

    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyPipelineCache(deviceHandle, handle, null);
            this.deviceHandle = null;
            this.handle = 0L;
        }
    }
}
