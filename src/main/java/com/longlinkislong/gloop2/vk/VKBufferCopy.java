/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKBufferCopy {
    public final long srcOffset;
    public final long dstOffset;
    public final long size;
    
    public VKBufferCopy(final long srcOffset, final long dstOffset, final long size) {
        this.srcOffset = srcOffset;
        this.dstOffset = dstOffset;
        this.size = size;
    }
    
    public VKBufferCopy() {
        this(0, 0, 0);
    }
    
    public VKBufferCopy withSrcOffset(final long srcOffset) {
        return new VKBufferCopy(srcOffset, dstOffset, size);
    }
    
    public VKBufferCopy withDstOffset(final long dstOffset) {
        return new VKBufferCopy(srcOffset, dstOffset, size);
    }
    
    public VKBufferCopy withSize(final long size) {
        return new VKBufferCopy(srcOffset, dstOffset, size);
    }
}
