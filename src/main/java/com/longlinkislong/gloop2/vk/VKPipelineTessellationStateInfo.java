/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineTessellationStateCreateInfo;

/**
 *
 * @author zmichaels
 */
public class VKPipelineTessellationStateInfo {

    public final int flags;
    public final int patchControlPoints;

    public VKPipelineTessellationStateInfo(final int flags, final int patchControlPoints) {
        this.flags = flags;
        this.patchControlPoints = patchControlPoints;
    }

    public VKPipelineTessellationStateInfo() {
        this(0, 0);
    }
    
    public VKPipelineTessellationStateInfo withFlags(final int flags) {
        return new VKPipelineTessellationStateInfo(flags, patchControlPoints);
    }

    public VKPipelineTessellationStateInfo withPatchControlPoints(final int patchControlPoints) {
        return new VKPipelineTessellationStateInfo(flags, patchControlPoints);
    }
    
    void vkStruct(final VkPipelineTessellationStateCreateInfo struct) {
        struct.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO)
                .flags(this.flags)
                .patchControlPoints(this.patchControlPoints);
    }
}
