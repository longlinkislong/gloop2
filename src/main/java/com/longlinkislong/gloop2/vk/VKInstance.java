/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWVulkan;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.EXTDebugReport;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkApplicationInfo;
import org.lwjgl.vulkan.VkDebugReportCallbackCreateInfoEXT;
import org.lwjgl.vulkan.VkDebugReportCallbackEXT;
import org.lwjgl.vulkan.VkInstance;
import org.lwjgl.vulkan.VkInstanceCreateInfo;
import org.lwjgl.vulkan.VkPhysicalDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class VKInstance implements VKObject {
    private static final Logger LOGGER = LoggerFactory.getLogger(VKInstance.class);
    
    static {
        try {
            assert false;
        } catch (AssertionError err) {
            LOGGER.trace("Assertions are enabled!");
        }
    }
    
    private static final Semaphore INSTANCE_LOCK = new Semaphore(1);
    private static VKInstance INSTANCE;
    
    public static VKInstance getInstance() {
        try {
            INSTANCE_LOCK.acquire();            
            
            return INSTANCE;
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        } finally {
            INSTANCE_LOCK.release();
        }
    }
    
    final VkInstance handle;
    final List<VKPhysicalDevice> physicalDevices;
    final long debugCallback;
    
    
    public Stream<VKPhysicalDevice> physicalDevices() {
        return this.physicalDevices.stream();
    }
    
    public VKInstance(final VKInstanceCreateInfo createInfo) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkApplicationInfo appInfo = VkApplicationInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_APPLICATION_INFO)
                    .pApplicationName(mem.UTF8(createInfo.applicationName))
                    .pEngineName(mem.UTF8(createInfo.engineName))
                    .apiVersion(VK10.VK_MAKE_VERSION(createInfo.versionMajor, createInfo.versionMinor, createInfo.versionPatch));            
                        
            if (!GLFW.glfwInit()) {
                throw new RuntimeException("Unable to initialize GLFW!");
            }
            
            final PointerBuffer glfwReqExt = GLFWVulkan.glfwGetRequiredInstanceExtensions();            
            final PointerBuffer ppEnabledExtensions = mem.callocPointer(glfwReqExt.remaining() + createInfo.extensions.size());
            
            ppEnabledExtensions.put(glfwReqExt);
            createInfo.extensions.stream().map(mem::UTF8).forEach(ppEnabledExtensions::put);
            ppEnabledExtensions.flip();
            
            final PointerBuffer ppEnabledLayers = mem.callocPointer(createInfo.layers.size());
            
            createInfo.layers.stream().map(mem::UTF8).forEach(ppEnabledLayers::put);
            ppEnabledLayers.flip();
            
            final VkInstanceCreateInfo pCreateInfo = VkInstanceCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO)
                    .pApplicationInfo(appInfo)
                    .ppEnabledExtensionNames(ppEnabledExtensions)
                    .ppEnabledLayerNames(ppEnabledLayers);
            
            final PointerBuffer pInstance = mem.callocPointer(1);
            int err = VK10.vkCreateInstance(pCreateInfo, null, pInstance);
            
            assert err == VK10.VK_SUCCESS : Tools.vkerr(err);
            
            this.handle = new VkInstance(pInstance.get(0), pCreateInfo);            
        }
        
        try {
            INSTANCE_LOCK.acquire();
            INSTANCE = this;
            LOGGER.trace("Successfully registered Vulkan Instance!");
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        } finally {
            INSTANCE_LOCK.release();
        }
        
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pPhysicalDeviceCount = mem.callocInt(1);
            int err = VK10.vkEnumeratePhysicalDevices(handle, pPhysicalDeviceCount, null);
            
            assert err == VK10.VK_SUCCESS : Tools.vkerr(err);
            
            final PointerBuffer pPhysicalDevices = mem.callocPointer(pPhysicalDeviceCount.get(0));
            err = VK10.vkEnumeratePhysicalDevices(handle, pPhysicalDeviceCount, pPhysicalDevices);
            
            assert err == VK10.VK_SUCCESS : Tools.vkerr(err);            
            
            this.physicalDevices = IntStream.range(0, pPhysicalDeviceCount.get(0))
                    .mapToLong(pPhysicalDevices::get)
                    .mapToObj(physicalDevice -> new VkPhysicalDevice(physicalDevice, handle))
                    .map(VKPhysicalDevice::new)
                    .collect(Collectors.toList());            
        }
        
        if (createInfo.extensions.contains(EXTDebugReport.VK_EXT_DEBUG_REPORT_EXTENSION_NAME)) {
            LOGGER.trace("Initializing EXT_debug_report");
            
            try (MemoryStack mem = MemoryStack.stackPush()) {
                final VkDebugReportCallbackCreateInfoEXT dbgInfo = VkDebugReportCallbackCreateInfoEXT.callocStack(mem)
                        .sType(EXTDebugReport.VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT)
                        .pfnCallback((flags, objType, obj, loc, msgCode, pLayerPrefix, pMessage, pUserData) -> {
                            LOGGER.info("{}", VkDebugReportCallbackEXT.getString(pMessage));
                            return 0;
                        })
                        .flags(EXTDebugReport.VK_DEBUG_REPORT_ERROR_BIT_EXT | EXTDebugReport.VK_DEBUG_REPORT_WARNING_BIT_EXT);
                
                final LongBuffer pCallback = mem.callocLong(1);
                final int err = EXTDebugReport.vkCreateDebugReportCallbackEXT(handle, dbgInfo, null, pCallback);
                
                assert Tools.vksuccess(err) : Tools.vkerr(err);
                
                this.debugCallback = pCallback.get(0);
            }
        } else {
            this.debugCallback = 0L;
        }
    }        

    @Override
    public boolean isValid() {
        return !this.physicalDevices.isEmpty();
    }

    @Override
    public void close() {
        if (this.isValid()) {
            if (this.debugCallback != 0L) {                
                EXTDebugReport.vkDestroyDebugReportCallbackEXT(handle, debugCallback, null);                
                LOGGER.trace("Successfully closed EXT_debug_report!");
            }
            
            VK10.vkDestroyInstance(this.handle, null);
            this.physicalDevices.clear();
            LOGGER.trace("Successfully closed Vulkan Instance!");
        }
    }
}
