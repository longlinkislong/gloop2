/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public class VKCommandBufferBeginInfo {
    public static final VKCommandBufferBeginInfo PRIMARY_ONE_TIME_USE = new VKCommandBufferBeginInfo()
            .withFlags(Usage.ONE_TIME_SUBMIT);        
    
    public static enum Usage implements CBitfield {
        ONE_TIME_SUBMIT(VK10.VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT),
        RENDER_PASS_CONTINUE(VK10.VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT),
        SIMULTANEOUS_USE(VK10.VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT);
        
        public final int value;
        
        @Override
        public int getValue() {
            return this.value;
        }
        
        private Usage(final int value) {
            this.value = value;
        }
        
        public static Set<Usage> flags(final int bits) {
            return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
        }
        
        public static int bits(final Set<Usage> flags) {
            return Tools.flagsToBitfield(flags);
        }
    }
    
    public static final class InheritanceInfo {

        public final VKRenderPass renderPass;
        public final int subpass;
        public final VKFramebuffer framebuffer;
        //TODO add the other contents
        
        public InheritanceInfo() {
            this(null, 0, null);
        }
        
        public InheritanceInfo(final VKRenderPass renderPass, final int subpass, final VKFramebuffer framebuffer) {
            this.renderPass = renderPass;
            this.subpass = subpass;
            this.framebuffer = framebuffer;
        }
        
        public InheritanceInfo withRenderPass(final VKRenderPass renderPass) {
            return new InheritanceInfo(renderPass, subpass, framebuffer);
        }
        
        public InheritanceInfo withSubpass(final int subpass) {
            return new InheritanceInfo(renderPass, subpass, framebuffer);
        }
        
        public InheritanceInfo withFramebuffer(final VKFramebuffer framebuffer) {
            return new InheritanceInfo(renderPass, subpass, framebuffer);
        }
    }
    
    public final Set<Usage> flags;
    public final InheritanceInfo inheritanceInfo;
    
    public VKCommandBufferBeginInfo() {
        this(null, null);
    }
    
    public VKCommandBufferBeginInfo(
            final Collection<Usage> flags,
            final InheritanceInfo inheritanceInfo) {
        
        this.flags = Tools.copySet(flags);
        this.inheritanceInfo = inheritanceInfo;
    }
    
    public VKCommandBufferBeginInfo withFlags(final Collection<Usage> flags) {
        return new VKCommandBufferBeginInfo(flags, inheritanceInfo);
    }
    
    public VKCommandBufferBeginInfo withFlags(final Usage... flags) {
        return withFlags(Arrays.asList(flags));
    }
    
    public VKCommandBufferBeginInfo withInheritanceInfo(final InheritanceInfo inheritanceInfo) {
        return new VKCommandBufferBeginInfo(flags, inheritanceInfo);
    }
}
