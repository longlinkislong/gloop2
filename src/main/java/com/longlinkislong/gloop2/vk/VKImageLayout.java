/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.KHRSwapchain;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKImageLayout implements CEnum {
    UNDEFINED(VK10.VK_IMAGE_LAYOUT_UNDEFINED),
    GENERAL(VK10.VK_IMAGE_LAYOUT_GENERAL),
    COLOR_ATTACHMENT_OPTIMAL(VK10.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL),
    DEPTH_STENCIL_ATTACHMENT_OPTIMAL(VK10.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL),
    DEPTH_STENCIL_READ_ONLY_OPTIMAL(VK10.VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL),
    SHADER_READ_ONLY_OPTIMAL(VK10.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL),
    TRANSFER_SRC_OPTIMAL(VK10.VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL),
    TRANSFER_DST_OPTIMAL(VK10.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL),
    PREINITIALIZED(VK10.VK_IMAGE_LAYOUT_PREINITIALIZED),
    PRESENT_SRC(KHRSwapchain.VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKImageLayout(final int value) {
        this.value = value;
    }
    
    public static Optional<VKImageLayout> valueOf(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
