/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author zmichaels
 */
public class VKDeviceCreateInfo {

    public static final class DeviceQueueCreateInfo {

        public final VKQueueFamily queueFamily;
        public final List<Float> queuePriorities;

        public DeviceQueueCreateInfo(
                final VKQueueFamily queueFamily,
                final List<Float> queuePriorities) {

            this.queueFamily = queueFamily;
            this.queuePriorities = (queuePriorities == null || queuePriorities.isEmpty())
                    ? Collections.unmodifiableList(Arrays.asList(0.0F))
                    : Collections.unmodifiableList(new ArrayList<>(queuePriorities));
        }

        public DeviceQueueCreateInfo() {
            this(null, null);
        }

        public DeviceQueueCreateInfo withQueueFamily(final VKQueueFamily queueFamily) {
            return new DeviceQueueCreateInfo(queueFamily, queuePriorities);
        }

        public DeviceQueueCreateInfo withQueuePriorities(final List<Float> queuePriorities) {
            return new DeviceQueueCreateInfo(queueFamily, queuePriorities);
        }

        public DeviceQueueCreateInfo withQueuePriorities(final float... queuePriorities) {
            return new DeviceQueueCreateInfo(queueFamily, IntStream.range(0, queuePriorities.length)
                    .mapToObj(i -> queuePriorities[i])
                    .collect(Collectors.toList()));
        }
    }

    public final List<DeviceQueueCreateInfo> queueCreateInfos;
    public final Set<String> extensions;
    public final Set<String> layers;

    public VKDeviceCreateInfo() {
        this(null, null, null);
    }
    
    public VKDeviceCreateInfo(
            final List<DeviceQueueCreateInfo> queueCreateInfos,
            final Collection<String> extensions,
            final Collection<String> layers) {

        this.queueCreateInfos = Tools.copyList(queueCreateInfos);
        this.extensions = Tools.copySet(extensions);
        this.layers = Tools.copySet(layers);
    }
    
    public VKDeviceCreateInfo withQueueCreateInfos(final List<DeviceQueueCreateInfo> queueCreateInfos) {
        return new VKDeviceCreateInfo(queueCreateInfos, extensions, layers);
    }
    
    public VKDeviceCreateInfo withExtensions(final Collection<String> extensions) {
        return new VKDeviceCreateInfo(queueCreateInfos, extensions, layers);
    }
    
    public VKDeviceCreateInfo withLayers(final Collection<String> layers) {
        return new VKDeviceCreateInfo(queueCreateInfos, extensions, layers);
    }
    
    public VKDeviceCreateInfo withQueueCreateInfos(final DeviceQueueCreateInfo... queueCreateInfos) {
        return new VKDeviceCreateInfo(Arrays.asList(queueCreateInfos), extensions, layers);
    }
    
    public VKDeviceCreateInfo withExtensions(final String... extensions) {
        return new VKDeviceCreateInfo(queueCreateInfos, Arrays.asList(extensions), layers);
    }
    
    public VKDeviceCreateInfo withLayers(final String... layers) {
        return new VKDeviceCreateInfo(queueCreateInfos, extensions, Arrays.asList(layers));
    }
    
    public VKDevice create() {
        return new VKDevice(this);
    }
}
