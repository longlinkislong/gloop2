/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public class VKCommandPoolInfo {
    public static enum CreateFlag implements CBitfield {
        TRANSIENT(VK10.VK_COMMAND_POOL_CREATE_TRANSIENT_BIT),
        RESET_COMMAND_BUFFER(VK10.VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);
        
        public final int value;
        
        @Override
        public int getValue() {
            return this.value;
        }
        
        private CreateFlag(final int value) {
            this.value = value;
        }
        
        public static int bits(final Set<CreateFlag> flags) {
            return Tools.flagsToBitfield(flags);
        }
        
        public static Set<CreateFlag> flags(final int bits) {
            return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
        }
    } 
    
    public final VKQueueFamily queueFamily;
    public final Set<CreateFlag> flags;
    
    public VKCommandPoolInfo(final Collection<CreateFlag> flags, final VKQueueFamily queueFamily) {
        this.flags = Tools.copySet(flags);
        this.queueFamily = queueFamily;
    }
    
    public VKCommandPoolInfo() {
        this(null, null);
    }
    
    public VKCommandPoolInfo withFlags(final Collection<CreateFlag> flags) {
        return new VKCommandPoolInfo(flags, queueFamily);
    }
    
    public VKCommandPoolInfo withFlags(final CreateFlag... flags) {
        return withFlags(Arrays.asList(flags));
    }
    
    public VKCommandPoolInfo withQueueFamily(final VKQueueFamily queueFamily) {
        return new VKCommandPoolInfo(flags, queueFamily);
    }        
    
    public VKCommandPool create(final VKContext ctx) {
        return new VKCommandPool(ctx, this);
    }
}
