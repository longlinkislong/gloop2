/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKSamplerInfo {

    public final int flags;
    public final VKFilter magFilter;
    public final VKFilter minFilter;
    public final VKSamplerMipmapMode mipmapMode;
    public final VKSamplerAddressMode addressModeU;
    public final VKSamplerAddressMode addressModeV;
    public final VKSamplerAddressMode addressModeW;
    public final float mipLodBias;
    public final boolean anisotropyEnable;
    public final float maxAnisotropy;
    public final boolean compareEnable;
    public final VKCompareOp compareOp;
    public final float minLod;
    public final float maxLod;
    public final VKBorderColor borderColor;
    public final boolean unnormalizedCoordinates;

    public VKSamplerInfo(
            final int flags,
            final VKFilter magFilter, final VKFilter minFilter,
            final VKSamplerMipmapMode mipmapMode,
            final VKSamplerAddressMode addressModeU, final VKSamplerAddressMode addressModeV, final VKSamplerAddressMode addressModeW,
            final float mipLodBias,
            final boolean anisotropyEnable, final float maxAnisotropy,
            final boolean compareEnable, final VKCompareOp compareOp,
            final float minLod, final float maxLod,
            final VKBorderColor borderColor,
            final boolean unnormalizedCoordinates) {

        this.flags = flags;
        this.magFilter = magFilter;
        this.minFilter = minFilter;
        this.mipmapMode = mipmapMode;
        this.addressModeU = addressModeU;
        this.addressModeV = addressModeV;
        this.addressModeW = addressModeW;
        this.mipLodBias = mipLodBias;
        this.anisotropyEnable = anisotropyEnable;
        this.maxAnisotropy = maxAnisotropy;
        this.compareEnable = compareEnable;
        this.compareOp = compareOp;
        this.minLod = minLod;
        this.maxLod = maxLod;
        this.borderColor = borderColor;
        this.unnormalizedCoordinates = unnormalizedCoordinates;
    }

    public VKSamplerInfo() {
        this(0, VKFilter.NEAREST, VKFilter.NEAREST,
                VKSamplerMipmapMode.NEAREST,
                VKSamplerAddressMode.REPEAT, VKSamplerAddressMode.REPEAT, VKSamplerAddressMode.REPEAT,
                0.0F,
                false, 0.0F,
                false, VKCompareOp.NEVER,
                0.0F, 0.0F,
                VKBorderColor.FLOAT_TRANSPARENT_BLACK,
                false);
    }

    public VKSamplerInfo withFlags(final int flags) {
        return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withMagFilter(final VKFilter magFilter) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withMinFilter(final VKFilter minFilter) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withMipmapMode(final VKSamplerMipmapMode mipmapMode) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withAddressModeU(final VKSamplerAddressMode addressModeU) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withAddressModeV(final VKSamplerAddressMode addressModeV) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withAddressModeW(final VKSamplerAddressMode addressModeW) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withMipLodBias(final float mipLodBias) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withAnisotropyEnable(final boolean anisotropyEnable) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withMaxAnisotropy(final float maxAnisotropy) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withCompareEnable(final boolean compareEnable) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withCompareOp(final VKCompareOp compareOp) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withMinLod(final float minLod) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withMaxLod(final float maxLod) {
         return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withBorderColor(final VKBorderColor borderColor) {
        return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSamplerInfo withUnnormalizedCoordinates(final boolean unnormalizedCoordinates) {
        return new VKSamplerInfo(flags, magFilter, minFilter, mipmapMode,
                addressModeU, addressModeV, addressModeW,
                mipLodBias,
                anisotropyEnable, maxAnisotropy,
                compareEnable, compareOp,
                minLod, maxLod,
                borderColor,
                unnormalizedCoordinates);
    }
    
    public VKSampler create(final VKContext ctx) {
        return new VKSampler(ctx, this);
    }
}
