/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKCullMode implements CEnum {
    NONE(VK10.VK_CULL_MODE_NONE),
    FRONT(VK10.VK_CULL_MODE_FRONT_BIT),
    BACK(VK10.VK_CULL_MODE_BACK_BIT),
    FRONT_AND_BACK(VK10.VK_CULL_MODE_FRONT_AND_BACK);

    public final int value;

    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKCullMode(final int value) {
        this.value = value;
    }

    public static Optional<VKCullMode> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
