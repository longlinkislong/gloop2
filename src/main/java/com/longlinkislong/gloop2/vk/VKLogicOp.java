/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKLogicOp implements CEnum {
    CLEAR(VK10.VK_LOGIC_OP_CLEAR),
    AND(VK10.VK_LOGIC_OP_AND),
    AND_REVERSE(VK10.VK_LOGIC_OP_AND_REVERSE),
    COPY(VK10.VK_LOGIC_OP_COPY),
    AND_INVERTED(VK10.VK_LOGIC_OP_AND_INVERTED),
    NO_OP(VK10.VK_LOGIC_OP_NO_OP),
    XOR(VK10.VK_LOGIC_OP_XOR),
    OR(VK10.VK_LOGIC_OP_OR),
    NOR(VK10.VK_LOGIC_OP_NOR),
    EQUIVALENT(VK10.VK_LOGIC_OP_EQUIVALENT),
    INVERT(VK10.VK_LOGIC_OP_INVERT),
    OR_REVERSE(VK10.VK_LOGIC_OP_OR_REVERSE),
    COPY_INVERTED(VK10.VK_LOGIC_OP_COPY_INVERTED),
    OR_INVERTED(VK10.VK_LOGIC_OP_OR_INVERTED),
    NAND(VK10.VK_LOGIC_OP_NAND),
    SET(VK10.VK_LOGIC_OP_SET);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKLogicOp(final int value) {
        this.value = value;
    }
    
    public static Optional<VKLogicOp> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
