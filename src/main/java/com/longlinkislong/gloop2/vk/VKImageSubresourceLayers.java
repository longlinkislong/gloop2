/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class VKImageSubresourceLayers {

    public final Set<VKImageAspect> aspectMask;
    public final int mipLevel;
    public final int baseArrayLayer;
    public final int layerCount;

    public VKImageSubresourceLayers(
            final Collection<VKImageAspect> aspectMask,
            final int mipLevel,
            final int baseArrayLayer, final int layerCount) {

        this.aspectMask = Tools.copySet(aspectMask);
        this.mipLevel = mipLevel;
        this.baseArrayLayer = baseArrayLayer;
        this.layerCount = layerCount;
    }
    
    public VKImageSubresourceLayers() {
        this(null, 0, 0, 0);
    }
    
    public VKImageSubresourceLayers withAspectMask(final Collection<VKImageAspect> aspectMask) {
        return new VKImageSubresourceLayers(aspectMask, mipLevel, baseArrayLayer, layerCount);
    }
    
    public VKImageSubresourceLayers withAspectMask(final VKImageAspect... aspectMask) {
        return withAspectMask(Arrays.asList(aspectMask));
    }
    
    public VKImageSubresourceLayers withBaseArrayLayer(final int baseArrayLayer) {
        return new VKImageSubresourceLayers(aspectMask, mipLevel, baseArrayLayer, layerCount);
    }
    
    public VKImageSubresourceLayers withMipLevel(final int mipLevel) {
        return new VKImageSubresourceLayers(aspectMask, mipLevel, baseArrayLayer, layerCount);
    }
    
    public VKImageSubresourceLayers withLayerCount(final int layerCount) {
        return new VKImageSubresourceLayers(aspectMask, mipLevel, baseArrayLayer, layerCount);
    }
}
