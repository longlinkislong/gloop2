/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VkRect2D;

/**
 *
 * @author zmichaels
 */
public class VKScissor {
    public final int x;
    public final int y;
    public final int width;
    public final int height;
    
    public VKScissor(
    final int x, final int y,
            final int width, final int height) {
        
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    public VKScissor() {
        this(0, 0, 0, 0);
    }
    
    public VKScissor withOffset(final int x, final int y) {
        return new VKScissor(x, y, width, height);
    }
    
    public VKScissor withExtent(final int width, final int height) {
        return new VKScissor(x, y, width, height);
    }
    
    void apply(VkRect2D r) {
        r.offset().x(x).y(y);
        r.extent().width(width).height(height);
    }
}
