/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class VKDescriptorSetLayoutInfo {

    public static final class Binding {

        public final int binding;
        public final VKDescriptorType descriptorType;
        public final int descriptorCount;
        public final Set<VKShaderStage> stageFlags;
        public final List<VKSampler> immutableSamplers;

        public Binding(
                final int binding,
                final VKDescriptorType descriptorType,
                final int descriptorCount,
                final Collection<VKShaderStage> stageFlags,
                final List<VKSampler> immutableSamplers) {

            this.binding = binding;
            this.descriptorType = descriptorType;
            this.descriptorCount = descriptorCount;
            this.stageFlags = Tools.copySet(stageFlags);
            this.immutableSamplers = Tools.copyList(immutableSamplers);
        }
        
        public Binding() {
            this(0, VKDescriptorType.SAMPLER, 0, null, null);
        }
        
        public Binding withBinding(final int binding) {
            return new Binding(binding, descriptorType, descriptorCount, stageFlags, immutableSamplers);
        }
        
        public Binding withDescriptorType(final VKDescriptorType descriptorType) {
            return new Binding(binding, descriptorType, descriptorCount, stageFlags, immutableSamplers);
        }
        
        public Binding withDescriptorCount(final int descriptorCount) {
            return new Binding(binding, descriptorType, descriptorCount, stageFlags, immutableSamplers);
        }
        
        public Binding withStageFlags(final Collection<VKShaderStage> stageFlags) {
            return new Binding(binding, descriptorType, descriptorCount, stageFlags, immutableSamplers);
        }
        
        public Binding withStageFlags(final VKShaderStage... stageFlags) {
            return withStageFlags(Arrays.asList(stageFlags));
        }
        
        public Binding withImmutableSamplers(final List<VKSampler> immutableSamplers) {
            return new Binding(binding, descriptorType, descriptorCount, stageFlags, immutableSamplers);
        }
        
        public Binding withImmutableSamplers(final VKSampler... immutableSamplers) {
            return withImmutableSamplers(Arrays.asList(immutableSamplers));
        }
    }
    public final int flags;    
    public final List<Binding> bindings;
    
    public VKDescriptorSetLayoutInfo(final int flags, final List<Binding> bindings) {
        this.flags = flags;
        this.bindings = Tools.copyList(bindings);
    }
    
    public VKDescriptorSetLayoutInfo() {
        this(0, null);
    }
    
    public VKDescriptorSetLayoutInfo withFlags(final int flags) {
        return new VKDescriptorSetLayoutInfo(flags, bindings);
    }
    
    public VKDescriptorSetLayoutInfo withBindings(final List<Binding> bindings) {
        return new VKDescriptorSetLayoutInfo(flags, bindings);
    }
    
    public VKDescriptorSetLayoutInfo withBindings(final Binding... bindings) {
        return withBindings(Arrays.asList(bindings));
    }
    
    public VKDescriptorSetLayout create(final VKContext ctx) {
        return new VKDescriptorSetLayout(ctx, this);
    }
}
