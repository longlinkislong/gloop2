/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkSemaphoreCreateInfo;

/**
 *
 * @author zmichaels
 */
public class VKSemaphore implements VKObject {

    VkDevice deviceHandle;
    long handle;

    public VKSemaphore(){}
    
    public VKSemaphore(final VKContext ctx) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkSemaphoreCreateInfo createInfo = VkSemaphoreCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO);

            final LongBuffer pHandle = mem.callocLong(1);
            final int err = VK10.vkCreateSemaphore(ctx.device.handle, createInfo, null, pHandle);

            assert err == VK10.VK_SUCCESS : Tools.vkerr(err);

            this.handle = pHandle.get(0);
        }
    }
    
    public void swap(final VKSemaphore other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final VkDevice tmpDev = other.deviceHandle;
        final long tmpHandle = other.handle;
        
        other.deviceHandle = this.deviceHandle;
        other.handle = this.handle;
        
        this.deviceHandle = tmpDev;
        this.handle = tmpHandle;
    }

    @Override
    public boolean isValid() {
        return this.handle > 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroySemaphore(deviceHandle, handle, null);
            this.deviceHandle = null;
            this.handle = 0L;
        }
    }

}
