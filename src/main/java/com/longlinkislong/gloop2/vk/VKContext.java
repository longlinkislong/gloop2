/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.glfw.GLFWWindow;
import com.longlinkislong.gloop2.memory.VKMemoryManager;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.lwjgl.glfw.GLFWVulkan;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.KHRSurface;
import org.lwjgl.vulkan.KHRSwapchain;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkExtent2D;
import org.lwjgl.vulkan.VkFormatProperties;
import org.lwjgl.vulkan.VkPresentInfoKHR;
import org.lwjgl.vulkan.VkSubmitInfo;
import org.lwjgl.vulkan.VkSurfaceCapabilitiesKHR;
import org.lwjgl.vulkan.VkSurfaceFormatKHR;
import org.lwjgl.vulkan.VkSwapchainCreateInfoKHR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class VKContext implements VKObject {

    private static final Logger LOGGER = LoggerFactory.getLogger(VKContext.class);

    int width = -1;
    int height = -1;
    public final VKFormat colorFormat;
    final int colorSpace;
    public final VKFormat depthFormat;

    public final VKDevice device;
    long surface;
    public final VKQueue mainQueue;
    public final VKQueue presentQueue;

    long swapchain;

    public static final boolean ASYNC_PRESENT = Boolean.getBoolean("com.longlinkislong.gloop2.vk.VKContext.async_present");
    public static final VKFormat PREFERRED_DEPTH_FORMAT = VKFormat.valueOf(System.getProperty("com.longlinkislong.gloop2.vk.VKContext.preferred_depth_format", VKFormat.D24_UNORM_S8_UINT.name()));
    public static final int BACKBUFFER_COUNT = Integer.getInteger("com.longlinkislong.gloop2.vk.VKContext.back_buffer_count", 3);

    final Queue<VKBackBuffer> backBuffers = new ArrayDeque<>();

    final ContextFrameData[] frameData = new ContextFrameData[8];
    int imageCount = BACKBUFFER_COUNT;

    public final VKCommandPool primaryCommandPool;
    VKBackBuffer acquiredBackBuffer;
    public static final boolean VSYNC = Boolean.getBoolean("com.longlinkislong.gloop2.vk.VKContext.vsync");

    public double getAspectRatio() {
        return (double) this.width / (double) this.height;
    }

    private boolean vsync = VSYNC;
    Runnable onSwapchainDestroy;
    Runnable onSwapchainCreate;
    VKImageView depthStencil;
    List<VKImageView> swapchainImages;
    List<VKFramebuffer> framebuffers;
    public final VKMemoryManager memoryManager;
    final Set<VKRenderPass> dummyRenderPasses = new HashSet<>();
    
    public ByteBuffer[] clearValues(final float r, final float g, final float b, final float a, final float d, final int s) {
        return new ByteBuffer[] {Tools.clearColor(r, g, b, a), Tools.clearDepthStencil(d, s)};
    }

    public VKContext(final VKDeviceCreateInfo createInfo, final GLFWWindow window) {
        if (createInfo == null) {
            final VKPhysicalDevice selectedPhysicalDevice = VKInstance.getInstance()
                    .physicalDevices()
                    .findFirst()
                    .orElseThrow(() -> new UnsupportedOperationException("No Vulkan devices were found!"));

            this.device = new VKDeviceCreateInfo()
                    .withQueueCreateInfos(new VKDeviceCreateInfo.DeviceQueueCreateInfo()
                            .withQueueFamily(selectedPhysicalDevice.queueFamilies()
                                    .filter(VKQueueFamily::canDraw)
                                    .filter(VKQueueFamily::canPresent)
                                    .findFirst()
                                    .orElseThrow(() -> new UnsupportedOperationException("No Queue Families support draw and present!"))))
                    .create();
        } else {
            this.device = createInfo.create();
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final LongBuffer pSurface = mem.callocLong(1);
            int err = GLFWVulkan.glfwCreateWindowSurface(VKInstance.getInstance().handle, window.handle, null, pSurface);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            this.surface = pSurface.get(0);

            final IntBuffer pFormatCount = mem.callocInt(1);

            err = KHRSurface.vkGetPhysicalDeviceSurfaceFormatsKHR(device.physicalDevice.handle, surface, pFormatCount, null);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            final int formatCount = pFormatCount.get(0);

            final VkSurfaceFormatKHR.Buffer surfaceFormats = VkSurfaceFormatKHR.callocStack(formatCount, mem);

            err = KHRSurface.vkGetPhysicalDeviceSurfaceFormatsKHR(device.physicalDevice.handle, surface, pFormatCount, surfaceFormats);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            if (formatCount == 1 && surfaceFormats.get(0).format() == VK10.VK_FORMAT_UNDEFINED) {
                this.colorFormat = VKFormat.B8G8R8A8_UNORM;
            } else {
                this.colorFormat = VKFormat.of(surfaceFormats.get(0).format()).get();
            }

            this.colorSpace = surfaceFormats.get(0).colorSpace();
        }

        this.mainQueue = this.device.queues()
                .filter(q -> q.family.canCompute() && q.family.canDraw())
                .findFirst()
                .orElseThrow(() -> new UnsupportedOperationException("No generic purpose queue!"));

        if (ASYNC_PRESENT) {
            this.presentQueue = this.device.queues()
                    .filter(q -> q.family.canPresent() && q != this.mainQueue)
                    .findFirst()
                    .orElse(this.mainQueue);
        } else {
            this.presentQueue = this.device.queues()
                    .filter(q -> q.family.canPresent())
                    .findFirst()
                    .orElseThrow(() -> new UnsupportedOperationException("Present not supported!"));
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final int[] depthFormats = {
                VK10.VK_FORMAT_D32_SFLOAT_S8_UINT,
                VK10.VK_FORMAT_D32_SFLOAT,
                VK10.VK_FORMAT_D24_UNORM_S8_UINT,
                VK10.VK_FORMAT_D16_UNORM_S8_UINT,
                VK10.VK_FORMAT_D16_UNORM};

            final VkFormatProperties pFormatProperties = VkFormatProperties.callocStack(mem);
            int selectedDepthFormat = PREFERRED_DEPTH_FORMAT.value;

            VK10.vkGetPhysicalDeviceFormatProperties(device.physicalDevice.handle, selectedDepthFormat, pFormatProperties);

            // if the preferred depth format does not support tiling...
            if ((pFormatProperties.optimalTilingFeatures() & VK10.VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) == 0) {
                for (int testDepthFormat : depthFormats) {
                    VK10.vkGetPhysicalDeviceFormatProperties(device.physicalDevice.handle, testDepthFormat, pFormatProperties);

                    if ((pFormatProperties.optimalTilingFeatures() & VK10.VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) != 0) {
                        selectedDepthFormat = testDepthFormat;
                        break;
                    }
                }
            }

            this.depthFormat = VKFormat.of(selectedDepthFormat).get();

            LOGGER.trace("Selected {} as depth format!", depthFormat);
        }

        this.memoryManager = new VKMemoryManager(
                this.device.physicalDevice.handle,
                this.device.physicalDevice.properties,
                this.device.handle,
                256 * 1024 * 1024,
                512 * 1024 * 1024);

        LOGGER.info("Successfully created VKContext!");

        this.primaryCommandPool = new VKCommandPoolInfo()
                .withQueueFamily(this.mainQueue.family)
                .withFlags(VKCommandPoolInfo.CreateFlag.RESET_COMMAND_BUFFER)
                .create(this);

        //this.createBackBuffers();
        this.createSwapchain();
        this.createFramedata();
    }

    private void createFramedata() {
        for (int i = 0; i < this.frameData.length; i++) {
            final VKFence fence = new VKFence(this, EnumSet.of(VKFence.CreateFlag.CREATE_SIGNALED_BIT));
            final VKCommandBuffer cmdbuf = this.primaryCommandPool.newPrimaryCommandBuffer();

            this.frameData[i] = new ContextFrameData(fence, cmdbuf);
        }
    }

    private void createSwapchain() {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pSupported = mem.callocInt(1);
            int err = KHRSurface.vkGetPhysicalDeviceSurfaceSupportKHR(device.physicalDevice.handle, presentQueue.family.index, surface, pSupported);

            assert Tools.vksuccess(err) : Tools.vkerr(err);
            assert pSupported.get(0) == VK10.VK_TRUE;

            this.swapchain = 0L;
            this.width = -1;
            this.height = -1;
        }
    }

    private void createBackBuffers() {
        for (int i = 0; i < this.imageCount; i++) {
            this.backBuffers.offer(new VKBackBuffer(this));
        }
        LOGGER.trace("Created {} backbuffer(s)!", this.backBuffers.size());
    }

    private void destroySwapchain() {
        if (this.swapchain != 0L) {
            Optional.ofNullable(this.onSwapchainDestroy).ifPresent(Runnable::run);
            KHRSwapchain.vkDestroySwapchainKHR(this.device.handle, this.swapchain, null);
            this.swapchain = 0L;
        }

        KHRSurface.vkDestroySurfaceKHR(VKInstance.getInstance().handle, this.surface, null);
        this.surface = 0L;
    }

    public void enableVsync(final boolean vsync) {
        if (this.vsync != vsync) {
            this.vsync = vsync;
            this.resizeSwapchain(width, height);
        }
    }

    public void resizeSwapchain(int widthHint, int heightHint) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkSurfaceCapabilitiesKHR surfaceCaps = VkSurfaceCapabilitiesKHR.callocStack(mem);
            int err = KHRSurface.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(this.device.physicalDevice.handle, this.surface, surfaceCaps);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            final VkExtent2D extent = surfaceCaps.currentExtent();

            if (extent.width() == -1) {
                extent.width(widthHint);
                extent.height(heightHint);
            }

            if (extent.width() < surfaceCaps.minImageExtent().width()) {
                extent.width(surfaceCaps.minImageExtent().width());
            } else if (extent.width() > surfaceCaps.maxImageExtent().width()) {
                extent.width(surfaceCaps.maxImageExtent().width());
            }

            if (extent.height() < surfaceCaps.minImageExtent().width()) {
                extent.width(surfaceCaps.minImageExtent().width());
            } else if (extent.height() > surfaceCaps.maxImageExtent().height()) {
                extent.height(surfaceCaps.maxImageExtent().height());
            }

            if (this.width == extent.width() && this.height == extent.height()) {
                return;
            }

            this.imageCount = BACKBUFFER_COUNT;

            if (this.imageCount < surfaceCaps.minImageCount()) {
                this.imageCount = surfaceCaps.minImageCount();
            } else if (this.imageCount > surfaceCaps.maxImageCount()) {
                this.imageCount = surfaceCaps.maxImageCount();
            }

            assert (surfaceCaps.supportedUsageFlags() & VK10.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT) != 0;
            assert (surfaceCaps.supportedTransforms() & surfaceCaps.currentTransform()) != 0;
            assert (surfaceCaps.supportedCompositeAlpha() & (KHRSurface.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR | KHRSurface.VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR)) != 0;

            final int compositeAlpha = (surfaceCaps.supportedCompositeAlpha() & KHRSurface.VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR) != 0
                    ? KHRSurface.VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR
                    : KHRSurface.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

            final IntBuffer pPresentModeCount = mem.callocInt(1);
            err = KHRSurface.vkGetPhysicalDeviceSurfacePresentModesKHR(device.physicalDevice.handle, surface, pPresentModeCount, null);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            final int presentModeCount = pPresentModeCount.get(0);
            final IntBuffer pPresentModes = mem.callocInt(presentModeCount);
            err = KHRSurface.vkGetPhysicalDeviceSurfacePresentModesKHR(device.physicalDevice.handle, surface, pPresentModeCount, pPresentModes);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            int presentMode = KHRSurface.VK_PRESENT_MODE_FIFO_KHR;

            while (pPresentModes.hasRemaining()) {
                final int testPresentMode = pPresentModes.get();

                if (this.vsync) {
                    if (testPresentMode == KHRSurface.VK_PRESENT_MODE_MAILBOX_KHR) {
                        presentMode = testPresentMode;
                        break;
                    }
                } else {
                    if (testPresentMode == KHRSurface.VK_PRESENT_MODE_IMMEDIATE_KHR) {
                        presentMode = testPresentMode;
                        break;
                    }
                }
            }

            switch (presentMode) {
                case KHRSurface.VK_PRESENT_MODE_FIFO_KHR:
                    LOGGER.trace("Present Mode: FIFO");
                    break;
                case KHRSurface.VK_PRESENT_MODE_FIFO_RELAXED_KHR:
                    LOGGER.trace("Present Mode: FIFO RELAXED");
                    break;
                case KHRSurface.VK_PRESENT_MODE_IMMEDIATE_KHR:
                    LOGGER.trace("Present Mode: IMMEDIATE");
                    break;
                case KHRSurface.VK_PRESENT_MODE_MAILBOX_KHR:
                    LOGGER.trace("Present Mode: MAILBOX");
                    break;
                default:
                    LOGGER.trace("Present Mode: UNKNOWN");
                    break;
            }

            pPresentModes.clear();

            final VkSwapchainCreateInfoKHR swapchainInfo = VkSwapchainCreateInfoKHR.callocStack(mem)
                    .sType(KHRSwapchain.VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR)
                    .surface(this.surface)
                    .minImageCount(imageCount)
                    .imageFormat(this.colorFormat.value)
                    .imageColorSpace(this.colorSpace)
                    .imageArrayLayers(1)
                    .imageExtent(extent)
                    .imageUsage(VK10.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)
                    .preTransform(surfaceCaps.currentTransform())
                    .compositeAlpha(compositeAlpha)
                    .presentMode(presentMode)
                    .clipped(true)
                    .oldSwapchain(this.swapchain);

            if (this.presentQueue == this.mainQueue) {
                swapchainInfo.imageSharingMode(VK10.VK_SHARING_MODE_EXCLUSIVE);
            } else {
                swapchainInfo.imageSharingMode(VK10.VK_SHARING_MODE_CONCURRENT)
                        .pQueueFamilyIndices(mem.ints(this.mainQueue.family.index, this.presentQueue.family.index));
            }

            final LongBuffer pSwapchain = mem.callocLong(1);
            err = KHRSwapchain.vkCreateSwapchainKHR(device.handle, swapchainInfo, null, pSwapchain);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            this.width = extent.width();
            this.height = extent.height();
            this.swapchain = pSwapchain.get(0);

            if (swapchainInfo.oldSwapchain() != 0L) {
                VK10.vkDeviceWaitIdle(device.handle);
                KHRSwapchain.vkDestroySwapchainKHR(device.handle, swapchainInfo.oldSwapchain(), null);
                Optional.ofNullable(this.onSwapchainDestroy).ifPresent(Runnable::run);

                this.backBuffers.forEach(VKBackBuffer::close);
                this.framebuffers.forEach(VKFramebuffer::close);
                this.swapchainImages.forEach(VKImageView::close);

                this.backBuffers.clear();
                this.framebuffers.clear();
                this.swapchainImages.clear();
            }

            this.createBackBuffers();
            this.prepareFramebuffers();

            Optional.ofNullable(this.onSwapchainCreate).ifPresent(Runnable::run);
        }
    }

    public void acquireBackBuffer() {
        final VKBackBuffer backBuffer = this.backBuffers.poll();

        try {
            //LOGGER.info("Waiting on fence: {}", backBuffer.presentFence.handle);
            backBuffer.presentFence.waitFor(Long.MAX_VALUE);
            backBuffer.presentFence.reset();
        } catch (VKTimeoutException ex) {
            throw new RuntimeException(ex);
        }

        int res = VK10.VK_TIMEOUT;

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pImageIndex = mem.callocInt(1);

            while (res != VK10.VK_SUCCESS) {
                pImageIndex.clear();
                res = KHRSwapchain.vkAcquireNextImageKHR(device.handle, swapchain, Long.MAX_VALUE, backBuffer.acquireSemaphore.handle, 0L, pImageIndex);
                backBuffer.imageIndex = pImageIndex.get(0);

                if (res == KHRSwapchain.VK_ERROR_OUT_OF_DATE_KHR) {
                    resizeSwapchain(0, 0);
                }
            }
        }

        this.acquiredBackBuffer = backBuffer;
    }

    private static final class ContextFrameData {

        private final VKFence fence;
        private final VKCommandBuffer primaryCommandBuffer;

        private ContextFrameData(final VKFence fence, final VKCommandBuffer primaryCmdBuf) {
            this.fence = fence;
            this.primaryCommandBuffer = primaryCmdBuf;
        }
    }

    int frameDataIndex = 0;

    private ContextFrameData getFrameData() {
        return this.frameData[this.frameDataIndex];
    }

    public VKCommandBuffer getPrimaryCommandBuffer() {
        return this.getFrameData().primaryCommandBuffer;
    }

    public VKFramebuffer getFramebuffer() {
        return this.framebuffers.get(VKContext.this.acquiredBackBuffer.imageIndex);
    }

    public void swapBuffers() {
        final ContextFrameData data = this.getFrameData();

        try {
            data.fence.waitFor(Long.BYTES);
            data.fence.reset();
        } catch (VKTimeoutException ex) {
            throw new RuntimeException(ex);
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkSubmitInfo submitInfo = VkSubmitInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SUBMIT_INFO)
                    .waitSemaphoreCount(1)
                    .pWaitSemaphores(mem.longs(this.acquiredBackBuffer.acquireSemaphore.handle))
                    .pSignalSemaphores(mem.longs(this.acquiredBackBuffer.renderSemaphore.handle))
                    .pWaitDstStageMask(mem.ints(VK10.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT))
                    .pCommandBuffers(mem.pointers(data.primaryCommandBuffer.handle));

            final int res = VK10.vkQueueSubmit(this.mainQueue.handle, submitInfo, data.fence.handle);

            assert Tools.vksuccess(res) : Tools.vkerr(res);
        }

        frameDataIndex = (frameDataIndex + 1) % this.imageCount;

        final VKBackBuffer buf = this.acquiredBackBuffer;

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkPresentInfoKHR presentInfo = VkPresentInfoKHR.callocStack(mem)
                    .sType(KHRSwapchain.VK_STRUCTURE_TYPE_PRESENT_INFO_KHR)
                    .pWaitSemaphores(mem.longs(buf.renderSemaphore.handle))
                    .swapchainCount(1)
                    .pSwapchains(mem.longs(this.swapchain))
                    .pImageIndices(mem.ints(buf.imageIndex));

            int err = KHRSwapchain.vkQueuePresentKHR(this.presentQueue.handle, presentInfo);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            final VkSubmitInfo submitInfo = VkSubmitInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SUBMIT_INFO);

            err = VK10.vkQueueSubmit(presentQueue.handle, submitInfo, buf.presentFence.handle);
            //LOGGER.info("Signaling fence: {}", buf.presentFence.handle);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            backBuffers.offer(buf);
        }
    }

    private void prepareFramebuffers() {
        this.swapchainImages = this.getSwapchainImages();
        this.framebuffers = new ArrayList<>(this.swapchainImages.size());

        final VKImage depthStencilImage = new VKImageInfo()
                .withImageType(VKImageType.IMAGE_2D)
                .withFormat(this.depthFormat)
                .withMipLevels(1)
                .withArrayLayers(1)
                .withSamples(VK10.VK_SAMPLE_COUNT_1_BIT)
                .withTiling(VKImageTiling.OPTIMAL)
                .withUsage(VKImageUsage.DEPTH_STENCIL_ATTACHMENT, VKImageUsage.TRANSFER_SRC)
                .withExtent(width, height, 1)
                .createManaged(this, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));

        this.depthStencil = VKImageView.fullView(this, depthStencilImage);

        for (int i = 0; i < this.swapchainImages.size(); i++) {
            final VKFramebuffer fb = new VKFramebufferInfo()
                    .withWidth(this.width)
                    .withHeight(this.height)
                    .withLayers(1)
                    .withAttachments(this.swapchainImages.get(i), this.depthStencil)
                    .create(this);

            this.framebuffers.add(fb);
        }
    }

    private List<VKImageView> getSwapchainImages() {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pSwapchainImageCount = mem.callocInt(1);
            int err = KHRSwapchain.vkGetSwapchainImagesKHR(device.handle, swapchain, pSwapchainImageCount, null);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            final int swapchainImageCount = pSwapchainImageCount.get(0);
            final LongBuffer pSwapchainImages = mem.callocLong(swapchainImageCount);
            err = KHRSwapchain.vkGetSwapchainImagesKHR(device.handle, swapchain, pSwapchainImageCount, pSwapchainImages);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            return IntStream.range(0, swapchainImageCount)
                    .mapToLong(pSwapchainImages::get)
                    .mapToObj(handle -> VKImage.wrapSwapchainImage(this, handle, colorFormat, width, height))
                    .map(image -> VKImageView.fullView(this, image))
                    .collect(Collectors.toList());

        }
    }

    @Override
    public boolean isValid() {
        return this.device.isValid();
    }

    @Override
    public void close() {
        if (this.isValid()) {
            this.destroySwapchain();
            this.backBuffers.stream().forEach(VKBackBuffer::close);
            this.backBuffers.clear();
            this.memoryManager.free();
            this.device.close();
        }
    }

    public List<VKRenderPassInfo.Attachment> getPresentAttachments() {
        return Collections.unmodifiableList(Arrays.asList(
                new VKRenderPassInfo.Attachment()
                        .withInitialLayout(VKImageLayout.UNDEFINED)
                        .withFinalLayout(VKImageLayout.PRESENT_SRC)
                        .withFormat(this.colorFormat)
                        .withLoadOp(VKRenderPassInfo.LoadOp.CLEAR)
                        .withStoreOp(VKRenderPassInfo.StoreOp.STORE)
                        .withSamples(VK10.VK_SAMPLE_COUNT_1_BIT),
                new VKRenderPassInfo.Attachment()
                        .withInitialLayout(VKImageLayout.UNDEFINED)
                        .withFinalLayout(VKImageLayout.DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
                        .withFormat(this.depthFormat)
                        .withLoadOp(VKRenderPassInfo.LoadOp.CLEAR)
                        .withStoreOp(VKRenderPassInfo.StoreOp.STORE)
                        .withSamples(VK10.VK_SAMPLE_COUNT_1_BIT)));
    }
}
