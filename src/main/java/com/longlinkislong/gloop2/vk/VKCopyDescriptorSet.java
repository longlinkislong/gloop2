/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKCopyDescriptorSet {

    public final VKDescriptorSet srcSet;
    public final int srcBinding;
    public final int srcArrayElement;
    public final VKDescriptorSet dstSet;
    public final int dstBinding;
    public final int dstArrayElement;
    public final int descriptorCount;

    public VKCopyDescriptorSet(
            final VKDescriptorSet srcSet, final int srcBinding, final int srcArrayElement,
            final VKDescriptorSet dstSet, final int dstBinding, final int dstArrayElement,
            final int descriptorCount) {
        
        this.srcSet = srcSet;
        this.srcBinding = srcBinding;
        this.srcArrayElement = srcArrayElement;
        
        this.dstSet = dstSet;
        this.dstBinding = dstBinding;
        this.dstArrayElement = dstArrayElement;
        
        this.descriptorCount = descriptorCount;
    }
    
    public VKCopyDescriptorSet() {
        this(null, 0, 0, null, 0, 0, 0);
    }
    
    public VKCopyDescriptorSet withSrcSet(final VKDescriptorSet srcSet) {
        return new VKCopyDescriptorSet(srcSet, srcBinding, srcArrayElement, dstSet, dstBinding, dstArrayElement, descriptorCount);
    }
    
    public VKCopyDescriptorSet withSrcBinding(final int srcBinding) {
        return new VKCopyDescriptorSet(srcSet, srcBinding, srcArrayElement, dstSet, dstBinding, dstArrayElement, descriptorCount);
    }
    
    public VKCopyDescriptorSet withSrcArrayElement(final int srcArrayElement) {
        return new VKCopyDescriptorSet(srcSet, srcBinding, srcArrayElement, dstSet, dstBinding, dstArrayElement, descriptorCount);
    }
    
    public VKCopyDescriptorSet withDstSet(final VKDescriptorSet dstSet) {
        return new VKCopyDescriptorSet(srcSet, srcBinding, srcArrayElement, dstSet, dstBinding, dstArrayElement, descriptorCount);
    }
    
    public VKCopyDescriptorSet withDstBinding(final int dstBinding) {
        return new VKCopyDescriptorSet(srcSet, srcBinding, srcArrayElement, dstSet, dstBinding, dstArrayElement, descriptorCount);
    }
    
    public VKCopyDescriptorSet withDstArrayElement(final int dstArrayElement) {
        return new VKCopyDescriptorSet(srcSet, srcBinding, srcArrayElement, dstSet, dstBinding, dstArrayElement, descriptorCount);
    }
    
    public VKCopyDescriptorSet withDescriptorCount(final int descriptorCount) {
        return new VKCopyDescriptorSet(srcSet, srcBinding, srcArrayElement, dstSet, dstBinding, dstArrayElement, descriptorCount);
    }
}
