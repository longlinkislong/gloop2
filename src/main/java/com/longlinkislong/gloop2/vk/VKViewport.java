/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VkViewport;

/**
 *
 * @author zmichaels
 */
public class VKViewport {

    public final float x;
    public final float y;
    public final float width;
    public final float height;
    public final float minDepth;
    public final float maxDepth;    
    
    public VKViewport(
            final float x, final float y,
            final float width, final float height,
            final float minDepth, final float maxDepth) {

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.minDepth = minDepth;
        this.maxDepth = maxDepth;
    }
    
    public VKViewport() {
        this(0F, 0F, 0F, 0F, 0F, 0F);
    }
    
    public VKViewport withOffset(final float x, final float y) {
        return new VKViewport(x, y, width, height, minDepth, maxDepth);
    }
    
    public VKViewport withExtent(final float width, final float height) {
        return new VKViewport(x, y, width, height, minDepth, maxDepth);
    }
    
    public VKViewport withDepthRange(final float minDepth, final float maxDepth) {
        return new VKViewport(x, y, width, height, minDepth, maxDepth);
    }
    
    void apply(VkViewport vp) {
        vp.x(x).y(y)
                .width(width).height(height)
                .minDepth(minDepth).maxDepth(maxDepth);
    }
}
