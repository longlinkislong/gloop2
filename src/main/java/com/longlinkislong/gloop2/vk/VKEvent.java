/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkEventCreateInfo;

/**
 *
 * @author zmichaels
 */
public class VKEvent implements VKObject {

    long handle;
    VkDevice deviceHandle;

    public VKEvent() {}
    
    public VKEvent(final VKContext ctx) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkEventCreateInfo createInfo = VkEventCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_EVENT_CREATE_INFO);

            final LongBuffer pHandle = mem.callocLong(1);
            final int err = VK10.vkCreateEvent(ctx.device.handle, createInfo, null, pHandle);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            this.handle = pHandle.get(0);
            this.deviceHandle = ctx.device.handle;
        }
    }

    public void swap(final VKEvent other) {
        if (other == null) {
            this.close();
            return;
        }

        final long tmpHandle = other.handle;
        final VkDevice tmpDev = other.deviceHandle;

        other.handle = this.handle;
        other.deviceHandle = this.deviceHandle;

        this.handle = tmpHandle;
        this.deviceHandle = tmpDev;
    }

    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyEvent(this.deviceHandle, this.handle, null);
            this.handle = 0L;
            this.deviceHandle = null;
        }
    }

    public VKEventStatus getStatus() {
        if (!this.isValid()) {
            throw new IllegalStateException("Event is not valid!");
        }

        final int res = VK10.vkGetEventStatus(this.deviceHandle, this.handle);

        return VKEventStatus.of(res).orElseThrow(() -> new AssertionError(Tools.vkerr(res)));
    }
    
    public void set() {
        if (!this.isValid()) {
            throw new IllegalStateException("Event is not valid!");
        }
        
        final int err = VK10.vkSetEvent(this.deviceHandle, this.handle);
        
        assert Tools.vksuccess(err) : Tools.vkerr(err);
    }
    
    public void reset() {
        if (!this.isValid()) {
            throw new IllegalStateException("Event is not valid!");
        }
        
        final int err = VK10.vkResetEvent(this.deviceHandle, this.handle);
        
        assert Tools.vksuccess(err) : Tools.vkerr(err);
    }
}
