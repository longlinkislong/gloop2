/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKSamplerAddressMode implements CEnum {
    REPEAT(VK10.VK_SAMPLER_ADDRESS_MODE_REPEAT),
    MIRRORED_REPEAT(VK10.VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT),
    CLAMP_TO_EDGE(VK10.VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE),
    CLAMP_TO_BORDER(VK10.VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER),
    MIRROR_CLAMP_TO_EDGE(4);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKSamplerAddressMode(final int value) {
        this.value = value;
    }
    
    public static Optional<VKSamplerAddressMode> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
