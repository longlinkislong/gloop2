/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkImageViewCreateInfo;

/**
 *
 * @author zmichaels
 */
public class VKImageView implements VKObject {
    VkDevice deviceHandle;
    long handle;    
    VKImageViewInfo info;
    
    public VKImageView() {}
    
    public VKImageViewInfo getInfo() {
        return this.info;
    }
    
    public static VKImageView fullView(final VKContext ctx, final VKImage image) {
        return new VKImageView(ctx, VKImageViewInfo.wrap(image));
    }
    
    public VKImageView(final VKContext ctx, final VKImageViewInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkImageViewCreateInfo viewInfo = VkImageViewCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO)
                    .flags(info.flags)
                    .image(info.image.handle)
                    .viewType(info.viewType.value)
                    .format(info.format.value);
            
            viewInfo.components()
                    .r(info.components.r.value)
                    .g(info.components.g.value)
                    .b(info.components.b.value)
                    .a(info.components.a.value);
            
            viewInfo.subresourceRange()
                    .aspectMask(VKImageAspect.bits(info.subresourceRange.aspectMask))
                    .baseMipLevel(info.subresourceRange.baseMipLevel)
                    .levelCount(info.subresourceRange.levelCount)
                    .baseArrayLayer(info.subresourceRange.baseArrayLayer)
                    .layerCount(info.subresourceRange.layerCount);
            
            final LongBuffer pHandle = mem.callocLong(1);
            final int err = VK10.vkCreateImageView(ctx.device.handle, viewInfo, null, pHandle);
            
            assert Tools.vksuccess(err) : Tools.vkerr(err);
            
            this.handle = pHandle.get(0);
            this.deviceHandle = ctx.device.handle;
            this.info = info;
        }
    }        

    @Override
    public boolean isValid() {
        return this.handle > 0L;
    }

    public void swap(final VKImageView other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final VkDevice tmpDev = other.deviceHandle;
        final long tmpHandle = other.handle;
        final VKImageViewInfo tmpInfo = other.info;
        
        other.deviceHandle = this.deviceHandle;
        other.handle = this.handle;
        other.info = this.info;
        
        this.deviceHandle = tmpDev;
        this.handle = tmpHandle;
        this.info = tmpInfo;
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyImageView(deviceHandle, handle, null);
            this.deviceHandle = null;
            this.handle = 0L;
            this.info = null;
        }
    }
}
