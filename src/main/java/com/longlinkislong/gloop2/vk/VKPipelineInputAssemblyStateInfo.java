/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineInputAssemblyStateCreateInfo;

/**
 *
 * @author zmichaels
 */
public class VKPipelineInputAssemblyStateInfo {    

    public final int flags;
    public final VKPrimitiveTopology topology;
    public final boolean primitiveRestartEnable;

    public static final VKPipelineInputAssemblyStateInfo DEFAULT_POINT_LIST = new VKPipelineInputAssemblyStateInfo(0, VKPrimitiveTopology.POINT_LIST, false);
    public static final VKPipelineInputAssemblyStateInfo DEFAULT_TRIANGLE_LIST = new VKPipelineInputAssemblyStateInfo(0, VKPrimitiveTopology.TRIANGLE_LIST, false);
    public static final VKPipelineInputAssemblyStateInfo DEFAULT_TRIANGLE_STRIP = new VKPipelineInputAssemblyStateInfo(0, VKPrimitiveTopology.TRIANGLE_STRIP, false);
    
    public VKPipelineInputAssemblyStateInfo(
            final int flags, 
            final VKPrimitiveTopology topology, 
            final boolean primitiveRestartEnable) {
        
        this.flags = flags;
        this.topology = topology;
        this.primitiveRestartEnable = primitiveRestartEnable;
    }

    public VKPipelineInputAssemblyStateInfo() {
        this.flags = 0;
        this.topology = VKPrimitiveTopology.TRIANGLE_LIST;
        this.primitiveRestartEnable = false;
    }
    
    public VKPipelineInputAssemblyStateInfo withFlags(final int flags) {
        return new VKPipelineInputAssemblyStateInfo(flags, topology, primitiveRestartEnable);
    }

    public VKPipelineInputAssemblyStateInfo withTopology(final VKPrimitiveTopology topology) {
        return new VKPipelineInputAssemblyStateInfo(flags, topology, primitiveRestartEnable);
    }

    public VKPipelineInputAssemblyStateInfo withPrimitiveRestartEnable(final boolean primitiveRestartEnable) {
        return new VKPipelineInputAssemblyStateInfo(flags, topology, primitiveRestartEnable);
    }
    
    void vkStruct(final VkPipelineInputAssemblyStateCreateInfo struct) {
        struct.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO)
                .flags(this.flags)
                .topology(this.topology.value)
                .primitiveRestartEnable(this.primitiveRestartEnable);
    }
}
