/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKPipelineStage implements CBitfield{
    TOP_OF_PIPE(VK10.VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT),
    DRAW_INDIRECT(VK10.VK_PIPELINE_STAGE_DRAW_INDIRECT_BIT),
    VERTEX_INPUT(VK10.VK_PIPELINE_STAGE_VERTEX_INPUT_BIT),
    VERTEX_SHADER(VK10.VK_PIPELINE_STAGE_VERTEX_SHADER_BIT),
    TESSELLATION_CONTROL_SHADER(VK10.VK_PIPELINE_STAGE_TESSELLATION_CONTROL_SHADER_BIT),
    TESSELLATION_EVALUATION_SHADER(VK10.VK_PIPELINE_STAGE_TESSELLATION_EVALUATION_SHADER_BIT),
    GEOMETRY_SHADER(VK10.VK_PIPELINE_STAGE_GEOMETRY_SHADER_BIT),
    FRAGMENT_SHADER(VK10.VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT),
    EARLY_FRAGMENT_TESTS(VK10.VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT),
    LATE_FRAGMENT_TESTS(VK10.VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT),
    COLOR_ATTACHMENT_OUTPUT(VK10.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT),
    COMPUTE_SHADER(VK10.VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT),
    TRANSFER(VK10.VK_PIPELINE_STAGE_TRANSFER_BIT),
    BOTTOM_OF_PIPE(VK10.VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT),
    HOST(VK10.VK_PIPELINE_STAGE_HOST_BIT),
    ALL_COMMANDS(VK10.VK_PIPELINE_STAGE_ALL_COMMANDS_BIT);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKPipelineStage(final int value) {
        this.value = value;
    }
    
    public static Set<VKPipelineStage> flags(final int bits) {
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<VKPipelineStage> flags) {
        return Tools.flagsToBitfield(flags);
    }    
}
