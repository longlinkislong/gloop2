/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VK10;

/**
 * Construction information for creating a VKBufferViewInfo. The source VKBuffer
 * must have either the usage flag UNIFORM_TEXEL_BUFFER or STORAGE_TEXEL_BUFFER.
 *
 * @author zmichaels
 */
public class VKBufferViewInfo {

    public final int flags;
    public final VKBuffer buffer;
    public final VKFormat format;
    public final long offset;
    public final long range;

    public VKBufferViewInfo(
            final int flags,
            final VKBuffer buffer,
            final VKFormat format,
            final long offset, final long range) {

        this.flags = flags;
        this.buffer = buffer;
        this.format = format;
        this.offset = offset;
        this.range = range;
    }

    /**
     * Creates a new VKBufferViewInfo with all default values. All numeric
     * fields are initialized as 0, all reference fields are initialized as
     * null, and the format field is initialized as R8G8B8A8_UNORM.
     */
    public VKBufferViewInfo() {
        this(0, null, VKFormat.R8G8B8A8_UNORM, 0L, 0L);
    }

    /**
     * Derives a new VKBufferViewInfo with a new flag value.
     *
     * @param flags the flag value. Must be 0.
     * @return the derivative VKBufferViewInfo.
     */
    public VKBufferViewInfo withFlags(final int flags) {
        assert flags == 0 : "Flags is reserved for future use and currently must be 0.";

        return new VKBufferViewInfo(flags, buffer, format, offset, range);
    }

    /**
     * Derives a new VKBufferViewInfo with a new VKBuffer value.
     *
     * @param buffer the VKBuffer value.
     * @return the derivative VKBufferViewInfo.
     */
    public VKBufferViewInfo withBuffer(final VKBuffer buffer) {
        assert buffer != null : "Buffer cannot be null!";
        assert buffer.isValid() : "Buffer must be valid!";
        assert buffer.info.usage.contains(VKBufferInfo.Usage.STORAGE_TEXEL) 
                || buffer.info.usage.contains(VKBufferInfo.Usage.UNIFORM_TEXEL) 
                : "VKBuffer must have usage of either STORAGE_TEXEL or UNIFORM_TEXEL!";
        
        return new VKBufferViewInfo(flags, buffer, format, offset, range);
    }

    /**
     * Derives a new VKBufferViewInfo with a new format.
     *
     * @param format the new format.
     * @return the derivative VKBufferViewInfo.
     */
    public VKBufferViewInfo withFormat(final VKFormat format) {
        assert format != null : "Format cannot be null!";
        
        return new VKBufferViewInfo(flags, buffer, format, offset, range);
    }

    /**
     * Derives a new VKBufferViewInfo with a new offset.
     *
     * @param offset the new offset.
     * @return the derivative VKBufferViewInfo.
     */
    public VKBufferViewInfo withOffset(final long offset) {
        assert offset >= 0 : "Offset cannot be negative!";
        
        return new VKBufferViewInfo(flags, buffer, format, offset, range);
    }

    /**
     * Derives a new VKBufferViewInfo with the entire remaining buffer.
     *
     * @return the derivative VKBufferViewInfo.
     */
    public VKBufferViewInfo withWholeSize() {
        return withRange(VK10.VK_WHOLE_SIZE);
    }

    /**
     * Derives a new VKBufferViewInfo with the new range value.
     * @param range the range value.
     * @return the derivative VKBufferViewInfo.
     */
    public VKBufferViewInfo withRange(final long range) {
        assert range == VK10.VK_WHOLE_SIZE || range >= 0 : "Range cannot be negative!";
        
        return new VKBufferViewInfo(flags, buffer, format, offset, range);
    }
}
