/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.glfw.GLFWVulkan;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkQueueFamilyProperties;

/**
 *
 * @author zmichaels
 */
public final class VKQueueFamily {
    public final int index;
    public final int flags;
    public final int queueCount;
    
    private final boolean supportsPresent;
    private final boolean supportsTransfer;
    private final boolean supportsDraw;
    private final boolean supportsCompute;
    
    private final VKPhysicalDevice physicalDevice;
    
    public VKPhysicalDevice getPhysicalDevice() {
        return this.physicalDevice;
    }
    
    public boolean canPresent() {
        return this.supportsPresent;
    }
    
    public boolean canDraw() {
        return this.supportsDraw;
    }
    
    public boolean canTransfer() {
        return this.supportsTransfer;
    }
    
    public boolean canCompute() {
        return this.supportsCompute;
    }
    
    VKQueueFamily(final VKPhysicalDevice physicalDevice, final int queueFamilyIndex, final VkQueueFamilyProperties.Buffer properties) {
        this.index = queueFamilyIndex;
        this.physicalDevice = physicalDevice;
        this.flags = properties.get(queueFamilyIndex).queueFlags();
        this.queueCount = properties.get(queueFamilyIndex).queueCount();                
        
        this.supportsPresent = GLFWVulkan.glfwGetPhysicalDevicePresentationSupport(VKInstance.getInstance().handle, physicalDevice.handle, queueFamilyIndex);
        this.supportsCompute = (flags & VK10.VK_QUEUE_COMPUTE_BIT) != 0;
        this.supportsDraw = (flags & VK10.VK_QUEUE_GRAPHICS_BIT) != 0;
        this.supportsTransfer = this.supportsCompute | this.supportsDraw | ((flags & VK10.VK_QUEUE_TRANSFER_BIT) != 0);
    }
}
