/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkQueue;
import org.lwjgl.vulkan.VkSubmitInfo;

/**
 *
 * @author zmichaels
 */
public final class VKQueue {

    final VkQueue handle;
    public final VKQueueFamily family;

    VKQueue(final VKDevice device, final VKQueueFamily queueFamily, final int queueIndex) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pQueue = mem.callocPointer(1);

            VK10.vkGetDeviceQueue(device.handle, queueFamily.index, queueIndex, pQueue);

            this.handle = new VkQueue(pQueue.get(0), device.handle);
        }

        this.family = queueFamily;
    }    
    
    public void waitIdle() {
        VK10.vkQueueWaitIdle(handle);
    }
    
    public void submit(final SubmitInfo info) {
        this.submit(info, null);
    }

    public void submit(final SubmitInfo info, final VKFence fence) {
        this.submit(new SubmitInfo[] {info}, fence);
    }
    
    public void submit(final SubmitInfo[] infos) {
        this.submit(infos, null);
    }
    
    public void submit(final SubmitInfo[] infos, final VKFence fence) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkSubmitInfo.Buffer pSubmits = VkSubmitInfo.callocStack(infos.length, mem);

            for (int i = 0; i < infos.length; i++) {
                final SubmitInfo info = infos[i];
                final LongBuffer pWaitSemaphores;
                final IntBuffer pWaitDstStageMask;

                if (info.waitInfo.isEmpty()) {
                    pWaitSemaphores = null;
                    pWaitDstStageMask = null;
                } else {                                        
                    pWaitSemaphores = mem.mallocLong(info.waitInfo.size());
                    pWaitDstStageMask = mem.mallocInt(info.waitInfo.size());

                    for (SubmitInfo.WaitInfo waitInfo : info.waitInfo) {
                        pWaitSemaphores.put(waitInfo.semaphore.handle);
                        pWaitDstStageMask.put(VKPipelineStage.bits(waitInfo.dstStageMask));
                    }
                                        
                    pWaitSemaphores.flip();
                    pWaitDstStageMask.flip();
                }
                
                final LongBuffer pSignalSemaphores;

                if (info.signalSemaphores.isEmpty()) {
                    pSignalSemaphores = null;
                } else {
                    pSignalSemaphores = mem.mallocLong(info.signalSemaphores.size());

                    info.signalSemaphores.stream()
                            .mapToLong(semaphore -> semaphore.handle)
                            .forEach(pSignalSemaphores::put);

                    pSignalSemaphores.flip();
                }

                final PointerBuffer pCommandBuffers;

                if (info.commandBuffers.isEmpty()) {
                    pCommandBuffers = null;
                } else {
                    pCommandBuffers = mem.mallocPointer(info.commandBuffers.size());

                    info.commandBuffers.stream()
                            .map(buffer -> buffer.handle)
                            .forEach(pCommandBuffers::put);

                    pCommandBuffers.flip();
                }
                
                pSubmits.get(i)
                        .sType(VK10.VK_STRUCTURE_TYPE_SUBMIT_INFO)
                        .waitSemaphoreCount(pWaitSemaphores == null ? 0 : pWaitSemaphores.remaining())
                        .pWaitSemaphores(pWaitSemaphores)                        
                        .pWaitDstStageMask(pWaitDstStageMask)                        
                        .pSignalSemaphores(pSignalSemaphores)                        
                        .pCommandBuffers(pCommandBuffers);
            }

            final int err = VK10.vkQueueSubmit(this.handle, pSubmits, fence == null ? 0L : fence.handle);
            
            assert Tools.vksuccess(err) : Tools.vkerr(err);
        }
    }

    public static final class SubmitInfo {

        public static SubmitInfo wrap(final VKCommandBuffer... buffers) {
            return new SubmitInfo().withCommandBuffers(buffers);
        }
        
        public static final class WaitInfo {
            public final VKSemaphore semaphore;
            public final Set<VKPipelineStage> dstStageMask;
            
            public WaitInfo(final VKSemaphore semaphore, final Collection<VKPipelineStage> dstStageMask) {
                this.semaphore = semaphore;
                this.dstStageMask = Tools.copySet(dstStageMask);
            }
            
            public WaitInfo() {
                this(null, null);
            }
            
            public WaitInfo withSemaphore(final VKSemaphore semaphore) {
                return new WaitInfo(semaphore, dstStageMask);
            }
            
            public WaitInfo withDstStageMask(final Collection<VKPipelineStage> dstStageMask) {
                return new WaitInfo(semaphore, dstStageMask);
            }
            
            public WaitInfo withDstStageMask(final VKPipelineStage... dstStageMask) {
                return withDstStageMask(Arrays.asList(dstStageMask));
            }
        }
        
        public final List<WaitInfo> waitInfo;
        public final List<VKCommandBuffer> commandBuffers;
        public final List<VKSemaphore> signalSemaphores;

        public SubmitInfo(
                final List<WaitInfo> waitInfo,
                final List<VKCommandBuffer> commandBuffers,
                final List<VKSemaphore> signalSemaphores) {

            this.waitInfo = Tools.copyList(waitInfo);
            this.commandBuffers = Tools.copyList(commandBuffers);
            this.signalSemaphores = Tools.copyList(signalSemaphores);
        }

        public SubmitInfo() {
            this(null,  null, null);
        }

        public SubmitInfo withWaitInfo(final List<WaitInfo> waitInfo) {
            return new SubmitInfo(waitInfo, commandBuffers, signalSemaphores);
        }
        
        public SubmitInfo withWaitInfo(final WaitInfo... waitInfo) {
            return withWaitInfo(Arrays.asList(waitInfo));
        }

        public SubmitInfo withCommandBuffers(final List<VKCommandBuffer> commandBuffers) {
            return new SubmitInfo(waitInfo, commandBuffers, signalSemaphores);
        }

        public SubmitInfo withCommandBuffers(final VKCommandBuffer... commandBuffers) {
            return withCommandBuffers(Arrays.asList(commandBuffers));
        }

        public SubmitInfo withSignalSemaphores(final List<VKSemaphore> signalSemaphores) {
            return new SubmitInfo(waitInfo, commandBuffers, signalSemaphores);
        }

        public SubmitInfo withSignalSemaphores(final VKSemaphore... signalSemaphores) {
            return withSignalSemaphores(Arrays.asList(signalSemaphores));
        }
    }
}
