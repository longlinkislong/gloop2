/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkPipelineLayoutCreateInfo;
import org.lwjgl.vulkan.VkPushConstantRange;

/**
 *
 * @author zmichaels
 */
public class VKPipelineLayout implements VKObject {

    long handle;
    VkDevice deviceHandle;
    VKPipelineLayoutInfo info;

    public VKPipelineLayout(final VKContext ctx) {
        this(ctx, new VKPipelineLayoutInfo());
    }

    public VKPipelineLayout(final VKContext ctx, final VKPipelineLayoutInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final LongBuffer pSetLayouts;

            if (info.setLayouts.isEmpty()) {
                pSetLayouts = null;
            } else {
                pSetLayouts = mem.callocLong(info.setLayouts.size());

                info.setLayouts.stream()
                        .mapToLong(layout -> layout.handle)
                        .forEach(pSetLayouts::put);

                pSetLayouts.flip();
            }

            final VkPushConstantRange.Buffer pPushConstantRanges;

            if (info.pushConstantRanges.isEmpty()) {
                pPushConstantRanges = null;
            } else {
                pPushConstantRanges = VkPushConstantRange.callocStack(info.pushConstantRanges.size(), mem);

                for (int i = 0; i < info.pushConstantRanges.size(); i++) {
                    final VKPushConstantRange range = info.pushConstantRanges.get(i);

                    pPushConstantRanges.get(i)
                            .stageFlags(VKShaderStage.bits(range.stageFlags))
                            .offset(range.offset)
                            .size(range.size);
                }

            }

            final VkPipelineLayoutCreateInfo createInfo = VkPipelineLayoutCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO)
                    .flags(info.flags)
                    .pSetLayouts(pSetLayouts)
                    .pPushConstantRanges(pPushConstantRanges);

            final LongBuffer pHandle = mem.callocLong(1);
            final int err = VK10.vkCreatePipelineLayout(ctx.device.handle, createInfo, null, pHandle);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            this.handle = pHandle.get(0);
            this.deviceHandle = ctx.device.handle;
            this.info = info;
        }
    }

    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyPipelineLayout(deviceHandle, handle, null);
            this.handle = 0L;
            this.deviceHandle = null;
            this.info = null;
        }
    }

    public VKPipelineLayoutInfo getInfo() {
        return this.info;
    }

    public void swap(final VKPipelineLayout other) {
        if (other == null) {
            this.close();
            return;
        }

        final long tmpHandle = other.handle;
        final VkDevice tmpDev = other.deviceHandle;
        final VKPipelineLayoutInfo tmpInfo = other.info;

        other.deviceHandle = this.deviceHandle;
        other.handle = this.handle;
        other.info = this.info;

        this.deviceHandle = tmpDev;
        this.handle = tmpHandle;
        this.info = tmpInfo;
    }
}
