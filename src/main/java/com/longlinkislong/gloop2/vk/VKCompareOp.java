/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKCompareOp implements CEnum {
    NEVER(VK10.VK_COMPARE_OP_NEVER),
    LESS(VK10.VK_COMPARE_OP_LESS),
    EQUAL(VK10.VK_COMPARE_OP_EQUAL),
    LESS_OR_EQUAL(VK10.VK_COMPARE_OP_LESS_OR_EQUAL),
    GREATER(VK10.VK_COMPARE_OP_GREATER),
    NOT_EQUAL(VK10.VK_COMPARE_OP_NOT_EQUAL),
    GREATER_OR_EQUAL(VK10.VK_COMPARE_OP_GREATER_OR_EQUAL),
    ALWAYS(VK10.VK_COMPARE_OP_ALWAYS);

    public final int value;

    @Override
    public int getValue() {
        return this.value;
    }

    private VKCompareOp(final int value) {
        this.value = value;
    }

    public static Optional<VKCompareOp> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
