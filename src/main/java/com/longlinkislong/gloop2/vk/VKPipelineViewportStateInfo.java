/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineViewportStateCreateInfo;
import org.lwjgl.vulkan.VkRect2D;
import org.lwjgl.vulkan.VkViewport;

/**
 *
 * @author zmichaels
 */
public class VKPipelineViewportStateInfo {
    
    public final int flags;
    public final List<VKViewport> viewports;
    public final List<VKScissor> scissors;
    
    public VKPipelineViewportStateInfo(
            final int flags,
            final List<VKViewport> viewports,
            final List<VKScissor> scissors) {
        
        this.flags = flags;
        this.viewports = Tools.copyList(viewports);
        this.scissors = Tools.copyList(scissors);
    }
    
    public VKPipelineViewportStateInfo() {
        this(0, null, null);
    }
    
    public VKPipelineViewportStateInfo withFlags(final int flags) {
        return new VKPipelineViewportStateInfo(flags, viewports, scissors);
    }
    
    public VKPipelineViewportStateInfo withViewportCount(final int viewports) {
        return withViewports(IntStream.range(0, viewports)
                .mapToObj(i -> new VKViewport())
                .collect(Collectors.toList()));
    }
    
    public VKPipelineViewportStateInfo withViewports(final List<VKViewport> viewports) {
        return new VKPipelineViewportStateInfo(flags, viewports, scissors);
    }
    
    public VKPipelineViewportStateInfo withViewports(final VKViewport... viewports) {
        return withViewports(Arrays.asList(viewports));
    }
    
    public VKPipelineViewportStateInfo withScissorCount(final int scissors) {
        return withScissors(IntStream.range(0, scissors)
                .mapToObj(i -> new VKScissor())
                .collect(Collectors.toList()));
    }
    
    public VKPipelineViewportStateInfo withScissors(final List<VKScissor> scissors) {
        return new VKPipelineViewportStateInfo(flags, viewports, scissors);
    }
    
    public VKPipelineViewportStateInfo withScissors(final VKScissor... scissors) {
        return withScissors(Arrays.asList(scissors));
    }
    
    void vkStruct(VkPipelineViewportStateCreateInfo struct, final MemoryStack mem) {
        final VkViewport.Buffer pViewports = VkViewport.callocStack(this.viewports.size(), mem);
        
        for (int i = 0; i < this.viewports.size(); i++) {
            final VKViewport viewport = this.viewports.get(i);
            
            pViewports.get(i)
                    .x(viewport.x)
                    .y(viewport.y)
                    .width(viewport.width)
                    .height(viewport.height)
                    .minDepth(viewport.minDepth)
                    .maxDepth(viewport.maxDepth);
        }
        
        final VkRect2D.Buffer pScissors = VkRect2D.callocStack(this.scissors.size(), mem);
        
        for (int i = 0; i < this.scissors.size(); i++) {
            final VKScissor scissor = this.scissors.get(i);
            
            pScissors.get(i)
                    .offset().x(scissor.x).y(scissor.y);
            pScissors.get(i)
                    .extent().width(scissor.width).height(scissor.height);
        }
        
        struct
                .sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO)
                .flags(this.flags)
                .viewportCount(this.viewports.size())
                .pViewports(pViewports)
                .scissorCount(this.scissors.size())
                .pScissors(pScissors);
    }
    
    public static VKPipelineViewportStateInfo dynamicViewports(final int count) {
        return new VKPipelineViewportStateInfo()
                .withViewportCount(count)
                .withScissorCount(count);
    }
}
