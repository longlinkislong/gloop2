/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKSharingMode implements CEnum {
    EXCLUSIVE(VK10.VK_SHARING_MODE_EXCLUSIVE),
    CONCURRENT(VK10.VK_SHARING_MODE_CONCURRENT);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKSharingMode(final int value) {
        this.value = value;
    }
    
    public static Optional<VKSharingMode> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
