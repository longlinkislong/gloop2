/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkShaderModuleCreateInfo;

/**
 *
 * @author zmichaels
 */
public class VKShaderModule implements VKObject {
    long handle;
    VkDevice deviceHandle;    

    public VKShaderModule(){}
    
    public VKShaderModule(final VKContext ctx, final ByteBuffer binary) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkShaderModuleCreateInfo info = VkShaderModuleCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO)
                    .pCode(binary);
            
            final LongBuffer pShaderModule = mem.callocLong(1);
            final int err = VK10.vkCreateShaderModule(ctx.device.handle, info, null, pShaderModule);
            
            assert Tools.vksuccess(err) : Tools.vkerr(err);
            
            this.handle = pShaderModule.get(0);
            this.deviceHandle = ctx.device.handle;
        }
    }
    
    public void swap(final VKShaderModule other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final long tmpHandle = other.handle;
        final VkDevice tmpDevice = other.deviceHandle;
        
        other.handle = this.handle;
        other.deviceHandle = this.deviceHandle;
        
        this.handle = tmpHandle;
        this.deviceHandle = tmpDevice;
    }
    
    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyShaderModule(this.deviceHandle, handle, null);
            this.deviceHandle = null;
            this.handle = 0L;
        }
    }
}
