/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineDepthStencilStateCreateInfo;

public final class VKPipelineDepthStencilStateInfo {

    public final int flags;
    public final boolean depthTestEnable;
    public final boolean depthWriteEnable;
    public final VKCompareOp depthCompareOp;
    public final boolean depthBoundsTestEnable;
    public final boolean stencilTestEnable;
    public final VKStencilOpState front;
    public final VKStencilOpState back;
    public final float minDepthBounds;
    public final float maxDepthBounds;
    
    public static VKPipelineDepthStencilStateInfo DEFAULT_OPENGL_DEPTH_TEST = new VKPipelineDepthStencilStateInfo()
            .withDepthTestEnable(true)
            .withDepthWriteEnable(true)
            .withDepthCompareOp(VKCompareOp.LESS)
            .withBack(new VKStencilOpState())
            .withFront(new VKStencilOpState());

    public VKPipelineDepthStencilStateInfo(
            final int flags,
            final boolean depthTestEnable, final boolean depthWriteEnable,
            final VKCompareOp depthCompareOp,
            final boolean depthBoundsTestEnable,
            final boolean stencilTestEnable,
            final VKStencilOpState front, final VKStencilOpState back,
            final float minDepthBounds, final float maxDepthBounds) {

        this.flags = flags;
        this.depthTestEnable = depthTestEnable;
        this.depthWriteEnable = depthWriteEnable;
        this.depthCompareOp = depthCompareOp;
        this.depthBoundsTestEnable = depthBoundsTestEnable;
        this.stencilTestEnable = stencilTestEnable;
        this.front = (front == null) ? new VKStencilOpState() : front;
        this.back = (back == null) ? new VKStencilOpState() : back;
        this.minDepthBounds = minDepthBounds;
        this.maxDepthBounds = maxDepthBounds;
    }

    public VKPipelineDepthStencilStateInfo() {
        this(0, false, false, VKCompareOp.NEVER, false, false, null, null, 0F, 0F);
    }
    
    public VKPipelineDepthStencilStateInfo withFlags(final int flags) {
        return new VKPipelineDepthStencilStateInfo(flags, depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, minDepthBounds, maxDepthBounds);
    }
    
    public VKPipelineDepthStencilStateInfo withDepthTestEnable(final boolean depthTestEnable) {
        return new VKPipelineDepthStencilStateInfo(flags, depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, minDepthBounds, maxDepthBounds);
    }
    
    public VKPipelineDepthStencilStateInfo withDepthWriteEnable(final boolean depthWriteEnable) {
        return new VKPipelineDepthStencilStateInfo(flags, depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, minDepthBounds, maxDepthBounds);
    }
    
    public VKPipelineDepthStencilStateInfo withDepthCompareOp(final VKCompareOp depthCompareOp) {
        return new VKPipelineDepthStencilStateInfo(flags, depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, minDepthBounds, maxDepthBounds);
    }
    
    public VKPipelineDepthStencilStateInfo withDepthBoundsTestEnable(final boolean depthBoundsTestEnable) {
        return new VKPipelineDepthStencilStateInfo(flags, depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, minDepthBounds, maxDepthBounds);
    }
    
    public VKPipelineDepthStencilStateInfo withStencilTestEnable(final boolean stencilTestEnable) {
        return new VKPipelineDepthStencilStateInfo(flags, depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, minDepthBounds, maxDepthBounds);
    }
    
    public VKPipelineDepthStencilStateInfo withFront(final VKStencilOpState front) {
        return new VKPipelineDepthStencilStateInfo(flags, depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, minDepthBounds, maxDepthBounds);
    }
    
    public VKPipelineDepthStencilStateInfo withBack(final VKStencilOpState back) {
        return new VKPipelineDepthStencilStateInfo(flags, depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, minDepthBounds, maxDepthBounds);
    }
    
    public VKPipelineDepthStencilStateInfo withMinDepthBounds(final float minDepthBounds) {
        return new VKPipelineDepthStencilStateInfo(flags, depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, minDepthBounds, maxDepthBounds);
    }
    
    public VKPipelineDepthStencilStateInfo withMaxDepthBounds(final float maxDepthBounds) {
        return new VKPipelineDepthStencilStateInfo(flags, depthTestEnable, depthWriteEnable, depthCompareOp, depthBoundsTestEnable, stencilTestEnable, front, back, minDepthBounds, maxDepthBounds);
    }

    void vkStruct(final VkPipelineDepthStencilStateCreateInfo struct) {                
        struct.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO)
                .flags(this.flags)
                .depthTestEnable(this.depthTestEnable)
                .depthWriteEnable(this.depthWriteEnable)
                .depthCompareOp(this.depthCompareOp.value)
                .depthBoundsTestEnable(this.depthBoundsTestEnable)
                .stencilTestEnable(this.stencilTestEnable);
        
        this.front.vkStruct(struct.front());
        this.back.vkStruct(struct.back());
    }
}
