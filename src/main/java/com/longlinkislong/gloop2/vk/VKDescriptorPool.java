/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDescriptorPoolCreateInfo;
import org.lwjgl.vulkan.VkDescriptorPoolSize;
import org.lwjgl.vulkan.VkDescriptorSetAllocateInfo;
import org.lwjgl.vulkan.VkDevice;

/**
 *
 * @author zmichaels
 */
public class VKDescriptorPool implements VKObject {
    long handle;
    VkDevice deviceHandle;
    VKDescriptorPoolInfo info;
    
    public VKDescriptorPool() {}
    
    public VKDescriptorPool(final VKContext ctx, final VKDescriptorPoolInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkDescriptorPoolSize.Buffer pPoolSizes = VkDescriptorPoolSize.callocStack(info.poolSizes.size(), mem);
            
            for (int i = 0; i < info.poolSizes.size(); i++) {
                final VKDescriptorPoolSize poolSize = info.poolSizes.get(i);
                
                pPoolSizes.get(i)
                        .type(poolSize.type.value)
                        .descriptorCount(poolSize.descriptorCount);
            }
            
            final VkDescriptorPoolCreateInfo createInfo = VkDescriptorPoolCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO)
                    .flags(info.flags)
                    .maxSets(info.maxSets)
                    .pPoolSizes(pPoolSizes);
            
            final LongBuffer pDescriptorPool = mem.callocLong(1);            
            final int err = VK10.vkCreateDescriptorPool(ctx.device.handle, createInfo, null, pDescriptorPool);
            
            assert Tools.vksuccess(err) : Tools.vkerr(err);
            
            this.handle = pDescriptorPool.get(0);
            this.deviceHandle = ctx.device.handle;
            this.info = info;
        }
    }
    
    public void swap(final VKDescriptorPool other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final long tmpHandle = other.handle;
        final VkDevice tmpDev = other.deviceHandle;
        final VKDescriptorPoolInfo tmpInfo = other.info;
        
        other.handle = this.handle;
        other.deviceHandle = this.deviceHandle;
        other.info = this.info;
        
        this.handle = tmpHandle;
        this.deviceHandle = tmpDev;
        this.info = tmpInfo;
    }

    public VKDescriptorPoolInfo getInfo() {
        return this.info;
    }
    
    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyDescriptorPool(this.deviceHandle, handle, null);
            this.deviceHandle = null;
            this.handle = 0L;
            this.info = null;
        }
    }  
    
    public VKDescriptorSet allocate(final VKDescriptorSetLayout layout) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkDescriptorSetAllocateInfo allocateInfo = VkDescriptorSetAllocateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO)
                    .descriptorPool(handle)
                    .pSetLayouts(mem.longs(layout.handle));
            
            final LongBuffer pDescriptorSet = mem.callocLong(1);
            final int err = VK10.vkAllocateDescriptorSets(deviceHandle, allocateInfo, pDescriptorSet);
            
            assert Tools.vksuccess(err) : Tools.vkerr(err);
            
            return new VKDescriptorSet(this.deviceHandle, this.handle, pDescriptorSet.get(0));
        }
    }
    
    public List<VKDescriptorSet> allocate(final VKDescriptorSetLayout... layouts) {
        return allocate(Arrays.asList(layouts));
    }
    
    public List<VKDescriptorSet> allocate(final List<VKDescriptorSetLayout> layouts) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final LongBuffer pSetLayouts = mem.callocLong(layouts.size());
            
            layouts.stream()
                    .mapToLong(layout -> layout.handle)
                    .forEach(pSetLayouts::put);
            
            pSetLayouts.flip();
            
            final VkDescriptorSetAllocateInfo allocateInfo = VkDescriptorSetAllocateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO)
                    .descriptorPool(handle)
                    .pSetLayouts(pSetLayouts);
            
            final LongBuffer pDescriptorSets = mem.callocLong(layouts.size());            
            final int err = VK10.vkAllocateDescriptorSets(deviceHandle, allocateInfo, pDescriptorSets);
            
            assert Tools.vksuccess(err) : Tools.vkerr(err);
            
            final List<VKDescriptorSet> out = new ArrayList<>(layouts.size());
            
            for (int i = 0; i < layouts.size(); i++) {
                out.add(new VKDescriptorSet(deviceHandle, handle, pDescriptorSets.get(i)));
            }
            
            return Collections.unmodifiableList(out);
        }
    }
}
