/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class VKImageSubresourceRange {

    public final Set<VKImageAspect> aspectMask;
    public final int baseMipLevel;
    public final int levelCount;
    public final int baseArrayLayer;
    public final int layerCount;

    public VKImageSubresourceRange(
            final Collection<VKImageAspect> aspectMask,
            final int baseMipLevel, final int levelCount,
            final int baseArrayLayer, final int layerCount) {

        this.aspectMask = Tools.copySet(aspectMask);
        this.baseMipLevel = baseMipLevel;
        this.levelCount = levelCount;
        this.baseArrayLayer = baseArrayLayer;
        this.layerCount = layerCount;
    }
    
    public VKImageSubresourceRange() {
        this(null, 0, 0, 0, 0);
    }
    
    public VKImageSubresourceRange withAspectMask(final Collection<VKImageAspect> aspectMask) {
        return new VKImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }
    
    public VKImageSubresourceRange withAspectMask(final VKImageAspect... aspectMask) {
        return withAspectMask(Arrays.asList(aspectMask));
    }
    
    public VKImageSubresourceRange withBaseMipLevel(final int baseMipLevel) {
        return new VKImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }
    
    public VKImageSubresourceRange withLevelCount(final int levelCount) {
        return new VKImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }
    
    public VKImageSubresourceRange withBaseArrayLayer(final int baseArrayLayer) {
        return new VKImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }
    
    public VKImageSubresourceRange withLayerCount(final int layerCount) {
        return new VKImageSubresourceRange(aspectMask, baseMipLevel, levelCount, baseArrayLayer, layerCount);
    }
    
    public static VKImageSubresourceRange from(final VKImageSubresourceLayers layers) {
        return new VKImageSubresourceRange()
                .withAspectMask(layers.aspectMask)
                .withBaseMipLevel(layers.mipLevel)
                .withLevelCount(1)
                .withBaseArrayLayer(layers.baseArrayLayer)
                .withLayerCount(layers.layerCount);
    }
}
