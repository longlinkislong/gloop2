/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import java.nio.IntBuffer;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPhysicalDevice;
import org.lwjgl.vulkan.VkPhysicalDeviceProperties;
import org.lwjgl.vulkan.VkQueueFamilyProperties;

/**
 *
 * @author zmichaels
 */
public class VKPhysicalDevice implements VKObject {
    
    final VkPhysicalDevice handle;
    final List<VKQueueFamily> queueFamilies;
    public final VkPhysicalDeviceProperties properties;

    public Stream<VKQueueFamily> queueFamilies() {
        return this.queueFamilies.stream();
    }
    
    private String vendorName(final int vendorID) {
        switch (vendorID) {
            case 0x1002:
                return "AMD";
            case 0x1010:
                return "ImgTec";
            case 0x10DE:
                return "NVIDIA";
            case 0x13B5:
                return "ARM";
            case 0x5143:
                return "Qualcomm";
            case 0x8086:
                return "INTEL";
            default:
                return "0x" + Integer.toHexString(vendorID);
        }
    }
    
    public boolean isCPU() {
        return this.properties.deviceType() == VK10.VK_PHYSICAL_DEVICE_TYPE_CPU;
    }
    
    public boolean isDiscreteGPU() {
        return this.properties.deviceType() == VK10.VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
    }
    
    public boolean isIntegratedGPU() {
        return this.properties.deviceType() == VK10.VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU;
    }
    
    public boolean isVirtualGPU() {
        return this.properties.deviceType() == VK10.VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU;
    }
    
    public boolean isGPU() {
        return isDiscreteGPU()
                || isIntegratedGPU()
                || isVirtualGPU();
    }
    
    @Override
    public String toString() {        
        return new StringBuilder(4096)                
                .append("{\n\t\"VendorID\" : \"")
                .append(vendorName(properties.vendorID()))                
                .append("\",\n\t\"DeviceName\" : \"")
                .append(properties.deviceNameString())
                .append("\",\n\t\"DeviceType\" : \"")
                .append(VKPhysicalDeviceType.of(properties.deviceType()).get())
                .append("\",\n\t\"APIVersion\" : \"")                
                .append(VK10.VK_VERSION_MAJOR(properties.apiVersion()))
                .append(".")
                .append(VK10.VK_VERSION_MINOR(properties.apiVersion()))
                .append(".")
                .append(VK10.VK_VERSION_PATCH(properties.apiVersion()))                
                .append("\"\n}")
                .toString();
    }

    VKPhysicalDevice(final VkPhysicalDevice physicalDevice) {
        this.handle = physicalDevice;

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pQueueFamilyCount = mem.callocInt(1);

            VK10.vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, pQueueFamilyCount, null);

            final int queueFamilyCount = pQueueFamilyCount.get(0);

            final VkQueueFamilyProperties.Buffer ppQueueFamilies = VkQueueFamilyProperties.callocStack(queueFamilyCount, mem);

            VK10.vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, pQueueFamilyCount, ppQueueFamilies);

            this.queueFamilies = IntStream.range(0, queueFamilyCount)
                    .mapToObj(queueFamilyIndex -> new VKQueueFamily(this, queueFamilyIndex, ppQueueFamilies))
                    .collect(Collectors.toList());

            this.properties = VkPhysicalDeviceProperties.calloc();

            VK10.vkGetPhysicalDeviceProperties(physicalDevice, this.properties);            
        }
    }

    @Override
    public boolean isValid() {
        return this.properties.address() != 0L;
    }

    @Override
    public void close() {
        this.properties.free();
        this.queueFamilies.clear();
        
    }
}
