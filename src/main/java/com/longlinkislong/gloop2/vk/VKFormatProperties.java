/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import java.util.Set;
import org.lwjgl.vulkan.VkFormatProperties;

/**
 *
 * @author zmichaels
 */
public class VKFormatProperties {
    public final Set<VKFormatFeature> linearTilingFeatures;
    public final Set<VKFormatFeature> optimalTilingFeatures;
    public final Set<VKFormatFeature> bufferFeatures;
    
    VKFormatProperties(final VkFormatProperties vkstruct) {
        this.linearTilingFeatures = VKFormatFeature.flags(vkstruct.linearTilingFeatures());
        this.optimalTilingFeatures = VKFormatFeature.flags(vkstruct.optimalTilingFeatures());
        this.bufferFeatures = VKFormatFeature.flags(vkstruct.bufferFeatures());
    }
}
