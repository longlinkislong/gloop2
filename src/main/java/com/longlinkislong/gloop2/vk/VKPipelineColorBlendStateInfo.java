/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineColorBlendAttachmentState;
import org.lwjgl.vulkan.VkPipelineColorBlendStateCreateInfo;

public final class VKPipelineColorBlendStateInfo {

    public final int flags;
    public final boolean logicOpEnable;
    public final VKLogicOp logicOp;
    public final List<VKPipelineColorBlendAttachmentState> attachments;
    public final float blendConstantR;
    public final float blendConstantG;
    public final float blendConstantB;
    public final float blendConstantA;

    public VKPipelineColorBlendStateInfo(
            final int flags,
            final boolean logicOpEnable,
            final VKLogicOp logicOp,
            final List<VKPipelineColorBlendAttachmentState> attachments,
            final float blendConstantR, final float blendConstantG, final float blendConstantB, final float blendConstantA) {

        this.flags = flags;
        this.logicOpEnable = logicOpEnable;
        this.logicOp = logicOp;
        this.attachments = Tools.copyList(attachments);
        this.blendConstantR = blendConstantR;
        this.blendConstantG = blendConstantG;
        this.blendConstantB = blendConstantB;
        this.blendConstantA = blendConstantA;
    }

    public VKPipelineColorBlendStateInfo() {
        this(0, false, VKLogicOp.NO_OP, null, 0F, 0F, 0F, 0F);
    }

    public VKPipelineColorBlendStateInfo withFlags(final int flags) {
        return new VKPipelineColorBlendStateInfo(flags, logicOpEnable, logicOp, attachments, blendConstantR, blendConstantG, blendConstantB, blendConstantA);
    }

    public VKPipelineColorBlendStateInfo withLogicOpEnable(final boolean logicOpEnable) {
        return new VKPipelineColorBlendStateInfo(flags, logicOpEnable, logicOp, attachments, blendConstantR, blendConstantG, blendConstantB, blendConstantA);
    }

    public VKPipelineColorBlendStateInfo withLogicOp(final VKLogicOp logicOp) {
        return new VKPipelineColorBlendStateInfo(0, logicOpEnable, logicOp, attachments, blendConstantR, blendConstantG, blendConstantB, blendConstantA);
    }

    public VKPipelineColorBlendStateInfo withAttachments(final List<VKPipelineColorBlendAttachmentState> attachments) {
        return new VKPipelineColorBlendStateInfo(0, logicOpEnable, logicOp, attachments, blendConstantR, blendConstantG, blendConstantB, blendConstantA);
    }

    public VKPipelineColorBlendStateInfo withAttachments(final VKPipelineColorBlendAttachmentState... attachments) {
        return withAttachments(Arrays.asList(attachments));
    }

    public VKPipelineColorBlendStateInfo withBlendConstant(final float blendConstantR, final float blendConstantG, final float blendConstantB, final float blendConstantA) {
        return new VKPipelineColorBlendStateInfo(0, logicOpEnable, logicOp, attachments, blendConstantR, blendConstantG, blendConstantB, blendConstantA);
    }
    
    void vkStruct(final VkPipelineColorBlendStateCreateInfo struct, final MemoryStack mem) {
        final VkPipelineColorBlendAttachmentState.Buffer pAttachments = VkPipelineColorBlendAttachmentState.callocStack(this.attachments.size(), mem);
        
        for (int i = 0; i < this.attachments.size(); i++) {
            this.attachments.get(i).vkStruct(pAttachments.get(i));
        }
        
        struct.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO)
                .flags(this.flags)
                .logicOpEnable(this.logicOpEnable)
                .logicOp(this.logicOp.value)
                .pAttachments(pAttachments)
                .blendConstants(0, this.blendConstantR)
                .blendConstants(1, this.blendConstantG)
                .blendConstants(2, this.blendConstantB)
                .blendConstants(3, this.blendConstantA);
    }
}
