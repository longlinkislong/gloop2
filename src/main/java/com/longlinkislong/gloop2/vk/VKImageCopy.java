/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKImageCopy {
    public final VKImageSubresourceLayers srcSubresource;
    public final int srcOffsetX;
    public final int srcOffsetY;
    public final int srcOffsetZ;
    public final VKImageSubresourceLayers dstSubresource;
    public final int dstOffsetX;
    public final int dstOffsetY;
    public final int dstOffsetZ;
    public final int width;
    public final int height;
    public final int depth;
    
    public VKImageCopy(
            final VKImageSubresourceLayers srcSubresource,
            final int srcOffsetX, final int srcOffsetY, final int srcOffsetZ,
            final VKImageSubresourceLayers dstSubresource,
            final int dstOffsetX, final int dstOffsetY, final int dstOffsetZ,
            final int width, final int height, final int depth) {
        
        this.srcSubresource = srcSubresource;
        this.srcOffsetX = srcOffsetX;
        this.srcOffsetY = srcOffsetY;
        this.srcOffsetZ = srcOffsetZ;
        this.dstSubresource = dstSubresource;
        this.dstOffsetX = dstOffsetX;
        this.dstOffsetY = dstOffsetY;
        this.dstOffsetZ = dstOffsetZ;
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
    
    public VKImageCopy() {
        this(null, 0, 0, 0, null, 0, 0, 0, 0, 0, 0);
    }
    
    public VKImageCopy withSrcSubresource(final VKImageSubresourceLayers srcSubresource) {
        return new VKImageCopy(srcSubresource, srcOffsetX, srcOffsetY, srcOffsetZ, dstSubresource, dstOffsetX, dstOffsetY, dstOffsetZ, width, height, depth);
    }
    
    public VKImageCopy withSrcOffset(final int srcOffsetX, final int srcOffsetY, final int srcOffsetZ) {
        return new VKImageCopy(srcSubresource, srcOffsetX, srcOffsetY, srcOffsetZ, dstSubresource, dstOffsetX, dstOffsetY, dstOffsetZ, width, height, depth);
    }
    
    public VKImageCopy withDstSubresource(final VKImageSubresourceLayers dstSubresource) {
        return new VKImageCopy(srcSubresource, srcOffsetX, srcOffsetY, srcOffsetZ, dstSubresource, dstOffsetX, dstOffsetY, dstOffsetZ, width, height, depth);
    }
    
    public VKImageCopy withDstOffset(final int dstOffsetX, final int dstOffsetY, final int dstOffsetZ) {
        return new VKImageCopy(srcSubresource, srcOffsetX, srcOffsetY, srcOffsetZ, dstSubresource, dstOffsetX, dstOffsetY, dstOffsetZ, width, height, depth);
    }
    
    public VKImageCopy withExtent(final int width, final int height, final int depth) {
        return new VKImageCopy(srcSubresource, srcOffsetX, srcOffsetY, srcOffsetZ, dstSubresource, dstOffsetX, dstOffsetY, dstOffsetZ, width, height, depth);
    }
}
