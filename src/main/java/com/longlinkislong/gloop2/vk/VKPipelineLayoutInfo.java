/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class VKPipelineLayoutInfo {

    public final int flags;
    public final List<VKDescriptorSetLayout> setLayouts;
    public final List<VKPushConstantRange> pushConstantRanges;

    public VKPipelineLayoutInfo(
            final int flags,
            final List<VKDescriptorSetLayout> setLayouts,
            final List<VKPushConstantRange> pushConstantRanges) {

        this.flags = flags;
        this.setLayouts = Tools.copyList(setLayouts);
        this.pushConstantRanges = Tools.copyList(pushConstantRanges);
    }
    
    public VKPipelineLayoutInfo() {
        this(0, null, null);
    }
    
    public VKPipelineLayoutInfo withFlags(final int flags) {
        return new VKPipelineLayoutInfo(flags, setLayouts, pushConstantRanges);
    }
    
    public VKPipelineLayoutInfo withSetLayouts(final List<VKDescriptorSetLayout> setLayouts) {
        return new VKPipelineLayoutInfo(flags, setLayouts, pushConstantRanges);
    }
    
    public VKPipelineLayoutInfo withSetLayouts(final VKDescriptorSetLayout... setLayouts) {
        return withSetLayouts(Arrays.asList(setLayouts));
    }
    
    public VKPipelineLayoutInfo withPushConstantRanges(final List<VKPushConstantRange> pushConstantRanges) {
        return new VKPipelineLayoutInfo(flags, setLayouts, pushConstantRanges);
    }
    
    public VKPipelineLayoutInfo withPushConstantRanges(final VKPushConstantRange... pushConstantRanges) {
        return withPushConstantRanges(Arrays.asList(pushConstantRanges));
    }
    
    public VKPipelineLayout create(final VKContext ctx) {
        return new VKPipelineLayout(ctx, this);
    }
}
