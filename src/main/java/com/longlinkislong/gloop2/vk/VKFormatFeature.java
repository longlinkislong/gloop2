/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKFormatFeature implements CBitfield {
    SAMPLED_IMAGE(VK10.VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT),
    STORAGE_IMAGE(VK10.VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT),
    STORAGE_IMAGE_ATOMIC(VK10.VK_FORMAT_FEATURE_STORAGE_IMAGE_ATOMIC_BIT),
    UNIFORM_TEXEL_BUFFER(VK10.VK_FORMAT_FEATURE_UNIFORM_TEXEL_BUFFER_BIT),
    STORAGE_TEXEL_BUFFER(VK10.VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_BIT),
    STORAGE_TEXEL_BUFFER_ATOMIC(VK10.VK_FORMAT_FEATURE_STORAGE_TEXEL_BUFFER_ATOMIC_BIT),
    VERTEX_BUFFER(VK10.VK_FORMAT_FEATURE_VERTEX_BUFFER_BIT),
    COLOR_ATTACHMENT(VK10.VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT),
    COLOR_ATTACHMENT_BLEND(VK10.VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT),
    DEPTH_STENCIL_ATTACHMENT(VK10.VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT),
    BLIT_SRC(VK10.VK_FORMAT_FEATURE_BLIT_SRC_BIT),
    BLIT_DST(VK10.VK_FORMAT_FEATURE_BLIT_DST_BIT),
    SAMPLED_IMAGE_FILTER_LINEAR(VK10.VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT);

    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }

    private VKFormatFeature(final int value) {
        this.value = value;
    }

    public static Set<VKFormatFeature> flags(final int bits) {
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<VKFormatFeature> flags) {
        return Tools.flagsToBitfield(flags);
    }
}
