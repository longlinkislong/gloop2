/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkGraphicsPipelineCreateInfo;
import org.lwjgl.vulkan.VkPipelineColorBlendStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineDepthStencilStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineDynamicStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineInputAssemblyStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineMultisampleStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineRasterizationStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineShaderStageCreateInfo;
import org.lwjgl.vulkan.VkPipelineTessellationStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineVertexInputStateCreateInfo;
import org.lwjgl.vulkan.VkPipelineViewportStateCreateInfo;

/**
 *
 * @author zmichaels
 */
public class VKGraphicsPipeline implements VKObject {

    long handle;
    VkDevice deviceHandle;
    VKGraphicsPipelineInfo info;

    public VKGraphicsPipeline() {}
    
    public VKGraphicsPipeline(final VKContext ctx, final VKGraphicsPipelineInfo info) {
        this(ctx, info, null);
    }
    
    public VKGraphicsPipeline(final VKContext ctx, final VKGraphicsPipelineInfo info, final VKPipelineCache cache) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkPipelineShaderStageCreateInfo.Buffer pStages = VkPipelineShaderStageCreateInfo.callocStack(info.stages.size(), mem);

            for (int i = 0; i < info.stages.size(); i++) {
                info.stages.get(i).vkStruct(pStages.get(i), mem);
            }

            final VkPipelineVertexInputStateCreateInfo pVertexInputState = VkPipelineVertexInputStateCreateInfo.callocStack(mem);

            info.vertexInputState.vkStruct(pVertexInputState, mem);

            final VkPipelineInputAssemblyStateCreateInfo pInputAssemblyState = VkPipelineInputAssemblyStateCreateInfo.callocStack(mem);

            info.inputAssemblyState.vkStruct(pInputAssemblyState);

            final VkPipelineTessellationStateCreateInfo pTessellationState;

            if (info.tessellationState == null) {
                pTessellationState = null;
            } else {
                pTessellationState = VkPipelineTessellationStateCreateInfo.callocStack(mem);
                info.tessellationState.vkStruct(pTessellationState);
            }

            final VkPipelineViewportStateCreateInfo pViewportState;

            if (info.viewportState == null) {
                pViewportState = null;
            } else {
                pViewportState = VkPipelineViewportStateCreateInfo.callocStack(mem);
                info.viewportState.vkStruct(pViewportState, mem);
            }

            final VkPipelineRasterizationStateCreateInfo pRasterizationState = VkPipelineRasterizationStateCreateInfo.callocStack(mem);

            info.rasterizationState.vkStruct(pRasterizationState);

            final VkPipelineMultisampleStateCreateInfo pMultisampleState;

            if (info.multisampleState == null) {
                pMultisampleState = null;
            } else {
                pMultisampleState = VkPipelineMultisampleStateCreateInfo.callocStack(mem);
                info.multisampleState.vkStruct(pMultisampleState);
            }

            final VkPipelineDepthStencilStateCreateInfo pDepthStencilState;

            if (info.depthStencilState == null) {
                pDepthStencilState = null;
            } else {
                pDepthStencilState = VkPipelineDepthStencilStateCreateInfo.callocStack(mem);
                info.depthStencilState.vkStruct(pDepthStencilState);
            }

            final VkPipelineColorBlendStateCreateInfo pColorBlendState;

            if (info.colorBlendState == null) {
                pColorBlendState = null;
            } else {
                pColorBlendState = VkPipelineColorBlendStateCreateInfo.callocStack(mem);
                info.colorBlendState.vkStruct(pColorBlendState, mem);
            }

            final VkPipelineDynamicStateCreateInfo pDynamicState;

            if (info.dynamicState == null) {
                pDynamicState = null;
            } else {
                pDynamicState = VkPipelineDynamicStateCreateInfo.callocStack(mem);
                info.dynamicState.vkStruct(pDynamicState, mem);
            }            
            
            final VkGraphicsPipelineCreateInfo.Buffer createInfo = VkGraphicsPipelineCreateInfo.callocStack(1, mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO)
                    .flags(VKGraphicsPipelineInfo.CreateFlag.bits(info.flags))
                    .pStages(pStages)
                    .pVertexInputState(pVertexInputState)
                    .pInputAssemblyState(pInputAssemblyState)
                    .pTessellationState(pTessellationState)
                    .pViewportState(pViewportState)
                    .pRasterizationState(pRasterizationState)
                    .pMultisampleState(pMultisampleState)
                    .pDepthStencilState(pDepthStencilState)
                    .pColorBlendState(pColorBlendState)
                    .pDynamicState(pDynamicState)
                    .layout(info.layout.handle)
                    .renderPass(info.renderPass.handle)
                    .subpass(info.subpass)
                    .basePipelineHandle(info.basePipeline == null ? 0L : info.basePipeline.handle)
                    .basePipelineIndex(info.basePipelineIndex);

            final LongBuffer pHandle = mem.callocLong(1);
            final int err = VK10.vkCreateGraphicsPipelines(ctx.device.handle, cache == null ? 0 : cache.handle, createInfo, null, pHandle);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            this.handle = pHandle.get(0);
            this.deviceHandle = ctx.device.handle;
            this.info = info;
        }
    }

    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyPipeline(deviceHandle, handle, null);
            this.deviceHandle = null;
            this.handle = 0L;
            this.info = null;
        }
    }
}
