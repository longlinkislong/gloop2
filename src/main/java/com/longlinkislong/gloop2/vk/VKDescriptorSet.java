/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;

/**
 *
 * @author zmichaels
 */
public class VKDescriptorSet implements VKObject {

    long handle;
    long pool;
    VkDevice deviceHandle;

    public VKDescriptorSet() {}
    
    VKDescriptorSet(VkDevice deviceHandle, long pool, long handle) {
        this.deviceHandle = deviceHandle;
        this.pool = pool;
        this.handle = handle;
    }
    
    public void swap(final VKDescriptorSet other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final long tmpHandle = other.handle;
        final long tmpPool = other.pool;
        final VkDevice tmpDev = other.deviceHandle;
        
        other.handle = tmpHandle;
        other.pool = tmpPool;
        other.deviceHandle = tmpDev;
        
        this.handle = tmpHandle;
        this.pool = tmpPool;
        this.deviceHandle = tmpDev;
    }
    
    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkFreeDescriptorSets(deviceHandle, pool, handle);
            this.handle = 0L;
            this.pool = 0L;
            this.deviceHandle = null;
        }
    }

}
