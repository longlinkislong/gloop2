/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKPolygonMode implements CEnum {
    FILL(VK10.VK_POLYGON_MODE_FILL),
    LINE(VK10.VK_POLYGON_MODE_LINE),
    POINT(VK10.VK_POLYGON_MODE_POINT);

    public final int value;

    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKPolygonMode(final int value) {
        this.value = value;
    }

    public static Optional<VKPolygonMode> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
