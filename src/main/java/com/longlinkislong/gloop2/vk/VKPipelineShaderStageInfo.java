/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import java.util.Objects;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineShaderStageCreateInfo;
import org.lwjgl.vulkan.VkSpecializationInfo;

/**
 *
 * @author zmichaels
 */
public final class VKPipelineShaderStageInfo {

    public final int flags;
    public final VKShaderStage stage;
    public final VKShaderModule module;
    public final String name;
    public final VKSpecializationInfo specializationInfo;

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        } else if (other instanceof VKPipelineShaderStageInfo) {
            return this.stage == ((VKPipelineShaderStageInfo) other).stage;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.stage);
        return hash;
    }

    public VKPipelineShaderStageInfo(
            final int flags,
            final VKShaderStage stage,
            final VKShaderModule module,
            final String name,
            final VKSpecializationInfo specializationInfo) {

        this.flags = flags;
        this.stage = stage;
        this.module = module;
        this.name = name;
        this.specializationInfo = specializationInfo;
    }

    public VKPipelineShaderStageInfo() {
        this(0, null, null, "main", null);
    }

    public VKPipelineShaderStageInfo withFlags(final int flags) {
        return new VKPipelineShaderStageInfo(flags, stage, module, name, specializationInfo);
    }

    public VKPipelineShaderStageInfo withStage(final VKShaderStage stage) {
        return new VKPipelineShaderStageInfo(flags, stage, module, name, specializationInfo);
    }

    public VKPipelineShaderStageInfo withModule(final VKShaderModule module) {
        return new VKPipelineShaderStageInfo(flags, stage, module, name, specializationInfo);
    }

    public VKPipelineShaderStageInfo withName(final String name) {
        return new VKPipelineShaderStageInfo(flags, stage, module, name, specializationInfo);
    }

    public VKPipelineShaderStageInfo withSpecializationInfo(final VKSpecializationInfo specializationInfo) {
        return new VKPipelineShaderStageInfo(flags, stage, module, name, specializationInfo);
    }

    void vkStruct(final VkPipelineShaderStageCreateInfo struct, final MemoryStack mem) {
        final VkSpecializationInfo pSpecializationInfo = this.specializationInfo == null
                ? null
                : this.specializationInfo.vkStruct(mem);

        struct.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO)
                .flags(this.flags)
                .stage(this.stage.value)
                .module(this.module.handle)
                .pName(mem.UTF8(this.name, true))
                .pSpecializationInfo(pSpecializationInfo);
    }
    
    public static VKPipelineShaderStageInfo vertexShader(final VKShaderModule module) {
        return new VKPipelineShaderStageInfo()
                .withModule(module)
                .withStage(VKShaderStage.VERTEX);
    }
    
    public static VKPipelineShaderStageInfo tessControlShader(final VKShaderModule module) {
        return new VKPipelineShaderStageInfo()
                .withModule(module)
                .withStage(VKShaderStage.TESS_CONTROL);
    }
    
    public static VKPipelineShaderStageInfo tessEvaluationShader(final VKShaderModule module) {
        return new VKPipelineShaderStageInfo()
                .withModule(module)
                .withStage(VKShaderStage.TESS_EVALUATION);
    }
    
    public static VKPipelineShaderStageInfo geometryShader(final VKShaderModule module) {
        return new VKPipelineShaderStageInfo()
                .withModule(module)
                .withStage(VKShaderStage.GEOMETRY);
    }
    
    public static VKPipelineShaderStageInfo fragmentShader(final VKShaderModule module) {
        return new VKPipelineShaderStageInfo()
                .withModule(module)
                .withStage(VKShaderStage.FRAGMENT);
    }
}
