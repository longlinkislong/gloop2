/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKComponentMapping {

    public final VKComponentSwizzle r;
    public final VKComponentSwizzle g;
    public final VKComponentSwizzle b;
    public final VKComponentSwizzle a;

    public VKComponentMapping(
            final VKComponentSwizzle r,
            final VKComponentSwizzle g,
            final VKComponentSwizzle b,
            final VKComponentSwizzle a) {

        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public VKComponentMapping() {
        this(VKComponentSwizzle.IDENTITY, VKComponentSwizzle.IDENTITY, VKComponentSwizzle.IDENTITY, VKComponentSwizzle.IDENTITY);
    }
    
    public VKComponentMapping withR(final VKComponentSwizzle r) {
        return new VKComponentMapping(r, g, b, a);
    }
    
    public VKComponentMapping withG(final VKComponentSwizzle g) {
        return new VKComponentMapping(r, g, b, a);
    }
    
    public VKComponentMapping withB(final VKComponentSwizzle b) {
        return new VKComponentMapping(r, g, b, a);
    }
    
    public VKComponentMapping withA(final VKComponentSwizzle a) {
        return new VKComponentMapping(r, g, b, a);
    }
}
