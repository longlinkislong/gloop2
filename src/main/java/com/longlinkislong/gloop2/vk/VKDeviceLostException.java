/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKDeviceLostException extends RuntimeException {
    public VKDeviceLostException(final String msg) {
        super(msg);
    }
    
    public VKDeviceLostException() {
        super("The device has been lost.");
    }
}
