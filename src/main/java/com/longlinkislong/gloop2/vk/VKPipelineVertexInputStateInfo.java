/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineVertexInputStateCreateInfo;
import org.lwjgl.vulkan.VkVertexInputAttributeDescription;
import org.lwjgl.vulkan.VkVertexInputBindingDescription;

/**
 *
 * @author zmichaels
 */
public class VKPipelineVertexInputStateInfo {

    public static final class BindingDescription {

        public final int binding;
        public final int stride;
        public final VKVertexInputRate inputRate;

        public BindingDescription(final int binding, final int stride, final VKVertexInputRate inputRate) {
            this.binding = binding;
            this.stride = stride;
            this.inputRate = inputRate;
        }

        public BindingDescription() {
            this(0, 0, VKVertexInputRate.VERTEX);
        }

        public BindingDescription withBinding(final int binding) {
            return new BindingDescription(binding, stride, inputRate);
        }

        public BindingDescription withStride(final int stride) {
            return new BindingDescription(binding, stride, inputRate);
        }

        public BindingDescription withInputRate(final VKVertexInputRate inputRate) {
            return new BindingDescription(binding, stride, inputRate);
        }
    }

    public static final class AttributeDescription {

        public final int location;
        public final int binding;
        public final VKFormat format;
        public final int offset;

        public AttributeDescription(final int location, final int binding, final VKFormat format, final int offset) {
            this.location = location;
            this.binding = binding;
            this.format = format;
            this.offset = offset;
        }

        public AttributeDescription() {
            this(0, 0, VKFormat.VEC4, 0);
        }

        public AttributeDescription withLocation(final int location) {
            return new AttributeDescription(location, binding, format, offset);
        }

        public AttributeDescription withBinding(final int binding) {
            return new AttributeDescription(location, binding, format, offset);
        }

        public AttributeDescription withFormat(final VKFormat format) {
            return new AttributeDescription(location, binding, format, offset);
        }

        public AttributeDescription withOffset(final int offset) {
            return new AttributeDescription(location, binding, format, offset);
        }
    }

    public final int flags;
    public final List<BindingDescription> bindingDescription;
    public final List<AttributeDescription> attributeDescriptions;

    public VKPipelineVertexInputStateInfo(
            final int flags,
            final List<BindingDescription> bindingDescriptions,
            final List<AttributeDescription> attributeDescriptions) {

        this.flags = flags;
        this.bindingDescription = Tools.copyList(bindingDescriptions);
        this.attributeDescriptions = Tools.copyList(attributeDescriptions);
    }

    public VKPipelineVertexInputStateInfo() {
        this(0, null, null);
    }

    public VKPipelineVertexInputStateInfo withFlags(final int flags) {
        return new VKPipelineVertexInputStateInfo(flags, bindingDescription, attributeDescriptions);
    }

    public VKPipelineVertexInputStateInfo withBindingDescriptions(final List<BindingDescription> bindingDescriptions) {
        return new VKPipelineVertexInputStateInfo(flags, bindingDescriptions, attributeDescriptions);
    }

    public VKPipelineVertexInputStateInfo withBindingDescriptions(final BindingDescription... bindingDescriptions) {
        return withBindingDescriptions(Arrays.asList(bindingDescriptions));
    }

    public VKPipelineVertexInputStateInfo withAttributeDescriptions(final List<AttributeDescription> attributeDescriptions) {
        return new VKPipelineVertexInputStateInfo(flags, bindingDescription, attributeDescriptions);
    }

    public VKPipelineVertexInputStateInfo withAttributeDescriptions(final AttributeDescription... attributeDescriptions) {
        return withAttributeDescriptions(Arrays.asList(attributeDescriptions));
    }
    
    void vkStruct(final VkPipelineVertexInputStateCreateInfo struct, final MemoryStack mem) {
        final VkVertexInputBindingDescription.Buffer pBindingDescriptions = VkVertexInputBindingDescription.callocStack(this.bindingDescription.size(), mem);
        
        for (int i = 0; i < this.bindingDescription.size(); i++) {
            final BindingDescription desc = this.bindingDescription.get(i);
            
            pBindingDescriptions.get(i)
                    .binding(desc.binding)
                    .stride(desc.stride)
                    .inputRate(desc.inputRate.value);
        }
        
        final VkVertexInputAttributeDescription.Buffer pAttributeDescriptions = VkVertexInputAttributeDescription.callocStack(this.attributeDescriptions.size(), mem);
        
        for (int i = 0; i < this.attributeDescriptions.size(); i++) {
            final AttributeDescription desc = this.attributeDescriptions.get(i);
            
            pAttributeDescriptions.get(i)
                    .location(desc.location)
                    .binding(desc.binding)
                    .format(desc.format.value)
                    .offset(desc.offset);
        }
        
        struct.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO)
                .flags(this.flags)
                .pVertexBindingDescriptions(pBindingDescriptions)
                .pVertexAttributeDescriptions(pAttributeDescriptions);
    }
}
