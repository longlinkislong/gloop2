/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKBlendFactor implements CEnum {
    ZERO(VK10.VK_BLEND_FACTOR_ZERO),
    ONE(VK10.VK_BLEND_FACTOR_ONE),
    SRC_COLOR(VK10.VK_BLEND_FACTOR_SRC_COLOR),
    ONE_MINUS_SRC_COLOR(VK10.VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR),
    DST_COLOR(VK10.VK_BLEND_FACTOR_DST_COLOR),
    ONE_MINUS_DST_COLOR(VK10.VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR),
    SRC_ALPHA(VK10.VK_BLEND_FACTOR_SRC_ALPHA),
    ONE_MINUS_SRC_ALPHA(VK10.VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA),
    DST_ALPHA(VK10.VK_BLEND_FACTOR_DST_ALPHA),
    ONE_MINUS_DST_ALPHA(VK10.VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA),
    CONSTANT_COLOR(VK10.VK_BLEND_FACTOR_CONSTANT_COLOR),
    ONE_MINUS_CONSTANT_COLOR(VK10.VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR),
    CONSTANT_ALPHA(VK10.VK_BLEND_FACTOR_CONSTANT_ALPHA),
    ONE_MINUS_CONSTANT_ALPHA(VK10.VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA),
    SRC_ALPHA_SATURATE(VK10.VK_BLEND_FACTOR_SRC_ALPHA_SATURATE),
    SRC1_COLOR(VK10.VK_BLEND_FACTOR_SRC1_COLOR),
    ONE_MINUS_SRC1_COLOR(VK10.VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR),
    SRC1_ALPHA(VK10.VK_BLEND_FACTOR_SRC1_ALPHA),
    ONE_MINUS_SRC1_ALPHA(VK10.VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA);
    
    public final int value;
    
    @Override
    public int getValue() {
        return value;
    }
    
    private VKBlendFactor(final int value) {
        this.value = value;
    }
    
    public static Optional<VKBlendFactor> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
