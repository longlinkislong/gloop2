/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKImageUsage implements CBitfield {
    TRANSFER_SRC(VK10.VK_IMAGE_USAGE_TRANSFER_SRC_BIT),
    TRANSFER_DST(VK10.VK_IMAGE_USAGE_TRANSFER_DST_BIT),
    SAMPLED(VK10.VK_IMAGE_USAGE_SAMPLED_BIT),
    STORAGE(VK10.VK_IMAGE_USAGE_STORAGE_BIT),
    COLOR_ATTACHMENT(VK10.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT),
    DEPTH_STENCIL_ATTACHMENT(VK10.VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT),
    TRANSIENT_ATTACHMENT(VK10.VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT),
    INPUT_ATTACHMENT(VK10.VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT);

    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }

    private VKImageUsage(final int value) {
        this.value = value;
    }

    public static Set<VKImageUsage> flags(final int bits) {
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<VKImageUsage> flags) {
        return Tools.flagsToBitfield(flags);
    }
}
