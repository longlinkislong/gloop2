/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKPrimitiveTopology implements CEnum {
    POINT_LIST(VK10.VK_PRIMITIVE_TOPOLOGY_POINT_LIST),
    LINE_LIST(VK10.VK_PRIMITIVE_TOPOLOGY_LINE_LIST),
    LINE_STRIP(VK10.VK_PRIMITIVE_TOPOLOGY_LINE_STRIP),
    TRIANGLE_LIST(VK10.VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST),
    TRIANGLE_STRIP(VK10.VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP),
    TRIANGLE_FAN(VK10.VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN),
    LINE_LIST_WITH_ADJACENCY(VK10.VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY),
    LINE_STRIP_WITH_ADJACENCY(VK10.VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY),
    TRIANGLE_LIST_WITH_ADJACENCY(VK10.VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY),
    TRIANGLE_STRIP_WITH_ADJACENCY(VK10.VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY),
    PATCH_LIST(VK10.VK_PRIMITIVE_TOPOLOGY_PATCH_LIST);

    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }

    private VKPrimitiveTopology(final int value) {
        this.value = value;
    }

    public static Optional<VKPrimitiveTopology> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
