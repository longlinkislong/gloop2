/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKBlendOp implements CEnum {
    ADD(VK10.VK_BLEND_OP_ADD),
    SUBTRACT(VK10.VK_BLEND_OP_SUBTRACT),
    REVERSE_SUBTRACT(VK10.VK_BLEND_OP_REVERSE_SUBTRACT),
    MIN(VK10.VK_BLEND_OP_MIN),
    MAX(VK10.VK_BLEND_OP_MAX);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKBlendOp(final int value) {
        this.value = value;
    }
    
    public static Optional<VKBlendOp> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
