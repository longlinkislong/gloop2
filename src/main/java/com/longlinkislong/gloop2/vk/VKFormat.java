/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import static com.longlinkislong.gloop2.vk.VKDataFormat.*;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkFormatProperties;

/**
 *
 * @author zmichaels
 */
public enum VKFormat implements CEnum {
    UNDEFINED(
            VK10.VK_FORMAT_UNDEFINED,
            0, 0, 0, 0,
            0, 0,
            VKDataFormat.UNKNOWN),
    R8_UNORM(
            VK10.VK_FORMAT_R8_UNORM,
            8, 0, 0, 0,
            0, 0, VKDataFormat.UNORM),
    R8_SNORM(
            VK10.VK_FORMAT_R8_SNORM,
            8, 0, 0, 0,
            0, 0, SNORM),
    R8_UINT(
            VK10.VK_FORMAT_R8_UINT,
            8, 0, 0, 0,
            0, 0, VKDataFormat.UINT),
    R8_SINT(
            VK10.VK_FORMAT_R8_SINT,
            8, 0, 0, 0,
            0, 0,
            VKDataFormat.SINT),
    R8G8_UNORM(
            VK10.VK_FORMAT_R8G8_UNORM,
            8, 8, 0, 0,
            0, 0,
            VKDataFormat.UNORM),
    R8G8_SNORM(
            VK10.VK_FORMAT_R8G8_SNORM,
            8, 8, 0, 0,
            0, 0,
            VKDataFormat.SNORM),
    R8G8_UINT(
            VK10.VK_FORMAT_R8G8_UINT,
            8, 8, 0, 0,
            0, 0,
            VKDataFormat.UINT),
    R8G8_SINT(
            VK10.VK_FORMAT_R8G8_SINT,
            8, 8, 0, 0,
            0, 0,
            VKDataFormat.SINT),
    R8G8B8_UNORM(
            VK10.VK_FORMAT_R8G8B8_UNORM,
            8, 8, 8, 0,
            0, 0,
            VKDataFormat.UNORM),
    R8G8B8_SNORM(
            VK10.VK_FORMAT_R8G8B8_SNORM,
            8, 8, 8, 0,
            0, 0,
            VKDataFormat.SNORM),
    R8G8B8_UINT(
            VK10.VK_FORMAT_R8G8B8_UINT,
            8, 8, 8, 0,
            0, 0,
            VKDataFormat.UINT),
    R8G8B8_SINT(
            VK10.VK_FORMAT_R8G8B8_SINT,
            8, 8, 8, 0,
            0, 0,
            VKDataFormat.SINT),
    B8G8R8_UNORM(
            VK10.VK_FORMAT_B8G8R8_UNORM,
            8, 8, 8, 0,
            0, 0,
            VKDataFormat.UNORM),
    B8G8R8_SNORM(
            VK10.VK_FORMAT_B8G8R8_SNORM,
            8, 8, 8, 0,
            0, 0,
            VKDataFormat.SNORM),
    B8G8R8_UINT(
            VK10.VK_FORMAT_B8G8R8_UINT,
            8, 8, 8, 0,
            0, 0,
            VKDataFormat.UINT),
    B8G8R8_SINT(
            VK10.VK_FORMAT_B8G8R8_SINT,
            8, 8, 8, 0,
            0, 0,
            VKDataFormat.SINT),
    R8G8B8A8_UNORM(
            VK10.VK_FORMAT_R8G8B8A8_UNORM,
            8, 8, 8, 8,
            0, 0,
            VKDataFormat.UNORM),
    R8G8B8A8_SNORM(
            VK10.VK_FORMAT_R8G8B8A8_SNORM,
            8, 8, 8, 8,
            0, 0,
            VKDataFormat.SNORM),
    R8G8B8A8_UINT(
            VK10.VK_FORMAT_R8G8B8A8_UINT,
            8, 8, 8, 8,
            0, 0,
            VKDataFormat.UINT),
    R8G8B8A8_SINT(
            VK10.VK_FORMAT_R8G8B8A8_SINT,
            8, 8, 8, 8,
            0, 0,
            VKDataFormat.SINT),
    B8G8R8A8_UNORM(
            VK10.VK_FORMAT_B8G8R8A8_UNORM,
            8, 8, 8, 8,
            0, 0,
            VKDataFormat.UNORM),
    B8G8R8A8_SNORM(
            VK10.VK_FORMAT_B8G8R8A8_SNORM,
            8, 8, 8, 8,
            0, 0,
            VKDataFormat.SNORM),
    B8G8R8A8_UINT(
            VK10.VK_FORMAT_B8G8R8A8_UINT,
            8, 8, 8, 8,
            0, 0,
            VKDataFormat.UINT),
    B8G8R8A8_SINT(
            VK10.VK_FORMAT_B8G8R8A8_SINT,
            8, 8, 8, 8,
            0, 0,
            VKDataFormat.SINT),
    R16_UNORM(
            VK10.VK_FORMAT_R16_UNORM,
            16, 0, 0, 0,
            0, 0,
            VKDataFormat.UNORM),
    R16_SNORM(
            VK10.VK_FORMAT_R16_SNORM,
            16, 0, 0, 0,
            0, 0,
            VKDataFormat.SNORM),
    R16_UINT(
            VK10.VK_FORMAT_R16_UINT,
            16, 0, 0, 0,
            0, 0,
            VKDataFormat.UINT),
    R16_SINT(
            VK10.VK_FORMAT_R16_SINT,
            16, 0, 0, 0,
            0, 0,
            VKDataFormat.SINT),
    R16_SFLOAT(
            VK10.VK_FORMAT_R16_SFLOAT,
            16, 0, 0, 0,
            0, 0,
            VKDataFormat.SFLOAT),
    R16G16_UNORM(
            VK10.VK_FORMAT_R16G16_UNORM,
            16, 16, 0, 0,
            0, 0,
            VKDataFormat.UNORM),
    R16G16_SNORM(
            VK10.VK_FORMAT_R16G16_SNORM,
            16, 16, 0, 0,
            0, 0,
            VKDataFormat.SNORM),
    R16G16_UINT(
            VK10.VK_FORMAT_R16G16_UINT,
            16, 16, 0, 0,
            0, 0,
            VKDataFormat.UINT),
    R16G16_SINT(
            VK10.VK_FORMAT_R16G16_SINT,
            16, 16, 0, 0,
            0, 0,
            VKDataFormat.SINT),
    R16G16_SFLOAT(
            VK10.VK_FORMAT_R16G16_SFLOAT,
            16, 16, 0, 0,
            0, 0,
            VKDataFormat.SFLOAT),
    R16G16B16_UNORM(
            VK10.VK_FORMAT_R16G16B16_UNORM,
            16, 16, 16, 0,
            0, 0,
            VKDataFormat.UNORM),
    R16G16B16_SNORM(
            VK10.VK_FORMAT_R16G16B16_SNORM,
            16, 16, 16, 0,
            0, 0,
            VKDataFormat.SNORM),
    R16G16B16_UINT(
            VK10.VK_FORMAT_R16G16B16_UINT,
            16, 16, 16, 0,
            0, 0,
            VKDataFormat.UINT),
    R16G16B16_SINT(
            VK10.VK_FORMAT_R16G16B16_SINT,
            16, 16, 16, 0,
            0, 0,
            VKDataFormat.SINT),
    R16G16B16_SFLOAT(
            VK10.VK_FORMAT_R16G16B16_SFLOAT,
            16, 16, 16, 0,
            0, 0,
            VKDataFormat.SFLOAT),
    R16G16B16A16_UNORM(
            VK10.VK_FORMAT_R16G16B16A16_UNORM,
            16, 16, 16, 16,
            0, 0,
            VKDataFormat.UNORM),
    R16G16B16A16_SNORM(
            VK10.VK_FORMAT_R16G16B16A16_SNORM,
            16, 16, 16, 16,
            0, 0,
            VKDataFormat.SNORM),
    R16G16B16A16_UINT(
            VK10.VK_FORMAT_R16G16B16A16_UINT,
            16, 16, 16, 16,
            0, 0,
            VKDataFormat.UINT),
    R16G16B16A16_SINT(
            VK10.VK_FORMAT_R16G16B16A16_SINT,
            16, 16, 16, 16,
            0, 0,
            VKDataFormat.SINT),
    R16G16B16A16_SFLOAT(
            VK10.VK_FORMAT_R16G16B16A16_SFLOAT,
            16, 16, 16, 16,
            0, 0,
            VKDataFormat.SFLOAT),
    R32_UINT(
            VK10.VK_FORMAT_R32_UINT,
            32, 0, 0, 0,
            0, 0,
            VKDataFormat.UINT),
    R32_SINT(
            VK10.VK_FORMAT_R32_SINT,
            32, 0, 0, 0,
            0, 0,
            VKDataFormat.SINT),
    R32_SFLOAT(
            VK10.VK_FORMAT_R32_SFLOAT,
            32, 0, 0, 0,
            0, 0,
            VKDataFormat.SFLOAT),
    R32G32_UINT(
            VK10.VK_FORMAT_R32G32_UINT,
            32, 32, 0, 0,
            0, 0,
            VKDataFormat.UINT),
    R32G32_SINT(
            VK10.VK_FORMAT_R32G32_SINT,
            32, 32, 0, 0,
            0, 0,
            VKDataFormat.SINT),
    R32G32_SFLOAT(
            VK10.VK_FORMAT_R32G32_SFLOAT,
            32, 32, 0, 0,
            0, 0,
            VKDataFormat.SFLOAT),
    R32G32B32_UINT(
            VK10.VK_FORMAT_R32G32B32_UINT,
            32, 32, 32, 0,
            0, 0,
            VKDataFormat.UINT),
    R32G32B32_SINT(
            VK10.VK_FORMAT_R32G32B32_SINT,
            32, 32, 32, 0,
            0, 0,
            VKDataFormat.SINT),
    R32G32B32_SFLOAT(
            VK10.VK_FORMAT_R32G32B32_SFLOAT,
            32, 32, 32, 0,
            0, 0,
            VKDataFormat.SFLOAT),
    R32G32B32A32_UINT(
            VK10.VK_FORMAT_R32G32B32A32_UINT,
            32, 32, 32, 32,
            0, 0,
            VKDataFormat.UINT),
    R32G32B32A32_SINT(
            VK10.VK_FORMAT_R32G32B32A32_SINT,
            32, 32, 32, 32,
            0, 0,
            VKDataFormat.SINT),
    R32G32B32A32_SFLOAT(
            VK10.VK_FORMAT_R32G32B32A32_SFLOAT,
            32, 32, 32, 32,
            0, 0,
            VKDataFormat.SFLOAT),
    D16_UNORM(
            VK10.VK_FORMAT_D16_UNORM,
            0, 0, 0, 0,
            16, 0,
            VKDataFormat.UNORM),
    S8_UINT(
            VK10.VK_FORMAT_S8_UINT,
            0, 0, 0, 0,
            0, 8,
            VKDataFormat.UINT),
    D16_UNORM_S8_UINT(
            VK10.VK_FORMAT_D16_UNORM_S8_UINT,
            0, 0, 0, 0,
            16, 8,
            VKDataFormat.UNORM_UINT),
    D24_UNORM_S8_UINT(
            VK10.VK_FORMAT_D24_UNORM_S8_UINT,
            0, 0, 0, 0,
            0, 8,
            VKDataFormat.UINT),
    D32_SFLOAT_S8_UINT(
            VK10.VK_FORMAT_D32_SFLOAT_S8_UINT,
            0, 0, 0, 0,
            0, 8,
            VKDataFormat.SFLOAT_UINT);

    public static final VKFormat FLOAT = VKFormat.R32_SFLOAT;
    public static final VKFormat INT = VKFormat.R32_SINT;
    public static final VKFormat UINT = VKFormat.R32_UINT;
    public static final VKFormat VEC2 = VKFormat.R32G32_SFLOAT;
    public static final VKFormat VEC3 = VKFormat.R32G32B32_SFLOAT;
    public static final VKFormat VEC4 = VKFormat.R32G32B32A32_SFLOAT;
    public static final VKFormat IVEC2 = VKFormat.R32G32_SINT;
    public static final VKFormat IVEC3 = VKFormat.R32G32B32_SINT;
    public static final VKFormat IVEC4 = VKFormat.R32G32B32A32_SINT;
    public static final VKFormat UIVEC2 = VKFormat.R32G32_SINT;
    public static final VKFormat UIVEC3 = VKFormat.R32G32B32_SINT;
    public static final VKFormat UIVec4 = VKFormat.R32G32B32A32_SINT;
    
    public final int value;
    public final int redBits;
    public final int greenBits;
    public final int blueBits;
    public final int alphaBits;
    public final int depthBits;
    public final int stencilBits;
    final VKDataFormat dataFormat;
    
    @Override
    public int getValue() {
        return this.value;
    }

    public boolean isColor() {
        return hasRed()
                || hasGreen()
                || hasBlue()
                || hasAlpha();
    }
    
    public boolean hasAlpha() {
        return this.alphaBits > 0;
    }
    
    public int getBits() {
        return this.redBits
                + this.greenBits
                + this.blueBits
                + this.alphaBits
                + this.depthBits
                + this.stencilBits;
    }

    public boolean hasRed() {
        return this.redBits > 0;
    }

    public boolean hasGreen() {
        return this.greenBits > 0;
    }

    public boolean hasBlue() {
        return this.blueBits > 0;
    }

    public boolean isSigned() {
        switch (dataFormat) {
            case SNORM:
            case SINT:
            case SFLOAT:
            case SFLOAT_UINT:
                return true;
            default:
                return false;
        }
    }

    public boolean isUnsigned() {
        switch (dataFormat) {
            case UINT:
            case UNORM:
            case SFLOAT_UINT:
            case UNORM_UINT:
                return true;
            default:
                return false;
        }
    }

    public boolean isFloatingPoint() {
        switch (dataFormat) {
            case SFLOAT:
            case SFLOAT_UINT:
                return true;
            default:
                return false;
        }
    }

    public boolean isNormalized() {
        switch (dataFormat) {
            case SNORM:
            case UNORM:
            case UNORM_UINT:
                return true;
            default:
                return false;
        }
    }

    public boolean hasDepth() {
        return this.depthBits > 0;
    }

    public boolean hasStencil() {
        return this.stencilBits > 0;
    }

    public boolean isDepthStencil() {
        return this.depthBits > 0 || this.stencilBits > 0;
    }

    private VKFormat(
            final int value,
            final int redBits, final int greenBits, final int blueBits, final int alphaBits,
            final int depthBits, final int stencilBits,
            final VKDataFormat dataFormat) {

        this.value = value;
        this.redBits = redBits;
        this.greenBits = greenBits;
        this.blueBits = blueBits;
        this.alphaBits = alphaBits;
        this.depthBits = depthBits;
        this.stencilBits = stencilBits;
        this.dataFormat = dataFormat;
    }

    public static Optional<VKFormat> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
    
    public VKFormatProperties getProperties(final VKContext ctx) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkFormatProperties props = VkFormatProperties.callocStack(mem);
            
            VK10.vkGetPhysicalDeviceFormatProperties(ctx.device.physicalDevice.handle, this.value, props);
            
            return new VKFormatProperties(props);
        }
    }
}
