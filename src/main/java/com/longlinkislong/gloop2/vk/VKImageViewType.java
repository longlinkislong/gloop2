/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKImageViewType implements CEnum {
    VIEW_1D(VK10.VK_IMAGE_VIEW_TYPE_1D),
    VIEW_2D(VK10.VK_IMAGE_VIEW_TYPE_2D),
    VIEW_3D(VK10.VK_IMAGE_VIEW_TYPE_3D),
    VIEW_CUBE(VK10.VK_IMAGE_VIEW_TYPE_CUBE),
    VIEW_1D_ARRAY(VK10.VK_IMAGE_VIEW_TYPE_1D_ARRAY),
    VIEW_2D_ARRAY(VK10.VK_IMAGE_VIEW_TYPE_2D_ARRAY),
    VIEW_CUBE_ARRAY(VK10.VK_IMAGE_VIEW_TYPE_CUBE_ARRAY);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKImageViewType(final int value) {
        this.value = value;
    }
    
    public static Optional<VKImageViewType> of(final int vkenum) {
        return Tools.valueOf(vkenum, Arrays.stream(values()));
    }        
}
