/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VkSubresourceLayout;

/**
 *
 * @author zmichaels
 */
public class VKSubresourceLayout {
    public final long offset;
    public final long size;
    public final long rowPitch;
    public final long arrayPitch;
    public final long depthPitch;
    
    VKSubresourceLayout(final VkSubresourceLayout vkstruct) {
        this.offset = vkstruct.offset();
        this.size = vkstruct.size();
        this.rowPitch = vkstruct.rowPitch();
        this.arrayPitch = vkstruct.arrayPitch();
        this.depthPitch = vkstruct.depthPitch();
    }
}
