/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkFenceCreateInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper for the VkFence object.
 *
 * @author zmichaels
 * @see
 * <a href="https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFence.html">Vulkan
 * Spec</a>
 */
public class VKFence implements VKObject {

    private static final Logger LOGGER = LoggerFactory.getLogger(VKFence.class);

    /**
     * Bitmask for specifying initial state and behavior of a fence.
     *
     * @see
     * <a href="https://www.khronos.org/registry/vulkan/specs/1.0/man/html/VkFenceCreateFlagBits.html">Vulkan
     * Spec</a>
     *
     */
    public static enum CreateFlag implements CBitfield {
        CREATE_SIGNALED_BIT(VK10.VK_FENCE_CREATE_SIGNALED_BIT);

        /**
         * Vulkan enum value.
         */
        public final int value;
        
        @Override
        public int getValue() {
            return this.value;
        }

        private CreateFlag(final int value) {
            this.value = value;
        }

        public static Set<CreateFlag> flags(final int bits) {
            return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
        }

        public static int bits(final Set<CreateFlag> flags) {
            return Tools.flagsToBitfield(flags);
        }
    }

    /**
     * Fence object status codes.
     *
     * @see
     * <a href="https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkGetFenceStatus.html">Vulkan
     * Spec</a>
     */
    public static enum Status implements CEnum {
        SUCCESS(VK10.VK_SUCCESS),
        NOT_READY(VK10.VK_NOT_READY);

        public final int value;
        
        @Override
        public int getValue() {
            return this.value;
        }

        private Status(final int value) {
            this.value = value;
        }

        public static Optional<Status> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }
    }

    VkDevice deviceHandle;
    long handle;

    public VKFence() {}
    
    /**
     * Constructs a new VKFence.
     *
     * @param ctx the Vulkan context to use.
     * @param flags any create flags.
     * @throws AssertionError if an unexpected status code is returned.
     * @throws VKOutOfHostMemoryException if a host memory allocation failed.
     * @throws VKOutOfDeviceMemoryException if a device memory allocation failed.
     */
    public VKFence(final VKContext ctx, final Set<CreateFlag> flags) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkFenceCreateInfo createInfo = VkFenceCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_FENCE_CREATE_INFO)
                    .flags(CreateFlag.bits(flags));

            final LongBuffer pFence = mem.callocLong(1);
            final int status = VK10.vkCreateFence(ctx.device.handle, createInfo, null, pFence);

            switch (status) {
                case VK10.VK_SUCCESS:
                    break;
                case VK10.VK_ERROR_OUT_OF_HOST_MEMORY:
                    throw new VKOutOfHostMemoryException();
                case VK10.VK_ERROR_OUT_OF_DEVICE_MEMORY:
                    throw new VKOutOfDeviceMemoryException();
                default:
                    throw new AssertionError(Tools.vkerr(status));
            }

            this.handle = pFence.get(0);
            this.deviceHandle = ctx.device.handle;
        }
    }

    /**
     * Switches the resource handles of this object with another's. Delete the
     * resources if the other object is null.
     *
     * @param other the other object.
     */
    public void swap(final VKFence other) {
        if (other == null) {
            this.close();
            return;
        }

        final long tmpHandle = other.handle;
        final VkDevice tmpDev = other.deviceHandle;

        other.handle = this.handle;
        other.deviceHandle = this.deviceHandle;

        this.handle = tmpHandle;
        this.deviceHandle = tmpDev;
    }

    @Override
    public boolean isValid() {
        return this.handle > 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyFence(deviceHandle, handle, null);
            deviceHandle = null;
            handle = 0L;
        }
    }

    /**
     * Waits for the fence to be signaled. This polls the fence status with an
     * exponential sleep period.
     *
     * @throws AssertionError if an unexpected status is reported.
     * @throws IllegalStateException if the fence is not valid.
     * @throws InterruptedException if the thread is interrupted while sleeping.
     * @throws VKOutOfDeviceMemoryException if a device memory allocation fails.
     * @throws VKOutOfHostMemoryException if a host memory allocation fails.
     * @throws VKDeviceLostException if the device is lost.
     * @see
     * <a href="https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkWaitForFences.html">Vulkan
     * Spec</a>
     */
    public void waitFor() throws InterruptedException {
        if (!this.isValid()) {
            throw new IllegalStateException("VKFence is not valid!");
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final LongBuffer pHandle = mem.longs(handle);
            // wait 1ms max
            int status = VK10.vkWaitForFences(deviceHandle, pHandle, true, 0L);            

            while (status == VK10.VK_TIMEOUT) {
                Thread.yield();

                status = VK10.vkWaitForFences(deviceHandle, pHandle, true, 0L);                
            }

            switch (status) {
                case VK10.VK_SUCCESS:
                    break;
                case VK10.VK_ERROR_OUT_OF_DEVICE_MEMORY:
                    throw new VKOutOfDeviceMemoryException();
                case VK10.VK_ERROR_OUT_OF_HOST_MEMORY:
                    throw new VKOutOfHostMemoryException();
                case VK10.VK_ERROR_DEVICE_LOST:
                    throw new VKDeviceLostException();                
                default:
                    throw new AssertionError(Tools.vkerr(status));
            }
        }
    }

    /**
     * Waits up to timeout nanoseconds for the fence to be signaled.
     *
     * @param timeout the timeout period. A value of 0 will immediately retrieve
     * the status without waiting.
     * @throws IllegalStateException if VKFence is not valid.
     * @throws VKTimeoutException if the timeout period expired before the fence
     * was signaled.
     * @throws VKOutOfDeviceMemoryException if a device memory allocation fails.
     * @throws VKOutOfHostMemoryException if a host memory allocation fails.
     * @throws VKDeviceLostException if the device is lost.
     * @see
     * <a href="https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkWaitForFences.html">Vulkan
     * Spec</a>
     */
    public void waitFor(long timeout) throws VKTimeoutException {
        if (!this.isValid()) {
            throw new IllegalStateException("VKFence is not valid!");
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final int status = VK10.vkWaitForFences(deviceHandle, mem.longs(handle), true, timeout);

            switch (status) {
                case VK10.VK_SUCCESS:                    
                    break;
                case VK10.VK_TIMEOUT:
                    throw new VKTimeoutException("VKFence timed out before being signaled.");
                case VK10.VK_ERROR_OUT_OF_DEVICE_MEMORY:
                    throw new VKOutOfDeviceMemoryException();
                case VK10.VK_ERROR_OUT_OF_HOST_MEMORY:
                    throw new VKOutOfHostMemoryException();
                case VK10.VK_ERROR_DEVICE_LOST:
                    throw new VKDeviceLostException();                
                default:
                    throw new AssertionError(Tools.vkerr(status));
            }
        }
    }

    /**
     * Resets the fence.
     *
     * @throws AssertionError if an unexpected status is returned.
     * @throws IllegalStateException if VKFence is not valid.
     * @throws VKOutOfDeviceMemoryException if a device memory allocation fails.
     * @throws VKOutOfHostMemoryException if a host memory allocation fails.
     * @see
     * <a href="https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkResetFences.html">Vulkan
     * Spec</a>
     */
    public void reset() {
        if (!this.isValid()) {
            throw new IllegalStateException("VKFence is not valid!");
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final int status = VK10.vkResetFences(deviceHandle, mem.longs(handle));

            switch (status) {
                case VK10.VK_SUCCESS:
                    break;
                case VK10.VK_ERROR_OUT_OF_DEVICE_MEMORY:
                    throw new VKOutOfDeviceMemoryException();
                case VK10.VK_ERROR_OUT_OF_HOST_MEMORY:
                    throw new VKOutOfHostMemoryException();
                default:
                    throw new AssertionError(Tools.vkerr(status));

            }
        }
    }

    /**
     * Returns the status of a fence.
     *
     * @return the status. Either SUCCESS or NOT_READY.
     * @throws IllegalStateException if the fence is not valid or
     * @throws AssertionError if the status is not SUCCESS or NOT_READY.
     * @see
     * <a href="https://www.khronos.org/registry/vulkan/specs/1.0/man/html/vkGetFenceStatus.html">Vulkan
     * Spec</a>
     */
    public Status getStatus() {
        if (!this.isValid()) {
            throw new IllegalStateException("VKFence is not valid!");
        }

        final int status = VK10.vkGetFenceStatus(this.deviceHandle, this.handle);

        switch (status) {
            case VK10.VK_SUCCESS:
                return Status.SUCCESS;
            case VK10.VK_NOT_READY:
                return Status.NOT_READY;
            case VK10.VK_ERROR_DEVICE_LOST:
                throw new VKDeviceLostException();
            default:
                throw new AssertionError(Tools.vkerr(status));
        }
    }
}
