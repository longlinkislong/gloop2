/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKClearRect {

    public final int x;
    public final int y;
    public final int width;
    public final int height;

    public final int baseArrayLayer;
    public final int layerCount;

    public VKClearRect(
            final int x, final int y,
            final int width, final int height,
            final int baseArrayLayer, final int layerCount) {

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.baseArrayLayer = baseArrayLayer;
        this.layerCount = layerCount;
    }

    public VKClearRect() {
        this(0, 0, 0, 0, 0, 0);
    }
    
    public VKClearRect withOffset(final int x, final int y) {
        return new VKClearRect(x, y, width, height, baseArrayLayer, layerCount);
    }
    
    public VKClearRect withExtent(final int width, final int height) {
        return new VKClearRect(x, y, width, height, baseArrayLayer, layerCount);
    }
    
    public VKClearRect withBaseArrayLayer(final int baseArrayLayer) {
        return new VKClearRect(x, y, width, height, baseArrayLayer, layerCount);
    }
    
    public VKClearRect withLayerCount(final int layerCount) {
        return new VKClearRect(x, y, width, height, baseArrayLayer, layerCount);
    }
}
