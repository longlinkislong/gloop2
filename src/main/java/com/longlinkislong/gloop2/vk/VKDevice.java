/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.FloatBuffer;
import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.KHRSwapchain;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkCopyDescriptorSet;
import org.lwjgl.vulkan.VkDescriptorBufferInfo;
import org.lwjgl.vulkan.VkDescriptorImageInfo;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkDeviceCreateInfo;
import org.lwjgl.vulkan.VkDeviceQueueCreateInfo;
import org.lwjgl.vulkan.VkWriteDescriptorSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class VKDevice implements VKObject {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(VKDevice.class);
    public final VKPhysicalDevice physicalDevice;
    
    final VkDevice handle;
    final List<VKQueue> enabledQueues;
    
    public List<VKQueue> getQueues() {
        return Collections.unmodifiableList(this.enabledQueues);
    }
    
    public Stream<VKQueue> queues() {
        return this.enabledQueues.stream();
    }
    
    public void updateDescriptorSets(final VKWriteDescriptorSet descriptorWrite, final VKCopyDescriptorSet descriptorCopy) {        
        updateDescriptorSets(
                descriptorWrite == null
                        ? Collections.emptyList()
                        : Collections.singletonList(descriptorWrite),
                descriptorCopy == null
                        ? Collections.emptyList()
                        : Collections.singletonList(descriptorCopy));
    }
    
    public void updateDescriptorSets(final VKWriteDescriptorSet[] descriptorWrites, final VKCopyDescriptorSet[] descriptorCopies) {
        updateDescriptorSets(
                descriptorWrites == null
                        ? Collections.emptyList()
                        : Arrays.asList(descriptorWrites),
                descriptorCopies == null
                        ? Collections.emptyList()
                        : Arrays.asList(descriptorCopies));
    }
    
    public void updateDescriptorSets(final List<VKWriteDescriptorSet> descriptorWrites, final List<VKCopyDescriptorSet> descriptorCopies) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkWriteDescriptorSet.Buffer pDescriptorWrites;
            
            if (descriptorWrites == null || descriptorWrites.isEmpty()) {
                pDescriptorWrites = null;
            } else {
                pDescriptorWrites = VkWriteDescriptorSet.callocStack(descriptorWrites.size(), mem);
                
                for (int i = 0; i < descriptorWrites.size(); i++) {
                    final VkDescriptorImageInfo.Buffer pImageInfo;
                    
                    if (descriptorWrites.get(i).imageInfo.isEmpty()) {
                        pImageInfo = null;
                    } else {
                        pImageInfo = VkDescriptorImageInfo.callocStack(descriptorWrites.get(i).imageInfo.size(), mem);
                        
                        for (int j = 0; j < descriptorWrites.get(i).imageInfo.size(); j++) {
                            final VKDescriptorImageInfo imageInfo = descriptorWrites.get(i).imageInfo.get(j);
                            
                            pImageInfo.get(j)
                                    .sampler(imageInfo.sampler.handle)
                                    .imageView(imageInfo.imageView.handle)
                                    .imageLayout(imageInfo.imageLayout.value);
                        }
                    }
                    
                    final VkDescriptorBufferInfo.Buffer pBufferInfo;
                    
                    if (descriptorWrites.get(i).bufferInfo.isEmpty()) {
                        pBufferInfo = null;
                    } else {
                        pBufferInfo = VkDescriptorBufferInfo.callocStack(descriptorWrites.get(i).bufferInfo.size(), mem);
                        
                        for (int j = 0; j < descriptorWrites.get(i).bufferInfo.size(); j++) {
                            final VKDescriptorBufferInfo bufferInfo = descriptorWrites.get(i).bufferInfo.get(j);
                            
                            pBufferInfo.get(j)
                                    .buffer(bufferInfo.buffer.handle)
                                    .offset(bufferInfo.offset)
                                    .range(bufferInfo.range);
                        }
                    }
                    
                    final LongBuffer pTexelBufferInfo;
                    
                    if (descriptorWrites.get(i).texelBufferView.isEmpty()) {
                        pTexelBufferInfo = null;
                    } else {
                        pTexelBufferInfo = mem.callocLong(descriptorWrites.get(i).texelBufferView.size());
                        
                        descriptorWrites.get(i).texelBufferView.stream()
                                .mapToLong(tbi -> tbi.handle)
                                .forEach(pTexelBufferInfo::put);
                        
                        pTexelBufferInfo.flip();
                    }
                    
                    pDescriptorWrites.get(i)
                            .sType(VK10.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
                            .dstSet(descriptorWrites.get(i).dstSet.handle)
                            .dstBinding(descriptorWrites.get(i).dstBinding)
                            .dstArrayElement(descriptorWrites.get(i).dstArrayElement)
                            .descriptorType(descriptorWrites.get(i).descriptorType.value)
                            .pImageInfo(pImageInfo)
                            .pBufferInfo(pBufferInfo)
                            .pTexelBufferView(pTexelBufferInfo);
                }
            }
            
            final VkCopyDescriptorSet.Buffer pDescriptorCopies;
            
            if (descriptorCopies == null || descriptorCopies.isEmpty()) {
                pDescriptorCopies = null;
            } else {
                pDescriptorCopies = VkCopyDescriptorSet.callocStack(descriptorCopies.size(), mem);
                
                for (int i = 0; i < descriptorCopies.size(); i++) {
                    pDescriptorCopies.get(i)
                            .sType(VK10.VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET)
                            .srcSet(descriptorCopies.get(i).srcSet.handle)
                            .srcBinding(descriptorCopies.get(i).srcBinding)
                            .srcArrayElement(descriptorCopies.get(i).srcArrayElement)
                            .dstSet(descriptorCopies.get(i).dstSet.handle)
                            .dstBinding(descriptorCopies.get(i).dstBinding)
                            .dstArrayElement(descriptorCopies.get(i).dstArrayElement)
                            .descriptorCount(descriptorCopies.get(i).descriptorCount);
                }
            }
            
            VK10.vkUpdateDescriptorSets(handle, pDescriptorWrites, pDescriptorCopies);
        }
    }
    
    public VKDevice(final VKDeviceCreateInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final int queueCreateInfoCount = info.queueCreateInfos.size();
            final VkDeviceQueueCreateInfo.Buffer queueCreateInfos = VkDeviceQueueCreateInfo.callocStack(queueCreateInfoCount, mem);
            VKPhysicalDevice selectedPhysicalDevice = null;
            
            this.enabledQueues = new ArrayList<>();
            
            for (int i = 0; i < queueCreateInfoCount; i++) {
                final VKDeviceCreateInfo.DeviceQueueCreateInfo queueInfo = info.queueCreateInfos.get(i);
                
                if (selectedPhysicalDevice == null) {
                    selectedPhysicalDevice = queueInfo.queueFamily.getPhysicalDevice();
                } else if (selectedPhysicalDevice != queueInfo.queueFamily.getPhysicalDevice()) {
                    throw new IllegalArgumentException("All queue families must be from the same physical device!");
                }
                
                final FloatBuffer pQueuePriorities = mem.callocFloat(queueInfo.queuePriorities.size());
                
                queueInfo.queuePriorities.forEach(pQueuePriorities::put);
                pQueuePriorities.flip();
                
                queueCreateInfos.get(i)
                        .sType(VK10.VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO)
                        .queueFamilyIndex(queueInfo.queueFamily.index)
                        .pQueuePriorities(pQueuePriorities);
            }
            
            if (selectedPhysicalDevice == null) {
                throw new IllegalStateException("At least one queue family must be selected!");
            }
            
            final Set<String> extensions = new HashSet<>(info.extensions);
            
            extensions.add(KHRSwapchain.VK_KHR_SWAPCHAIN_EXTENSION_NAME);
            
            final PointerBuffer ppEnabledExtensions = mem.callocPointer(extensions.size());
            
            extensions.stream().map(mem::UTF8).forEach(ppEnabledExtensions::put);
            ppEnabledExtensions.flip();
            
            final PointerBuffer ppEnabledLayers = mem.callocPointer(info.layers.size());
            
            info.layers.stream().map(mem::UTF8).forEach(ppEnabledLayers::put);
            ppEnabledLayers.flip();
            
            final VkDeviceCreateInfo deviceCreateInfo = VkDeviceCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO)
                    .pQueueCreateInfos(queueCreateInfos)
                    .ppEnabledExtensionNames(ppEnabledExtensions)
                    .ppEnabledLayerNames(ppEnabledLayers);
            
            final PointerBuffer pDevice = mem.callocPointer(1);
            int err = VK10.vkCreateDevice(selectedPhysicalDevice.handle, deviceCreateInfo, null, pDevice);
            
            assert err == VK10.VK_SUCCESS : Tools.vkerr(err);
            
            this.handle = new VkDevice(pDevice.get(0), selectedPhysicalDevice.handle, deviceCreateInfo);
            this.physicalDevice = selectedPhysicalDevice;
            
            LOGGER.trace("Successfully created logical Vulkan Device!");
            
            for (int i = 0; i < info.queueCreateInfos.size(); i++) {
                for (int j = 0; j < info.queueCreateInfos.get(i).queuePriorities.size(); j++) {
                    this.enabledQueues.add(new VKQueue(this, info.queueCreateInfos.get(i).queueFamily, j));
                }
            }
        }
    }
    
    @Override
    public boolean isValid() {
        return !this.enabledQueues.isEmpty();
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyDevice(this.handle, null);
            this.enabledQueues.clear();
            LOGGER.trace("Successfully closed logical Vulkan Device!");
        }
    }
}
