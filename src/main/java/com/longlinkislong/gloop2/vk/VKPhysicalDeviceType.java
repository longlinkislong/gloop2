/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKPhysicalDeviceType implements CEnum {
    OTHER(VK10.VK_PHYSICAL_DEVICE_TYPE_OTHER),
    INTEGRATED_GPU(VK10.VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU),
    DISCRETE_GPU(VK10.VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU),
    VIRTUAL_GPU(VK10.VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU),
    CPU(VK10.VK_PHYSICAL_DEVICE_TYPE_CPU);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKPhysicalDeviceType(final int value) {
        this.value = value;
    }
    
    public static Optional<VKPhysicalDeviceType> of (final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
