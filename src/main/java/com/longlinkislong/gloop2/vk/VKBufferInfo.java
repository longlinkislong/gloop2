/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public class VKBufferInfo {

    /**
     * Bits that specify additional parameters for a buffer.
     */
    public static enum CreateFlag implements CBitfield {
        /**
         * The buffer will be backed using sparse memory bindings.
         */
        SPARSE_BINDING(VK10.VK_BUFFER_CREATE_SPARSE_BINDING_BIT),
        /**
         * The buffer can be partially backed using sparse memory bindings.
         * Buffers with this flag must also be created with the SPARSE_BINDING
         * flag.
         */
        SPARSE_RESIDENCY(VK10.VK_BUFFER_CREATE_SPARSE_RESIDENCY_BIT),
        /**
         * The buffer will be backed using sparse memory bindings with memory
         * ranges that might also simultaneously be backing another buffer.
         * Buffers with this flag must also be created with the SPARSE_BINDING
         * flag.
         */
        SPARSE_ALIASED(VK10.VK_BUFFER_CREATE_SPARSE_ALIASED_BIT);

        private final int value;

        @Override
        public int getValue() {
            return this.value;
        }

        private CreateFlag(final int value) {
            this.value = value;
        }

        public static Set<CreateFlag> flags(final int bitflags) {
            return Tools.bitfieldToFlags(bitflags, Arrays.stream(values()));
        }

        public static int bits(final Set<CreateFlag> flags) {
            return Tools.flagsToBitfield(flags);
        }
    }

    /**
     * Bits that specify usage behavior of a buffer.
     */
    public static enum Usage implements CBitfield {
        /**
         * The buffer can be used as the source of a transfer command.
         */
        TRANSFER_SRC(VK10.VK_BUFFER_USAGE_TRANSFER_SRC_BIT),
        /**
         * The buffer can be used as the destination of a transfer command.
         */
        TRANSFER_DST(VK10.VK_BUFFER_USAGE_TRANSFER_DST_BIT),
        /**
         * The buffer can be used to create a VKBufferView for use as a
         * UNIFORM_TEXEL_BUFFER in a VKDescriptorSet.
         */
        UNIFORM_TEXEL(VK10.VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT),
        /**
         * The buffer can be used to create a VKBufferView for use as a
         * STORAGE_TEXEL_BUFFER in a VKDescriptorSet.
         */
        STORAGE_TEXEL(VK10.VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT),
        /**
         * The buffer can be used as either a UNIFORM_BUFFER or
         * UNIFORM_BUFFER_DYNAMIC.
         */
        UNIFORM(VK10.VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT),
        /**
         * The buffer can be used as either STORAGE_BUFFER or
         * STORAGE_BUFFER_DYNAMIC.
         */
        STORAGE(VK10.VK_BUFFER_USAGE_STORAGE_BUFFER_BIT),
        /**
         * The buffer can store index data.
         */
        INDEX(VK10.VK_BUFFER_USAGE_INDEX_BUFFER_BIT),
        /**
         * The buffer can stores vertex data.
         */
        VERTEX(VK10.VK_BUFFER_USAGE_VERTEX_BUFFER_BIT),
        /**
         * The buffer can be used for indirect draw commands.
         */
        INDIRECT(VK10.VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT);

        private final int value;

        @Override
        public int getValue() {
            return this.value;
        }

        private Usage(final int value) {
            this.value = value;
        }

        public static Set<Usage> flags(final int bitflag) {
            return Tools.bitfieldToFlags(bitflag, Arrays.stream(values()));
        }

        public static int bits(final Set<Usage> flags) {
            return Tools.flagsToBitfield(flags);
        }
    }

    public final Set<CreateFlag> flags;
    public final long size;
    public final Set<Usage> usage;
    public final VKSharingMode sharingMode;
    public final List<VKQueueFamily> queueFamilies;

    public VKBufferInfo(
            final Collection<CreateFlag> flags,
            final long size,
            final Collection<Usage> usage,
            final VKSharingMode sharingMode,
            final List<VKQueueFamily> queueFamilies) {

        this.flags = Tools.copySet(flags);
        this.size = size;
        this.usage = Tools.copySet(usage);
        this.sharingMode = sharingMode;
        this.queueFamilies = Tools.copyList(queueFamilies);
    }

    /**
     * Creates a new VKBufferInfo with the default values. All flags are
     * initialized as empty sets, all lists are initialized as empty lists, all
     * numeric values are initialized as 0, and sharingMode is initialized as
     * EXCLUSIVE.
     */
    public VKBufferInfo() {
        this(null, 0, null, VKSharingMode.EXCLUSIVE, null);
    }

    /**
     * Derives a new VKBufferInfo with changes to the flag field.
     *
     * @param flags the flag field. Null is treated as an empty set.
     * @return the derivative VKBufferInfo.
     */
    public VKBufferInfo withFlags(final Collection<CreateFlag> flags) {
        return new VKBufferInfo(flags, size, usage, sharingMode, queueFamilies);
    }

    /**
     * Derives a new VKBufferInfo with changes to the flag field. This method
     * calls [code]withFlags(Arrays.asList(flags))[/code]
     *
     * @param flags the flag field. An array of size 0 is treated as an empty
     * set.
     * @return the derivative VKBufferInfo.
     */
    public VKBufferInfo withFlags(final CreateFlag... flags) {
        return withFlags(Arrays.asList(flags));
    }

    /**
     * Derives a new VKBufferInfo with changes to the size field.
     *
     * @param size the size field.
     * @return the derivative VKBufferInfo.
     */
    public VKBufferInfo withSize(final long size) {
        assert size >= 0 : "Size cannot be negative!";

        return new VKBufferInfo(flags, size, usage, sharingMode, queueFamilies);
    }

    /**
     * Derives a new VKBufferInfo with changes to the usage field.
     *
     * @param usage the usage field. Null is treated as an empty set.
     * @return the derivative VKBufferInfo.
     */
    public VKBufferInfo withUsage(final Collection<Usage> usage) {
        return new VKBufferInfo(flags, size, usage, sharingMode, queueFamilies);
    }

    /**
     * Derives a new VKBufferInfo with changes to the usage field. This methods
     * calls [code]withUsage(Arrays.asList(usage))[/code]
     *
     * @param usage the usage field. An array of size 0 is treated as an empty
     * set.
     * @return the derivative VKBufferInfo.
     */
    public VKBufferInfo withUsage(final Usage... usage) {
        return withUsage(Arrays.asList(usage));
    }

    /**
     * Derives a new VKBufferInfo with changes to the sharing mode field.
     *
     * @param sharingMode the new sharing mode.
     * @return the derivative VKBufferInfo.
     */
    public VKBufferInfo withSharingMode(final VKSharingMode sharingMode) {
        assert sharingMode != null : "Sharing mode cannot be null!";

        return new VKBufferInfo(flags, size, usage, sharingMode, queueFamilies);
    }

    /**
     * Derives a new VKBufferInfo with changes to the queue families field.
     *
     * @param queueFamilies the queue families. Null is treated as an empty
     * list.
     * @return the derivative VKBufferInfo.
     */
    public VKBufferInfo withQueueFamilies(final List<VKQueueFamily> queueFamilies) {
        return new VKBufferInfo(flags, size, usage, sharingMode, queueFamilies);
    }

    /**
     * Derives a new VKBufferinfo with changes to the queue families field. This
     * method calls
     * [code]withQueueFamilies(Arrays.asList(queueFamilies))[/code].
     *
     * @param queueFamilies the queue families. An array of size 0 is treated as
     * an empty list.
     * @return the derivative VKBufferInfo.
     */
    public VKBufferInfo withQueueFamilies(final VKQueueFamily... queueFamilies) {
        return withQueueFamilies(Arrays.asList(queueFamilies));
    }

    /**
     * Creates a new VKBuffer that uses the memory manager.
     *
     * @param ctx the Vulkan context.
     * @param memoryProperties the memory properties to require.
     * @return the managed buffer.
     */
    public VKManagedBuffer createManaged(final VKContext ctx, final Set<VKMemoryProperty> memoryProperties) {
        return new VKManagedBuffer(ctx, this, memoryProperties);
    }

    /**
     * Creates a new VKBuffer. No memory is allocated.
     * @param ctx the Vulkan context.
     * @return the managed buffer.
     */
    public VKBuffer create(final VKContext ctx) {
        return new VKBuffer(ctx, this);
    }
}
