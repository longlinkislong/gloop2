/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.memory.Allocation;
import com.longlinkislong.gloop2.memory.VKMemoryManager;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkMemoryRequirements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class VKManagedBuffer extends VKBuffer {
    private static final Logger LOGGER = LoggerFactory.getLogger(VKManagedBuffer.class);
    
    Allocation memory;
    Set<VKMemoryProperty> memoryProperties;

    public VKManagedBuffer() {}
    
    public VKManagedBuffer(VKContext ctx, VKBufferInfo info, final Set<VKMemoryProperty> memoryProperties) {
        super(ctx, info);

        try (MemoryStack mem = MemoryStack.stackPush()) {
            this.memoryProperties = Collections.unmodifiableSet(new HashSet<>(memoryProperties));

            final VkMemoryRequirements memReqs = VkMemoryRequirements.callocStack(mem);

            VK10.vkGetBufferMemoryRequirements(ctx.device.handle, handle, memReqs);            
            
            this.memory = ctx.memoryManager.allocateBuffer(memReqs, VKMemoryProperty.bits(memoryProperties));  
            
            final int err = VK10.vkBindBufferMemory(ctx.device.handle, handle, this.memory.ptr(), this.memory.offset());
            
            assert Tools.vksuccess(err) : Tools.vkerr(err);
        }
    }

    public static final class MappedBuffer implements VKObject {

        private VKManagedBuffer host;
        private ByteBuffer ptr;
        
        private MappedBuffer(final VKManagedBuffer host, final long offset, final long size) {
            this.host = host;            
            this.ptr = VKMemoryManager.map(host.deviceHandle, host.memory, offset, size);
        }
                
        public ByteBuffer getPointer() {
            return this.ptr;
        }        
        
        public void write(final float... floats) {
            if (!this.isValid()) {
                throw new IllegalStateException("MappedBuffer data is no longer valid!");
            }
            
            final int pos = this.ptr.position();
            final FloatBuffer fptr = this.ptr.asFloatBuffer();
            
            fptr.put(floats);            
            
            this.ptr.position(pos + Float.BYTES * fptr.position());
        }
        
        public void write(final int... ints) {
            if (!this.isValid()) {
                throw new IllegalStateException("MappedBuffer data is no longer valid!");
            }
            
            final int pos = this.ptr.position();
            final IntBuffer iptr = this.ptr.asIntBuffer();
            
            iptr.put(ints);
            
            this.ptr.position(pos + Integer.BYTES * iptr.position());
        }

        public void write(final ByteBuffer data) {
            if (!this.isValid()) {
                throw new IllegalStateException("MappedBuffer data is no longer valid!");
            }
            
            this.ptr.put(data);
        }

        public void read(final byte[] data, final int off, final int len) {
            if (!this.isValid()) {
                throw new IllegalStateException("MappedBuffer data is no longer valid!");
            }
            
            this.ptr.get(data, off, len);
        }

        @Override
        public boolean isValid() {
            return this.host != null && this.host.isValid();
        }

        @Override
        public void close() {
            if (this.isValid()) {
                VKMemoryManager.unmap(this.host.deviceHandle, this.host.memory);
                this.host = null;
                this.ptr = null;
            }
        }

    }
    
    public MappedBuffer map() {
        return map(0L, this.memory.size());
    }

    public MappedBuffer map(final long offset, final long size) {
        if (!this.isValid()) {
            throw new IllegalStateException("VKManagedBuffer is not valid!");
        }

        return new MappedBuffer(this, offset, size);
    }

    public void swap(VKManagedBuffer other) {
        super.swap(other);

        if (other != null) {
            final Allocation tmpMemory = other.memory;

            other.memory = this.memory;
            this.memory = tmpMemory;
        }
    }

    @Override
    public void close() {
        if (this.memory != null) {
            this.memory.free();
            this.memory = null;
        }
    }
}
