/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Set;
import org.lwjgl.vulkan.VK10;
import com.longlinkislong.gloop2.CBitfield;

/**
 *
 * @author zmichaels
 */
public enum VKAccess implements CBitfield {
    INDIRECT_COMMAND_READ(VK10.VK_ACCESS_INDIRECT_COMMAND_READ_BIT),
    INDEX_READ(VK10.VK_ACCESS_INDEX_READ_BIT),
    VERTEX_ATTRIBUTE_READ(VK10.VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT),
    UNIFORM_READ(VK10.VK_ACCESS_UNIFORM_READ_BIT),
    INPUT_ATTACHMENT_READ(VK10.VK_ACCESS_INPUT_ATTACHMENT_READ_BIT),
    SHADER_READ(VK10.VK_ACCESS_SHADER_READ_BIT),
    SHADER_WRITE(VK10.VK_ACCESS_SHADER_WRITE_BIT),
    COLOR_ATTACHMENT_READ(VK10.VK_ACCESS_COLOR_ATTACHMENT_READ_BIT),
    COLOR_ATTACHMENT_WRITE(VK10.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT),
    DEPTH_STENCIL_ATTACHMENT_READ(VK10.VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT),
    DEPTH_STENCIL_ATTACHMENT_WRITE(VK10.VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT),
    TRANSFER_READ(VK10.VK_ACCESS_TRANSFER_READ_BIT),
    TRANSFER_WRITE(VK10.VK_ACCESS_TRANSFER_WRITE_BIT),
    HOST_READ(VK10.VK_ACCESS_HOST_READ_BIT),
    HOST_WRITE(VK10.VK_ACCESS_HOST_WRITE_BIT),
    MEMORY_READ(VK10.VK_ACCESS_MEMORY_READ_BIT),
    MEMORY_WRITE(VK10.VK_ACCESS_MEMORY_WRITE_BIT);
    
    public final int value;
    
    private VKAccess(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    public static Set<VKAccess> flags(final int bits) {
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<VKAccess> flags) {
        return Tools.flagsToBitfield(flags);
    }
}
