/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKImageType implements CEnum {
    IMAGE_1D(VK10.VK_IMAGE_TYPE_1D),
    IMAGE_2D(VK10.VK_IMAGE_TYPE_2D),
    IMAGE_3D(VK10.VK_IMAGE_TYPE_3D);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKImageType(final int value) {
        this.value = value;
    }
    
    public static Optional<VKImageType> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
