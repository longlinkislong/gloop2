/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.memory.Allocation;
import com.longlinkislong.gloop2.memory.VKMemoryManager;
import java.nio.ByteBuffer;
import java.util.Set;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkMemoryRequirements;

/**
 *
 * @author zmichaels
 */
public class VKManagedImage extends VKImage {
    
    Allocation memory;
    Set<VKMemoryProperty> memoryProperties;
    
    public VKManagedImage() {}
    
    public VKManagedImage(VKContext ctx, VKImageInfo info, final Set<VKMemoryProperty> memoryProperties) {
        super(ctx, info);
        
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkMemoryRequirements memReq = VkMemoryRequirements.callocStack(mem);
            
            VK10.vkGetImageMemoryRequirements(ctx.device.handle, handle, memReq);
            
            this.memoryProperties = memoryProperties;
            this.memory = ctx.memoryManager.allocateImage(memReq, VKMemoryProperty.bits(memoryProperties));
            
            final int err = VK10.vkBindImageMemory(ctx.device.handle, handle, memory.ptr(), memory.offset());
            
            assert Tools.vksuccess(err) : Tools.vkerr(err);                        
        }
    }
    
    public MappedImage map(final long offset, final long size) {
        if (!this.isValid()) {
            throw new IllegalStateException("MappedImage is not valid!");
        }
        
        return new MappedImage(this, offset, size);
    }
    
    public MappedImage map() {
        return this.map(0L, this.memory.size());
    }
    
    public static final class MappedImage implements VKObject {
        private VKManagedImage host;
        private ByteBuffer ptr;
        
        private MappedImage(final VKManagedImage host, final long offset, final long size) {
            this.host = host;
            this.ptr = VKMemoryManager.map(host.deviceHandle, host.memory, offset, size);
        }
        
        public void write(final ByteBuffer data) {
            if (!this.isValid()) {
                throw new IllegalStateException("MappedImage data is no longer valid!");
            }
            
            this.ptr.put(data);
        }
        
        public void read(final byte[] data, final int off, final int len) {
            if (!this.isValid()) {
                throw new IllegalStateException("MappedImage data is no longer valid!");
            }
            
            this.ptr.get(data, off, len);
        }
        
        @Override
        public boolean isValid() {
            return this.host != null && this.host.isValid();
        }

        @Override
        public void close() {
            if (this.isValid()) {
                VKMemoryManager.unmap(this.host.deviceHandle, this.host.memory);
                this.host = null;
                this.ptr = null;
            }
        }        
    }
    
    public void swap(final VKManagedImage other) {
        super.swap(other);
        
        final Allocation tmpMemory = other.memory;
        
        other.memory = this.memory;
        this.memory = tmpMemory;
    }
    
    @Override
    public void close() {
        super.close();
        
        if (this.memory != null) {
            this.memory.free();
            this.memory = null;
        }
    }
}
