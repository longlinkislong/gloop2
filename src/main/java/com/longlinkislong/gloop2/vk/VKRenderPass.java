/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkAttachmentDescription;
import org.lwjgl.vulkan.VkAttachmentReference;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkRenderPassCreateInfo;
import org.lwjgl.vulkan.VkSubpassDependency;
import org.lwjgl.vulkan.VkSubpassDescription;

/**
 *
 * @author zmichaels
 */
public class VKRenderPass implements VKObject {

    VkDevice deviceHandle;
    long handle;
    private VKRenderPassInfo info;
    
    public VKRenderPassInfo getInfo() {
        return this.info;
    }
    
    public VKRenderPass() {}
    
    VKRenderPass(final VKDevice dev, final VKRenderPassInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {           
            final VkRenderPassCreateInfo renderPassCreateInfo = VkRenderPassCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO)
                    .flags(info.flags);
            
            final VkAttachmentDescription.Buffer pAttachments = VkAttachmentDescription.callocStack(info.attachments.size(), mem);
            
            
            for (int i = 0; i < info.attachments.size(); i++) {
                final VKRenderPassInfo.Attachment attachment = info.attachments.get(i);
                
                pAttachments.get(i)
                        .flags(attachment.flags)
                        .format(attachment.format.value)
                        .samples(attachment.samples)
                        .loadOp(attachment.loadOp.value)
                        .storeOp(attachment.storeOp.value)
                        .stencilLoadOp(attachment.stencilLoadOp.value)
                        .stencilStoreOp(attachment.stencilStoreOp.value)
                        .initialLayout(attachment.initialLayout.value)
                        .finalLayout(attachment.finalLayout.value);
            }
            
            renderPassCreateInfo.pAttachments(pAttachments);
            
            final VkSubpassDescription.Buffer pSubpasses = VkSubpassDescription.callocStack(info.subpasses.size(), mem);
            
            for (int i = 0; i < info.subpasses.size(); i++) {
                final VKRenderPassInfo.Subpass subpass = info.subpasses.get(i);
                
                pSubpasses.get(i)
                        .flags(subpass.flags)
                        .pipelineBindPoint(subpass.pipelineBindPoint);
                
                if (!subpass.inputAttachments.isEmpty()) {
                    final VkAttachmentReference.Buffer pInputAttachments = VkAttachmentReference.callocStack(subpass.inputAttachments.size(), mem);
                    
                    for (int j = 0; j < subpass.inputAttachments.size(); j++) {
                        final VKRenderPassInfo.AttachmentReference inputRef = subpass.inputAttachments.get(j);
                        
                        pInputAttachments.get(j)
                                .attachment(inputRef.attachment)
                                .layout(inputRef.layout.value);                        
                    }
                                        
                    pSubpasses.get(i).pInputAttachments(pInputAttachments);
                }
                
                if (!subpass.colorAttachments.isEmpty()) {
                    final VkAttachmentReference.Buffer pColorAttachments = VkAttachmentReference.callocStack(subpass.colorAttachments.size(), mem);
                    
                    for (int j = 0; j < subpass.colorAttachments.size(); j++) {
                        final VKRenderPassInfo.AttachmentReference colorRef = subpass.colorAttachments.get(j);
                        
                        pColorAttachments.get(j)
                                .attachment(colorRef.attachment)
                                .layout(colorRef.layout.value);
                    }
                    
                    pSubpasses.get(i)
                            .pColorAttachments(pColorAttachments)
                            .colorAttachmentCount(subpass.colorAttachments.size());
                }
                
                if (!subpass.resolveAttachments.isEmpty()) {
                    final VkAttachmentReference.Buffer pResolveAttachments = VkAttachmentReference.callocStack(subpass.resolveAttachments.size(), mem);
                    
                    for (int j = 0; j < subpass.resolveAttachments.size(); j++) {
                        final VKRenderPassInfo.AttachmentReference resolveRef = subpass.resolveAttachments.get(j);
                        
                        pResolveAttachments.get(j)
                                .attachment(resolveRef.attachment)
                                .layout(resolveRef.layout.value);
                    }
                    
                    pSubpasses.get(i).pResolveAttachments(pResolveAttachments);
                }
                
                if (subpass.depthStencilAttachment.isUsed()) {
                    final VKRenderPassInfo.AttachmentReference depthStencilRef = subpass.depthStencilAttachment;
                    final VkAttachmentReference pDepthStencilAttachment = VkAttachmentReference.callocStack(mem)
                            .attachment(depthStencilRef.attachment)
                            .layout(depthStencilRef.layout.value);
                    
                    pSubpasses.get(i).pDepthStencilAttachment(pDepthStencilAttachment);
                }
                
                if (!subpass.preserveAttachments.isEmpty()) {
                    final IntBuffer pPreserveAttachments = mem.callocInt(subpass.preserveAttachments.size());
                    
                    subpass.preserveAttachments.forEach(pPreserveAttachments::put);
                    pPreserveAttachments.flip();
                    
                    pSubpasses.get(i).pPreserveAttachments(pPreserveAttachments);
                }
            }
            
            renderPassCreateInfo.pSubpasses(pSubpasses);
            
            final VkSubpassDependency.Buffer pDepdendencies = VkSubpassDependency.callocStack(info.dependencies.size(), mem);
            
            for (int i = 0; i < info.dependencies.size(); i++) {
                final VKRenderPassInfo.SubpassDependency dependency = info.dependencies.get(i);
                
                pDepdendencies.get(i)
                        .srcSubpass(dependency.srcSubpass)
                        .dstSubpass(dependency.dstSubpass)
                        .srcStageMask(dependency.srcStageMask)
                        .dstStageMask(dependency.dstStageMask)
                        .srcAccessMask(dependency.srcAccessMask)
                        .dstAccessMask(dependency.dstAccessMask)
                        .dependencyFlags(dependency.dependencyFlags);
            }
            
            renderPassCreateInfo.pDependencies(pDepdendencies);
            
            final LongBuffer pRenderPass = mem.callocLong(1);
            final int err = VK10.vkCreateRenderPass(dev.handle, renderPassCreateInfo, null, pRenderPass);
            
            assert Tools.vksuccess(err) : Tools.vkerr(err);
            
            this.handle = pRenderPass.get();
            this.deviceHandle = dev.handle;
            this.info = info;
        }
    }
    
    public boolean isCompatible(VKRenderPass other) {
        if (other == null) {
            return false;
        } else if (!this.isValid() || !other.isValid()) {
            return false;
        } else {
            return this.info.isCompatible(other.info);
        }
    }

    @Override
    public boolean isValid() {
        return this.handle > 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyRenderPass(deviceHandle, handle, null);
            this.deviceHandle = null;
            this.handle = 0L;
            this.info = null;
        }
    }
    
    public void swap(final VKRenderPass other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final VkDevice tmpDev = other.deviceHandle;
        final long tmpHandle = other.handle;
        final VKRenderPassInfo tmpInfo = other.info;
        
        other.deviceHandle = this.deviceHandle;
        other.handle = this.handle;
        other.info = this.info;
        
        this.deviceHandle = tmpDev;
        this.handle = tmpHandle;
        this.info = tmpInfo;
    }
}
