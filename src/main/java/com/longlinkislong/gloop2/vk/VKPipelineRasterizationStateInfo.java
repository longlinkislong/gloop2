/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineRasterizationStateCreateInfo;

/**
 *
 * @author zmichaels
 */
public final class VKPipelineRasterizationStateInfo {

    public static VKPipelineRasterizationStateInfo DEFAULT_OPENGL = new VKPipelineRasterizationStateInfo()
            .withFrontFace(VKFrontFace.COUNTER_CLOCKWISE)
            .withPolygonMode(VKPolygonMode.FILL)
            .withCullMode(VKCullMode.NONE)
            .withDepthClampEnable(false)
            .withLineWidth(1.0F);
    
    public final int flags;
    public final boolean depthClampEnable;
    public final boolean rasterizerDiscardEnable;
    public final VKPolygonMode polygonMode;
    public final VKCullMode cullMode;
    public final VKFrontFace frontFace;
    public final boolean depthBiasEnable;
    public final float depthBiasConstantFactor;
    public final float depthBiasClamp;
    public final float depthBiasSlopeFactor;
    public final float lineWidth;

    public VKPipelineRasterizationStateInfo(
            final int flags,
            final boolean depthClampEnable,
            final boolean rasterizerDiscardEnable,
            final VKPolygonMode polygonMode,
            final VKCullMode cullMode,
            final VKFrontFace frontFace,
            final boolean depthBiasEnable,
            final float depthBiasConstantFactor,
            final float depthBiasClamp,
            final float depthBiasSlopeFactor,
            final float lineWidth) {

        this.flags = flags;
        this.depthClampEnable = depthClampEnable;
        this.rasterizerDiscardEnable = rasterizerDiscardEnable;
        this.polygonMode = polygonMode;
        this.cullMode = cullMode;
        this.frontFace = frontFace;
        this.depthBiasEnable = depthBiasEnable;
        this.depthBiasConstantFactor = depthBiasConstantFactor;
        this.depthBiasClamp = depthBiasClamp;
        this.depthBiasSlopeFactor = depthBiasSlopeFactor;
        this.lineWidth = lineWidth;
    }

    public VKPipelineRasterizationStateInfo() {
        this(0, false, false, VKPolygonMode.FILL, VKCullMode.BACK, VKFrontFace.COUNTER_CLOCKWISE, false, 0F, 0F, 0F, 0F);
    }
    
    public VKPipelineRasterizationStateInfo withFlags(final int flags) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public VKPipelineRasterizationStateInfo withDepthClampEnable(final boolean depthClampEnable) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public VKPipelineRasterizationStateInfo withRasterizerDiscardEnable(final boolean rasterizerDiscardEnable) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public VKPipelineRasterizationStateInfo withPolygonMode(final VKPolygonMode polygonMode) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public VKPipelineRasterizationStateInfo withCullMode(final VKCullMode cullMode) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public VKPipelineRasterizationStateInfo withFrontFace(final VKFrontFace frontFace) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public VKPipelineRasterizationStateInfo withDepthBiasEnable(final boolean depthBiasEnable) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public VKPipelineRasterizationStateInfo withDepthBiasConstantFactor(final float depthBiasConstantFactor) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public VKPipelineRasterizationStateInfo withDepthBiasClamp(final float depthBiasClamp) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public VKPipelineRasterizationStateInfo withDepthBiasSlopeFactor(final float depthBiasSlopeFactor) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    public VKPipelineRasterizationStateInfo withLineWidth(final float lineWidth) {
        return new VKPipelineRasterizationStateInfo(flags, depthClampEnable, rasterizerDiscardEnable, polygonMode, cullMode, frontFace, depthBiasEnable, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }
    
    void vkStruct(final VkPipelineRasterizationStateCreateInfo struct) {
        struct.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO)
                .flags(this.flags)
                .depthClampEnable(this.depthClampEnable)
                .rasterizerDiscardEnable(this.rasterizerDiscardEnable)
                .polygonMode(this.polygonMode.value)
                .cullMode(this.cullMode.value)
                .frontFace(this.frontFace.value)
                .depthBiasEnable(this.depthBiasEnable)
                .depthBiasConstantFactor(this.depthBiasConstantFactor)
                .depthBiasClamp(this.depthBiasClamp)
                .depthBiasSlopeFactor(this.depthBiasSlopeFactor)
                .lineWidth(this.lineWidth);
    }
}
