/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class VKPushConstantRange {

    public final Set<VKShaderStage> stageFlags;
    public final int offset;
    public final int size;

    public VKPushConstantRange(
            final Collection<VKShaderStage> stageFlags,
            final int offset,
            final int size) {

        this.stageFlags = Tools.copySet(stageFlags);
        this.offset = offset;
        this.size = size;
    }
    
    public VKPushConstantRange() {
        this(null, 0, 0);
    }
    
    public VKPushConstantRange withStageFlags(final Collection<VKShaderStage> stageFlags) {
        return new VKPushConstantRange(stageFlags, offset, size);
    }
    
    public VKPushConstantRange withStageFlags(final VKShaderStage... stageFlags) {
        return withStageFlags(Arrays.asList(stageFlags));
    }
    
    public VKPushConstantRange withOffset(final int offset) {
        return new VKPushConstantRange(stageFlags, offset, size);
    }
    
    public VKPushConstantRange withSize(final int size) {
        return new VKPushConstantRange(stageFlags, offset, size);
    }
}
