/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKImageViewInfo {

    public final int flags;
    public final VKImage image;
    public final VKImageViewType viewType;
    public final VKFormat format;
    public final VKComponentMapping components;
    public final VKImageSubresourceRange subresourceRange;

    public VKImageViewInfo(
            final int flags,
            final VKImage image,
            final VKImageViewType viewType,
            final VKFormat format,
            final VKComponentMapping components,
            final VKImageSubresourceRange subresourceRange) {

        this.flags = flags;
        this.image = image;
        this.viewType = viewType;
        this.format = format;
        this.components = components;
        this.subresourceRange = subresourceRange;
    }
    
    public VKImageViewInfo() {
        this(0, null, VKImageViewType.VIEW_1D, VKFormat.B8G8R8A8_UNORM, new VKComponentMapping(), null);
    }
    
    public VKImageViewInfo withFlags(final int flags) {
        return new VKImageViewInfo(flags, image, viewType, format, components, subresourceRange);
    }
    
    public VKImageViewInfo withImage(final VKImage image) {
        return new VKImageViewInfo(flags, image, viewType, format, components, subresourceRange);
    }
    
    public VKImageViewInfo withViewType(final VKImageViewType viewType) {
        return new VKImageViewInfo(flags, image, viewType, format, components, subresourceRange);
    }
    
    public VKImageViewInfo withFormat(final VKFormat format) {
        return new VKImageViewInfo(flags, image, viewType, format, components, subresourceRange);
    }
    
    public VKImageViewInfo withComponents(final VKComponentMapping components) {
        return new VKImageViewInfo(flags, image, viewType, format, components, subresourceRange);
    }
    
    public VKImageViewInfo withSubresourceRange(final VKImageSubresourceRange subresourceRange) {
        return new VKImageViewInfo(flags, image, viewType, format, components, subresourceRange);
    }
    
    public VKImageView create(final VKContext ctx) {
        return new VKImageView(ctx, this);
    }
    
    public static VKImageViewInfo wrap(final VKImage image) {        
        return new VKImageViewInfo()
                .withImage(image)
                .withViewType(VKImageViewType.of(image.info.imageType.value).get())
                .withFormat(image.info.format)
                .withSubresourceRange(image.getFullRange());
    }
}
