/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public class VKGraphicsPipelineInfo {
    
    public static enum CreateFlag implements CBitfield {
        DISABLE_OPTIMIZATION(VK10.VK_PIPELINE_CREATE_DISABLE_OPTIMIZATION_BIT),
        ALLOW_DERIVATIVES(VK10.VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT),
        DERIVATIVE(VK10.VK_PIPELINE_CREATE_DERIVATIVE_BIT);
        
        private final int value;
        
        @Override
        public int getValue() {
            return this.value;
        }
        
        private CreateFlag(final int value) {
            this.value = value;
        }
        
        public static Set<CreateFlag> flags(final int bits) {
            return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
        }
        
        public static int bits(final Set<CreateFlag> flags) {
            return Tools.flagsToBitfield(flags);
        }
    }
    
    public final Set<CreateFlag> flags;
    public final List<VKPipelineShaderStageInfo> stages;
    public final VKPipelineVertexInputStateInfo vertexInputState;
    public final VKPipelineInputAssemblyStateInfo inputAssemblyState;
    public final VKPipelineTessellationStateInfo tessellationState;
    public final VKPipelineViewportStateInfo viewportState;
    public final VKPipelineRasterizationStateInfo rasterizationState;
    public final VKPipelineMultisampleStateInfo multisampleState;
    public final VKPipelineDepthStencilStateInfo depthStencilState;
    public final VKPipelineColorBlendStateInfo colorBlendState;
    public final VKPipelineDynamicStateInfo dynamicState;
    public final VKPipelineLayout layout;
    public final VKRenderPass renderPass;
    public final int subpass;
    public final VKGraphicsPipeline basePipeline;
    public final int basePipelineIndex;
    
    public VKGraphicsPipelineInfo(
            final Collection<CreateFlag> flags,
            final Collection<VKPipelineShaderStageInfo> stages,
            final VKPipelineVertexInputStateInfo vertexInputState,
            final VKPipelineInputAssemblyStateInfo assemblyState,
            final VKPipelineTessellationStateInfo tessellationState,
            final VKPipelineViewportStateInfo viewportState,
            final VKPipelineRasterizationStateInfo rasterizationState,
            final VKPipelineMultisampleStateInfo multisampleState,
            final VKPipelineDepthStencilStateInfo depthStencilState,
            final VKPipelineColorBlendStateInfo colorBlendState,
            final VKPipelineDynamicStateInfo dynamicState,
            final VKPipelineLayout layout,
            final VKRenderPass renderPass,
            final int subpass,
            final VKGraphicsPipeline basePipelineHandle,
            final int basePipelineIndex) {
        
        this.flags = Tools.copySet(flags);
        this.stages = Tools.copyList(stages);
        this.vertexInputState = vertexInputState;
        
        this.inputAssemblyState = assemblyState;
        this.tessellationState = tessellationState;
        this.viewportState = viewportState;
        this.rasterizationState = rasterizationState;
        this.multisampleState = multisampleState;
        this.depthStencilState = depthStencilState;
        this.colorBlendState = colorBlendState;
        this.dynamicState = dynamicState;
        this.layout = layout;
        this.renderPass = renderPass;
        this.subpass = subpass;
        this.basePipeline = basePipelineHandle;
        this.basePipelineIndex = basePipelineIndex;
    }
    
    public VKGraphicsPipelineInfo() {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, 0, null, 0);
    }
    
    public VKGraphicsPipelineInfo withFlags(final Collection<CreateFlag> flags) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withFlags(final CreateFlag... flags) {
        return withFlags(Arrays.asList(flags));
    }
    
    public VKGraphicsPipelineInfo withStages(final Collection<VKPipelineShaderStageInfo> stages) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withStages(final VKPipelineShaderStageInfo... stages) {
        return withStages(Arrays.asList(stages));
    }
    
    public VKGraphicsPipelineInfo withVertexInputState(final VKPipelineVertexInputStateInfo vertexInputState) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withAssemblyState(final VKPipelineInputAssemblyStateInfo assemblyState) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, assemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withTessellationState(final VKPipelineTessellationStateInfo tessellationState) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withViewportState(final VKPipelineViewportStateInfo viewportState) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withRasterizationState(final VKPipelineRasterizationStateInfo rasterizationState) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withMultisampleState(final VKPipelineMultisampleStateInfo multisampleState) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withDepthStencilState(final VKPipelineDepthStencilStateInfo depthStencilState) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withColorBlendState(final VKPipelineColorBlendStateInfo colorBlendState) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withDynamicState(final VKPipelineDynamicStateInfo dynamicState) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withLayout(final VKPipelineLayout layout) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withRenderPass(final VKRenderPass renderPass) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withSubpass(final int subpass) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withBasePipelineHandle(final VKGraphicsPipeline basePipelineHandle) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipelineHandle, basePipelineIndex);
    }
    
    public VKGraphicsPipelineInfo withBasePipelineIndex(final int basePipelineIndex) {
        return new VKGraphicsPipelineInfo(flags, stages, vertexInputState, inputAssemblyState, tessellationState, viewportState, rasterizationState, multisampleState, depthStencilState, colorBlendState, dynamicState, layout, renderPass, subpass, basePipeline, basePipelineIndex);
    }
    
    public VKGraphicsPipeline create(final VKContext ctx) {
        return new VKGraphicsPipeline(ctx, this);
    }    
    
    public static VKGraphicsPipelineInfo inferDynamicViewports(final VKGraphicsPipelineInfo base, final int count) {
        if (base == null) {
            return inferDynamicViewports(new VKGraphicsPipelineInfo(), count);
        }
        
        return base.withViewportState(VKPipelineViewportStateInfo.dynamicViewports(count))
                .withDynamicState(new VKPipelineDynamicStateInfo()
                        .withDynamicStates(VKDynamicState.VIEWPORT, VKDynamicState.SCISSOR));
    }
    
    public static VKGraphicsPipelineInfo inferFromRenderPass(final VKGraphicsPipelineInfo base, final VKRenderPass rp) {
        if (rp.getInfo().subpasses.size() == 1) {
            return inferFromRenderPass(base, rp, 0);
        } else {
            throw new IllegalArgumentException("RenderPass has more than 1 subpass!");
        }
    }
    
    public static VKGraphicsPipelineInfo inferFromRenderPass(final VKGraphicsPipelineInfo base, final VKRenderPass rp, final int subpassIndex) {
        if (base == null) {
            return inferFromRenderPass(new VKGraphicsPipelineInfo(), rp, subpassIndex);
        }
        
        final VKRenderPassInfo rpinfo = rp.getInfo();
        final VKRenderPassInfo.Subpass subpass = rpinfo.subpasses.get(subpassIndex);
        final List<VKPipelineColorBlendAttachmentState> colorBlendStateAttachments = subpass.colorAttachments.stream()
                .map(ref -> new VKPipelineColorBlendAttachmentState().withColorWriteMask(0xF))
                .collect(Collectors.toList());        
        
        // unsure if this is proper...
        final int samples = subpass.colorAttachments.stream()
                .mapToInt(ref -> ref.attachment)
                .mapToObj(rpinfo.attachments::get)
                .mapToInt(attachment -> attachment.samples)
                .max()
                .orElse(0);                
        
        return base.withRenderPass(rp)
                .withMultisampleState(new VKPipelineMultisampleStateInfo()
                        .withRasterizationSamples(samples))
                .withColorBlendState(new VKPipelineColorBlendStateInfo()
                        .withAttachments(colorBlendStateAttachments))
                .withSubpass(subpassIndex);
    }
}
