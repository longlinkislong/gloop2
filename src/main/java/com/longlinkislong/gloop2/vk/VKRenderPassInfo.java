/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public class VKRenderPassInfo {

    public enum LoadOp implements CEnum {
        CLEAR(VK10.VK_ATTACHMENT_LOAD_OP_CLEAR),
        LOAD(VK10.VK_ATTACHMENT_LOAD_OP_LOAD),
        DONT_CARE(VK10.VK_ATTACHMENT_LOAD_OP_DONT_CARE);

        public final int value;
        
        @Override
        public int getValue() {
            return this.value;
        }

        private LoadOp(final int value) {
            this.value = value;
        }
        
        public static Optional<LoadOp> valueOf(int vkenum) {
            return Tools.valueOf(vkenum, Arrays.stream(values()));
        }
    }

    public enum StoreOp implements CEnum {
        STORE(VK10.VK_ATTACHMENT_STORE_OP_STORE),
        DONT_CARE(VK10.VK_ATTACHMENT_STORE_OP_DONT_CARE);
        
        public final int value;
        
        @Override
        public int getValue() {
            return this.value;
        }

        private StoreOp(final int value) {
            this.value = value;
        }
        
        public static Optional<StoreOp> valueOf(final int vkenum) {
            return Tools.valueOf(vkenum, Arrays.stream(values()));
        }
    }

    public static final class Attachment {

        public final int flags;
        public final VKFormat format;
        public final int samples;
        public final LoadOp loadOp;
        public final StoreOp storeOp;
        public final LoadOp stencilLoadOp;
        public final StoreOp stencilStoreOp;
        public final VKImageLayout initialLayout;
        public final VKImageLayout finalLayout;

        public Attachment(
                final int flags,
                final VKFormat format,
                final int samples,
                final LoadOp loadOp, final StoreOp storeOp,
                final LoadOp stencilLoadOp, final StoreOp stencilStoreOp,
                final VKImageLayout initialLayout, final VKImageLayout finalLayout) {

            this.flags = flags;
            this.format = format;
            this.samples = samples;
            this.loadOp = loadOp;
            this.storeOp = storeOp;
            this.stencilLoadOp = stencilLoadOp;
            this.stencilStoreOp = stencilStoreOp;
            this.initialLayout = initialLayout;
            this.finalLayout = finalLayout;
        }

        public Attachment() {
            this(
                    0,
                    VKFormat.B8G8R8A8_UNORM, VK10.VK_SAMPLE_COUNT_1_BIT,
                    LoadOp.CLEAR, StoreOp.STORE,
                    LoadOp.DONT_CARE, StoreOp.DONT_CARE,
                    VKImageLayout.UNDEFINED,
                    VKImageLayout.GENERAL);
        }

        public Attachment withFlags(final int flags) {
            return new Attachment(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
        }

        public Attachment withFormat(final VKFormat format) {
            return new Attachment(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
        }

        public Attachment withSamples(final int samples) {
            return new Attachment(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
        }

        public Attachment withLoadOp(final LoadOp loadOp) {
            return new Attachment(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
        }

        public Attachment withStoreOp(final StoreOp storeOp) {
            return new Attachment(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
        }

        public Attachment withStencilLoadOp(final LoadOp loadOp) {
            return new Attachment(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
        }

        public Attachment withStencilStoreOp(final StoreOp storeOp) {
            return new Attachment(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);
        }

        public Attachment withInitialLayout(final VKImageLayout initialLayout) {
            return new Attachment(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);

        }

        public Attachment withFinalLayout(final VKImageLayout finalLayout) {
            return new Attachment(flags, format, samples, loadOp, storeOp, stencilLoadOp, stencilStoreOp, initialLayout, finalLayout);

        }

        public static Attachment of(final VKImage img) {                        
            if (img.info.format.isColor()) {
                return new Attachment()
                        .withFormat(img.info.format)
                        .withFinalLayout(VKImageLayout.COLOR_ATTACHMENT_OPTIMAL);

            } else {                
                return new Attachment()
                        .withFormat(img.info.format)
                        .withFinalLayout(VKImageLayout.DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
            }
        }
    }

    public final List<Attachment> attachments;        

    public static final class SubpassDependency {

        public final int srcSubpass;
        public final int dstSubpass;
        public final int srcStageMask;
        public final int dstStageMask;
        public final int srcAccessMask;
        public final int dstAccessMask;
        public final int dependencyFlags;

        public SubpassDependency(
                final int srcSubpass, final int dstSubpass,
                final int srcStageMask, final int dstStageMask,
                final int srcAccessMask, final int dstAccessMask,
                final int dependencyFlags) {

            this.srcSubpass = srcSubpass;
            this.dstSubpass = dstSubpass;
            this.srcStageMask = srcStageMask;
            this.dstStageMask = dstStageMask;
            this.srcAccessMask = srcAccessMask;
            this.dstAccessMask = dstAccessMask;
            this.dependencyFlags = dependencyFlags;
        }

        public SubpassDependency() {
            this(0, 0, 0, 0, 0, 0, 0);
        }

        public SubpassDependency withSrcSubpass(final int srcSubpass) {
            return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
        }

        public SubpassDependency withDstSubpass(final int dstSubpass) {
            return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
        }

        public SubpassDependency withSrcStageMask(final int srcStageMask) {
            return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
        }

        public SubpassDependency withDstStageMask(final int dstStageMask) {
            return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
        }

        public SubpassDependency withSrcAccessMask(final int srcAccessMask) {
            return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
        }

        public SubpassDependency withDstAccessMask(final int dstAccessMask) {
            return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
        }

        public SubpassDependency withDependencyFlags(final int dependencyFlags) {
            return new SubpassDependency(srcSubpass, dstSubpass, srcStageMask, dstStageMask, srcAccessMask, dstAccessMask, dependencyFlags);
        }
    }

    public final List<SubpassDependency> dependencies;

    public static final class AttachmentReference {

        public final int attachment;
        public final VKImageLayout layout;
        
        public boolean isUsed() {            
            return this.attachment != VK10.VK_ATTACHMENT_UNUSED;
        }

        public AttachmentReference(final int attachment, final VKImageLayout layout) {
            this.attachment = attachment;
            this.layout = layout;
        }

        public AttachmentReference() {
            this(VK10.VK_ATTACHMENT_UNUSED, VKImageLayout.GENERAL);
        }

        public AttachmentReference withAttachment(final int attachment) {
            return new AttachmentReference(attachment, layout);
        }

        public AttachmentReference withLayout(final VKImageLayout layout) {
            return new AttachmentReference(attachment, layout);
        }

    }

    public static final class Subpass {

        public final int flags;
        public final int pipelineBindPoint;
        public final List<AttachmentReference> colorAttachments;
        public final List<AttachmentReference> resolveAttachments;
        public final AttachmentReference depthStencilAttachment;
        public final List<AttachmentReference> inputAttachments;
        public final List<Integer> preserveAttachments;

        public Subpass() {
            this(0, VK10.VK_PIPELINE_BIND_POINT_GRAPHICS, null, null, null, null, null);
        }

        public Subpass(
                final int flags,
                final int pipelineBindPoint,
                final List<AttachmentReference> inputAttachments,
                final List<AttachmentReference> colorAttachments,
                final List<AttachmentReference> resolveAttachments,
                final AttachmentReference depthStencilAttachment,
                final List<Integer> preserveAttachments) {

            this.flags = flags;
            this.pipelineBindPoint = pipelineBindPoint;
            this.inputAttachments = Tools.copyList(inputAttachments);
            this.colorAttachments = Tools.copyList(colorAttachments);
            this.resolveAttachments = Tools.copyList(resolveAttachments);
            this.depthStencilAttachment = (depthStencilAttachment == null)
                    ? new AttachmentReference()
                    : depthStencilAttachment;
            
            this.preserveAttachments = Tools.copyList(preserveAttachments);
        }

        public Subpass withFlags(final int flags) {
            return new Subpass(flags, pipelineBindPoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
        }

        public Subpass withPipelineBindPoint(final int pipelineBindPoint) {
            return new Subpass(flags, pipelineBindPoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
        }

        public Subpass withInputAttachments(final List<AttachmentReference> inputAttachments) {
            return new Subpass(flags, pipelineBindPoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
        }

        public Subpass withInputAttachments(final AttachmentReference... inputAttachments) {
            return withInputAttachments(Arrays.asList(inputAttachments));
        }

        public Subpass withColorAttachments(final List<AttachmentReference> colorAttachments) {
            return new Subpass(flags, pipelineBindPoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
        }

        public Subpass withColorAttachments(final AttachmentReference... colorAttachments) {
            return withColorAttachments(Arrays.asList(colorAttachments));
        }

        public Subpass withResolveAttachments(final List<AttachmentReference> resolveAttachments) {
            return new Subpass(flags, pipelineBindPoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
        }

        public Subpass withResolveAttachments(final AttachmentReference... resolveAttachments) {
            return withResolveAttachments(Arrays.asList(resolveAttachments));
        }

        public Subpass withDepthStencilAttachment(final AttachmentReference depthStencilAttachment) {
            return new Subpass(flags, pipelineBindPoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
        }

        public Subpass withPreserveAttachments(final List<Integer> preserveAttachments) {
            return new Subpass(flags, pipelineBindPoint, inputAttachments, colorAttachments, resolveAttachments, depthStencilAttachment, preserveAttachments);
        }

        public Subpass withPreserveAttachments(final Integer... preserveAttachments) {
            return withPreserveAttachments(Arrays.asList(preserveAttachments));
        }

        public static Subpass infer(final Attachment... attachments) {
            return infer(Arrays.asList(attachments));
        }
        
        public static Subpass infer(final List<Attachment> attachments) {
            final List<AttachmentReference> colorAttachments = new ArrayList<>();
            AttachmentReference depthStencilAttachment = null;

            for (int attachmentId = 0; attachmentId < attachments.size(); attachmentId++) {
                if (attachments.get(attachmentId).format.isColor()) {
                    colorAttachments.add(new AttachmentReference(attachmentId, VKImageLayout.COLOR_ATTACHMENT_OPTIMAL));
                } else if (depthStencilAttachment == null) {
                    depthStencilAttachment = new AttachmentReference(attachmentId, VKImageLayout.DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
                } else {
                    throw new IllegalArgumentException("Only one DepthStencil attachment is allowed!");
                }
            }

            return new Subpass()                                        
                    .withColorAttachments(colorAttachments)
                    .withDepthStencilAttachment(depthStencilAttachment);
        }
    }

    public final List<Subpass> subpasses;

    public final int flags;

    public VKRenderPassInfo(
            final int flags,
            final List<Attachment> attachments,
            final List<Subpass> subpasses,
            final List<SubpassDependency> dependencies) {

        this.flags = flags;
        this.attachments = Tools.copyList(attachments);
        this.subpasses = Tools.copyList(subpasses);
        this.dependencies = Tools.copyList(dependencies);
    }

    public VKRenderPassInfo() {
        this(0, null, null, DEFAULT_DEPENDENCIES);
    }

    public VKRenderPassInfo withFlags(final int flags) {
        return new VKRenderPassInfo(flags, attachments, subpasses, dependencies);
    }

    public VKRenderPassInfo withAttachments(final List<Attachment> attachments) {
        return new VKRenderPassInfo(flags, attachments, subpasses, dependencies);
    }

    public VKRenderPassInfo withAttachments(final Attachment... attachments) {
        return withAttachments(Arrays.asList(attachments));
    }

    public VKRenderPassInfo withSubpasses(final List<Subpass> subpasses) {
        return new VKRenderPassInfo(flags, attachments, subpasses, dependencies);
    }

    public VKRenderPassInfo withSubpasses(final Subpass... subpasses) {
        return withSubpasses(Arrays.asList(subpasses));
    }

    public VKRenderPassInfo withDependencies(final List<SubpassDependency> dependencies) {
        return new VKRenderPassInfo(flags, attachments, subpasses, dependencies);
    }

    public VKRenderPassInfo withDependencies(final SubpassDependency... dependencies) {
        return withDependencies(Arrays.asList(dependencies));
    }           
    
    public static VKRenderPassInfo infer(final List<Attachment> attachments) {
        return new VKRenderPassInfo()
                .withAttachments(attachments)
                .withSubpasses(Subpass.infer(attachments));
    }
    
    public static VKRenderPassInfo infer(final Attachment... attachments) {
        return new VKRenderPassInfo()
                .withAttachments(attachments)
                .withSubpasses(Subpass.infer(attachments));
    }            

    public static final List<SubpassDependency> DEFAULT_DEPENDENCIES = Collections.unmodifiableList(Arrays.asList(
            new SubpassDependency()
                    .withSrcSubpass(VK10.VK_SUBPASS_EXTERNAL)
                    .withDstSubpass(0)
                    .withSrcStageMask(VK10.VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT)
                    .withDstStageMask(VK10.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
                    .withSrcAccessMask(VK10.VK_ACCESS_MEMORY_READ_BIT)
                    .withDstAccessMask(VK10.VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK10.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT)
                    .withDependencyFlags(VK10.VK_DEPENDENCY_BY_REGION_BIT),
            new SubpassDependency()
                    .withSrcSubpass(0)
                    .withDstSubpass(VK10.VK_SUBPASS_EXTERNAL)
                    .withSrcStageMask(VK10.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
                    .withDstStageMask(VK10.VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT)
                    .withSrcAccessMask(VK10.VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK10.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT)
                    .withDstAccessMask(VK10.VK_ACCESS_MEMORY_READ_BIT)
                    .withDependencyFlags(VK10.VK_DEPENDENCY_BY_REGION_BIT)));

    private static boolean isCompatible(
            final List<Attachment> lA, final AttachmentReference rA,
            final List<Attachment> lB, final AttachmentReference rB) {

        
        if (rA.attachment == VK10.VK_ATTACHMENT_UNUSED && rB.attachment == VK10.VK_ATTACHMENT_UNUSED) {
            return true;
        } else {
            final Attachment a = lA.get(rA.attachment);
            final Attachment b = lB.get(rB.attachment);

            return a.format == b.format
                    && a.samples == b.samples;
        }
    }

    private static boolean isCompatible(
            final List<Attachment> lA, final List<AttachmentReference> rA,
            final List<Attachment> lB, final List<AttachmentReference> rB) {

        final int count = Math.max(rA.size(), rB.size());
        
        for (int i = 0; i < count; i++) {
            final AttachmentReference a = (i < rA.size()) ? rA.get(i) : new AttachmentReference();
            final AttachmentReference b = (i < rB.size()) ? rB.get(i) : new AttachmentReference();

            if (!isCompatible(lA, a, lB, b)) {
                return false;
            }
        }

        return true;
    }

    public boolean isCompatible(final VKRenderPassInfo other) {
        if (this.subpasses.size() == 1 && other.subpasses.size() == 1) {
            
            return isCompatible(
                    this.attachments, this.subpasses.get(0).colorAttachments,
                    other.attachments, other.subpasses.get(0).colorAttachments)
                    && isCompatible(
                            this.attachments, this.subpasses.get(0).inputAttachments,
                            other.attachments, other.subpasses.get(0).inputAttachments)
                    && isCompatible(
                            this.attachments, this.subpasses.get(0).resolveAttachments,
                            other.attachments, other.subpasses.get(0).resolveAttachments)
                    && isCompatible(
                            this.attachments, Collections.singletonList(this.subpasses.get(0).depthStencilAttachment),
                            other.attachments, Collections.singletonList(other.subpasses.get(0).depthStencilAttachment));
        } else {
            // confusing specs
            return false;
        }
    }

    public VKRenderPass create(final VKContext ctx) {
        return new VKRenderPass(ctx.device, this);
    }
    
    public VKRenderPass getCompatible(final VKContext ctx) {        
        final Optional<VKRenderPass> cachedDummyPass = ctx.dummyRenderPasses.stream()
                .filter(Objects::nonNull)
                .filter(VKRenderPass::isValid)
                .filter(rp -> rp.getInfo().isCompatible(this))
                .findFirst();
        
        if (cachedDummyPass.isPresent()) {
            return cachedDummyPass.get();
        } else {
            final VKRenderPass dummyPass = create(ctx);
            
            ctx.dummyRenderPasses.add(dummyPass);
            
            return dummyPass;
        }                
    }
}
