/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public final class VKInstanceCreateInfo {

    public final Set<String> extensions;
    public final Set<String> layers;
    public final int versionMajor;
    public final int versionMinor;
    public final int versionPatch;
    public final String applicationName;
    public final String engineName;

    public VKInstanceCreateInfo(
            final Collection<String> extensions,
            final Collection<String> layers,
            final int versionMajor,
            final int versionMinor,
            final int versionPatch,
            final String applicationName,
            final String engineName) {

        this.extensions = Tools.copySet(extensions);
        this.layers = Tools.copySet(layers);
        this.versionMajor = versionMajor;
        this.versionMinor = versionMinor;
        this.versionPatch = versionPatch;
        this.applicationName = applicationName;
        this.engineName = engineName;
    }

    public VKInstanceCreateInfo() {
        this(null, null, 1, 0, 0, "VKApp", "");
    }
    
    public VKInstanceCreateInfo withVersion(final int versionMajor, final int versionMinor, final int versionPatch) {
        return new VKInstanceCreateInfo(extensions, layers, versionMajor, versionMinor, versionPatch, applicationName, engineName);
    }
    
    public VKInstanceCreateInfo withExtensions(final Collection<String> extensions) {
        return new VKInstanceCreateInfo(extensions, layers, versionMajor, versionMinor, versionPatch, applicationName, engineName);
    }
    
    public VKInstanceCreateInfo withExtensions(final String... extensions) {
        return withExtensions(Arrays.asList(extensions));
    }
    
    public VKInstanceCreateInfo withLayers(final String... layers) {
        return withLayers(Arrays.asList(layers));
    }
    
    public VKInstanceCreateInfo withLayers(final Collection<String> layers) {
        return new VKInstanceCreateInfo(extensions, layers, versionMajor, versionMinor, versionPatch, applicationName, engineName);
    }
    
    public VKInstanceCreateInfo withApplicationName(final String applicationName) {
        return new VKInstanceCreateInfo(extensions, layers, versionMajor, versionMinor, versionPatch, applicationName, engineName);
    }
    
    public VKInstanceCreateInfo withEngineName(final String engineName) {
        return new VKInstanceCreateInfo(extensions, layers, versionMajor, versionMinor, versionPatch, applicationName, engineName);
    }
    
    public VKInstance create() {
        return new VKInstance(this);
    }
}
