/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKBorderColor implements CEnum {
    FLOAT_TRANSPARENT_BLACK(VK10.VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK),
    INT_TRANSPARENT_BLACK(VK10.VK_BORDER_COLOR_INT_TRANSPARENT_BLACK),
    FLOAT_OPAQUE_BLACK(VK10.VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK),
    INT_OPAQUE_BLACK(VK10.VK_BORDER_COLOR_INT_OPAQUE_BLACK),
    FLOAT_OPAQUE_WHITE(VK10.VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE),
    INT_OPAQUE_WHITE(VK10.VK_BORDER_COLOR_INT_OPAQUE_WHITE);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKBorderColor(final int value) {
        this.value = value;
    }
    
    public static Optional<VKBorderColor> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
