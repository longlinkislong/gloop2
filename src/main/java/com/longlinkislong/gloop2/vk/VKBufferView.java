/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkBufferViewCreateInfo;
import org.lwjgl.vulkan.VkDevice;

/**
 * A buffer view represents a contiguous range of a buffer and a specific format
 * to be used to interpret the data. Buffer views are used to enable shaders to
 * access buffer contents interpreted as formatted data.
 *
 * @author zmichaels
 */
public class VKBufferView implements VKObject {

    long handle;
    VKBufferViewInfo info;
    VkDevice deviceHandle;

    /**
     * Creates an invalid VKBufferView.
     */
    public VKBufferView() {

    }

    /**
     * Creates a new VKBufferView from the VKBufferViewInfo.
     * @param ctx the Vulkan context.
     * @param info the VKBufferViewInfo.
     */
    public VKBufferView(final VKContext ctx, final VKBufferViewInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkBufferViewCreateInfo createInfo = VkBufferViewCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO)
                    .flags(info.flags)
                    .buffer(info.buffer.handle)
                    .offset(info.offset)
                    .range(info.range);

            final LongBuffer pHandle = mem.callocLong(1);
            final int err = VK10.vkCreateBufferView(ctx.device.handle, createInfo, null, pHandle);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            this.handle = pHandle.get(0);
            this.info = info;
            this.deviceHandle = ctx.device.handle;
        }
    }

    public void swap(final VKBufferView other) {
        if (other == null) {
            this.close();
            return;
        }

        final long tmpHandle = other.handle;
        final VkDevice tmpDevice = other.deviceHandle;
        final VKBufferViewInfo tmpInfo = other.info;

        other.handle = this.handle;
        other.deviceHandle = this.deviceHandle;
        other.info = this.info;

        this.handle = tmpHandle;
        this.deviceHandle = tmpDevice;
        this.info = tmpInfo;
    }

    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyBufferView(this.deviceHandle, this.handle, null);
            this.deviceHandle = null;
            this.handle = 0L;
            this.info = null;
        }
    }

    public VKBufferViewInfo getInfo() {
        return this.info;
    }
}
