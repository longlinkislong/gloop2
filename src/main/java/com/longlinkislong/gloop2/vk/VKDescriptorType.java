/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKDescriptorType implements CEnum {
    SAMPLER(VK10.VK_DESCRIPTOR_TYPE_SAMPLER),
    COMBINED_IMAGE_SAMPLER(VK10.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER),
    SAMPLED_IMAGE(VK10.VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE),
    STORAGE_IMAGE(VK10.VK_DESCRIPTOR_TYPE_STORAGE_IMAGE),
    UNIFORM_TEXEL_BUFFER(VK10.VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER),
    STORAGE_TEXEL_BUFFER(VK10.VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER),
    UNIFORM_BUFFER(VK10.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER),
    STORAGE_BUFFER(VK10.VK_DESCRIPTOR_TYPE_STORAGE_BUFFER),
    UNIFORM_BUFFER_DYNAMIC(VK10.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC),
    STORAGE_BUFFER_DYNAMIC(VK10.VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC),
    INPUT_ATTACHMENT(VK10.VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKDescriptorType(final int value) {
        this.value = value;
    }
    
    public static Optional<VKDescriptorType> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
