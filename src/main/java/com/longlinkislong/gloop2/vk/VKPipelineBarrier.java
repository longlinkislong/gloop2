/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class VKPipelineBarrier {     
    public final Set<VKPipelineStage> srcStageMask;
    public final Set<VKPipelineStage> dstStageMask;
    public final Set<VKDependency> dependencyFlags;
    public final List<VKMemoryBarrier> memoryBarriers;
    public final List<VKBufferMemoryBarrier> bufferMemoryBarriers;
    public final List<VKImageMemoryBarrier> imageMemoryBarriers;
    
    public VKPipelineBarrier(
        final Collection<VKPipelineStage> srcStageMask,
            final Collection<VKPipelineStage> dstStageMask,
            final Collection<VKDependency> dependencyFlags,
            final List<VKMemoryBarrier> memoryBarriers,
            final List<VKBufferMemoryBarrier> bufferMemoryBarriers,
            final List<VKImageMemoryBarrier> imageMemoryBarriers) {
        
        this.srcStageMask = Tools.copySet(srcStageMask);        
        this.dstStageMask = Tools.copySet(dstStageMask);
        this.dependencyFlags = Tools.copySet(dependencyFlags);        
        this.memoryBarriers = Tools.copyList(memoryBarriers);
        this.bufferMemoryBarriers = Tools.copyList(bufferMemoryBarriers);        
        this.imageMemoryBarriers = Tools.copyList(imageMemoryBarriers);
    }
    
    public VKPipelineBarrier() {
        this(null, null, null, null, null, null);
    }
    
    public VKPipelineBarrier withSrcStageMask(final Collection<VKPipelineStage> srcStageMask) {
        return new VKPipelineBarrier(srcStageMask, dstStageMask, dependencyFlags, memoryBarriers, bufferMemoryBarriers, imageMemoryBarriers);
    }
    
    public VKPipelineBarrier withSrcStageMask(final VKPipelineStage... srcStageMask) {
        return withSrcStageMask(Arrays.asList(srcStageMask));
    }
    
    public VKPipelineBarrier withDstStageMask(final Collection<VKPipelineStage> dstStageMask) {
        return new VKPipelineBarrier(srcStageMask, dstStageMask, dependencyFlags, memoryBarriers, bufferMemoryBarriers, imageMemoryBarriers);
    }
    
    public VKPipelineBarrier withDstStageMask(final VKPipelineStage... dstStageMask) {
        return withDstStageMask(Arrays.asList(dstStageMask));
    }
    
    public VKPipelineBarrier withDependencyFlags(final Collection<VKDependency> dependencyFlags) {
        return new VKPipelineBarrier(srcStageMask, dstStageMask, dependencyFlags, memoryBarriers, bufferMemoryBarriers, imageMemoryBarriers);
    }
    
    public VKPipelineBarrier withDependencyFlags(final VKDependency... dependencyFlags) {
        return withDependencyFlags(Arrays.asList(dependencyFlags));
    }
    
    public VKPipelineBarrier withMemoryBarriers(final List<VKMemoryBarrier> memoryBarriers) {
        return new VKPipelineBarrier(srcStageMask, dstStageMask, dependencyFlags, memoryBarriers, bufferMemoryBarriers, imageMemoryBarriers);
    }
    
    public VKPipelineBarrier withMemoryBarriers(final VKMemoryBarrier... memoryBarriers) {
        return withMemoryBarriers(Arrays.asList(memoryBarriers));
    }
    
    public VKPipelineBarrier withBufferMemoryBarriers(final List<VKBufferMemoryBarrier> bufferMemoryBarriers) {
        return new VKPipelineBarrier(srcStageMask, dstStageMask, dependencyFlags, memoryBarriers, bufferMemoryBarriers, imageMemoryBarriers);
    }
    
    public VKPipelineBarrier withBufferMemoryBarriers(final VKBufferMemoryBarrier... bufferMemoryBarriers) {
        return withBufferMemoryBarriers(Arrays.asList(bufferMemoryBarriers));
    }
    
    public VKPipelineBarrier withImageMemoryBarriers(final List<VKImageMemoryBarrier> imageMemoryBarriers) {
        return new VKPipelineBarrier(srcStageMask, dstStageMask, dependencyFlags, memoryBarriers, bufferMemoryBarriers, imageMemoryBarriers);
    }
    
    public VKPipelineBarrier withImageMemoryBarriers(final VKImageMemoryBarrier... imageMemoryBarriers) {
        return withImageMemoryBarriers(Arrays.asList(imageMemoryBarriers));
    }
}
