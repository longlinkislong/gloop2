/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKDescriptorImageInfo {

    public final VKSampler sampler;
    public final VKImageView imageView;
    public final VKImageLayout imageLayout;

    public VKDescriptorImageInfo(
            final VKSampler sampler,
            final VKImageView imageView,
            final VKImageLayout imageLayout) {

        this.sampler = sampler;
        this.imageView = imageView;
        this.imageLayout = imageLayout;
    }
    
    public VKDescriptorImageInfo() {
        this(null, null, VKImageLayout.UNDEFINED);
    }
    
    public VKDescriptorImageInfo withSampler(final VKSampler sampler) {
        return new VKDescriptorImageInfo(sampler, imageView, imageLayout);
    }
    
    public VKDescriptorImageInfo withImageView(final VKImageView imageView) {
        return new VKDescriptorImageInfo(sampler, imageView, imageLayout);
    }
    
    public VKDescriptorImageInfo withImageLayout(final VKImageLayout imageLayout) {
        return new VKDescriptorImageInfo(sampler, imageView, imageLayout);
    }
}
