/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKEventStatus implements CEnum {
    SET(VK10.VK_EVENT_SET),
    RESET(VK10.VK_EVENT_RESET);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    private VKEventStatus(final int value) {
        this.value = value;
    }
    
    public static Optional<VKEventStatus> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
