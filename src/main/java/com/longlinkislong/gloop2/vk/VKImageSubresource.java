/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class VKImageSubresource {

    public final Set<VKImageAspect> aspectMask;
    public final int mipLevel;
    public final int arrayLayer;

    public VKImageSubresource(
            final Collection<VKImageAspect> aspectMask,
            final int mipLevel,
            final int arrayLayer) {

        this.aspectMask = Tools.copySet(aspectMask);
        this.mipLevel = mipLevel;
        this.arrayLayer = arrayLayer;
    }

    public VKImageSubresource() {
        this(null, 0, 0);
    }

    public VKImageSubresource withAspectMask(final Collection<VKImageAspect> aspectMask) {
        return new VKImageSubresource(aspectMask, mipLevel, arrayLayer);
    }

    public VKImageSubresource withAspectMask(final VKImageAspect... aspectMask) {
        return withAspectMask(Arrays.asList(aspectMask));
    }

    public VKImageSubresource withMipLevel(final int mipLevel) {
        return new VKImageSubresource(aspectMask, mipLevel, arrayLayer);
    }

    public VKImageSubresource withArrayLayer(final int arrayLayer) {
        return new VKImageSubresource(aspectMask, mipLevel, arrayLayer);
    }
}
