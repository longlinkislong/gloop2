/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class VKImageMemoryBarrier {
    public static VKImageMemoryBarrier inferWriteStagingBarrier(final VKImageView imgview) {
        final VKImage img = imgview.info.image;
        
        if (img.info.format.isColor()) {
            return new VKImageMemoryBarrier()
                    .withDstAccessMask(VKAccess.COLOR_ATTACHMENT_WRITE)                    
                    .withNewLayout(VKImageLayout.COLOR_ATTACHMENT_OPTIMAL)
                    .withSubresourceRange(imgview.info.subresourceRange)
                    .withImage(img);
        } else if (img.info.format.isDepthStencil()) {
            return new VKImageMemoryBarrier()
                    .withDstAccessMask(VKAccess.DEPTH_STENCIL_ATTACHMENT_WRITE)
                    .withNewLayout(VKImageLayout.DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
                    .withSubresourceRange(imgview.info.subresourceRange)
                    .withImage(img);
        } else {
            throw new IllegalArgumentException("Write Staging Barrier requires either Color or DepthStencil image!");
        }
    }
    
    public static VKImageMemoryBarrier inferReadStagingBarrier(final VKImageView imgview) {
        final VKImage img = imgview.info.image;
        
        if (img.info.format.isColor()) {
            return new VKImageMemoryBarrier()
                    .withSrcAccessMask(VKAccess.COLOR_ATTACHMENT_WRITE)
                    .withDstAccessMask(VKAccess.COLOR_ATTACHMENT_READ)
                    .withOldLayout(VKImageLayout.COLOR_ATTACHMENT_OPTIMAL)
                    .withNewLayout(VKImageLayout.GENERAL) //?
                    .withImage(img)
                    .withSubresourceRange(imgview.info.subresourceRange);
        } else if (img.info.format.isDepthStencil()) {
            return new VKImageMemoryBarrier()
                    .withSrcAccessMask(VKAccess.DEPTH_STENCIL_ATTACHMENT_WRITE)
                    .withDstAccessMask(VKAccess.DEPTH_STENCIL_ATTACHMENT_READ)
                    .withOldLayout(VKImageLayout.DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
                    .withNewLayout(VKImageLayout.GENERAL)
                    .withImage(img)
                    .withSubresourceRange(imgview.info.subresourceRange);
        } else {
            throw new IllegalArgumentException("Read Staging Barrier requires either Color or DepthStencil image!");
        }
    }

    public final Set<VKAccess> srcAccessMask;
    public final Set<VKAccess> dstAccessMask;
    public final VKImageLayout oldLayout;
    public final VKImageLayout newLayout;
    public final VKQueueFamily srcQueueFamily;
    public final VKQueueFamily dstQueueFamily;
    public final VKImage image;
    public final VKImageSubresourceRange subresourceRange;

    public VKImageMemoryBarrier(
            final Collection<VKAccess> srcAccessMask,
            final Collection<VKAccess> dstAccessMask,
            final VKImageLayout oldLayout,
            final VKImageLayout newLayout,
            final VKQueueFamily srcQueueFamily,
            final VKQueueFamily dstQueueFamily,
            final VKImage image,
            final VKImageSubresourceRange subresourceRange) {

        this.srcAccessMask = Tools.copySet(srcAccessMask);
        this.dstAccessMask = Tools.copySet(dstAccessMask);
        this.oldLayout = oldLayout;
        this.newLayout = newLayout;
        this.srcQueueFamily = srcQueueFamily;
        this.dstQueueFamily = dstQueueFamily;
        this.image = image;
        this.subresourceRange = subresourceRange;
    }

    public VKImageMemoryBarrier() {
        this(null, null, VKImageLayout.UNDEFINED, VKImageLayout.GENERAL, null, null, null, null);
    }
    
    public VKImageMemoryBarrier withSrcAccessMask(final Collection<VKAccess> srcAccessMask) {
        return new VKImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }
    
    public VKImageMemoryBarrier withSrcAccessMask(final VKAccess... srcAccessMask) {
        return withSrcAccessMask(Arrays.asList(srcAccessMask));
    }
    
    public VKImageMemoryBarrier withDstAccessMask(final Collection<VKAccess> dstAccessMask) {
        return new VKImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }
    
    public VKImageMemoryBarrier withDstAccessMask(final VKAccess... dstAccessMask) {
        return withDstAccessMask(Arrays.asList(dstAccessMask));
    }
    
    public VKImageMemoryBarrier withOldLayout(final VKImageLayout oldLayout) {
        return new VKImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }
    
    public VKImageMemoryBarrier withNewLayout(final VKImageLayout newLayout) {
        return new VKImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }
    
    public VKImageMemoryBarrier withSrcQueueFamily(final VKQueueFamily srcQueueFamily) {
        return new VKImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }
    
    public VKImageMemoryBarrier withDstQueueFamily(final VKQueueFamily dstQueueFamily) {
        return new VKImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }
    
    public VKImageMemoryBarrier withImage(final VKImage image) {
        return new VKImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }
    
    public VKImageMemoryBarrier withSubresourceRange(final VKImageSubresourceRange subresourceRange) {
        return new VKImageMemoryBarrier(srcAccessMask, dstAccessMask, oldLayout, newLayout, srcQueueFamily, dstQueueFamily, image, subresourceRange);
    }
}
