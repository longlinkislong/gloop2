/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 * @param <UserDataT>
 */
public class VKFrameLocal<UserDataT> {    
    private final VKContext ctx;
    private final Object[] userdata;
    
    public VKFrameLocal(final VKContext ctx) {
        this.ctx = ctx;
        this.userdata = new Object[8];
    }
    
    protected UserDataT initialValue(final VKContext ctx) {
        return null;
    }
    
    public void set(final UserDataT userData) {
        this.userdata[this.ctx.frameDataIndex] = userData;
    }
    
    public UserDataT get() {
        Object frameData = this.userdata[this.ctx.frameDataIndex];
        
        if (frameData == null) {
            frameData = this.userdata[this.ctx.frameDataIndex] = this.initialValue(this.ctx);
        }
        
        return (UserDataT) frameData;
    }
}
