/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKDependency implements CBitfield {
    BY_REGION(VK10.VK_DEPENDENCY_BY_REGION_BIT);

    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }

    private VKDependency(final int value) {
        this.value = value;
    }

    public static Set<VKDependency> flags(final int bits) {
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<VKDependency> flags) {
        return Tools.flagsToBitfield(flags);
    }
}
