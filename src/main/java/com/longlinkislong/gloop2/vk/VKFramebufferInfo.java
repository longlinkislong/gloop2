/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class VKFramebufferInfo {

    public final int flags;
    public final VKRenderPass renderPass;
    public final List<VKImageView> attachments;
    public final int width;
    public final int height;
    public final int layers;

    public VKFramebufferInfo(
            final int flags,
            final VKRenderPass renderPass,
            final List<VKImageView> attachments,
            final int width, final int height, final int layers) {

        this.flags = flags;
        this.renderPass = renderPass;
        this.attachments = Tools.copyList(attachments);
        this.width = width;
        this.height = height;
        this.layers = layers;
    }
    
    public VKFramebufferInfo() {
        this(0, null, null, 0, 0, 0);
    }
    
    public VKFramebufferInfo withFlags(final int flags) {
        return new VKFramebufferInfo(flags, renderPass, attachments, width, height, layers);
    }
    
    public VKFramebufferInfo withRenderPass(final VKRenderPass renderPass) {
        return new VKFramebufferInfo(flags, renderPass, attachments, width, height, layers);
    }
    
    public VKFramebufferInfo withAttachments(final List<VKImageView> attachments) {
        return new VKFramebufferInfo(flags, renderPass, attachments, width, height, layers);
    }
    
    public VKFramebufferInfo withAttachments(final VKImageView... attachments) {
        return withAttachments(Arrays.asList(attachments));
    }
    
    public VKFramebufferInfo withWidth(final int width) {
        return new VKFramebufferInfo(flags, renderPass, attachments, width, height, layers);
    }
    
    public VKFramebufferInfo withHeight(final int height) {
        return new VKFramebufferInfo(flags, renderPass, attachments, width, height, layers);
    }
    
    public VKFramebufferInfo withLayers(final int layers) {
        return new VKFramebufferInfo(flags, renderPass, attachments, width, height, layers);
    }
    
    public VKFramebuffer create(final VKContext ctx) {
        return new VKFramebuffer(ctx, this);
    }
}
