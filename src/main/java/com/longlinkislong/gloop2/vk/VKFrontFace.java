/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKFrontFace implements CEnum {
    COUNTER_CLOCKWISE(VK10.VK_FRONT_FACE_COUNTER_CLOCKWISE),
    CLOCKWISE(VK10.VK_FRONT_FACE_CLOCKWISE);

    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }

    private VKFrontFace(final int value) {
        this.value = value;
    }

    public static Optional<VKFrontFace> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
