/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;
import org.lwjgl.vulkan.VK10;

/**
 *
 * @author zmichaels
 */
public enum VKShaderStage implements CBitfield {
    VERTEX(VK10.VK_SHADER_STAGE_VERTEX_BIT),    
    TESS_EVALUATION(VK10.VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT),
    TESS_CONTROL(VK10.VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT),
    GEOMETRY(VK10.VK_SHADER_STAGE_GEOMETRY_BIT),
    FRAGMENT(VK10.VK_SHADER_STAGE_FRAGMENT_BIT),
    COMPUTE(VK10.VK_SHADER_STAGE_COMPUTE_BIT);
    
    public final int value;
    
    @Override
    public int getValue() {
        return this.value;
    }        
    
    public static final Set<VKShaderStage> ALL_GRAPHICS = EnumSet.of(VERTEX, TESS_CONTROL, TESS_EVALUATION, GEOMETRY, FRAGMENT);
    public static final Set<VKShaderStage> ALL = EnumSet.allOf(VKShaderStage.class);
    
    private VKShaderStage(final int value) {
        this.value = value;
    }
    
    public static Set<VKShaderStage> flags(final int bits) {
        return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
    }
    
    public static int bits(final Set<VKShaderStage> flags) {
        return Tools.flagsToBitfield(flags);
    }    
}
