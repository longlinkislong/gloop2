/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import java.nio.IntBuffer;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkPipelineMultisampleStateCreateInfo;

/**
 *
 * @author zmichaels
 */
public final class VKPipelineMultisampleStateInfo {

    public final int flags;
    public final int rasterizationSamples;
    public final boolean sampleShadingEnable;
    public final float minSampleShading;
    public final IntBuffer pSampleMask;
    public final boolean alphaToCoverageEnable;
    public final boolean alphaToOneEnable;

    public VKPipelineMultisampleStateInfo(
            final int flags,
            final int rasterizationSamples,
            final boolean sampleShadingEnable,
            final float minSampleShading,
            final IntBuffer pSampleMask,
            final boolean alphaToCoverageEnable,
            final boolean alphaToOneEnable) {

        this.flags = flags;
        this.rasterizationSamples = rasterizationSamples;
        this.sampleShadingEnable = sampleShadingEnable;
        this.minSampleShading = minSampleShading;
        this.pSampleMask = pSampleMask;
        this.alphaToCoverageEnable = alphaToCoverageEnable;
        this.alphaToOneEnable = alphaToOneEnable;
    }

    public VKPipelineMultisampleStateInfo() {
        this(0, VK10.VK_SAMPLE_COUNT_1_BIT, false, 0F, null, false, false);
    }

    public VKPipelineMultisampleStateInfo withRasterizationSamples(final int rasterizationSamples) {
        return new VKPipelineMultisampleStateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public VKPipelineMultisampleStateInfo withSampleShadingEnable(final boolean sampleShadingEnable) {
        return new VKPipelineMultisampleStateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public VKPipelineMultisampleStateInfo withMinSampleShading(final float minSampleShading) {
        return new VKPipelineMultisampleStateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public VKPipelineMultisampleStateInfo withSampleMask(final IntBuffer pSampleMask) {
        return new VKPipelineMultisampleStateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public VKPipelineMultisampleStateInfo withAlphaToCoverageEnable(final boolean alphaToCoverageEnable) {
        return new VKPipelineMultisampleStateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }

    public VKPipelineMultisampleStateInfo withAlphaToOneEnable(final boolean alphaToOneEnable) {
        return new VKPipelineMultisampleStateInfo(flags, rasterizationSamples, sampleShadingEnable, minSampleShading, pSampleMask, alphaToCoverageEnable, alphaToOneEnable);
    }
    
    void vkStruct(VkPipelineMultisampleStateCreateInfo struct) {
        struct.sType(VK10.VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO)
                .flags(this.flags)
                .rasterizationSamples(this.rasterizationSamples)
                .sampleShadingEnable(this.sampleShadingEnable)
                .minSampleShading(this.minSampleShading)
                .pSampleMask(this.pSampleMask)
                .alphaToCoverageEnable(this.alphaToCoverageEnable)
                .alphaToOneEnable(this.alphaToOneEnable);
    }
}
