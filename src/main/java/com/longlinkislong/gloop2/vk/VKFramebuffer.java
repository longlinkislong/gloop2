/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import java.util.List;
import java.util.stream.Collectors;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkFramebufferCreateInfo;

/**
 *
 * @author zmichaels
 */
public class VKFramebuffer implements VKObject {

    VkDevice deviceHandle;
    long handle;
    VKFramebufferInfo info;

    public VKFramebuffer() {}
    
    public VKFramebuffer(final VKContext ctx, final VKFramebufferInfo info) {              
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final LongBuffer pAttachments = mem.callocLong(info.attachments.size());

            info.attachments.stream()
                    .mapToLong(attachment -> attachment.handle)
                    .forEach(pAttachments::put);

            pAttachments.flip();

            final LongBuffer pFramebuffer = mem.callocLong(1);
            final VKRenderPass dummyPass;

            if (info.renderPass == null) {
                final List<VKRenderPassInfo.Attachment> rpAttachments = info.attachments.stream()
                        .map(view -> VKRenderPassInfo.Attachment.of(view.info.image))
                        .collect(Collectors.toList());

                dummyPass = new VKRenderPassInfo()
                        .withAttachments(rpAttachments)
                        .withSubpasses(VKRenderPassInfo.Subpass.infer(rpAttachments))                        
                        .getCompatible(ctx);                
            } else {
                dummyPass = info.renderPass;
            }

            final VkFramebufferCreateInfo fbInfo = VkFramebufferCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO)
                    .flags(info.flags)
                    .renderPass(dummyPass.handle)
                    .pAttachments(pAttachments)
                    .width(info.width)
                    .height(info.height)
                    .layers(info.layers);

            final int err = VK10.vkCreateFramebuffer(ctx.device.handle, fbInfo, null, pFramebuffer);

            assert err == VK10.VK_SUCCESS : Tools.vkerr(err);

            this.handle = pFramebuffer.get(0);
        }
    }
    
    public void swap(final VKFramebuffer other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final long tmpHandle = other.handle;
        final VkDevice tmpDev = other.deviceHandle;
        final VKFramebufferInfo tmpInfo = other.info;
        
        other.handle = this.handle;
        other.deviceHandle = this.deviceHandle;
        other.info = this.info;
        
        this.handle = tmpHandle;
        this.deviceHandle = tmpDev;
        this.info = tmpInfo;
    }

    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyFramebuffer(this.deviceHandle, this.handle, null);
            this.handle = 0L;
            this.deviceHandle = null;
            this.info = null;
        }
    }
}
