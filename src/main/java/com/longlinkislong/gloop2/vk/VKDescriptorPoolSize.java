/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKDescriptorPoolSize {
    public final VKDescriptorType type;
    public final int descriptorCount;
    
    public VKDescriptorPoolSize(final VKDescriptorType type, final int descriptorCount) {
        this.type = type;
        this.descriptorCount = descriptorCount;
    }
    
    public VKDescriptorPoolSize() {
        this(VKDescriptorType.SAMPLER, 0);
    }
    
    public VKDescriptorPoolSize withType(final VKDescriptorType type) {
        return new VKDescriptorPoolSize(type, descriptorCount);
    }
    
    public VKDescriptorPoolSize withDescriptorCount(final int descriptorCount) {
        return new VKDescriptorPoolSize(type, descriptorCount);
    }
}
