/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.CBitfield;
import com.longlinkislong.gloop2.Tools;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public final class VKPipelineCacheInfo {
    public static enum CreateFlag implements CBitfield {

        ;

        public final int value;
            
        private CreateFlag(final int value) {
            this.value = value;
        }
            
        @Override
        public int getValue() {
            return this.value;
        }
        
        public static Set<CreateFlag> flags(final int bits) {
            return Tools.bitfieldToFlags(bits, Arrays.stream(values()));
        }
        
        public static int bits(final Set<CreateFlag> flags) {
            return Tools.flagsToBitfield(flags);
        }
        
    }
    
    public final Set<CreateFlag> flags;
    public final ByteBuffer initialData;
    
    public VKPipelineCacheInfo(final Collection<CreateFlag> flags, final ByteBuffer initialData) {
        this.flags = Tools.copySet(flags);
        this.initialData = initialData;
    }
    
    public VKPipelineCacheInfo() {
        this(null, null);
    }
    
    public VKPipelineCacheInfo withFlags(final Collection<CreateFlag> flags) {
        return new VKPipelineCacheInfo(flags, initialData);
    }
    
    public VKPipelineCacheInfo withFlags(final CreateFlag... flags) {
        return withFlags(Arrays.asList(flags));
    }
    
    public VKPipelineCacheInfo withInitialData(final ByteBuffer initialData) {
        return new VKPipelineCacheInfo(flags, initialData);
    }
    
    public VKPipelineCache create(final VKContext ctx) {
        return new VKPipelineCache(ctx, this);
    }
}
