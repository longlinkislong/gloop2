/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

import com.longlinkislong.gloop2.Tools;
import java.nio.LongBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VK10;
import org.lwjgl.vulkan.VkDescriptorSetLayoutBinding;
import org.lwjgl.vulkan.VkDescriptorSetLayoutCreateInfo;
import org.lwjgl.vulkan.VkDevice;

/**
 *
 * @author zmichaels
 */
public class VKDescriptorSetLayout implements VKObject {

    long handle;
    VkDevice deviceHandle;
    VKDescriptorSetLayoutInfo info;

    public VKDescriptorSetLayout() {}
    
    public VKDescriptorSetLayout(final VKContext ctx, final VKDescriptorSetLayoutInfo info) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final VkDescriptorSetLayoutBinding.Buffer pBindings = VkDescriptorSetLayoutBinding.callocStack(info.bindings.size(), mem);

            for (int i = 0; i < info.bindings.size(); i++) {
                final VKDescriptorSetLayoutInfo.Binding binding = info.bindings.get(i);
                final LongBuffer pImmutableSamplers;

                if (binding.immutableSamplers.isEmpty()) {
                    pImmutableSamplers = null;
                } else {
                    pImmutableSamplers = mem.callocLong(binding.immutableSamplers.size());

                    binding.immutableSamplers.stream()
                            .mapToLong(sampler -> sampler.handle)
                            .forEach(pImmutableSamplers::put);

                    pImmutableSamplers.flip();
                }

                pBindings.get(i)
                        .binding(binding.binding)
                        .descriptorType(binding.descriptorType.value)
                        .descriptorCount(binding.descriptorCount)
                        .stageFlags(VKShaderStage.bits(binding.stageFlags))
                        .pImmutableSamplers(pImmutableSamplers);
            }

            final VkDescriptorSetLayoutCreateInfo createInfo = VkDescriptorSetLayoutCreateInfo.callocStack(mem)
                    .sType(VK10.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO)
                    .flags(info.flags)
                    .pBindings(pBindings);

            final LongBuffer pHandle = mem.callocLong(1);
            final int err = VK10.vkCreateDescriptorSetLayout(ctx.device.handle, createInfo, null, pHandle);

            assert Tools.vksuccess(err) : Tools.vkerr(err);

            this.handle = pHandle.get(0);
            this.deviceHandle = ctx.device.handle;
            this.info = info;
        }
    }

    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }

    @Override
    public void close() {
        if (this.isValid()) {
            VK10.vkDestroyDescriptorSetLayout(deviceHandle, handle, null);
            this.handle = 0L;
            this.deviceHandle = null;
            this.info = null;
        }
    }

    public VKDescriptorSetLayoutInfo getInfo() {
        return this.info;
    }

    public void swap(final VKDescriptorSetLayout other) {
        if (other == null) {
            this.close();
            return;
        }

        final long tmpHandle = other.handle;
        final VkDevice tmpDev = other.deviceHandle;
        final VKDescriptorSetLayoutInfo tmpInfo = other.info;

        other.handle = this.handle;
        other.deviceHandle = this.deviceHandle;
        other.info = this.info;

        this.handle = tmpHandle;
        this.deviceHandle = tmpDev;
        this.info = tmpInfo;
    }
}
