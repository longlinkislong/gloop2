/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.vk;

/**
 *
 * @author zmichaels
 */
public class VKBufferImageCopy {

    public final long bufferOffset;
    public final int bufferRowLength;
    public final int bufferImageHeight;
    public final VKImageSubresourceLayers imageSubresource;
    public final int imageOffsetX;
    public final int imageOffsetY;
    public final int imageOffsetZ;

    public final int imageWidth;
    public final int imageHeight;
    public final int imageDepth;

    public VKBufferImageCopy(
            final long bufferOffset,
            final int bufferRowLength, final int bufferImageHeight,
            final VKImageSubresourceLayers imageSubresources,
            final int imageOffsetX, final int imageOffsetY, final int imageOffsetZ,
            final int imageWidth, final int imageHeight, final int imageDepth) {

        this.bufferOffset = bufferOffset;
        this.bufferRowLength = bufferRowLength;
        this.bufferImageHeight = bufferImageHeight;
        this.imageSubresource = imageSubresources;
        this.imageOffsetX = imageOffsetX;
        this.imageOffsetY = imageOffsetY;
        this.imageOffsetZ = imageOffsetZ;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.imageDepth = imageDepth;
    }
    
    public VKBufferImageCopy() {
        this(0, 0, 0, null, 0, 0, 0, 0, 0, 0);
    }
    
    public VKBufferImageCopy withBufferOffset(final long bufferOffset) {
        return new VKBufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffsetX, imageOffsetY, imageOffsetZ, imageWidth, imageHeight, imageDepth);
    }
    
    public VKBufferImageCopy withBufferRowLength(final int bufferRowLength) {
        return new VKBufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffsetX, imageOffsetY, imageOffsetZ, imageWidth, imageHeight, imageDepth);
    }
    
    public VKBufferImageCopy withBufferImageHeight(final int bufferImageHeight) {
        return new VKBufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffsetX, imageOffsetY, imageOffsetZ, imageWidth, imageHeight, imageDepth);
    }
    
    public VKBufferImageCopy withImageSubresource(final VKImageSubresourceLayers imageSubresource) {
        return new VKBufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffsetX, imageOffsetY, imageOffsetZ, imageWidth, imageHeight, imageDepth);
    }
    
    public VKBufferImageCopy withImageOffset(final int imageOffsetX, final int imageOffsetY, final int imageOffsetZ) {
        return new VKBufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffsetX, imageOffsetY, imageOffsetZ, imageWidth, imageHeight, imageDepth);
    }
    
    public VKBufferImageCopy withImageExtent(final int imageWidth, final int imageHeight, final int imageDepth) {
        return new VKBufferImageCopy(bufferOffset, bufferRowLength, bufferImageHeight, imageSubresource, imageOffsetX, imageOffsetY, imageOffsetZ, imageWidth, imageHeight, imageDepth);
    }
}
