/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public enum GLESFormat {
    ALPHA(GLES20.GL_ALPHA),
    RGBA(GLES20.GL_RGBA),
    RGB(GLES20.GL_RGB),
    RG(GLES30.GL_RG),
    RED(GLES30.GL_RED),
    RGBA_INTEGER(GLES30.GL_RGBA_INTEGER),
    RGB_INTEGER(GLES30.GL_RGB_INTEGER),
    RG_INTEGER(GLES30.GL_RG_INTEGER),
    RED_INTEGER(GLES30.GL_RED_INTEGER),
    DEPTH_COMPONENT(GLES20.GL_DEPTH_COMPONENT),
    DEPTH_STENCIL(GLES30.GL_DEPTH_STENCIL);
    
    public final int value;
    
    private GLESFormat(final int value) {
        this.value = value;
    }
    
    public static Optional<GLESFormat> of(final int value) {
        return Arrays.stream(values())
                .filter(e -> e.value == value)
                .findFirst();
    }
}
