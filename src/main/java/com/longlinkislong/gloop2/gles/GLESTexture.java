/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.EXTTextureFilterAnisotropic;
import org.lwjgl.opengles.GLES;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;
import org.lwjgl.opengles.GLESCapabilities;

/**
 *
 * @author zmichaels
 */
public class GLESTexture extends GLESObject<GLESTextureInfo, GLESTexture> implements GLESAttachment {
    public GLESTexture() {}
    
    public GLESTexture(final GLESTextureInfo info) {
        assert info.samplerInfo.magFilter.magSupported : "Unsupported mag filter!";

        this.info = info;
        this.handle = GLES20.glGenTextures();

        final GLESTextureTarget tgt = info.target();

        GLES20.glBindTexture(tgt.value, handle);

        GLES20.glTexParameteri(tgt.value, GLES20.GL_TEXTURE_MIN_FILTER, info.samplerInfo.minFilter.value);
        GLES20.glTexParameteri(tgt.value, GLES20.GL_TEXTURE_MAG_FILTER, info.samplerInfo.magFilter.value);

        GLES20.glTexParameteri(tgt.value, GLES20.GL_TEXTURE_WRAP_S, info.samplerInfo.wrapS.value);
        GLES20.glTexParameteri(tgt.value, GLES20.GL_TEXTURE_WRAP_T, info.samplerInfo.wrapT.value);

        final GLESCapabilities caps = GLES.getCapabilities();

        int baseLevel = 0;
        int maxLevel = Math.max(info.initialData.size(), info.levelCount);

        if (caps.GLES30) {
            if (tgt == GLESTextureTarget.TEXTURE_3D) {
                GLES20.glTexParameteri(tgt.value, GLES30.GL_TEXTURE_WRAP_R, info.samplerInfo.wrapR.value);
            }

            GLES20.glTexParameterf(tgt.value, GLES30.GL_TEXTURE_MIN_LOD, info.samplerInfo.minLOD);
            GLES20.glTexParameterf(tgt.value, GLES30.GL_TEXTURE_MAX_LOD, info.samplerInfo.maxLOD);
            
            GLES20.glTexParameteri(tgt.value, GLES30.GL_TEXTURE_MAX_LEVEL, maxLevel);
            GLES20.glTexParameteri(tgt.value, GLES30.GL_TEXTURE_BASE_LEVEL, baseLevel);

            if (info.samplerInfo.swizzle.r != GLESSamplerInfo.Swizzle.Mode.IDENTITY) {
                GLES20.glTexParameteri(tgt.value, GLES30.GL_TEXTURE_SWIZZLE_R, info.samplerInfo.swizzle.r.value);
            }

            if (info.samplerInfo.swizzle.g != GLESSamplerInfo.Swizzle.Mode.IDENTITY) {
                GLES20.glTexParameteri(tgt.value, GLES30.GL_TEXTURE_SWIZZLE_G, info.samplerInfo.swizzle.g.value);
            }

            if (info.samplerInfo.swizzle.b != GLESSamplerInfo.Swizzle.Mode.IDENTITY) {
                GLES20.glTexParameteri(tgt.value, GLES30.GL_TEXTURE_SWIZZLE_B, info.samplerInfo.swizzle.b.value);
            }

            if (info.samplerInfo.swizzle.a != GLESSamplerInfo.Swizzle.Mode.IDENTITY) {
                GLES20.glTexParameteri(tgt.value, GLES30.GL_TEXTURE_SWIZZLE_A, info.samplerInfo.swizzle.a.value);
            }
        }

        if (caps.GL_EXT_texture_filter_anisotropic) {
            GLES20.glTexParameterf(tgt.value, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, info.samplerInfo.maxAnisotropy);
        }

        GLES20.glBindTexture(tgt.value, 0);

        switch (tgt) {
            case TEXTURE_2D:
                init2D(baseLevel, maxLevel);
                break;
            case TEXTURE_3D:
                init3D(baseLevel, maxLevel);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported target type: " + tgt);
        }
    }

    private void init3D(final int baseLevel, final int maxLevel) {
        GLES20.glBindTexture(GLES30.GL_TEXTURE_3D, handle);

        if (info.initialData.isEmpty()) {
            int w = info.width;
            int h = info.height;
            int d = info.depth;

            for (int i = baseLevel; i <= maxLevel; i++) {
                GLES30.glTexImage3D(GLES30.GL_TEXTURE_3D, i, info.format.value, w, h, d, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, 0L);
                w = Math.max(1, w >> 1);
                h = Math.max(1, h >> 1);
                d = Math.max(1, h >> 1);
            }
        } else {
            int w = info.width;
            int h = info.height;
            int d = info.depth;

            for (int i = baseLevel; i <= maxLevel; i++) {
                final GLESPixelInfo px = info.initialData.get(i);

                GLES30.glTexImage3D(GLES30.GL_TEXTURE_3D, i, info.format.value, w, h, d, 0, px.format.value, px.type.value, px.data);
                w = Math.max(1, w >> 1);
                h = Math.max(1, h >> 1);
                d = Math.max(1, d >> 1);
            }
        }

        GLES20.glBindTexture(GLES30.GL_TEXTURE_3D, 0);
    }

    private void init2D(final int baseLevel, final int maxLevel) {
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, handle);

        if (info.initialData.isEmpty()) {
            int w = info.width;
            int h = info.height;

            for (int i = baseLevel; i <= maxLevel; i++) {
                GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, i, info.format.value, w, h, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, 0L);
                w = Math.max(1, w >> 1);
                h = Math.max(1, h >> 1);
            }
        } else {
            int w = info.width;
            int h = info.height;

            for (int i = baseLevel; i <= maxLevel; i++) {
                final GLESPixelInfo px = info.initialData.get(i);

                GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, i, info.format.value, w, h, 0, px.format.value, px.type.value, px.data);
                w = Math.max(1, w >> 1);
                h = Math.max(1, h >> 1);
            }
        }

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }

    public void generateMipmap() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESTexture is not valid!");
        }

        final GLESTextureTarget tgt = info.target();

        GLES20.glBindTexture(tgt.value, handle);
        GLES20.glGenerateMipmap(tgt.value);
        GLES20.glBindTexture(tgt.value, 0);
    }

    public void bind(int unit) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESTexture is not valid!");
        }
        
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + unit);
        GLES20.glBindTexture(info.target().value, handle);
    }        

    public void subImage(final int level, final int x, final int y, final int z, final int w, final int h, final int d, final GLESPixelInfo px) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESTexture is not valid!");
        }

        final GLESTextureTarget tgt = info.target();

        assert (tgt == GLESTextureTarget.TEXTURE_2D && d == 1) || tgt == GLESTextureTarget.TEXTURE_3D;

        switch (tgt) {
            case TEXTURE_2D:
                GLES20.glBindTexture(tgt.value, handle);
                GLES20.glTexSubImage2D(tgt.value, level, x, y, w, h, px.format.value, px.type.value, px.data);
                GLES20.glBindTexture(tgt.value, 0);
                break;
            case TEXTURE_3D:
                GLES20.glBindTexture(tgt.value, handle);
                GLES30.glTexSubImage3D(tgt.value, level, x, y, z, w, h, d, px.format.value, px.type.value, px.data);
                GLES20.glBindTexture(tgt.value, 0);
                break;
        }
    }

    @Override
    public void close() {
        if (this.isValid()) {
            GLES20.glDeleteTextures(this.handle);
            this.handle = 0;
            this.info = null;
        }
    }       
    
    public static final GLESTexture DEFAULT_TEXTURE2D = new GLESTexture() {
        @Override
        public void bind(int unit) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + unit);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        }
    };
    
    public static final GLESTexture DEFAULT_TEXTURE3D = new GLESTexture() {
        @Override
        public void bind(int unit) {
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + unit);
            GLES20.glBindTexture(GLES30.GL_TEXTURE_3D, 0);
        }
    };
}
