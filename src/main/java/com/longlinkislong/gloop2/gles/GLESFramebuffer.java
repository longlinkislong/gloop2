/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESFramebuffer extends GLESObject<GLESFramebufferInfo, GLESFramebuffer> {
    public GLESFramebuffer() {
        
    }
    
    public GLESFramebuffer(final GLESFramebufferInfo info) {
        this.info = info;
        this.handle = GLES20.glGenFramebuffers();
        
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, handle);
        
        int colorAttachId = GLES20.GL_COLOR_ATTACHMENT0;
        for (GLESAttachment attachment : info.attachments) {
            if (attachment instanceof GLESRenderbuffer) {
                final GLESRenderbuffer rba = (GLESRenderbuffer) attachment;
                
                final int attachId;
                
                if (rba.info.format.isColor()) {
                    attachId = colorAttachId++;
                } else if (rba.info.format.isDepthStencil()) {
                    attachId = GLES30.GL_DEPTH_STENCIL_ATTACHMENT;
                } else if (rba.info.format.isDepth()) {
                    attachId = GLES20.GL_DEPTH_ATTACHMENT;
                } else if (rba.info.format.isStencil()) {
                    attachId = GLES20.GL_STENCIL_ATTACHMENT;
                } else {
                    throw new UnsupportedOperationException("Unsupported attachment: " + rba.info.format);
                }
                
                GLES20.glFramebufferRenderbuffer(GLES20.GL_FRAMEBUFFER, attachId, GLES20.GL_RENDERBUFFER, rba.handle);
            } else if (attachment instanceof GLESTexture) {
                final GLESTexture ta = (GLESTexture) attachment;
                
                assert ta.info.target() == GLESTextureTarget.TEXTURE_2D;
                
                final int attachId;
                
                if (ta.info.format.isColor()) {
                    attachId = colorAttachId++;
                } else if (ta.info.format.isDepthStencil()) {
                    attachId = GLES30.GL_DEPTH_STENCIL_ATTACHMENT;
                } else if (ta.info.format.isDepth()) {
                    attachId = GLES20.GL_DEPTH_ATTACHMENT;
                } else if (ta.info.format.isStencil()) {
                    attachId = GLES20.GL_STENCIL_ATTACHMENT;
                } else {
                    throw new UnsupportedOperationException("Unsupported attachment: " + ta.info.format);
                }
                
                GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, attachId, GLES20.GL_TEXTURE_2D, ta.handle, 0);
            }                        
        }
        
        if (GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER) != GLES20.GL_FRAMEBUFFER_COMPLETE) {
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
            GLES20.glDeleteFramebuffers(this.handle);
            
            this.handle = 0;
            this.info = null;
            
            throw new GLESIncompleteFramebufferException();
        }
    }
    
    public void bind() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESFramebuffer is not valid!");
        }
        
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, this.handle);
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GLES20.glDeleteFramebuffers(this.handle);
            this.handle = 0;
            this.info = null;
        }
    }
    
    public static final GLESFramebuffer DEFAULT_FRAMEBUFFER = new GLESFramebuffer() {
        @Override
        public void bind() {
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        }
    };
}
