/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public class GLESDepthStencilStateInfo {

    public final boolean depthTestEnable;
    public final GLESCompareOp depthCompareOp;
    public final boolean depthWriteEnable;
    public final boolean stencilTestEnable;
    public final GLESStencilOpState front;
    public final GLESStencilOpState back;

    public GLESDepthStencilStateInfo(
            final boolean depthTestEnable, final boolean depthWriteEnable,
            final GLESCompareOp depthCompareOp,
            final boolean stencilTestEnable, final GLESStencilOpState front, final GLESStencilOpState back) {

        this.depthTestEnable = depthTestEnable;
        this.depthWriteEnable = depthWriteEnable;
        this.depthCompareOp = depthCompareOp;
        this.stencilTestEnable = stencilTestEnable;
        this.front = front;
        this.back = back;
    }

    public GLESDepthStencilStateInfo() {
        this(false, true, GLESCompareOp.LESS, false, new GLESStencilOpState(), new GLESStencilOpState());
    }

    public GLESDepthStencilStateInfo withDepthTestEnable(final boolean depthTestEnable) {
        return new GLESDepthStencilStateInfo(depthTestEnable, depthWriteEnable, depthCompareOp, stencilTestEnable, front, back);
    }

    public GLESDepthStencilStateInfo withDepthCompareOp(final GLESCompareOp depthCompareOp) {
        return new GLESDepthStencilStateInfo(depthTestEnable, depthWriteEnable, depthCompareOp, stencilTestEnable, front, back);
    }

    public GLESDepthStencilStateInfo withStencilTestEnable(final boolean stencilTestEnable) {
        return new GLESDepthStencilStateInfo(depthTestEnable, depthWriteEnable, depthCompareOp, stencilTestEnable, front, back);
    }

    public GLESDepthStencilStateInfo withFront(final GLESStencilOpState front) {
        return new GLESDepthStencilStateInfo(depthTestEnable, depthWriteEnable, depthCompareOp, stencilTestEnable, front, back);
    }

    public GLESDepthStencilStateInfo withBack(final GLESStencilOpState back) {
        return new GLESDepthStencilStateInfo(depthTestEnable, depthWriteEnable, depthCompareOp, stencilTestEnable, front, back);
    }

    public void apply() {
        if (this.depthTestEnable) {
            GLES20.glEnable(GLES20.GL_DEPTH_TEST);
            GLES20.glDepthFunc(this.depthCompareOp.value);
        } else {
            GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        }

        GLES20.glDepthMask(this.depthWriteEnable);
        
        if (this.stencilTestEnable) {
            GLES20.glEnable(GLES20.GL_STENCIL_TEST);
            
            GLES20.glStencilOpSeparate(GLES20.GL_FRONT, this.front.failOp.value, this.front.depthFailOp.value, this.front.passOp.value);
            GLES20.glStencilOpSeparate(GLES20.GL_BACK, this.back.failOp.value, this.back.depthFailOp.value, this.back.passOp.value);
            
            GLES20.glStencilMaskSeparate(GLES20.GL_FRONT, this.front.writeMask);
            GLES20.glStencilMaskSeparate(GLES20.GL_BACK, this.back.writeMask);
            
            GLES20.glStencilFuncSeparate(GLES20.GL_FRONT, this.front.compareOp.value, this.front.reference, this.front.compareMask);
            GLES20.glStencilFuncSeparate(GLES20.GL_BACK, this.back.compareOp.value, this.back.reference, this.back.compareMask);
        } else {
            GLES20.glDisable(GLES20.GL_STENCIL_TEST);
        }
    }
}
