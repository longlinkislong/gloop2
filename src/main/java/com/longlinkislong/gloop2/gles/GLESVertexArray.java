/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.ANGLEInstancedArrays;
import org.lwjgl.opengles.GLES;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;
import org.lwjgl.opengles.GLESCapabilities;
import org.lwjgl.opengles.OESVertexArrayObject;

/**
 *
 * @author zmichaels
 */
public class GLESVertexArray extends GLESObject<GLESVertexArrayInfo, GLESVertexArray> {

    public GLESVertexArray() {}
    
    public GLESVertexArray(final GLESVertexArrayInfo info) {
        this.info = info;

        final GLESCapabilities caps = GLES.getCapabilities();

        if (caps.GLES30) {
            this.handle = GLES30.glGenVertexArrays();
            
            GLES30.glBindVertexArray(this.handle);
            
            if (info.indexBuffer != null) {
                GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, info.indexBuffer.handle);
            }
            
            for (GLESVertexAttributeInfo attrib : info.vertexAttributes) {                                
                switch (attrib.format.type) {
                    case FLOAT:
                        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, attrib.buffer.handle);
                        GLES20.glVertexAttribPointer(attrib.location, attrib.format.size, attrib.format.value, false, attrib.stride, attrib.offset);
                        break;
                    case NORMALIZED:
                        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, attrib.buffer.handle);
                        GLES20.glVertexAttribPointer(attrib.location, attrib.format.size, attrib.format.value, true, attrib.stride, attrib.offset);
                        break;
                    case INTEGER:
                        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, attrib.buffer.handle);
                        GLES30.glVertexAttribIPointer(attrib.location, attrib.format.size, attrib.format.value, attrib.stride, attrib.offset);
                        break;
                    default:
                        throw new UnsupportedOperationException("Unsupported format type: " + attrib.format.type);
                }
                
                if (attrib.divisor > 0) {
                    GLES30.glVertexAttribDivisor(attrib.location, attrib.divisor);
                }
                
                GLES20.glEnableVertexAttribArray(attrib.location);
            }
            
            GLES30.glBindVertexArray(0);
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
            GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);            
        } else if (caps.GL_OES_vertex_array_object) {
            this.handle = OESVertexArrayObject.glGenVertexArraysOES();
            
            OESVertexArrayObject.glBindVertexArrayOES(this.handle);
            
            if (info.indexBuffer != null) {
                GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, info.indexBuffer.handle);
            }
            
            for (GLESVertexAttributeInfo attrib : info.vertexAttributes) {                                
                switch (attrib.format.type) {
                    case FLOAT:
                    case INTEGER:
                        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, attrib.buffer.handle);
                        GLES20.glVertexAttribPointer(attrib.location, attrib.format.size, attrib.format.value, false, attrib.stride, attrib.offset);
                        break;
                    case NORMALIZED:
                        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, attrib.buffer.handle);
                        GLES20.glVertexAttribPointer(attrib.location, attrib.format.size, attrib.format.value, true, attrib.stride, attrib.offset);
                        break;
                    default:
                        throw new UnsupportedOperationException("Unsupported format type: " + attrib.format.type);
                }
                
                if (attrib.divisor > 0) {
                    ANGLEInstancedArrays.glVertexAttribDivisorANGLE(attrib.location, attrib.divisor);
                }
                
                GLES20.glEnableVertexAttribArray(attrib.location);
            }                        
            
            OESVertexArrayObject.glBindVertexArrayOES(0);
            GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
            GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
        } else {
            throw new UnsupportedOperationException("GLESVertexArray requires either GLES30 or GL_OES_vertex_array_object!");
        }
    }

    public void bind() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESVertexArray is not valid!");
        }

        final GLESCapabilities caps = GLES.getCapabilities();

        if (caps.GLES30) {
            GLES30.glBindVertexArray(this.handle);
        } else if (caps.GL_OES_vertex_array_object) {
            OESVertexArrayObject.glBindVertexArrayOES(this.handle);
        } else {
            throw new UnsupportedOperationException("GLESVertexArray requires either GLES30 or GL_OES_vertex_array_object!");
        }
    }

    @Override
    public void close() {
        if (this.isValid()) {
            final GLESCapabilities caps = GLES.getCapabilities();

            if (caps.GLES30) {
                GLES30.glDeleteVertexArrays(this.handle);
            } else if (caps.GL_OES_vertex_array_object) {
                OESVertexArrayObject.glDeleteVertexArraysOES(this.handle);
            } else {
                throw new UnsupportedOperationException("GLESVertexArray requires either GLES30 or GL_OES_vertex_array_object!");
            }
        }
    }
    
    public static final GLESVertexArray DEFAULT_VERTEX_ARRAY = new GLESVertexArray() {
        @Override
        public void bind() {
            final GLESCapabilities caps = GLES.getCapabilities();

            if (caps.GLES30) {
                GLES30.glBindVertexArray(0);
            } else if (caps.GL_OES_vertex_array_object) {
                OESVertexArrayObject.glBindVertexArrayOES(0);
            } else {
                throw new UnsupportedOperationException("GLESVertexArray requires either GLES30 or GL_OES_vertex_array_object!");
            }
        }
    };
}
