/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public class GLESBlendStateInfo {

    public final boolean blendEnable;
    public final GLESBlendEquation colorBlendOp;
    public final GLESBlendEquation alphaBlendOp;
    public final GLESBlendFunction srcColorBlend;
    public final GLESBlendFunction dstColorBlend;
    public final GLESBlendFunction srcAlphaBlend;
    public final GLESBlendFunction dstAlphaBlend;
    public final int colorWriteMask;

    public GLESBlendStateInfo(
            final boolean blendEnable,
            final GLESBlendEquation colorBlendOp, final GLESBlendEquation alphaBlendOp,
            final GLESBlendFunction srcColorBlend, final GLESBlendFunction srcAlphaBlend,
            final GLESBlendFunction dstColorBlend, final GLESBlendFunction dstAlphaBlend,
            final int colorWriteMask) {

        this.blendEnable = blendEnable;
        this.colorBlendOp = colorBlendOp;
        this.alphaBlendOp = alphaBlendOp;
        this.srcColorBlend = srcColorBlend;
        this.dstColorBlend = dstColorBlend;
        this.srcAlphaBlend = srcAlphaBlend;
        this.dstAlphaBlend = dstAlphaBlend;
        this.colorWriteMask = colorWriteMask;
    }

    public GLESBlendStateInfo() {
        this(false,
                GLESBlendEquation.ADD, GLESBlendEquation.ADD,
                GLESBlendFunction.ONE, GLESBlendFunction.ONE,
                GLESBlendFunction.ZERO, GLESBlendFunction.ZERO, 0xFF);
    }
    
    public GLESBlendStateInfo withColorMask(final int colorWriteMask) {
        return new GLESBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }

    public GLESBlendStateInfo withBlendEnable(final boolean blendEnable) {
        return new GLESBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }
    
    public GLESBlendStateInfo withSrcColorBlend(final GLESBlendFunction srcColorBlend) {
        return new GLESBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }
    
    public GLESBlendStateInfo withDstColorBlend(final GLESBlendFunction dstColorBlend) {
        return new GLESBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }
    
    public GLESBlendStateInfo withSrcAlphaBlend(final GLESBlendFunction srcAlphaBlend) {
        return new GLESBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }
    
    public GLESBlendStateInfo withDstAlphaBlend(final GLESBlendFunction dstAlphaBlend) {
        return new GLESBlendStateInfo(blendEnable, colorBlendOp, alphaBlendOp, srcColorBlend, srcAlphaBlend, dstColorBlend, dstAlphaBlend, colorWriteMask);
    }

    public void apply() {
        if (this.blendEnable) {
            GLES20.glEnable(GLES20.GL_BLEND);
            GLES20.glBlendFuncSeparate(this.srcColorBlend.value, this.dstColorBlend.value, this.srcAlphaBlend.value, this.dstAlphaBlend.value);
            GLES20.glBlendEquationSeparate(this.colorBlendOp.value, this.alphaBlendOp.value);
                        
            GLES20.glColorMask((colorWriteMask & 0x1) != 0, (colorWriteMask & 0x2) != 0, (colorWriteMask & 0x4) != 0, (colorWriteMask & 0x8) != 0);
        } else {
            GLES20.glDisable(GLES20.GL_BLEND);
        }
    }
}
