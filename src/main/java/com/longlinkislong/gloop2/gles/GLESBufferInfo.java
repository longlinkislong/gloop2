/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.nio.ByteBuffer;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESBufferInfo {

    public static enum Usage {
        STATIC_DRAW(GLES20.GL_STATIC_DRAW),
        DYNAMIC_DRAW(GLES20.GL_DYNAMIC_DRAW),
        STREAM_DRAW(GLES20.GL_STREAM_DRAW),
        STATIC_READ(GLES30.GL_STATIC_READ),
        DYNAMIC_READ(GLES30.GL_DYNAMIC_READ),
        STREAM_READ(GLES30.GL_STREAM_READ),
        STATIC_COPY(GLES30.GL_STATIC_COPY),
        DYNAMIC_COPY(GLES30.GL_DYNAMIC_COPY),
        STREAM_COPY(GLES30.GL_STREAM_COPY);

        public final int value;

        private Usage(final int value) {
            this.value = value;
        }
    }

    public final long size;
    public final Usage usage;
    public final GLESBufferTarget target;
    public final ByteBuffer initialData;

    public GLESBufferInfo(
            final long size,
            final Usage usage,
            final GLESBufferTarget preferredTarget,
            final ByteBuffer initialData) {

        this.size = size;
        this.usage = usage;
        this.target = preferredTarget;
        this.initialData = initialData;
    }
    
    public GLESBufferInfo() {
        this(0L, Usage.STATIC_DRAW, GLESBufferTarget.ARRAY, null);
    }
    
    public GLESBufferInfo withSize(final long size) {
        return new GLESBufferInfo(size, usage, target, initialData);
    }
    
    public GLESBufferInfo withUsage(final Usage usage) {
        return new GLESBufferInfo(size, usage, target, initialData);
    }
    
    public GLESBufferInfo withPreferredTarget(final GLESBufferTarget preferredTarget) {
        return new GLESBufferInfo(size, usage, preferredTarget, initialData);
    }
    
    public GLESBufferInfo withInitialData(final ByteBuffer initialData) {
        return new GLESBufferInfo(size, usage, target, initialData);
    }
    
    public GLESBuffer create() {
        return new GLESBuffer(this);
    }
}
