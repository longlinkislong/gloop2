/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

/**
 *
 * @author zmichaels
 * @param <SwapT> type allowed for swap.
 * @param <InfoT> type allowed for info.
 */
public abstract class GLESObject<InfoT, SwapT extends GLESObject<InfoT, ?>> implements AutoCloseable {
    protected int handle;
    protected InfoT info;            
    
    public boolean isValid() {
        return this.handle != 0;
    }
    
    public InfoT getInfo() {
        return this.info;
    }        
    
    public void swap(SwapT other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final int tmpHandle = other.handle;
        final InfoT tmpInfo = other.info;
        
        other.handle = this.handle;
        other.info = this.info;
        
        this.handle = tmpHandle;
        this.info = tmpInfo;
    }
    
    @Override
    public void close() {
        
    }
}
