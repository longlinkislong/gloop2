/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public enum GLESFenceCondition {
    GPU_COMMANDS_COMPLETE(GLES30.GL_SYNC_GPU_COMMANDS_COMPLETE);
    
    public final int value;
    
    private GLESFenceCondition(final int value) {
        this.value = value;
    }
}
