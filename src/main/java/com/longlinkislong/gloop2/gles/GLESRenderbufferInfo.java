/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

/**
 *
 * @author zmichaels
 */
public class GLESRenderbufferInfo {
    public final GLESInternalFormat format;
    public final int width;
    public final int height;
    public final int samples;
    
    public GLESRenderbufferInfo(
            final GLESInternalFormat internalFormat,
            final int width, final int height,
            final int samples) {
        
        this.format = internalFormat;
        this.width = width;
        this.height = height;
        this.samples = samples;
    }
    
    public GLESRenderbufferInfo() {
        this(GLESInternalFormat.RGBA, 0, 0, 0);
    }
    
    public GLESRenderbufferInfo withFormat(final GLESInternalFormat format) {
        return new GLESRenderbufferInfo(format, width, height, samples);
    }
    
    public GLESRenderbufferInfo withExtent(final int width, final int height) {
        return new GLESRenderbufferInfo(format, width, height, samples);
    }
    
    public GLESRenderbufferInfo withSamples(final int samples) {
        return new GLESRenderbufferInfo(format, width, height, samples);
    }
    
    public GLESRenderbuffer create() {
        return new GLESRenderbuffer(this);
    }
}
