/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public enum GLESBlendEquation {
    ADD(GLES20.GL_FUNC_ADD),
    SUBTRACT(GLES20.GL_FUNC_SUBTRACT),
    REVERSE_SUBTRACT(GLES20.GL_FUNC_REVERSE_SUBTRACT),
    MIN(GLES30.GL_MIN),
    MAX(GLES30.GL_MAX);
    
    public final int value;
    
    private GLESBlendEquation(final int value) {
        this.value = value;
    }
    
    public static Optional<GLESBlendEquation> of(final int value) {
        return Arrays.stream(values())
                .filter(e -> e.value == value)
                .findFirst();
    }
}
