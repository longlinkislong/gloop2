/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public class GLESClearStateInfo {

    public final Set<GLESClearBuffer> buffers;
    public final float r;
    public final float g;
    public final float b;
    public final float a;

    public final float depth;
    public final int stencil;

    public GLESClearStateInfo(
            final Collection<GLESClearBuffer> buffers,
            final float r, final float g, final float b, final float a,
            final float depth, final int stencil) {

        this.buffers = Tools.copySet(buffers);
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
        this.depth = depth;
        this.stencil = stencil;
    }

    public GLESClearStateInfo() {
        this(null, 0F, 0F, 0F, 0F, 1F, 0);
    }

    public GLESClearStateInfo withColor(final float r, final float g, final float b, final float a) {
        return new GLESClearStateInfo(buffers, r, g, b, a, depth, stencil);
    }

    public GLESClearStateInfo withBuffers(final Collection<GLESClearBuffer> buffers) {
        return new GLESClearStateInfo(buffers, r, g, b, a, depth, stencil);
    }

    public GLESClearStateInfo withBuffers(final GLESClearBuffer... buffers) {
        return withBuffers(Arrays.asList(buffers));
    }

    public GLESClearStateInfo withDepth(final float depth) {
        return new GLESClearStateInfo(buffers, r, g, b, a, depth, stencil);
    }

    public GLESClearStateInfo withStencil(final int stencil) {
        return new GLESClearStateInfo(buffers, r, g, b, a, depth, stencil);
    }

    public void apply() {
        if (this.buffers.contains(GLESClearBuffer.COLOR)) {
            GLES20.glClearColor(r, g, b, a);
        }

        if (this.buffers.contains(GLESClearBuffer.DEPTH)) {
            GLES20.glClearDepthf(depth);
        }

        if (this.buffers.contains(GLESClearBuffer.STENCIL)) {
            GLES20.glClearStencil(stencil);
        }

        GLES20.glClear(GLESClearBuffer.bits(buffers));
    }
}
