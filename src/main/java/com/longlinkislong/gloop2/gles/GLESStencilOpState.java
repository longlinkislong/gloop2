/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

/**
 *
 * @author zmichaels
 */
public class GLESStencilOpState {
    public final GLESStencilOp failOp;
    public final GLESStencilOp passOp;
    public final GLESStencilOp depthFailOp;
    public final GLESCompareOp compareOp;
    public final int reference;
    public final int compareMask;
    public final int writeMask;
    
    public GLESStencilOpState(
            final GLESStencilOp failOp, final GLESStencilOp passOp, final GLESStencilOp depthFailOp,
            final GLESCompareOp compareOp, final int reference, final int compareMask,
            final int writeMask) {
        
        this.failOp = failOp;
        this.passOp = passOp;
        this.depthFailOp = depthFailOp;
        this.compareOp = compareOp;
        this.reference = reference;
        this.compareMask = compareMask;
        this.writeMask = writeMask;
    }
    
    public GLESStencilOpState() {
        this(GLESStencilOp.KEEP, GLESStencilOp.KEEP, GLESStencilOp.KEEP, GLESCompareOp.ALWAYS, 0, 0xFFFFFFFF, 0xFFFFFFFF);
    }
    
    public GLESStencilOpState withFailOp(final GLESStencilOp failOp) {
        return new GLESStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
    
    public GLESStencilOpState withPassOp(final GLESStencilOp passOp) {
        return new GLESStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
    
    public GLESStencilOpState withDepthFailOp(final GLESStencilOp depthFailOp) {
        return new GLESStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
    
    public GLESStencilOpState withCompareOp(final GLESCompareOp compareOp) {
        return new GLESStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
    
    public GLESStencilOpState withReference(final int reference) {
        return new GLESStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
    
    public GLESStencilOpState withCompareMask(final int compareMask) {
        return new GLESStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }        
    
    public GLESStencilOpState withWriteMask(final int writeMask) {
        return new GLESStencilOpState(failOp, passOp, depthFailOp, compareOp, reference, compareMask, writeMask);
    }
}
