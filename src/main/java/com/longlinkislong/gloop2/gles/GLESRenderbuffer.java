/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESRenderbuffer extends GLESObject<GLESRenderbufferInfo, GLESRenderbuffer> implements GLESAttachment {
    public GLESRenderbuffer() {}
    
    public GLESRenderbuffer(final GLESRenderbufferInfo info) {
        this.info = info;
        this.handle = GLES20.glGenRenderbuffers();
        
        GLES20.glBindRenderbuffer(GLES20.GL_RENDERER, handle);
        
        if (info.samples > 1) {            
            GLES30.glRenderbufferStorageMultisample(GLES20.GL_RENDERBUFFER, info.samples, info.format.value, info.width, info.height);            
        } else {
            GLES20.glRenderbufferStorage(GLES20.GL_RENDERBUFFER, info.format.value, info.width, info.height);
        }
        
        GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, 0);
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GLES20.glDeleteRenderbuffers(this.handle);
            this.handle = 0;
            this.info = null;
        }
    }
}
