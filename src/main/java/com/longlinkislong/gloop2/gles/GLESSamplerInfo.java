/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESSamplerInfo {

    public static final class Swizzle {

        public static enum Mode {
            // pseudo-state
            IDENTITY(-1),
            ONE(GLES20.GL_ONE),
            ZERO(GLES20.GL_ZERO),
            RED(GLES30.GL_RED),
            GREEN(GLES30.GL_GREEN),
            BLUE(GLES30.GL_BLUE),
            ALPHA(GLES20.GL_ALPHA);

            public final int value;

            private Mode(final int value) {
                this.value = value;
            }
        }

        public final Mode r;
        public final Mode g;
        public final Mode b;
        public final Mode a;

        public Swizzle(final Mode r, final Mode g, final Mode b, final Mode a) {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public Swizzle() {
            this(Mode.IDENTITY, Mode.IDENTITY, Mode.IDENTITY, Mode.IDENTITY);
        }
    }

    public static enum AddressMode {
        REPEAT(GLES20.GL_REPEAT),
        MIRRORED_REPEAT(GLES20.GL_MIRRORED_REPEAT),
        CLAMP_TO_EDGE(GLES20.GL_CLAMP_TO_EDGE);

        public final int value;

        private AddressMode(final int value) {
            this.value = value;
        }
    }

    public final GLESFilter minFilter;
    public final GLESFilter magFilter;
    public final float minLOD;
    public final float maxLOD;

    public final Swizzle swizzle;
    public final AddressMode wrapS;
    public final AddressMode wrapT;
    public final AddressMode wrapR;
    public final float maxAnisotropy;

    //TODO: support texture compare func
    public GLESSamplerInfo(
            final GLESFilter minFilter, final GLESFilter magFilter,
            final float minLOD, final float maxLOD,
            final Swizzle swizzle,
            final AddressMode wrapS, final AddressMode wrapT, final AddressMode wrapR,
            final float maxAnisotropy) {

        this.minFilter = minFilter;
        this.magFilter = magFilter;
        this.minLOD = minLOD;
        this.maxLOD = maxLOD;
        this.swizzle = swizzle;
        this.wrapS = wrapS;
        this.wrapT = wrapT;
        this.wrapR = wrapR;
        this.maxAnisotropy = maxAnisotropy;
    }

    public GLESSamplerInfo() {
        this(
                GLESFilter.NEAREST_MIPMAP_LINEAR, GLESFilter.LINEAR,
                -1000F, 1000F,
                new Swizzle(),
                AddressMode.REPEAT, AddressMode.REPEAT, AddressMode.REPEAT,
                1.0F);
    }
    
    public GLESSamplerInfo withMinFilter(final GLESFilter minFilter) {
        return new GLESSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLESSamplerInfo withMagFilter(final GLESFilter magFilter) {
        return new GLESSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLESSamplerInfo withMinLOD(final float minLOD) {
        return new GLESSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLESSamplerInfo withMaxLOD(final float maxLOD) {
        return new GLESSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLESSamplerInfo withSwizzle(final Swizzle swizzle) {
        return new GLESSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLESSamplerInfo withWrapS(final AddressMode wrapS) {
        return new GLESSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLESSamplerInfo withWrapT(final AddressMode wrapT) {
        return new GLESSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLESSamplerInfo withWrapR(final AddressMode wrapR) {
        return new GLESSamplerInfo(minFilter, magFilter, minLOD, maxLOD, swizzle, wrapS, wrapT, wrapR, maxAnisotropy);
    }
    
    public GLESSampler create() {
        return new GLESSampler(this);
    }
}
