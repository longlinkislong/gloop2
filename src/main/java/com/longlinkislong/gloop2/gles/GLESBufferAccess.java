/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public enum GLESBufferAccess {
    READ(GLES30.GL_MAP_READ_BIT),
    WRITE(GLES30.GL_MAP_WRITE_BIT),
    INVALIDATE_RANGE(GLES30.GL_MAP_INVALIDATE_RANGE_BIT),
    INVALIDATE_BUFFER(GLES30.GL_MAP_INVALIDATE_BUFFER_BIT),
    FLUSH_EXPLICIT(GLES30.GL_MAP_FLUSH_EXPLICIT_BIT),
    UNSYNCHRONIZED(GLES30.GL_MAP_UNSYNCHRONIZED_BIT);
    
    public final int value;
    
    private GLESBufferAccess(final int value) {
        this.value = value;
    }
    
    public static Set<GLESBufferAccess> flags(final int bits) {
        return Collections.unmodifiableSet(Arrays.stream(values())
                .filter(e -> (e.value & bits) != 0)
                .collect(Collectors.toSet()));
    }
    
    public static int bits(final Set<GLESBufferAccess> flags) {
        return flags.stream()
                .mapToInt(flag -> flag.value)
                .reduce(0, (a, b) -> a | b);
    }
}
