/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public class GLESMultisampleStateInfo {

    public final boolean alphaToCoverageEnabled;
    public final boolean sampleCoverageEnabled;
    public final float sampleCoverage;
    public boolean sampleCoverageInvert;

    public GLESMultisampleStateInfo(
            final boolean alphaToCoverageEnabled,
            final boolean sampleCoverageEnabled,
            final float sampleCoverage, final boolean sampleCoverageInvert) {

        this.alphaToCoverageEnabled = alphaToCoverageEnabled;
        this.sampleCoverage = sampleCoverage;
        this.sampleCoverageEnabled = sampleCoverageEnabled;
        this.sampleCoverageInvert = sampleCoverageInvert;
    }
    
    public void apply() {
        if (alphaToCoverageEnabled) {
            GLES20.glEnable(GLES20.GL_SAMPLE_ALPHA_TO_COVERAGE);
        } else {
            GLES20.glDisable(GLES20.GL_SAMPLE_ALPHA_TO_COVERAGE);
        }
        
        if (sampleCoverageEnabled) {
            GLES20.glEnable(GLES20.GL_SAMPLE_COVERAGE);
        } else {
            GLES20.glDisable(GLES20.GL_SAMPLE_COVERAGE);
        }
        
        if (alphaToCoverageEnabled || sampleCoverageEnabled) {
            GLES20.glSampleCoverage(this.sampleCoverage, this.sampleCoverageInvert);
        }
    }
}
