/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class GLESFramebufferInfo {
    public final List<GLESAttachment> attachments;
    
    public GLESFramebufferInfo(final List<GLESAttachment> attachments) {
        this.attachments = Tools.copyList(attachments);
    }
    
    public GLESFramebufferInfo withAttachments(final List<GLESAttachment> attachments) {
        return new GLESFramebufferInfo(attachments);
    }
    
    public GLESFramebufferInfo withAttachments(final GLESAttachment... attachments) {
        return withAttachments(Arrays.asList(attachments));
    }
    
    public GLESFramebuffer create() {
        return new GLESFramebuffer(this);
    }
}
