/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public final class GLESUniform {
    private GLESUniform() {}
    
    public void uniform1(final int location, final int value) {
        GLES20.glUniform1i(location, value);
    }
    
    public void uniform1i(final int location, final IntBuffer values) {
        GLES20.glUniform1iv(location, values);
    }
    
    public void uniform2(final int location, final int x, final int y) {
        GLES20.glUniform2i(location, x, y);
    }
    
    public void uniform2(final int location, final IntBuffer values) {
        GLES20.glUniform2iv(location, values);
    }
    
    public void uniform3(final int location, final int x, final int y, final int z) {
        GLES20.glUniform3i(location, x, y, z);
    }
    
    public void uniform3(final int location, final IntBuffer values) {
        GLES20.glUniform3iv(location, values);
    }
    
    public void uniform4(final int location, final int x, final int y, final int z, final int w) {
        GLES20.glUniform4i(location, x, y, z, w);
    }
    
    public void uniform4(final int location, final IntBuffer values) {
        GLES20.glUniform4iv(location, values);
    }
    
    public void uniform1(final int location, final float value) {
        GLES20.glUniform1f(location, value);
    }
    
    public void uniform1(final int location, final FloatBuffer values) {
        GLES20.glUniform1fv(location, values);
    }
    
    public void uniform2(final int location, final float x, final float y) {
        GLES20.glUniform2f(location, x, y);
    }
    
    public void uniform2(final int location, final FloatBuffer values) {
        GLES20.glUniform2fv(location, values);
    }
    
    public void uniform3(final int location, final float x, final float y, final float z) {
        GLES20.glUniform3f(location, x, y, z);
    }
    
    public void uniform3(final int location, final FloatBuffer values) {
        GLES20.glUniform3fv(location, values);
    }
    
    public void uniform4(final int location, final float x, final float y, final float z, final float w) {
        GLES20.glUniform4f(location, x, y, z, w);
    }
    
    public void uniform4(final int location, final FloatBuffer values) {
        GLES20.glUniform4fv(location, values);
    }
    
    public void uniformMatrix2(final int location, final boolean transpose, final FloatBuffer values) {
        GLES20.glUniformMatrix2fv(location, transpose, values);
    }
    
    public void uniformMatrix3(final int location, final boolean transpose, final FloatBuffer values) {
        GLES20.glUniformMatrix3fv(location, transpose, values);
    }
    
    public void uniformMatrix4(final int location, final boolean transpose, final FloatBuffer values) {
        GLES20.glUniformMatrix4fv(location, transpose, values);
    }
}
