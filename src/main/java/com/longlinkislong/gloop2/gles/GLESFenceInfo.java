/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

/**
 *
 * @author zmichaels
 */
public class GLESFenceInfo {
    public final GLESFenceCondition condition;
    public final int flags;
    
    public GLESFenceInfo(final GLESFenceCondition condition, final int flags) {
        this.condition = condition;
        this.flags = flags;
    }
    
    public GLESFenceInfo() {
        this(GLESFenceCondition.GPU_COMMANDS_COMPLETE, 0);
    }
    
    public GLESFenceInfo withFlags(final int flags) {
        return new GLESFenceInfo(condition, flags);
    }
    
    public GLESFenceInfo withCondition(final GLESFenceCondition condition) {
        return new GLESFenceInfo(condition, flags);
    }
    
    public GLESSync fence() {
        return GLESSync.fence(this);
    }
}
