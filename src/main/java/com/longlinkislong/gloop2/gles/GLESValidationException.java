/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

/**
 *
 * @author zmichaels
 */
public class GLESValidationException extends Exception {
    public GLESValidationException(final String infoLog) {
        super(infoLog);
    }
}
