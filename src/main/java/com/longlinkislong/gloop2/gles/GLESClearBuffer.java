/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public enum GLESClearBuffer {
    COLOR(GLES20.GL_COLOR_BUFFER_BIT),
    DEPTH(GLES20.GL_DEPTH_BUFFER_BIT),
    STENCIL(GLES20.GL_STENCIL_BUFFER_BIT);
    
    public int value;
    
    private GLESClearBuffer(final int value) {
        this.value = value;
    }
    
    public static Set<GLESClearBuffer> flags(final int bits) {
        return Collections.unmodifiableSet(
                Arrays.stream(values())
                .filter(flag -> (flag.value & bits) != 0)
                .collect(Collectors.toSet()));
    }
    
    public static int bits(final Set<GLESClearBuffer> flags) {
        return flags.stream()
                .mapToInt(flag -> flag.value)
                .reduce(0, (a, b) -> a | b);
    }
}
