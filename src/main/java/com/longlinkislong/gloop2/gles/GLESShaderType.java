/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public enum GLESShaderType {
    VERTEX(GLES20.GL_VERTEX_SHADER),
    FRAGMENT(GLES20.GL_FRAGMENT_SHADER);
    
    public final int value;
    
    private GLESShaderType(final int value) {
        this.value = value;
    }
    
    public static Optional<GLESShaderType> of(final int value) {
        return Arrays.stream(values())
                .filter(e -> e.value == value)
                .findFirst();
    }
}
