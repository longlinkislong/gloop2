/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESQuery extends GLESObject<Object, GLESQuery> {    
    public GLESQuery() {        
        this.handle = GLES30.glGenQueries();
    }
    
    public static final class QueryTask implements AutoCloseable {
        GLESQuery host;
        GLESQueryTarget target;
        
        private QueryTask(final GLESQuery host, final GLESQueryTarget target) {
            this.host = host;
            this.target = target;
            
            GLES30.glBeginQuery(target.value, host.handle);
        }
        
        public boolean isValid() {
            return this.host != null && this.host.isValid();
        }
        
        @Override
        public void close() {
            if (this.isValid()) {
                GLES30.glEndQuery(this.target.value);
                this.host = null;
                this.target = null;
            }
        }
        
    }
            
    public QueryTask begin(final GLESQueryTarget target) {
        return new QueryTask(this, target);
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GLES30.glDeleteQueries(this.handle);
            this.info = null;
            this.handle = 0;
        }
    }
    
    public boolean isResultAvailable() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESQuery is not valid!");
        }
        
        return GLES30.glGetQueryObjectui(this.handle, GLES30.GL_QUERY_RESULT_AVAILABLE) == GLES20.GL_TRUE;
    }
    
    public int getResult() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESQuery is not valid!");
        }
        
        return GLES30.glGetQueryObjectui(this.handle, GLES30.GL_QUERY_RESULT);
    }
}
