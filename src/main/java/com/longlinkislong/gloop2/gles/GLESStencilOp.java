/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public enum GLESStencilOp {
    KEEP(GLES20.GL_KEEP),
    ZERO(GLES20.GL_ZERO),
    REPLACE(GLES20.GL_REPLACE),
    INCR(GLES20.GL_INCR),
    DECR(GLES20.GL_DECR),
    INVERT(GLES20.GL_INVERT),
    INCR_WRAP(GLES20.GL_INCR_WRAP),
    DECR_WRAP(GLES20.GL_DECR_WRAP);

    public final int value;

    private GLESStencilOp(final int value) {
        this.value = value;
    }
    
    public static Optional<GLESStencilOp> of(final int value) {
        return Arrays.stream(values())
                .filter(e -> e.value == value)
                .findFirst();
    }
}
