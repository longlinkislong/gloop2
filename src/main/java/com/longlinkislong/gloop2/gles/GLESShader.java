/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public class GLESShader extends GLESObject<GLESShaderInfo, GLESShader> {
    public GLESShader() {}
    
    public GLESShader(final GLESShaderInfo info) {
        this.info = info;
        this.handle = GLES20.glCreateShader(info.type.value);
        
        GLES20.glShaderSource(this.handle, info.src);
        GLES20.glCompileShader(this.handle);
        
        if (GLES20.glGetShaderi(this.handle, GLES20.GL_COMPILE_STATUS) != GLES20.GL_TRUE) {
            GLES20.glDeleteShader(this.handle);
            
            final String infoLog = GLES20.glGetShaderInfoLog(this.handle);
            
            this.handle = 0;
            
            throw new GLESCompileException(infoLog);
        }                
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GLES20.glDeleteShader(this.handle);
            this.handle = 0;
            this.info = null;
        }
    }
    
    public static GLESShader newVertexShader(final String src) {
        return new GLESShaderInfo()
                .withSrc(src)
                .withType(GLESShaderType.VERTEX)
                .create();
    }
    
    public static GLESShader newFragmentShader(final String src) {
        return new GLESShaderInfo()
                .withSrc(src)
                .withType(GLESShaderType.FRAGMENT)
                .create();
    }
}
