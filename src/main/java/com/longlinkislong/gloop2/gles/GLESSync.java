/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESSync extends GLESObject<GLESFenceInfo, GLESSync> {     
    long handle;
    
    private GLESSync(final GLESFenceInfo info) {
        this.info = info;
        this.handle = GLES30.glFenceSync(info.condition.value, info.flags);        
    }
    
    
    @Override
    public void swap(final GLESSync other) {
        if (other == null) {
            this.close();
            return;
        }
        
        final long tmpHandle = other.handle;
        final GLESFenceInfo tmpInfo = other.info;
        
        other.handle = this.handle;
        other.info = this.info;
        
        this.handle = tmpHandle;
        this.info = tmpInfo;
    }
    
    public static enum ClientWait {
        FLUSH_COMMANDS(GLES30.GL_SYNC_FLUSH_COMMANDS_BIT);
        
        public final int value;
        
        private ClientWait(final int value) {
            this.value = value;
        }
    }
    
    public void clientWaitSync(final ClientWait wait, final long timeout) {
        GLES30.glClientWaitSync(handle, wait == null ? 0 : wait.value, timeout);
    }
    
    public void waitSync() {
        GLES30.glWaitSync(handle, 0, GLES30.GL_TIMEOUT_IGNORED);
    }
    
    public static GLESSync fence(final GLESFenceInfo info) {
        return new GLESSync(info);
    }
    
    @Override
    public boolean isValid() {
        return this.handle != 0L;
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GLES30.glDeleteSync(this.handle);
            this.handle = 0L;
            this.info = null;
        }
    }
}
