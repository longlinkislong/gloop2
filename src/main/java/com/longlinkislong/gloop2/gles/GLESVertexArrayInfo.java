/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class GLESVertexArrayInfo {

    public final GLESBuffer indexBuffer;
    public final List<GLESVertexAttributeInfo> vertexAttributes;

    public GLESVertexArrayInfo(
            final GLESBuffer indexBuffer,
            final List<GLESVertexAttributeInfo> vertexAttributes) {

        this.indexBuffer = indexBuffer;
        this.vertexAttributes = Tools.copyList(vertexAttributes);
    }
    
    public GLESVertexArrayInfo() {
        this(null, null);
    }
    
    public GLESVertexArrayInfo withIndexBuffer(final GLESBuffer indexBuffer) {
        return new GLESVertexArrayInfo(indexBuffer, vertexAttributes);
    }
    
    public GLESVertexArrayInfo withVertexAttributes(final List<GLESVertexAttributeInfo> vertexAttributes) {
        return new GLESVertexArrayInfo(indexBuffer, vertexAttributes);
    }
    
    public GLESVertexArrayInfo withVertexAttributes(final GLESVertexAttributeInfo... vertexAttributes) {
        return withVertexAttributes(Arrays.asList(vertexAttributes));
    }
    
    public GLESVertexArray create() {
        return new GLESVertexArray(this);
    }
}
