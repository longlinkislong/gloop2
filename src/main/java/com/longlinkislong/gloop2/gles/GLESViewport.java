/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public class GLESViewport {
    public final int x;
    public final int y;
    public final int width;
    public final int height;
    public final float minDepth;
    public final float maxDepth;
    
    public GLESViewport(final int x, final int y, final int width, final int height, final float minDepth, final float maxDepth) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.minDepth = minDepth;
        this.maxDepth = maxDepth;
    }
    
    public GLESViewport() {
        this(0, 0, 0, 0, 0, 0);
    }
    
    public GLESViewport withOffset(final int x, final int y) {
        return new GLESViewport(x, y, width, height, minDepth, maxDepth);
    }
    
    public GLESViewport withExtent(final int width, final int height) {
        return new GLESViewport(x, y, width, height, minDepth, maxDepth);
    }
    
    public GLESViewport withDepthRange(final float minDepth, final float maxDepth) {
        return new GLESViewport(x, y, width, height, minDepth, maxDepth);
    }
    
    public void apply() {
        GLES20.glDepthRangef(this.minDepth, this.maxDepth);
        GLES20.glViewport(x, y, width, height);
    }
}
