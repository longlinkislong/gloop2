/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public class GLESScissorTest {
    public final boolean scissorTestEnabled;
    public final int left, bottom, width, height;
    
    public GLESScissorTest(
            final boolean scissorTestEnabled, 
            final int left, final int bottom, final int width, final int height) {
        
        this.scissorTestEnabled = scissorTestEnabled;
        this.left = left;
        this.bottom = bottom;
        this.width = width;
        this.height = height;
    }
    
    public void apply() {
        if (this.scissorTestEnabled) {
            GLES20.glEnable(GLES20.GL_SCISSOR_TEST);
            GLES20.glScissor(left, bottom, width, height);
        } else {
            GLES20.glDisable(GLES20.GL_SCISSOR_TEST);
        }
    }
}
