/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public enum GLESCompareOp {
    NEVER(GLES20.GL_NEVER),
    ALWAYS(GLES20.GL_ALWAYS),
    LESS(GLES20.GL_LESS),
    GREATER(GLES20.GL_GREATER),
    LEQUAL(GLES20.GL_LEQUAL),
    GEQUAL(GLES20.GL_GEQUAL),
    NOTEQUAL(GLES20.GL_NOTEQUAL);
    
    public final int value;
    
    private GLESCompareOp(final int value) {
        this.value = value;
    }
    
    public static Optional<GLESCompareOp> of(final int value) {
        return Arrays.stream(values())
                .filter(e -> e.value == value)
                .findFirst();
    }
}
