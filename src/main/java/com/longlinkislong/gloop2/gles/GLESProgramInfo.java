/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESProgramInfo {

    public static final class VaryingStateInfo {

        public static enum BufferMode {
            INTERLEAVED(GLES30.GL_INTERLEAVED_ATTRIBS),
            SEPARATE(GLES30.GL_SEPARATE_ATTRIBS);

            public final int value;

            private BufferMode(final int value) {
                this.value = value;
            }
            
            public static Optional<BufferMode> of(final int value) {
                return Arrays.stream(values())
                        .filter(e -> e.value == value)
                        .findFirst();
            }
        }

        public final String name;
        public final BufferMode mode;

        public VaryingStateInfo(final String name, final BufferMode mode) {
            this.name = name;
            this.mode = mode;
        }

        public VaryingStateInfo() {
            this("", BufferMode.SEPARATE);
        }

        public VaryingStateInfo withName(final String name) {
            return new VaryingStateInfo(name, mode);
        }

        public VaryingStateInfo withBufferMode(final BufferMode mode) {
            return new VaryingStateInfo(name, mode);
        }
    }

    public static final class AttributeStateInfo {

        public final String name;
        public final int location;

        public AttributeStateInfo(final String name, final int index) {
            this.name = name;
            this.location = index;
        }

        public AttributeStateInfo() {
            this("", 0);
        }

        public AttributeStateInfo withName(final String name) {
            return new AttributeStateInfo(name, location);
        }

        public AttributeStateInfo withIndex(final int index) {
            return new AttributeStateInfo(name, index);
        }
    }

    public final List<GLESShader> shaders;
    public final List<VaryingStateInfo> varyings;
    public final List<AttributeStateInfo> attributes;

    public GLESProgramInfo(
            final List<GLESShader> shaders,
            final List<VaryingStateInfo> varyings,
            final List<AttributeStateInfo> attributes) {

        this.shaders = Tools.copyList(shaders);
        this.varyings = Tools.copyList(varyings);
        this.attributes = Tools.copyList(attributes);
    }
    
    public GLESProgramInfo withShaders(final List<GLESShader> shaders) {
        return new GLESProgramInfo(shaders, varyings, attributes);
    }
    
    public GLESProgramInfo withShaders(final GLESShader... shaders) {
        return withShaders(Arrays.asList(shaders));
    }
    
    public GLESProgramInfo withVaryings(final List<VaryingStateInfo> varyings) {
        return new GLESProgramInfo(shaders, varyings, attributes);
    }
    
    public GLESProgramInfo withVaryings(final VaryingStateInfo... varyings) {
        return withVaryings(Arrays.asList(varyings));
    }
    
    public GLESProgramInfo withAttributes(final List<AttributeStateInfo> attributes) {
        return new GLESProgramInfo(shaders, varyings, attributes);
    }
    
    public GLESProgramInfo withAttributes(final AttributeStateInfo... attributes) {
        return withAttributes(Arrays.asList(attributes));
    }
    
    public GLESProgram create() {
        return new GLESProgram(this);
    }
}
