/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public enum GLESQueryTarget {
    ANY_SAMPLES_PASSED(GLES30.GL_ANY_SAMPLES_PASSED),
    ANY_SAMPLES_PASSED_CONSERVATIVE(GLES30.GL_ANY_SAMPLES_PASSED_CONSERVATIVE);
    
    public final int value;
    
    private GLESQueryTarget(final int value) {
        this.value = value;
    }
}
