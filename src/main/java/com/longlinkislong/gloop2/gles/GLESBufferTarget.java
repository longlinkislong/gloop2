/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public enum GLESBufferTarget {
    ELEMENT_ARRAY(GLES20.GL_ELEMENT_ARRAY_BUFFER),
    ARRAY(GLES20.GL_ARRAY_BUFFER),
    PIXEL_UNPACK(GLES30.GL_PIXEL_UNPACK_BUFFER),
    PIXEL_PACK(GLES30.GL_PIXEL_PACK_BUFFER),
    COPY_READ(GLES30.GL_COPY_READ_BUFFER),
    COPY_WRITE(GLES30.GL_COPY_WRITE_BUFFER),
    UNIFORM(GLES30.GL_UNIFORM_BUFFER),
    TRANSFORM_FEEDBACK(GLES30.GL_TRANSFORM_FEEDBACK);
    
    public final int value;
    
    private GLESBufferTarget(final int value) {
        this.value = value;
    }
    
    public final Optional<GLESBufferTarget> of (final int value) {
        return Arrays.stream(values())
                .filter(e -> e.value == value)
                .findFirst();
    }
}
