/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESVertexAttributeInfo {

    public static enum Type {
        INTEGER,
        FLOAT,
        NORMALIZED
    }
    
    public static enum Format {
        X8_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_BYTE, 1),
        X8_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_BYTE, 1),
        X8_SINT(Type.INTEGER, GLES20.GL_BYTE, 1),
        X8_SNORM(Type.NORMALIZED, GLES20.GL_BYTE, 1),
        X16_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_SHORT, 1),
        X16_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_SHORT, 1),
        X16_SINT(Type.INTEGER, GLES20.GL_SHORT, 1),
        X16_SNORM(Type.NORMALIZED, GLES20.GL_SHORT, 1),
        X16_SFLOAT(Type.FLOAT, GLES30.GL_HALF_FLOAT, 1),
        X32_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_INT, 1),
        X32_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_INT, 1),
        X32_SINT(Type.INTEGER, GLES20.GL_INT, 1),
        X32_SNORM(Type.NORMALIZED, GLES20.GL_INT, 1),
        X32_SFLOAT(Type.FLOAT, GLES20.GL_FLOAT, 1),
        X8Y8_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_BYTE, 2),
        X8Y8_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_BYTE, 2),
        X8Y8_SINT(Type.INTEGER, GLES20.GL_BYTE, 2),
        X8Y8_SNORM(Type.NORMALIZED, GLES20.GL_BYTE, 2),
        X16Y16_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_SHORT, 2),
        X16Y16_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_SHORT, 2),
        X16Y16_SINT(Type.INTEGER, GLES20.GL_SHORT, 2),
        X16Y16_SNORM(Type.NORMALIZED, GLES20.GL_SHORT, 2),
        X16Y16_SFLOAT(Type.FLOAT, GLES30.GL_HALF_FLOAT, 2),
        X32Y32_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_INT, 2),
        X32Y32_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_INT, 2),
        X32Y32_SINT(Type.INTEGER, GLES20.GL_INT, 2),
        X32Y32_SNORM(Type.NORMALIZED, GLES20.GL_INT, 2),
        X32Y32_SFLOAT(Type.FLOAT, GLES20.GL_FLOAT, 2),
        X8Y8Z8_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_BYTE, 2),
        X8Y8Z8_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_BYTE, 2),
        X8Y8Z8_SINT(Type.INTEGER, GLES20.GL_BYTE, 2),
        X8Y8Z8_SNORM(Type.NORMALIZED, GLES20.GL_BYTE, 2),
        X16Y16Z16_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_SHORT, 3),
        X16Y16Z16_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_SHORT, 3),
        X16Y16Z16_SINT(Type.INTEGER, GLES20.GL_SHORT, 3),
        X16Y16Z16_SNORM(Type.NORMALIZED, GLES20.GL_SHORT, 3),
        X16Y16Z16_SFLOAT(Type.FLOAT, GLES30.GL_HALF_FLOAT, 3),
        X32Y32Z32_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_INT, 3),
        X32Y32Z32_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_INT, 3),
        X32Y32Z32_SINT(Type.INTEGER, GLES20.GL_INT, 3),
        X32Y32Z32_SNORM(Type.NORMALIZED, GLES20.GL_INT, 3),
        X32Y32Z32_SFLOAT(Type.FLOAT, GLES20.GL_FLOAT, 3),
        X8Y8Z8W8_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_BYTE, 4),
        X8Y8Z8W8_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_BYTE, 4),
        X8Y8Z8W8_SINT(Type.INTEGER, GLES20.GL_BYTE, 4),
        X8Y8Z8W8_SNORM(Type.NORMALIZED, GLES20.GL_BYTE, 4),
        X16Y16Z16W16_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_SHORT, 4),
        X16Y16Z16W16_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_SHORT, 4),
        X16Y16Z16W16_SINT(Type.INTEGER, GLES20.GL_SHORT, 4),
        X16Y16Z16W16_SNORM(Type.NORMALIZED, GLES20.GL_SHORT, 4),
        X16Y16Z16W16_SFLOAT(Type.FLOAT, GLES30.GL_HALF_FLOAT, 4),
        X32Y32Z32W32_UINT(Type.INTEGER, GLES20.GL_UNSIGNED_INT, 4),
        X32Y32Z32W32_UNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_INT, 4),
        X32Y32Z32W32_SINT(Type.INTEGER, GLES20.GL_UNSIGNED_INT, 4),
        X32Y32Z32W32_SNORM(Type.NORMALIZED, GLES20.GL_UNSIGNED_INT, 4),
        X32Y32Z32W32_SFLOAT(Type.FLOAT, GLES20.GL_UNSIGNED_INT, 4),
        W2Z10Y10X10_UINT(Type.INTEGER, GLES30.GL_UNSIGNED_INT_2_10_10_10_REV, 4),
        W2Z10Y10X10_UNORM(Type.NORMALIZED, GLES30.GL_UNSIGNED_INT_2_10_10_10_REV, 4),
        W2Z10Y10X10_SINT(Type.INTEGER, GLES30.GL_INT_2_10_10_10_REV, 4),
        W2Z10Y10X10_SNORM(Type.NORMALIZED, GLES30.GL_INT_2_10_10_10_REV, 4);

        public static final Format VEC2 = X32Y32_SFLOAT;
        public static final Format VEC3 = X32Y32Z32_SFLOAT;
        public static final Format VEC4 = X32Y32Z32W32_SFLOAT;

        public static final Format IVEC2 = X32Y32_SINT;
        public static final Format IVEC3 = X32Y32Z32_SINT;
        public static final Format IVEC4 = X32Y32Z32W32_SINT;

        public static final Format UIVEC2 = X32Y32_UINT;
        public static final Format UIVEC3 = X32Y32Z32_UINT;
        public static final Format UIVEC4 = X32Y32Z32W32_UINT;

        public final Type type;
        public final int value;
        public final int size;

        private Format(
                final Type type,
                final int value, final int size) {

            this.type = type;
            this.value = value;
            this.size = size;
        }
    }

    public static enum VertexInputRate {
        VERTEX,
        INSTANCE
    }

    public final int location;
    public final long offset;
    public final int stride;
    public final int divisor;
    public final GLESBuffer buffer;
    public final Format format;

    public GLESVertexAttributeInfo(
            final int location,
            final long offset,
            final int stride,
            final int divisor,
            final GLESBuffer buffer,
            final Format format) {

        this.location = location;
        this.offset = offset;
        this.stride = stride;
        this.divisor = divisor;
        this.buffer = buffer;
        this.format = format;
    }

    public GLESVertexAttributeInfo() {
        this(0, 0, 0, 0, null, Format.VEC4);
    }

    public GLESVertexAttributeInfo withLocation(final int location) {
        return new GLESVertexAttributeInfo(location, offset, stride, divisor, buffer, format);
    }

    public GLESVertexAttributeInfo withOffset(final long offset) {
        return new GLESVertexAttributeInfo(location, offset, stride, divisor, buffer, format);
    }

    public GLESVertexAttributeInfo withStride(final int stride) {
        return new GLESVertexAttributeInfo(location, offset, stride, divisor, buffer, format);
    }

    public GLESVertexAttributeInfo withDivisor(final int divisor) {
        return new GLESVertexAttributeInfo(location, offset, stride, divisor, buffer, format);
    }

    public GLESVertexAttributeInfo withBuffer(final GLESBuffer buffer) {
        return new GLESVertexAttributeInfo(location, offset, stride, divisor, buffer, format);
    }

    public GLESVertexAttributeInfo withFormat(final Format format) {
        return new GLESVertexAttributeInfo(location, offset, stride, divisor, buffer, format);
    }
}
