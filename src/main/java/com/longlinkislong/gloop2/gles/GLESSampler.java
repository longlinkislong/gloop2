/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.EXTTextureFilterAnisotropic;
import org.lwjgl.opengles.GLES;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESSampler extends GLESObject<GLESSamplerInfo, GLESSampler> {
    public GLESSampler() {}
    
    public GLESSampler(final GLESSamplerInfo info) {
        this.info = info;
        
        this.handle = GLES30.glGenSamplers();
        
        GLES30.glSamplerParameteri(handle, GLES20.GL_TEXTURE_WRAP_S, info.wrapS.value);
        GLES30.glSamplerParameteri(handle, GLES20.GL_TEXTURE_WRAP_T, info.wrapT.value);
        GLES30.glSamplerParameteri(handle, GLES30.GL_TEXTURE_WRAP_R, info.wrapR.value);
        
        GLES30.glSamplerParameteri(handle, GLES20.GL_TEXTURE_MIN_FILTER, info.minFilter.value);
        GLES30.glSamplerParameteri(handle, GLES20.GL_TEXTURE_MAG_FILTER, info.magFilter.value);
        
        GLES30.glSamplerParameterf(handle, GLES30.GL_TEXTURE_MIN_LOD, info.minLOD);
        GLES30.glSamplerParameterf(handle, GLES30.GL_TEXTURE_MAX_LOD, info.maxLOD);
        
        if (GLES.getCapabilities().GL_EXT_texture_filter_anisotropic) {
            GLES30.glSamplerParameterf(handle, EXTTextureFilterAnisotropic.GL_TEXTURE_MAX_ANISOTROPY_EXT, info.maxAnisotropy);
        }
    }
    
    public void bind(final int unit) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESampler is not valid!");
        }
        
        GLES30.glBindSampler(unit, handle);
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GLES30.glDeleteSamplers(this.handle);
            this.handle = 0;
            this.info = null;
        }
    }
    
    public static final GLESSampler DEFAULT_SAMPLER = new GLESSampler() {
        @Override
        public void bind(int unit) {
            GLES30.glBindSampler(unit, 0);
        }
    };
}
