/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESProgram extends GLESObject<GLESProgramInfo, GLESProgram> {
    public GLESProgram() {}
    
    public GLESProgram(final GLESProgramInfo info) {
        this.info = info;
        
        this.handle = GLES20.glCreateProgram();
        
        info.shaders.stream()
                .filter(GLESShader::isValid)
                .forEach(shader -> GLES20.glAttachShader(handle, shader.handle));
        
        info.attributes.stream()
                .forEach(attribute -> GLES20.glBindAttribLocation(handle, attribute.location, attribute.name));
        
        info.varyings.stream()
                .forEach(varying -> GLES30.glTransformFeedbackVaryings(handle, varying.name, varying.mode.value));
        
        GLES20.glLinkProgram(handle);
        
        if (GLES20.glGetProgrami(handle, GLES20.GL_LINK_STATUS) != GLES20.GL_TRUE) {
            GLES20.glDeleteProgram(handle);
            
            final String infoLog = GLES20.glGetProgramInfoLog(this.handle);
            
            this.handle = 0;
            
            throw new GLESLinkException(infoLog);
        }
    }
    
    public int getUniformLocation(final String name) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESProgram is not valid!");
        }
        
        return GLES20.glGetUniformLocation(this.handle, name);
    }
    
    public int getUniformBlockIndex(final String name) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESProgram is not valid!");
        }
        
        return GLES30.glGetUniformBlockIndex(this.handle, name);
    }
    
    public int getFragDataLocation(final String name) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESProgram is not valid!");
        }
        
        return GLES30.glGetFragDataLocation(this.handle, name);
    }
    
    public void validate() throws GLESValidationException {
        GLES20.glValidateProgram(this.handle);
        
        if (GLES20.glGetProgrami(this.handle, GLES20.GL_VALIDATE_STATUS) != GLES20.GL_TRUE) {
            throw new GLESValidationException(GLES20.glGetProgramInfoLog(this.handle));
        }
    }
    
    public void use() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESProgram is not valid!");
        }
        
        GLES20.glUseProgram(this.handle);
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            GLES20.glDeleteProgram(this.handle);
            this.handle = 0;
            this.info = null;
        }
    }
    
    public static final GLESProgram DEFAULT_PROGRAM = new GLESProgram() {      
        @Override
        public void use() {
            GLES20.glUseProgram(0);
        }
    };
}
