/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public class GLESRasterizationStateInfo {

    public static enum FrontFace {
        CCW(GLES20.GL_CCW),
        CW(GLES20.GL_CW);
        
        public final int value;
        
        private FrontFace(final int value) {
            this.value = value;
        }
        
        public static Optional<FrontFace> of(final int value) {
            return Arrays.stream(values())
                    .filter(e -> e.value == value)
                    .findFirst();
        }
    }
    
    public static enum CullMode {
        NONE(0),
        FRONT(GLES20.GL_FRONT),
        BACK(GLES20.GL_BACK),
        FRONT_AND_BACK(GLES20.GL_FRONT_AND_BACK);

        public final int value;

        private CullMode(final int value) {
            this.value = value;
        }

        public static Optional<CullMode> of(final int value) {
            return Arrays.stream(values())
                    .filter(e -> e.value == value)
                    .findFirst();
        }
    }
    public final float lineWidth;
    public final FrontFace frontFace;
    public final CullMode cullMode;
    public final boolean polygonOffsetEnabled;
    public final float polygonOffsetFactor;
    public final float polygonOffsetUnits;

    public GLESRasterizationStateInfo(
            final CullMode cullMode,
            final float lineWidth,
            final boolean polygonOffsetEnabled, final float polygonOffsetFactor, final float polygonOffsetUnits,
            final FrontFace frontFace) {
        
        this.cullMode = cullMode;
        this.lineWidth = lineWidth;
        this.polygonOffsetEnabled = polygonOffsetEnabled;
        this.polygonOffsetFactor = polygonOffsetFactor;
        this.polygonOffsetUnits = polygonOffsetUnits;
        this.frontFace = frontFace;
    }
    
    public GLESRasterizationStateInfo() {
        this(CullMode.NONE, 1.0F, false, 0.0F, 0.0F, FrontFace.CCW);
    }
    
    public GLESRasterizationStateInfo withFrontFace(final FrontFace frontFace) {
        return new GLESRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, frontFace);
    }
    
    public GLESRasterizationStateInfo withCullMode(final CullMode cullMode) {
        return new GLESRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, frontFace);
    }

    public GLESRasterizationStateInfo withLineWidth(final float lineWidth) {
return new GLESRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, frontFace);
    }
    
    public GLESRasterizationStateInfo withPolygonOffsetEnabled(final boolean polygonOffsetEnabled) {
        return new GLESRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, frontFace);
    }
    
    public GLESRasterizationStateInfo withPolygonOffsetFactor(final float polygonOffsetFactor) {
        return new GLESRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, frontFace);
    }
    
    public GLESRasterizationStateInfo withPolygonOffsetUnits(final float polygonOffsetUnits) {
        return new GLESRasterizationStateInfo(cullMode, lineWidth, polygonOffsetEnabled, polygonOffsetFactor, polygonOffsetUnits, frontFace);
    }
    
    public void apply() {
        if (this.polygonOffsetEnabled) {
            GLES20.glPolygonOffset(this.polygonOffsetFactor, this.polygonOffsetUnits);
        }
        
        GLES20.glLineWidth(this.lineWidth);
        
        if (this.cullMode != CullMode.NONE) {
            GLES20.glEnable(GLES20.GL_CULL_FACE);
            
            GLES20.glCullFace(this.cullMode.value);
            GLES20.glFrontFace(this.frontFace.value);
        } else {
            GLES20.glDisable(GLES20.GL_CULL_FACE);
        }
        
    }
}
