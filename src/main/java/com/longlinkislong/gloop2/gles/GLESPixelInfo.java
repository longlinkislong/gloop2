/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import com.longlinkislong.gloop2.image.Image;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public final class GLESPixelInfo {

    public static enum Type {
        UNSIGNED_BYTE(GLES20.GL_UNSIGNED_BYTE),
        UNSIGNED_SHORT(GLES20.GL_UNSIGNED_SHORT),
        UNSIGNED_INT(GLES20.GL_UNSIGNED_INT),
        BYTE(GLES20.GL_BYTE),
        SHORT(GLES20.GL_SHORT),
        INT(GLES20.GL_INT),
        HALF_FLOAT(GLES30.GL_HALF_FLOAT),
        FLOAT(GLES20.GL_FLOAT),
        UNSIGNED_SHORT_4_4_4_4(GLES20.GL_UNSIGNED_SHORT_4_4_4_4),
        UNSIGNED_SHORT_5_5_5_1(GLES20.GL_UNSIGNED_SHORT_5_5_5_1),
        UNSIGNED_SHORT_5_6_5(GLES20.GL_UNSIGNED_SHORT_5_6_5),
        UNSIGNED_INT_2_10_10_10_REV(GLES30.GL_UNSIGNED_INT_2_10_10_10_REV),
        UNSIGNED_INT_24_8(GLES30.GL_UNSIGNED_INT_24_8),
        UNSIGNED_INT_10F_11F_11F_REV(GLES30.GL_UNSIGNED_INT_10F_11F_11F_REV),
        UNSIGNED_INT_5_9_9_9_REV(GLES30.GL_UNSIGNED_INT_5_9_9_9_REV);

        public final int value;

        private Type(final int value) {
            this.value = value;
        }
        
        public static Optional<Type> of(final int value) {
            return Arrays.stream(values())
                    .filter(e -> e.value == value)
                    .findFirst();
        }
    }

    public final ByteBuffer data;
    public final GLESFormat format;

    public final Type type;

    public GLESPixelInfo(final GLESFormat format, final Type type, final ByteBuffer data) {
        this.data = data;
        this.format = format;
        this.type = type;
    }

    public GLESPixelInfo() {
        this(null, Type.UNSIGNED_BYTE, null);
    }

    public GLESPixelInfo withFormat(final GLESFormat format) {
        return new GLESPixelInfo(format, type, data);
    }

    public GLESPixelInfo withData(final ByteBuffer data) {
        return new GLESPixelInfo(format, type, data);
    }

    public GLESPixelInfo withType(final Type type) {
        return new GLESPixelInfo(format, type, data);
    }
    
    public static GLESPixelInfo of(final Image img) {
        final GLESFormat format;
        final Type type;
        
        switch (img.getFormat()) {
            case R8_UNORM:
                format = GLESFormat.RED;
                type = Type.UNSIGNED_BYTE;
                break;
            case R8G8_UNORM:
                format = GLESFormat.RG;
                type = Type.UNSIGNED_BYTE;
                break;
            case R8G8B8_UNORM:
                format = GLESFormat.RGB;
                type = Type.UNSIGNED_BYTE;
                break;
            case R8G8B8A8_UNORM:
                format = GLESFormat.RGBA;
                type = Type.UNSIGNED_BYTE;
                break;
            default:
                throw new UnsupportedOperationException("Unsupported format: " + img.getFormat());
        }
        
        return new GLESPixelInfo()
                .withData(img.getData())
                .withFormat(format)
                .withType(type);
    }
}
