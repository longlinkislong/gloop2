/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public enum GLESBlendFunction {
    ZERO(GLES20.GL_ZERO),
    ONE(GLES20.GL_ONE),
    ONE_MINUS_SRC_COLOR(GLES20.GL_ONE_MINUS_SRC_COLOR),
    SRC_COLOR(GLES20.GL_SRC_COLOR),
    ONE_MINUS_DST_COLOR(GLES20.GL_ONE_MINUS_DST_COLOR),
    DST_COLOR(GLES20.GL_DST_COLOR),
    ONE_MINUS_SRC_ALPHA(GLES20.GL_ONE_MINUS_SRC_ALPHA),
    SRC_ALPHA(GLES20.GL_SRC_ALPHA),
    ONE_MINUS_DST_ALPHA(GLES20.GL_ONE_MINUS_DST_ALPHA),
    DST_ALPHA(GLES20.GL_DST_ALPHA),
    ONE_MINUS_CONSTANT_COLOR(GLES20.GL_ONE_MINUS_CONSTANT_COLOR),
    CONSTANT_COLOR(GLES20.GL_CONSTANT_COLOR),
    ONE_MINUS_CONSTANT_ALPHA(GLES20.GL_ONE_MINUS_CONSTANT_ALPHA),
    CONSTANT_ALPHA(GLES20.GL_CONSTANT_ALPHA),
    SRC_ALPHA_SATURATE(GLES20.GL_SRC_ALPHA_SATURATE);
    
    public final int value;
    
    private GLESBlendFunction(final int value) {
        this.value = value;
    }
}
