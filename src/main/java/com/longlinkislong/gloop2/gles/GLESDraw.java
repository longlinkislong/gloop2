/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import org.lwjgl.opengles.ANGLEInstancedArrays;
import org.lwjgl.opengles.GLES;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;
import org.lwjgl.opengles.GLESCapabilities;

/**
 *
 * @author zmichaels
 */
public final class GLESDraw {
    private GLESDraw() {}
    
    public static enum Mode {
        POINTS(GLES20.GL_POINTS),
        TRIANGLES(GLES20.GL_TRIANGLES),
        LINES(GLES20.GL_LINES),
        LINE_STRIP(GLES20.GL_LINE_STRIP),
        LINE_LOOP(GLES20.GL_LINE_LOOP),
        TRIANGLE_STRIP(GLES20.GL_TRIANGLE_STRIP),
        TRIANGLE_FAN(GLES20.GL_TRIANGLE_FAN);
        
        public final int value;
        
        private Mode(final int value) {
            this.value = value;
        }
    }
    
    public static enum IndexType {
        UNSIGNED_BYTE(GLES20.GL_UNSIGNED_BYTE),
        UNSIGNED_SHORT(GLES20.GL_UNSIGNED_SHORT),
        UNSIGNED_INT(GLES20.GL_UNSIGNED_INT);
        
        public final int value;
        
        private IndexType(final int value) {
            this.value = value;
        }
    }
    
    public static void drawArrays(final Mode mode, final int first, final int count) {
        GLES20.glDrawArrays(mode.value, first, count);
    }
    
    public static void drawArraysInstanced(final Mode mode, final int first, final int count, final int primCount) {
        final GLESCapabilities caps = GLES.getCapabilities();
        
        if (caps.GLES30) {
            GLES30.glDrawArraysInstanced(mode.value, first, count, primCount);
        } else if (caps.GL_ANGLE_instanced_arrays) {
            ANGLEInstancedArrays.glDrawArraysInstancedANGLE(mode.value, first, count, primCount);
        } else {
            throw new UnsupportedOperationException("drawArraysInstanced requires either OpenGLES 3.0 or GL_ANGLE_instanced_arrays.");
        }
    }
    
    public static void drawElements(final Mode mode, final int count, final IndexType type, final long offset) {
        GLES20.glDrawElements(mode.value, count, type.value, offset);
    }
    
    public static void drawElementsInstanced(final Mode mode, final int count, final IndexType type, final long offset, final int primCount) {
        final GLESCapabilities caps = GLES.getCapabilities();
        
        if (caps.GLES30) {
            GLES30.glDrawElementsInstanced(mode.value, count, type.value, offset, primCount);
        } else if (caps.GL_ANGLE_instanced_arrays) {
            ANGLEInstancedArrays.glDrawElementsInstancedANGLE(mode.value, count, type.value, offset, primCount);
        } else {
            throw new UnsupportedOperationException("drawElementsInstanced requires either OpenGLES 3.0 or GL_ANGLE_instanced_arrays.");
        }
    }
    
    public static void drawRangeElements(final Mode mode, final int start, final int end, final int count, final IndexType type, final long offset) {
        if (GLES.getCapabilities().GLES30) {
            GLES30.glDrawRangeElements(mode.value, start, end, count, type.value, offset);
        } else {
            throw new UnsupportedOperationException("drawRangeElements requires OpenGLES 3.0.");
        }
    }
    
    public void drawBuffers(int... attachments) {
        GLES30.glDrawBuffers(attachments);
    }
}
