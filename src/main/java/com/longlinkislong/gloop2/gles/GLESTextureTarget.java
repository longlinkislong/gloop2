/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public enum GLESTextureTarget {
    TEXTURE_2D(GLES20.GL_TEXTURE_2D),
    TEXTURE_3D(GLES30.GL_TEXTURE_3D);
    
    public final int value;
    
    private GLESTextureTarget(final int value) {
        this.value = value;
    }
    
    public static Optional<GLESTextureTarget> of(final int value) {
        return Arrays.stream(values())
                .filter(e -> e.value == value)
                .findFirst();
    }
}
