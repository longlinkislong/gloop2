/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

/**
 *
 * @author zmichaels
 */
public class GLESShaderInfo {
    public final GLESShaderType type;
    public final String src;
    
    public GLESShaderInfo(final GLESShaderType type, final String src) {
        this.type = type;
        this.src = src;
    }
    
    public GLESShaderInfo() {
        this(GLESShaderType.VERTEX, "");
    }
    
    public GLESShaderInfo withType(final GLESShaderType type) {
        return new GLESShaderInfo(type, src);
    }
    
    public GLESShaderInfo withSrc(final String src) {
        return new GLESShaderInfo(type, src);
    }
    
    public GLESShader create() {
        return new GLESShader(this);
    }
}
