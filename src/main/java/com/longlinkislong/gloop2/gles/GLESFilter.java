/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;

/**
 *
 * @author zmichaels
 */
public enum GLESFilter {
    NEAREST(GLES20.GL_NEAREST, true),
    LINEAR(GLES20.GL_LINEAR, true),
    NEAREST_MIPMAP_NEAREST(GLES20.GL_NEAREST_MIPMAP_NEAREST, false),
    LINEAR_MIPMAP_NEAREST(GLES20.GL_LINEAR_MIPMAP_NEAREST, false),
    NEAREST_MIPMAP_LINEAR(GLES20.GL_NEAREST_MIPMAP_LINEAR, false),
    LINEAR_MIPMAP_LINEAR(GLES20.GL_LINEAR_MIPMAP_LINEAR, false);
    
    public final int value;
    public final boolean magSupported;
    
    private GLESFilter(final int value, final boolean magSupported) {
        this.value = value;
        this.magSupported = magSupported;
    }
    
    public static Optional<GLESFilter> of(final int value) {
        return Arrays.stream(values())
                .filter(e -> e.value == value)
                .findFirst();
    }
}
