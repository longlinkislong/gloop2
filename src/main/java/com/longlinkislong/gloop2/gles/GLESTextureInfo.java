/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class GLESTextureInfo {

    public final int width;
    public final int height;
    public final int depth;
    public final int levelCount;
    public final GLESSamplerInfo samplerInfo;
    public final List<GLESPixelInfo> initialData;
    public final GLESInternalFormat format;

    public GLESTextureInfo(
            final int width, final int height, final int depth,
            final int levelCount,
            final GLESSamplerInfo samplerInfo,
            final List<GLESPixelInfo> data,
            final GLESInternalFormat format) {

        this.width = width;
        this.height = height;
        this.depth = depth;
        this.levelCount = levelCount;
        this.samplerInfo = samplerInfo;
        this.initialData = data;
        this.format = format;
    }
    
    public GLESTextureInfo() {
        this(0, 0, 0, 1, new GLESSamplerInfo(), null, GLESInternalFormat.RGBA);
    }
    
    public GLESTextureInfo withExtent(final int width, final int height, final int depth) {
        return new GLESTextureInfo(width, height, depth, levelCount, samplerInfo, initialData, format);
    }
    
    public GLESTextureInfo withSamplerInfo(final GLESSamplerInfo samplerInfo) {
        return new GLESTextureInfo(width, height, depth, levelCount, samplerInfo, initialData, format);
    }
    
    public GLESTextureInfo withInitialData(final List<GLESPixelInfo> initialData) {
        return new GLESTextureInfo(width, height, depth, levelCount, samplerInfo, initialData, format);
    }
    
    public GLESTextureInfo withFormat(final GLESInternalFormat format) {
        return new GLESTextureInfo(width, height, depth, levelCount, samplerInfo, initialData, format);
    }
    
    public GLESTextureInfo withData(final GLESPixelInfo... data) {
        return withInitialData(Arrays.asList(data));
    }
    
    public GLESTexture create() {
        return new GLESTexture(this);
    }
    
    public GLESTextureTarget target() {
        return this.depth > 1 ? GLESTextureTarget.TEXTURE_3D : GLESTextureTarget.TEXTURE_2D;
    }
}
