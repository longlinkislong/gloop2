/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public enum GLESInternalFormat {
    R8(GLES30.GL_R8, 8, 0, 0, 0, 0, 0),
    R8I(GLES30.GL_R8I, 8, 0, 0, 0, 0, 0),
    R8UI(GLES30.GL_R8UI, 8, 0, 0, 0, 0, 0),
    R8_SNORM(GLES30.GL_R8_SNORM, 8, 0, 0, 0, 0, 0),
    R16I(GLES30.GL_R16I, 16, 0, 0, 0, 0, 0),
    R16UI(GLES30.GL_R16UI, 16, 0, 0, 0, 0, 0),
    R16F(GLES30.GL_R16F, 16, 0, 0, 0, 0, 0),
    R32I(GLES30.GL_R32I, 32, 0, 0, 0, 0, 0),
    R32UI(GLES30.GL_R32UI, 32, 0, 0, 0, 0, 0),
    R32F(GLES30.GL_R32F, 32, 0, 0, 0, 0, 0),
    RG8(GLES30.GL_RG8, 8, 8, 0, 0, 0, 0),
    RG8I(GLES30.GL_RG8I, 8, 8, 0, 0, 0, 0),
    RG8UI(GLES30.GL_RG8UI, 8, 8, 0, 0, 0, 0),
    RG8_SNORM(GLES30.GL_RG8_SNORM, 8, 8, 0, 0, 0, 0),
    RG16I(GLES30.GL_RG16I, 16, 16, 0, 0, 0, 0),
    RG16UI(GLES30.GL_RG16UI, 16, 16, 0, 0, 0, 0),
    RG16F(GLES30.GL_RG16F, 16, 16, 0, 0, 0, 0),
    RG32I(GLES30.GL_RG32I, 32, 32, 0, 0, 0, 0),
    RG32UI(GLES30.GL_RG32UI, 32, 32, 0, 0, 0, 0),
    RG32F(GLES30.GL_RG32F, 32, 32, 0, 0, 0, 0),
    RGB(GLES20.GL_RGB, 8, 8, 8, 0, 0, 0),
    RGB5_A1(GLES20.GL_RGB5_A1, 5, 5, 5, 1, 0, 0),
    RG565(GLES20.GL_RGB565, 5, 6, 5, 0, 0, 0),
    RGB8(GLES30.GL_RGB8, 8, 8, 8, 0, 0, 0),
    RGB8I(GLES30.GL_RGB8I, 8, 8, 8, 0, 0, 0),
    RGB8UI(GLES30.GL_RGB8UI, 8, 8, 8, 0, 0, 0),
    RGB8_SNORM(GLES30.GL_RGB8_SNORM, 8, 8, 8, 0, 0, 0),
    RGB9_E5(GLES30.GL_RGB9_E5, 9, 9, 9, 5, 0, 0),
    RGB10_A2(GLES30.GL_RGB10_A2, 10, 10, 10, 2, 0, 0),
    RGB10_A2UI(GLES30.GL_RGB10_A2UI, 10, 10, 10, 2, 0, 0),
    RGB16I(GLES30.GL_RGB16I, 16, 16, 16, 0, 0, 0),
    RGB16UI(GLES30.GL_RGB16UI, 16, 16, 16, 0, 0, 0),
    RGB16F(GLES30.GL_RGB16F, 16, 16, 16, 0, 0, 0),
    RGB32I(GLES30.GL_RGB32I, 32, 32, 32, 0, 0, 0),
    RGB32UI(GLES30.GL_RGB32UI, 32, 32, 32, 0, 0, 0),
    RGB32F(GLES30.GL_RGB32F, 32, 32, 32, 0, 0, 0),
    SRGB8(GLES30.GL_SRGB8, 8, 8, 8, 0, 0, 0),
    RGBA(GLES20.GL_RGBA, 8, 8, 8, 8, 0, 0),
    RGBA4(GLES20.GL_RGBA4, 4, 4, 4, 4, 0, 0),
    RGBA8(GLES30.GL_RGBA8, 8, 8, 8, 8, 0, 0),
    RGBA8I(GLES30.GL_RGBA8I, 8, 8, 8, 8, 0, 0),
    RGBA8UI(GLES30.GL_RGBA8UI, 8, 8, 8, 8, 0, 0),
    RGBA8_SNORM(GLES30.GL_RGBA8_SNORM, 8, 8, 8, 8, 0, 0),
    RGBA16I(GLES30.GL_RGBA16I, 16, 16, 16, 16, 0, 0),
    RGBA16UI(GLES30.GL_RGBA16UI, 16, 16, 16, 16, 0, 0),
    RGBA16F(GLES30.GL_RGBA16F, 16, 16, 16, 16, 0, 0),
    RGBA32I(GLES30.GL_RGBA32I, 32, 32, 32, 32, 0, 0),
    RGBA32UI(GLES30.GL_RGBA32UI, 32, 32, 32, 32, 0, 0),
    RGBA32F(GLES30.GL_RGBA32F, 32, 32, 32, 32, 0, 0),
    SRGB8_ALPHA8(GLES30.GL_SRGB8_ALPHA8, 8, 8, 8, 8, 0, 0),
    R11F_G11F_B10F(GLES30.GL_R11F_G11F_B10F, 11, 11, 10, 0, 0, 0),
    DEPTH_COMPONENT16(GLES20.GL_DEPTH_COMPONENT16, 0, 0, 0, 0, 16, 0),
    DEPTH_COMPONENT24(GLES30.GL_DEPTH_COMPONENT24, 0, 0, 0, 0, 24, 0),
    DEPTH_COMPONENT32F(GLES30.GL_DEPTH_COMPONENT32F, 0, 0, 0, 0, 32, 0),
    DEPTH24_STENCIL8(GLES30.GL_DEPTH24_STENCIL8, 0, 0, 0, 0, 24, 8),
    DEPTH32F_STENCIL8(GLES30.GL_DEPTH32F_STENCIL8, 0, 0, 0, 0, 32, 8);

    public final int value;
    public final int redBits, greenBits, blueBits, alphaBits;
    public final int depthBits, stencilBits;

    public boolean isColor() {
        return (redBits > 0 || greenBits > 0 || blueBits > 0 || alphaBits > 0);
    }

    public boolean isDepth() {
        return this.depthBits > 0;
    }

    public boolean isStencil() {
        return this.stencilBits > 0;
    }

    public boolean isDepthStencil() {
        return this.isDepth() && this.isStencil();
    }

    private GLESInternalFormat(final int value,
            final int redBits, final int greenBits, final int blueBits, final int alphaBits,
            final int depthBits, final int stencilBits) {

        this.value = value;
        this.redBits = redBits;
        this.greenBits = greenBits;
        this.blueBits = blueBits;
        this.alphaBits = alphaBits;
        this.depthBits = depthBits;
        this.stencilBits = stencilBits;
    }

    public static Optional<GLESInternalFormat> of(final int value) {
        return Arrays.stream(values())
                .filter(e -> e.value == value)
                .findFirst();
    }
}
