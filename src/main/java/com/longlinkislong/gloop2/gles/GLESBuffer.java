/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.nio.ByteBuffer;
import java.util.Set;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESBuffer extends GLESObject<GLESBufferInfo, GLESBuffer> {    
    
    public GLESBuffer() {}
    
    public GLESBuffer(final GLESBufferInfo info) {
        this.info = info;        
        this.handle = GLES20.glGenBuffers();
        
        GLES20.glBindBuffer(info.target.value, handle);
        
        if (info.initialData != null) {            
            assert info.size == info.initialData.remaining();
            
            GLES20.glBufferData(info.target.value, info.initialData, info.usage.value);            
        } else {
            GLES20.glBufferData(info.target.value, info.size, info.usage.value);
        }
        
        GLES20.glBindBuffer(info.target.value, 0);
    }
    
    public void bindRange(final GLESBufferTarget target, final int index, final long offset, final long size) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESBuffer is not valid!");
        }
        
        switch (target) {
            case TRANSFORM_FEEDBACK:
            case UNIFORM:
                break;
            default:
                throw new IllegalArgumentException("Only TRANSFORM_FEEDBACK and UNIFORM are supported!");
        }
                
        GLES30.glBindBufferRange(target.value, index, this.handle, offset, size);
    }
    
    public void bindRange(final int index, final long offset, final long size) {
        bindRange(this.info.target, index, offset, size);
    }
    
    public void bindBase(final int index) {
        bindBase(this.info.target, index);
    }
    
    public void bindBase(final GLESBufferTarget target, final int index) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESBuffer is not valid!");
        }
        
        switch (target) {
            case TRANSFORM_FEEDBACK:
            case UNIFORM:
                break;
            default:
                throw new IllegalArgumentException("Only TRANSFORM_FEEDBACK and UNIFORM are supported!");
        }
        
        GLES30.glBindBufferBase(target.value, index, this.handle);
    }
    
    public void bind() {
        this.bind(this.info.target);
    }
    
    public void bind(final GLESBufferTarget target) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESBuffer is not valid!");
        }
        
        GLES20.glBindBuffer(target.value, handle);
    }
    
    public void subData(final long offset, final ByteBuffer data) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESBuffer is not valid!");
        }
        
        GLES20.glBindBuffer(this.info.target.value, this.handle);
        GLES20.glBufferSubData(this.info.target.value, offset, data);
        GLES20.glBindBuffer(this.info.target.value, 0);
    }
    
    public static final class MappedBuffer implements AutoCloseable {
        GLESBuffer host;
        ByteBuffer ptr;
        
        private MappedBuffer(final GLESBuffer buffer, final long offset, final long size, final Set<GLESBufferAccess> access) {
            this.host = buffer;
            
            GLES20.glBindBuffer(buffer.info.target.value, buffer.handle);
            this.ptr = GLES30.glMapBufferRange(buffer.info.target.value, offset, size, GLESBufferAccess.bits(access));
            GLES20.glBindBuffer(buffer.info.target.value, 0);
        }
        
        public ByteBuffer getPointer() {
            return this.ptr;
        }
        
        public void flushRange(long offset, long size) {
            if (!this.isValid()) {
                throw new IllegalStateException("MappedBuffer data is no longer valid!");
            }
            
            GLES20.glBindBuffer(host.info.target.value, host.handle);
            GLES30.glFlushMappedBufferRange(host.info.target.value, offset, size);
            GLES20.glBindBuffer(host.info.target.value, 0);
        }
        
        public void write(final ByteBuffer data) {
            if (!this.isValid()) {
                throw new IllegalStateException("MappedBuffer data is no longer valid!");
            }
            
            this.ptr.put(ptr);
        }
        
        public void read(final byte[] data, final int off, final int len) {
            if (!this.isValid()) {
                throw new IllegalStateException("MappedBuffer data is no longer valid!");
            }
            
            this.ptr.get(data, off, len);
        }
        
        public boolean isValid() {
            return this.host != null && this.host.isValid();
        }                

        @Override
        public void close() {
            if (this.isValid()) {
                GLES20.glBindBuffer(host.info.target.value, host.handle);
                GLES30.glUnmapBuffer(host.info.target.value);
                GLES20.glBindBuffer(host.info.target.value, host.handle);
                
                this.host = null;
                this.ptr = null;
            }
        }
    }        

    public MappedBuffer map(final long offset, final long size, final Set<GLESBufferAccess> access) {
        return new MappedBuffer(this, offset, size, access);
    }        
    
    @Override
    public void close() {
        if (this.isValid()) {
            GLES20.glDeleteBuffers(this.handle);
            this.handle = 0;
            this.info = null;
        }
    }
    
    public static void copySubData(final GLESBuffer src, final GLESBuffer dst, final long srcOff, final long dstOff, final long size) {
        if (!src.isValid()) {
            throw new IllegalStateException("Source buffer is not valid!");
        } else if (!dst.isValid()) {
            throw new IllegalStateException("Destination buffer is not valid!");
        }
        
        GLES20.glBindBuffer(GLES30.GL_COPY_READ_BUFFER, src.handle);
        GLES20.glBindBuffer(GLES30.GL_COPY_WRITE_BUFFER, dst.handle);
        
        GLES30.glCopyBufferSubData(GLES30.GL_COPY_READ_BUFFER, GLES30.GL_COPY_WRITE_BUFFER, srcOff, dstOff, size);
        
        GLES20.glBindBuffer(GLES30.GL_COPY_WRITE_BUFFER, 0);
        GLES20.glBindBuffer(GLES30.GL_COPY_READ_BUFFER, 0);
    }
    
    public void invalidate() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESBuffer is not valid!");
        }
        
        GLES20.glBindBuffer(this.info.target.value, this.handle);
        GLES20.glBufferData(this.info.target.value, this.info.size, this.info.usage.value);
        GLES20.glBindBuffer(this.info.target.value, 0);
    }
    
    public static final GLESBuffer DEFAULT_BUFFER = new GLESBuffer() {                  
        @Override
        public void bind(GLESBufferTarget tgt) {
            GLES20.glBindBuffer(tgt.value, 0);
        }
        
        @Override
        public void bindBase(final GLESBufferTarget tgt, final int index) {
            GLES30.glBindBufferBase(tgt.value, index, 0);
        }
        
        @Override
        public void bindRange(GLESBufferTarget tgt, int index, long offset, long size) {
            GLES30.glBindBufferRange(tgt.value, index, 0, offset, size);
        }
        
        @Override
        public void close() {
            if (this.handle != 0) {
                GLES20.glDeleteBuffers(this.handle);                
            }
            
            this.handle = 0;
            this.info = null;
        }
    };
}
