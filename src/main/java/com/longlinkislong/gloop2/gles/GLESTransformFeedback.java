/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gles;

import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.opengles.GLES20;
import org.lwjgl.opengles.GLES30;

/**
 *
 * @author zmichaels
 */
public class GLESTransformFeedback extends GLESObject<Object, GLESTransformFeedback> {

    public static enum PrimitiveMode {
        TRIANGLES(GLES20.GL_TRIANGLES),
        LINES(GLES20.GL_LINES),
        POINTS(GLES20.GL_POINTS);

        public final int value;

        private PrimitiveMode(final int value) {
            this.value = value;
        }

        public static Optional<PrimitiveMode> of(final int value) {
            return Arrays.stream(values())
                    .filter(e -> e.value == value)
                    .findFirst();
        }
    }

    public GLESTransformFeedback() {
        this.handle = GLES30.glGenTransformFeedbacks();
    }

    @Override
    public void close() {
        if (this.isValid()) {
            GLES30.glDeleteTransformFeedbacks(this.handle);
            this.handle = 0;
        }
    }

    public static final class TransformTask implements AutoCloseable {

        private TransformTask(final PrimitiveMode primitiveMode) {
            GLES30.glBeginTransformFeedback(primitiveMode.value);
        }

        @Override
        public void close() {
            GLES30.glEndTransformFeedback();
        }
    }
    
    public void bind() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESTransformFeedback is not valid!");
        }
        
        GLES30.glBindTransformFeedback(GLES30.GL_TRANSFORM_FEEDBACK, this.handle);
    }
    
    public void pause() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESTransformFeedback is not valid!");
        }
        
        GLES30.glPauseTransformFeedback();
    }
    
    public void resume() {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESTransformFeedback is not valid!");
        }
        
        GLES30.glResumeTransformFeedback();
    }

    public TransformTask begin(final PrimitiveMode primitiveMode) {
        if (!this.isValid()) {
            throw new IllegalStateException("GLESTransformFeedback is not valid!");
        }

        return new TransformTask(primitiveMode);
    }
}
