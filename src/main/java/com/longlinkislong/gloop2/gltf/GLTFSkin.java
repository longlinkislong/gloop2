/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import com.longlinkislong.gloop2.Tools;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public final class GLTFSkin {
    public final String name;
    public final WeakReference<GLTFAccessor> inverseBindMatrices;
    public final WeakReference<GLTFNode> skeleton;
    public final List<Integer> joints;
    
    public GLTFSkin(
            final String name, final WeakReference<GLTFAccessor> inverseBindMatrices,
            final WeakReference<GLTFNode> skeleton, final List<Integer> joints) {
        
        this.name = name;
        this.inverseBindMatrices = inverseBindMatrices;
        this.skeleton = skeleton;
        this.joints = Tools.copyList(joints);
    }
    
    public GLTFSkin() {
        this("", null, null, null);
    }
    
    public GLTFSkin withName(final String name) {
        return new GLTFSkin(name, inverseBindMatrices, skeleton, joints);
    }
    
    public GLTFSkin withInverseBindMatrices(final GLTFAccessor inverseBindMatrices) {
        return new GLTFSkin(name, new WeakReference<>(inverseBindMatrices), skeleton, joints);
    }
    
    public GLTFSkin withSkeleton(final GLTFNode skeleton) {
        return new GLTFSkin(name, inverseBindMatrices, new WeakReference<>(skeleton), joints);
    }
    
    public GLTFSkin withJoints(final List<Integer> joints) {
        return new GLTFSkin(name, inverseBindMatrices, skeleton, joints);
    }
    
    public GLTFSkin withJoints(final Integer... joints) {
        return withJoints(Arrays.asList(joints));
    }
        
}
