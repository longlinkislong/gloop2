/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public final class GLTFSampler {
    public static enum MagFilter implements CEnum {
        NEAREST(9728),
        LINEAR(9729);
        
        public final int value;
        
        private MagFilter(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
        
        public static Optional<MagFilter> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }
    }
    
    public static enum MinFilter implements CEnum {
        NEAREST(9728),
        LINEAR(9729),
        NEAREST_MIPMAP_NEAREST(9984),
        LINEAR_MIPMAP_NEAREST(9985),
        NEAREST_MIPMAP_LINEAR(9986),
        LINEAR_MIPMAP_LINEAR(9987);
        
        public final int value;
        
        @Override
        public int getValue() {
            return this.value;
        }
        
        private MinFilter(final int value) {
            this.value = value;
        }
        
        public static Optional<MinFilter> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }
    }
    
    public static enum WrapMode implements CEnum {
        CLAMP_TO_EDGE(33071),
        MIRRORED_REPEAT(33648),
        REPEAT(10497);
        
        public final int value;
        
        private WrapMode(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
        
        public static Optional<WrapMode> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }
    }
    
    public final String name;
    public final MagFilter magFilter;
    public final MinFilter minFilter;
    public final WrapMode wrapS;
    public final WrapMode wrapT;
    
    public GLTFSampler(
            final String name, 
            final MagFilter magFilter, final MinFilter minFilter, 
            final WrapMode wrapS, final WrapMode wrapT) {
     
        this.name = name;
        this.magFilter = magFilter;
        this.minFilter = minFilter;
        this.wrapS = wrapS;
        this.wrapT = wrapT;
    }
    
    public GLTFSampler() {
        this("", null, null, WrapMode.REPEAT, WrapMode.REPEAT);
    }
    
    public GLTFSampler withName(final String name) {
        return new GLTFSampler(name, magFilter, minFilter, wrapS, wrapT);
    }
    
    public GLTFSampler withMagFilter(final MagFilter magFilter) {
        return new GLTFSampler(name, magFilter, minFilter, wrapS, wrapT);
    }
    
    public GLTFSampler withMinFilter(final MinFilter minFilter) {
        return new GLTFSampler(name, magFilter, minFilter, wrapS, wrapT);
    }
    
    public GLTFSampler withWrapS(final WrapMode wrapS) {
        return new GLTFSampler(name, magFilter, minFilter, wrapS, wrapT);
    }
    
    public GLTFSampler withWrapT(final WrapMode wrapT) {
        return new GLTFSampler(name, magFilter, minFilter, wrapS, wrapT);
    }
    
}
