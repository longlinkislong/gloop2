/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

/**
 *
 * @author zmichaels
 */
public final class GLTFMaterial {
    public static final class PBRMetallicRoughness {
        public final float[] baseColorFactor;
        public final GLTFTextureInfo baseColorTexture;
        public final float metallicFactor;
        public final float roughnessFactor;
        public final GLTFTextureInfo metallicRoughnessTexture;
        
        public PBRMetallicRoughness(
                final float[] baseColorFactor, 
                final GLTFTextureInfo baseColorTexture, 
                final float metallicFactor, final float roughnessFactor,
                final GLTFTextureInfo metallicRoughnessTexture) {
            
            this.baseColorFactor = baseColorFactor;
            this.baseColorTexture = baseColorTexture;
            this.metallicFactor = metallicFactor;
            this.roughnessFactor = roughnessFactor;
            this.metallicRoughnessTexture = metallicRoughnessTexture;
        }
        
        public PBRMetallicRoughness() {
            this(new float[]{1F, 1F, 1F, 1F}, null, 1F, 1F, null);
        }
        
        public PBRMetallicRoughness withBaseColorFactor(final float[] baseColorFactor) {
            return new PBRMetallicRoughness(baseColorFactor, baseColorTexture, metallicFactor, roughnessFactor, metallicRoughnessTexture);
        }        
        
        public PBRMetallicRoughness withBaseColorTexture(final GLTFTextureInfo baseColorTexture) {
            return new PBRMetallicRoughness(baseColorFactor, baseColorTexture, metallicFactor, roughnessFactor, metallicRoughnessTexture);
        }
        
        public PBRMetallicRoughness withMetallicFactor(final float metallicFactor) {
            return new PBRMetallicRoughness(baseColorFactor, baseColorTexture, metallicFactor, roughnessFactor, metallicRoughnessTexture);
        }
        
        public PBRMetallicRoughness withRoughnessFactor(final float roughnessFactor) {
            return new PBRMetallicRoughness(baseColorFactor, baseColorTexture, metallicFactor, roughnessFactor, metallicRoughnessTexture);
        }
        
        public PBRMetallicRoughness withMetallicRoughnessTexture(final GLTFTextureInfo metallicRoughnessTexture) {
            return new PBRMetallicRoughness(baseColorFactor, baseColorTexture, metallicFactor, roughnessFactor, metallicRoughnessTexture);
        }        
    }
    
    public static final class NormalTextureInfo {
        public final WeakReference<GLTFTexture> texture;
        public final int texCoord;
        public final float scale;
        
        public NormalTextureInfo(final WeakReference<GLTFTexture> texture, final int texCoord, final float scale) {
            this.texture = texture;
            this.texCoord = texCoord;
            this.scale = scale;
        }
        
        public NormalTextureInfo(final GLTFTextureInfo baseInfo, final float scale) {
            this.texture = baseInfo.texture;
            this.texCoord = baseInfo.texCoord;
            this.scale = scale;
        }
        
        public NormalTextureInfo() {
            this(null, 0, 1F);
        }
        
        public NormalTextureInfo withTexture(final GLTFTexture texture) {
            return new NormalTextureInfo(new WeakReference<>(texture), texCoord, scale);
        }
        
        public NormalTextureInfo withTexCoord(final int texCoord) {
            return new NormalTextureInfo(texture, texCoord, scale);
        }
        
        public NormalTextureInfo withScale(final float scale) {
            return new NormalTextureInfo(texture, texCoord, scale);
        }
    }
    
    public static final class OcclusionTextureInfo {
        public final WeakReference<GLTFTexture> texture;
        public final int texCoord;
        public final float strength;
        
        public OcclusionTextureInfo(final WeakReference<GLTFTexture> texture, final int texCoord, final float strength) {
            this.texture = texture;
            this.texCoord = texCoord;
            this.strength = strength;
        }
        
        public OcclusionTextureInfo(final GLTFTextureInfo baseInfo, final float strength) {
            this.texture = baseInfo.texture;
            this.texCoord = baseInfo.texCoord;
            this.strength = strength;
        }
        
        public OcclusionTextureInfo withTexture(final GLTFTexture texture) {
            return new OcclusionTextureInfo(new WeakReference<>(texture), texCoord, strength);
        }
        
        public OcclusionTextureInfo withTexCoord(final int texCoord) {
            return new OcclusionTextureInfo(texture, texCoord, strength);
        }
        
        public OcclusionTextureInfo withStrength(final float strength) {
            return new OcclusionTextureInfo(texture, texCoord, strength);
        }
    }
    
    public static enum AlphaMode {
        OPAQUE,
        MASK,
        BLEND
    }
    
    public final String name;
    public final PBRMetallicRoughness pbrMetallicRoughness;
    public final NormalTextureInfo normalTexture;
    public final OcclusionTextureInfo occlusionTexture;
    public final GLTFTextureInfo emissiveTexture;
    public final float[] emissiveFactor;
    public final AlphaMode alphaMode;
    public final float alphaCutoff;
    public final boolean doubleSided;
    
    public GLTFMaterial(
            final String name, final PBRMetallicRoughness pbrMetallicRoughness,
            final NormalTextureInfo normalTexture, 
            final OcclusionTextureInfo occlusionTexture,
            final GLTFTextureInfo emissiveTexture,
            final float[] emissiveFactor,
            final AlphaMode alphaMode, final float alphaCutoff, final boolean doubleSided) {
     
        this.name = name;
        this.pbrMetallicRoughness = pbrMetallicRoughness;
        this.normalTexture = normalTexture;
        this.occlusionTexture = occlusionTexture;
        this.emissiveTexture = emissiveTexture;
        this.emissiveFactor = emissiveFactor;
        this.alphaMode = alphaMode;
        this.alphaCutoff = alphaCutoff;
        this.doubleSided = doubleSided;
    }
    
    public GLTFMaterial() {
        this(null, null, null, null, null, new float[] {0F, 0F, 0F}, AlphaMode.OPAQUE, 0.5F, false);
    }
    
    public GLTFMaterial withName(final String name) {
        return new GLTFMaterial(name, pbrMetallicRoughness, normalTexture, occlusionTexture, emissiveTexture, emissiveFactor, alphaMode, alphaCutoff, doubleSided);
    }
    
    public GLTFMaterial withPBRMetallicRoughness(final PBRMetallicRoughness pbrMetallicRoughness) {
        return new GLTFMaterial(name, pbrMetallicRoughness, normalTexture, occlusionTexture, emissiveTexture, emissiveFactor, alphaMode, alphaCutoff, doubleSided);
    }
    
    public GLTFMaterial withNormalTexture(final NormalTextureInfo normalTexture) {
        return new GLTFMaterial(name, pbrMetallicRoughness, normalTexture, occlusionTexture, emissiveTexture, emissiveFactor, alphaMode, alphaCutoff, doubleSided);
    }
    
    public GLTFMaterial withOcclusionTexture(final OcclusionTextureInfo occlusionTexture) {
        return new GLTFMaterial(name, pbrMetallicRoughness, normalTexture, occlusionTexture, emissiveTexture, emissiveFactor, alphaMode, alphaCutoff, doubleSided);
    }
    
    public GLTFMaterial withEmissiveTexture(final GLTFTextureInfo emissiveTexture) {
        return new GLTFMaterial(name, pbrMetallicRoughness, normalTexture, occlusionTexture, emissiveTexture, emissiveFactor, alphaMode, alphaCutoff, doubleSided);
    }
    
    public GLTFMaterial withEmissiveFactor(final float[] emissiveFactor) {
        return new GLTFMaterial(name, pbrMetallicRoughness, normalTexture, occlusionTexture, emissiveTexture, emissiveFactor, alphaMode, alphaCutoff, doubleSided);
    }
    
    public GLTFMaterial withAlphaMode(final AlphaMode alphaMode) {
        return new GLTFMaterial(name, pbrMetallicRoughness, normalTexture, occlusionTexture, emissiveTexture, emissiveFactor, alphaMode, alphaCutoff, doubleSided);
    }
    
    public GLTFMaterial withAlphaCutoff(final float alphaCutoff) {
        return new GLTFMaterial(name, pbrMetallicRoughness, normalTexture, occlusionTexture, emissiveTexture, emissiveFactor, alphaMode, alphaCutoff, doubleSided);
    }
    
    public GLTFMaterial withDoubleSided(final boolean doubleSided) {
        return new GLTFMaterial(name, pbrMetallicRoughness, normalTexture, occlusionTexture, emissiveTexture, emissiveFactor, alphaMode, alphaCutoff, doubleSided);
    }
}
