/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import com.longlinkislong.gloop2.Tools;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author zmichaels
 */
public final class GLTFScene {

    public final String name;
    public final List<WeakReference<GLTFNode>> nodes;

    public GLTFScene(final String name, final List<WeakReference<GLTFNode>> nodes) {
        this.name = name;
        this.nodes = Tools.copyList(nodes);
    }
    
    public GLTFScene() {
        this(null, null);
    }
    
    public GLTFScene withName(final String name) {
        return new GLTFScene(name, nodes);
    }
    
    public GLTFScene withNodes(final List<GLTFNode> nodes) {
        final List<WeakReference<GLTFNode>> ref = nodes.stream()
                .map(WeakReference::new)
                .collect(Collectors.toList());
        
        return new GLTFScene(name, ref);
    }
    
    public GLTFScene withNodes(final GLTFNode... nodes) {
        return withNodes(Arrays.asList(nodes));
    }
}
