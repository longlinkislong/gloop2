/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public final class GLTFBufferView {

    public static enum Target implements CEnum {
        ARRAY(34962),
        ELEMENT(34963);
        public final int value;

        private Target(final int value) {
            this.value = value;
        }

        @Override
        public int getValue() {
            return this.value;
        }

        public static Optional<Target> of(final int value) {
            return Tools.valueOf(value, Arrays.stream(values()));
        }
    }
    
    public final String name;
    public final WeakReference<GLTFBuffer> buffer;
    public final int byteOffset;
    public final int byteLength;
    public final int byteStride;
    public final Target target;
    
    public GLTFBufferView(
            final String name,
            final WeakReference<GLTFBuffer> buffer, final int byteOffset, 
            final int byteLength, final int byteStride, final Target target) {
        
        this.name = name;
        this.buffer = buffer;
        this.byteOffset = byteOffset;
        this.byteLength = byteLength;
        this.byteStride = byteStride;
        this.target = target;
    }
    
    public GLTFBufferView() {
        this(null, null, 0, 0, 0, null);
    }
    
    public GLTFBufferView withName(final String name) {
        return new GLTFBufferView(name, buffer, byteOffset, byteLength, byteStride, target);
    }
    
    public GLTFBufferView withBuffer(final GLTFBuffer buffer) {
        return new GLTFBufferView(name, new WeakReference<>(buffer), byteOffset, byteLength, byteStride, target);
    }
    
    public GLTFBufferView withByteOffset(final int byteOffset) {
        return new GLTFBufferView(name, buffer, byteOffset, byteLength, byteStride, target);
    }
    
    public GLTFBufferView withByteLength(final int byteLength) {
        return new GLTFBufferView(name, buffer, byteOffset, byteLength, byteStride, target);
    }
    
    public GLTFBufferView withByteStride(final int byteStride) {
        return new GLTFBufferView(name, buffer, byteOffset, byteLength, byteStride, target);
    }
    
    public GLTFBufferView withTarget(final Target target) {
        return new GLTFBufferView(name, buffer, byteOffset, byteLength, byteStride, target);
    }    
}
