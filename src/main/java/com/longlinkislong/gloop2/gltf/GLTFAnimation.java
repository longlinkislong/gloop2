/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import com.longlinkislong.gloop2.Tools;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class GLTFAnimation {

    public static final class Channel {

        public static final class Target {

            public static enum Path {
                TRANSLATION("translation"),
                ROTATION("rotation"),
                SCALE("scale"),
                WEIGHTS("weights");
                public final String value;

                private Path(final String value) {
                    this.value = value;
                }
            }

            public final WeakReference<GLTFNode> node;
            public final Path path;

            public Target(final WeakReference<GLTFNode> node, final Path path) {
                this.node = node;
                this.path = path;
            }

            public Target() {
                this(null, Path.TRANSLATION);
            }

            public Target withNode(final GLTFNode node) {
                return new Target(new WeakReference<>(node), path);
            }

            public Target withPath(final Path path) {
                return new Target(node, path);
            }
        }

        public final Sampler sampler;
        public final Target target;

        public Channel(final Target target, final Sampler sampler) {
            this.target = target;
            this.sampler = sampler;
        }

        public Channel() {
            this(null, null);
        }

        public Channel withTarget(final Target target) {
            return new Channel(target, sampler);
        }

        public Channel withSampler(final Sampler sampler) {
            return new Channel(target, sampler);
        }
    }

    public static final class Sampler {

        public static enum Interpolation {
            LINEAR,
            STEP,
            CATMULROMSPLINE,
            CUBICSPLINE
        }

        public final WeakReference<GLTFAccessor> input;
        public final Interpolation interpolation;
        public final Reference<GLTFAccessor> output;

        public Sampler(final WeakReference<GLTFAccessor> inputId, final Interpolation interpolation, final Reference<GLTFAccessor> outputId) {
            this.input = inputId;
            this.interpolation = interpolation;
            this.output = outputId;
        }

        public Sampler() {
            this(null, null, null);
        }

        public Sampler withInput(final GLTFAccessor input) {
            return new Sampler(new WeakReference<>(input), interpolation, output);
        }

        public Sampler withOutput(final GLTFAccessor outputId) {
            return new Sampler(input, interpolation, new WeakReference<>(outputId));
        }

        public Sampler withInterpolation(final Interpolation interpolation) {
            return new Sampler(input, interpolation, output);
        }
    }

    public final String name;
    public final List<Channel> channels;
    public final List<Sampler> samplers;

    public GLTFAnimation(
            final String name,
            final List<Channel> channels, final List<Sampler> samplers) {

        this.name = name;
        this.channels = Tools.copyList(channels);
        this.samplers = Tools.copyList(samplers);
    }

    public GLTFAnimation() {
        this(null, null, null);
    }
    
    public GLTFAnimation withName(final String name) {
        return new GLTFAnimation(name, channels, samplers);
    }

    public GLTFAnimation withChannels(final List<Channel> channels) {
        return new GLTFAnimation(name, channels, samplers);
    }

    public GLTFAnimation withChannels(final Channel... channels) {
        return withChannels(Arrays.asList(channels));
    }

    public GLTFAnimation withSamplers(final List<Sampler> samplers) {
        return new GLTFAnimation(name, channels, samplers);
    }

    public GLTFAnimation withSamplers(final Sampler... samplers) {
        return withSamplers(Arrays.asList(samplers));
    }
}
