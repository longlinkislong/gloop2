/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public final class GLTFMesh {

    public static final class Primitive {

        public static enum Mode implements CEnum {
            POINTS(0),
            LINES(1),
            LINE_LOOP(2),
            LINE_STRIP(3),
            TRIANGLES(4),
            TRIANGLE_STRIP(5),
            TRIANGLE_FAN(6);

            public final int value;

            private Mode(final int value) {
                this.value = value;
            }

            @Override
            public int getValue() {
                return this.value;
            }

            public static Optional<Mode> of(final int value) {
                return Tools.valueOf(value, Arrays.stream(values()));
            }
        }

        public final Map<String, WeakReference<GLTFAccessor>> attributes;
        public final WeakReference<GLTFAccessor> indices;
        public final WeakReference<GLTFMaterial> material;
        public final Mode mode;
        public final List<Map<String, WeakReference<GLTFAccessor>>> targets;

        public Primitive(
                final Map<String, WeakReference<GLTFAccessor>> attributes,
                final WeakReference<GLTFAccessor> indices,
                final WeakReference<GLTFMaterial> material,
                final Mode mode,
                final List<Map<String, WeakReference<GLTFAccessor>>> targets) {

            this.attributes = Tools.copyMap(attributes);
            this.indices = indices;
            this.material = material;
            this.mode = mode;
            this.targets = Tools.copyList(targets);
        }

        public Primitive() {
            this(null, null, null, Mode.TRIANGLES, null);
        }

        public Primitive withAttributes(final Map<String, GLTFAccessor> attributes) {
            final Map<String, WeakReference<GLTFAccessor>> ref = new HashMap<>();
            
            attributes.forEach((key, value) -> ref.put(key, new WeakReference<>(value)));
            
            return new Primitive(ref, indices, material, mode, targets);
        }

        public Primitive withIndices(final GLTFAccessor indices) {
            return new Primitive(attributes, new WeakReference<>(indices), material, mode, targets);
        }

        public Primitive withMaterial(final GLTFMaterial material) {
            return new Primitive(attributes, indices, new WeakReference<>(material), mode, targets);
        }

        public Primitive withMode(final Mode mode) {
            return new Primitive(attributes, indices, material, mode, targets);
        }

        public Primitive withTargets(final List<Map<String, GLTFAccessor>> targets) {
            final List<Map<String, WeakReference<GLTFAccessor>>> ref = new ArrayList<>();
            
            targets.forEach(tgt -> {
                final Map<String, WeakReference<GLTFAccessor>> rtgt = new HashMap<>();
                
                tgt.forEach((key, value) -> rtgt.put(key, new WeakReference<>(value)));
                
                ref.add(rtgt);
            });
            
            return new Primitive(attributes, indices, material, mode, ref);
        }

        public Primitive withTargets(final Map<String, GLTFAccessor>... targets) {
            return withTargets(Arrays.asList(targets));
        }
    }

    public final String name;
    public final List<Primitive> primitives;
    public final float[] weights;

    public GLTFMesh(final String name, final List<Primitive> primitives, final float[] weights) {
        this.name = name;
        this.primitives = Tools.copyList(primitives);
        this.weights = weights;
    }

    public GLTFMesh() {
        this("", null, null);
    }

    public GLTFMesh withName(final String name) {
        return new GLTFMesh(name, primitives, weights);
    }

    public GLTFMesh withPrimitives(final List<Primitive> primitives) {
        return new GLTFMesh(name, primitives, weights);
    }

    public GLTFMesh withPrimitives(final Primitive... primitives) {
        return withPrimitives(Arrays.asList(primitives));
    }

    public GLTFMesh withWeights(final float[] weights) {
        return new GLTFMesh(name, primitives, weights);
    }
}
