/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import java.lang.ref.WeakReference;

/**
 *
 * @author zmichaels
 */
public final class GLTFTexture {
    public final String name;
    public final WeakReference<GLTFSampler> sampler;
    public final WeakReference<GLTFImage> source;
    
    public GLTFTexture(final String name, 
            final WeakReference<GLTFSampler> sampler, 
            final WeakReference<GLTFImage> source) {
        
        this.name = name;
        this.sampler = sampler;
        this.source = source;
    }
    
    public GLTFTexture withName(final String name) {
        return new GLTFTexture(name, sampler, source);
    }
    
    public GLTFTexture withSampler(final GLTFSampler sampler) {
        return new GLTFTexture(name, new WeakReference<>(sampler), source);
    }
    
    public GLTFTexture withSource(final GLTFImage source) {
        return new GLTFTexture(name, sampler, new WeakReference<>(source));
    }
}
