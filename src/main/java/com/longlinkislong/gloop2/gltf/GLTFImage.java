/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

/**
 *
 * @author zmichaels
 */
public final class GLTFImage {

    public static enum MimeType {
        JPEG("image/jpeg"),
        PNG("image/png");
        
        public final String value;

        private MimeType(final String value) {
            this.value = value;
        }

    }
    
    public final String name;
    public final String uri;
    public final MimeType mimeType;
    public final WeakReference<GLTFBufferView> bufferView;
    
    public GLTFImage(
            final String name,
            final String uri, 
            final MimeType mimeType, final WeakReference<GLTFBufferView> bufferView) {
        
        this.name = name;
        this.uri = uri;
        this.mimeType = mimeType;
        this.bufferView = bufferView;
    }
    
    public GLTFImage() {
        this(null, null, null, null);
    }
    
    public GLTFImage withName(final String name) {
        return new GLTFImage(name, uri, mimeType, bufferView);
    }
    
    public GLTFImage withURI(final String uri) {
        return new GLTFImage(name, uri, mimeType, bufferView);
    }
    
    public GLTFImage withMimeType(final MimeType mimeType) {
        return new GLTFImage(name, uri, mimeType, bufferView);
    }
    
    public GLTFImage withBufferView(final GLTFBufferView bufferView) {
        return new GLTFImage(name, uri, mimeType, new WeakReference<>(bufferView));
    }
    
    public GLTFImage validate() {
        return this;
    }
}
