/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public final class GLTFAccessor {

    public static enum ComponentType implements CEnum {
        BYTE(5120, 1),
        UNSIGNED_BYTE(5121, 1),
        SHORT(5122, 2),
        UNSIGNED_SHORT(5123, 2),
        UNSIGNED_INT(5125, 4),
        FLOAT(5126, 4);

        public final int value;
        public final int bytes;

        private ComponentType(final int value, final int bytes) {
            this.value = value;
            this.bytes = bytes;
        }

        @Override
        public int getValue() {
            return this.value;
        }

        public static Optional<ComponentType> of(final int cenum) {
            return Tools.valueOf(cenum, Arrays.stream(values()));
        }
    }

    public static enum Type {
        SCALAR(1),
        VEC2(2),
        VEC3(3),
        VEC4(4),
        MAT2(4),
        MAT3(9),
        MAT4(16);

        public final int components;

        private Type(final int components) {
            this.components = components;
        }
    }
    
    public static long elementSize(final ComponentType componentType, final Type type) {
        return type.components * componentType.bytes;
    }
    
    public final String name;
    public final WeakReference<GLTFBufferView> bufferView;
    public final int byteOffset;
    public final ComponentType componentType;
    public final boolean normalized;
    public final int count;
    public final Type type;
    public final List<Number> max;
    public final List<Number> min;
    
    public GLTFAccessor(
            final String name,
            final WeakReference<GLTFBufferView> bufferView, final int byteOffset, 
            final ComponentType componentType, final boolean normalized,
            final int count, final Type type, final List<Number> max,
            final List<Number> min) {                
        
        this.name = name;
        this.bufferView = bufferView;
        this.byteOffset = byteOffset;
        this.componentType = componentType;
        this.normalized = normalized;
        this.count = count;
        this.type = type;
        this.max = max == null ? null : Tools.copyList(max);
        this.min = min == null ? null : Tools.copyList(min);
    }
    
    public GLTFAccessor() {
        this(null, null, 0, null, false, 1, null, null, null);
    }
    
    public GLTFAccessor withName(final String name) {
        return new GLTFAccessor(name, bufferView, byteOffset, componentType, normalized, count, type, max, min);
    }
    
    public GLTFAccessor withBufferView(final GLTFBufferView bufferView) {
        return new GLTFAccessor(name, new WeakReference<>(bufferView), byteOffset, componentType, normalized, count, type, max, min);
    }
    
    public GLTFAccessor withByteOffset(final int byteOffset) {
        return new GLTFAccessor(name, bufferView, byteOffset, componentType, normalized, count, type, max, min);
    }
    
    public GLTFAccessor withComponentType(final ComponentType componentType) {
        return new GLTFAccessor(name, bufferView, byteOffset, componentType, normalized, count, type, max, min);
    }
    
    public GLTFAccessor withNormalized(final boolean normalized) {
        return new GLTFAccessor(name, bufferView, byteOffset, componentType, normalized, count, type, max, min);
    }
    
    public GLTFAccessor withCount(final int count) {
        return new GLTFAccessor(name, bufferView, byteOffset, componentType, normalized, count, type, max, min);
    }
    
    public GLTFAccessor withType(final Type type) {
        return new GLTFAccessor(name, bufferView, byteOffset, componentType, normalized, count, type, max, min);
    }
    
    public GLTFAccessor withMax(final List<Number> max) {
        return new GLTFAccessor(name, bufferView, byteOffset, componentType, normalized, count, type, max, min);
    }
    
    public GLTFAccessor withMax(final Number... max) {
        return withMax(Arrays.asList(max));
    }
    
    public GLTFAccessor withMin(final List<Number> min) {
        return new GLTFAccessor(name, bufferView, byteOffset, componentType, normalized, count, type, max, min);
    }
    
    public GLTFAccessor withMin(final Number... min) {
        return withMin(Arrays.asList(min));
    }    
}
