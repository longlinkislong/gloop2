/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import java.lang.ref.WeakReference;

/**
 *
 * @author zmichaels
 */
public final class GLTFTextureInfo {
    public final WeakReference<GLTFTexture> texture;
    public final int texCoord;
    
    public GLTFTextureInfo(final WeakReference<GLTFTexture> index, final int texCoord) {
        this.texture = index;
        this.texCoord = texCoord;
    }
    
    public GLTFTextureInfo() {
        this(null, 0);
    }
    
    public GLTFTextureInfo withTexture(final GLTFTexture texture) {
        return new GLTFTextureInfo(new WeakReference<>(texture), texCoord);
    }
    
    public GLTFTextureInfo withTexCoord(final int texCoord) {
        return new GLTFTextureInfo(texture, texCoord);
    }    
}
