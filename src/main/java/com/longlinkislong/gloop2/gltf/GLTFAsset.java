/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

/**
 *
 * @author zmichaels
 */
public final class GLTFAsset {
    public final String copyright;
    public final String generator;
    public final String version;
    public final String minVersion;
    
    public GLTFAsset(final String copyright, final String generator, final String version, final String minVersion) {
        this.copyright = copyright;
        this.generator = generator;
        this.version = version;
        this.minVersion = minVersion;
    }
    
    public GLTFAsset() {
        this(null, null, null, null);
    }
    
    public GLTFAsset withCopyright(final String copyright) {
        return new GLTFAsset(copyright, generator, version, minVersion);
    }
    
    public GLTFAsset withGenerator(final String generator) {
        return new GLTFAsset(copyright, generator, version, minVersion);
    }
    
    public GLTFAsset withVersion(final String version) {
        return new GLTFAsset(copyright, generator, version, minVersion);
    }
    
    public GLTFAsset withMinVersion(final String minVersion) {
        return new GLTFAsset(copyright, generator, version, minVersion);
    }
    
    public GLTFAsset validate() {
        if (this.version == null) {
            throw new IllegalArgumentException("Version cannot be null!");
        } else {
            return this;
        }
    }
}
