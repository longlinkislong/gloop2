/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public final class GLTF {

    public final List<String> extensionsUsed;
    public final List<String> extensionsRequired;
    public final List<GLTFAccessor> accessors;
    public final List<GLTFAnimation> animations;
    public final GLTFAsset asset;
    public final List<GLTFBuffer> buffers;
    public final List<GLTFBufferView> bufferViews;
    public final List<GLTFCamera> cameras;
    public final List<GLTFImage> images;
    public final List<GLTFMaterial> materials;
    public final List<GLTFMesh> meshes;
    public final List<GLTFNode> nodes;
    public final List<GLTFSampler> samplers;
    public final List<GLTFScene> scenes;
    public final List<GLTFSkin> skins;
    public final List<GLTFTexture> textures;

    public GLTF(
            final List<String> extensionsUsed, final List<String> extensionsRequired,
            final List<GLTFAccessor> accessors, final List<GLTFAnimation> animations,
            final GLTFAsset asset, final List<GLTFBuffer> buffers,
            final List<GLTFBufferView> bufferViews, final List<GLTFCamera> cameras,
            final List<GLTFImage> images, final List<GLTFMaterial> materials,
            final List<GLTFMesh> meshes, final List<GLTFNode> nodes, final List<GLTFSampler> samplers,
            final List<GLTFScene> scenes, final List<GLTFSkin> skins, final List<GLTFTexture> textures) {

        this.extensionsUsed = Tools.copyList(extensionsUsed);
        this.extensionsRequired = Tools.copyList(extensionsRequired);
        this.accessors = Tools.copyList(accessors);
        this.animations = Tools.copyList(animations);
        this.asset = asset;
        this.buffers = Tools.copyList(buffers);
        this.bufferViews = Tools.copyList(bufferViews);
        this.cameras = Tools.copyList(cameras);
        this.images = Tools.copyList(images);
        this.materials = Tools.copyList(materials);
        this.meshes = Tools.copyList(meshes);
        this.nodes = Tools.copyList(nodes);
        this.samplers = Tools.copyList(samplers);
        this.scenes = Tools.copyList(scenes);
        this.skins = Tools.copyList(skins);
        this.textures = Tools.copyList(textures);
    }

    public GLTF() {
        this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    public GLTF withExtensionsUsed(final List<String> extensionsUsed) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withExtensionsUsed(final String... extensionsUsed) {
        return withExtensionsUsed(Arrays.asList(extensionsUsed));
    }
    
    public GLTF withExtensionsRequired(final List<String> extensionsRequired) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withExtensionsRequired(final String... extensionsRequired) {
        return withExtensionsRequired(Arrays.asList(extensionsRequired));
    }
    
    public GLTF withAccessors(final List<GLTFAccessor> accessors ) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withAccessors(final GLTFAccessor... accessors) {
        return withAccessors(Arrays.asList(accessors));
    }
    
    public GLTF withAnimations(final List<GLTFAnimation> animations) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withAnimations(final GLTFAnimation... animations) {
        return withAnimations(Arrays.asList(animations));
    }
    
    public GLTF withAsset(final GLTFAsset asset) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withBuffers(final List<GLTFBuffer> buffers) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withBuffers(final GLTFBuffer... buffers) {
        return withBuffers(Arrays.asList(buffers));
    }
    
    public GLTF withBufferViews(final List<GLTFBufferView> bufferViews) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withBufferViews(final GLTFBufferView... bufferViews) {
        return withBufferViews(Arrays.asList(bufferViews));
    }
    
    public GLTF withCameras(final List<GLTFCamera> cameras) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withCameras(final GLTFCamera... cameras) {
        return withCameras(Arrays.asList(cameras));
    }
    
    public GLTF withImages(final List<GLTFImage> images) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withImages(final GLTFImage... images) {
        return withImages(Arrays.asList(images));
    }
    
    public GLTF withMaterials(final List<GLTFMaterial> materials) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withMaterials(final GLTFMaterial... materials) {
        return withMaterials(Arrays.asList(materials));
    }
    
    public GLTF withMeshes(final List<GLTFMesh> meshes) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withMeshes(final GLTFMesh... meshes) {
        return withMeshes(Arrays.asList(meshes));
    }
    
    public GLTF withNodes(final List<GLTFNode> nodes) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withNodes(final GLTFNode... nodes) {
        return withNodes(Arrays.asList(nodes));
    }
    
    public GLTF withSamplers(final List<GLTFSampler> samplers) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withSamplers(final GLTFSampler... samplers) {
        return withSamplers(Arrays.asList(samplers));
    }
    
    public GLTF withScenes(final List<GLTFScene> scenes) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withScenes(final GLTFScene... scenes) {
        return withScenes(Arrays.asList(scenes));
    }
    
    public GLTF withSkins(final List<GLTFSkin> skins) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withSkins(final GLTFSkin... skins) {
        return withSkins(Arrays.asList(skins));
    }
    
    public GLTF withTextures(final List<GLTFTexture> textures) {
        return new GLTF(extensionsUsed, extensionsRequired, accessors, animations, asset, buffers, bufferViews, cameras, images, materials, meshes, nodes, samplers, scenes, skins, textures);
    }
    
    public GLTF withTextures(final GLTFTexture... textures) {
        return withTextures(Arrays.asList(textures));
    }
}
