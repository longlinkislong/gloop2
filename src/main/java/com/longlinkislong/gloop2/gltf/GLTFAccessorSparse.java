/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public final class GLTFAccessorSparse {

    public static final class Indices {

        public static enum ComponentType implements CEnum {
            UNSIGNED_BYTE(5121),
            UNSIGNED_SHORT(5123),
            UNSIGNED_INT(5125);

            public final int value;

            private ComponentType(final int value) {
                this.value = value;
            }

            public static Optional<ComponentType> of(final int value) {
                return Tools.valueOf(value, Arrays.stream(values()));
            }

            @Override
            public int getValue() {
                return this.value;
            }
        }

        public final WeakReference<GLTFBufferView> bufferView;
        public final int byteOffset;
        public final ComponentType componentType;

        public Indices(final WeakReference<GLTFBufferView> bufferView, final int byteOffset, final ComponentType componentType) {
            this.bufferView = bufferView;
            this.byteOffset = byteOffset;
            this.componentType = componentType;
        }

        public Indices() {
            this(null, 0, null);
        }

        public Indices withBufferView(final GLTFBufferView bufferView) {
            return new Indices(new WeakReference<>(bufferView), byteOffset, componentType);
        }

        public Indices withByteOffset(final int byteOffset) {
            return new Indices(bufferView, byteOffset, componentType);
        }

        public Indices withComponentType(final ComponentType componentType) {
            return new Indices(bufferView, byteOffset, componentType);
        }
    }

    public static final class Values {

        public final WeakReference<GLTFBufferView> bufferView;
        public final int byteOffset;

        public Values(final WeakReference<GLTFBufferView> bufferView, final int byteOffset) {
            this.bufferView = bufferView;
            this.byteOffset = byteOffset;
        }

        public Values() {
            this(null, 0);
        }

        public Values withBufferViewId(final GLTFBufferView bufferView) {
            return new Values(new WeakReference<>(bufferView), byteOffset);
        }

        public Values withByteOffset(final int byteOffset) {
            return new Values(bufferView, byteOffset);
        }
    }

    public final int count;
    public final List<Indices> indices;
    public final List<Values> values;
    
    public GLTFAccessorSparse(final int count, final List<Indices> indices, final List<Values> values) {        
        this.count = count;
        this.indices = Tools.copyList(indices);
        this.values = Tools.copyList(values);
    }
    
    public GLTFAccessorSparse() {
        this(0, null, null);
    }
    
    public GLTFAccessorSparse withCount(final int count) {
        return new GLTFAccessorSparse(count, indices, values);
    }
    
    public GLTFAccessorSparse withIndices(final List<Indices> indices) {
        return new GLTFAccessorSparse(count, indices, values);
    }
    
    public GLTFAccessorSparse withIndices(final Indices... indices) {
        return withIndices(Arrays.asList(indices));
    }
    
    public GLTFAccessorSparse withValues(final List<Values> values) {
        return new GLTFAccessorSparse(count, indices, values);
    }
    
    public GLTFAccessorSparse withValues(final Values... values) {
        return withValues(Arrays.asList(values));
    }
}
