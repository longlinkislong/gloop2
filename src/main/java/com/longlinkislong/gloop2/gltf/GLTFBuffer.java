/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

/**
 *
 * @author zmichaels
 */
public final class GLTFBuffer {

    public final String name;
    public final String uri;
    public final int byteLength;

    public GLTFBuffer(final String name, final String uri, final int byteLength) {
        this.name = name;
        this.uri = uri;
        this.byteLength = byteLength;
    }

    public GLTFBuffer() {
        this(null, null, 1);
    }
    
    public GLTFBuffer withName(final String name) {
        return new GLTFBuffer(name, uri, byteLength);
    }

    public GLTFBuffer withURI(final String uri) {
        return new GLTFBuffer(name, uri, byteLength);
    }

    public GLTFBuffer withByteLength(final int byteLength) {
        return new GLTFBuffer(name, uri, byteLength);
    }
}
