/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

/**
 *
 * @author zmichaels
 */
public class GLTFCamera {
    public static enum Type {
        PERSPECTIVE("perspective"),
        ORTHOGRAPHIC("orthographic");
        
        public final String value;
        
        private Type(final String value) {
            this.value = value;
        }
    }
    
    public static final class Orthographic {
        public final float xmag;
        public final float ymag;
        public final float zfar;
        public final float znear;
        
        public Orthographic(final float xmag, final float ymag, final float zfar, final float znear) {
            this.xmag = xmag;
            this.ymag = ymag;
            this.zfar = zfar;
            this.znear = znear;
        }
        
        public Orthographic() {
            this(0, 0, 0, 0);
        }
        
        public Orthographic withXMag(final float xmag) {
            return new Orthographic(xmag, ymag, zfar, znear);
        }
        
        public Orthographic withYMag(final float ymag) {
            return new Orthographic(xmag, ymag, zfar, znear);
        }
        
        public Orthographic withZNear(final float znear) {
            return new Orthographic(xmag, ymag, zfar, znear);
        }
        
        public Orthographic withZFar(final float zfar) {
            return new Orthographic(xmag, ymag, zfar, znear);
        }
        
        public Orthographic validate() {
            if (Math.abs(this.xmag) < 1E-7F) {
                throw new IllegalArgumentException("Horizontal magnification view cannot be 0!");
            } else if (Math.abs(this.ymag) < 1E-7F) {
                throw new IllegalArgumentException("Vertical magnification view cannot be 0!");
            } else if (this.zfar < znear) {
                throw new IllegalArgumentException("Far clipping plane value must be larger than the near clipping plane value!");
            } else if (this.zfar < 0.0F) {
                throw new IllegalArgumentException("Far clipping plane value cannot be negative!");
            } else if (this.znear < 0.0F) {
                throw new IllegalArgumentException("Near clipping plane value cannot be negative!");
            } else {
                return this;
            }
        }
    }
    
    public static final class Perspective {
        public final float aspectRatio;
        public final float yfov;
        public final float zfar;
        public final float znear;
        
        public Perspective(final float aspectRatio, final float yfov, final float zfar, final float znear) {
            this.aspectRatio = aspectRatio;
            this.yfov = yfov;
            this.zfar = zfar;
            this.znear = znear;
        }
        
        public Perspective() {
            this(0, 0, 0, 0);
        }
        
        public Perspective withAspectRatio(final float aspectRatio) {
            return new Perspective(aspectRatio, yfov, zfar, znear);
        }
        
        public Perspective withYFov(final float yfov) {
            return new Perspective(aspectRatio, yfov, zfar, znear);
        }
        
        public Perspective withZFar(final float zfar) {
            return new Perspective(aspectRatio, yfov, zfar, znear);
        }
        
        public Perspective withZNear(final float znear) {
            return new Perspective(aspectRatio, yfov, zfar, znear);
        }
        
        public Perspective validate() {
            if (this.aspectRatio < 0.0F) {
                throw new IllegalArgumentException("AspectRatio cannot be negative!");
            } else if (this.yfov < 0.0F) {
                throw new IllegalArgumentException("YFov cannot be negative!");
            } else if (this.zfar < 0.0F) {
                throw new IllegalArgumentException("ZFar cannot be negative!");
            } else if (this.znear < 0.0F) {
                throw new IllegalArgumentException("ZNear cannot be negative!");
            } else {
                return this;
            }
        }
    }
    
    public final String name;
    public final Type type;
    public final Orthographic orthographic;
    public final Perspective perspective;
    
    public GLTFCamera(
            final String name,
            final Type type, 
            final Orthographic orthographic, final Perspective perspective) {
        
        this.name = name;
        this.type = type;
        this.orthographic = orthographic;
        this.perspective = perspective;
    }
    
    public GLTFCamera withName(final String name) {
        return new GLTFCamera(name, type, orthographic, perspective);
    }
    
    public GLTFCamera withType(final Type type) {
        return new GLTFCamera(name, type, orthographic, perspective);
    }
    
    public GLTFCamera withOrthographic(final Orthographic orthographic) {
        return new GLTFCamera(name, type, orthographic, perspective);
    }
    
    public GLTFCamera withPerspective(final Perspective perspective) {
        return new GLTFCamera(name, type, orthographic, perspective);
    }
}
