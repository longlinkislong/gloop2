/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.gltf;

import com.longlinkislong.gloop2.Tools;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author zmichaels
 */
public final class GLTFNode {
    public final String name;
    public final WeakReference<GLTFCamera> camera;
    public final List<WeakReference<GLTFNode>> children;
    public final WeakReference<GLTFSkin> skin;
    public final float[] matrix;
    public final WeakReference<GLTFMesh> mesh;
    public final float[] rotation;
    public final float[] scale;
    public final float[] translation;
    public final float[] weights;
    
    public GLTFNode(
            final String name, final WeakReference<GLTFCamera> camera, 
            final List<WeakReference<GLTFNode>> children, final WeakReference<GLTFSkin> skin,
            final float[] matrix, final WeakReference<GLTFMesh> mesh, final float[] rotation,
            final float[] scale, final float[] translation, final float[] weights) {
        
        this.name = name;
        this.camera = camera;
        this.children = Tools.copyList(children);
        this.skin = skin;
        this.matrix = matrix;
        this.mesh = mesh;
        this.rotation = rotation;
        this.scale = scale;
        this.translation = translation;
        this.weights = weights;
    }
    
    public GLTFNode() {
        this(
                null, null, 
                null, null, 
                null, null, new float[] {0F, 0F, 0F, 1F},
                new float[] {1F, 1F, 1F}, new float[] {0F, 0F, 0F},
                null);
    }
    
    public GLTFNode withName(final String name) {
        return new GLTFNode(name, camera, children, skin, matrix, mesh, rotation, scale, translation, weights);
    }
    
    public GLTFNode withCamera(final GLTFCamera camera) {
        return new GLTFNode(name, new WeakReference<>(camera), children, skin, matrix, mesh, rotation, scale, translation, weights);
    }
    
    public GLTFNode withChildren(final List<GLTFNode> children) {
        final List<WeakReference<GLTFNode>> ref = children.stream()
                .map(WeakReference::new)
                .collect(Collectors.toList());
        
        return new GLTFNode(name, camera, ref, skin, matrix, mesh, rotation, scale, translation, weights);
    }
    
    public GLTFNode withChildren(final GLTFNode... children) {
        return withChildren(Arrays.asList(children));
    }
    
    public GLTFNode withSkin(final GLTFSkin skin) {
        return new GLTFNode(name, camera, children, new WeakReference<>(skin), matrix, mesh, rotation, scale, translation, weights);
    }
    
    public GLTFNode withMatrix(final float[] matrix) {
        return new GLTFNode(name, camera, children, skin, matrix, mesh, rotation, scale, translation, weights);
    }
    
    public GLTFNode withMesh(final GLTFMesh mesh) {
        return new GLTFNode(name, camera, children, skin, matrix, new WeakReference<>(mesh), rotation, scale, translation, weights);
    }
    
    public GLTFNode withRotation(final float[] rotation) {
        return new GLTFNode(name, camera, children, skin, matrix, mesh, rotation, scale, translation, weights);
    }
    
    public GLTFNode withScale(final float[] scale) {
        return new GLTFNode(name, camera, children, skin, matrix, mesh, rotation, scale, translation, weights);
    }
    
    public GLTFNode withTranslation(final float[] translation) {
        return new GLTFNode(name, camera, children, skin, matrix, mesh, rotation, scale, translation, weights);
    }
    
    public GLTFNode withWeights(final float[] weights) {
        return new GLTFNode(name, camera, children, skin, matrix, mesh, rotation, scale, translation, weights);
    }
}
