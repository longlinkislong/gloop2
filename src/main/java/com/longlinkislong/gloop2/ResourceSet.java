/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public final class ResourceSet implements Set<AutoCloseable>, AutoCloseable {
    private final Set<AutoCloseable> internal;
    
    public static ResourceSet setOf(final AutoCloseable... resources) {        
        return new ResourceSet(Arrays.asList(resources));
    }
    
    public ResourceSet() {
        this.internal = new HashSet<>();
    }
    
    public ResourceSet(final Collection<AutoCloseable> base) {
        this.internal = new HashSet<> (base);
    }
    
    @Override
    public int size() {
        return this.internal.size();
    }

    @Override
    public boolean isEmpty() {
        return this.internal.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return this.internal.contains(o);
    }

    @Override
    public Iterator<AutoCloseable> iterator() {
        return this.internal.iterator();
    }

    @Override
    public Object[] toArray() {
        return this.internal.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return this.internal.toArray(a);
    }

    @Override
    public boolean add(AutoCloseable e) {
        return this.internal.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return this.internal.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return this.internal.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends AutoCloseable> c) {
        return this.internal.addAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return this.internal.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return this.internal.removeAll(c);
    }

    @Override
    public void clear() {
        this.internal.clear();
    }

    @Override
    public void close() throws Exception {
        for (AutoCloseable res : this.internal) {
            res.close();
        }
        
        this.internal.clear();
    }
    
}
