/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

/**
 *
 * @author zmichaels
 */
public enum DecorationBuiltin {
    POSITION,
    POINT_SIZE,
    CLIP_DISTANCE,
    CULL_DISTANCE,
    VERTEX_ID,
    INSTANCE_ID,
    PRIMITIVE_ID,
    INVOCATION_ID,
    LAYER,
    VIEWPORT_INDEX,
    TESS_LEVEL_OUTER,
    TESS_LEVEL_INNER,
    TESS_COORD,
    PATCH_VERTICES,
    FRAG_COORD,
    POINT_COORD,
    FRONT_FACING,
    SAMPLE_ID,
    SAMPLE_POSITION,
    SAMPLE_MASK,
    FRAG_DEPTH,
    HELPER_INVOCATION,
    NUM_WORKGROUPS,
    WORKGROUP_SIZE,
    WORKGROUP_ID,
    LOCAL_INVOCATION_ID,
    GLOBAL_INVOCATION_ID,
    LEVEL_INVOCATION_INDEX,
    WORK_DIM,
    GLOBAL_SIZE,
    ENQUEUED_WORKGROUP_SIZE,
    GLOBAL_OFFSET,
    GLOBAL_LINEAR_ID,
    SUBGROUP_SIZE,
    SUBGROUP_MAX_SIZE,
    NUM_SUBGROUPS,
    NUM_ENQUEUED_SUBGROUPS,
    SUBGROUP_ID,
    SUBGROUP_LOCAL_INVOCATION_ID,
    VERTEX_INDEX,
    INSTANCE_INDEX,
    SUBGROUP_EQ_MASK_KHR,
    SUBGROUP_GE_MASK_KHR,
    SUBGROUP_GT_MASK_KHR,
    SUBGROUP_LE_MASK_KHR,
    SUBGROUP_LT_MASK_KHR,
    BASE_VERTEX,
    BASE_INSTANCE,
    DRAW_INDEX,
    DEVICE_INDEX,
    VIEW_INDEX,
    VIEWPORT_MASK_NV,
    SECONDARY_VIEWPORT_MASK_NV,
    POSITION_PER_VIEW_NV,
    VIEWPORT_MASK_PER_VIEW_NV
}
