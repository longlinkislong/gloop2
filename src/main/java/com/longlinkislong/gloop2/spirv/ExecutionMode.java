/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum ExecutionMode implements CEnum {
    INVOCATION(0),
    SPACING_EQUAL(1),
    SPACING_FRACTIONAL_EVEN(2),
    SPACING_FRACTIONAL_ODD(3),
    VERTEX_ORDER_C2(4),
    VERTEX_ORDER_CCW(5),
    PIXEL_CENTER_INTEGER(6),
    ORIGIN_UPPER_LEFT(7),
    ORIGIN_LOWER_LEFT(8),
    EARLY_FRAGMENT_TESTS(9),
    POINT_MODE(10),
    XFB(11),
    DEPTH_REPLACING(12),
    DEPTH_GREATER(14),
    DEPTH_LESS(15),
    DEPTH_UNCHANGED(16),
    LOCAL_SIZE(17),
    LOCAL_SIZE_HINT(18),
    INPUT_POINTS(19),
    INPUT_LINES(20),
    INPUT_LINES_ADJACENCY(21),
    TRIANGLES(22),
    INPUT_TRIANGLES_ADJACENCY(23),
    QUADS(24),
    ISOLINES(25),
    OUTPUT_VERTICES(26),
    OUTPUT_POINTS(27),
    OUTPUT_LINE_STRIP(28),
    OUTPUT_TRIANGLE_STRIP(29),
    VEC_TYPE_HINT(30),
    CONTRACTION_OFF(31);

    final int value;

    private ExecutionMode(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<ExecutionMode> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
