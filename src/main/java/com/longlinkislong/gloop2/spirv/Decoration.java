/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum Decoration implements CEnum {
    RELAXED_PRECISION(0),
    SPEC_ID(1),
    BLOCK(2),
    BUFFER_BLOCK(3),
    ROW_MAJOR(4),
    COL_MAJOR(5),
    ARRAY_STRIDE(6),
    MATRIX_STRIDE(7),
    GLSL_SHARED(8),
    GLSL_PACKED(9),
    C_PACKED(10),
    BUILT_IN(11),
    NO_PERSPECTIVE(13),
    FLAT(14),
    PATCH(15),
    CENTROID(16),
    SAMPLE(17),
    INVARIANT(18),
    RESTRICT(19),
    ALIASED(20),
    VOLATILE(21),
    CONSTANT(22),
    COHERENT(23),
    NON_WRITABLE(24),
    NON_READABLE(25),
    UNIFORM(26),
    SATURATED_CONVERSION(28),
    STREAM(29),
    LOCATION(30),
    COMPONENT(31),
    INDEX(32),
    BINDING(33),
    DESCRIPTOR_SET(34),
    OFFSET(35),
    XFB_BUFFER(36),
    XFB_STRIDE(37),
    FUNC_PARAM_ATTR(38),
    FP_ROUNDING_MODE(39),
    FP_FAST_MATH_MODE(40),
    LINKAGE_ATTRIBUTES(41),
    NO_CONTRACTION(42),
    INPUT_ATTACHMENT_INDEX(43),
    ALIGNMENT(44),
    OVERRIDE_COVERAGE_NV(5248),
    PASSTHROUGH_NV(5250),
    VIEWPORT_RELATIVE_NV(5252),
    SECONDARY_VIEWPORT_RELATIVE_NV(5256);
    
    public final int value;

    private Decoration(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    public static Optional<Decoration> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
