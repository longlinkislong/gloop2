/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum FunctionParameterAttribute implements CEnum {
    Z_EXT(0),
    S_EXT(1),
    BY_VAL(2),
    S_RET(3),
    NO_ALIAS(4),
    NO_CAPTURE(5),
    NO_WRITE(6),
    NO_READ_WRITE(7);

    final int value;

    private FunctionParameterAttribute(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<FunctionParameterAttribute> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

}
