/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;

/**
 *
 * @author zmichaels
 */
public enum BuiltIn implements CEnum {
    POSITION(0),
    POINT_SIZE(1),
    CLIP_DISTANCE(3),
    CULL_DISTANCE(4),
    VERTEX_ID(5),
    INSTANCE_ID(6),
    PRIMITIVE_ID(7),
    INVOCATION_ID(8),
    LAYER(9),
    VIEWPORT_INDEX(10),
    TESS_LEVEL_OUTER(11),
    TESS_LEVEL_INNER(12),
    TESS_COORD(13),
    PATCH_VERTICES(14),
    FRAG_COORD(15),
    POINT_COORD(16),
    FRONT_FACING(17),
    SAMPLE_ID(18),
    SAMPLE_POSITION(19),
    SAMPLE_MASK(20),
    FRAG_DEPTH(22),
    HELPER_INVOCATION(23),
    NUM_WORKGROUPS(24),
    WORKGROUP_SIZE(25),
    WORKGROUP_ID(26),
    LOCAL_INVOCATION_ID(27),
    GLOBAL_INVOCATION_ID(28),
    LOCAL_INVOCATION_INDEX(29),
    WORK_DIM(30),
    GLOBAL_SIZE(31),
    ENQUEUED_WORKGROUP_SIZE(32),
    GLOBAL_OFFSET(33),
    GLOBAL_LINEAR_ID(34),
    SUBGROUP_SIZE(36),
    SUBGROUP_MAX_SIZE(37),
    NUM_SUBGROUPS(38),
    NUM_ENQUEUED_SUBGROUPS(39),
    SUBGROUP_ID(40),
    SUBGROUP_LOCAL_INVOCATION_ID(41),
    VERTEX_INDEX(42),
    INSTANCE_INDEX(43),
    SUBGROUP_EQ_MASK_KHR(4416),
    SUBGROUP_GE_MASK_KHR(4417),
    SUBGROUP_GT_MASK_KHR(4418),
    SUBGROUP_LE_MASK_KHR(4419),
    SUBGROUP_LT_MASK_KHR(4420),
    BASE_VERTEX(4424),
    BASE_INSTANCE(4425),
    DRAW_INDEX(4426),
    DEVICE_INDEX(4438),
    VIEW_INDEX(4440),
    VIEWPORT_MASK_NV(5253),
    SECONDARY_POSITION_NV(5257),
    SECONDARY_VIEWPORT_MASK_NV(5258),
    POSITION_PER_VIEW_NV(5261),
    VIEWPORT_MASK_PER_VIEW_NV(5262);

    final int value;

    private BuiltIn(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

}
