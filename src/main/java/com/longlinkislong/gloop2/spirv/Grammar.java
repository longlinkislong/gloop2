/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

/**
 *
 * @author zmichaels
 */
public class Grammar {

    public static String opcodeName(final int opcode) {
        switch (opcode) {
            case 0:
                return "OpNop";
            case 1:
                return "OpUndef";
            case 2:
                return "OpSourceContinued";
            case 3:
                return "OpSource";
            case 4:
                return "OpSourceExtension";
            case 5:
                return "OpName";
            case 6:
                return "OpMemberName";
            case 7:
                return "OpString";
            case 8:
                return "OpLine";
            case 10:
                return "OpExtension";
            case 11:
                return "OpExtInstImport";
            case 12:
                return "OpExtInst";
            case 14:
                return "OpMemoryModel";
            case 15:
                return "OpEntryPoint";
            case 16:
                return "OpExecutionMode";
            case 17:
                return "OpCapability";
            case 19:
                return "OpTypeVoid";
            case 20:
                return "OpTypeBool";
            case 21:
                return "OpTypeInt";
            case 22:
                return "OpTypeFloat";
            case 23:
                return "OpTypeVector";
            case 24:
                return "OpTypeMatrix";
            case 25:
                return "OpTypeImage";
            case 26:
                return "OpTypeSampler";
            case 27:
                return "OpTypeSampledImage";
            case 28:
                return "OpTypeArray";
            case 29:
                return "OpTypeRuntimeArray";
            case 30:
                return "OpTypeStruct";
            case 31:
                return "OpTypeOpaque";
            case 32:
                return "OpTypePointer";
            case 33:
                return "OpTypeFunction";
            case 34:
                return "OpTypeEvent";
            case 35:
                return "OpTypeDeviceEvent";
            case 36:
                return "OpTypeReserveId";
            case 37:
                return "OpTypeQueue";
            case 38:
                return "OpTypePipe";
            case 39:
                return "OpTypeForwardPointer";
            case 41:
                return "OpConstantTrue";
            case 42:
                return "OpConstantFalse";
            case 43:
                return "OpConstant";
            case 44:
                return "OpConstantComposite";
            case 45:
                return "OpConstantSampler";
            case 46:
                return "OpConstantNull";
            case 48:
                return "OpSpecConstantTrue";
            case 49:
                return "OpSpecConstantFalse";
            case 50:
                return "OpSpecConstant";
            case 51:
                return "OpSpecConstantComposite";
            case 52:
                return "OpSpecConstantOp";
            case 54:
                return "OpFunction";
            case 55:
                return "OpFunctionParameter";
            case 56:
                return "OpFunctionEnd";
            case 57:
                return "OpFunctionCall";
            case 59:
                return "OpVariable";
            case 60:
                return "OpImageTexelPointer";
            case 61:
                return "OpLoad";
            case 62:
                return "OpStore";
            case 63:
                return "OpCopyMemory";
            case 64:
                return "OpCopyMemorySized";
            case 65:
                return "OpAccessChain";
            case 66:
                return "OpInBoundsAccessChain";
            case 67:
                return "OpPtrAccessChain";
            case 68:
                return "OpArrayLength";
            case 69:
                return "OpGenericPtrMemSemantics";
            case 70:
                return "OpInBoundsPtrAccessChain";
            case 71:
                return "OpDecorate";
            case 72:
                return "OpMemberDecorate";
            case 73:
                return "OpDecorationGroup";
            case 74:
                return "OpGroupDecorate";
            case 75:
                return "OpGroupMemberDecorate";
            case 77:
                return "OpVectorExtractDynamic";
            case 78:
                return "OpVectorInsertDynamic";
            case 79:
                return "OpVectorShuffle";
            case 80:
                return "OpCompositeConstruct";
            case 81:
                return "OpCompositeExtract";
            case 82:
                return "OpCompositeInsert";
            case 83:
                return "OpCopyObject";
            case 84:
                return "OpTranspose";
            case 86:
                return "OpSampledImage";
            case 87:
                return "OpImageSampleImplicitLod";
            case 88:
                return "OpImageSampleExplicitLod";
            case 89:
                return "OpImageSampleDrefImplicitLod";
            case 90:
                return "OpImageSampleDrefExplicitLod";
            case 91:
                return "OpImageSampleProjImplicitLod";
            case 92:
                return "OpImageSampleProjExplicitLod";
            case 93:
                return "OpImageSampleProjDrefImplicitLod";
            case 94:
                return "OpImageSampleProjDrefExplicitLod";
            case 95:
                return "OpImageFetch";
            case 96:
                return "OpImageGather";
            case 97:
                return "OpImageDrefGather";
            case 98:
                return "OpImageRead";
            case 99:
                return "OpImageWrite";
            case 100:
                return "OpImage";
            case 101:
                return "OpImageQueryFormat";
            case 102:
                return "OpImageQueryOrder";
            case 103:
                return "OpImageQuerySizeLod";
            case 104:
                return "OpImageQuerySize";
            case 105:
                return "OpImageQueryLod";
            case 106:
                return "OpImageQueryLevels";
            case 107:
                return "OpImageQuerySamples";
            case 109:
                return "OpConvertFToU";
            case 110:
                return "OpConvertFToS";
            case 111:
                return "OpConvertSToF";
            case 112:
                return "OpConvertUToF";
            case 113:
                return "OpUConvert";
            case 114:
                return "OpSConvert";
            case 115:
                return "OpFConvert";
            case 116:
                return "OpQuantizeToF16";
            case 117:
                return "OpConvertPtrToU";
            case 118:
                return "OpSatConvertSToU";
            case 119:
                return "OpSatConvertUToS";
            case 120:
                return "OpConvertUToPtr";
            case 121:
                return "OpPtrCastToGeneric";
            case 122:
                return "OpGenericCastToPtr";
            case 123:
                return "OpGenericCastToPtrExplicit";
            case 124:
                return "OpBitcast";
            case 126:
                return "OpSNegate";
            case 127:
                return "OpFNegate";
            case 128:
                return "OpIAdd";
            case 129:
                return "OpFAdd";
            case 130:
                return "OpISub";
            case 131:
                return "OpFSub";
            case 132:
                return "OpIMul";
            case 133:
                return "OpFMul";
            case 134:
                return "OpUDiv";
            case 135:
                return "OpSDiv";
            case 136:
                return "OpFDiv";
            case 137:
                return "OpUMod";
            case 138:
                return "OpSRem";
            case 139:
                return "OpSMod";
            case 140:
                return "OpFRem";
            case 141:
                return "OpFMod";
            case 142:
                return "OpVectorTimesScalar";
            case 143:
                return "OpMatrixTimesScalar";
            case 144:
                return "OpVectorTimesMatrix";
            case 145:
                return "OpMatrixTimesVector";
            case 146:
                return "OpMatrixTimesMatrix";
            case 147:
                return "OpOuterProduct";
            case 148:
                return "OpDot";
            case 149:
                return "OpIAddCarry";
            case 150:
                return "OpISubBorrow";
            case 151:
                return "OpUMulExtended";
            case 152:
                return "OpSMulExtended";
            case 154:
                return "OpAny";
            case 155:
                return "OpAll";
            case 156:
                return "OpIsNan";
            case 157:
                return "OpIsInf";
            case 158:
                return "OpIsFinite";
            case 159:
                return "OpIsNormal";
            case 160:
                return "OpSignBitSet";
            case 161:
                return "OpLessOrGreater";
            case 162:
                return "OpOrdered";
            case 163:
                return "OpUnordered";
            case 164:
                return "OpLogicalEqual";
            case 165:
                return "OpLogicalNotEqual";
            case 166:
                return "OpLogicalOr";
            case 167:
                return "OpLogicalAnd";
            case 168:
                return "OpLogicalNot";
            case 169:
                return "OpSelect";
            case 170:
                return "OpIEqual";
            case 171:
                return "OpINotEqual";
            case 172:
                return "OpUGreaterThan";
            case 173:
                return "OpSGreaterThan";
            case 174:
                return "OpUGreaterThanEqual";
            case 175:
                return "OpSGreaterThanEqual";
            case 176:
                return "OpULessThan";
            case 177:
                return "OpSLessThan";
            case 178:
                return "OpULessThanEqual";
            case 179:
                return "OpSLessThanEqual";
            case 180:
                return "OpFOrdEqual";
            case 181:
                return "OpFUnordEqual";
            case 182:
                return "OpFordNotEqual";
            case 183:
                return "OpFUnordNotEqual";
            case 184:
                return "OpFOrdLessThan";
            case 185:
                return "OpFUnordLessThan";
            case 186:
                return "OpFOrdGreaterThan";
            case 187:
                return "OpFUnordGreaterThan";
            case 188:
                return "OpFOrdLessThanEqual";
            case 189:
                return "OpFUnordLessThanEqual";
            case 191:
                return "OpFUnordGreaterThanEqual";
            case 194:
                return "OpShiftRightLogical";
            case 195:
                return "OpShiftRightArithmetic";
            case 196:
                return "OpShiftLeftLogical";
            case 197:
                return "OpBitwiseOr";
            case 198:
                return "OpBitwiseXor";
            case 199:
                return "OpBitwiseAnd";
            case 200:
                return "OpNot";
            case 201:
                return "OpBitFieldInsert";
            case 202:
                return "OpBitFieldSExtract";
            case 203:
                return "OpBitFieldUExtract";
            case 204:
                return "OpBitReverse";
            case 205:
                return "OpBitCount";
            case 207:
                return "OpDPdx";
            case 208:
                return "OpDPdy";
            case 209:
                return "OpFwidth";
            case 210:
                return "OpDpdxFine";
            case 211:
                return "OpDpdyFine";
            case 212:
                return "OpFwidthFine";
            case 213:
                return "OpDPdxCoarse";
            case 214:
                return "OpDPdyCoarse";
            case 215:
                return "OpFwidthCoarse";
            case 218:
                return "OpEmitVertex";
            case 219:
                return "OpEndPrimitive";
            case 220:
                return "OpEmitStreamVertex";
            case 221:
                return "OpEndStreamPrimitive";
            case 224:
                return "OpControlBarrier";
            case 225:
                return "OpMemoryBarrier";
            case 227:
                return "OpAtomicLoad";
            case 228:
                return "OpAtomicStore";
            case 229:
                return "OpAtomicExchange";
            case 230:
                return "OpAtomicCompareExchange";
            case 231:
                return "OpAtomicCompareExchangeWeak";
            case 232:
                return "OpAtomicIIncrement";
            case 233:
                return "OpAtomicIDecrement";
            case 234:
                return "OpAtomicIAdd";
            case 235:
                return "OpAtomicISub";
            case 236:
                return "OpAtomicSMin";
            case 237:
                return "OpAtomicUMin";
            case 238:
                return "OpAtomicSMax";
            case 239:
                return "OpAtomicUMax";
            case 240:
                return "OpAtomicAnd";
            case 241:
                return "OpAtomicOr";
            case 242:
                return "OpAtomicXor";
            case 245:
                return "OpPhi";
            case 246:
                return "OpLoopMerge";
            case 247:
                return "OpSelectMerge";
            case 248:
                return "OpLabel";
            case 249:
                return "OpBranch";
            case 250:
                return "OpBranchConditional";
            case 251:
                return "OpSwitch";
            case 252:
                return "OpKill";
            case 253:
                return "OpReturn";
            case 254:
                return "OpReturnValue";
            case 255:
                return "OpUnreachable";
            case 256:
                return "OpLifetimeStart";
            case 257:
                return "OpLifetimeStop";
            case 259:
                return "OpGroupAsyncCopy";
            case 260:
                return "OpGroupWaitEvents";
            case 261:
                return "OpGroupAll";
            case 262:
                return "OpGroupAny";
            case 263:
                return "OpGroupBroadcast";
            case 264:
                return "OpGroupIAdd";
            case 265:
                return "OpGroupFAdd";
            case 266:
                return "OpGroupFMin";
            case 267:
                return "OpGroupUMin";
            case 268:
                return "OpGroupSMin";
            case 269:
                return "OpGroupFMax";
            case 270:
                return "OpGroupUMax";
            case 271:
                return "OpGroupSMax";
            case 274:
                return "OpReadPipe";
            case 275:
                return "OpWritePipe";
            case 276:
                return "OpReservedReadPipe";
            case 277:
                return "OpReservedWritePipe";
            case 278:
                return "OpReserveReadPipePackets";
            case 279:
                return "OpReserveWritePipePackets";
            case 280:
                return "OpCommitReadPipe";
            case 281:
                return "OpCommitWritePipe";
            case 282:
                return "OpIsValidResrveId";
            case 283:
                return "OpGetNumPipePackets";
            case 284:
                return "OpGetMaxPipePackets";
            case 285:
                return "OpGroupReserveReadPipePackets";
            case 286:
                return "OpGroupReserveWritePipePackets";
            case 287:
                return "OpGroupCommitReadPipe";
            case 288:
                return "OpGroupCommitWritePipe";
            case 291:
                return "OpEnqueueMarker";
            case 292:
                return "OpEnqueueKernel";
            case 293:
                return "OpGetKernelNDRangeSubGroupCount";
            case 294:
                return "OpGetKernelNDRangeMaxSubGroupSize";
            case 295:
                return "OpGetKernelWorkGroupSize";
            case 296:
                return "OpGetKernelPreferredWorkGroupSizeMultiple";
            case 297:
                return "OpRetainEvent";
            case 298:
                return "OpReleaseEvent";
            case 299:
                return "OpCreateUserEvent";
            case 300:
                return "OpIsValidEvent";
            case 301:
                return "OpSetUserEventStatus";
            case 302:
                return "OpCaptureEventProfilingInfo";
            case 303:
                return "OpGetDefaultQueue";
            case 304:
                return "OpBuildNDRange";
            case 305:
                return "OpImageSparseSampleImplicitLod";
            case 306:
                return "OpImageSparseSampleExplicitLod";
            case 307:
                return "OpImageSparseSampleDrefImplicitLod";
            case 308:
                return "OpImageSparseSampleDrefExplicitLod";
            case 309:
                return "OpImageSparseSampleProjImplicitLod";
            case 310:
                return "OpImageSparseSampleProjExplicitLod";
            case 311:
                return "OpImageSparseSampleProjDrefImplicitLod";
            case 312:
                return "OpImageSparseSampleProjDrefExplicitLod";
            case 313:
                return "OpImageSparseFetch";
            case 314:
                return "OpImageSparseGather";
            case 315:
                return "OpImageSparseDrefGather";
            case 316:
                return "OpImageSparseTexelsResident";
            case 317:
                return "OpNoLine";
            case 318:
                return "OpAtomicFlagTestAndSet";
            case 319:
                return "OpAtomicFlagClear";
            case 320:
                return "OpImageSparseRead";
            default:
                return "OpUNKNOWN(" + opcode + ')';
        }
    }
}
