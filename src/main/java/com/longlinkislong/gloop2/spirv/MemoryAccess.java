/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum MemoryAccess implements CEnum {
    NONE(0X0),
    VOLATILE(0X1),
    ALIGNED(0X2),
    NONTEMPORAL(0X4);

    final int value;

    private MemoryAccess(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<MemoryAccess> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

}
