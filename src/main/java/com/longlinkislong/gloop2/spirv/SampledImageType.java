/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

/**
 *
 * @author zmichaels
 */
public class SampledImageType {
    public final int imageType;
    
    public SampledImageType(final int imageType) {
        this.imageType = imageType;
    }
    
    @Override
    public String toString() {
        return new StringBuilder()
                .append("{\"Type\":\"Sampled Image\",\"Image Type\":")
                .append(imageType)
                .append("}")
                .toString();
    }
}
