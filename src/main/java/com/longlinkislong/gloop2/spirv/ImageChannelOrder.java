/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;

/**
 *
 * @author zmichaels
 */
public enum ImageChannelOrder implements CEnum {
    R(0),
    A(1),
    RG(2),
    RA(3),
    RGB(4),
    RGBA(5),
    BGRA(6),
    ARGB(7),
    INTENSITY(8),
    LUMINANCE(9),
    RX(10),
    RGX(11),
    RGBX(12),
    DEPTH(13),
    DEPTH_STENCIL(14),
    SRGB(15),
    SRGBX(16),
    SRGBA(17),
    SBGRA(18),
    SABGR(19);

    final int value;

    private ImageChannelOrder(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

}
