/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum StorageClass implements CEnum {
    UNIFORM_CONSTANT(0),
    INPUT(1),
    UNIFORM(2),
    OUTPUT(3),
    WORKGROUP(4),
    CROSS_WORKGROUP(5),
    PRIVATE(6),
    FUNCTION(7),
    GENERIC(8),
    PUSH_CONSTANT(9),
    ATOMIC_COUNTER(10),
    IMAGE(11),
    STORAGE_BUFFER(12);

    final int value;
    
    private StorageClass(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    public static Optional<StorageClass> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
    
}
