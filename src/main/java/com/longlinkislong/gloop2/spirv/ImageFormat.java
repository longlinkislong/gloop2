/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum ImageFormat implements CEnum {
    UNKNOWN(0),
    RGBA32F(1),
    RGBA16F(2),
    R32F(3),
    RGBA8(4),
    RGBA8_SNORM(5),
    RG32F(6),
    RG16F(7),
    R11FG11FB10F(8),
    R16F(9),
    RGBA16(10),
    RGB10A2(11),
    RG16(12),
    RG8(13),
    R16(14),
    R8(15),
    RGBA16_SNORM(16),
    RG16_SNORM(17),
    RG8_SNORM(18),
    R16_SNORM(19),
    R8_SNORM(20),
    RGBA32I(21),
    RGBA16I(22),
    RGBA8I(23),
    R32I(24),
    RG32I(25),
    RG16I(26),
    RG8I(27),
    R16I(28),
    R8I(29),
    RGBA32UI(30),
    RGBA16UI(31),
    RGBA8UI(32),
    R32UI(33),
    RGB10A2UI(34),
    RG32UI(35),
    RG16UI(36),
    RG8UI(37),
    R16UI(38),
    R8UI(39);

    private final int value;

    private ImageFormat(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<ImageFormat> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
