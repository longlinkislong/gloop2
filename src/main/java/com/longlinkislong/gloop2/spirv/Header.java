/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 *
 * @author zmichaels
 */
public class Header {
    public static final int MAGIC_NUMBER = 0x07230203;
    private static final int REBMUN_CIGAM = 0x03022307;
    
    public final int magicNumber;
    public final int versionMajor;
    public final int versionMinor;
    public final int generator;
    public final int bound;
    public final int schema;
    public final ByteOrder order;
    
    private static ByteOrder swapOrder(final ByteOrder base) {
        return base == ByteOrder.BIG_ENDIAN
                ? ByteOrder.LITTLE_ENDIAN
                : ByteOrder.BIG_ENDIAN;
    }
    
    public Header(final ByteBuffer bytecode) throws IOException {
        switch(this.magicNumber = bytecode.getInt()) {
            case MAGIC_NUMBER:
                this.order = bytecode.order();
                break;
            case REBMUN_CIGAM:
                bytecode.order(this.order = swapOrder(bytecode.order()));
                break;
            default:
                throw new IOException("Bytecode is not Spir-V!");
        }
        
        final int version = bytecode.getInt();
        
        this.versionMajor = (version >> 16) & 0xFF;
        this.versionMinor = (version >> 8) & 0xFF;
        
        this.generator = bytecode.getInt();
        this.bound = bytecode.getInt();
        this.schema = bytecode.getInt();
    }
    
    @Override
    public String toString() {
        return new StringBuilder(4096)
                .append("{")
                .append("\"Magic Number\":").append(String.format("\"0x%x\",", this.magicNumber))
                .append("\"Version\":").append(String.format("\"%d.%d\",", this.versionMajor, this.versionMinor))
                .append("\"Generator\":").append(String.format("\"0x%x\",", this.generator))
                .append("\"Bound\":").append(this.bound).append(",")
                .append("\"Schema\":").append(this.schema)
                .append("}")
                .toString();
    }
}
