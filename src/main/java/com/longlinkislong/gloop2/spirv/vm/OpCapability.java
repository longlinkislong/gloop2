/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Capability;
import com.longlinkislong.gloop2.spirv.Instruction;

/**
 *
 * @author zmichaels
 */
public class OpCapability extends ExecutableInstruction {        
    public static final int OPCODE = 17;
    
    public OpCapability(final Instruction instruction) {
        super(instruction);
    }
    
    @Override
    public void execute(SpirVM vm) {        
        final int capId = this.instruction.payload.getInt();
        final Capability cap = Capability.of(capId).orElseThrow(() -> new UnsupportedOperationException("Unsupported Capability ID: " + capId));
        
        vm.capabilities.add(cap);
    }
    
}
