/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.vm.types.Type;
import com.longlinkislong.gloop2.spirv.vm.types.TypeDouble;
import com.longlinkislong.gloop2.spirv.vm.types.TypeFloat;
import com.longlinkislong.gloop2.spirv.vm.types.TypeHalf;

/**
 *
 * @author zmichaels
 */
public class OpTypeFloat extends ExecutableInstruction {
    public static final int OPCODE = 22;
    
    public OpTypeFloat(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int refId = this.instruction.payload.getInt();
        final int width = this.instruction.payload.getInt();
        final Type type;
        
        switch (width) {
            case 16:
                type = new TypeHalf();
                break;
            case 32:
                type = new TypeFloat();
                break;
            case 64:
                type = new TypeDouble();
                break;
            default:
                throw new UnsupportedOperationException("Unsupported type: " + width + "bit floating point");
        }
        
        vm.references[refId] = new TypeReference(refId, type);
    }
    
}
