/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.AddressingModel;
import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.MemoryModel;

/**
 *
 * @author zmichaels
 */
public class OpMemoryModel extends ExecutableInstruction {
    public static final int OPCODE = 14;

    public OpMemoryModel(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int amId = this.instruction.payload.getInt();
        final int mmId = this.instruction.payload.getInt();
        
        vm.addressingModel = AddressingModel.of(amId).orElseThrow(() -> new UnsupportedOperationException("Unsupported AddressingModel Id: " + amId));
        vm.memoryModel = MemoryModel.of(mmId).orElseThrow(() -> new UnsupportedOperationException("Unsupported MemoryModel Id: " + mmId));
    }
}
