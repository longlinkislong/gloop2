/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.StorageClass;
import com.longlinkislong.gloop2.spirv.vm.types.Type;

/**
 *
 * @author zmichaels
 */
public class VariableReference implements Reference, Value {
    public final StorageClass storageClass;
    public final Type type;
    public final int id;
    private Object value;
        
    public VariableReference(final int id, final Type type, final StorageClass storageClass) {
        this.id = id;
        this.storageClass = storageClass;
        this.type = type;
    }    
    
    @Override
    public Object getValue() {
        return this.value;
    }
    
    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public int getId() {
        return this.id;
    }
    
    @Override
    public String toString() {
        return new StringBuilder(1024)
                .append("{\"Type\":\"").append(type)
                .append("\",\"Storage Class\":\"").append(this.storageClass)
                .append("\",\"Value\":").append(this.value)
                .append("}")
                .toString();
    }
}
