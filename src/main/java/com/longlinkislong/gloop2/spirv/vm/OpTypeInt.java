/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.vm.types.Type;
import com.longlinkislong.gloop2.spirv.vm.types.TypeInt16;
import com.longlinkislong.gloop2.spirv.vm.types.TypeInt32;
import com.longlinkislong.gloop2.spirv.vm.types.TypeInt64;
import com.longlinkislong.gloop2.spirv.vm.types.TypeUInt16;
import com.longlinkislong.gloop2.spirv.vm.types.TypeUInt32;
import com.longlinkislong.gloop2.spirv.vm.types.TypeUInt64;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public class OpTypeInt extends ExecutableInstruction {

    public static final int OPCODE = 21;

    public OpTypeInt(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int refId = this.instruction.payload.getInt();
        final int width = this.instruction.payload.getInt();
        final int signedness = this.instruction.payload.getInt();

        final Type type;

        if (signedness == 1) {
            switch (width) {                
                case 16:
                    type = new TypeInt16();
                    break;
                case 32:
                    type = new TypeInt32();
                    break;
                case 64:
                    type = new TypeInt64();
                default:
                    throw new UnsupportedOperationException("Unsupported type: " + width + "bit signed integer");
            }
        } else {
            switch (width) {
                case 16:
                    type = new TypeInt16();
                    break;
                case 32:
                    type = new TypeInt32();
                    break;
                case 64:
                    type = new TypeInt64();
                    break;
                default:
                    throw new UnsupportedOperationException("Unsupported type: " + width + "bit unsigned integer");
            }
        }
        
        vm.references[refId] = new TypeReference(refId, type);
    }

}
