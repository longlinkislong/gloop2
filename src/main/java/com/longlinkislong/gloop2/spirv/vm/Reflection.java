/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo;
import com.longlinkislong.gloop2.gl.GLVertexFormat;
import com.longlinkislong.gloop2.spirv.Decoration;
import com.longlinkislong.gloop2.spirv.ExecutionModel;
import com.longlinkislong.gloop2.spirv.StorageClass;
import com.longlinkislong.gloop2.spirv.vm.types.TypePointer;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author zmichaels
 */
public class Reflection {

    private final SpirVM vm;

    public Reflection(final SpirVM vm) {
        this.vm = vm;

        try {
            while (true) {
                vm.step();
            }
        } catch (ExecutionCompleteException ex) {

        }
    }

    public List<GLVertexArrayInfo.AttributeDescription> getVertexAttributes() {
        assert this.vm.entryPoints.get(0).executionModel == ExecutionModel.VERTEX : "Executable does not encode for a Vertex Shader!";

        final List<GLVertexArrayInfo.AttributeDescription> out = vm.debugSymbols()
                .map(symbol -> {
                    final Reference ref = vm.getReference(symbol);

                    if (ref instanceof VariableReference) {
                        final VariableReference var = (VariableReference) ref;
                        final Optional<DecorationAnnotation> decoration = vm.getDecoration(ref);
                        
                        if (decoration.isPresent() && decoration.get().decoration == Decoration.LOCATION && var.storageClass == StorageClass.INPUT) {
                            final int location = decoration.get().literals.getInt(0);
                            final GLVertexFormat format = com.longlinkislong.gloop2.spirv.vm.Tools.vertexFormat((TypePointer) var.type);
                                                        
                            return new GLVertexArrayInfo.AttributeDescription(location, 0, format, 0);
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        return Tools.copyList(out);
    }
}
