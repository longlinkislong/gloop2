/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.SourceLanguage;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class Source {
    public final SourceLanguage sourceLanguage;
    public final int version;
    public String file;
    List<String> extensions = new ArrayList<>();
    String source;
    
    public Source(final SourceLanguage sourceLanguage, final int version, final String file) {
        this.sourceLanguage = sourceLanguage;
        this.version = version;
        this.file = file;
    }
    
    public List<String> getExtensions() {
        return com.longlinkislong.gloop2.Tools.copyList(extensions);
    }
    
    public String getSource() {
        return this.source;
    }
    
    @Override
    public String toString() {
        final StringBuilder out = new StringBuilder(8192)
                .append("{")
                .append("\"Source Language\":\"").append(sourceLanguage)
                .append("\",\"Version\":").append(version)
                .append(",\"File\":\"").append(file)
                .append("\",\"Extensions\":[");
        
        for (int i = 0; i < extensions.size(); i++) {
            out.append("\"").append(extensions.get(i)).append("\"");
            
            if (i < extensions.size() - 1) {
                out.append(",");
            }
        }
        
        return out.append("]}").toString();
    }
}
