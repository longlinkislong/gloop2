/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm.types;

import com.longlinkislong.gloop2.spirv.StorageClass;

/**
 *
 * @author zmichaels
 */
public class TypePointer implements Type {
    public final Type type;
    public final StorageClass storageClass;
    
    public TypePointer(final StorageClass storageClass, final Type type) {
        this.storageClass = storageClass;
        this.type = type;
    }
    
    @Override
    public String toString() {
        return "*" + type;
    }
}
