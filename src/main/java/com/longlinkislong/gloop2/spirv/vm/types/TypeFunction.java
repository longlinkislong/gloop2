/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm.types;

import com.longlinkislong.gloop2.Tools;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class TypeFunction implements Type{
    public final Type returnType;
    public final List<Type> parameterTypes;
    
    public TypeFunction(final Type returnType, final List<Type> parameterTypes) {
        this.returnType = returnType;
        this.parameterTypes = Tools.copyList(parameterTypes);
    }    
    
    @Override
    public String toString() {
        final StringBuilder out = new StringBuilder(1024)
                .append("(")
                .append(returnType)
                .append(")(");
        
        for (int i = 0; i < parameterTypes.size(); i++) {
            out.append(parameterTypes.get(i));
            
            if (i < parameterTypes.size() - 1) {
                out.append(",");
            }
        }
        
        return out.append(")").toString();
    }
}
