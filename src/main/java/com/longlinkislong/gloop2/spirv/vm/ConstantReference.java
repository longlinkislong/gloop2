/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

/**
 *
 * @author zmichaels
 */
public class ConstantReference implements Reference, Value {

    public final int id;
    public final Object value;

    public ConstantReference(final int id, final Object value) {
        this.id = id;
        this.value = value;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public Object getValue() {
        return this.value;
    }

}
