/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

/**
 *
 * @author zmichaels
 */
public class MemberName {
    public final ForwardReference type;
    public final int member;
    public final String name;
    
    public MemberName(final ForwardReference type, final int member, final String name) {
        this.type = type;
        this.member = member;
        this.name = name;
    }
    
    @Override
    public String toString() {
        return new StringBuilder(1024)
                .append("{")
                .append("\"Type\":").append(type)
                .append(",\"Member\":").append(member)
                .append(",\"Name\":\"").append(name).append("\"")
                .append("}")
                .toString();
    }
}
