/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.vm.types.Type;
import com.longlinkislong.gloop2.spirv.vm.types.TypeStruct;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class OpTypeStruct extends ExecutableInstruction {

    public static final int OPCODE = 30;

    public OpTypeStruct(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int refId = this.instruction.payload.getInt();
        final List<Type> memberTypes = new ArrayList<>();
        
        while (this.instruction.payload.hasRemaining()) {
            final int memberTypeId = this.instruction.payload.getInt();
            final TypeReference memberTypeRef = (TypeReference) vm.references[memberTypeId];
            final Type memberType = memberTypeRef.type;
            
            memberTypes.add(memberType);
        }
        
        vm.references[refId] = new TypeReference(refId, new TypeStruct(memberTypes));
    }

}
