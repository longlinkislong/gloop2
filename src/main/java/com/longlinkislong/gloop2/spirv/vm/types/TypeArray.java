/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm.types;

/**
 *
 * @author zmichaels
 */
public class TypeArray implements Type {
    public final Type elementType;
    public final int length;
    
    public TypeArray(final Type elementType, final int length) {
        this.elementType = elementType;
        this.length = length;
    }
    
    @Override
    public String toString() {
        return String.format("%s[%d]", elementType, length);
    }
}
