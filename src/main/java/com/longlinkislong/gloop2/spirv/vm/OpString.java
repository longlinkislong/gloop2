/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;

/**
 *
 * @author zmichaels
 */
public class OpString extends ExecutableInstruction {
    public static final int OPCODE = 7;
    
    public OpString(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int refId = this.instruction.payload.getInt();
        final String str = SpirVM.decodeString(this.instruction.payload);
        
        vm.references[refId] = new StringReference(refId, str);
    }
    
}
