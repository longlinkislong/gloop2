/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.vm.types.Type;

/**
 *
 * @author zmichaels
 */
public class TypeReference implements Reference {
    public final Type type;
    public final int id;
    
    public TypeReference(final int id, final Type type) {
        this.id = id;
        this.type = type;
    }
    
    @Override
    public int getId() {
        return this.id;
    }
    
    @Override
    public String toString() {
        return new StringBuilder(1024)
                .append("{")
                .append("\"Id\":").append(this.id)
                .append(",\"Type\":\"").append(this.type.getClass().getSimpleName())
                .append("\"}")
                .toString();
    }
}
