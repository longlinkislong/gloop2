/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;

/**
 *
 * @author zmichaels
 */
public class OpSourceContinued extends ExecutableInstruction {
    public static final int OPCODE = 2;
    
    public OpSourceContinued(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        vm.source.source += SpirVM.decodeString(this.instruction.payload);
    }
    
}
