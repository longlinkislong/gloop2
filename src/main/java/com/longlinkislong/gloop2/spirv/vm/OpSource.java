/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.SourceLanguage;

/**
 *
 * @author zmichaels
 */
public class OpSource extends ExecutableInstruction {
    public static final int OPCODE = 3;
    
    public OpSource(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int srcLangId = this.instruction.payload.getInt();
        final int version = this.instruction.payload.getInt();
        final String file;
        String src;
        
        if (this.instruction.payload.hasRemaining()) {
            final int fileId = this.instruction.payload.getInt();
            final StringReference fileNameRef = (StringReference) vm.references[fileId];
            
            if (fileNameRef != null) {
                file = fileNameRef.value;
            } else {
                file = "";
            }
        } else {
            file = "";
        }
        
        if (this.instruction.payload.hasRemaining()) {
            src = SpirVM.decodeString(this.instruction.payload);
        } else {
            src = "";
        }
        
        final SourceLanguage srcLang = SourceLanguage.of(srcLangId).orElseThrow(() -> new UnsupportedOperationException("Unsupported SourceLanguage Id: " + srcLangId));
        
        vm.source = new Source(srcLang, version, file);
        vm.source.source = src;
    }
    
}
