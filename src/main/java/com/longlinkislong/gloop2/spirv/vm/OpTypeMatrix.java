/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.vm.types.TypeMatrix;
import com.longlinkislong.gloop2.spirv.vm.types.TypeVector;

/**
 *
 * @author zmichaels
 */
public class OpTypeMatrix extends ExecutableInstruction {
    public static final int OPCODE = 24;
    
    public OpTypeMatrix(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int refId = this.instruction.payload.getInt();
        final int columnTypeId = this.instruction.payload.getInt();
        final int columnCount = this.instruction.payload.getInt();
        
        final TypeVector columnType = (TypeVector) ((TypeReference) vm.references[columnTypeId]).type;
        
        vm.references[refId] = new TypeReference(refId, new TypeMatrix(columnType, columnCount));
    }
    
}
