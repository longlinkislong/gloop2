/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;

/**
 *
 * @author zmichaels
 */
public class OpMemberName extends ExecutableInstruction {

    public static final int OPCODE = 6;

    public OpMemberName(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int typeId = this.instruction.payload.getInt();
        final int memId = this.instruction.payload.getInt();
        final String name = SpirVM.decodeString(this.instruction.payload);

        vm.dbgMemberNames.add(new MemberName(new ForwardReference(typeId, vm), memId, name));
    }

}
