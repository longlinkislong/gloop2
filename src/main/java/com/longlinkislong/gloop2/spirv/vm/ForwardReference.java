/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import java.util.function.Supplier;

/**
 *
 * @author zmichaels
 */
public class ForwardReference implements Reference {
    private final int id;
    private final Supplier<Reference> forward;
    
    public ForwardReference(final int id, final SpirVM vm) {
        this.id = id;
        this.forward = () -> vm.references[id];
    }
    
    public Reference get() {
        return this.forward.get();
    }
    
    @Override
    public int getId() {
        return this.id;
    }
    
    @Override
    public String toString() {
        final Reference fwd = this.get();
        
        if (fwd == null) {
            return String.format("{\"Type\":\"Forward Ref\",\"Id\":%d}", this.id);
        } else {
            return fwd.toString();
        }
    }    
}
