/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.spirv.AddressingModel;
import com.longlinkislong.gloop2.spirv.Capability;
import com.longlinkislong.gloop2.spirv.Header;
import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.MemoryModel;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class SpirVM {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SpirVM.class);
    public static final int STAGE1 = 0;
    public static final int STAGE2 = 1;
    public static final int STAGE3 = 2;
    public static final int STAGE4 = 3;
    public static final int STAGE5 = 4;
    public static final int STAGE6 = 5;
    public static final int STAGE7A = 6;
    public static final int STAGE7B = 7;
    public static final int STAGE8 = 8;
    public static final int STAGE9 = 9;
    public static final int STAGE10 = 10;
    public static final int STAGE11 = 11;
    public static final int STAGE_END = 12;
    
    public final Header header;
    
    AddressingModel addressingModel;
    MemoryModel memoryModel;
    final List<Capability> capabilities = new ArrayList<>();
    final List<String> extensions = new ArrayList<>();
    final List<EntryPoint> entryPoints = new ArrayList<>();
    final Map<String, Reference> dbgNames = new HashMap<>();
    final List<MemberName> dbgMemberNames = new ArrayList<>();
    final Reference[] references;
    final DecorationAnnotation[] decorations;
    Source source;
    
    private final ByteBuffer bytecode;
    private int stage;
    
    public SpirVM(final ByteBuffer bytecode) throws IOException {
        this.bytecode = bytecode.asReadOnlyBuffer();
        this.header = new Header(this.bytecode);
        this.references = new Reference[this.header.bound];
        this.decorations = new DecorationAnnotation[this.header.bound];
        
        this.reset();
    }
    
    public Stream<DecorationAnnotation> decorations() {
        return Arrays.stream(this.decorations)
                .filter(Objects::nonNull);
    }
    
    public Optional<DecorationAnnotation> getDecoration(final Reference ref) {
        if (ref == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(this.decorations[ref.getId()]);
    }
    
    public Optional<Source> getSource() {
        return Optional.ofNullable(this.source);
    }
    
    public String getStageName() {
        switch (this.stage) {
            case STAGE1:
                return "Stage1";
            case STAGE2:
                return "Stage2";
            case STAGE3:
                return "Stage3";
            case STAGE4:
                return "Stage4";
            case STAGE5:
                return "Stage5";
            case STAGE6:
                return "Stage6";
            case STAGE7A:
                return "Stage7a";
            case STAGE7B:
                return "Stage7b";
            case STAGE8:
                return "Stage8";
            case STAGE9:
                return "Stage9";
            case STAGE10:
                return "Stage10";
            case STAGE11:
                return "Stage11";
            default:
                return "ERROR";
        }
    }
    
    public int getStage() {
        return this.stage;
    }
    
    public Stream<String> debugSymbols() {
        return this.dbgNames.keySet().stream();
    }
    
    public Reference getReference(final String dbgSymbol) {
        return this.references[this.dbgNames.get(dbgSymbol).getId()];
    }
    
    public Stream<Reference> references() {
        return Arrays.stream(this.references)
                .filter(Objects::nonNull);
    }
    
    public Stream<EntryPoint> entryPoints() {
        return this.entryPoints.stream();
    }
    
    public Stream<String> extensions() {
        return this.extensions.stream();
    }
    
    public MemoryModel getMemoryModel() {
        return this.memoryModel;
    }
    
    public AddressingModel getAddressingModel() {
        return this.addressingModel;
    }
    
    public void reset() {
        this.stage = STAGE1;
        this.capabilities.clear();
        
        for (int i = 0; i < references.length; i++) {
            references[i] = null;
        }
        
        this.extensions.clear();
        this.addressingModel = null;
        this.memoryModel = null;
    }
    
    private Instruction fetch() throws ExecutionCompleteException {
        if (this.bytecode.hasRemaining()) {
            return new Instruction(this.bytecode);
        } else {
            throw new ExecutionCompleteException();
        }
    }
    
    private ExecutableInstruction decode(final Instruction instruction) throws StageCompleteException, ExecutionCompleteException {        
        switch (this.stage) {
            case STAGE1:
                switch (instruction.opcode) {
                    case OpCapability.OPCODE:
                        return new OpCapability(instruction);
                    default:
                        throw new StageCompleteException();
                }
            case STAGE2:
                switch (instruction.opcode) {
                    case OpExtension.OPCODE:
                        return new OpExtension(instruction);
                    default:
                        throw new StageCompleteException();
                }
            case STAGE3:
                switch (instruction.opcode) {
                    case OpExtInstImport.OPCODE:
                        return new OpExtInstImport(instruction);
                    default:
                        throw new StageCompleteException();
                }
            case STAGE4:
                switch (instruction.opcode) {
                    case OpMemoryModel.OPCODE:
                        return new OpMemoryModel(instruction);
                    default:
                        throw new StageCompleteException();
                }
            case STAGE5: {                
                switch (instruction.opcode) {
                    case OpEntryPoint.OPCODE:
                        return new OpEntryPoint(instruction);
                    default:
                        throw new StageCompleteException();
                }
            }
            case STAGE6: {                
                switch (instruction.opcode) {
                    case OpExecutionMode.OPCODE:
                        return new OpExecutionMode(instruction);
                    default:                        
                        throw new StageCompleteException();
                }
            }
            case STAGE7A: {
                switch (instruction.opcode) {
                    case OpSource.OPCODE:
                        return new OpSource(instruction);
                    case OpSourceContinued.OPCODE:
                        return new OpSourceContinued(instruction);
                    case OpSourceExtension.OPCODE:
                        return new OpSourceExtension(instruction);
                    case OpString.OPCODE:
                        return new OpString(instruction);
                    default:
                        throw new StageCompleteException();
                }
            }
            case STAGE7B: {
                switch (instruction.opcode) {
                    case OpMemberName.OPCODE:
                        return new OpMemberName(instruction);
                    case OpName.OPCODE:
                        return new OpName(instruction);
                    default:
                        throw new StageCompleteException();
                }
            }
            case STAGE8: {
                switch (instruction.opcode) {
                    case OpDecorate.OPCODE:
                        return new OpDecorate(instruction);
                    case 72:
                    case 73:
                    case 74:
                    case 75:
                        LOGGER.warn("[{}]: Instruction not yet implemented: {}", this.getStageName(), instruction);
                        return new OpNop(instruction);
                    default:
                        throw new StageCompleteException();
                }
            }
            case STAGE9: {
                switch (instruction.opcode) {
                    case OpTypeVoid.OPCODE:
                        return new OpTypeVoid(instruction);
                    case OpTypeBool.OPCODE:
                        return new OpTypeBool(instruction);
                    case OpTypeInt.OPCODE:
                        return new OpTypeInt(instruction);
                    case OpTypeFloat.OPCODE:
                        return new OpTypeFloat(instruction);
                    case OpTypeVector.OPCODE:
                        return new OpTypeVector(instruction);
                    case OpTypeMatrix.OPCODE:
                        return new OpTypeMatrix(instruction);
                    case OpTypeFunction.OPCODE:
                        return new OpTypeFunction(instruction);
                    case OpTypePointer.OPCODE:
                        return new OpTypePointer(instruction);
                    case OpVariable.OPCODE:
                        return new OpVariable(instruction);
                    case OpConstant.OPCODE:
                        return new OpConstant(instruction);
                    case OpTypeArray.OPCODE:
                        return new OpTypeArray(instruction);
                    case OpTypeStruct.OPCODE:
                        return new OpTypeStruct(instruction);
                    case 25:
                    case 26:
                    case 27:
                    case 29:
                    case 31:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 41:
                    case 42:
                    case 44:
                    case 45:
                    case 46:
                    case 48:
                    case 49:
                    case 50:
                    case 51:
                    case 52:
                    case 1:
                    case 8:                        
                        LOGGER.warn("[{}]: Instruction not yet implemented {}", this.getStageName(), instruction);
                        return new OpNop(instruction);
                    default:
                        throw new StageCompleteException();
                }
            }
            case STAGE10:
            case STAGE11:
                throw new StageCompleteException();
            case STAGE_END:
                throw new ExecutionCompleteException();
            default:
                throw new IllegalStateException("SpirVM decode stage: [" + this.stage + "] is invalid!");
        }
    }
    
    public void step() throws ExecutionCompleteException {
        final Instruction rawInstruction = this.fetch();
        ExecutableInstruction instruction;
        
        while (true) {
            try {
                instruction = this.decode(rawInstruction);
                break;
            } catch (StageCompleteException ex) {
                this.stage++;
            }
        }
        
        instruction.execute(this);
    }
    
    public static String decodeString(final ByteBuffer bytecode) {
        final CharBuffer strBuffer = StandardCharsets.UTF_8.decode(bytecode.slice());
        final StringBuilder strBuilder = new StringBuilder(strBuffer.remaining());
        
        int offset = bytecode.position();
        while (strBuffer.hasRemaining()) {
            offset += 4;
            
            final char c0 = strBuffer.get();
            final char c1 = strBuffer.get();
            final char c2 = strBuffer.get();
            final char c3 = strBuffer.get();
            
            if (c0 == '\0') {
                break;
            }
            
            strBuilder.append(c0);
            
            if (c1 == '\0') {
                break;
            }
            
            strBuilder.append(c1);
            
            if (c2 == '\0') {
                break;
            }
            
            strBuilder.append(c2);
            
            if (c3 == '\0') {
                break;
            }
            
            strBuilder.append(c3);
        }
        
        bytecode.position(offset);
        
        return strBuilder.toString();
    }
}
