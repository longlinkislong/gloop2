/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Decoration;
import java.nio.ByteBuffer;

/**
 *
 * @author zmichaels
 */
public class DecorationAnnotation {
    public final ForwardReference target;
    public final Decoration decoration;
    public final ByteBuffer literals;
    
    public DecorationAnnotation(final ForwardReference target, final Decoration decoration, final ByteBuffer literals) {
        this.target = target;
        this.decoration = decoration;
        this.literals = literals;
    }
    
    @Override
    public String toString() {
        final StringBuilder out = new StringBuilder(4096)
                .append("{")
                .append("\"Target\":").append(target)
                .append(",\"Decoration\":\"").append(decoration)
                .append("\"");
        
        if (literals != null) {
            out.append(",\"Literals\":[");
            
            while (literals.hasRemaining()) {
                out.append(String.format("\"0x%x\"", literals.getInt()));
                
                if (literals.hasRemaining()) {
                    out.append(",");
                }
            }
            
            out.append("]");
            
            literals.flip();
        }
        
        return out.append("}").toString();
    }
}
