/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Decoration;
import com.longlinkislong.gloop2.spirv.Instruction;
import java.nio.ByteBuffer;

/**
 *
 * @author zmichaels
 */
public class OpDecorate extends ExecutableInstruction {
    public static final int OPCODE = 71;
    
    public OpDecorate(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int tgtId = this.instruction.payload.getInt();
        final int decId = this.instruction.payload.getInt();
        final ByteBuffer literals = ByteBuffer.allocate(this.instruction.payload.remaining())
                .order(this.instruction.payload.order());
        
        literals.put(this.instruction.payload).flip();
        
        final Decoration dec = Decoration.of(decId).orElseThrow(() -> new UnsupportedOperationException("Unsupported Decoration Id: " + decId));
        final ForwardReference ref = new ForwardReference(tgtId, vm);
        
        vm.decorations[tgtId] = new DecorationAnnotation(ref, dec, literals);        
    }
    
}
