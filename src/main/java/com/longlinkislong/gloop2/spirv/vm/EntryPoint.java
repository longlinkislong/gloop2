/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.spirv.ExecutionMode;
import com.longlinkislong.gloop2.spirv.ExecutionModel;
import java.nio.ByteBuffer;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class EntryPoint {
    public final ExecutionModel executionModel;
    public final ForwardReference entryPoint;
    public final String name;
    public final List<ForwardReference> iface;
    ExecutionMode executionMode;
    ByteBuffer literals;
    
    public EntryPoint(
            final ExecutionModel executionModel,
            final ForwardReference entryPoint,
            final String name, 
            final List<ForwardReference> iface) {
        
        this.executionModel = executionModel;
        this.entryPoint = entryPoint;
        this.name = name;
        this.iface = Tools.copyList(iface);
    }
    
    @Override
    public String toString() {
        final StringBuilder out = new StringBuilder(4096)
                .append("{")
                .append("\"Execution Model\":\"").append(this.executionModel)
                .append("\",\"Execution Mode\":\"").append(this.executionMode);
        
        if (this.literals != null) {
            out.append("\",\"Literals\":[");
            
            while (literals.hasRemaining()) {                
                final String fmt = String.format("\"0x%x\"", this.literals.getInt());
                
                out.append(fmt);
            }
            
            out.append("]");
        }
        
        return out
                .append("\",\"Name\":").append(this.name)
                .append("\",\"Interface\":").append(this.iface)
                .append("}")
                .toString();
    }
}
