/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.ExecutionMode;
import com.longlinkislong.gloop2.spirv.Instruction;
import java.nio.ByteBuffer;

/**
 *
 * @author zmichaels
 */
public class OpExecutionMode extends ExecutableInstruction {
    public static final int OPCODE = 16;
    public OpExecutionMode(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int epId = this.instruction.payload.getInt();
        final int emId = this.instruction.payload.getInt();
        final ByteBuffer literals = ByteBuffer.allocate(this.instruction.payload.remaining())
                .order(this.instruction.payload.order());
        
        literals.put(this.instruction.payload).flip();
        
        final ExecutionMode em = ExecutionMode.of(emId).orElseThrow(() -> new UnsupportedOperationException("Unsupported ExecutionMode Id: " + emId));
        
        final EntryPoint entryPoint = vm.entryPoints.stream()
                .filter(ep -> ep.entryPoint.getId() == epId)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("OpExecutionMode references an EntryPoint not defined by OpEntryPoint!"));
        
        entryPoint.executionMode = em;
        entryPoint.literals = literals;
    }
    
}
