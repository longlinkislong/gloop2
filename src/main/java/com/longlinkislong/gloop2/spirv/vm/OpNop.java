/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;

/**
 *
 * @author zmichaels
 */
public class OpNop extends ExecutableInstruction {

    public OpNop(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
    }
    
}
