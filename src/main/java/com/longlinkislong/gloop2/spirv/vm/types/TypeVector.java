/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm.types;

/**
 *
 * @author zmichaels
 */
public class TypeVector implements Type {
    public final TypeScalar componentType;
    public final int componentCount;
    
    public TypeVector(final TypeScalar componentType, final int componentCount) {
        this.componentType = componentType;
        this.componentCount = componentCount;
    }
    
    @Override
    public String toString() {
        if (componentType instanceof TypeFloat) {
            return "vec" + this.componentCount;
        } else if (componentType instanceof TypeInt32) {
            return "ivec" + this.componentCount;
        } else if (componentType instanceof TypeUInt32) {
            return "uivec" + this.componentCount;
        } else {
            return super.toString();
        }
    }
}
