/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;

/**
 *
 * @author zmichaels
 */
public class OpExtension extends ExecutableInstruction {
    public static final int OPCODE = 10;
    
    public OpExtension(Instruction instruction) {
        super(instruction);
    }   
    
    @Override
    public void execute(SpirVM vm) {
        vm.extensions.add(SpirVM.decodeString(this.instruction.payload));        
    }
    
}
