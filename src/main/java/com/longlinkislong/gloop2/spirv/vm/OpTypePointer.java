/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.StorageClass;
import com.longlinkislong.gloop2.spirv.vm.types.Type;
import com.longlinkislong.gloop2.spirv.vm.types.TypePointer;

/**
 *
 * @author zmichaels
 */
public class OpTypePointer extends ExecutableInstruction {
    public static final int OPCODE = 32;
    
    public OpTypePointer(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {        
        final int refId = this.instruction.payload.getInt();
        final int scId = this.instruction.payload.getInt();
        final int typeId = this.instruction.payload.getInt();
        
        final StorageClass sc = StorageClass.of(scId).orElseThrow(() -> new UnsupportedOperationException("Unsupported StorageClass Id: " + scId));
        final Type type = ((TypeReference) vm.references[typeId]).type;
        
        vm.references[refId] = new TypeReference(refId, new TypePointer(sc, type));
    }
    
}
