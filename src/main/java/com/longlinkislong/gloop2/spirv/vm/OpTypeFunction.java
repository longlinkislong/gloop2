/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.vm.types.Type;
import com.longlinkislong.gloop2.spirv.vm.types.TypeFunction;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class OpTypeFunction extends ExecutableInstruction {
    public static final int OPCODE = 33;
    
    public OpTypeFunction(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int refId = this.instruction.payload.getInt();
        final int returnTypeId = this.instruction.payload.getInt();
        
        final Type returnType = ((TypeReference) vm.references[returnTypeId]).type;
        final List<Type> parameterTypes = new ArrayList<>();
        
        while (this.instruction.payload.hasRemaining()) {
            final int paramId = this.instruction.payload.getInt();
            final Type paramType = ((TypeReference) vm.references[paramId]).type;
            
            parameterTypes.add(paramType);
        }
        
        vm.references[refId] = new TypeReference(refId, new TypeFunction(returnType, parameterTypes));
    }
    
}
