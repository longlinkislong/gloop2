/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.StorageClass;
import com.longlinkislong.gloop2.spirv.vm.types.Type;
import com.longlinkislong.gloop2.spirv.vm.types.TypePointer;

/**
 *
 * @author zmichaels
 */
public class OpVariable extends ExecutableInstruction {
    public static final int OPCODE = 59;
    
    public OpVariable(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int resultTypeId = this.instruction.payload.getInt();
        final int refId = this.instruction.payload.getInt();
        final int scId = this.instruction.payload.getInt();        
        final Type resultType = ((TypeReference) vm.references[resultTypeId]).type;
        final StorageClass sc = StorageClass.of(scId).orElseThrow(() -> new UnsupportedOperationException("Unsupported StorageClass Id: " + scId));
        
        assert resultType instanceof TypePointer : "ResultType must be a Pointer!";
        assert sc != StorageClass.GENERIC : "StorageClass cannot be generic!";
        
        final VariableReference out = new VariableReference(refId, resultType, sc);
        
        if (this.instruction.payload.hasRemaining()) {
            final int initializerId = this.instruction.payload.getInt();
            final VariableReference initializer = (VariableReference) vm.references[initializerId];
            
            out.setValue(initializer.getValue());
        }                
        
        vm.references[refId] = out;
    }
}
