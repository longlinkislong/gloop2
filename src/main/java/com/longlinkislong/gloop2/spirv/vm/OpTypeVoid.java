/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.vm.types.TypeVoid;

/**
 *
 * @author zmichaels
 */
public class OpTypeVoid extends ExecutableInstruction {
    public static final int OPCODE = 19;
    
    public OpTypeVoid(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int refId = this.instruction.payload.getInt();        
        
        vm.references[refId] = new TypeReference(refId, new TypeVoid());
    }
    
}
