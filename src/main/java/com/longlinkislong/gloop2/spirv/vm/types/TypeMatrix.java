/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm.types;

/**
 *
 * @author zmichaels
 */
public class TypeMatrix implements Type {

    public final TypeVector columnType;
    public final int columnCount;

    public TypeMatrix(final TypeVector columnType, final int columnCount) {
        this.columnType = columnType;
        this.columnCount = columnCount;
    }

    @Override
    public String toString() {

        if (columnType.componentType instanceof TypeFloat) {
            if (columnType.componentCount == columnCount) {
                return "mat" + columnCount;
            } else {
                return "mat" + columnCount + "x" + columnType.componentCount;
            }
        } else {
           return super.toString();
        }
    }
}
