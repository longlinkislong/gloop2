/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.vm.types.TypeScalar;
import com.longlinkislong.gloop2.spirv.vm.types.TypeVector;

/**
 *
 * @author zmichaels
 */
public class OpTypeVector extends ExecutableInstruction {
    public static final int OPCODE = 23;
    
    public OpTypeVector(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int refId = this.instruction.payload.getInt();
        final int compTypeId = this.instruction.payload.getInt();
        final int compCount = this.instruction.payload.getInt();        
        final TypeScalar compType = (TypeScalar) ((TypeReference) vm.references[compTypeId]).type;
                
        vm.references[refId] = new TypeReference(refId, new TypeVector(compType, compCount));
    }
    
}
