/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm.types;

import com.longlinkislong.gloop2.Tools;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class TypeStruct implements Type {
    public final List<Type> memberTypes;
    
    public TypeStruct(final List<Type> memberTypes) {
        this.memberTypes = Tools.copyList(memberTypes);
    }
    
    @Override
    public String toString() {
        return memberTypes.toString();
    }
}
