/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

/**
 *
 * @author zmichaels
 */
public class StringReference implements Reference {
    public final int refId;
    public final String value;
    
    public StringReference(final int refId, final String value) {
        this.refId = refId;
        this.value = value;
    }
    
    @Override
    public int getId() {
        return this.refId;
    }
    
    @Override
    public String toString() {
        return new StringBuilder(4096)
                .append("{")
                .append("\"id\":").append(this.refId)
                .append(",\"Value\":\"").append(this.value)
                .append("\"}")
                .toString();
    }
    
}
