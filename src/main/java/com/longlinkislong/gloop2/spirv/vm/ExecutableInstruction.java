/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;

/**
 *
 * @author zmichaels
 */
public abstract class ExecutableInstruction {
    protected final Instruction instruction;
    
    public ExecutableInstruction(final Instruction instruction) {
        this.instruction = instruction;
    }
    
    public abstract void execute(SpirVM vm);
}
