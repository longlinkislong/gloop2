/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.gl.GLVertexFormat;
import com.longlinkislong.gloop2.spirv.StorageClass;
import com.longlinkislong.gloop2.spirv.vm.types.TypeFloat;
import com.longlinkislong.gloop2.spirv.vm.types.TypeInt32;
import com.longlinkislong.gloop2.spirv.vm.types.TypePointer;
import com.longlinkislong.gloop2.spirv.vm.types.TypeUInt32;
import com.longlinkislong.gloop2.spirv.vm.types.TypeVector;

/**
 *
 * @author zmichaels
 */
public class Tools {
    public static GLVertexFormat vertexFormat(final TypePointer ptype) {
        assert ptype.storageClass == StorageClass.INPUT : "Vertex inputs must have INPUT StorageClass!";
        
        if (ptype.type instanceof TypeVector) {
            final TypeVector vec = (TypeVector) ptype.type;
            
            if (vec.componentType instanceof TypeUInt32) {
                switch(vec.componentCount) {
                    case 2:
                        return GLVertexFormat.UIVEC2;
                    case 3:
                        return GLVertexFormat.UIVEC3;
                    case 4:
                        return GLVertexFormat.UIVEC4;
                    default:
                        throw new UnsupportedOperationException("Unsupported vector type: uivec" + vec.componentCount);
                }
            } else if (vec.componentType instanceof TypeInt32) {
                switch (vec.componentCount) {
                    case 2:
                        return GLVertexFormat.IVEC2;
                    case 3:
                        return GLVertexFormat.IVEC3;
                    case 4:
                        return GLVertexFormat.IVEC4;
                    default:
                        throw new UnsupportedOperationException("Unsupported vector type: ivec" + vec.componentCount);
                }
            } else if (vec.componentType instanceof TypeFloat) {
                switch (vec.componentCount) {
                    case 2:
                        return GLVertexFormat.VEC2;
                    case 3:
                        return GLVertexFormat.VEC3;
                    case 4:
                        return GLVertexFormat.VEC4;
                    default:
                        throw new UnsupportedOperationException("Unsupported vector type: vec" + vec.componentCount);
                }
            } else {
                throw new UnsupportedOperationException("Unsupported vector component type: " + vec.componentType);
            }
        } else if (ptype.type instanceof TypeInt32) {
            return GLVertexFormat.INT;
        } else if (ptype.type instanceof TypeUInt32) {
            return GLVertexFormat.UINT;
        } else if (ptype.type instanceof TypeFloat) {
            return GLVertexFormat.FLOAT;
        } else {
            throw new UnsupportedOperationException("Unsupported input type: " + ptype.type);
        }
    }
}
