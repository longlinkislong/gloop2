/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;

/**
 *
 * @author zmichaels
 */
public class OpName extends ExecutableInstruction {
    public static final int OPCODE = 5;
    
    public OpName(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int tgtId = this.instruction.payload.getInt();
        final String name = SpirVM.decodeString(this.instruction.payload);
        
        vm.dbgNames.put(name, new ForwardReference(tgtId, vm));
    }
    
}
