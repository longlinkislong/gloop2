/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.ExecutionModel;
import com.longlinkislong.gloop2.spirv.Instruction;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public class OpEntryPoint extends ExecutableInstruction {
    public static final int OPCODE = 15;
    
    public OpEntryPoint(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int emId = this.instruction.payload.getInt();
        final int epId = this.instruction.payload.getInt();
        final String name = SpirVM.decodeString(this.instruction.payload);
        final List<ForwardReference> iface = new ArrayList<>();
        
        while (this.instruction.payload.hasRemaining()) {
            final int id = this.instruction.payload.getInt();
            final ForwardReference fref = new ForwardReference(id, vm);
            
            iface.add(fref);
        }
        
        final ExecutionModel em = ExecutionModel.of(emId).orElseThrow(() -> new UnsupportedOperationException("Unsupported ExecutionModel Id: " + emId));
        final ForwardReference ep = new ForwardReference(epId, vm);        
        
        vm.entryPoints.add(new EntryPoint(em, ep, name, iface));  
    }
    
}
