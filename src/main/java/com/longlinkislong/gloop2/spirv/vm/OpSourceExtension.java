/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;

/**
 *
 * @author zmichaels
 */
public class OpSourceExtension extends ExecutableInstruction {
    public static final int OPCODE = 4;
    
    public OpSourceExtension(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        vm.source.extensions.add(SpirVM.decodeString(this.instruction.payload));
    }
    
}
