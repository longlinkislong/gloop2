/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.vm.types.Type;
import com.longlinkislong.gloop2.spirv.vm.types.TypeArray;

/**
 *
 * @author zmichaels
 */
public class OpTypeArray extends ExecutableInstruction {
    public static final int OPCODE = 28;
    
    public OpTypeArray(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int refId = this.instruction.payload.getInt();
        final int elementTypeId = this.instruction.payload.getInt();
        final int lengthId = this.instruction.payload.getInt();        
        final TypeReference elementTypeRef = (TypeReference) vm.references[elementTypeId];
        final Type elementType = elementTypeRef.type;
        final Value lengthValue = (Value) vm.references[lengthId];
        final int length = ((Number) lengthValue.getValue()).intValue();
        
        vm.references[refId] = new TypeReference(refId, new TypeArray(elementType, length));
    }
    
}
