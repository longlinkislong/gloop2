/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv.vm;

import com.longlinkislong.gloop2.spirv.Instruction;
import com.longlinkislong.gloop2.spirv.vm.types.TypeDouble;
import com.longlinkislong.gloop2.spirv.vm.types.TypeFloat;
import com.longlinkislong.gloop2.spirv.vm.types.TypeInt16;
import com.longlinkislong.gloop2.spirv.vm.types.TypeInt32;
import com.longlinkislong.gloop2.spirv.vm.types.TypeInt64;
import com.longlinkislong.gloop2.spirv.vm.types.TypeScalar;
import com.longlinkislong.gloop2.spirv.vm.types.TypeUInt16;
import com.longlinkislong.gloop2.spirv.vm.types.TypeUInt32;
import com.longlinkislong.gloop2.spirv.vm.types.TypeUInt64;

/**
 *
 * @author zmichaels
 */
public class OpConstant extends ExecutableInstruction {
    public static final int OPCODE = 43;
    
    public OpConstant(Instruction instruction) {
        super(instruction);
    }

    @Override
    public void execute(SpirVM vm) {
        final int resultTypeId = this.instruction.payload.getInt();
        final int refId = this.instruction.payload.getInt();
        final TypeReference resultRef = (TypeReference) vm.references[resultTypeId];
        final TypeScalar resultType = (TypeScalar) resultRef.type;
        
        Object value;
        
        if (resultType instanceof TypeInt16) {
            value = (short) (this.instruction.payload.getInt() & 0xFFFF);            
        } else if (resultType instanceof TypeUInt16) {            
            value = (short) (this.instruction.payload.getInt() & 0x7FFF);            
        } else if (resultType instanceof TypeInt32) {
            value = this.instruction.payload.getInt();
        } else if (resultType instanceof TypeInt64) {
            value = this.instruction.payload.getLong();
        } else if (resultType instanceof TypeUInt32) {
            value = this.instruction.payload.getInt() & 0x7FFF_FFFF;
        } else if (resultType instanceof TypeUInt64) {
            value = this.instruction.payload.getLong() & 0x7FFF_FFFF_FFFF_FFFFL;
        } else if (resultType instanceof TypeFloat) {
            value = this.instruction.payload.getFloat();
        } else if (resultType instanceof TypeDouble) {
            value = this.instruction.payload.getDouble();
        } else {
            throw new UnsupportedOperationException("Unsupported Type: " + resultType.getClass().getSimpleName());
        }
        
        vm.references[refId] = new ConstantReference(refId, value);
    }
    
}
