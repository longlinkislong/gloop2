/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum ExecutionModel implements CEnum {
    VERTEX(0),
    TESSELLATION_CONTROL(1),
    TESSELLATION_EVALUATION(2),
    GEOMETRY(3),
    FRAGMENT(4),
    GL_COMPUTE(5),
    KERNEL(6);

    final int value;

    private ExecutionModel(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<ExecutionModel> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

}
