/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum FunctionControl implements CEnum {
    NONE(0),
    INLINE(0x1),
    DONT_INLINE(0x2),
    PURE(0x4),
    CONST(0x8);

    final int value;

    private FunctionControl(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<FunctionControl> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

}
