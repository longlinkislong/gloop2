/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;

/**
 *
 * @author zmichaels
 */
public enum SamplerFilterMode implements CEnum {
    NEAREST(0),
    LINEAR(1);

    final int value;
    
    private SamplerFilterMode(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

}
