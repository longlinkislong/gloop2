/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum ImageChannelDataType implements CEnum {
    SNORM_INT8(0),
    SNORM_INT16(1),
    UNORM_INT8(2),
    UNORM_INT16(3),
    UNORM_SHORT565(4),
    UNORM_SHORT555(5),
    UNORM_INT101010(6),
    SIGNED_INT8(7),
    SIGNED_INT16(8),
    SIGNED_INT32(9),
    UNSIGNED_INT8(10),
    UNSIGNED_INT16(11),
    UNSIGNED_INT32(12),
    HALF_FLOAT(13),
    FLOAT(14),
    UNORM_INT24(15),
    UNORM_INT101010_2(16);

    final int value;

    private ImageChannelDataType(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<ImageChannelDataType> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

}
