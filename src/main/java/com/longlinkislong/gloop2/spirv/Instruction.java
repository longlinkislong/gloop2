/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import java.nio.ByteBuffer;

/**
 *
 * @author zmichaels
 */
public class Instruction {

    public final int wordCount;
    public final int opcode;
    public final ByteBuffer payload;

    public Instruction(final ByteBuffer bin) {
        final int instr = bin.getInt();
        this.wordCount = (instr >> 16) & 0xFF;
        this.opcode = instr & 0xFF;

        if (this.wordCount == 1) {
            this.payload = null;
        } else {
            final int size = this.wordCount - 1;
                        
            this.payload = bin.slice();
            this.payload.limit(size * 4);
            this.payload.order(bin.order());
            
            bin.position(bin.position() + size * 4);
        }                
    }
    
    @Override
    public String toString() {
        final StringBuilder out = new StringBuilder()
                .append("{\"").append(Grammar.opcodeName(this.opcode)).append("\":");
        
        if (payload != null) {            
            out.append('[');
            
            final ByteBuffer readPayload = this.payload.asReadOnlyBuffer()
                    .order(this.payload.order());
            
            while (readPayload.hasRemaining()) {
                out.append("\"0x");
                out.append(Integer.toHexString(readPayload.getInt()));
                out.append("\"");
                if (readPayload.hasRemaining()) {
                    out.append(',');
                }                       
            }
            
            out.append(']');
        } else {
            out.append("[]");
        }
        
        out.append('}');
        
        return out.toString();
    }
}
