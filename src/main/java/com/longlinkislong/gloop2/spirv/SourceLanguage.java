/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum SourceLanguage implements CEnum {
    UNKNOWN(0),
    ESSL(1),
    GLSL(2),
    OPENCL_C(3),
    OPENCL_CPP(4),
    HLSL(5);
    
    public final int value;
    
    private SourceLanguage(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }
    
    public static Optional<SourceLanguage> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
