/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum GroupOperation implements CEnum {
    REDUCE(0),
    INCLUSIVE_SCAN(1),
    EXCLUSIVE_SCALE(2);

    final int value;

    private GroupOperation(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }
    
    public static Optional<GroupOperation> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

}
