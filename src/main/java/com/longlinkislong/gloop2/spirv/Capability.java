/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum Capability implements CEnum {
    MATRIX(0),
    SHADER(1),
    GEOMETRY(2),
    TESSELLATION(3),
    ADDRESSES(4),
    LINKAGE(5),
    KERNEL(6),
    VECTOR16(7),
    FLOAT16_BUFFER(8),
    FLOAT16(9),
    FLOAT64(10),
    INT64(11),
    INT64_ATOMICS(12),
    IMAGE_BASIC(13),
    IMAGE_READ_WRITE(14),
    IMAGE_MIPMAP(15),
    PIPES(17),
    GROUPS(18),
    DEVICE_ENQUEUE(19),
    LITERAL_SAMPLER(20),
    ATOMIC_STORAGE(21),
    INT16(22),
    TESSELLATION_POINT_SIZE(23),
    GEOMETRY_POINT_SIZE(24),
    IMAGE_GATHER_EXTENDED(25),
    STORAGE_IMAGE_MULTISAMPLE(27),
    UNIFORM_BUFFER_ARRAY_DYNAMIC_INDEXING_BLOCK(28),
    SAMPLED_IMAGE_ARRAY_DYNAMIC_INDEXING(29),
    STORAGE_BUFFER_ARRAY_DYNAMIC_INDEXING_BUFFER_BLOCK(30),
    STORAGE_IMAGE_ARRAY_DYNAMIC_INDEXING(31),
    CLIP_DISTANCE(32),
    CULL_DISTANCE(33),
    IMAGE_CUBE_ARRAY(34),
    SAMPLER_RAGE_SHADING(35),
    IMAGE_RECT(36),
    SAMPLED_RECT(37),
    GENERIC_POINTER(38),
    INT8(39),
    INPUT_ATTACHMENT(40),
    SPARSE_RESIDENCY(41),
    MIN_LOD(42),
    SAMPLED1D(43),
    IMAGE1D(44),
    SAMPLED_CUBE_ARRAY(45),
    SAMPLED_BUFFER(46),
    IMAGE_BUFFER(47),
    IMAGE_MS_ARRAY(48),
    STORAGE_IMAGE_EXTENDED_FORMAT(49),
    IMAGE_QUERY(50),
    DERIVATIVE_CONTROL(51),
    INTERPOLATED_FUNCTION(52),
    TRANSFORM_FEEDBACK(53),
    GEOMETRY_STREAMS(54),
    STORAGE_IMAGE_READ_WITHOUT_FORMAT(55),
    STORAGE_IMAGE_WRITE_WITHOUT_FORMAT(56),
    MULTI_VIEWPORT(57),
    SUBGROUP_BALLOT_KHR(4423),
    DRAW_PARAMETERS(4427),
    SUBGROUP_VOTE_KHR(4431),
    STORAGE_BUFFER_16BIT_ACCESS(4433),
    STORAGE_UNIFORM_BUFFER_BLOCK16(4433),
    UNIFORM_AND_STORAGE_BUFFER_16BIT_ACCESS(4434),
    STORAGE_UNIFORM16(4434),
    STORAGE_PUSH_CONSTANT16(4435),
    STORAGE_INPUT_OUTPUT16(4436),
    DEVICE_GROUP(4437),
    MULTI_VIEW(4439),
    VARIABLE_POINTERS_STORAGE_BUFFER(4441),
    VARIABLE_POINTERS(4442),
    SAMPLE_MASK_OVERRIDE_COVERAGE_NV(5249),
    GEOMETRY_SHADER_PASSTHROUGH_NV(5251),
    SHADER_VIEWPORT_INDEX_LAYER_NV(5254),
    SHADER_VIEWPORT_MASK_NV(5255),
    SHADER_STEREO_VIEW_NV(5259),
    PER_VIEW_ATTRIBUTES_NV(5260);

    final int value;

    private Capability(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<Capability> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
