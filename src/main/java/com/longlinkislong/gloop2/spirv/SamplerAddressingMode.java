/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;

/**
 *
 * @author zmichaels
 */
public enum SamplerAddressingMode implements CEnum {
    NONE(0),
    CLAMP_TO_EDGE(1),
    CLAMP(2),
    REPEAT(3),
    REPEAT_MIRRORED(4);

    final int value;

    private SamplerAddressingMode(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

}
