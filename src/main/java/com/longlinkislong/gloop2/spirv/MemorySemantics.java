/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum MemorySemantics implements CEnum {
    NONE(0),
    ACQUIRE(0X2),
    RELEASE(0X4),
    ACQUIRE_RELEASE(0X8),
    SEQUENTIALLY_CONSISTENT(0X10),
    UNIFORM_MEMORY(0X40),
    SUBGROUP_MEMORY(0X80),
    WORKGROUP_MEMORY(0X100),
    CROSS_WORKGROUP_MEMORY(0X200),
    ATOMIC_COUNTER_MEMORY(0X400),
    IMAGE_MEMORY(0X800);

    final int value;

    private MemorySemantics(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<MemorySemantics> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }

}
