/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.spirv;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public enum Dim implements CEnum{
    DIM_1D(0),
    DIM_2D(1),
    DIM_3D(2),
    DIM_CUBE(3),
    DIM_RECT(4),
    DIM_BUFFER(5),
    DIM_SUBPASS_DATA(6);

    final int value;
    
    private Dim(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    public static Optional<Dim> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }    
}
