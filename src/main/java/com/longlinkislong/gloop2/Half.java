/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2;

/**
 *
 * @author zmichaels
 */
public class Half extends Number {

    public final int binary;

    public static final int MAX_EXPONENT = 15;
    public static final int MAX_VALUE = 0x7BFF;
    public static final int MIN_EXPONENT = -14;
    public static final int MIN_VALUE = 0x400;
    public static final int NaN = 0xFFFF;
    public static final int NEGATIVE_INFINITY = 0xFC00;
    public static final int POSITIVE_INFINITY = 0x7C00;
    public static final int SIZE = 2;

    private Half(final int binary) {
        this.binary = binary;
    }

    public Half(final float value) {
        this.binary = floatToIntBits(value);
    }

    @Override
    public int intValue() {
        return (int) floatValue();
    }

    @Override
    public long longValue() {
        return (long) floatValue();
    }

    @Override
    public float floatValue() {
        return intToFloat(this.binary);
    }

    @Override
    public double doubleValue() {
        return floatValue();
    }

    @Override
    public int hashCode() {
        return this.binary;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }

        if (obj instanceof Half) {
            return this.binary == ((Half) obj).binary;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return Float.toString(this.floatValue());
    }

    public boolean isNaN() {
        return isNaN(this.binary);
    }

    public boolean isInfinity() {
        return isInfinity(this.binary);
    }

    public static int normalizedFloatToIntBits(final float fval) {
        final int bits = Float.floatToIntBits(fval);
        final int sign = bits >>> 16 & 0x8000;
        final int val = (bits & 0x7FFF_FFFF) + 0x1000;

        return sign | val - 0x3800_0000 >>> 13;
    }

    public static int denormalizedFloatToIntBits(final float fval) {
        final int bits = Float.floatToIntBits(fval);
        final int sign = bits >>> 16 & 0x8000;
        final int val = (bits & 0x7FFF_FFFF) >>> 23;

        return sign | ((bits & 0x7f_FFFF | 0x80_0000)
                + (0x80_0000 >>> val - 102)
                >>> 126 - val);
    }

    public static int floatToIntBits(final float fval) {
        // https://stackoverflow.com/questions/6162651/half-precision-floating-point-in-java
        int fbits = Float.floatToIntBits(fval);
        int sign = fbits >>> 16 & 0x8000;
        int val = (fbits & 0x7FFF_FFFF) + 0x1000;

        if (val >= 0x4780_0000) {
            if ((fbits & 0x7FFF_FFFF) >= 0x4780_0000) {
                if (val < 0x7F80_0000) {
                    return sign | 0x7C00;
                }

                return sign | 0x7C00
                        | (fbits & 0x007F_FFFF) >>> 13;
            }
            return sign | 0x7bFF;
        }

        if (val >= 0x3880_0000) {
            return sign | val - 0x3800_0000 >>> 13;
        }

        if (val < 0x33000000) {
            return sign;
        }

        val = (fbits & 0x7FFF_FFFF) >>> 23;

        return sign | ((fbits & 0x7F_FFFF | 0x80_0000)
                + (0x80_0000 >>> val - 102)
                >>> 126 - val);
    }

    public static float intToFloat(final int hbits) {
        int mant = hbits & 0x03FF;
        int exp = hbits & 0x7C00;

        if (exp == 0x7C00) {
            exp = 0x3_FC00;
        } else if (exp != 0) {
            exp += 0x1_C000;

            if (mant == 0 && exp > 0x1_C400) {
                return Float.intBitsToFloat(
                        (hbits & 0x8000) << 16
                        | exp << 13 | 0x3FF);
            }
        } else if (mant != 0) {
            exp = 0x1_C400;

            do {
                mant <<= 1;
                exp -= 0x400;
            } while ((mant & 0x400) == 0);

            mant &= 0x3FF;
        }

        return Float.intBitsToFloat(
                (hbits & 0x8000) << 16
                | (exp | mant) << 13);
    }

    public static String toHexString(final float value) {
        return toHexString(floatToIntBits(value));
    }

    public static String toHexString(final int binary) {
        return String.format("%x", binary);
    }

    public static boolean isInfinity(final int binary) {
        // check if exponent is all 1's and significant is zero.
        return (binary & 0x7C00) == 0x7C00
                && (binary & 0x3FF) == 0;
    }

    public static boolean isNaN(final int binary) {
        // check if exponent is all 1's and significant is not zero
        return (binary & 0x7C00) == 0x7C00
                && (binary & 0x3FF) != 0;
    }

    public static String toString(final int binary) {
        return new Half(binary).toString();
    }

    public static Half valueOf(final int binary) {
        return new Half(binary);
    }

    public static Half valueOf(final float f) {
        return new Half(floatToIntBits(f));
    }

    public static Half valueOf(final String s) {
        return valueOf(Float.valueOf(s));
    }
}
