/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.lwjgl.openal.AL10;
import org.lwjgl.system.MemoryStack;

/**
 *
 * @author zmichaels
 */
public class ALSource extends ALObject {

    public ALSource() {
        ALContext.getInstance();
        this.handle = AL10.alGenSources();

        assert AL10.alGetError() == AL10.AL_NO_ERROR : "GenSources";
    }

    public void setPitch(final float value) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcef(this.handle, AL10.AL_PITCH, value);
    }

    public float getPitch() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcef(this.handle, AL10.AL_PITCH);
    }

    public void setGain(final float value) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcef(this.handle, AL10.AL_GAIN, value);
    }

    public float getGain() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcef(this.handle, AL10.AL_GAIN);
    }

    public void setMinGain(final float minGain) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcef(this.handle, AL10.AL_MIN_GAIN, minGain);
    }

    public float getMinGain() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcef(this.handle, AL10.AL_MIN_GAIN);
    }

    public void setMaxGain(final float maxGain) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcef(this.handle, AL10.AL_MAX_GAIN, maxGain);
    }

    public float getMaxGain() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcef(this.handle, AL10.AL_MAX_GAIN);
    }

    public void setMaxDistance(final float maxDistance) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcef(this.handle, AL10.AL_MAX_DISTANCE, maxDistance);
    }

    public float getMaxDistance() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcef(this.handle, AL10.AL_MAX_DISTANCE);
    }

    public void setRolloffFactor(final float rolloffFactor) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcef(this.handle, AL10.AL_ROLLOFF_FACTOR, rolloffFactor);
    }

    public float getRolloffFactor() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcef(this.handle, AL10.AL_ROLLOFF_FACTOR);
    }

    public void setConeOuterGain(final float outerGain) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcef(this.handle, AL10.AL_CONE_OUTER_GAIN, outerGain);
    }

    public float getConeOuterGain() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcef(this.handle, AL10.AL_CONE_OUTER_GAIN);
    }

    public void setConeInnerAngle(final float innerAngle) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcef(this.handle, AL10.AL_CONE_INNER_ANGLE, innerAngle);
    }

    public float getConeInnerAngle() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcef(this.handle, AL10.AL_CONE_INNER_ANGLE);
    }

    public void setConeOuterAngle(final float outerAngle) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcef(this.handle, AL10.AL_CONE_OUTER_ANGLE, outerAngle);
    }

    public float getConeOuterAngle() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcef(this.handle, AL10.AL_CONE_OUTER_ANGLE);
    }

    public void setReferenceDistance(final float ref) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcef(this.handle, AL10.AL_REFERENCE_DISTANCE, ref);
    }

    public float getReferenceDistance() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcef(this.handle, AL10.AL_REFERENCE_DISTANCE);
    }

    public void setPosition(final float x, final float y, final float z) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSource3f(this.handle, AL10.AL_POSITION, x, y, z);
    }

    public void setPosition(final FloatBuffer pos) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcefv(this.handle, AL10.AL_POSITION, pos);
    }

    public float[] getPosition() {
        assert this.isValid() : "ALSource is not valid!";

        final float[] pos = new float[3];

        AL10.alGetSourcefv(this.handle, AL10.AL_POSITION, pos);

        return pos;
    }

    public FloatBuffer getPosition(final FloatBuffer pos) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alGetSourcefv(this.handle, AL10.AL_POSITION, pos);

        return pos;
    }

    public void setVelocity(final float x, final float y, final float z) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSource3f(this.handle, AL10.AL_VELOCITY, x, y, z);
    }

    public float[] getVelocity() {
        assert this.isValid() : "ALSource is not valid!";

        final float[] vel = new float[3];

        AL10.alGetSourcefv(this.handle, AL10.AL_VELOCITY, vel);

        return vel;
    }

    public FloatBuffer getVelocity(final FloatBuffer vel) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alGetSourcefv(this.handle, AL10.AL_VELOCITY, vel);

        return vel;
    }

    public void setDirection(final float x, final float y, final float z) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSource3f(this.handle, AL10.AL_DIRECTION, x, y, z);
    }

    public float[] getDirection() {
        assert this.isValid() : "ALSource is not valid!";

        final float[] dir = new float[3];

        AL10.alGetSourcefv(this.handle, AL10.AL_DIRECTION, dir);

        return dir;
    }

    public FloatBuffer getDirection(final FloatBuffer dir) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alGetSourcefv(this.handle, AL10.AL_DIRECTION, dir);

        return dir;
    }

    public void setRelative(final boolean relative) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcei(this.handle, AL10.AL_SOURCE_RELATIVE, relative ? AL10.AL_TRUE : AL10.AL_FALSE);
    }

    public boolean isRelative() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcei(this.handle, AL10.AL_SOURCE_RELATIVE) == AL10.AL_TRUE;
    }

    public void setLooping(final boolean looping) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcei(this.handle, AL10.AL_LOOPING, looping ? AL10.AL_TRUE : AL10.AL_FALSE);
    }

    public void setBuffer(final ALBuffer buffer) {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcei(this.handle, AL10.AL_BUFFER, buffer.handle);
    }

    public ALBuffer getBuffer() {
        assert this.isValid() : "ALSource is not valid!";

        return new ALBuffer(AL10.alGetSourcei(this.handle, AL10.AL_BUFFER));
    }

    public ALSourceState getState() {
        assert this.isValid() : "ALSource is not valid!";

        final int sourceState = AL10.alGetSourcei(this.handle, AL10.AL_SOURCE_STATE);

        switch (sourceState) {
            case AL10.AL_STOPPED:
                return ALSourceState.STOPPED;
            case AL10.AL_PLAYING:
                return ALSourceState.PLAYING;
            default:
                throw new IllegalStateException("Unknown source state: " + sourceState);
        }
    }

    public int getBuffersQueued() {
        assert this.isValid() : "ALSource is not valid!";

        return AL10.alGetSourcei(this.handle, AL10.AL_BUFFERS_QUEUED);
    }

    public List<ALBuffer> unqueueBuffers() {
        assert this.isValid() : "ALSource is not valid!";

        final int nbuffers = AL10.alGetSourcei(this.handle, AL10.AL_BUFFERS_PROCESSED);
        
        if (nbuffers == 0) {
            return Collections.emptyList();
        }
        
        final List<ALBuffer> out = new ArrayList<>(nbuffers);

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pBuffers = mem.callocInt(nbuffers);

            AL10.alSourceUnqueueBuffers(this.handle, pBuffers);
            
            assert AL10.alGetError() == AL10.AL_NO_ERROR : "SourceUnqueueBuffers";            

            while (pBuffers.hasRemaining()) {
                out.add(new ALBuffer(pBuffers.get()));
            }
        }

        return out;
    }

    public void play() {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcePlay(this.handle);
        assert AL10.alGetError() == AL10.AL_NO_ERROR : "SourcePlay";
    }

    public void pause() {
        assert this.isValid() : "ALSource is not valid!";

        AL10.alSourcePause(this.handle);
        assert AL10.alGetError() == AL10.AL_NO_ERROR : "SourcePause";
    }
    
    public void stop() {
        assert this.isValid() : "ALSource is not valid!";
        
        AL10.alSourceStop(this.handle);
        assert AL10.alGetError() == AL10.AL_NO_ERROR : "SourceStop";
    }
    
    public void rewind() {
        assert this.isValid() : "ALSource is not valid!";
        
        AL10.alSourceRewind(this.handle);
        assert AL10.alGetError() == AL10.AL_NO_ERROR : "SourceRewind";
    }
    
    public void queueBuffers(final List<ALBuffer> buffers) {
        assert this.isValid() : "ALSource is not valid!";
        
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pBuffers = mem.callocInt(buffers.size());
            
            buffers.forEach(buffer -> pBuffers.put(buffer.handle));
            pBuffers.flip();
            
            AL10.alSourceQueueBuffers(this.handle, pBuffers);
            
           assert AL10.alGetError() == AL10.AL_NO_ERROR : "SourceQueueBuffers";
        }
    }
    
    public void queueBuffers(final ALBuffer... buffers) {
        queueBuffers(Arrays.asList(buffers));
    }

    @Override
    public void close() {
        if (this.isValid()) {
            AL10.alDeleteSources(this.handle);
            this.handle = 0;

            assert AL10.alGetError() == AL10.AL_NO_ERROR : "DeleteSources";
        }
    }
}
