/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.openal.AL11;

/**
 *
 * @author zmichaels
 */
public enum ALSourceType implements CEnum {
    UNDETERMINED(AL11.AL_UNDETERMINED),
    STATIC(AL11.AL_STATIC),
    STREAMING(AL11.AL_STREAMING);
    
    public final int value;
    
    private ALSourceType(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }
    
    public static Optional<ALSourceType> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
