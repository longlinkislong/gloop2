/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import com.longlinkislong.gloop2.CEnum;
import org.lwjgl.openal.AL10;
import org.lwjgl.openal.AL11;

/**
 *
 * @author zmichaels
 */
public enum ALDistanceModel implements CEnum {
    INVERSE_DISTANCE(AL10.AL_INVERSE_DISTANCE),
    INVERSE_DISTANCE_CLAMPED(AL10.AL_INVERSE_DISTANCE_CLAMPED),
    LINEAR_DISTANCE(AL11.AL_LINEAR_DISTANCE),
    LINEAR_DISTANCE_CLAMPED(AL11.AL_LINEAR_DISTANCE_CLAMPED),
    EXPONENT_DISTANCE(AL11.AL_EXPONENT_DISTANCE),
    EXPONENT_DISTANCE_CLAMPED(AL11.AL_EXPONENT_DISTANCE_CLAMPED),
    NONE(AL10.AL_NONE);

    public final int value;

    private ALDistanceModel(final int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

}
