/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import java.nio.ByteBuffer;
import org.lwjgl.openal.AL10;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public class ALBuffer extends ALObject {   
    protected ALBuffer(int handle) {
        this.handle = handle;
    }
    
    public ALBuffer() {
        ALContext.getInstance();
        this.handle = AL10.alGenBuffers();
        
        assert AL10.alGetError() == AL10.AL_NO_ERROR : "GenBuffers";
    }
    
    public int getFrequency() {
        assert this.isValid() : "ALBuffer is not valid!";
        
        return AL10.alGetBufferi(this.handle, AL10.AL_FREQUENCY);
    }
    
    public int getBitDepth() {
        assert this.isValid() : "ALBuffer is not valid!";
        
        return AL10.alGetBufferi(this.handle, AL10.AL_BITS);
    }
    
    public int getChannels() {
        assert this.isValid() : "ALBuffer is not valid!";
        
        return AL10.alGetBufferi(this.handle, AL10.AL_CHANNELS);
    }
    
    public int getSize() {
        assert this.isValid() : "ALBuffer is not valid!";
        
        return AL10.alGetBufferi(this.handle, AL10.AL_SIZE);
    }        
    
    public void setData(ALFormat format, final ByteBuffer data, final int freq) {
        assert this.isValid() : "ALBuffer is not valid!";
        
        if (data.isDirect()) {
            AL10.alBufferData(this.handle, format.value, data, freq);
        } else {
            final ByteBuffer directData = MemoryUtil.memAlloc(data.remaining())
                    .order(data.order())
                    .put(data);
            
            directData.flip();
            
            try {
                AL10.alBufferData(this.handle, format.value, directData, freq);
            } finally {
                MemoryUtil.memFree(directData);
            }
        }
        
        assert AL10.alGetError() == AL10.AL_NO_ERROR : "BufferData";
    }
    
    @Override
    public void close() {
        if (this.isValid()) {
            AL10.alDeleteBuffers(this.handle);                        
            this.handle = 0;
            
            assert AL10.alGetError() == AL10.AL_NO_ERROR : "DeleteBuffers";
        }
    }
    
    @Override
    public boolean equals(final Object other) {
        if (other == null) {
            return false;
        } else if (this == other) {
            return true;
        } else if (other instanceof ALBuffer) {
            return this.handle == ((ALBuffer) other).handle;
        } else {
            return false;
        }
    }
    
    public static ALBuffer NULL_BUFFER = new ALBuffer(0);

    @Override
    public int hashCode() {
        return this.handle;
    }
}
