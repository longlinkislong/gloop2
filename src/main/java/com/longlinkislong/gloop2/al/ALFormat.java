/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.openal.AL10;
import org.lwjgl.openal.EXTAlaw;
import org.lwjgl.openal.EXTDouble;
import org.lwjgl.openal.EXTFloat32;
import org.lwjgl.openal.EXTIma4;
import org.lwjgl.openal.EXTMulaw;

/**
 *
 * @author zmichaels
 */
public enum ALFormat implements CEnum {
    MONO16(AL10.AL_FORMAT_MONO16),
    MONO8(AL10.AL_FORMAT_MONO8),  
    MONO_ALAW(EXTAlaw.AL_FORMAT_MONO_ALAW_EXT),
    MONO_DOUBLE(EXTDouble.AL_FORMAT_MONO_DOUBLE_EXT),
    MONO_FLOAT32(EXTFloat32.AL_FORMAT_MONO_FLOAT32),
    MONO_IMA4(EXTIma4.AL_FORMAT_MONO_IMA4),
    MONO_MULAW(EXTMulaw.AL_FORMAT_MONO_MULAW_EXT),
    
    STEREO16(AL10.AL_FORMAT_STEREO16),
    STEREO8(AL10.AL_FORMAT_STEREO8),
    STEREO_ALAW(EXTAlaw.AL_FORMAT_STEREO_ALAW_EXT),
    STEREO_DOUBLE(EXTDouble.AL_FORMAT_STEREO_DOUBLE_EXT),    
    STEREO_FLOAT32(EXTFloat32.AL_FORMAT_STEREO_FLOAT32),
    STEREO_IMA4(EXTIma4.AL_FORMAT_MONO_IMA4),    
    STEREO_MULAW(EXTMulaw.AL_FORMAT_STEREO_MULAW_EXT);
    
    public final int value;
    
    private ALFormat(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
    
    public static Optional<ALFormat> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
