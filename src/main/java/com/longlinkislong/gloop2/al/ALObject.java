/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

/**
 *
 * @author zmichaels
 */
public abstract class ALObject implements AutoCloseable {
    protected int handle;
    
    public boolean isValid() {
        return this.handle != 0;
    }
    
    @Override
    public void close() {
    }
    
}
