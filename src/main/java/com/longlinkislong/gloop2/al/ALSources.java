/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.List;
import org.lwjgl.openal.AL10;
import org.lwjgl.system.MemoryStack;

/**
 *
 * @author zmichaels
 */
public final class ALSources {

    private ALSources() {
    }

    public static void play(final List<ALSource> sources) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pSources = mem.callocInt(sources.size());

            sources.forEach(source -> pSources.put(source.handle));
            pSources.flip();

            AL10.alSourcePlayv(pSources);
            assert AL10.alGetError() == AL10.AL_NO_ERROR : "SourcePlayv";
        }
    }

    public static void play(final ALSource... sources) {
        play(Arrays.asList(sources));
    }
    
    public static void stop(final List<ALSource> sources) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pSources = mem.callocInt(sources.size());
            
            sources.forEach(source -> pSources.put(source.handle));
            pSources.flip();
            
            AL10.alSourcePausev(pSources);
            assert AL10.alGetError() == AL10.AL_NO_ERROR : "SourcePausev";
        }
    }
    
    public static void stop(final ALSource... sources) {
        stop(Arrays.asList(sources));
    }
    
    public static void rewind(final List<ALSource> sources) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pSources = mem.callocInt(sources.size());
            
            sources.forEach(source -> pSources.put(source.handle));
            pSources.flip();
            
            AL10.alSourceRewindv(pSources);
            assert AL10.alGetError() == AL10.AL_NO_ERROR : "SourceRewindv";
        }
    }
    
    public static void rewind(final ALSource... sources) {
        rewind(Arrays.asList(sources));
    }
}
