/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import org.lwjgl.openal.AL10;
import org.lwjgl.openal.AL11;

/**
 *
 * @author zmichaels
 */
public final class ALState {
    private ALState() {}
    
    public static float getDopplerFactor() {
        return AL10.alGetFloat(AL10.AL_DOPPLER_FACTOR);
    }
    
    public static void setDopplerFactor(final float value) {
        AL10.alDopplerFactor(value);
    }
    
    public static float getSpeedOfSound() {
        return AL10.alGetFloat(AL11.AL_SPEED_OF_SOUND);
    }
    
    public static void setSpeedOfSound(final float value) {
        AL11.alSpeedOfSound(value);
    }
    
    public static void setDistanceModel(final ALDistanceModel model) {
        AL10.alDistanceModel(model.value);
    }
    
    public static ALDistanceModel getDistanceModel() {
        final int model = AL10.alGetInteger(AL10.AL_DISTANCE_MODEL);
        
        switch (model) {
            case AL10.AL_INVERSE_DISTANCE:
                return ALDistanceModel.INVERSE_DISTANCE;
            case AL10.AL_INVERSE_DISTANCE_CLAMPED:
                return ALDistanceModel.INVERSE_DISTANCE_CLAMPED;
            case AL11.AL_LINEAR_DISTANCE:
                return ALDistanceModel.LINEAR_DISTANCE;
            case AL11.AL_LINEAR_DISTANCE_CLAMPED:
                return ALDistanceModel.LINEAR_DISTANCE_CLAMPED;
            case AL11.AL_EXPONENT_DISTANCE:
                return ALDistanceModel.EXPONENT_DISTANCE;
            case AL11.AL_EXPONENT_DISTANCE_CLAMPED:
                return ALDistanceModel.EXPONENT_DISTANCE_CLAMPED;
            case AL10.AL_NONE:
                return ALDistanceModel.NONE;
            default:
                throw new IllegalStateException("Unknown DistanceModel: " + model);
        }
    }
}
