/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import java.nio.FloatBuffer;
import org.lwjgl.openal.AL10;

/**
 *
 * @author zmichaels
 */
public final class ALListener {
    static {
        ALContext.getInstance();
    }
    
    private ALListener() {}
    
    public static void setGain(final float value) {
        AL10.alListenerf(AL10.AL_GAIN, value);
    }
    
    public static float getGain() {
        return AL10.alGetListenerf(AL10.AL_GAIN);
    }
    
    public static void setPosition(final float x, final float y, final float z) {
        AL10.alListener3f(AL10.AL_POSITION, x, y, z);
    }
    
    public static void setPosition(final FloatBuffer pos) {
        AL10.alListenerfv(AL10.AL_POSITION, pos);
    }
    
    public static float[] getPosition() {
        final float[] pos = new float[3];
        
        AL10.alGetListenerfv(AL10.AL_POSITION, pos);
        
        return pos;
    }
    
    public static FloatBuffer getPosition(final FloatBuffer pos) {
        AL10.alGetListenerfv(AL10.AL_POSITION, pos);
        
        return pos;
    }
    
    public static void setVelocity(final float x, final float y, final float z) {
        AL10.alListener3f(AL10.AL_VELOCITY, x, y, z);
    }
    
    public static float[] getVelocity() {
        final float[] vel = new float[3];
        
        AL10.alGetListenerfv(AL10.AL_VELOCITY, vel);
        
        return vel;
    }
    
    public static FloatBuffer getVelocity(final FloatBuffer vel) {
        AL10.alGetListenerfv(AL10.AL_VELOCITY, vel);
        
        return vel;
    }
    
    public static void setOrientation(final FloatBuffer atup) {
        AL10.alListenerfv(AL10.AL_ORIENTATION, atup);
    }
    
    public static FloatBuffer getOrientation(final FloatBuffer atup) {
        AL10.alGetListenerfv(AL10.AL_ORIENTATION, atup);
        
        return atup;
    }
}
