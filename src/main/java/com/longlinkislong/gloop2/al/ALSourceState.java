/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import com.longlinkislong.gloop2.CEnum;
import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Optional;
import org.lwjgl.openal.AL10;

/**
 *
 * @author zmichaels
 */
public enum ALSourceState implements CEnum {
    STOPPED(AL10.AL_STOPPED),
    PLAYING(AL10.AL_PLAYING);

    public final int value;

    private ALSourceState(final int value) {        
        this.value = value;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    public static Optional<ALSourceState> of(final int value) {
        return Tools.valueOf(value, Arrays.stream(values()));
    }
}
