/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.al;

import java.nio.ByteBuffer;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALC10;
import org.lwjgl.openal.ALCCapabilities;
import org.lwjgl.openal.ALCapabilities;
import org.lwjgl.system.MemoryStack;

/**
 *
 * @author zmichaels
 */
public final class ALContext {
    public final long deviceId;
    public final long contextId;
    public final ALCCapabilities alcCapabilities;
    public final ALCapabilities alCapabilities;
    
    private ALContext() {
        this.deviceId = ALC10.alcOpenDevice((ByteBuffer) null);
        
        try (MemoryStack mem = MemoryStack.stackPush()) {
            this.contextId = ALC10.alcCreateContext(deviceId, mem.ints(0));            
        }
        
        ALC10.alcMakeContextCurrent(contextId);
        
        this.alcCapabilities = ALC.createCapabilities(this.deviceId);
        this.alCapabilities = AL.createCapabilities(alcCapabilities);
        
    }
    
    public static ALContext getInstance() {
        return INSTANCE.get();
    }
    
    static final ThreadLocal<ALContext> INSTANCE = new ThreadLocal<ALContext>(){
        @Override
        protected ALContext initialValue() {
            return new ALContext();
        }
    };
}
