/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2;

/**
 *
 * @author zmichaels
 */
public class LongFixed extends Number {
    public static final LongFixed POSITIVE_INFINITY = new LongFixed(1L, 0L);
    public static final LongFixed NEGATIVE_INFINITY = new LongFixed(-1L, 0L);
    public static final int SIZE = 16;
    public static final LongFixed MAX_VALUE = new LongFixed(Long.MAX_VALUE, 1L);
    public static final LongFixed MIN_VALUE = new LongFixed(0L, 1L);
    public static final LongFixed NaN = new LongFixed(0L, 0L);
    
    public final long value;
    public final long scale;
    
    public LongFixed(final long value, final long scale) {
        this.value = value;
        this.scale = scale;
    }
    
    public LongFixed(final double value, final long scale) {
        this.scale = scale;
        this.value = (long) (value * scale);
    }
    
    public LongFixed(final double value) {
        this(value, 0x800);
    }
    
    public double epsilon() {
        return 1.0 / this.scale;
    }

    @Override
    public int intValue() {
        return (int) this.longValue();
    }

    @Override
    public long longValue() {
        return this.value / this.scale;
    }

    @Override
    public float floatValue() {
        return (float) this.doubleValue();
    }

    @Override
    public double doubleValue() {
        return ((double) this.value) / this.scale;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        } else if (obj instanceof LongFixed) {
            final LongFixed other = (LongFixed) obj;
            
            if (this.value == other.value && this.scale == other.scale) {
                return true;
            } else {
                final double epsilon = Math.max(this.epsilon(), other.epsilon());
                
                return Math.abs(this.doubleValue() - other.doubleValue()) < epsilon;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (int) (this.value ^ (this.value >>> 32));
        hash = 61 * hash + (int) (this.scale ^ (this.scale >>> 32));
        return hash;
    }
    
    public static String toHexString(final long value, final long scale) {
        return String.format("%x%x", value, scale);
    }
    
    @Override
    public String toString() {
        return toString(this.value, this.scale);
    }
    
    public static String toString(final long value, final long scale) {
        if (scale == 1) {
            return Long.toString(value);
        } else {
            return new StringBuilder(4096)
                    .append(Long.toString(value))
                    .append('/')
                    .append(Long.toString(scale))
                    .toString();
        }
    }        
}
