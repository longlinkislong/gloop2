/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

/**
 * Image file formats supported by ImageIO.
 *
 * @author zmichaels
 */
public enum ImageFileFormat {
    /**
     * Unformatted data.
     */
    RAW,
    /**
     * Portable Network Graphics. A lossless compressed format that supports 8
     * bits per channel with 8 bits alpha.
     */
    PNG,
    /**
     * Microsoft Bitmap. An uncompressed format that supports 8 bits per channel
     * without alpha.
     */
    BMP,
    /**
     * Targa Graphics Array. An optionally compressed format that supports 8
     * bits per channel with 8 bits alpha.
     */
    TGA
}
