/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

import java.nio.ByteBuffer;

/**
 *
 * @author zmichaels
 */
public final class DummyImage implements Image {
    private final int width;
    private final int height;
    private final PixelFormat format;
    
    public DummyImage(final int width, final int height, final PixelFormat format) {
        this.width = width;
        this.height = height;
        this.format = format;
    }
    
    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public ByteBuffer getData() {
        throw new UnsupportedOperationException("DummyImage does not contain any data!");
    }

    @Override
    public long ptr() {
        throw new UnsupportedOperationException("DummyImage does not contain any data!");
    }

    @Override
    public BufferedImage getSubimage(int x, int y, int w, int h) {
        throw new UnsupportedOperationException("DummyImage does not contain any data!");
    }

    @Override
    public void setSubimage(int x, int y, int w, int h, Image subimg) {
        throw new UnsupportedOperationException("DummyImage does not contain any data!");
    }

    @Override
    public PixelFormat getFormat() {
        return this.format;
    }

    @Override
    public void close() {        
    }
    
}
