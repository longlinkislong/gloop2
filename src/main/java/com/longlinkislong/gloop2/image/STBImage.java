package com.longlinkislong.gloop2.image;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

/**
 * An implementation of Image that uses STB for loading.
 *
 * @author zmichaels
 */
final class STBImage implements Image {

    private final int width;
    private final int height;
    private final PixelFormat format;
    private ByteBuffer data;

    STBImage(final ByteBuffer filedata, final int forceComponents) throws IOException {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pWidth = mem.callocInt(1);
            final IntBuffer pHeight = mem.callocInt(1);
            final IntBuffer pComp = mem.callocInt(1);

            this.data = org.lwjgl.stb.STBImage.stbi_load_from_memory(filedata, pWidth, pHeight, pComp, forceComponents);

            if (this.data == null) {
                throw new IOException(org.lwjgl.stb.STBImage.stbi_failure_reason());
            }

            final int comp = forceComponents == 0 ? pComp.get(0) : forceComponents;

            switch (comp) {
                case 1:
                    this.format = PixelFormat.R8_UNORM;
                    break;
                case 2:
                    this.format = PixelFormat.R8G8_UNORM;
                    break;
                case 3:
                    this.format = PixelFormat.R8G8B8_UNORM;
                    break;
                case 4:
                    this.format = PixelFormat.R8G8B8A8_UNORM;
                    break;
                default:
                    throw new IOException("Unsupported component count: " + comp);
            }

            this.width = pWidth.get(0);
            this.height = pHeight.get(0);

            if (this.width == 0 || this.height == 0) {
                throw new IOException("Image did not encode for any data!");
            }
        }
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public ByteBuffer getData() {
        return this.data.slice();
    }

    @Override
    public long ptr() {
        return MemoryUtil.memAddress(this.data);
    }

    @Override
    public PixelFormat getFormat() {
        return this.format;
    }

    @Override
    public void close() {
        if (this.data != null) {
            org.lwjgl.stb.STBImage.stbi_image_free(this.data);
            this.data = null;
        }
    }

}
