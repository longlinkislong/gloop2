/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import org.lwjgl.stb.STBImageWrite;
import org.lwjgl.system.MemoryUtil;

/**
 * A utility class for creating and writing Image objects. Similar to the AWT
 * ImageIO class.
 *
 * @author zmichaels
 */
public final class ImageIO {

    /**
     * Reads an Image from the supplied path.
     *
     * @param path the path to the image.
     * @param forceChannels the desired number of channels. Default is 0.
     * @return the loaded image.
     * @throws IOException if the image could not be loaded.
     */
    public static Image read(final Path path, final int forceChannels) throws IOException {
        try (FileChannel ch = FileChannel.open(path, StandardOpenOption.READ)) {
            return read(ch, forceChannels);
        }
    }

    /**
     * Reads an Image from the supplied path.
     *
     * @param path the path to the image.
     * @return the loaded image.
     * @throws IOException if the image could not be loaded.
     */
    public static Image read(final Path path) throws IOException {
        return read(path, 0);
    }

    /**
     * Reads an Image from a ReadableByteChannel.
     *
     * @param ch the source channel.
     * @return the loaded image.
     * @throws IOException if the image could not be loaded.
     */
    public static Image read(final ReadableByteChannel ch) throws IOException {
        return read(ch, 0);
    }

    /**
     * Reads an Image from an InputStream.
     *
     * @param in the source InputStream.
     * @return the loaded image.
     * @throws IOException if the image could not be loaded.
     */
    public static Image read(final InputStream in) throws IOException {
        return read(in, 0);
    }

    /**
     * Reads an Image from a URL.
     *
     * @param url the source URL.
     * @param forceChannels the desired number of channels. Default is 0.
     * @return the loaded image.
     * @throws IOException if the image could not be loaded.
     */
    public static Image read(final URL url, final int forceChannels) throws IOException {
        try (InputStream in = url.openStream()) {
            return read(in, forceChannels);
        }
    }

    /**
     * Reads an Image from a URL.
     *
     * @param url the source URL.
     * @return the loaded image.
     * @throws IOException if the image could not be loaded.
     */
    public static Image read(final URL url) throws IOException {
        return read(url, 0);
    }

    /**
     * Reads an Image from a ReadableByteChannel.
     *
     * @param ch the source ReadableByteChannel.
     * @param forceChannels the desired number of channels. Default is 0.
     * @return the loaded image.
     * @throws IOException if the image could not be loaded.
     */
    public static Image read(final ReadableByteChannel ch, final int forceChannels) throws IOException {
        ByteBuffer data = null;

        try (ReadableByteChannel in = ch) {
            data = MemoryUtil.memAlloc(8192);

            while (in.read(data) != -1) {
                if (!data.hasRemaining()) {
                    final ByteBuffer newData = MemoryUtil.memAlloc(data.capacity() * 2);

                    data.flip();
                    newData.put(data);

                    MemoryUtil.memFree(data);

                    data = newData;
                }
            }

            data.flip();

            return new STBImage(data, forceChannels);
        } finally {
            if (data != null) {
                MemoryUtil.memFree(data);
            }
        }
    }

    /**
     * Reads an Image from an InputStream.
     *
     * @param in the source InputStream.
     * @param forceChannels the desired number of channels. Default is 0.
     * @return the loaded image.
     * @throws IOException if the Image could not be loaded.
     */
    public static Image read(final InputStream in, final int forceChannels) throws IOException {
        try (ReadableByteChannel ch = Channels.newChannel(in)) {
            return read(ch, forceChannels);
        }
    }

    /**
     * Writes an Image to an OutputStream.
     * @param img the Image to write.
     * @param fmt the format to encode the image as.
     * @param out
     * @throws IOException 
     */
    public static void write(final Image img, final ImageFileFormat fmt, final OutputStream out) throws IOException {
        try (WritableByteChannel ch = Channels.newChannel(out)) {
            write(img, fmt, ch);
        }
    }

    public static void write(final Image img, final ImageFileFormat fmt, final Path out) throws IOException {
        try (FileChannel ch = FileChannel.open(out, StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {
            write(img, fmt, ch);
        }
    }

    public static void write(final Image img, final ImageFileFormat fmt, final WritableByteChannel out) throws IOException {
        final boolean res;
        final IOException[] pEx = new IOException[1];

        switch (fmt) {
            case RAW:
                res = true;                
                out.write(img.getData());
                break;                
            case PNG:
                res = STBImageWrite.stbi_write_png_to_func((ctx, src, size) -> {
                    try {
                        out.write(MemoryUtil.memByteBuffer(src, size));
                    } catch (IOException ex) {
                        pEx[0] = ex;
                    }
                }, 0L,
                        img.getWidth(), img.getHeight(),
                        img.getFormat().components, img.getData(), img.getFormat().bytes * img.getWidth());
                break;
            case TGA:
                res = STBImageWrite.stbi_write_tga_to_func((ctx, src, size) -> {
                    try {
                        out.write(MemoryUtil.memByteBuffer(src, size));
                    } catch (IOException ex) {
                        pEx[0] = ex;
                    }
                }, 0L,
                        img.getWidth(), img.getHeight(),
                        img.getFormat().components, img.getData());
                break;
            case BMP:
                res = STBImageWrite.stbi_write_bmp_to_func((ctx, src, size) -> {
                    try {
                        out.write(MemoryUtil.memByteBuffer(src, size));
                    } catch (IOException ex) {
                        pEx[0] = ex;
                    }
                }, 0L,
                        img.getWidth(), img.getHeight(),
                        img.getFormat().components, img.getData());
                break;
            default:
                throw new IOException("Unsupported ImageFileFormat: " + fmt);
        }

        if (!res) {
            throw new IOException("Unable to write image!", pEx[0]);
        }
    }
}
