/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

import com.longlinkislong.gloop2.Tools;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.lwjgl.stb.STBTTAlignedQuad;
import org.lwjgl.stb.STBTTBakedChar;
import org.lwjgl.stb.STBTTFontinfo;
import org.lwjgl.stb.STBTruetype;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class FontImage implements Image {

    private static final Logger LOGGER = LoggerFactory.getLogger(FontImage.class);

    private final FontInfo info;
    private final STBTTFontinfo fontInfo;
    private final STBTTBakedChar.Buffer cdata;
    private Image image;
    public final float scale;
    public final float ascent;
    public final float descent;
    public final float lineGap;
    public final float lineSpacing;

    public FontImage(final long pCache) {
        long ptr = pCache;
        
        this.info = new FontInfo(
                null,
                MemoryUtil.memGetFloat(ptr),
                (char) MemoryUtil.memGetInt((ptr + 4)), MemoryUtil.memGetInt(ptr + 8),
                MemoryUtil.memGetInt(ptr + 12));
        
        ptr += 16;
        
        this.scale = MemoryUtil.memGetFloat(ptr);
        this.ascent = MemoryUtil.memGetFloat(ptr + 4);
        this.descent = MemoryUtil.memGetFloat(ptr + 8);
        this.lineGap = MemoryUtil.memGetFloat(ptr + 12);
        this.lineSpacing = MemoryUtil.memGetFloat(ptr + 16);
        
        ptr += 20;
        
        final int width = MemoryUtil.memGetInt(ptr);
        final int height = MemoryUtil.memGetInt(ptr + 4);        
        final int size = width * height;
        
        ptr += 8;
        this.image = new ProxyImage(width, height, PixelFormat.R8_UNORM, MemoryUtil.memByteBuffer(ptr, size));                
        ptr += size;
        
        this.fontInfo = STBTTFontinfo.create(ptr);
        ptr += STBTTFontinfo.SIZEOF;
        
        final int cdataSize = MemoryUtil.memGetInt(ptr);
        
        ptr += 4;
        this.cdata = STBTTBakedChar.create(ptr, cdataSize);
    }
    
    public int sizeOf() {
        return 16 
                + STBTTFontinfo.SIZEOF 
                + cdata.capacity() * STBTTBakedChar.SIZEOF
                + image.getWidth() * image.getHeight()
                + 20;
    }
    
    public void encode(final long pCache) {
        long ptr = pCache;
        
        MemoryUtil.memPutFloat(ptr, info.charHeight);
        MemoryUtil.memPutInt(ptr + 4, info.firstChar);
        MemoryUtil.memPutInt(ptr + 8, info.charCount);
        MemoryUtil.memPutInt(ptr + 12, info.fontIndex);        
        ptr += 16;
        
        MemoryUtil.memPutFloat(ptr, this.scale);
        MemoryUtil.memPutFloat(ptr + 4, this.ascent);
        MemoryUtil.memPutFloat(ptr + 8, this.descent);
        MemoryUtil.memPutFloat(ptr + 12, this.lineGap);
        MemoryUtil.memPutFloat(ptr + 16, this.lineSpacing);
        ptr += 20;
        
        MemoryUtil.memPutInt(ptr, this.image.getWidth());
        MemoryUtil.memPutInt(ptr + 4, this.image.getHeight());
        final int size = this.image.getWidth() * this.image.getHeight();
        ptr += 8;
        
        MemoryUtil.memCopy(this.image.ptr(), ptr, size);
        ptr += size;
        
        MemoryUtil.memCopy(this.fontInfo.address(), ptr, STBTTFontinfo.SIZEOF);
        ptr += STBTTFontinfo.SIZEOF;
        
        MemoryUtil.memPutInt(ptr, this.cdata.capacity());
        ptr += 4;
        
        MemoryUtil.memCopy(this.cdata.address(), ptr, this.cdata.capacity());        
    }

    public FontImage(final FontInfo info) {
        this.info = info;
        this.cdata = STBTTBakedChar.calloc(info.charCount);
        this.fontInfo = STBTTFontinfo.calloc();

        STBTruetype.stbtt_InitFont(this.fontInfo, info.fontData);

        this.scale = STBTruetype.stbtt_ScaleForPixelHeight(fontInfo, info.charHeight);

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final IntBuffer pAscent = mem.callocInt(1);
            final IntBuffer pDescent = mem.callocInt(1);
            final IntBuffer pLineGap = mem.callocInt(1);

            STBTruetype.stbtt_GetFontVMetrics(fontInfo, pAscent, pDescent, pLineGap);

            this.ascent = this.scale * pAscent.get();
            this.descent = this.scale * pDescent.get();
            this.lineGap = this.scale * pLineGap.get();
            this.lineSpacing = this.ascent - this.descent + this.lineGap;
        }

        int w = 1;
        int h = 1;

        final float neededArea = info.charCount * info.charHeight * info.charHeight;
        boolean flipflop = false;

        while (w * h < neededArea) {
            if (flipflop) {
                w <<= 1;
            } else {
                h <<= 1;
            }

            flipflop = !flipflop;
        }

        Image tmpImage = new BufferedImage(w, h, PixelFormat.R8_UNORM);
        int resizes = 0;

        while (STBTruetype.stbtt_BakeFontBitmap(info.fontData, info.charHeight, tmpImage.getData(), w, h, info.firstChar, cdata) <= 0) {
            if (flipflop) {
                w <<= 1;
            } else {
                h <<= 1;
            }

            flipflop = !flipflop;
            info.fontData.rewind();
            cdata.rewind();

            tmpImage.close();
            tmpImage = new BufferedImage(w, h, PixelFormat.R8_UNORM);
            resizes++;
        }

        LOGGER.debug("Created Font with {} resizes! Size: <{}, {}>", resizes, w, h);

        this.image = tmpImage;
    }

    public List<CharSprite> encode(final float x, final float y, final String text) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final FloatBuffer px = mem.floats(x);
            final FloatBuffer py = mem.floats(y);
            final STBTTAlignedQuad q = STBTTAlignedQuad.callocStack(mem);
            final List<CharSprite> out = new ArrayList<>(text.length());

            for (char c : text.toCharArray()) {
                STBTruetype.stbtt_GetBakedQuad(this.cdata, this.image.getWidth(), this.image.getHeight(), (c - this.info.firstChar), px, py, q, true);

                out.add(new CharSprite(q));
            }

            return Collections.unmodifiableList(out);
        }
    }

    public static final class CharSprite {

        public final float s0;
        public final float t0;
        public final float s1;
        public final float t1;
        public final float x0;
        public final float y0;
        public final float x1;
        public final float y1;

        private CharSprite(final STBTTAlignedQuad q) {
            this.s0 = q.s0();
            this.s1 = q.s1();
            this.t0 = q.t0();
            this.t1 = q.t1();
            this.x0 = q.x0();
            this.x1 = q.x1();
            this.y0 = q.y0();
            this.y1 = q.y1();
        }

        public void writeVertexData(final ByteBuffer data) {
            data.putFloat(x0).putFloat(y0)
                    .putFloat(x1).putFloat(y0)
                    .putFloat(x0).putFloat(y1)
                    .putFloat(x1).putFloat(y0)
                    .putFloat(x0).putFloat(y1)
                    .putFloat(x1).putFloat(y1);
        }

        public void writeTextureData(final ByteBuffer data) {
            data.putFloat(s0).putFloat(t0)
                    .putFloat(s1).putFloat(t0)
                    .putFloat(s0).putFloat(t1)
                    .putFloat(s1).putFloat(t0)
                    .putFloat(s0).putFloat(t1)
                    .putFloat(s1).putFloat(t1);
        }

        public void writeVertexTextureData(final ByteBuffer data) {
            data.putFloat(x0).putFloat(y0).putFloat(s0).putFloat(t0)
                    .putFloat(x1).putFloat(y0).putFloat(s1).putFloat(t0)
                    .putFloat(x0).putFloat(y1).putFloat(s0).putFloat(t1)
                    .putFloat(x1).putFloat(y0).putFloat(s1).putFloat(t0)
                    .putFloat(x0).putFloat(y1).putFloat(s0).putFloat(t1)
                    .putFloat(x1).putFloat(y1).putFloat(s1).putFloat(t1);

        }

        public void writeTextureVertexData(final ByteBuffer data) {
            data.putFloat(s0).putFloat(t0).putFloat(x0).putFloat(y0)
                    .putFloat(s1).putFloat(t0).putFloat(x1).putFloat(y0)
                    .putFloat(s0).putFloat(t1).putFloat(x0).putFloat(y1)
                    .putFloat(s1).putFloat(t0).putFloat(x1).putFloat(y0)
                    .putFloat(s0).putFloat(t1).putFloat(x0).putFloat(y1)
                    .putFloat(s1).putFloat(t1).putFloat(x1).putFloat(y1);
        }
    }

    @Override
    public int getWidth() {
        return this.image.getWidth();
    }

    @Override
    public int getHeight() {
        return this.image.getHeight();
    }

    @Override
    public ByteBuffer getData() {
        return this.image.getData();
    }

    @Override
    public PixelFormat getFormat() {
        return PixelFormat.R8_UNORM;
    }

    @Override
    public void close() {
        if (this.image != null) {
            this.fontInfo.free();
            this.cdata.free();
            this.image.close();
            this.image = null;
        }
    }

    public static FontImage loadASCII(final InputStream in, final float charHeight) throws IOException {
        return Tools.withStreamData(in, data -> new FontInfo()
                .withCharHeight(charHeight)
                .withFontData(data)
                .withFirstASCIIChar()
                .withASCIICharCount()
                .create());
    }
}
