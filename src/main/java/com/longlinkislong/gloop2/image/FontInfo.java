/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

import java.nio.ByteBuffer;

/**
 *
 * @author zmichaels
 */
public class FontInfo {
    public final char firstChar;
    public final int charCount;
    public final ByteBuffer fontData;
    public final float charHeight;
    public final int fontIndex;
    public static final float PT16_FONT = 16F * 96F / 72F;            
    
    public FontInfo() {
        this(null, PT16_FONT, ' ', 96, 0);
    }
    
    public FontInfo(
            final ByteBuffer fontData, 
            final float charHeight,
            final char firstChar, final int charCount,
            final int fontIndex) {
        
        if (fontData == null) {
            this.fontData = null;
        } else {
            this.fontData = fontData.isReadOnly() ? fontData : fontData.asReadOnlyBuffer();
        }
        
        this.charCount = charCount;
        this.firstChar = firstChar;
        this.charHeight = charHeight;
        this.fontIndex = fontIndex;
    }
    
    public FontInfo withFontData(final ByteBuffer fontData) {
        return new FontInfo(fontData, charHeight, firstChar, charCount, fontIndex);
    }
    
    public FontInfo withCharHeight(final float charHeight) {
        return new FontInfo(fontData, charHeight, firstChar, charCount, fontIndex);
    }
    
    public FontInfo withFirstASCIIChar() {
        return withFirstChar(' ');
    }
    
    public FontInfo withASCIICharCount() {
        return withCharCount(96);
    }
    
    public FontInfo withFirstChar(final char firstChar) {
        return new FontInfo(fontData, charHeight, firstChar, charCount, fontIndex);
    }
    
    public FontInfo withCharCount(final int charCount) {
        return new FontInfo(fontData, charHeight, firstChar, charCount, fontIndex);
    }
    
    public FontImage create() {
        return new FontImage(this);
    }        
}
