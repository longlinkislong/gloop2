/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

import java.nio.ByteBuffer;
import org.lwjgl.system.MemoryUtil;

/**
 * An implementation of Image that uses off-heap memory for backed data.
 *
 * @author zmichaels
 */
public final class BufferedImage implements Image {

    private final int width;
    private final int height;
    private ByteBuffer data;
    private final PixelFormat format;

    /**
     * Constructs a new BufferedImage and allocates the needed storage.
     *
     * @param width the width of the image.
     * @param height the height of the image.
     * @param format the format of the BufferedImage.
     */
    public BufferedImage(final int width, final int height, final PixelFormat format) {
        this.width = width;
        this.height = height;
        this.format = format;

        final int size;

        switch (format) {
            case R8_UNORM:
                size = width * height;
                break;
            case R8G8_UNORM:
                size = width * height * 2;
                break;
            case R8G8B8_UNORM:
                size = width * height * 3;
                break;
            case R8G8B8A8_UNORM:
                size = width * height * 4;
                break;
            default:
                throw new UnsupportedOperationException("Unsupported image format: " + format);
        }

        this.data = MemoryUtil.memCalloc(size);
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public ByteBuffer getData() {
        return this.data.slice();
    }

    @Override
    public PixelFormat getFormat() {
        return this.format;
    }

    @Override
    public long ptr() {
        return MemoryUtil.memAddress(this.data);
    }

    @Override
    public void close() {
        if (this.data != null) {
            MemoryUtil.memFree(this.data);
            this.data = null;
        }
        
    }

}
