/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

import java.nio.ByteBuffer;
import org.lwjgl.system.MemoryUtil;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * A lightweight image object that allows simple rectangle copies.
 *
 * @author zmichaels
 */
public interface Image extends AutoCloseable {   
    
    /**
     * Get the width of the image.
     *
     * @return the width
     */
    int getWidth();

    /**
     * Get the height of the image.
     *
     * @return the height
     */
    int getHeight();

    /**
     * Get the backing data of the image.
     *
     * @return the backing data. This should be a read-only ByteBuffer.
     */
    ByteBuffer getData();

    /**
     * Retrieves the pointer to the backing data. This method is allowed to
     * return 0 if the backing data is on the Java heap.
     *
     * @return the pointer to the backing data.
     */
    default long ptr() {
        return NULL;
    }

    default BufferedImage getSubimage(int x, int y, int w, int h) {
        final BufferedImage out = new BufferedImage(w, h, this.getFormat());
        final long src = this.ptr();
        final long dst = out.ptr();        

        assert src != NULL : "The source image is on the heap!";        
        
        final int scale = this.getFormat().bytes;
        final int lineSize = w * scale;
        
        for (int i = 0; i < h; i++) {
            final long srcOff = (y + i) * this.getWidth() + x;
            final long dstOff = i * w;
            final long lineSrc = src + srcOff * scale;
            final long lineDst = dst + dstOff * scale;

            MemoryUtil.memCopy(lineSrc, lineDst, lineSize);            
        }

        return out;
    }

    default void setSubimage(int x, int y, int w, int h, Image subimg) {
        assert this.getFormat() == subimg.getFormat() : "Formats must match!";

        final long src = subimg.ptr();
        final long dst = this.ptr();
        
        assert src != NULL : "The source image is on the heap!";
        assert dst != NULL : "The destination image is on the heap!";
                
        final int scale = this.getFormat().bytes;
        final int lineSize = w * scale;
        
        for (int i = 0; i < h; i++) {
            final long srcOff = i * subimg.getWidth();
            final long dstOff = (y + i) * this.getWidth() + x;
            final long lineSrc = src + srcOff * scale;
            final long lineDst = dst + dstOff * scale;            

            MemoryUtil.memCopy(lineSrc, lineDst, lineSize);
        }
    }

    PixelFormat getFormat();

    @Override
    default void close() {

    }
}
