/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

/**
 * Pixel formats supported by the Image API.
 *
 * @author zmichaels
 */
public enum PixelFormat {
    /**
     * 1 channel; 8 bits. This is probably 8 bits grayscale.
     */
    R8_UNORM(1, 1),
    /**
     * 2 channels; 8 bits each. This is probably 8 bits grayscale, 8 bits alpha.
     */
    R8G8_UNORM(2, 2),
    /**
     * 3 channels; 8 bits each. This is probably 8 bits red, 8 bits green, 8
     * bits blue.
     */
    R8G8B8_UNORM(3, 3),
    /**
     * 4 channels; 8 bits each. This is probably 8 bits red, 8 bits green, 8
     * bits blue, 8 bits alpha.
     */
    R8G8B8A8_UNORM(4, 4);

    /**
     * The number of channels per pixel.
     */
    public final int components;
    /**
     * The number of bytes per pixel. This number will equal the number of
     * channels if the format has 8 bits per channel.
     */
    public final int bytes;

    private PixelFormat(final int bytes, final int components) {
        this.bytes = bytes;
        this.components = components;
    }
}
