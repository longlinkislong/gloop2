/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.longlinkislong.gloop2.image;

import java.nio.ByteBuffer;
import java.util.Objects;
import org.lwjgl.system.MemoryUtil;

/**
 * An implementation of Image that uses an external image buffer 
 * @author zmichaels
 */
public final class ProxyImage implements Image {
    private final int width;
    private final int height;
    private final ByteBuffer data;
    private final PixelFormat format;
    
    public ProxyImage(final int width, final int height, final PixelFormat format, final ByteBuffer data) {
        this.width = width;
        this.height = height;
        this.data = data;
        this.format = format;
    }

    @Override
    public long ptr() {
        if (this.data.isDirect()) {
            return MemoryUtil.memAddress(this.data);
        } else {
            return MemoryUtil.NULL;
        }
    }
    
    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public ByteBuffer getData() {        
        return this.data.asReadOnlyBuffer();
    }

    @Override
    public PixelFormat getFormat() {
        return this.format;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (this == obj) {
            return true;
        } else if (obj instanceof ProxyImage) {
            final ProxyImage other = (ProxyImage) obj;
            
            return this.ptr() == other.ptr()
                    && this.format == other.format
                    && this.width == other.width
                    && this.height == other.height;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.width;
        hash = 83 * hash + this.height;
        hash = 83 * hash + Long.hashCode(this.ptr());
        hash = 83 * hash + Objects.hashCode(this.format);
        return hash;
    }
}
